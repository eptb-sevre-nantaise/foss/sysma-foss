-- Extension: unaccent

-- DROP EXTENSION unaccent;

CREATE EXTENSION IF NOT EXISTS unaccent
    SCHEMA public
    VERSION "1.1";
