 
-- ADD parameter SYSMA_OFFLINE_MODE_ENABLED to sysma_conf table if parameter not exists
WITH to_ins as (
SELECT 
	 'Activation du mode Off-line' AS  "parameter"
	,'SYSMA_OFFLINE_MODE_ENABLED' AS alias
	,'text' AS data_type
	,'off' AS value
	,0 AS display_order
	,'{"on":"Activé", "off":"Desactivé"}' AS value_choices
)
SELECT xx_99_utils.pg_insert_or_update(
    schemadest:='sysma' :: name,
    tabledest:= 'sysma_conf' ::name,
    ignore_col_list_in_json := '{sysma_conf_id}'::name[],
    matching_col_list_or_null_for_keeping_all:= '{alias}'::NAME[],
    json_raw := to_json (s) ::json,
    operation_mode := 'NO UPDATE ON SAME'::text,
    debugmode := FALSE ::boolean
   ) 
   FROM to_ins  s 
