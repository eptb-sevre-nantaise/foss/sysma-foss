/* SYSMA DATA MODEL MIGRATION FILE */;
/* -- Data Model Version from  3.0 -> 3.4 */;
/* -- Generated on : 2022-06-17 17:33:37.232384+02 */;
INSERT INTO  sysma.versions_schema (version_schema_num,version_schema_comment ) VALUES (3.1, 'Patch function export PG si type sysma is null'),(3.2, 'ADD not null pour colonne data_type dans tables [object/action/relation]_type_parameter'),(3.3, 'ADD functions export PG speed-up'),(3.4, 'ADD thesaurus_item_metadata');
;

COMMENT ON TABLE sysma.other_layer
  IS '{
   "display_groupe_order":101,
   "groupe_name":"Configuration de sysma",
   "table_comment":"Liste des couches au format SIG standard utilisées par exemple en affichage dans la zone couches utiles..."
}';

;
-- Function: xx_99_utils.f_reseau_hydro_correction_topologie_et_seg_isoles_v01_10(geometry, name, name, name, name, name, numeric, text, text, name, name, name, name, text, name, integer, boolean, boolean, numeric, numeric, integer)

-- DROP FUNCTION xx_99_utils.f_reseau_hydro_correction_topologie_et_seg_isoles_v01_10(geometry, name, name, name, name, name, numeric, text, text, name, name, name, name, text, name, integer, boolean, boolean, numeric, numeric, integer);


CREATE OR REPLACE FUNCTION xx_99_utils.f_reseau_hydro_correction_topologie_et_seg_isoles_v01_11(
    _geom_loc_root_seg geometry,
    _source_schemaname name,
    _source_tablename name,
    _source_geomname name,
    _dest_schemaname name,
    _dest_tablename name,
    _buffer_snap numeric DEFAULT 15,
    _colsnames_suffix text DEFAULT ''::text,
    _source_where_clause text DEFAULT NULL::text,
    _ecoulements_theo_schemaname name DEFAULT NULL::name,
    _ecoulements_theo_tablename name DEFAULT NULL::name,
    _ecoulements_theo_geomname name DEFAULT NULL::name,
    _ecoulements_theo_pkname name DEFAULT NULL::name,
    _ecoulements_theo_where_clause text DEFAULT NULL::text,
    _ecoulements_theo_ordername name DEFAULT NULL::name,
    _nb_boucles_max_find_theo integer DEFAULT 2000,
    _drop_if_exist_and_create_traitement_table_detail boolean DEFAULT true,
    _debug_mode boolean DEFAULT false,
    _begin_step numeric DEFAULT 1,
    _end_step numeric DEFAULT 999,
    _number_of_pass integer DEFAULT 0)
  RETURNS text AS
$BODY$
   DECLARE
   /*
   Author : ariviere

   : réflechi a utilise la précision du mnt pour /2 (exemple (5./2.) pour le fbiffer de recherche de réseau théorique ou réseau déjà injecté dans corr_detail (qui est en fait la couche de trasil iterative)
   
   Chaine de traitement SQL qui corrige une couche composite qui a perdu sa cohérence topologique de réseau.
   Est aussi capable de raccoder un tronçon isolé grace au donnée d'écoulempent théorique (issus d'un MNT surcreusé par le tracé de la couche en entrée à corriger) 
   Créée deux nouvelle tables : une nouvelle table et ajoute les colonnes suivantes :
         1) Table de résultats _dest_tablename 
            geom (linestring) = tronçon hydro découpés à chaque intersection. La géométrie est orientée
            correction_result[**] : OK : cours d'eau dans réseau topologique // ISOLE : Segment non rattaché
         2) Table détaillant les opération réalisées :
            geom geometry(LineString,2154),
            geom_ori geometry(LineString,2154),
            correction_context : 
               referentiel
               Seg Isole
               link_iso_2_theo
               Seg Isole => link_with_theo_stream
               link_to_pt_start_ori
               link_to_linestring_ori
               link_theo_part_or_not
            ref_max_theo_order

   Exemple d'appel :

set work_mem = '150000';

   SELECT xx_99_utils.f_reseau_hydro_correction_topologie_et_seg_isoles_v01_11 (
        _geom_loc_root_seg := ST_BUFFER(ST_SETSRID (ST_geomfromtext('POINT(355884 6687390)'),2154),100)::geometry          -- geometrie contenant le point terminal (aval) du segment root. Attention cette geom ne doit pas couvrir le point de départ du segment root
      , _source_schemaname := 'r020_territoire_hydrographie'::NAME                                                               -- nom du schema de la table contenant le réseau hydro à corriger
      , _source_tablename:='t_cours_ref_ddtm_et_ign':: NAME                                                                      -- nom de la table contenant le réseau hydro avec graph corrigé
      , _source_geomname := 'geom' ::  NAME                                                                                      -- nom de la colonne geom 
      , _dest_schemaname := 'm060_milieux_continuite_tetes_de_bv' ::  NAME                                                         -- Schéma de destination (création des nouvelles tables)
      , _dest_tablename := 't_cours_ref_ddtm_et_ign_corr'::  NAME                                                                      -- table de destination (création des nouvelles tables)
      , _buffer_snap :=15::numeric                                                                                                --distance de raccordement des cours d'eau
      , _colsnames_suffix := ''::name                                                                                             -- [optionnel] : suffixe qui sera ajouté aux noms des colonnes ajoutées
      , _source_where_clause :=  '  st_intersects(geom , (SELECT geom FROM r020_territoire_hydrographie.t_bvsn_actu_carthage) ) AND geom is not null ':: TEXT -- [optionnel] : Clause Where sur la donnee source (utiliser geom pour a geomtrie source)
      , _ecoulements_theo_schemaname :='m060_milieux_continuite_tetes_de_bv'::name                                                -- [optionnel] : Schéma contenant la couche des réseau théoriques surcreusé par la données en entrée
      , _ecoulements_theo_tablename :=  't_mnt_sc_ddtm_sk_stream_500'::name                                                       -- [optionnel] : table contenant la couche des réseau théoriques surcreusé par la données en entrée
      , _ecoulements_theo_geomname := 'wkb_geometry'::name                                                                        -- [optionnel] : géométrie de la table contenant la couche des réseau théoriques surcreusé par la données en entrée
      , _ecoulements_theo_pkname := 'ogc_fid' ::name                                                                             -- [optionnel] : pk  de la table contenant la couche des réseau théoriques surcreusé par la données en entrée
      ,_ecoulements_theo_where_clause :=  ' st_intersects(theo.wkb_geometry , (SELECT geom FROM r020_territoire_hydrographie.t_bvsn_actu_carthage) ) ':: TEXT  -- [optionnel] : Clause Where sur la réseau théoriques
      , _ecoulements_theo_ordername := 'shreve'::name                                                                            -- [optionnel] : Colonne contenant la meilleure données de hierarchisation des cours d'eau( amont / aval)                     
      , _nb_boucles_max_find_theo := 2000::int                                                                                   -- [optionnel] : Nombre de boucles max lors de la recherche d ecoulemnt théoriques (évite boucles infines)  
      , _drop_if_exist_and_create_traitement_table_detail :=TRUE::BOOLEAN                                                       -- [optionnel] : Efface la table de traitement (*_detail) si elle existe dejà et la recréé (TRUE par défaut / FALSE pour corriger des geom dans une seconde passe sans perdre les données déjà corrigées)

       , _debug_mode :=FALSE::BOOLEAN                                                                                             -- [optionnel] : Affiche les sorties de debug
       , _begin_step :=1                                                                                                        -- [optionnel] : commence le traitement à l'étape  X (1 :creation de la table detail, 2 orientation set snap, 3 prolongement par réseau theorique et  tag seg isolé, 4 creation table finale)
       , _end_step :=999                                                                                                       -- [optionnel] : termine le traitement à l'étape  X (1 :creation de la table detail, 2 orientation set snap, 3 prolongement par réseau theorique et tag seg isolé, 4 creation table finale)
       , _number_of_pass:=0::int                                                                                              -- [optionnel] : boucle n fois sur les traitement itératif 0 : pas de limite (ne supplente pas le paramètre pour éviter boucle infinie ;
_nb_boucles_max_find_theo)
 )

      
   notes de version :
   -V01_11 :   corrige descente théorique
   
   -V01_10 :   fusionne step 3 et 4 = 3 , step 5 devient step 4
               ajoute le filtre geographique en step 3
               supprime tag segment isolé en étape 3 (pas nécessaire)
               recursive descente de réseau théorique
   -V01_09 : recherche seg theo start point le plus près (corrige bug dans trancés sinueux) 
   -V01_08 : Ajout :_number_of_pass (pour limiter le traitements à n pass)
              Ajout : comment date update sur tables créées
   - V01_07 : Ajout steps (_begin_step et _end_step) pour gerer les etapes
   - V01_06 : add _delete_if_exist_and_create_dest_table : FALSE permet de relancer une correction sans effacer les geom dejà corrigées
   - V01_05 : evite bouclage infini _nb_boucles_max_find_theo sur recherche théorique + add debug_mode
   - V01_04 : si création link nearest point (and not to start point) => ajoute un point au niveau de la confluence sur le segment exutoire  (nécessite itération individuelle sur la recherche de seg isolés (buffer snap)) pour mettre à jour chaque géométrie si elle est impactée plusieurs fois
   - V01_03 : rattachement fin theo en utilisant link (snap_buffer) le theo est coupé à une distance légèrement < snap buffer
   - V01_02 : recherche reseau theo dans un buffer autour de l'union des seg_isoles
   - V01 : first version
 

   */
   --mode_debug BOOLEAN := _debug_mode ;
 ---true;
  -- TRUE  renvoi des notices

   
   rcd_add_link record;

   rcd_add_link_theo RECORD;

   i int;

   j int;

   str_sql text;

   str_sql_comment text;

   str_sql_f_orientation TEXT ;

   str_sql_add_link_isoles TEXT ;

   str_sql_find_better_theo TEXT;

   step numeric;


   str_sql_add_link_isoles_filtre_on_last_segment_theo_add TEXT;

   
   
   srid INT:=  srid from geometry_columns WHERE f_table_schema = _source_schemaname::text AND f_table_name = _source_tablename::text AND f_geometry_column = _source_geomname::text;

   _colsnames_suffix name := case WHEN _colsnames_suffix like '' THEN NULL ELSE _colsnames_suffix END ;


   col_geom_name NAME := 'geom' ||  coalesce ('_'|| _colsnames_suffix ,'') ;

   col_geom_ori_name NAME := 'geom_ori' ||  coalesce ('_'|| _colsnames_suffix ,'') ;

   col_corr_context_name  NAME := 'correction_context' || coalesce ('_'|| _colsnames_suffix ,'') ;

   col_corr_result_name NAME:= 'correction_result' || coalesce ('_'|| _colsnames_suffix ,'') ;


   col_corr_context_name_debug NAME := col_corr_context_name||'_debug';
 

   source_where_clause text := CASE WHEN (_source_where_clause IS NOT NULL  AND _source_where_clause <>'')   THEN ' WHERE ' ||  _source_where_clause   ELSE '' END;

   ecoulement_theo_where_clause_with_and TEXT :=  CASE WHEN ( _ecoulements_theo_where_clause IS NOT NULL  AND _ecoulements_theo_where_clause <>'') THEN ' AND ' ||  _ecoulements_theo_where_clause   ELSE '' END;


   dest_tablename_detail NAME:= _dest_tablename || '_detail';

   msg_rtn TEXT :='PAS de message ';

   BEGIN

   
   SET client_min_messages = ERROR;
 
   if _debug_mode IS TRUE THEN
      SET client_min_messages = NOTICE;
 -- pour le debug
   END IF;

   


   -- 
   -- 1 . Copie des geom de la table source et ajout  de colonnes:
   --    . geom_ori (pour le stokage de la geométrie orientée)
   --    . corr_context 
   -- 
   
   IF  _drop_if_exist_and_create_traitement_table_detail IS TRUE THEN
      str_sql :=format (
      $SQL$
      DROP TABLE IF EXISTS %I.%I ;

      $SQL$
      , _dest_schemaname ,  dest_tablename_detail
      );


      ELSE str_sql := '';

   END IF;



   str_sql := str_sql || format (
      $SQL$
      
      CREATE TABLE IF NOT EXISTS  %I.%I (
        gid SERIAL primary key
      , %I geometry(linestring,%s) 
      , %I geometry(linestring,%s) 
      , %I text
      , %I TEXT
      , ref_max_theo_order int DEFAULT 0 
      );

      
      INSERT INTO %I.%I (
       %I
       , %I
      )
      SELECT ST_CollectionExtract((ST_Dump(%I)).geom,2)
      , 'Referentiel'
       FROM 
      %I.%I 
  
      ;
 
      

      CREATE INDEX sx_%I%I ON %I.%I USING GIST (%I);

      CREATE INDEX sx_%I%I ON %I.%I USING GIST (%I);


      With filtre_ids as (
         SELECT gid from %I.%I 
      %s
      )

      DELETE FROM %I.%I 
      WHERE gid not in (select t.gid from filtre_ids t)
      

      
      $SQL$
      ,_dest_schemaname ,  dest_tablename_detail
      ,col_geom_name , srid 
      , col_geom_ori_name , srid 
      , col_corr_context_name
      , col_corr_context_name_debug
      
      , _dest_schemaname ,  dest_tablename_detail
      , col_geom_name
      , col_corr_context_name
      , _source_geomname
      ,_source_schemaname , _source_tablename


      , col_geom_name,  dest_tablename_detail  ,_dest_schemaname ,  dest_tablename_detail , col_geom_name
      , col_geom_ori_name,  dest_tablename_detail ,_dest_schemaname ,  dest_tablename_detail , col_geom_ori_name

      , _dest_schemaname ,  dest_tablename_detail
      , source_where_clause

      , _dest_schemaname ,  dest_tablename_detail
      );


      
      
   step := 1;

   if (_begin_step <= step AND _end_step >= step)  THEN 
      RAISE NOTICE 'str_sql: %', str_sql;
  
      Execute str_sql;

      msg_rtn :=  'Step : ' ||step::text;

      EXECUTE ' COMMENT ON TABLE "' || _dest_schemaname || '"."' ||dest_tablename_detail || '" IS $$
      Date de Création : '|| now() || ' 
      '|| msg_rtn || '$$ ';

   END IF;

   

   -- 
   -- 2 . Orientation des geom_graph_ori :
   --    
   -- 


   -- 2.1 orientation du segment root

   str_sql := format (
      $SQL$
      UPDATE %I.%I t_up SET  %I  = case WHEN st_intersects(St_startpoint(t_up.%I), %L::geometry) THEN ST_reverse(t_up.%I) ELSE t_up.%I END  
      WHERE ST_intersects (t_up.%I ,  %L::geometry) ;
 

      $SQL$
      ,_dest_schemaname ,  dest_tablename_detail , col_geom_ori_name , col_geom_name, _geom_loc_root_seg , col_geom_name , col_geom_name
      , col_geom_name , _geom_loc_root_seg
      );


   step := 2.1;

   if (_begin_step <= step AND _end_step >= step)  THEN 
    
           Execute str_sql;

           RAISE NOTICE 'str_sql: %', str_sql;
 
           msg_rtn :=  'Step : ' ||step::text;

           EXECUTE ' COMMENT ON TABLE "' || _dest_schemaname || '"."' ||dest_tablename_detail || '" IS $$
           Date de MAJ : '|| now() || ' 
           '|| msg_rtn || '$$ ';

   END IF;

  

   -- 2.2 orientation des autres segments 

      str_sql_f_orientation := format (
      $SQL$
       SELECT xx_99_utils.f_reseau_hydro_orient_seg_from_root_v01_02 (
           _geom_loc_root_seg := %L
         , _schemaname := %L                                                                
         , _tablename:=%L                                   
         , _tablepkname := 'gid'
         , _geomname := %L                                                                      
         , _geomoriname := %L
         , _nb_iteration_max:=%s::int
         )
      $SQL$
      , _geom_loc_root_seg
      , _dest_schemaname
      ,  dest_tablename_detail
      , col_geom_name
      , col_geom_ori_name
      , _number_of_pass
      );


   step := 2.2;

   if (_begin_step <= step AND _end_step >= step)  THEN 
    RAISE NOTICE 'str_sql_f_orientation: %', str_sql_f_orientation;
  
           Execute str_sql_f_orientation;

           msg_rtn :=  'Step : ' ||step::text;

           EXECUTE ' COMMENT ON TABLE "' || _dest_schemaname || '"."' ||dest_tablename_detail || '" IS $$
           Date de MAJ : '|| now() || ' 
           '|| msg_rtn || '$$ ';

   END IF;


   -- 2.3 Commentaire sur les ségments correctes sur le plan topologique
   str_sql := format (
   $SQL$

      update %I.%I t_up 
         SET  %I = '00_Referentiel_topologie_OK'
      WHERE t_up.%I IS NOT NULL 
      
    $SQL$
      , _dest_schemaname, dest_tablename_detail
      , col_corr_context_name
      , col_geom_ori_name
      );
  
   step := 2.3;

   if (_begin_step <= step AND _end_step >= step)  THEN 
      RAISE NOTICE 'SQL tag Référentinel topologie_OK: %', str_sql;

      Execute str_sql;
 
      msg_rtn :=  'Step : ' ||step::text;
  
      EXECUTE ' COMMENT ON TABLE "' || _dest_schemaname || '"."' ||dest_tablename_detail || '" IS $$
      Date de MAJ : '|| now() || ' 
      '|| msg_rtn || '$$ ';

   END IF;


   
   -- 2.4 recherche des segments isolés et rattachement si distance <= buffer snap)
   str_sql_add_link_isoles_filtre_on_last_segment_theo_add := '';
 -- servira en étape 3 pour ne travaillier que sur un segmentv isolé
   str_sql_add_link_isoles := format (
         $SQL$
         with w_seg_isoles as (
            SELECT t1.gid
            , t1.%I as geom
            , st_startpoint(t1.%I) as geom_start
            , st_endpoint(t1.%I) AS geom_end

            from  %I.%I t1
            WHERE t1.%I IS null
            %s
         )
          $SQL$
            , col_geom_name
            , col_geom_name
            , col_geom_name
            , _dest_schemaname,  dest_tablename_detail
            , col_geom_ori_name
            , str_sql_add_link_isoles_filtre_on_last_segment_theo_add
            );

            
   str_sql_add_link_isoles := str_sql_add_link_isoles ||format (
         $SQL$
         , new_link as (

         select 
            case when t_ori_start_point.geom_l is not null 
               THEN t_ori_start_point.geom_l
               ELSE  t_ori.geom_l
               END ::geometry(linestring,%s) as geom
             , 
             case when t_ori_start_point.geom_l is not null 
               THEN t_ori_start_point.geom_l
               ELSE  t_ori.geom_l
               END ::geometry(linestring,%s) as geom_oriented
             ,
			   case when t_ori_start_point.geom_l is not null 
               THEN 'Link (buffer) : Seg Isole => Oriented Linestring (point start)'
               ELSE 'Link (buffer) : Seg Isole => Oriented Linestring (nearest point)'
               END ::text as commentaire
             
			   , case when t_ori_start_point.geom_l is not null 
               THEN NULL
               ELSE gid_corr_to_up
               END ::int as gid_corr_to_up
            
            FROM 
               w_seg_isoles seg_iso
            $SQL$
            
            , srid
            , srid
            );


   str_sql_add_link_isoles := str_sql_add_link_isoles ||format (
         $SQL$
            LEFT JOIN LATERAL (
               with 
                  w_analyse_seg_start_with_ori_start as (
                     SELECT 
                          seg_iso.geom_start as geom_pt_start
                        , st_startpoint( t_corr.%I) as geom_pt_end
                        , ST_distance(seg_iso.geom_start,st_startpoint(t_corr.%I)) dist
                     FROM %I.%I t_corr 
                        WHERE ( st_intersects(st_buffer(seg_iso.geom_start,%s),t_corr.%I)  ) 
                        AND (st_intersects(st_buffer(seg_iso.geom_start,%s),st_startpoint(t_corr.%I)  ) )
                        AND t_corr.gid <> seg_iso.gid  
                        ORDER by ST_distance(seg_iso.geom_start,st_startpoint(t_corr.%I)) ASC LIMIT 1
                        )
					 
               , w_analyse_seg_end_with_ori_start as (
                    SELECT seg_iso.geom_end geom_pt_start
                    ,st_startpoint( t_corr.%I) geom_pt_end
                   , ST_distance(seg_iso.geom_end,st_startpoint(t_corr.%I)) dist
                    FROM %I.%I t_corr 
                           WHERE ( st_intersects(st_buffer(seg_iso.geom_end,%s),t_corr.%I)  ) 
                           AND (st_intersects(st_buffer(seg_iso.geom_end,%s),st_startpoint(t_corr.%I)  ) )
                           AND t_corr.gid <> seg_iso.gid  
                           ORDER by ST_distance(seg_iso.geom_end,st_startpoint(t_corr.%I)) ASC LIMIT 1
			                  )
				  , w_union  as (
                  SELECT  ST_makeline(geom_pt_start, geom_pt_end)  as  geom_l, dist FROM w_analyse_seg_start_with_ori_start
                  Union ALL
                  SELECT  ST_makeline(geom_pt_start, geom_pt_end)  as  geom_l, dist FROM w_analyse_seg_end_with_ori_start
               )
				 SELECT geom_l geom_l from w_union WHERE dist IS NOT NULL order by dist asc LIMIT 1
				 )
				 as t_ori_start_point ON TRUE
				$SQL$
            , col_geom_ori_name 
            , col_geom_ori_name 
            , _dest_schemaname,  dest_tablename_detail
            , _buffer_snap, col_geom_ori_name
            , _buffer_snap, col_geom_ori_name
            , col_geom_ori_name

            , col_geom_ori_name 
            , col_geom_ori_name 
            , _dest_schemaname,  dest_tablename_detail
            , _buffer_snap, col_geom_ori_name
            , _buffer_snap, col_geom_ori_name
            , col_geom_ori_name
            );


				 
   str_sql_add_link_isoles := str_sql_add_link_isoles ||format (
         $SQL$
             LEFT JOIN LATERAL  (

                  with w_analyse_seg_start_with_ori as (
                        SELECT 
                       t_corr.gid gid_corr_to_up
                     , seg_iso.geom_start as geom_pt_start
                     ,ST_ClosestPoint(t_corr.%I, seg_iso.geom_start) as geom_pt_end
                     , ST_distance(seg_iso.geom_start,ST_ClosestPoint(t_corr.%I, seg_iso.geom_start) ) dist
                    FROM %I.%I t_corr 
                           WHERE ( st_intersects(st_buffer(seg_iso.geom_start,%s),t_corr.%I)  ) 
                           AND t_corr.gid <> seg_iso.gid  
                           ORDER by ST_distance(seg_iso.geom_start,ST_ClosestPoint(t_corr.%I, seg_iso.geom_start) ) ASC LIMIT 1
                           )
                      
                , w_analyse_seg_end_with_ori as (
                    SELECT 
                     t_corr.gid gid_corr_to_up
                   , seg_iso.geom_end geom_pt_start
                   ,ST_ClosestPoint( t_corr.%I, seg_iso.geom_end)  geom_pt_end
                   , ST_distance(seg_iso.geom_end,ST_ClosestPoint(t_corr.%I, seg_iso.geom_end)) dist
                    FROM %I.%I t_corr 
                           WHERE ( st_intersects(st_buffer(seg_iso.geom_end,%s),t_corr.%I)  ) 
                           AND t_corr.gid <> seg_iso.gid  
                           ORDER by ST_distance(seg_iso.geom_end,ST_ClosestPoint(t_corr.%I, seg_iso.geom_end)) ASC LIMIT 1
                
                
                        )
                    , w_union  as (
                        SELECT  gid_corr_to_up, geom_pt_end, ST_makeline(geom_pt_start, geom_pt_end)  as  geom_l, dist FROM w_analyse_seg_start_with_ori
                        Union ALL
                        SELECT  gid_corr_to_up, geom_pt_end, ST_makeline(geom_pt_start, geom_pt_end)  as  geom_l, dist FROM w_analyse_seg_end_with_ori
                        )
                   SELECT gid_corr_to_up, geom_pt_end,  geom_l geom_l from w_union WHERE   geom_l IS NOT NULL AND dist IS NOT NULL order by dist asc LIMIT 1
                     ) t_ori ON TRUE

            $SQL$
             , col_geom_ori_name 
            , col_geom_ori_name 
            , _dest_schemaname,  dest_tablename_detail
            , _buffer_snap, col_geom_ori_name
            , col_geom_ori_name

            , col_geom_ori_name 
            , col_geom_ori_name 
            , _dest_schemaname,  dest_tablename_detail
            , _buffer_snap, col_geom_ori_name
            , col_geom_ori_name
            );

-- ajout de LIMIT 1 pour itérrer sur chaque cas
   str_sql_add_link_isoles := str_sql_add_link_isoles ||format (
         $SQL$            
                  WHERE  (t_ori_start_point.geom_l is not null  OR t_ori.geom_l is not null  ) 
                  LIMIT 1
                 )
         $SQL$
         );

   str_sql_add_link_isoles := str_sql_add_link_isoles ||format (
         $SQL$ 
           , w_add_point_on_oriented_seg_linked_at_nearest_point as (
               update %I.%I t_up 
                     set %I = (select xx_99_utils.ST_Addpoint_at_nearest_seg_on_linestring_V01_01 (
                                            _geomLinestring := ST_RemoveRepeatedPoints(t_up.%I)::geometry(linestring, 2154)
                                          , _geomPointToAdd := st_endpoint(new_link.geom_oriented) 
                                          )::geometry(linestring,2154) from new_link WHERE gid_corr_to_up = t_up.gid  LIMIT 1
                                          )
                      , %I = coalesce(%I ,'') || ' ==> add new point'
                      , %I = coalesce(%I ,'') || ' ==> add new point ' || (select st_astext(st_endpoint(new_link.geom_oriented)) from new_link WHERE gid_corr_to_up = t_up.gid  LIMIT 1)
               WHERE gid =  (select gid_corr_to_up from  new_link WHERE gid_corr_to_up is not null LIMIT 1)
               )
         $SQL$
            , _dest_schemaname,  dest_tablename_detail
            , col_geom_ori_name 
            , col_geom_ori_name
            , col_corr_context_name , col_corr_context_name
            , col_corr_context_name_debug , col_corr_context_name_debug
         );


   

   str_sql_add_link_isoles := str_sql_add_link_isoles || format (
         $SQL$ 
           insert into %I.%I 
            (%I, %I, %I)
            SELECT geom, geom_oriented, commentaire from new_link
			  RETURNING gid as rtn;

         $SQL$
            , _dest_schemaname,  dest_tablename_detail
            , col_geom_name, col_geom_ori_name , col_corr_context_name
         );


   

   i=0;
 
   step := 2.4;

   if (_begin_step <= step AND _end_step >= step)  THEN 
      LOOP 
         Execute str_sql_add_link_isoles into rcd_add_link;

         EXIT WHEN rcd_add_link.rtn is null;

         Execute str_sql_f_orientation;
  -- met à jour l'orientation
         i=i+1;

         if _debug_mode IS TRUE THEN
            if i=0  THEN -- première pass
               RAISE NOTICE 'str_sql_add_link_isoles: %', str_sql_add_link_isoles;

            END IF ;
 
            RAISE NOTICE 'nombre de boucles str_sql_add_link_isoles (premier passage): %', i;

         END IF ;

         EXIT WHEN (_number_of_pass <> 0 AND i>=_number_of_pass);
 --  si _number_of_pass est configuré et limité à i passes
      END LOOP;

   msg_rtn :=  'Step : ' ||step::text;

   EXECUTE ' COMMENT ON TABLE "' || _dest_schemaname || '"."' ||dest_tablename_detail || '" IS $$
   Date de MAJ : '|| now() || ' 
   '|| msg_rtn || '$$ ';

   END IF;



--
-- 3 Rattachement des derniers segments isolés avec les données d'écoulement théoriques surcreusés par la donnée d'origine (corrigée ou non)
--

--ToDo AR ajouter recherche des impossible à écouler par réseau théorique  (autre approche buffer sur théorique si dans bv unique):
/*
set work_mem = '150000';


drop table if exists ext_02_forum_marais.t_test_tecoul_the_impossible;

create table ext_02_forum_marais.t_test_tecoul_the_impossible as 
  WITH w_union_seg_isoles as (
         SELECT  (ST_Dump(ST_union(st_linemerge(geom)))).geom as geom 
         FROM ext_02_forum_marais.t_cours_eau_ign_corr_v02_detail t1
         WHERE t1.geom_ori IS NULL
      )
      , w_union_seg_isoles_with_max_order as (
         SELECT tu.geom 
         , st_buffer(tu.geom,15) geom_buff
         , max(t2.ref_max_theo_order) as actual_max_order
         FROM w_union_seg_isoles tu
         LEFT JOIN ext_02_forum_marais.t_cours_eau_ign_corr_v02_detail t2 ON (st_intersects(st_buffer(tu.geom,15), t2.geom) AND t2.geom_ori is null)
         GROUP BY tu.geom
         
         )

      --   select * from w_union_seg_isoles_with_max_order
      
      , w_union_seg_isoles_better_candidate as (
         SELECT  
            union_seg_isoles.geom  as   seg_isole_geom
          , union_seg_isoles.geom_buff as  seg_isole_geom_buff 
          , theo.wkb_geometry theo_geom
          , theo.shreve theo_order
          FROM 
            w_union_seg_isoles_with_max_order union_seg_isoles
          JOIN ext_02_forum_marais.t_mnt_sc_sk_stream_500 theo ON (ST_intersects (st_buffer(union_seg_isoles.geom_buff,15/100.),theo.wkb_geometry))
          WHERE union_seg_isoles.actual_max_order >= theo.shreve
           
           group by union_seg_isoles.geom,  union_seg_isoles.geom_buff,   theo.wkb_geometry 
          , theo.shreve ,union_seg_isoles.actual_max_order
          HAVING union_seg_isoles.actual_max_order >= max(theo.shreve)
          ORDER BY theo.shreve DESC, ST_Distance(ST_intersection(union_seg_isoles.geom_buff,theo.wkb_geometry), ST_endpoint(theo.wkb_geometry)) asc
         --LIMIT 1 
      )

select row_number() over () as gid, * from w_union_seg_isoles_better_candidate
      

*/
IF (_ecoulements_theo_schemaname IS NOT NULL AND _ecoulements_theo_schemaname  <> '')  THEN

   
   str_sql_find_better_theo := format (
      $SQL$
         -- 
         -- selection séguements isolés = geom_ori(=oriented) IS NULL) 
         --  
         WITH w_union_seg_isoles as ( 
            WITH isoles as(
               SELECT  (ST_Dump(ST_union(st_linemerge(%I)))).geom as geom 
               FROM %I.%I t1
               WHERE t1.%I IS NULL
            ) 
            SELECT * 
            FROM isoles
            %s -- filtre geographique
            
         )
      $SQL$
         , col_geom_name
         , _dest_schemaname, dest_tablename_detail
         , col_geom_ori_name 
         , source_where_clause
   );

   

   str_sql_find_better_theo := str_sql_find_better_theo || '
      ' || format (
      $SQL$
      -- 
      -- Recupere le max de ref_max_theo_order des segments compossants les unions de segments isolés
      -- car un segmenti solé peut déjà avoir été prolongé par un ou plusieurs dégments théoriques sans toutefois avoir encore été jusqu'au réseau principal (non isolé)
      --  
      , w_union_seg_isoles_with_max_order as (
         SELECT tu.geom 
         , st_buffer(tu.geom,%s) geom_buff
         , max(t2.ref_max_theo_order) as actual_max_order
         FROM w_union_seg_isoles tu
         LEFT JOIN %I.%I t2 ON (st_intersects(st_buffer(tu.geom,%s), t2.%I) AND t2.%I is null)
         GROUP BY tu.geom
         
         )
      $SQL$
      , _buffer_snap
      , _dest_schemaname, dest_tablename_detail , _buffer_snap, col_geom_name , col_geom_ori_name
   )
   ;

   
   str_sql_find_better_theo := str_sql_find_better_theo || '
      ' || format ( 
      $SQL$ 
      -- 
      -- Parmi l'union ds segments isolés
      -- Recherche les ségments théoriques pouvant les prolonger
      -- Selectionne le meilleur candidat pour le couple (segment isolé / réseau théorique) :
      -- -- a) segment théorique le plus aval (_coulements_theo_ordername le plus grand)
      -- -- b) Distance la plus courte entre entre l'intersection de theo etd e l'union des segments isolés et l'extrémité avale du segment theo correspondant
      -- 
       
      , w_union_seg_isoles_better_candidate as (
         SELECT  
            union_seg_isoles.geom  as   seg_isole_geom
          , union_seg_isoles.geom_buff as  seg_isole_geom_buff 
          , theo.%I theo_gid
          , theo.%I theo_geom
          , theo.%I theo_order
          FROM 
            w_union_seg_isoles_with_max_order union_seg_isoles
          JOIN %I.%I theo ON (ST_intersects (st_buffer(union_seg_isoles.geom_buff,%s/100.),theo.%I))
          WHERE union_seg_isoles.actual_max_order < theo.%I
            %s
          ORDER BY theo.%I DESC, ST_Distance(ST_intersection(union_seg_isoles.geom_buff,theo.%I), ST_endpoint(theo.%I)) asc  -- pour le segment théorique le plus aval (_coulements_theo_ordername le plus grand), 
         LIMIT 1 
      )
      $SQL$ 
      , _ecoulements_theo_pkname
      , _ecoulements_theo_geomname
      , _ecoulements_theo_ordername
      , _ecoulements_theo_schemaname, _ecoulements_theo_tablename, _buffer_snap, _ecoulements_theo_geomname
      , _ecoulements_theo_ordername
      , ecoulement_theo_where_clause_with_and
      , _ecoulements_theo_ordername, _ecoulements_theo_geomname, _ecoulements_theo_geomname
   );


   str_sql_find_better_theo := str_sql_find_better_theo || '
      ' || format (  
      $SQL$
      -- 
      -- Récupère les ids des segments isolés 
      -- Estime le start point et la distance du end_point au réseau théorique
      --

      , w_lst_seg_in_union_seg_to_update as (
         select 
               seg_iso.gid 
            ,  seg_iso.%I geom_isol_uniq
            , CASE WHEN st_distance(st_startpoint(seg_iso.%I),ST_endpoint(c.theo_geom)) < st_distance(st_endpoint(seg_iso.%I),ST_endpoint(c.theo_geom)) THEN
            --, CASE WHEN st_distance(st_startpoint(seg_iso.I),ST_startpoint(c.theo_geom)) < st_distance(st_endpoint(seg_iso.I),ST_startpoint(c.theo_geom)) THEN
               st_startpoint(seg_iso.%I)
               ELSE 
               st_endpoint(seg_iso.%I)
               END as estim_end_pt_geom_isol_uniq
            , CASE WHEN st_distance(st_startpoint(seg_iso.%I),ST_endpoint(c.theo_geom)) < st_distance(st_endpoint(seg_iso.%I),ST_endpoint(c.theo_geom)) THEN
            --, CASE WHEN st_distance(st_startpoint(seg_iso.I),ST_startpoint(c.theo_geom)) < st_distance(st_endpoint(seg_iso.I),ST_startpoint(c.theo_geom)) THEN
               st_distance(st_startpoint(seg_iso.%I),ST_endpoint(c.theo_geom))  -- ST_startpoint(c.
               ELSE 
               st_distance(st_endpoint(seg_iso.%I),ST_endpoint(c.theo_geom))  -- ST_startpoint(c.
               END as estim_distance_end_pt_geom_isol_uniq
         FROM w_union_seg_isoles_better_candidate c
         LEFT JOIN %I.%I seg_iso on (ST_intersects(c.seg_isole_geom, seg_iso.%I) AND seg_iso.%I IS NULL)
         
      )
      $SQL$ 
      , col_geom_name
      , col_geom_name, col_geom_name
      , col_geom_name
      , col_geom_name
      , col_geom_name, col_geom_name
      , col_geom_name
      , col_geom_name
      , _dest_schemaname, dest_tablename_detail,  col_geom_name, col_geom_ori_name


   );

      
   str_sql_find_better_theo := str_sql_find_better_theo || '
      ' || format (  
      $SQL$   
      , w_creation_link_00 as (
           SELECT
           c.theo_geom
         , c.theo_order
         , ST_endpoint(theo_geom) end_pt_theo
         -- recherche du point aval parmi les ségements isolés (de l'union sélectionnée pour cette itération)
         , (SELECT  estim_end_pt_geom_isol_uniq FROM w_lst_seg_in_union_seg_to_update ORDER BY estim_distance_end_pt_geom_isol_uniq ASC LIMIT 1) estim_end_pt_geom_isol
         -- recherche du point de rattachement avec le réseau théorique
         , ST_ClosestPoint(c.theo_geom,(SELECT  estim_end_pt_geom_isol_uniq FROM w_lst_seg_in_union_seg_to_update ORDER BY estim_distance_end_pt_geom_isol_uniq ASC LIMIT 1)) as estim_start_pt_theo
      from w_union_seg_isoles_better_candidate c
      )
      $SQL$  
   );


   str_sql_find_better_theo := str_sql_find_better_theo || '
      ' || format (  
      $SQL$   
      , w_creation_link_01 as (
      -- création de la géométrie d'un segment entre le point aval isolé et le point de rattachement théorique
      SELECT ST_Makeline(l.estim_end_pt_geom_isol, l.estim_start_pt_theo) as geom_link
      from w_creation_link_00 l
      )
      $SQL$ 
   );
 
        
   str_sql_find_better_theo := str_sql_find_better_theo || '
      ' || format (  
      $SQL$      
      , w_creation_link_theo_cut_01_part_amont as (
      SELECT
      -- création de la geometrie du ségment théorique sans sa partie en amont du point de rattachement
           l.theo_order
         ,  l.end_pt_theo
         , l.estim_end_pt_geom_isol 
         , l.estim_start_pt_theo
         , ST_collectionextract((ST_DUMP(st_split(l.theo_geom,ST_buffer(l.estim_start_pt_theo,%s/1000.)))).geom,2) as geom_link
      from w_creation_link_00 l
     
      )
      $SQL$ 
      ,  _buffer_snap
   );

   
   str_sql_find_better_theo := str_sql_find_better_theo || '
      ' || format (  
      $SQL$
      , w_creation_link_theo_02_cut_part_aval as (
      -- création de la geométrie du ségement isolé sans sa partie en aval du link
         select 
              CASE WHEN st_union(seg_ori.%I) is null then 
                p1.geom_link
                ELSE  ST_collectionextract((ST_DUMP(st_difference(p1.geom_link,st_buffer(st_union(seg_ori.%I),%s*0.8)))).geom,2) 
              END as geom_link
            , p1.estim_start_pt_theo
            ,p1.theo_order
         FROM w_creation_link_theo_cut_01_part_amont p1 
         LEFT JOIN %I.%I  seg_ori on (ST_intersects(p1.geom_link, seg_ori.%I ))
         WHERE  ST_intersects (p1.geom_link, st_buffer(p1.end_pt_theo,%s/100. ) )
         group by p1.geom_link ,p1.estim_start_pt_theo, p1.theo_order
      )
      $SQL$
      , col_geom_ori_name
      , col_geom_ori_name , _buffer_snap
      , _dest_schemaname, dest_tablename_detail, col_geom_ori_name 
      , _buffer_snap
   );


   str_sql_find_better_theo := str_sql_find_better_theo || '
      ' || format (  
      $SQL$
      , w_links as (
         SELECT   
            geom_link 
            ,0 as theo_order
            ,  'NEW Link : Isole => Theoric Stream' as context 
            FROM w_creation_link_01 WHERE  ST_length(geom_link)>0.
         union all
         select 
               ST_RemoveRepeatedPoints(st_addpoint(geom_link,estim_start_pt_theo, 0)) 
              , theo_order
              ,  'NEW Seg : Theoric Stream (part or not)' as context 
            FROM w_creation_link_theo_02_cut_part_aval 
            WHERE ST_intersects (geom_link, st_buffer(estim_start_pt_theo,%s/100. ) )
         AND ST_length(geom_link)>0.
      )
      $SQL$
      , _buffer_snap  

   );

   
   str_sql_find_better_theo := str_sql_find_better_theo || '
      ' || format (  
      $SQL$      
      , w_up as (
         update %I.%I t_up 
            SET ref_max_theo_order = (select p.theo_order FROM w_creation_link_theo_02_cut_part_aval p limit 1)
            , %I = %I || ' ==> ' || 'Seg Isole => link with theoric stream'
         WHERE t_up.gid in (select lst_seg.gid FROM w_lst_seg_in_union_seg_to_update lst_seg)
         )

      $SQL$
      , _dest_schemaname, dest_tablename_detail 
      , col_corr_context_name, col_corr_context_name
      );


   str_sql_find_better_theo := str_sql_find_better_theo || '
      ' || format (  
      $SQL$ 
      -- downstream theoric stream     
      , w_insert_down_theo_stream AS(
          with paths as (
             WITH RECURSIVE nodes_cte(theo_gid, geom, theo_order, inter ) AS (
                SELECT theo.%I as theo_gid, theo.%I as geom, theo.%I as theo_order, NULL::boolean as intersection_with_network_linked_to_exutoire

                FROM %I.%I AS theo 
                WHERE theo.%I = (select theo_gid FROM w_union_seg_isoles_better_candidate) -- racine

               UNION ALL
                SELECT c.%I as theo_gid, c.%I as geom, c.%I as theo_order, i.intersection_with_network_linked_to_exutoire
                FROM nodes_cte AS p
                , %I.%I AS c 
                LEFT JOIN LATERAL (
                SELECT ST_intersects(c.%I, hyd.geom) as intersection_with_network_linked_to_exutoire 
                from %I.%I hyd 
                where
                  ST_intersects(c.%I, hyd.geom)  
                  AND geom_ori is not null
                )  i on true

                WHERE ST_intersects(c.%I ,p.geom)
                AND  ST_intersects(ST_EndPoint(p.geom) ,ST_StartPoint(c.%I))              
                and  c.%I > p.theo_order
                AND i.intersection_with_network_linked_to_exutoire is NULL
            )
            SELECT n.*
            FROM nodes_cte AS n 

            where theo_gid <> (select theo_gid FROM w_union_seg_isoles_better_candidate) -- ne recopie pas le segment identifié comme meilleur candidat
            ORDER BY n.theo_order ASC
            limit 100
        )
        , w_max_theo_order as (
            select max (theo_order) as max_theo_order from paths
        )

        select 
          geom as geom_link
          , 'Inject Theoric downstream'::text as context  
          ,  w_m.max_theo_order  as theo_order 
          --, ogc_fid 
        from paths , w_max_theo_order w_m 
           

      )
      $SQL$
         , _ecoulements_theo_pkname ,  _ecoulements_theo_geomname,  _ecoulements_theo_ordername
         , _ecoulements_theo_schemaname, _ecoulements_theo_tablename
         , _ecoulements_theo_pkname 

         , _ecoulements_theo_pkname ,  _ecoulements_theo_geomname,  _ecoulements_theo_ordername
         , _ecoulements_theo_schemaname, _ecoulements_theo_tablename

         , _ecoulements_theo_geomname, _dest_schemaname, dest_tablename_detail
         , _ecoulements_theo_geomname
         , _ecoulements_theo_geomname
         , _ecoulements_theo_geomname
         , _ecoulements_theo_ordername
      ) ;



        str_sql_find_better_theo := str_sql_find_better_theo || '
      ' || format (
      $SQL$   
         insert into %I.%I 
            (%I,%I ,ref_max_theo_order) 
            SELECT geom_link, context, theo_order FROM w_links
            UNION ALL
            SELECT geom_link, context, theo_order FROM w_insert_down_theo_stream
         RETURNING gid as rtn;
     
      $SQL$     
         , _dest_schemaname, dest_tablename_detail
         , col_geom_name ,  col_corr_context_name 
      );

   
   -- RAISE NOTICE 'str_sql_find_better_theo: %', str_sql_find_better_theo;
 



   i=0;

   j=0;
 

   step := 3;

   if (_begin_step <= step AND _end_step >= step)  THEN 
      LOOP 
        
         Execute str_sql_find_better_theo into rcd_add_link_theo;

         if _debug_mode IS TRUE THEN
            RAISE NOTICE 'rcd_add_link_theo.rtn: %', rcd_add_link_theo.rtn;

         END IF;

         EXIT WHEN (rcd_add_link_theo.rtn IS NULL ) ;
 -- exit si pas de nouveau link theo ajouté 
         if _debug_mode IS TRUE THEN
            RAISE NOTICE 'str_sql_find_better_theo: %', str_sql_find_better_theo;
 
         END IF ;

         j=0;

         
         LOOP 
            if _debug_mode IS TRUE THEN
               RAISE NOTICE 'nombre de boucles add link: %', j;

            END IF;

            Execute str_sql_add_link_isoles into rcd_add_link;

            EXIT WHEN rcd_add_link.rtn is null;

            Execute str_sql_f_orientation;

            j=j+1;

            --EXIT WHEN j >=2;

            

         END LOOP;

        
      
         i=i+1;

         if _debug_mode IS TRUE THEN
            RAISE NOTICE 'nombre de boucles find_better theo: %', i;

         END IF;

         EXIT WHEN (i =_nb_boucles_max_find_theo OR (_number_of_pass <> 0 AND i>=_number_of_pass));
 -- exit si _nb_boucles_max_find_theo est attent (boucel infinie ?) ou si _number_of_pass est configuré et limité à i pass
      END LOOP;

      if i=0 THEN 
         msg_rtn :=  'Step : ' ||step::text || ' Super Il n''y a plus de segments isolés raccordables. ';

      ELSE
         msg_rtn :=  'Step : ' ||step::text || ' nombre de segments traités : '|| i::text;

      END IF;

      EXECUTE ' COMMENT ON TABLE "' || _dest_schemaname || '"."' ||dest_tablename_detail || '" IS $$
      Date de MAJ : '|| now() || ' 
      '|| msg_rtn || '$$ ';

   END IF;


   RAISE NOTICE 'nombre de segments traités : %', i;

   
   

   

END IF;


-- 3.x tag segement isole indiscociable de l'étape précédante ?
/*
   str_sql := format (
   $SQL$

      update %I.%I t_up 
         SET  %I = 'Seg Isole'
      WHERE t_up.%I IS NULL 
      
    $SQL$
      , _dest_schemaname, dest_tablename_detail
      , col_corr_context_name
      , col_geom_ori_name
      );
 
   step := 3;

   if (_begin_step <= step AND _end_step >= step)  THEN  
      RAISE NOTICE 'SQL tag ISOLE: %', str_sql;

      Execute str_sql;

      msg_rtn :=  'Step : ' ||step::text;

      EXECUTE ' COMMENT ON TABLE "' || _dest_schemaname || '"."' ||dest_tablename_detail || '" IS $$
      Date de MAJ : '|| now() || ' 
      '|| msg_rtn || '$$ ';


   END IF;
  

*/



-- 
-- 4 CReation de la table finale line Merge
--


str_sql := format (
      $SQL$
      DROP TABLE IF  EXISTS %I.%I;

      CREATE TABLE %I.%I AS 
      WITH dump_merge AS (
         SELECT 'OK' as %I, (ST_Dump(St_Linemerge(ST_Union(%I)))).geom ::geometry(linestring,%s) as geom
         FROM %I.%I  WHERE %I IS NOT NULL
         UNION ALL 
         SELECT 'ISOLE' as %I, (ST_Dump(St_Linemerge(st_union(%I)))).geom ::geometry(linestring,%s) as geom
         FROM %I.%I  WHERE %I IS  NULL
      )   
      SELECT row_number() over () ::bigint as gid 
      , * from dump_merge
      ;


      CREATE INDEX sx_%I%I ON %I.%I USING GIST (%I);


      ALTER TABLE %I.%I ADD CONSTRAINT pkey_%I PRIMARY KEY(gid);


      


      
      $SQL$
      ,_dest_schemaname ,  _dest_tablename
      ,_dest_schemaname ,  _dest_tablename
      , col_corr_result_name ,  col_geom_ori_name ,  srid
      ,_dest_schemaname ,  dest_tablename_detail, col_geom_ori_name
      , col_corr_result_name ,  col_geom_name  , srid
      ,_dest_schemaname ,  dest_tablename_detail, col_geom_ori_name

      , col_geom_name,  _dest_tablename  ,_dest_schemaname ,  _dest_tablename , col_geom_name

      ,_dest_schemaname ,  _dest_tablename  ,  _dest_tablename

      )
 ;

   step := 4;

   if (_begin_step <= step AND _end_step >= step)  THEN  
      RAISE NOTICE 'SQL CREATE DEST TABLE : %', str_sql;

      Execute str_sql;
 
      msg_rtn:='Step : ' || step || '. Traitement terminé . Voir résultat de correction dans la table  :' || _dest_schemaname || '.' || _dest_tablename || ' Il faut filter avec le champ '|| col_corr_context_name ||' = OK.Le detail des operation est dans la table :' || _dest_schemaname || '.' || dest_tablename_detail  ;

      EXECUTE ' COMMENT ON TABLE "' || _dest_schemaname || '"."' ||_dest_tablename || '" IS $$
      Date de création : '|| now() || ' 
      '|| msg_rtn || '$$ ';


   END IF;
 
   RETURN msg_rtn;

   END;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;



COMMENT ON TABLE sysma.layer_group
  IS '{
   "display_groupe_order":160,
   "groupe_name":"Configuration de sysma",
   "table_comment":"Liste des groupes de couches sysma"
}';

;
-- Function: sysma.f_dictionary_sync_v03(name, name, name, name, name, text, name, bigint, bigint, boolean, bigint)

-- DROP FUNCTION sysma.f_dictionary_sync_v04(name, name, name, name, name, text, name, bigint, bigint, boolean, bigint);


CREATE OR REPLACE FUNCTION sysma.f_dictionary_sync_v04(
    source_schema name,
    source_table name,
    dest_schema name,
    dest_table name,
    id_col_name name,
    col_list text,
    parent_col_name name,
    source_id bigint,
    dest_parent_id bigint,
    new_dest_id boolean DEFAULT false,
    modificateur_id bigint DEFAULT 1)
  RETURNS json AS
$BODY$

/*
permet de copier une entrée du dico de sysma depuis les tables fdw vers sysma des territoires mutualisés

V04 : compatibilité avec sysma V03 modificateur_id => modified_by
V03 : attribue id_modificateur = modificateur_id, 1 par défaut = init sysma
      
V02 : si new_dest_id := FALSE et id existe déjà dans le dictionnaire l'entrée ne sera pas ajoutée, et l'id existant sera retourné.

Exemple d'appel :

select sysma.f_dictionary_sync_v04(
     source_schema := 'rx000_sysma_referentiel_mutualise_fdw':: NAME
   , source_table := 'sysma_object_type_parameter':: NAME
   , dest_schema := 'sysma'::NAME
   , dest_table := 'sysma_object_type_parameter'::NAME
   , id_col_name := 'parameter_id'::NAME
   , col_list := ' parameter, description, data_type, ordered_by, created_at, modified_at, parameter_source, data_source, unit, required, parameter_alias, time_tracking, required_field_parameter, hidden_in_form '::text
   , parent_col_name := 'sysma_object_type_id'::NAME
   , source_id := '100'::bigint
   , dest_parent_id := '1'::bigint 
   , new_dest_id := FALSE::boolean  
   , modificateur_id :=1 -- init data (mutualisation)
   )

*/

declare 
sql text;


sql_test_exist TEXT;


bigint_test_exist bigint;

rtn json;


col_list_ins TEXT;

parent_col_name_or_not text;

dest_parent_id_or_not  text;


BEGIN

IF new_dest_id IS FALSE THEN
   sql_test_exist := format  (
   $SQL$ 

   SELECT %I   -- dest_parent_id , col_list_ins
   FROM %I.%I 
   WHERE %I = %s

   $SQL$
   , id_col_name
   , dest_schema, dest_table
   , id_col_name , source_id

   );


   execute sql_test_exist into bigint_test_exist;


   IF (bigint_test_exist::bigint = source_id::bigint) THEN

   rtn :=  to_json(t) FROM (SELECT bigint_test_exist as res_id, 'ID existe deja, entrée non ajoutée' as  res_comment) t;

      
   return   rtn;



   ELSE 

   if new_dest_id IS TRUE THEN 
      col_list_ins = col_list ;

   ELSE 
      col_list_ins = id_col_name || ', '|| col_list ;

   END IF ;


   if parent_col_name IS NULL THEN 
      parent_col_name_or_not = '' ;

      dest_parent_id_or_not = '';

      
   ELSE 
      parent_col_name_or_not = parent_col_name || ' , ' ;

      dest_parent_id_or_not = dest_parent_id || ' , ';


   END IF ;



   sql := format (
   $SQL$ 
   WITH ins as (
   INSERT INTO %I.%I   -- table dest
      (%s  %s, modified_by) -- parent_col_name , col_list_ins
   SELECT %s   %s , %s -- dest_parent_id , col_list_ins, modificateur_id
   FROM %I.%I 
   WHERE %I = %s
   -- LIMIT 1

   RETURNING  %I as res_id, 'ajout ok'::text as res_comment

  )
  select to_json (ins) from ins
   $SQL$
   , dest_schema, dest_table
   , parent_col_name_or_not, col_list_ins


   , dest_parent_id_or_not, col_list_ins , modificateur_id
   , source_schema, source_table
   , id_col_name , source_id

   , id_col_name
   );


   execute sql into rtn;


   return   rtn;


   END IF;


END IF;


END

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;


;
ALTER TABLE sysma.l_sysma_object_sysma_relation
   ALTER COLUMN track_historytables_id SET DEFAULT nextval('sysma.increment_x99_t_track_historytables'::regclass);

   
ALTER TABLE sysma.l_sysma_object_type_sysma_relation_type
   ALTER COLUMN track_historytables_id SET DEFAULT nextval('sysma.increment_x99_t_track_historytables'::regclass);

   
;
-- Function: sysma_export_layer.create_table_from_sysma_action_type_id(bigint, text, boolean)

-- DROP FUNCTION sysma_export_layer.create_table_from_sysma_action_type_id(bigint, text, boolean);


CREATE OR REPLACE FUNCTION sysma_export_layer.create_table_from_sysma_action_type_id(
    _sysma_action_type_id bigint,
    _url_base text DEFAULT 'https://sysma.sevre-nantaise.com'::text,
    _debugmode boolean DEFAULT false)
  RETURNS boolean AS
$BODY$
/*
constantes : sysma_export_layer , sysma.sysma_action_type , sysma.sysma_action, temp_sysma
exemple d'appel:
SELECT  sysma_export_layer.create_table_from_sysma_action_type_id(1, 'https://sysma.sevre-nantaise.com', FALSE);

SELECT  sysma_export_layer.create_table_from_sysma_action_type_id(10266796, 'https://eptb-vilaine.sysma.io', FALSE);


SELECT  sysma_export_layer.create_table_from_sysma_action_type_id(95);

SELECT  sysma_export_layer.create_table_from_sysma_action_type_id(100838);


SELECT  sysma_export_layer.create_table_from_sysma_action_type_id(sysma_action_type_id), *
from   sysma.sysma_action_type
WHERE 
  sysma_action_type_alias IS not null
order by sysma_action_type_id;



*/
DECLARE

--sysma_object_type_id int := sysma_export_layer.create_table_from_sysma_action_type_id(id_objetgeo);

t_text text;

is_parameter_action_exists boolean default false;


--lst_c_travaux text;


BEGIN
--lst_c_travaux ='sysma_action_id, travaux, sysma_action_type_id, date_debut, date_fin, id_organisation,  id_createur, annee_prog, statut, date_creation_travaux, id_modificateur, date_modification_travaux'
DROP TABLE IF EXISTS temp_sysma ;

-- RAISE NOTICE 'id type travaux %', $1;
 -- debug
is_parameter_action_exists:= sysma_export_layer.create_temptable_action_anayelement(_sysma_action_type_id );

         
         t_text := 
                
               ' DROP TABLE IF EXISTS sysma_export_layer.' || sysma_object_type_alias || '_act_' || sysma_action_type_alias || ';
' ||
               
 
                ' CREATE TABLE sysma_export_layer.' || sysma_object_type_alias || '_act_' || sysma_action_type_alias || 
                     ' AS SELECT ' ||
                       'row_number() over() as gid ' ||
                        

                       '
                       , xx_99_utils.f_html_no_html_specials_chars_and_balises(tstructure.organisation) ::character varying(250) organisation_act

                       , t.* 
                        
                       ' 
         || CASE WHEN is_parameter_action_exists = true then ', p.*' ELSE '' END ||
         /*if is_parameter_action_exists = true then
            t_text :=  t_text || ', p.*' ;

         END IF;

         */
          ' , null::text as sysma_link,  ' ||  c_geo.cols_objgeo || 
                       ', o.geom  ' ||
                       ' FROM  sysma.sysma_action t '
         || CASE WHEN is_parameter_action_exists = true then ' LEFT JOIN LATERAL sysma_export_layer.get_params_values_from_sysma_action_id(t.sysma_action_id, null::temp_sysma) p on true ' ELSE ''  END ||

         /*
         if is_parameter_action_exists = true then
            t_text :=  t_text || ' LEFT JOIN LATERAL sysma_export_layer.get_params_values_from_sysma_action_id(t.sysma_action_id, null::temp_sysma) p on true ' ;

         END IF;

         */
                           
         '  
          LEFT JOIN sysma.organisation tstructure on (t.organisation_id=tstructure.organisation_id)
         

         LEFT JOIN sysma.l_sysma_object_sysma_action l ON (l.sysma_action_id = t.sysma_action_id) ' ||
                           
                           '  JOIN sysma_export_layer.'  || sysma_object_type_alias || ' o ON (o.sysma_object_id = l.sysma_object_id)
                           
     
                         WHERE t.sysma_action_type_id = ' ||_sysma_action_type_id||' ;
 '
                        -- GROUP BY ||
                  --'ALTER TABLE sysma_export_layer.' || tto.alias_groupe ||'_' || sysma_object_type_alias || '_act_' || sysma_action_type_alias || ' ADD CONSTRAINT pk_' || tto.alias_groupe ||'_' || sysma_object_type_alias || '_act_' || sysma_action_type_alias || ' PRIMARY KEY(gid);
 || 'ALTER TABLE sysma_export_layer.'  || sysma_object_type_alias || '_act_' || sysma_action_type_alias || ' ADD CONSTRAINT pk_' ||'_' || sysma_object_type_alias || '_act_' || sysma_action_type_alias || ' PRIMARY KEY(gid);
' ||
                  
                  ' COMMENT ON TABLE sysma_export_layer.' || sysma_object_type_alias || '_act_' || sysma_action_type_alias || ' IS '' Thème : ' || sysma_object_type_alias || '_act_' || sysma_action_type_alias ||  ' - 
                  description_sysma_object : ' || coalesce(replace(xx_99_utils.f_html_no_html_specials_chars_and_balises( tto.description),'''',''''''), 'Non renseigné')::text || '  
                  description_sysma_action : ' || coalesce(replace(xx_99_utils.f_html_no_html_specials_chars_and_balises( tt.description),'''',''''''), 'Non renseigné')::text || '  
                   MAJ :' || now()|| ' user : ' || current_user || ''';
'
                ' ALTER TABLE sysma_export_layer.' || sysma_object_type_alias || '_act_' || sysma_action_type_alias || ' 
                     ALTER COLUMN geom TYPE geometry(' || sysma_export_layer.get_pg_geom_type_from_sysma_geom_type (geometry_type) || ', 2154) ;
 
                  CREATE INDEX sidx_' || sysma_object_type_alias || '_act_' || sysma_action_type_alias || '_geom  ON sysma_export_layer.' || sysma_object_type_alias || '_act_' || sysma_action_type_alias || '   USING gist  (geom) ;
 '

                ' COMMENT ON COLUMN sysma_export_layer.' || sysma_object_type_alias || '_act_' || sysma_action_type_alias || '.gid IS ''identifiant unique de la couche'';
' 
                ||  c_geo.col_comment ||  --  ajout des commentaires des colonnes des objets geo
                ';
' ||  c_travaux.col_comment ||  --  ajout des commentaires des colonnes des travaux
                ';
' ||
                ' COMMENT ON COLUMN sysma_export_layer.' || sysma_object_type_alias || '_act_' || sysma_action_type_alias || '.organisation_act  IS ''intitulé de la structure maître d''''ouvrage des travaux'' ;

 
                 COMMENT ON COLUMN sysma_export_layer.' || sysma_object_type_alias || '_act_' || sysma_action_type_alias || '.sysma_link  IS '' Lien vers les travaux sur sysma web'' ;
' ||
                  'UPDATE sysma_export_layer.' || sysma_object_type_alias || '_act_' || sysma_action_type_alias || ' set sysma_link = (select '''|| _url_base || '/carte/objet/''|| obj_sysma_object_id ||''/travaux/''|| sysma_action_id) ;
'  

                  'UPDATE sysma_export_layer.' || sysma_object_type_alias || '_act_' || sysma_action_type_alias || ' set sysma_action = (select xx_99_utils.f_html_no_html_specials_chars_and_balises(sysma_action)::text) ;
'  
    

                /*||
                ' COMMENT ON COLUMN sysma_export_layer.' || sysma_object_type_alias || '_act_' || sysma_action_type_alias || '.sysma_object_id IS ''identifiant de l''objet geographique'';
' ||
                ' COMMENT ON COLUMN sysma_export_layer.' || sysma_object_type_alias || '_act_' || sysma_action_type_alias || '.ogeo_date_creation IS ''date de création de l''objet geographique'';
' ||
                ' COMMENT ON COLUMN sysma_export_layer.' || sysma_object_type_alias || '_act_' || sysma_action_type_alias || '.ogeo_date_fin IS ''date de fin de validité de l''objet geographique'';
' ||  
                -- TODO continuer...
 */
                  

         from   sysma.sysma_action_type tt 
         LEFT JOIN sysma.sysma_object_type tto ON (tto.sysma_object_type_id = tt.sysma_object_type_id)
         LEFT JOIN LATERAL 
          (WITH w_col as (SELECT column_name  as col 
            , col_description(
            ('sysma_export_layer.' || table_name)::regclass::oid
            , c.ordinal_position
            ) as description FROM information_schema.columns c WHERE c.table_schema= 'sysma_export_layer' AND c.table_name = tto.sysma_object_type_alias 
               AND column_name <>'geom') 
            SELECT string_agg( 'o.' || col || ' AS obj_' || col , ',') cols_objgeo, 
            coalesce(string_agg('COMMENT ON COLUMN sysma_export_layer.' || tto.sysma_object_type_alias || '_act_' || tt.sysma_action_type_alias || '.obj_' || col ||
            '   IS ''' ||  coalesce(replace(xx_99_utils.f_html_no_html_specials_chars_and_balises( w_col.description),'''',''''''), 'Non renseigné') || ''';
 '
            , ';
'), ' -- Pas de description de champs') as col_comment
            FROM  w_col
            )  c_geo on true

            LEFT JOIN LATERAL 
          (WITH 
            w_col as (
            SELECT 
                  column_name  as col 
               , col_description(
                  ('sysma.' || table_name)::regclass::oid
                  , c.ordinal_position
                  ) 
               as description 
               FROM information_schema.columns c WHERE c.table_schema= 'sysma' AND c.table_name = 'sysma_action'
               ) 
            SELECT 
            coalesce(string_agg('COMMENT ON COLUMN sysma_export_layer.' || tto.sysma_object_type_alias || '_act_' || tt.sysma_action_type_alias || '.' || col ||
            '   IS ''' ||  coalesce(replace(xx_99_utils.f_html_no_html_specials_chars_and_balises( w_col.description),'''',''''''), 'Non renseigné') || ''';
 '
            , ';
'), ' -- Pas de description de champs') as col_comment
            FROM  w_col
            )  c_travaux on true
         WHERE sysma_action_type_id = _sysma_action_type_id AND sysma_action_type_alias IS not null
      ;


      IF _debugmode IS TRUE THEN
         RAISE NOTICE 'SQL part 1  : %', t_text;
 -- debug
      END IF;


      if is_parameter_action_exists = true then   

              -- Ajout des commentaire de champs des fiches travaux
         t_text := t_text || coalesce(string_agg( 
         ' COMMENT ON COLUMN sysma_export_layer.' || tto.sysma_object_type_alias || '_act_' || tt.sysma_action_type_alias || '.' || p.parameter_alias || 
         '   IS ''' || coalesce(p.description, 'Non renseigné') || ''';
 '
         , ';
'), ' -- Pas de description de champs')
            from   sysma.sysma_action_type tt 
            LEFT JOIN sysma.sysma_object_type tto ON (tto.sysma_object_type_id = tt.sysma_object_type_id)
             LEFT JOIN  LATERAL sysma_export_layer.get_params_dictionary_info_from_sysma_action_type_id(_sysma_action_type_id ) p on true
            WHERE tt.sysma_action_type_id = _sysma_action_type_id AND tt.sysma_action_type_alias IS not null;

      END IF;

      
      IF _debugmode IS TRUE THEN
         RAISE NOTICE 'SQL part 1 et 2  : %', t_text;
 -- debug
      END IF;


      if t_text is not null  then 
         EXECUTE format(
            $sql$
            %s 
            $sql$
          , t_text)
         --USING  _param::int
         ;


    
       
            return true;

        ELSE
            return FALSE;

        END IF;



END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;


;
-- Function: sysma_export_layer.get_pg_data_type_from_sysma_data_type(character varying)

-- DROP FUNCTION sysma_export_layer.get_pg_data_type_from_sysma_data_type(character varying);


CREATE OR REPLACE FUNCTION sysma_export_layer.get_pg_data_type_from_sysma_data_type(data_type character varying)
  RETURNS character varying AS
$BODY$


    
    SELECT 
    CASE 
       WHEN $1 ilike 'integerType' THEN  'INT'
       WHEN $1 ilike 'floatType' THEN  'NUMERIC'
       WHEN $1 ilike 'booleanType' THEN  'BOOLEAN'

    --   WHEN $1 ilike 'dayMonthYearDateType' THEN  'DATE'
    --WHEN $1 ilike 'geoObjectTypeLink' THEN  'BIGINT'

    
    ELSE 'TEXT'
    END as type_pg
  
   
  
$BODY$
  LANGUAGE sql STABLE 
  COST 100;

;
ALTER TABLE sysma.sysma_action_type_parameter
   ALTER COLUMN data_type SET NOT NULL;

COMMENT ON COLUMN sysma.sysma_action_type_parameter.data_type IS 'type de donnée caracteres, entier etc..';

COMMENT ON TABLE sysma.sysma_action_type_parameter
  IS '{
   "display_groupe_order":300,
   "groupe_name":"Gestion du dictionnaire de données",
   "table_comment":"liste des paramètres à suivre pour chaque type de travaux"
}';

;
ALTER TABLE sysma.sysma_object_type_parameter
   ALTER COLUMN data_type SET NOT NULL;

COMMENT ON COLUMN sysma.sysma_object_type_parameter.data_type IS 'type de donnée : texte, entier, flottant etc.';

COMMENT ON TABLE sysma.sysma_object_type_parameter
  IS '{
   "display_groupe_order":300,
   "groupe_name":"Gestion du dictionnaire de données",
   "table_comment":"Liste des paramètres pour chaque type d''objet"
}';

;
ALTER TABLE sysma.sysma_action_type_parameter
   ALTER COLUMN data_type TYPE character varying(50);

COMMENT ON COLUMN sysma.sysma_action_type_parameter.data_type IS 'type de donnée caracteres, entier etc..';

;
ALTER TABLE sysma.sysma_relation_type_parameter
   ALTER COLUMN data_type SET NOT NULL;

;
-- Function: sysma_export_layer.create_table_from_sysma_object_type_id(bigint, boolean, text)

-- 
-- DROP FUNCTION sysma_export_layer.create_empty_table_from_sysma_object_type_id(bigint, boolean, text);


CREATE OR REPLACE FUNCTION sysma_export_layer.create_empty_table_from_sysma_object_type_id(
    _sysma_object_type_id bigint,
    _objet_actuel boolean DEFAULT false
    --,  _url_base text DEFAULT 'https://sysma.sevre-nantaise.com'::text
    )
  RETURNS boolean AS
$BODY$


/*
Creation de la table vide avant copy from (fait par sysma)

Note de version :
- V01 : first implementation


constantes : sysma_export_layer , sysma.sysma_object_type , sysma.objetsgeo, temp_sysma
exemple d'appel:
SELECT  sysma_export_layer.create_empty_table_from_sysma_object_type_id(107988::int, true);

SELECT  sysma_export_layer.create_empty_table_from_sysma_object_type_id(100833::int);



SELECT  *, sysma_export_layer.f_sysma_create_table_from_sysma_object_type_id(sysma_object_type_id)
from   sysma.sysma_object_type
WHERE 
alias_groupe is not null AND sysma_object_type_alias IS not null
order by sysma_object_type_id;


*/
DECLARE

--sysma_object_type_id int := sysma_export_layer.f_sysma_rt_sysma_object_type_id_from_sysma_object_id(sysma_object_id);

t_text text;

t_text_c text;

_sysma_object_type_alias TEXT;

_description_objetgeo TEXT;

_geometry_type TEXT ;



schema_export_tout name:='sysma_export_layer_all' ;
 -- attention revoir les noms ??? (fait en urgence pour les besoins du CT 2015 -2019)
schema_export_actu name:='sysma_export_layer' ;
 -- attention revoir les noms ??? (fait en urgence pour les besoins du CT 2015 -2019)

schema_export name;


BEGIN
-- raise notice '_sysma_object_type_id : %', _sysma_object_type_id;
 -- debug

IF _objet_actuel is true 
         then schema_export:=schema_export_actu ;

      ELSE schema_export:=schema_export_all;

END IF;


DROP TABLE IF EXISTS temp_sysma ;

-- RAISE NOTICE '_sysma_object_type_id :  %', $1;
  -- debug

_sysma_object_type_alias := sysma_object_type_alias from   sysma.sysma_object_type  WHERE sysma_object_type_id = _sysma_object_type_id AND sysma_object_type_alias IS not null;



IF _sysma_object_type_alias is not null AND _sysma_object_type_alias <>'' THEN

_description_objetgeo := description from   sysma.sysma_object_type  WHERE sysma_object_type_id = _sysma_object_type_id ;

_geometry_type := geometry_type from   sysma.sysma_object_type  WHERE sysma_object_type_id = _sysma_object_type_id ;





      t_text := 
                $$ DROP TABLE IF EXISTS $$ ||schema_export || $$.$$ ||  _sysma_object_type_alias ||$$;
$$ ||  --alias_groupe ||'_' ||
                $$ CREATE TABLE $$  ||schema_export || $$ .$$  ||  _sysma_object_type_alias ||  --alias_groupe ||'_' ||
                     '  AS SELECT                       
                          o.sysma_object_id
                        , o.start_date
                        , o.end_date
                        , o.organisation_id
                        , xx_99_utils.f_html_no_html_specials_chars_and_balises(obj_organisation.organisation) ::character varying(250) organisation_obj
                        , o.created_by
                        , o.status
                        , o.sysma_object_type_id
                        , o.created_at 
                        , replace(xx_99_utils.f_html_no_html_specials_chars_and_balises(o.sysma_object),'''''''','''''''''''') AS sysma_object  
                        , o.modified_by AS modified_by
                        , o.modified_at AS modified_at
                        , o.geom ';



               if  sysma_export_layer.create_temptable_object_anayelement(_sysma_object_type_id ) then --  if  exists parameter whith valid alias ( L'objet a des parametres avec des alias SIG)
                     t_text := t_text  ||'
                        ,  p.* ' ;

               END IF;



               t_text := t_text  ||
                       ' FROM  sysma.sysma_object o ';


               if  sysma_export_layer.create_temptable_object_anayelement(_sysma_object_type_id ) then --  if  exists parameter whith valid alias ( L'objet a des parametres avec des alias SIG)
                     t_text := t_text  ||'
                        LEFT JOIN LATERAL sysma_export_layer.get_params_values_from_sysma_object_id(o.sysma_object_id, null::temp_sysma) p on true' ;

               END IF;


               t_text := t_text  ||'                           
                         LEFT JOIN sysma.organisation obj_organisation on (o.organisation_id=obj_organisation.organisation_id)
                         WHERE o.sysma_object_type_id = ' ||_sysma_object_type_id || 
                         
                           ' AND  (CASE WHEN ' || _objet_actuel || ' IS TRUE
                            
                           THEN   (o.end_date = ''0'' OR o.end_date ='''')  
                           ELSE 1=1
                           END )'
                         
                         || ' AND o.geom is not null 
                         LIMIT 0;
 ' ||
                  ' COMMENT ON TABLE ' ||schema_export || '.' ||  _sysma_object_type_alias || ' IS '' Thème : '  ||  _sysma_object_type_alias ||  ' - 
                  description_objetgeo : ' || coalesce(replace(xx_99_utils.f_html_no_html_specials_chars_and_balises( _description_objetgeo),'''',''''''), 'Non renseigné')::text || ' 
                   MAJ :' || now()|| ' user : ' || current_user || ''';
' ||
                '  ALTER TABLE ' ||schema_export || '.' ||  _sysma_object_type_alias || ' ADD CONSTRAINT ' ||  _sysma_object_type_alias || '_pkey PRIMARY KEY(sysma_object_id);

                  
                     ALTER TABLE ' ||schema_export || '.' ||  _sysma_object_type_alias || ' 
                     ALTER COLUMN geom TYPE geometry(' || sysma_export_layer.get_pg_geom_type_from_sysma_geom_type (_geometry_type) || ', 2154) ;
 
                     
                     ALTER TABLE ' ||schema_export || '.' ||  _sysma_object_type_alias || ' ADD column sysma_link character varying(510) ;

                     COMMENT ON COLUMN ' ||schema_export || '.' ||  _sysma_object_type_alias || '.sysma_link  IS '' Lien vers sysma web'' ;



                     
                     COMMENT ON COLUMN ' ||schema_export || '.' ||  _sysma_object_type_alias || '.organisation_obj  IS ''intitulé de la structure maître d''''ouvrage de la création de l''''objet dans le cas où il s''''agit d''''objets installés lors de travaux'' ;


                   

                     ALTER TABLE ' ||schema_export || '.' ||  _sysma_object_type_alias || ' ADD column phototheque_json jsonb ;

                     COMMENT ON COLUMN ' ||schema_export || '.' ||  _sysma_object_type_alias || '.phototheque_json  IS '' Json contenant les identifiants de la photothèque'' ;

                     UPDATE ' ||schema_export || '.' ||  _sysma_object_type_alias || ' set phototheque_json = (select sysma_export_layer.get_json_photos_from_sysma_object_id(sysma_object_id))::jsonb ;
 
                     
    
                 CREATE INDEX sx_' ||  _sysma_object_type_alias || '_geom  ON ' ||schema_export || '.' ||  _sysma_object_type_alias || '   USING gist  (geom) ;
 '

        
      ;

      -- Ajout des commentaire de champs à partir des donnnées de paramètres
      t_text := t_text || coalesce(string_agg( 
      'COMMENT ON COLUMN ' ||schema_export || '.' ||  o.sysma_object_type_alias || '.' || p.parameter_alias || 
      '   IS ''' || coalesce(p.description, 'Non renseigné') || ''';
 '
      , ';
'), ' -- Pas de description de champs')
         from   sysma.sysma_object_type o LEFT JOIN  LATERAL sysma_export_layer.get_params_dictionary_info_from_sysma_object_type_id(_sysma_object_type_id) p on true
         WHERE o.sysma_object_type_id = _sysma_object_type_id ;

         
  


      EXECUTE format(
         $sql$
            %s
         $sql$
       , t_text)
      ;


      
      -- return true;

-- Ajout des commentaire de champs à partir des tables objet geo
t_text_c := '';

t_text_c := t_text_c || 
'COMMENT ON COLUMN ' ||schema_export || '.' ||  o.sysma_object_type_alias || '.sysma_object_id'
'   IS '''  || coalesce (replace(xx_99_utils.f_html_no_html_specials_chars_and_balises( 
       xx_99_utils.f_rtn_column_comment('sysma', 'sysma_object', 'sysma_object_id'))
      ,'''',''''''), 'Non renseigné')::text
      || ''';
 '
      from   sysma.sysma_object_type o
   WHERE o.sysma_object_type_id = _sysma_object_type_id 
   --AND o.alias_groupe is not null 
   AND o.sysma_object_type_alias IS not null
;

      t_text_c := t_text_c || 
'COMMENT ON COLUMN ' ||schema_export || '.' ||  o.sysma_object_type_alias || '.start_date'
'   IS '''  || coalesce (replace(xx_99_utils.f_html_no_html_specials_chars_and_balises( 
       xx_99_utils.f_rtn_column_comment('sysma', 'sysma_object', 'start_date'))
      ,'''',''''''), 'Non renseigné')::text
      || ''';
 '
      from   sysma.sysma_object_type o
   WHERE o.sysma_object_type_id = _sysma_object_type_id 
   --AND o.alias_groupe is not null 
   AND o.sysma_object_type_alias IS not null
;

      t_text_c := t_text_c || 
'COMMENT ON COLUMN ' ||schema_export || '.' ||  o.sysma_object_type_alias || '.end_date'
'   IS '''  || coalesce (replace(xx_99_utils.f_html_no_html_specials_chars_and_balises( 
       xx_99_utils.f_rtn_column_comment('sysma', 'sysma_object', 'end_date'))
      ,'''',''''''), 'Non renseigné')::text
      || ''';
 '
      from   sysma.sysma_object_type o
   WHERE o.sysma_object_type_id = _sysma_object_type_id 
   --AND o.alias_groupe is not null 
   AND o.sysma_object_type_alias IS not null
   ;

               t_text_c := t_text_c || 
'COMMENT ON COLUMN ' ||schema_export || '.' ||  o.sysma_object_type_alias || '.organisation_id'
'   IS '''  || coalesce (replace(xx_99_utils.f_html_no_html_specials_chars_and_balises( 
       xx_99_utils.f_rtn_column_comment('sysma', 'sysma_object', 'organisation_id'))
      ,'''',''''''), 'Non renseigné')::text
      || ''';
 '
      from   sysma.sysma_object_type o
   WHERE o.sysma_object_type_id = _sysma_object_type_id 
   --AND o.alias_groupe is not null 
   AND o.sysma_object_type_alias IS not null
;

      t_text_c := t_text_c || 
'COMMENT ON COLUMN ' ||schema_export || '.' ||  o.sysma_object_type_alias || '.created_by'
'   IS '''  || coalesce (replace(xx_99_utils.f_html_no_html_specials_chars_and_balises( 
       xx_99_utils.f_rtn_column_comment('sysma', 'sysma_object', 'created_by'))
      ,'''',''''''), 'Non renseigné')::text
      || ''';
 '
      from   sysma.sysma_object_type o
   WHERE o.sysma_object_type_id = _sysma_object_type_id 
   --AND o.alias_groupe is not null 
   AND o.sysma_object_type_alias IS not null
;

      t_text_c := t_text_c || 
'COMMENT ON COLUMN ' ||schema_export || '.' ||  o.sysma_object_type_alias || '.status'
'   IS '''  || coalesce (replace(xx_99_utils.f_html_no_html_specials_chars_and_balises( 
       xx_99_utils.f_rtn_column_comment('sysma', 'sysma_object', 'status'))
      ,'''',''''''), 'Non renseigné')::text
      || ''';
 '
      from   sysma.sysma_object_type o
   WHERE o.sysma_object_type_id = _sysma_object_type_id 
   --AND o.alias_groupe is not null 
   AND o.sysma_object_type_alias IS not null
;

      t_text_c := t_text_c || 
'COMMENT ON COLUMN ' ||schema_export || '.' ||  o.sysma_object_type_alias || '.sysma_object_type_id'
'   IS '''  || coalesce (replace(xx_99_utils.f_html_no_html_specials_chars_and_balises( 
       xx_99_utils.f_rtn_column_comment('sysma', 'sysma_object', 'sysma_object_type_id'))
      ,'''',''''''), 'Non renseigné')::text
      || ''';
 '
      from   sysma.sysma_object_type o
   WHERE o.sysma_object_type_id = _sysma_object_type_id 
   --AND o.alias_groupe is not null 
   AND o.sysma_object_type_alias IS not null
;

 
            t_text_c := t_text_c || 
'COMMENT ON COLUMN ' ||schema_export || '.' ||  o.sysma_object_type_alias || '.created_at'
'   IS '''  || coalesce (replace(xx_99_utils.f_html_no_html_specials_chars_and_balises( 
       xx_99_utils.f_rtn_column_comment('sysma', 'sysma_object', 'created_at'))
      ,'''',''''''), 'Non renseigné')::text
      || ''';
 '
      from   sysma.sysma_object_type o
   WHERE o.sysma_object_type_id = _sysma_object_type_id 
   --AND o.alias_groupe is not null 
   AND o.sysma_object_type_alias IS not null
;

            t_text_c := t_text_c || 
'COMMENT ON COLUMN ' ||schema_export || '.' ||  o.sysma_object_type_alias || '.sysma_object'
'   IS '''  || coalesce (replace(xx_99_utils.f_html_no_html_specials_chars_and_balises( 
       xx_99_utils.f_rtn_column_comment('sysma', 'sysma_object', 'sysma_object'))
      ,'''',''''''), 'Non renseigné')::text
      || ''';
 '
      from   sysma.sysma_object_type o
   WHERE o.sysma_object_type_id = _sysma_object_type_id 
   --AND o.alias_groupe is not null 
   AND o.sysma_object_type_alias IS not null
;



   t_text_c := t_text_c || 
'COMMENT ON COLUMN ' ||schema_export || '.' ||  o.sysma_object_type_alias || '.modified_by'
'   IS '''  || coalesce (replace(xx_99_utils.f_html_no_html_specials_chars_and_balises( 
       xx_99_utils.f_rtn_column_comment('sysma', 'sysma_object', 'modified_by'))
      ,'''',''''''), 'Non renseigné')::text
      || ''';
 '
      from   sysma.sysma_object_type o
   WHERE o.sysma_object_type_id = _sysma_object_type_id 
   --AND o.alias_groupe is not null 
   AND o.sysma_object_type_alias IS not null
;


   t_text_c := t_text_c || 
'COMMENT ON COLUMN ' ||schema_export || '.' ||  o.sysma_object_type_alias || '.modified_at'
'   IS '''  || coalesce (replace(xx_99_utils.f_html_no_html_specials_chars_and_balises( 
       xx_99_utils.f_rtn_column_comment('sysma', 'sysma_object', 'modified_at'))
      ,'''',''''''), 'Non renseigné')::text
      || ''';
 '
      from   sysma.sysma_object_type o
   WHERE o.sysma_object_type_id = _sysma_object_type_id 
   --AND o.alias_groupe is not null 
   AND o.sysma_object_type_alias IS not null
;




       t_text_c := t_text_c || 
'COMMENT ON COLUMN ' ||schema_export || '.' ||  o.sysma_object_type_alias || '.geom'
'   IS '''  || coalesce (replace(xx_99_utils.f_html_no_html_specials_chars_and_balises( 
       xx_99_utils.f_rtn_column_comment('sysma', 'sysma_object', 'geom'))
      ,'''',''''''), 'Non renseigné')::text
      || ''';
 '
      from   sysma.sysma_object_type o
   WHERE o.sysma_object_type_id = _sysma_object_type_id 
   --AND o.alias_groupe is not null 
   AND o.sysma_object_type_alias IS not null
;





EXECUTE format(
   $sql$
   %s 
   
   $sql$
 , t_text_c)
 ;


return true;

ELSE 
return FALSE;

END IF;
 -- alias exists
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;



;
COMMENT ON TABLE sysma."l_sysma_object_sysma_relation"
  IS '{
   "display_groupe_order":500,
   "groupe_name":"Données saisies",
   "table_comment":"Liaison en les objets et les relations"
}';


;
-- select * from sysma."l_sysma_object_sysma_relation"
-- select * from sysma."l_sysma_object_type_sysma_relation_type"

COMMENT ON TABLE sysma."l_sysma_object_sysma_relation"
  IS '{
   "display_groupe_order":500,
   "groupe_name":"Données saisies",
   "table_comment":"Les liaisons en les objets et les relations créées"
}';



COMMENT ON TABLE sysma."l_sysma_object_type_sysma_relation_type"
  IS '{
   "display_groupe_order":300,
   "groupe_name":"Gestion du dictionnaire de données",
   "table_comment":"Liste des liens possibles entre relations et objets"
}';


COMMENT ON TABLE sysma."sysma_relation_type"
  IS '{
   "display_groupe_order":300,
   "groupe_name":"Gestion du dictionnaire de données",
   "table_comment":"Liste des types de relations"
}';


COMMENT ON TABLE sysma."sysma_relation_type_parameter"
  IS '{
   "display_groupe_order":300,
   "groupe_name":"Gestion du dictionnaire de données",
   "table_comment":"Liste des parametres des relations"
}';



COMMENT ON TABLE sysma."sysma_relation_type_parameter_choice"
  IS '{
   "display_groupe_order":300,
   "groupe_name":"Gestion du dictionnaire de données",
   "table_comment":"Liste des choix des parametres des relations"
}';


COMMENT ON TABLE sysma."sysma_relation"
  IS '{
   "display_groupe_order":500,
   "groupe_name":"Données saisies",
   "table_comment":"Les relations créées"
}';


COMMENT ON TABLE sysma."sysma_relation_data"
  IS '{
   "display_groupe_order":500,
   "groupe_name":"Données saisies",
   "table_comment":"Les données des relations créées"
}';
;
-- Function: sysma_export_layer.get_params_dictionary_info_from_sysma_object_type_id(bigint)

-- DROP FUNCTION sysma_export_layer.get_params_dictionary_info_from_sysma_object_type_id(bigint);


CREATE OR REPLACE FUNCTION sysma_export_layer.get_params_dictionary_info_from_sysma_object_type_id(IN sysma_object_type_id bigint)
  RETURNS TABLE(parameter_id bigint, parameter character varying, parameter_alias character varying, data_type character varying, unit character varying, description text, required integer) AS
$BODY$
/*
Retourne les parametres d'un type d'objet geo
Exemple d'appel :
select * from sysma_export_layer.get_params_dictionary_info_from_sysma_object_type_id(1)
*/
 SELECT parameter_id::bigint
 , xx_99_utils.f_html_no_html_specials_chars_and_balises(parameter) as parameter
 , parameter_alias
  ,data_type
 , unit
 , COALESCE(replace(xx_99_utils.f_html_no_html_specials_chars_and_balises(description),'''',''''''), 'Non renseigné')::text  description 
 , required::int
  FROM sysma.sysma_object_type_parameter 
   where sysma_object_type_id = $1
      AND parameter_alias IS not NULL
      AND parameter_alias <>''
 ORDER BY  ordered_by ASC , parameter_id ASC
$BODY$
  LANGUAGE sql STABLE
  COST 100
  ROWS 1000;

;
-- Function: sysma_export_layer.get_params_dictionary_info_from_sysma_action_type_id(bigint)

-- DROP FUNCTION sysma_export_layer.get_params_dictionary_info_from_sysma_action_type_id(bigint);


CREATE OR REPLACE FUNCTION sysma_export_layer.get_params_dictionary_info_from_sysma_action_type_id(IN _sysma_action_type_id bigint)
  RETURNS TABLE(parameter_id bigint, parameter character varying, parameter_alias character varying, data_type character varying, unit character varying, description text, required integer) AS
$BODY$

-- select *  from sysma_export_layer.get_params_dictionary_info_from_sysma_action_type_id(1)
 SELECT parameter_id::bigint
 , parameter
 , parameter_alias
 , data_type
 , unit
 ,  COALESCE(replace(xx_99_utils.f_html_no_html_specials_chars_and_balises(description),'''',''''''), 'Non renseigné')::text  description 
 ,  required::int
  FROM sysma.sysma_action_type_parameter 
   where sysma_action_type_id = $1
      AND parameter_alias IS not NULL
      AND parameter_alias <>''
 ORDER BY  ordered_by ASC , parameter_id ASC
$BODY$
  LANGUAGE sql STABLE
  COST 100
  ROWS 1000;

;
COMMENT ON COLUMN sysma.sysma_object.end_date
  IS 'date de suppression sur le terrain';

;
ALTER TABLE sysma.sysma_object_type ADD COLUMN thesaurus_item_metadata jsonb;

COMMENT ON COLUMN sysma.sysma_object_type.thesaurus_item_metadata IS 'thesaurus_item_metadata';
;


-- https://gitlab.sevre-nantaise.com/eptbsn/sysma-tickets/-/issues/205
UPDATE sysma.data_type_choice
SET data_type = 'sysmaObjectLinkType'
WHERE data_type='geoObjectTypeLink';

