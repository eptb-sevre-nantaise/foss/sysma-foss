/* add svg icon path in sysma conf parameters */

WITH to_ins as (
select 
42 as sysma_conf_id
, 'Chemins des stocks d''icônes SVG (séparateur ,)' as "parameter"
, 'SVG_PATHS' as alias
, 'text' as data_type
, '/icon' as value
, 0 as display_order
, 'null'::json as value_choices
)

SELECT xx_99_utils.pg_insert_or_update(
    schemadest:='sysma' :: name,
    tabledest:= 'sysma_conf' ::name,
    ignore_col_list_in_json := '{sysma_conf_id}'::name[],
    matching_col_list_or_null_for_keeping_all:= '{alias}'::NAME[],
    json_raw := to_json (s) ::json,
    operation_mode := 'NO UPDATE ON SAME'::text,
    debugmode := FALSE ::boolean
   ) 
   FROM to_ins  s 
