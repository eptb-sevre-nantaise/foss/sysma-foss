-- ADD DEBUG_DEV_MAIL in sysma_conf table  
-- relate https://gitlab.sevre-nantaise.com/eptbsn/sysma-foss/-/merge_requests/81

ALTER TABLE IF EXISTS sysma.sysma_conf
    ADD CONSTRAINT alias_uniq UNIQUE (alias);
	
INSERT INTO sysma.sysma_conf (parameter,alias,data_type,value,display_order)
VALUES ('Adresse de courriel pour la transmission des notifications de débug aux équipes de dev. Laisser vide si vous ne voulez rien transmettre.'
		, 'DEBUG_DEV_MAIL'
		, 'text'
		, NULL::text
		, '0'
		);

