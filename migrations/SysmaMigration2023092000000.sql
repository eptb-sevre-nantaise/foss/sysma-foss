/*
2023-09-20 new functions raster mnt get alti
*/

-- FUNCTION: xx_99_utils.f_mnt_get_linestring_slope(json)

-- DROP FUNCTION IF EXISTS xx_99_utils.f_mnt_get_linestring_slope(json);

CREATE OR REPLACE FUNCTION xx_99_utils.f_mnt_get_linestring_slope(
	_json json)
    RETURNS TABLE(alti_up numeric, alti_down numeric, length numeric, slope numeric, geom_origin_is_well_oriented boolean, geom_oriented geometry) 
    LANGUAGE 'plpgsql'
    COST 100
    STABLE STRICT PARALLEL UNSAFE
    ROWS 1000

AS $BODY$

/* *************************************************************************************************
f_mnt_get_linestring_slope - SQL function retun slope from MNT (can use mutiples params)
    Author          : A RIVIERE 
    Version         : 1.0.0
    License         : ????
    Documentation   : https://

Inputs :
   "mntschemaname" : schema name of mnt table. For exemple "r020_territoire_physique"
 , "mnttablename"  : schema name of mnt table. For exemple  "t_mnt_alti_aspect_slope_percent_grass"
 , "mntgeomname"   : geometry column name in mnt table. For exemple "geom"
 , "mntaltiname"   : elevation column name in mnt table. For exemple "alti"
 , "geom"          : input geometry 
 , "buffer_find_m" : search distance in meters "15"
 , "function_find" : search function "MIN" or "MAX" or "AVG" 
 , "slope_rtn_unit" : unit for return slope value. For exemple "PC" = percentage , "mPERm" metre per metre  


Returns :
   alti_up numeric      :  maximum elevation (elevation of the starting point of the linestring or elevation of the finishing point of the linestring) 
   alti_down numeric    :  minimum elevation (elevation of the starting point of the linestring or elevation of the finishing point of the linestring) 
   length numeric       :  length of the linestring
   slope numeric        :  slope (exprimeted in argument function unit)
   geom_origin_is_well_oriented boolean : TRUE if the starting point of the input linestring is upper than his end point, else FALSE
   geom_oriented geometry : geometry with starting point upper or egual of end point (use ST_Reverse function on input geom if geom_origin_is_well_oriented is FALSE, otherwise return input geom.

Exemple d'appel :
1 ) Simple:

SELECT 
sysma_object_id,
(xx_99_utils.f_mnt_get_linestring_slope ((
   '{
   "mntschemaname" : "r020_territoire_physique"
 , "mnttablename" : "t_mnt_alti_aspect_slope_percent_grass"
 , "mntgeomname" :"geom"
 , "mntaltiname" :"alti"
 , "geom" :  "'|| foo.geom::text ||'"
 , "buffer_find_m" : "15"
 , "function_find"  : "MIN"  
 , "slope_rtn_unit"  : "PC" 
 }')::text :: json
 )).*
 FROM  (select sysma_object_id,  geom from sysma.sysma_object where sysma_object_type_id = 315280 ) as foo

 LIMIT 10

 
************************************************************************************************* */

DECLARE 
_mntschemaname name;
_mnttablename name;
_mntgeomname name;
_mntaltiname NAME;
_geom geometry;
_buffer_find_m numeric; -- buffer de recherche autour du point
_function_find TEXT;
_slope_rtn_unit TEXT;  -- unit of slope reterning value (PC=percentage ; DEG=degrees TODO à implementer)



geom_origin_start_pt_geom geometry;
geom_origin_end_pt_geom geometry;


geom_origin_start_pt_alti numeric;
geom_origin_end_pt_alti NUMERIC;


geom_origin_length numeric;

geom_origin_is_well_oriented boolean;

geom_oriented geometry;


function_get_alti_name text;
function_filter TEXT;
sql_geom_origin_start_pt_alti text ;
sql_geom_origin_end_pt_alti text ;
sql_retun_val text;
return_val NUMERIC;

alti_up NUMERIC;
alti_down NUMERIC;
slope NUMERIC;

--return_error json := null;
--return_warning json:=null;



BEGIN
_mntschemaname    := _json->>'mntschemaname';
_mnttablename     := _json->>'mnttablename';
_mntgeomname      := _json->>'mntgeomname';
_mntaltiname      := _json->>'mntaltiname';
_geom             := _json->>'geom';
_buffer_find_m    := _json->>'buffer_find_m';
_function_find    := _json->>'function_find';
_slope_rtn_unit  := _json->>'slope_rtn_unit';

function_get_alti_name:= 'xx_99_utils.f_mnt_get_alti';


--geomRecherche := ST_BUFFER((_geom)::geometry,_buffer_m);

--raise notice 'geom_buff , %', geomRecherche;


geom_origin_start_pt_geom := ST_StartPoint(_geom);

geom_origin_end_pt_geom := ST_EndPoint(_geom);

geom_origin_length := ST_Length(_geom);



sql_geom_origin_start_pt_alti := format (
$SQL$ 
   SELECT alti::numeric  from %s 
      ((
      '{
      "mntschemaname" : "%s"
    , "mnttablename" : "%s"
    , "mntgeomname" :"%s"
    , "mntaltiname" :"%s"
    , "geom" :  "%s"
    , "buffer_find_m" : "%s"
    , "function_find"  : "%s"  
    }')::text :: json
    )
  ;
$SQL$ 
   ,function_get_alti_name 
   ,_mntschemaname
   ,_mnttablename
   ,_mntgeomname
   ,_mntaltiname
   ,geom_origin_start_pt_geom
   ,_buffer_find_m
   ,_function_find

   )
   ;

--raise notice 'sql_geom_origin_start_pt_alti , %', sql_geom_origin_start_pt_alti;
EXECUTE sql_geom_origin_start_pt_alti into geom_origin_start_pt_alti;


sql_geom_origin_end_pt_alti := format (
$SQL$ 
   SELECT alti::numeric from %s 
      ((
      '{
      "mntschemaname" : "%s"
    , "mnttablename" : "%s"
    , "mntgeomname" :"%s"
    , "mntaltiname" :"%s"
    , "geom" :  "%s"
    , "buffer_find_m" : "%s"
    , "function_find"  : "%s"  
    }')::text :: json
    )
  ;
$SQL$ 
   ,function_get_alti_name 
   ,_mntschemaname
   ,_mnttablename
   ,_mntgeomname
   ,_mntaltiname
   ,geom_origin_end_pt_geom
   ,_buffer_find_m
   ,_function_find

   )
   ;
--raise notice 'geom_origin_end_pt_alti , %', sql_geom_origin_end_pt_alti;
EXECUTE sql_geom_origin_end_pt_alti into geom_origin_end_pt_alti;

if geom_origin_start_pt_alti < geom_origin_end_pt_alti 
   THEN
      geom_origin_is_well_oriented := FALSE;
      geom_oriented := ST_reverse(_geom);
      alti_up     := geom_origin_end_pt_alti;
      alti_down   := geom_origin_start_pt_alti;
   ELSE 
      geom_origin_is_well_oriented := TRUE;
      geom_oriented := _geom;
      alti_up     := geom_origin_start_pt_alti ;
      alti_down   := geom_origin_end_pt_alti;
END IF;


CASE _slope_rtn_unit 
   WHEN 'PC' THEN slope := (alti_up -alti_down)/(geom_origin_length/100.);
   WHEN 'mPERm' THEN slope := (alti_up -alti_down)/(geom_origin_length);
  
  -- WHEN 'DEG' THEN slope := (100.*alti_up-alti_down)/geom_origin_length;
END CASE;

RETURN  QUERY SELECT  
   alti_up::numeric as alti_up
 , alti_down::numeric  as alti_down
 , geom_origin_length::numeric as length
 , slope::numeric  as slope
 , geom_origin_is_well_oriented as geom_origin_is_well_oriented
 , geom_oriented::geometry as geom_oriented
 ;

--RETURN NEXT sql_start_alti::text;

/*
raise notice 'sql , %', sql_retun_val;
execute sql_retun_val into return_val;
RETURN return_val;

*/

END
$BODY$;




-- FUNCTION: xx_99_utils.f_mnt_rast_get_alti(json)

-- DROP FUNCTION IF EXISTS xx_99_utils.f_mnt_rast_get_alti(json);

CREATE OR REPLACE FUNCTION xx_99_utils.f_mnt_rast_get_alti(
	_json json)
    RETURNS TABLE(alti numeric,  geom_buffer2find geometry) 
    LANGUAGE 'plpgsql'
    COST 100
    STABLE STRICT PARALLEL UNSAFE
    ROWS 1000

AS $BODY$

/* *************************************************************************************************
f_mnt_rast_get_alti - SQL function retun alti from MNT (can use mutiples params)
    Author          : A RIVIERE 
    Version         : 1.0.0
    License         : ????
    Documentation   : https://

Inputs :
   "mntschemaname" : schema name of mnt table. For exemple "r020_territoire_physique"
 , "mnttablename"  : schema name of mnt table. For exemple  "r_mnt_alti_aspect_slope_percent_grass"
 , "mntrastername" : raster column name in mnt table. For exemple "rast"
 , "mntaltibande"  : alti bande number  in mnt table. For exemple "1"
 , "mntaltinodata" : nodatavalue in alti bande. For exemple "-99999.0000000000"
 , "mntrasterdim"  : raster pixel dimention. For exemple "5"
 , "geom"          : input geometry 
 , "buffer_find_m" : search distance in meters "15"
 , "function_find" : search function "MIN" or "MAX" or "AVG" 


Returns :
 alti : elevation value 
 geom_buffer2find : geometry of search area where function_find is apply -- usefull for debug

Exemple d'appel :
1 ) Simple:

SELECT 1 as gid  ,  geom as geom_origin, (xx_99_utils.f_mnt_rast_get_alti ((
   '{
   "mntschemaname" : "r020_territoire_physique"
 , "mnttablename"  : "r_mnt_alti_aspect_slope_percent_grass"
 , "mntrastername" : "rast"
 , "mntaltibande"  : "1"
 , "mntaltinodata" : "-99999.0000000000"
 , "mntrasterdim"  : "5"
 , "geom"          : "'|| foo.geom::text ||'"
 , "buffer_find_m" : "15"
 , "function_find" : "MIN"  
 }')::text :: json
 )).*
 FROM  (SELECT  ST_SetSRID(ST_POINT(368132,6680739),2154)::geometry(point,2154) as geom) as foo

 
************************************************************************************************* */

DECLARE 
_mntschemaname name;
_mnttablename name;
_mntrastername name;
_mntaltibande INT;
_mntaltinodata NUMERIC;
_mntrasterdim NUMERIC;
_geom geometry;
_buffer_find_m numeric;
_function_find TEXT;
geomRecherche geometry;
function_select TEXT;
sql_with TEXT;
sql_retun_val text;
return_val NUMERIC;
--return_error json := null;
--return_warning json:=null;



BEGIN
_mntschemaname := _json->>'mntschemaname';
_mnttablename := _json->>'mnttablename';
_mntrastername :=  _json->>'mntrastername';
_mntaltibande :=  _json->>'mntaltibande';
_mntaltinodata :=  _json->>'mntaltinodata';
_mntrasterdim :=  _json->>'mntrasterdim';
_geom :=  _json->>'geom';
_buffer_find_m := _json->>'buffer_find_m';
_function_find := _json->>'function_find';

if _buffer_find_m < _mntrasterdim then  -- to prenvent empty result on clip
	_buffer_find_m = _mntrasterdim;
END IF;
geomRecherche := ST_BUFFER((_geom)::geometry,_buffer_find_m);

--raise notice 'geom_buff , %', geomRecherche;


sql_with  := format (
$SQL$ 
with 
 clip_rast as (
	SELECT 
	   public.ST_Clip(
			%I --col rast
		  , %s -- num band
		  , %L::geometry -- geom de decoupage
		  , %s   -- no data values  exemple -99999.0000000000
		) as rast 
	 FROM %I.%I mnt
	WHERE mnt.%I &&  %L::geometry
	)

 SELECT 
  	  %L::geometry as geom_buffer2find
	, (ST_DumpAsPolygons(rast)).val as alti
	from clip_rast


$SQL$ 
   
   ,_mntrastername
   ,_mntaltibande
   ,geomRecherche
   ,_mntaltinodata
   ,_mntschemaname
   ,_mnttablename
   ,_mntrastername
   ,geomRecherche
   ,geomRecherche
   )
   ;
-- raise notice 'sql_with , %', sql_with;
-- if _function_find ='MIN' THEN function_select := ' alti, geom as geom_pix_alti , geom_buffer2find FROM find ORDER BY alti ASC LIMIT 1 '; END IF;
-- if _function_find ='MAX' THEN function_select := ' alti, geom as geom_pix_alti, geom_buffer2find FROM find ORDER BY alti DESC LIMIT 1 '; END IF;
-- if _function_find ='AVG' THEN function_select := ' avg(alti), st_union(geom) as geom_pix_alti , geom_buffer2find FROM find GROUP BY geom_buffer2find'; END IF;

if _function_find ='MIN' THEN function_select := ' alti::numeric, geom_buffer2find FROM find ORDER BY alti ASC LIMIT 1 '; END IF;
if _function_find ='MAX' THEN function_select := ' alti::numeric, geom_buffer2find FROM find ORDER BY alti DESC LIMIT 1 '; END IF;
if _function_find ='AVG' THEN function_select := ' avg(alti)::numeric, geom_buffer2find FROM find GROUP BY geom_buffer2find'; END IF;



sql_retun_val := format (
$SQL$ 
   WITH find as (%s
   )
   SELECT %s
 
$SQL$ 
   
   , sql_with
   ,function_select
  
   )
   ;
--raise notice 'sql , %', sql_retun_val;

RETURN  QUERY EXECUTE  sql_retun_val ; --NEXT return_val;



END
$BODY$;




-- FUNCTION: xx_99_utils.f_mnt_rast_get_linestring_slope(json)

-- DROP FUNCTION IF EXISTS xx_99_utils.f_mnt_rast_get_linestring_slope(json);

CREATE OR REPLACE FUNCTION xx_99_utils.f_mnt_rast_get_linestring_slope(
	_json json)
    RETURNS TABLE(alti_up numeric, alti_down numeric, length numeric, slope numeric, geom_origin_is_well_oriented boolean, geom_oriented geometry) 
    LANGUAGE 'plpgsql'
    COST 100
    STABLE STRICT PARALLEL UNSAFE
    ROWS 1000

AS $BODY$

/* *************************************************************************************************
f_mnt_rast_get_linestring_slope - SQL function retun slope from MNT (can use mutiples params)
    Author          : A RIVIERE 
    Version         : 1.0.0
    License         : ????
    Documentation   : https://

Inputs :
   "mntschemaname" : schema name of mnt table. For exemple "r020_territoire_physique"
 , "mnttablename"  : schema name of mnt table. For exemple  "r_mnt_alti_aspect_slope_percent_grass"
 , "mntrastername" : raster column name in mnt table. For exemple "rast"
 , "mntaltibande"  : alti bande number  in mnt table. For exemple "1"
 , "mntaltinodata" : nodatavalue in alti bande. For exemple "-99999.0000000000"
 , "mntrasterdim"  : raster pixel dimention. For exemple "5"
 , "geom"          : input geometry 
 , "buffer_find_m" : search distance in meters "15"
 , "function_find" : search function "MIN" or "MAX" or "AVG" 
 , "slope_rtn_unit" : unit for return slope value. For exemple "PC" = percentage , "mPERm" metre per metre  


Returns :
   alti_up numeric      :  maximum elevation (elevation of the starting point of the linestring or elevation of the finishing point of the linestring) 
   alti_down numeric    :  minimum elevation (elevation of the starting point of the linestring or elevation of the finishing point of the linestring) 
   length numeric       :  length of the linestring
   slope numeric        :  slope (exprimeted in argument function unit)
   geom_origin_is_well_oriented boolean : TRUE if the starting point of the input linestring is upper than his end point, else FALSE
   geom_oriented geometry : geometry with starting point upper or egual of end point (use ST_Reverse function on input geom if geom_origin_is_well_oriented is FALSE, otherwise return input geom.

Exemple d'appel :
1 ) Simple:

SELECT 
sysma_object_id,
(xx_99_utils.f_mnt_rast_get_linestring_slope ((
   '{
   "mntschemaname" : "r020_territoire_physique"
 , "mnttablename"  : "r_mnt_alti_aspect_slope_percent_grass"
 , "mntrastername" : "rast"
 , "mntaltibande"  : "1"
 , "mntaltinodata" : "-99999.0000000000"
 , "mntrasterdim"  : "5"
 , "geom"          : "'|| foo.geom::text ||'"
 , "buffer_find_m" : "15"
 , "function_find" : "MIN" 
 , "slope_rtn_unit"  : "PC" 
 }')::text :: json
 )).*
 FROM  (select sysma_object_id,  geom from sysma.sysma_object where sysma_object_type_id = 315280 ) as foo

 LIMIT 10

 
************************************************************************************************* */

DECLARE 
_mntschemaname name;
_mnttablename name;
_mntrastername name;
_mntaltibande INT;
_mntaltinodata NUMERIC;
_mntrasterdim NUMERIC;
_geom geometry;
_buffer_find_m numeric; -- buffer de recherche autour du point
_function_find TEXT;
_slope_rtn_unit TEXT;  -- unit of slope reterning value (PC=percentage ; DEG=degrees TODO à implementer)



geom_origin_start_pt_geom geometry;
geom_origin_end_pt_geom geometry;


geom_origin_start_pt_alti numeric;
geom_origin_end_pt_alti NUMERIC;


geom_origin_length numeric;

geom_origin_is_well_oriented boolean;

geom_oriented geometry;


function_get_alti_name text;
function_filter TEXT;
sql_geom_origin_start_pt_alti text ;
sql_geom_origin_end_pt_alti text ;
sql_retun_val text;
return_val NUMERIC;

alti_up NUMERIC;
alti_down NUMERIC;
slope NUMERIC;

--return_error json := null;
--return_warning json:=null;



BEGIN
_mntschemaname 		:= _json->>'mntschemaname';
_mnttablename 		:= _json->>'mnttablename';
_mntrastername 		:=  _json->>'mntrastername';
_mntaltibande 		:=  _json->>'mntaltibande';
_mntaltinodata 		:=  _json->>'mntaltinodata';
_mntrasterdim 		:=  _json->>'mntrasterdim';
_geom             	:= _json->>'geom';
_buffer_find_m    	:= _json->>'buffer_find_m';
_function_find    	:= _json->>'function_find';
_slope_rtn_unit  	:= _json->>'slope_rtn_unit';

function_get_alti_name:= 'xx_99_utils.f_mnt_rast_get_alti';


--geomRecherche := ST_BUFFER((_geom)::geometry,_buffer_m);

--raise notice 'geom_buff , %', geomRecherche;


geom_origin_start_pt_geom := ST_StartPoint(_geom);

geom_origin_end_pt_geom := ST_EndPoint(_geom);

geom_origin_length := ST_Length(_geom);



sql_geom_origin_start_pt_alti := format (
$SQL$ 
   SELECT alti::numeric  from %s 
      ((
      '{
      "mntschemaname" : "%s"
    , "mnttablename"  : "%s"
	, "mntrastername" : "%s"
	, "mntaltibande"  : "%s"
	, "mntaltinodata" : "%s"
	, "mntrasterdim"  : "%s"
    , "geom"          : "%s"
    , "buffer_find_m" : "%s"
    , "function_find" : "%s"  
    }')::text :: json
    )
  ;
$SQL$ 
   ,function_get_alti_name 
   ,_mntschemaname
   ,_mnttablename
   ,_mntrastername
   ,_mntaltibande
   ,_mntaltinodata
   ,_mntrasterdim
   ,geom_origin_start_pt_geom
   ,_buffer_find_m
   ,_function_find

   )
   ;

--raise notice 'sql_geom_origin_start_pt_alti , %', sql_geom_origin_start_pt_alti;
EXECUTE sql_geom_origin_start_pt_alti into geom_origin_start_pt_alti;


sql_geom_origin_end_pt_alti := format (
$SQL$ 
   SELECT alti::numeric from %s 
      ((
      '{
      "mntschemaname" : "%s"
    , "mnttablename" : "%s"
	, "mntrastername" : "%s"
	, "mntaltibande"  : "%s"
	, "mntaltinodata" : "%s"
	, "mntrasterdim"  : "%s"
    , "geom" :  "%s"
    , "buffer_find_m" : "%s"
    , "function_find"  : "%s"  
    }')::text :: json
    )
  ;
$SQL$ 
   ,function_get_alti_name 
   ,_mntschemaname
   ,_mnttablename
   ,_mntrastername
   ,_mntaltibande
   ,_mntaltinodata
   ,_mntrasterdim
   ,geom_origin_end_pt_geom
   ,_buffer_find_m
   ,_function_find

   )
   ;
--raise notice 'geom_origin_end_pt_alti , %', sql_geom_origin_end_pt_alti;
EXECUTE sql_geom_origin_end_pt_alti into geom_origin_end_pt_alti;

if geom_origin_start_pt_alti < geom_origin_end_pt_alti 
   THEN
      geom_origin_is_well_oriented := FALSE;
      geom_oriented := ST_reverse(_geom);
      alti_up     := geom_origin_end_pt_alti;
      alti_down   := geom_origin_start_pt_alti;
   ELSE 
      geom_origin_is_well_oriented := TRUE;
      geom_oriented := _geom;
      alti_up     := geom_origin_start_pt_alti ;
      alti_down   := geom_origin_end_pt_alti;
END IF;


CASE _slope_rtn_unit 
   WHEN 'PC' THEN slope := (alti_up -alti_down)/(geom_origin_length/100.);
   WHEN 'mPERm' THEN slope := (alti_up -alti_down)/(geom_origin_length);
  
  -- WHEN 'DEG' THEN slope := (100.*alti_up-alti_down)/geom_origin_length;
END CASE;

RETURN  QUERY SELECT  
   alti_up::numeric as alti_up
 , alti_down::numeric  as alti_down
 , geom_origin_length::numeric as length
 , slope::numeric  as slope
 , geom_origin_is_well_oriented as geom_origin_is_well_oriented
 , geom_oriented::geometry as geom_oriented
 ;

--RETURN NEXT sql_start_alti::text;

/*
raise notice 'sql , %', sql_retun_val;
execute sql_retun_val into return_val;
RETURN return_val;

*/

END
$BODY$;












