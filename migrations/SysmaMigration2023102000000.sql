-- Hack Fix export pg table with phototheque who have photo title with " 
-- relate https://gitlab.sevre-nantaise.com/eptbsn/sysma-tickets/-/issues/336
-- WARNING this new fonction change real photo title when they content " and replace by '


CREATE OR REPLACE FUNCTION sysma_export_layer.get_json_photos_from_sysma_object_id(
	_sysma_object_id bigint)
    RETURNS json
    LANGUAGE 'sql'
    COST 100
    STABLE PARALLEL UNSAFE
AS $BODY$



WITH list as (
SELECT 
photo_object_id, photo_id, photo_type, sysma_object_id, display_order, 
       before_after, track_historytables_id, photo_file, replace(photo_title, '"', '''') as photo_title, 
       photo_date

       FROM sysma.photo_object where sysma_object_id =$1
       order by photo_type asc, display_order asc

       )

,list2 as (
  SELECT  (photo_type || '_array')::text as k, jsonb_agg(row_to_json(l))::jsonb  as v from list l
       group by photo_type

)
select json_object_agg(k, v)::json  from list2



$BODY$;
