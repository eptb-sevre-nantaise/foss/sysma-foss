#!/bin/bash

##########################################################################################################################################
# Sysma DB migration update tool
##########################################################################################################################################



##########################################################################################################################################
# DO NOT EDIT BELOW UNLESS YOU KNOW WHAT YOU'RE DOING
##########################################################################################################################################
# consts
BLUE='\033[0;34m'
CYAN='\033[0;36m'
ORANGE='\033[0;32m'
LIGHTPURPLE='\033[1;35m'
LIGHTGREEN='\033[1;32m'


GREEN='\033[0;32m'
YELLOW='\033[0;33m'
RED='\033[0;31m'
NOCOLOR='\033[0m'
BOLD="\033[1m"

ASK_COLOR=$LIGHTPURPLE
INPUT_COLOR=$LIGHTGREEN

# Little helpers functions
checkError () {
    if [ $1 -ne '0' ]; then
        echo 
        echo -e $RED"Error | Migration exit."$NOCOLOR
        exit
    fi
}

##########################################################################################################################################
# Migrate Sysma DB up to date / ie. Apply all DB evolutions/migrations consecutively since the age of time.
##########################################################################################################################################

# get conf access private DB credentials
# sed -i "s/define('DBNAME',.*);/define('DBNAME', '$sysmadbname');/g" $CONF_ACCES_PRIVATE_FILE_PATH
# sed -i "s/define('DBUSER',.*);/define('DBUSER', '$dbuser');/g" $CONF_ACCES_PRIVATE_FILE_PATH
# sed -i "s/define('DBPWD',.*);/define('DBPWD', '$dbpswd');/g" $CONF_ACCES_PRIVATE_FILE_PATH
# sed -i "s/define('DBHOST',.*);/define('DBHOST', '$dbhost');/g" $CONF_ACCES_PRIVATE_FILE_PATH
# sed -i "s/define('DBPORT',.*);/define('DBPORT', '$dbport');/g" $CONF_ACCES_PRIVATE_FILE_PATH

echo -e ""
echo -e $NOCOLOR"Migrating Sysma DB up to date..."
echo -e ""

# awk '/^define/' ../conf/private/conf_access.private.php | awk "/'DBPWD'/"
# DBPWD=$(awk '/^define/' ../conf/private/conf_access.private.php | awk "/'DBPWD'/")
# define('DBPWD', '8fcf7e6d681461c8fa32e714aa7c1a73');
defines=$(awk '/^define/' ../conf/private/conf_access.private.php)
# echo $defines
# DBPWD=$(echo $line| sed -n "s/.*DBPWD', '([^"]*\).*//p")
dbpswd=$(echo $defines| grep -Po "'DBPWD', '\K[^']*")
dbuser=$(echo $defines| grep -Po "'DBUSER', '\K[^']*")
dbhost=$(echo $defines| grep -Po "'DBHOST', '\K[^']*")
dbport=$(echo $defines| grep -Po "'DBPORT', '\K[^']*")
sysmadbname=$(echo $defines| grep -Po "'DBNAME', '\K[^']*")

# echo $dbuser
# echo $dbpswd
# echo $dbhost
# echo $dbport

export PGPASSWORD=$dbpswd
export PGOPTIONS='--client-min-messages=warning'

already_migrated_once=0

ls | sort -n | while read filename
do
  # echo "Found file: $filename"
  # if [ "${filename}" == "*.sql" ]; then
  if [[ $filename == SysmaMigration*.sql ]]; then
    # echo -e $NOCOLOR"******************************************************************************************"
    if find "./already_migrated" -name ${filename} -print -quit | grep -q '^'; then
      if already_migrated_once==0; then
        echo -e "  - Skipping migration from ${filename} since it has already been done."
      else
        echo -e $RED"Error | This is pretty confusing. We already migrate at least once and now this late migration seems to have already been done. Check consistancy in migration and migrations/already_migrated folders and your DB state."
        echo -e "Error | Migration exit."$NOCOLOR
        exit -1
      fi
    else
      echo -e "  + Migrating from migration file ${filename}..." $RED
      psql $dbuser -h $dbhost -p $dbport -d $sysmadbname -f ${filename} --quiet -v "ON_ERROR_STOP=1"
      # echo -e "pe_esse_ku_elle $dbuser -h $dbhost -p $dbport -d $sysmadbname -f ${filename} --quiet"
      checkError $?
      cp ${filename} "./already_migrated"
      already_migrated_once=1
      echo -e $GREEN"    -> done"$NOCOLOR
    fi
  fi
done
echo -e ""