-- ADD parameter SYSMA_OFFLINE_MODE_ENABLED to sysma_conf table if parameter not exists
WITH to_ins as (
SELECT 
	 'Activation des fonctionalités RPG' AS  "parameter"
	,'RPG_MODE' AS alias
	,'text' AS data_type
	,'off' AS value
	,100 AS display_order
	,'{"on":"Activé", "off":"Desactivé"}' AS value_choices
UNION
SELECT 
	 'Table contenant les données RPG niveau 2 (format schema.table)' AS  "parameter"
	,'RPG_SCHEMA_TABLE' AS alias
	,'text' AS data_type
	, null AS value
	,101 AS display_order
	,null AS value_choices
UNION 
SELECT 
	 'Filtre éventuel sur la table RPG' AS  "parameter"
	,'RPG_FILTER_TABLE' AS alias
	,'text' AS data_type
	, null AS value
	,102 AS display_order
	,null AS value_choices
UNION
SELECT 
	'Année de référence des données RPG' AS  "parameter"
	,'RPG_YEAR' AS alias
	,'text' AS data_type
	, null AS value
	,103 AS display_order
	,null AS value_choices
UNION
SELECT 
	'Correspondance des champs de la table RPG (format json). Exemple : {"pacage": "pacage", "num_ilot": "num_ilot", "num_parcel": "num_parcel", "code_cultu": "code_cultu", "code_grp_culture": "code_grp_culture", "geom": "geom" }' AS  "parameter"
	,'RPG_MAPPING' AS alias
	,'json' AS data_type
	, null AS value
	,104 AS display_order
	,null AS value_choices
)
SELECT xx_99_utils.pg_insert_or_update(
    schemadest:='sysma' :: name,
    tabledest:= 'sysma_conf' ::name,
    ignore_col_list_in_json := '{sysma_conf_id}'::name[],
    matching_col_list_or_null_for_keeping_all:= '{alias}'::NAME[],
    json_raw := to_json (s) ::json,
    operation_mode := 'NO UPDATE ON SAME'::text,
    debugmode := FALSE ::boolean
   ) 
   FROM to_ins  s 
