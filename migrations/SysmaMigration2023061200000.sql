/* close https://gitlab.sevre-nantaise.com/eptbsn/sysma-foss/-/issues/104 */

ALTER TABLE sysma.x99_t_track_historytables ALTER COLUMN old_row TYPE jsonb using (old_row::jsonb);
ALTER TABLE sysma.x99_t_track_historytables ALTER COLUMN new_row TYPE jsonb using (new_row::jsonb);

