INSERT INTO sysma.sysma_conf
( "parameter", alias, data_type, value, display_order, value_choices)
select 
'Liste des sites pouvant intégrer des iframes, séparés par des espaces', 'FRAME_ANCESTORS', 'text', '', 0, null
WHERE 'FRAME_ANCESTORS' not in (select alias from sysma.sysma_conf)
