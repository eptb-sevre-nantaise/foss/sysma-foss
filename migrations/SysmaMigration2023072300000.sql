-- cf https://gitlab.sevre-nantaise.com/eptbsn/sysma-tickets/-/issues/299

-- update sequence value if necessary
SELECT setval('sysma.data_type_choice_data_type_id_seq', 14, true);

-- insert new value

INSERT INTO sysma.data_type_choice(	data_type, description)  VALUES ('hyperlink','Hyperlien');  
