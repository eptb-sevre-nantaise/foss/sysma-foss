/* SYSMA DATA MODEL MIGRATION FILE */;
/* -- Data Model Version from  3.4 -> 3.5 */;
/* -- Generated on : 2023-03-02 13:01:33.073047+01 */;
INSERT INTO  sysma.versions_schema (version_schema_num,version_schema_comment ) VALUES (3.5, '3.4 to 3.5 (avant offline)');
;
-- Function: xx_99_utils.f_mnt_get_alti(json)

-- DROP FUNCTION xx_99_utils.f_mnt_get_alti(json);


CREATE OR REPLACE FUNCTION xx_99_utils.f_mnt_get_alti(IN _json json)
  RETURNS TABLE(alti numeric, geom_pix_alti geometry, geom_buffer2find geometry) AS
$BODY$
/* *************************************************************************************************
f_mnt_get_alti - SQL function retun alti from MNT (can use mutiples params)
    Author          : A RIVIERE 
    Version         : 1.0.0
    License         : ????
    Documentation   : https://

Inputs :
   "mntschemaname" : schema name of mnt table. For exemple "r020_territoire_physique"
 , "mnttablename"  : schema name of mnt table. For exemple  "t_mnt_alti_aspect_slope_percent_grass"
 , "mntgeomname"   : geometry column name in mnt table. For exemple "geom"
 , "mntaltiname"   : elevation column name in mnt table. For exemple "alti"
 , "geom"          : input geometry 
 , "buffer_find_m" : search distance in meters "15"
 , "function_find" : search function "MIN" or "MAX" or "AVG" 


Returns :
 alti : elevation value 
 geom_pix_alti : geom union of elevation pixel used to evaluate alti  --  usefull for debug
 geom_buffer2find : geometry of search area where function_find is apply -- usefull for debug

Exemple d'appel :
1 ) Simple:

SELECT 1 as gid  ,  geom as geom_origin, (xx_99_utils.f_mnt_get_alti ((
   '{
   "mntschemaname" : "r020_territoire_physique"
 , "mnttablename" : "t_mnt_alti_aspect_slope_percent_grass"
 , "mntgeomname" :"geom"
 , "mntaltiname" :"alti"
 , "geom" :  "'|| foo.geom::text ||'"
 , "buffer_find_m" : "15"
 , "function_find"  : "MIN"  
 }')::text :: json
 )).*
 FROM  (SELECT  ST_SetSRID(ST_POINT(368132,6680739),2154)::geometry(point,2154) as geom) as foo

 
************************************************************************************************* */

DECLARE 
_mntschemaname name;

_mnttablename name;

_mntgeomname name;

_mntaltiname NAME;

_geom geometry;

_buffer_find_m numeric;

_function_find TEXT;

geomRecherche geometry;

function_select TEXT;

sql_with TEXT;

sql_retun_val text;

return_val NUMERIC;

--return_error json := null;

--return_warning json:=null;




BEGIN
_mntschemaname := _json->>'mntschemaname';

_mnttablename := _json->>'mnttablename';

_mntgeomname :=  _json->>'mntgeomname';

_mntaltiname :=  _json->>'mntaltiname';

_geom :=  _json->>'geom';

_buffer_find_m := _json->>'buffer_find_m';

_function_find := _json->>'function_find';



geomRecherche := ST_BUFFER((_geom)::geometry,_buffer_find_m);


--raise notice 'geom_buff , %', geomRecherche;



sql_with  := format (
$SQL$ 

   SELECT 
    mnt.%I::numeric as alti, mnt.%I ,%L::geometry AS geom_buffer2find
   FROM %I.%I mnt
   WHERE ST_intersects (%L::geometry,mnt.%I)
   AND mnt.%I IS NOT NULL

$SQL$ 
   
   , _mntaltiname
   ,_mntgeomname
   ,geomRecherche
   ,_mntschemaname
   ,_mnttablename
   ,geomRecherche
   ,_mntgeomname 
   ,_mntaltiname 
   )
   ;


if _function_find ='MIN' THEN function_select := ' alti, geom as geom_pix_alti , geom_buffer2find FROM find ORDER BY alti ASC LIMIT 1 ';
 END IF;

if _function_find ='MAX' THEN function_select := ' alti, geom as geom_pix_alti, geom_buffer2find FROM find ORDER BY alti DESC LIMIT 1 ';
 END IF;

if _function_find ='AVG' THEN function_select := ' avg(alti), st_union(geom) as geom_pix_alti , geom_buffer2find FROM find GROUP BY geom_buffer2find';
 END IF;





sql_retun_val := format (
$SQL$ 
   WITH find as (%s
   )
   SELECT %s
 
$SQL$ 
   
   , sql_with
   ,function_select
  
   )
   ;

--raise notice 'sql , %', sql_retun_val;


RETURN  QUERY EXECUTE  sql_retun_val ;
 --NEXT return_val;




END
$BODY$
  LANGUAGE plpgsql STABLE STRICT
  COST 100
  ROWS 1000;

;
CREATE OR REPLACE FUNCTION sysma.f_test_action_parameter_is_ld(parameter_id bigint)
  RETURNS boolean AS
$BODY$
   select true
   from sysma.sysma_action_type_parameter 
   where data_type  in ('textChoiceType', 'multipleTextChoiceType')
   AND parameter_id = $1
   $BODY$
  LANGUAGE sql IMMUTABLE
  COST 100;
;
;
CREATE OR REPLACE FUNCTION sysma.f_test_object_parameter_is_ld(parameter_id bigint)
  RETURNS boolean AS
$BODY$
   select true
   from sysma.sysma_object_type_parameter 
   where data_type  in ('textChoiceType', 'multipleTextChoiceType')
   AND parameter_id = $1
   $BODY$
  LANGUAGE sql IMMUTABLE
  COST 100;
;
;
CREATE OR REPLACE FUNCTION sysma.f_test_relation_parameter_is_ld(parameter_id bigint)
  RETURNS boolean AS
$BODY$
   select true
   from sysma.sysma_relation_type_parameter 
   where data_type  in ('textChoiceType', 'multipleTextChoiceType')
   AND parameter_id = $1
   $BODY$
  LANGUAGE sql IMMUTABLE
  COST 100;
;
;
ALTER TABLE IF EXISTS sysma.sysma_action_data DROP CONSTRAINT IF EXISTS sysma_action_data_non_ld_uniq;
;
ALTER TABLE sysma.sysma_action_data
  ADD CONSTRAINT sysma_action_data_non_ld_uniq EXCLUDE 
  USING btree (sysma_action_id WITH =, parameter_id WITH =, created_at WITH =) WHERE (sysma.f_test_action_parameter_is_ld(parameter_id) IS NOT TRUE);
;
ALTER TABLE IF EXISTS sysma.sysma_object_data DROP CONSTRAINT IF EXISTS sysma_object_data_non_ld_uniq;
;
ALTER TABLE sysma.sysma_object_data
  ADD CONSTRAINT sysma_object_data_non_ld_uniq EXCLUDE
  USING btree (sysma_object_id WITH =, parameter_id WITH =, end_date WITH =, start_date WITH =) WHERE (sysma.f_test_object_parameter_is_ld(parameter_id) IS NOT TRUE);
;
ALTER TABLE IF EXISTS sysma.sysma_relation_data DROP CONSTRAINT IF EXISTS sysma_relation_data_non_ld_uniq;
;
ALTER TABLE sysma.sysma_relation_data
  ADD CONSTRAINT sysma_relation_data_non_ld_uniq EXCLUDE
  USING btree (sysma_relation_id WITH =, parameter_id WITH =, created_at WITH =) WHERE (sysma.f_test_relation_parameter_is_ld(parameter_id) IS NOT TRUE);
;
ALTER TABLE sysma.status
    ADD CONSTRAINT uniq_status_statusvalue_objecttype UNIQUE (status, status_value, object_type);

;
ALTER TABLE sysma.sysma_object
    ALTER COLUMN status SET NOT NULL;


ALTER TABLE sysma.sysma_action
    ALTER COLUMN status SET NOT NULL;


ALTER TABLE sysma.sysma_relation
    ALTER COLUMN status SET NOT NULL;
;
INSERT INTO sysma.status(
	 status, status_value, description, object_type, display_order, style)
	SELECT 
	status, status_value, description, 'sysma_relation', display_order, style
	FROM sysma.status 
	WHERE object_type = 'sysma_object'
	EXCEPT 
	SELECT status, status_value, description, object_type, display_order, style
	FROM  sysma.status 
	WHERE object_type = 'sysma_relation'
	;

	
ALTER TABLE sysma.sysma_object
    ALTER COLUMN status SET NOT NULL;


ALTER TABLE sysma.sysma_action
    ALTER COLUMN status SET NOT NULL;


ALTER TABLE sysma.sysma_relation
    ALTER COLUMN status SET NOT NULL;
;
-- FUNCTION: xx_99_utils.pg_insert_or_update(name, name, name[], name[], json, text, boolean)

-- DROP FUNCTION IF EXISTS xx_99_utils.pg_insert_or_update(name, name, name[], name[], json, text, boolean);


CREATE OR REPLACE FUNCTION xx_99_utils.pg_insert_or_update(
	schemadest name,
	tabledest name,
	ignore_col_list_in_json name[],
	matching_col_list_or_null_for_keeping_all name[],
	json_raw json,
	operation_mode text,
	debugmode boolean DEFAULT false)
    RETURNS text
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$

/*
permet d'insérer ou mettre à jour une ligne ilike insert or update


select  xx_99_utils.pg_insert_or_update(
    schemadest:='xx_99_utils' :: name,
    tabledest:= 'pg_insert_or_update_test_dest' ::name,
    ignore_col_list_in_json := string_to_array('cc3',',')::NAME[]::name[],
    matching_col_list_or_null_for_keeping_all:= string_to_array('cc1,cc2',',')::NAME[],
    json_raw := to_json (s) ::json,
    operation_mode := 'UPDATE ON SAME'::text,

    debugmode := true ::boolean
    ) from xx_99_utils.pg_insert_or_update_test_source s where s.cc1 = 1
    ;


    SELECT 
      '01_source' as table_type
       , * 
      FROM  xx_99_utils.pg_insert_or_update_test_source 
    UNION ALL 
    SELECT
    '02_dest' 
    , * 
    FROM  xx_99_utils.pg_insert_or_update_test_dest 
    ORDER BY  table_type,cc1

-- préparation des tables de test
drop table if exists xx_99_utils.pg_insert_or_update_test_source;

create table xx_99_utils.pg_insert_or_update_test_source as (
SELECT 1::int as cc1, 't1 up'::text as cc2 , true::boolean cc3
union all 

SELECT 1::int as cc1, 't1bis up'::text as cc2 ,  false::boolean cc3
union all 

SELECT 2::int as cc1, 'toto2 tutu up'::text as cc2 ,  false::boolean cc3

union all 

SELECT 3::int as cc1, 'toto3 tutu up'::text as cc2 ,  false::boolean cc3

union all 

SELECT 4::int as cc1, 'toto4 tutu up'::text as cc2 ,  false::boolean cc3
);



drop table if exists xx_99_utils.pg_insert_or_update_test_dest;

create table xx_99_utils.pg_insert_or_update_test_dest  (
cc1 serial primary key , cc2 text, cc3 boolean)
;

INSERT into xx_99_utils.pg_insert_or_update_test_dest (cc1, cc2, cc3) 

SELECT 1::int as cc1, 'toto titi init'::text as cc2 ,  true
union all 

SELECT 2::int as cc1, 'toto2 tutu init'::text as cc2 , true


note de version :
V03 : DEBUG geometry geojson in json raw ( TODO adapter srid)
      DEBUD array type json -> array pg 
V02 : ADD capability to chose insert mode :  between use :
   UPDATE ON CONFLICT (need index on columns sometimes to heavy for loading in cache (exemple hub eau 'qualuié cours d'eau' ) or 
   UPDATE ON SAME (no need index ... slower)
   NO UPDATE ON CONFLICT
   NO UPDATE ON SAME
V01 : first implementation

*/
DECLARE
 


    rtn_txt TEXT;

    sql_ins_up text;

   sql_matching_col_list TEXT;
 
   matching_col_list NAME[];

   sql_unnest_ignore_col_in_json text;

   sql_unnest_matching_col_list TEXT;

   string_matching_col_list TEXT;

   sql_string_col_list_to_input text;

   string_col_list_to_input TEXT;

   sql_string_val_list_to_input TEXT;

   string_val_list_to_input TEXT;

   sql_string_set_values TEXT;

   string_set_values TEXT;


   sql_string_set_where TEXT;

   string_set_where TEXT;


   msg text;

   msg_precise_mode TEXT;

   skeep_update_set boolean;


   toto TEXT;

   sql_json_line_and_pg_type text;

   
   
    
BEGIN

  msg_precise_mode = 'preciser le mode operatoire : 
      UPDATE ON CONFLICT 
      NO UPDATE ON CONFLICT
      UPDATE ON SAME
      NO UPDATE ON SAME';

  -- s'il n'y a pas de colonnes à ignorer : select null , sinon liste les colonnes:  
  if ignore_col_list_in_json::name[] is not null  THEN 
      sql_unnest_ignore_col_in_json := 'SELECT unnest( ' || quote_literal(ignore_col_list_in_json::TEXT) || '::NAME[] )::TEXT as k';

  else 
      sql_unnest_ignore_col_in_json :='SELECT ''''::text as k';

  END IF;




  -- Liste des colonnes à utiliser pour la comparaison (retire les colonnes à ignorer s'il y en a), utilise toutes les colonnes si pas de restriction ( matching_col_list = NULL:name[])
  IF matching_col_list_or_null_for_keeping_all is null THEN
      
      sql_matching_col_list := format (
         $SQL$
         WITH jk AS (SELECT json_object_keys(%L::json) as k)
         SELECT string_to_array(string_agg(k,','),',')::name[]
         FROM jk 
         $SQL$
         , json_raw
         );

      execute sql_matching_col_list into matching_col_list;

      --matching_col_list := string_to_array(string_agg(json_object_keys(json_raw::json),','),',')::name[];
 
  ELSE 
      matching_col_list := matching_col_list_or_null_for_keeping_all;

  END IF;


  -- remove ignore cols
  matching_col_list :=   xx_99_utils.f_array_subtraction(
    _array_a:= matching_col_list::anyarray,
    _array_b:= ignore_col_list_in_json::anyarray,
    _strict_number_of_uniq_values:=false)::name[] ;

    

  -- sql de select :  table qui liste des colonnes dont on recherche la correspondance des valeurs (<>quasi pk) 
  sql_unnest_matching_col_list :=  'SELECT unnest( ' || quote_literal(matching_col_list::text) || '::NAME[])::TEXT ';


  -- liste des cols qui définissent en text (a, b, c...) 
  string_matching_col_list :=  array_to_string(matching_col_list::name[], ',');



  -- SQL qui permet d'obtenir la liste les colonnes à insérer
  sql_string_col_list_to_input :=  format (
     $SQL$
        WITH 
         json_lines as (select * from json_each_text(%L::json))
        SELECT string_agg(quote_ident(key),',')   
            from json_lines 
	  	    ORDER BY key -- ajout 2022 10 04
            WHERE key  NOT in (%s)  

      $SQL$
      , json_raw
      , sql_unnest_ignore_col_in_json
      ) ;


   
  if debugmode then  raise notice 'sql_string_col_list_to_input : % ', sql_string_col_list_to_input;
   END IF;

  execute sql_string_col_list_to_input into string_col_list_to_input ;

  if debugmode then  raise notice 'string_col_list_to_input : % ', string_col_list_to_input;
   END IF;

  IF sql_string_col_list_to_input IS NULL THEN 
      msg := 'problème string_col_list_to_input : est null ';

      raise notice '%', msg;

      RETURN 'SELECT ' ||quote_ident (msg) ;

  END IF;


sql_json_line_and_pg_type := format (
$SQL$
   dest_table_cols_pg_types as (
         SELECT * from xx_99_utils.f_table_columns_list ('%s', '%s') 
         )
      , json_lines as (
         select * from json_each_text(%L::json)
         )
      , json_lines_and_pgtype as (
        SELECT j.key
        ,  case when t.datatype::text ilike'character varying[]' 
           THEN  replace(replace(j.value::text , '[','{'),']','}')
            ELSE j.value
            END
        ,t.datatype 
        FROM json_lines j 
        LEFT JOIN dest_table_cols_pg_types t on t.col::name= j.key::name
		  ORDER by j.key -- ajout 2022 10 04
        )

$SQL$
        , schemadest
         , tabledest
         , json_raw
);


  -- SQL qui permet d'obtenir la liste des values à insérer (val1::type, val2::type)
  sql_string_val_list_to_input :=  format (
     $SQL$
         WITH 
       %s

        SELECT string_agg(
         CASE WHEN j.datatype::name = 'geometry' AND  right(j.VALUE,1)='}' -- geojson force 2154 ( TODO adapt with srid input -> srid dest)
         THEN coalesce(quote_literal(st_setsrid(ST_GeomFromGeoJSON(j.VALUE),2154)::text),'NULL'::text)::text 
         ELSE 
         coalesce (quote_literal(j.VALUE),'NULL')
         END::text

         ||'::' || j.datatype ||
         case when  j.datatype::name = 'geometry' AND  right(j.VALUE,1)='}' 
         THEN '(geometry,2154) '
         ELSE ''
         END

         || '/*' || j.key ||  '*/' ,',')   
         FROM json_lines_and_pgtype j
         WHERE j.key  NOT in (%s)  
      $SQL$
      , sql_json_line_and_pg_type
      , sql_unnest_ignore_col_in_json
      ) ;

   if debugmode then  raise notice 'sql_string_val_list_to_input : % ', sql_string_val_list_to_input;
   END IF;


   -- liste des values à insérer (val1::type, val2::type)
   execute sql_string_val_list_to_input into string_val_list_to_input ;

   if debugmode then  raise notice 'sql_string_val_list_to_input : % ', sql_string_val_list_to_input;
   END IF;


   -- SQL qui permet d'obtenir la liste des values à setter dans un update :   col1=val1::type, col2=val2::type
   sql_string_set_values :=   format (
      $SQL$
      WITH 
       %s
        
        select string_agg(quote_ident(j.key) ||'=' ||
         CASE WHEN j.datatype::name = 'geometry' AND  right(j.VALUE,1)='}' --  geojson force 2154 ( TODO adapt with srid input -> srid dest) 
         THEN coalesce(quote_literal(st_setsrid(ST_GeomFromGeoJSON(j.VALUE),2154)::text),'NULL'::text)::text 
         ELSE 
         coalesce (quote_literal(j.VALUE),'NULL')
         END::text


        ||'::' || j.datatype ||
         case when  j.datatype::name = 'geometry' AND  right(j.VALUE,1)='}' 
         THEN '(geometry,2154) '
         ELSE ''
         END
         ,',') 
        FROM json_lines_and_pgtype j
        where j.key not in (%s )
       $SQL$
         , sql_json_line_and_pg_type
         , sql_unnest_ignore_col_in_json
    --     , sql_unnest_matching_col_list  
     );

   if debugmode then  raise notice 'sql_string_set_values : % ', sql_string_set_values;
   END IF;

   -- liste des values à setter dans un update :   col1=val1::type, col2=val2::type
   execute sql_string_set_values into string_set_values;

   
   skeep_update_set = false;
 -- init
   if string_set_values is  null THEN -- -- matching col list and ignore col cover all the json key list (there is no cols to set)
      if operation_mode ilike 'UPDATE%' THEN -- update
         raise notice 'Pas de colonne à seter dans un update : % ', string_set_values;

         skeep_update_set = true;

       
      END IF;

   END IF;


   if debugmode then  raise notice 'string_set_values : % ', string_set_values;
   END IF;




  sql_string_set_where :=   format (
   $SQL$
      WITH 
      %s
      
      select string_agg('d.' || quote_ident(j.key) ||'=' ||coalesce(quote_literal(j.value), 'NULL')||'::' || j.datatype , ' AND ') 
         from json_lines_and_pgtype j
        where j.key  in (%s ) 
    $SQL$
    , sql_json_line_and_pg_type
    , sql_unnest_matching_col_list  
  );


   if debugmode then  raise notice 'sql_string_set_where : % ', sql_string_set_where;
   END IF;


   execute sql_string_set_where into string_set_where;

   if debugmode then  raise notice 'string_set_where : % ', string_set_where;
   END IF;


  IF skeep_update_set is true THEN
   CASE  
      WHEN (operation_mode ='UPDATE ON CONFLICT' OR operation_mode ='NO UPDATE ON CONFLICT') THEN 
         operation_mode='NO UPDATE ON CONFLICT';

         raise notice 'Reset operation_mode to %', operation_mode;

      WHEN  (operation_mode ='UPDATE ON SAME' OR operation_mode ='NO UPDATE ON SAME')  THEN 
         operation_mode='NO UPDATE ON SAME';

         raise notice 'Reset operation_mode to %', operation_mode;

      ELSE 
      raise notice '%', msg_precise_mode;

   END CASE;
 
  END IF;


  CASE  operation_mode 
  WHEN  ('UPDATE ON CONFLICT' ) THEN 
   
        sql_ins_up := format (
        $SQL$
        WITH upsert as ( 
           INSERT INTO %I.%I AS d (%s) values (%s)
             ON CONFLICT (%s) DO UPDATE SET %s 
           WHERE  %s
           RETURNING 'UPSERT UPDATE POSSIBLE' as mod_mod, d.*
        )
        select upsert FROM upsert
          $SQL$
       , schemadest
       , tabledest

       , string_col_list_to_input
       , string_val_list_to_input
       , string_matching_col_list
       , string_set_values
       , string_set_where
       )
      ;

 
   WHEN  'NO UPDATE ON CONFLICT' THEN 
     sql_ins_up := format (
     $SQL$
     WITH upsert as (
        INSERT INTO %I.%I AS d (%s) values (%s)
          ON CONFLICT (%s) DO NOTHING
          RETURNING 'UPSERT NO UPDATE :' as mod_mod, d.*
      )
      select upsert FROM upsert
       $SQL$
    , schemadest
    , tabledest

    , string_col_list_to_input
    , string_val_list_to_input
    , string_matching_col_list

    )
   ;


   WHEN  'UPDATE ON SAME' THEN 
     sql_ins_up := format (
     $SQL$

     WITH 
     identiq as (
        SELECT * from %I.%I AS d
        WHERE  %s 
      )
     , nb_identiq as (select count(*) as nb from  identiq)
     , up as (
        UPDATE  %I.%I AS d 
        SET %s 
        WHERE  (
         (select nb from nb_identiq)=1 AND (%s)
         )
         RETURNING 'UPDATE'::text as mod_mod, d.*
      )
     , ins as (
      INSERT INTO %I.%I AS d (%s) SELECT * from 
      (SELECT %s 
         WHERE (
         (select nb from nb_identiq)=0
         )) as foo
        
        RETURNING 'INSERT'::text as mod_mod, d.*
     )

     SELECT  up  from up union all SELECT ins from ins
  
  
       $SQL$
       -- select identiq
       , schemadest
       , tabledest
       , string_set_where

       -- update
       , schemadest
       , tabledest
       , string_set_values
       , string_set_where


       -- insert
      , schemadest
      , tabledest
      , string_col_list_to_input
      , string_val_list_to_input
       
     

    )
   ;


    WHEN  'NO UPDATE ON SAME' THEN 
     sql_ins_up := format (
     $SQL$
     WITH identiq as (
        SELECT * from %I.%I AS d
        WHERE  %s 
      )
     , nb_identiq as (select count(*) as nb from  identiq)
     , no_up as (SELECT 'NO UPDATE ON SAME'::text 
        WHERE (
            (select nb from nb_identiq)=1
            )
            )
     , ins as (
      INSERT INTO %I.%I AS d (%s) SELECT * from 
      (SELECT %s
         WHERE (
         (select nb from nb_identiq)=0
         )) as foo
        
        RETURNING 'INSERT :'::text as mod_mod, d.*
     )
     SELECT  no_up  from no_up union all SELECT ins from ins
     
  
  
       $SQL$
       -- select identiq
       , schemadest
       , tabledest
       , string_set_where

       -- insert
      , schemadest
      , tabledest
      , string_col_list_to_input
      , string_val_list_to_input
       
     

    )
   ;

   ELSE 
      sql_ins_up := 'SELECT ''preciser le mode operatoire''';
 -- ne fait rien
      raise notice '%', msg_precise_mode;

   END CASE ;

   
if debugmode then 
raise notice 'sql_ins_up :
%', sql_ins_up;

END IF;

execute sql_ins_up into rtn_txt;


--return sql_ins_up::text;

return rtn_txt;


END;

$BODY$;


;
-- FUNCTION: xx_99_utils.pg_insert_or_update(name, name, name[], name[], json, text, boolean)

-- DROP FUNCTION IF EXISTS xx_99_utils.pg_insert_or_update(name, name, name[], name[], json, text, boolean);


CREATE OR REPLACE FUNCTION xx_99_utils.pg_insert_or_update(
	schemadest name,
	tabledest name,
	ignore_col_list_in_json name[],
	matching_col_list_or_null_for_keeping_all name[],
	json_raw json,
	operation_mode text,
	debugmode boolean DEFAULT false)
    RETURNS text
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$

/*
permet d'insérer ou mettre à jour une ligne ilike insert or update


select  xx_99_utils.pg_insert_or_update(
    schemadest:='xx_99_utils' :: name,
    tabledest:= 'pg_insert_or_update_test_dest' ::name,
    ignore_col_list_in_json := string_to_array('cc3',',')::NAME[]::name[],
    matching_col_list_or_null_for_keeping_all:= string_to_array('cc1,cc2',',')::NAME[],
    json_raw := to_json (s) ::json,
    operation_mode := 'UPDATE ON SAME'::text,

    debugmode := true ::boolean
    ) from xx_99_utils.pg_insert_or_update_test_source s where s.cc1 = 1
    ;


    SELECT 
      '01_source' as table_type
       , * 
      FROM  xx_99_utils.pg_insert_or_update_test_source 
    UNION ALL 
    SELECT
    '02_dest' 
    , * 
    FROM  xx_99_utils.pg_insert_or_update_test_dest 
    ORDER BY  table_type,cc1

-- préparation des tables de test
drop table if exists xx_99_utils.pg_insert_or_update_test_source;

create table xx_99_utils.pg_insert_or_update_test_source as (
SELECT 1::int as cc1, 't1 up'::text as cc2 , true::boolean cc3
union all 

SELECT 1::int as cc1, 't1bis up'::text as cc2 ,  false::boolean cc3
union all 

SELECT 2::int as cc1, 'toto2 tutu up'::text as cc2 ,  false::boolean cc3

union all 

SELECT 3::int as cc1, 'toto3 tutu up'::text as cc2 ,  false::boolean cc3

union all 

SELECT 4::int as cc1, 'toto4 tutu up'::text as cc2 ,  false::boolean cc3
);



drop table if exists xx_99_utils.pg_insert_or_update_test_dest;

create table xx_99_utils.pg_insert_or_update_test_dest  (
cc1 serial primary key , cc2 text, cc3 boolean)
;

INSERT into xx_99_utils.pg_insert_or_update_test_dest (cc1, cc2, cc3) 

SELECT 1::int as cc1, 'toto titi init'::text as cc2 ,  true
union all 

SELECT 2::int as cc1, 'toto2 tutu init'::text as cc2 , true


note de version :
V03 : DEBUG geometry geojson in json raw ( TODO adapter srid)
      DEBUD array type json -> array pg 
V02 : ADD capability to chose insert mode :  between use :
   UPDATE ON CONFLICT (need index on columns sometimes to heavy for loading in cache (exemple hub eau 'qualuié cours d'eau' ) or 
   UPDATE ON SAME (no need index ... slower)
   NO UPDATE ON CONFLICT
   NO UPDATE ON SAME
V01 : first implementation

*/
DECLARE
 


    rtn_txt TEXT;

    sql_ins_up text;

   sql_matching_col_list TEXT;
 
   matching_col_list NAME[];

   sql_unnest_ignore_col_in_json text;

   sql_unnest_matching_col_list TEXT;

   string_matching_col_list TEXT;

   sql_string_col_list_to_input text;

   string_col_list_to_input TEXT;

   sql_string_val_list_to_input TEXT;

   string_val_list_to_input TEXT;

   sql_string_set_values TEXT;

   string_set_values TEXT;


   sql_string_set_where TEXT;

   string_set_where TEXT;


   msg text;

   msg_precise_mode TEXT;

   skeep_update_set boolean;


   toto TEXT;

   sql_json_line_and_pg_type text;

   
   
    
BEGIN

  msg_precise_mode = 'preciser le mode operatoire : 
      UPDATE ON CONFLICT 
      NO UPDATE ON CONFLICT
      UPDATE ON SAME
      NO UPDATE ON SAME';

  -- s'il n'y a pas de colonnes à ignorer : select null , sinon liste les colonnes:  
  if ignore_col_list_in_json::name[] is not null  THEN 
      sql_unnest_ignore_col_in_json := 'SELECT unnest( ' || quote_literal(ignore_col_list_in_json::TEXT) || '::NAME[] )::TEXT as k';

  else 
      sql_unnest_ignore_col_in_json :='SELECT ''''::text as k';

  END IF;




  -- Liste des colonnes à utiliser pour la comparaison (retire les colonnes à ignorer s'il y en a), utilise toutes les colonnes si pas de restriction ( matching_col_list = NULL:name[])
  IF matching_col_list_or_null_for_keeping_all is null THEN
      
      sql_matching_col_list := format (
         $SQL$
         WITH jk AS (SELECT json_object_keys(%L::json) as k)
         SELECT string_to_array(string_agg(k,','),',')::name[]
         FROM jk 
         $SQL$
         , json_raw
         );

      execute sql_matching_col_list into matching_col_list;

      --matching_col_list := string_to_array(string_agg(json_object_keys(json_raw::json),','),',')::name[];
 
  ELSE 
      matching_col_list := matching_col_list_or_null_for_keeping_all;

  END IF;


  -- remove ignore cols
  matching_col_list :=   xx_99_utils.f_array_subtraction(
    _array_a:= matching_col_list::anyarray,
    _array_b:= ignore_col_list_in_json::anyarray,
    _strict_number_of_uniq_values:=false)::name[] ;

    

  -- sql de select :  table qui liste des colonnes dont on recherche la correspondance des valeurs (<>quasi pk) 
  sql_unnest_matching_col_list :=  'SELECT unnest( ' || quote_literal(matching_col_list::text) || '::NAME[])::TEXT ';


  -- liste des cols qui définissent en text (a, b, c...) 
  string_matching_col_list :=  array_to_string(matching_col_list::name[], ',');



  -- SQL qui permet d'obtenir la liste les colonnes à insérer
  sql_string_col_list_to_input :=  format (
     $SQL$
        WITH 
         json_lines as (select * from json_each_text(%L::json))
        SELECT string_agg(quote_ident(key),',')   
            FROM json_lines  
            WHERE key  NOT in (%s)  
	  		ORDER BY key -- ajout 2022 10 04

      $SQL$
      , json_raw
      , sql_unnest_ignore_col_in_json
      ) ;


   
  if debugmode then  raise notice 'sql_string_col_list_to_input : % ', sql_string_col_list_to_input;
   END IF;

  execute sql_string_col_list_to_input into string_col_list_to_input ;

  if debugmode then  raise notice 'string_col_list_to_input : % ', string_col_list_to_input;
   END IF;

  IF sql_string_col_list_to_input IS NULL THEN 
      msg := 'problème string_col_list_to_input : est null ';

      raise notice '%', msg;

      RETURN 'SELECT ' ||quote_ident (msg) ;

  END IF;


sql_json_line_and_pg_type := format (
$SQL$
   dest_table_cols_pg_types as (
         SELECT * from xx_99_utils.f_table_columns_list ('%s', '%s') 
         )
      , json_lines as (
         select * from json_each_text(%L::json)
         )
      , json_lines_and_pgtype as (
        SELECT j.key
        ,  case when t.datatype::text ilike'character varying[]' 
           THEN  replace(replace(j.value::text , '[','{'),']','}')
            ELSE j.value
            END
        ,t.datatype 
        FROM json_lines j 
        LEFT JOIN dest_table_cols_pg_types t on (t.col::name= j.key::name)
		ORDER by j.key -- ajout 2022 10 04
        )

$SQL$
        , schemadest
         , tabledest
         , json_raw
);


  -- SQL qui permet d'obtenir la liste des values à insérer (val1::type, val2::type)
  sql_string_val_list_to_input :=  format (
     $SQL$
         WITH 
       %s

        SELECT string_agg(
         CASE WHEN j.datatype::name = 'geometry' AND  right(j.VALUE,1)='}' -- geojson force 2154 ( TODO adapt with srid input -> srid dest)
         THEN coalesce(quote_literal(st_setsrid(ST_GeomFromGeoJSON(j.VALUE),2154)::text),'NULL'::text)::text 
         ELSE 
         coalesce (quote_literal(j.VALUE),'NULL')
         END::text

         ||'::' || j.datatype ||
         case when  j.datatype::name = 'geometry' AND  right(j.VALUE,1)='}' 
         THEN '(geometry,2154) '
         ELSE ''
         END

         || '/*' || j.key ||  '*/' ,',')   
         FROM json_lines_and_pgtype j
         WHERE j.key  NOT in (%s)  
      $SQL$
      , sql_json_line_and_pg_type
      , sql_unnest_ignore_col_in_json
      ) ;

   if debugmode then  raise notice 'sql_string_val_list_to_input : % ', sql_string_val_list_to_input;
   END IF;


   -- liste des values à insérer (val1::type, val2::type)
   execute sql_string_val_list_to_input into string_val_list_to_input ;

   if debugmode then  raise notice 'sql_string_val_list_to_input : % ', sql_string_val_list_to_input;
   END IF;


   -- SQL qui permet d'obtenir la liste des values à setter dans un update :   col1=val1::type, col2=val2::type
   sql_string_set_values :=   format (
      $SQL$
      WITH 
       %s
        
        select string_agg(quote_ident(j.key) ||'=' ||
         CASE WHEN j.datatype::name = 'geometry' AND  right(j.VALUE,1)='}' --  geojson force 2154 ( TODO adapt with srid input -> srid dest) 
         THEN coalesce(quote_literal(st_setsrid(ST_GeomFromGeoJSON(j.VALUE),2154)::text),'NULL'::text)::text 
         ELSE 
         coalesce (quote_literal(j.VALUE),'NULL')
         END::text


        ||'::' || j.datatype ||
         case when  j.datatype::name = 'geometry' AND  right(j.VALUE,1)='}' 
         THEN '(geometry,2154) '
         ELSE ''
         END
         ,',') 
        FROM json_lines_and_pgtype j
        where j.key not in (%s )
       $SQL$
         , sql_json_line_and_pg_type
         , sql_unnest_ignore_col_in_json
    --     , sql_unnest_matching_col_list  
     );

   if debugmode then  raise notice 'sql_string_set_values : % ', sql_string_set_values;
   END IF;

   -- liste des values à setter dans un update :   col1=val1::type, col2=val2::type
   execute sql_string_set_values into string_set_values;

   
   skeep_update_set = false;
 -- init
   if string_set_values is  null THEN -- -- matching col list and ignore col cover all the json key list (there is no cols to set)
      if operation_mode ilike 'UPDATE%' THEN -- update
         raise notice 'Pas de colonne à seter dans un update : % ', string_set_values;

         skeep_update_set = true;

       
      END IF;

   END IF;


   if debugmode then  raise notice 'string_set_values : % ', string_set_values;
   END IF;




  sql_string_set_where :=   format (
   $SQL$
      WITH 
      %s
      
      select string_agg('d.' || quote_ident(j.key) ||'=' ||coalesce(quote_literal(j.value), 'NULL')||'::' || j.datatype , ' AND ') 
         from json_lines_and_pgtype j
        where j.key  in (%s ) 
    $SQL$
    , sql_json_line_and_pg_type
    , sql_unnest_matching_col_list  
  );


   if debugmode then  raise notice 'sql_string_set_where : % ', sql_string_set_where;
   END IF;


   execute sql_string_set_where into string_set_where;

   if debugmode then  raise notice 'string_set_where : % ', string_set_where;
   END IF;


  IF skeep_update_set is true THEN
   CASE  
      WHEN (operation_mode ='UPDATE ON CONFLICT' OR operation_mode ='NO UPDATE ON CONFLICT') THEN 
         operation_mode='NO UPDATE ON CONFLICT';

         raise notice 'Reset operation_mode to %', operation_mode;

      WHEN  (operation_mode ='UPDATE ON SAME' OR operation_mode ='NO UPDATE ON SAME')  THEN 
         operation_mode='NO UPDATE ON SAME';

         raise notice 'Reset operation_mode to %', operation_mode;

      ELSE 
      raise notice '%', msg_precise_mode;

   END CASE;
 
  END IF;


  CASE  operation_mode 
  WHEN  ('UPDATE ON CONFLICT' ) THEN 
   
        sql_ins_up := format (
        $SQL$
        WITH upsert as ( 
           INSERT INTO %I.%I AS d (%s) values (%s)
             ON CONFLICT (%s) DO UPDATE SET %s 
           WHERE  %s
           RETURNING 'UPSERT UPDATE POSSIBLE' as mod_mod, d.*
        )
        select upsert FROM upsert
          $SQL$
       , schemadest
       , tabledest

       , string_col_list_to_input
       , string_val_list_to_input
       , string_matching_col_list
       , string_set_values
       , string_set_where
       )
      ;

 
   WHEN  'NO UPDATE ON CONFLICT' THEN 
     sql_ins_up := format (
     $SQL$
     WITH upsert as (
        INSERT INTO %I.%I AS d (%s) values (%s)
          ON CONFLICT (%s) DO NOTHING
          RETURNING 'UPSERT NO UPDATE :' as mod_mod, d.*
      )
      select upsert FROM upsert
       $SQL$
    , schemadest
    , tabledest

    , string_col_list_to_input
    , string_val_list_to_input
    , string_matching_col_list

    )
   ;


   WHEN  'UPDATE ON SAME' THEN 
     sql_ins_up := format (
     $SQL$

     WITH 
     identiq as (
        SELECT * from %I.%I AS d
        WHERE  %s 
      )
     , nb_identiq as (select count(*) as nb from  identiq)
     , up as (
        UPDATE  %I.%I AS d 
        SET %s 
        WHERE  (
         (select nb from nb_identiq)=1 AND (%s)
         )
         RETURNING 'UPDATE'::text as mod_mod, d.*
      )
     , ins as (
      INSERT INTO %I.%I AS d (%s) SELECT * from 
      (SELECT %s 
         WHERE (
         (select nb from nb_identiq)=0
         )) as foo
        
        RETURNING 'INSERT'::text as mod_mod, d.*
     )

     SELECT  up  from up union all SELECT ins from ins
  
  
       $SQL$
       -- select identiq
       , schemadest
       , tabledest
       , string_set_where

       -- update
       , schemadest
       , tabledest
       , string_set_values
       , string_set_where


       -- insert
      , schemadest
      , tabledest
      , string_col_list_to_input
      , string_val_list_to_input
       
     

    )
   ;


    WHEN  'NO UPDATE ON SAME' THEN 
     sql_ins_up := format (
     $SQL$
     WITH identiq as (
        SELECT * from %I.%I AS d
        WHERE  %s 
      )
     , nb_identiq as (select count(*) as nb from  identiq)
     , no_up as (SELECT 'NO UPDATE ON SAME'::text 
        WHERE (
            (select nb from nb_identiq)=1
            )
            )
     , ins as (
      INSERT INTO %I.%I AS d (%s) SELECT * from 
      (SELECT %s
         WHERE (
         (select nb from nb_identiq)=0
         )) as foo
        
        RETURNING 'INSERT :'::text as mod_mod, d.*
     )
     SELECT  no_up  from no_up union all SELECT ins from ins
     
  
  
       $SQL$
       -- select identiq
       , schemadest
       , tabledest
       , string_set_where

       -- insert
      , schemadest
      , tabledest
      , string_col_list_to_input
      , string_val_list_to_input
       
     

    )
   ;

   ELSE 
      sql_ins_up := 'SELECT ''preciser le mode operatoire''';
 -- ne fait rien
      raise notice '%', msg_precise_mode;

   END CASE ;

   
if debugmode then 
raise notice 'sql_ins_up :
%', sql_ins_up;

END IF;

execute sql_ins_up into rtn_txt;


--return sql_ins_up::text;

return rtn_txt;


END;

$BODY$;


;
-- FUNCTION: xx_99_utils.pg_insert_or_update(name, name, name[], name[], json, text, boolean)

-- DROP FUNCTION IF EXISTS xx_99_utils.pg_insert_or_update(name, name, name[], name[], json, text, boolean);


CREATE OR REPLACE FUNCTION xx_99_utils.pg_insert_or_update(
	schemadest name,
	tabledest name,
	ignore_col_list_in_json name[],
	matching_col_list_or_null_for_keeping_all name[],
	json_raw json,
	operation_mode text,
	debugmode boolean DEFAULT false)
    RETURNS text
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$

/*
permet d'insérer ou mettre à jour une ligne ilike insert or update


select  xx_99_utils.pg_insert_or_update(
    schemadest:='xx_99_utils' :: name,
    tabledest:= 'pg_insert_or_update_test_dest' ::name,
    ignore_col_list_in_json := string_to_array('cc3',',')::NAME[]::name[],
    matching_col_list_or_null_for_keeping_all:= string_to_array('cc1,cc2',',')::NAME[],
    json_raw := to_json (s) ::json,
    operation_mode := 'UPDATE ON SAME'::text,

    debugmode := true ::boolean
    ) from xx_99_utils.pg_insert_or_update_test_source s where s.cc1 = 1
    ;


    SELECT 
      '01_source' as table_type
       , * 
      FROM  xx_99_utils.pg_insert_or_update_test_source 
    UNION ALL 
    SELECT
    '02_dest' 
    , * 
    FROM  xx_99_utils.pg_insert_or_update_test_dest 
    ORDER BY  table_type,cc1

-- préparation des tables de test
drop table if exists xx_99_utils.pg_insert_or_update_test_source;

create table xx_99_utils.pg_insert_or_update_test_source as (
SELECT 1::int as cc1, 't1 up'::text as cc2 , true::boolean cc3
union all 

SELECT 1::int as cc1, 't1bis up'::text as cc2 ,  false::boolean cc3
union all 

SELECT 2::int as cc1, 'toto2 tutu up'::text as cc2 ,  false::boolean cc3

union all 

SELECT 3::int as cc1, 'toto3 tutu up'::text as cc2 ,  false::boolean cc3

union all 

SELECT 4::int as cc1, 'toto4 tutu up'::text as cc2 ,  false::boolean cc3
);



drop table if exists xx_99_utils.pg_insert_or_update_test_dest;

create table xx_99_utils.pg_insert_or_update_test_dest  (
cc1 serial primary key , cc2 text, cc3 boolean)
;

INSERT into xx_99_utils.pg_insert_or_update_test_dest (cc1, cc2, cc3) 

SELECT 1::int as cc1, 'toto titi init'::text as cc2 ,  true
union all 

SELECT 2::int as cc1, 'toto2 tutu init'::text as cc2 , true


note de version :
V03 : DEBUG geometry geojson in json raw ( TODO adapter srid)
      DEBUD array type json -> array pg 
V02 : ADD capability to chose insert mode :  between use :
   UPDATE ON CONFLICT (need index on columns sometimes to heavy for loading in cache (exemple hub eau 'qualuié cours d'eau' ) or 
   UPDATE ON SAME (no need index ... slower)
   NO UPDATE ON CONFLICT
   NO UPDATE ON SAME
V01 : first implementation

*/
DECLARE
 


    rtn_txt TEXT;

    sql_ins_up text;

   sql_matching_col_list TEXT;
 
   matching_col_list NAME[];

   sql_unnest_ignore_col_in_json text;

   sql_unnest_matching_col_list TEXT;

   string_matching_col_list TEXT;

   sql_string_col_list_to_input text;

   string_col_list_to_input TEXT;

   sql_string_val_list_to_input TEXT;

   string_val_list_to_input TEXT;

   sql_string_set_values TEXT;

   string_set_values TEXT;


   sql_string_set_where TEXT;

   string_set_where TEXT;


   msg text;

   msg_precise_mode TEXT;

   skeep_update_set boolean;


   toto TEXT;

   sql_json_line_and_pg_type text;

   
   
    
BEGIN

  msg_precise_mode = 'preciser le mode operatoire : 
      UPDATE ON CONFLICT 
      NO UPDATE ON CONFLICT
      UPDATE ON SAME
      NO UPDATE ON SAME';

  -- s'il n'y a pas de colonnes à ignorer : select null , sinon liste les colonnes:  
  if ignore_col_list_in_json::name[] is not null  THEN 
      sql_unnest_ignore_col_in_json := 'SELECT unnest( ' || quote_literal(ignore_col_list_in_json::TEXT) || '::NAME[] )::TEXT as k';

  else 
      sql_unnest_ignore_col_in_json :='SELECT ''''::text as k';

  END IF;




  -- Liste des colonnes à utiliser pour la comparaison (retire les colonnes à ignorer s'il y en a), utilise toutes les colonnes si pas de restriction ( matching_col_list = NULL:name[])
  IF matching_col_list_or_null_for_keeping_all is null THEN
      
      sql_matching_col_list := format (
         $SQL$
         WITH jk AS (SELECT json_object_keys(%L::json) as k)
         SELECT string_to_array(string_agg(k,','),',')::name[]
         FROM jk 
         $SQL$
         , json_raw
         );

      execute sql_matching_col_list into matching_col_list;

      --matching_col_list := string_to_array(string_agg(json_object_keys(json_raw::json),','),',')::name[];
 
  ELSE 
      matching_col_list := matching_col_list_or_null_for_keeping_all;

  END IF;


  -- remove ignore cols
  matching_col_list :=   xx_99_utils.f_array_subtraction(
    _array_a:= matching_col_list::anyarray,
    _array_b:= ignore_col_list_in_json::anyarray,
    _strict_number_of_uniq_values:=false)::name[] ;

    

  -- sql de select :  table qui liste des colonnes dont on recherche la correspondance des valeurs (<>quasi pk) 
  sql_unnest_matching_col_list :=  'SELECT unnest( ' || quote_literal(matching_col_list::text) || '::NAME[])::TEXT ';


  -- liste des cols qui définissent en text (a, b, c...) 
  string_matching_col_list :=  array_to_string(matching_col_list::name[], ',');



  -- SQL qui permet d'obtenir la liste les colonnes à insérer
  sql_string_col_list_to_input :=  format (
     $SQL$
        WITH 
         json_lines as (select * from json_each_text(%L::json))
	     ,json_lines_ordering as (select * from json_lines
				ORDER BY key -- ajout 2022 10 04
								 )
        SELECT string_agg(quote_ident(key),',')   
            FROM json_lines_ordering  
            WHERE key  NOT in (%s)  
	  		

      $SQL$
      , json_raw
      , sql_unnest_ignore_col_in_json
      ) ;


   
  if debugmode then  raise notice 'sql_string_col_list_to_input : % ', sql_string_col_list_to_input;
   END IF;

  execute sql_string_col_list_to_input into string_col_list_to_input ;

  if debugmode then  raise notice 'string_col_list_to_input : % ', string_col_list_to_input;
   END IF;

  IF sql_string_col_list_to_input IS NULL THEN 
      msg := 'problème string_col_list_to_input : est null ';

      raise notice '%', msg;

      RETURN 'SELECT ' ||quote_ident (msg) ;

  END IF;


sql_json_line_and_pg_type := format (
$SQL$
   dest_table_cols_pg_types as (
         SELECT * from xx_99_utils.f_table_columns_list ('%s', '%s') 
         )
      , json_lines as (
         select * from json_each_text(%L::json)
         )
      , json_lines_and_pgtype as (
        SELECT j.key
        ,  case when t.datatype::text ilike'character varying[]' 
           THEN  replace(replace(j.value::text , '[','{'),']','}')
            ELSE j.value
            END
        ,t.datatype 
        FROM json_lines j 
        LEFT JOIN dest_table_cols_pg_types t on (t.col::name= j.key::name)
		ORDER by j.key -- ajout 2022 10 04
        )

$SQL$
        , schemadest
         , tabledest
         , json_raw
);


  -- SQL qui permet d'obtenir la liste des values à insérer (val1::type, val2::type)
  sql_string_val_list_to_input :=  format (
     $SQL$
         WITH 
       %s

        SELECT string_agg(
         CASE WHEN j.datatype::name = 'geometry' AND  right(j.VALUE,1)='}' -- geojson force 2154 ( TODO adapt with srid input -> srid dest)
         THEN coalesce(quote_literal(st_setsrid(ST_GeomFromGeoJSON(j.VALUE),2154)::text),'NULL'::text)::text 
         ELSE 
         coalesce (quote_literal(j.VALUE),'NULL')
         END::text

         ||'::' || j.datatype ||
         case when  j.datatype::name = 'geometry' AND  right(j.VALUE,1)='}' 
         THEN '(geometry,2154) '
         ELSE ''
         END

         || '/*' || j.key ||  '*/' ,',')   
         FROM json_lines_and_pgtype j
         WHERE j.key  NOT in (%s)  
      $SQL$
      , sql_json_line_and_pg_type
      , sql_unnest_ignore_col_in_json
      ) ;

   if debugmode then  raise notice 'sql_string_val_list_to_input : % ', sql_string_val_list_to_input;
   END IF;


   -- liste des values à insérer (val1::type, val2::type)
   execute sql_string_val_list_to_input into string_val_list_to_input ;

   if debugmode then  raise notice 'sql_string_val_list_to_input : % ', sql_string_val_list_to_input;
   END IF;


   -- SQL qui permet d'obtenir la liste des values à setter dans un update :   col1=val1::type, col2=val2::type
   sql_string_set_values :=   format (
      $SQL$
      WITH 
       %s
        
        select string_agg(quote_ident(j.key) ||'=' ||
         CASE WHEN j.datatype::name = 'geometry' AND  right(j.VALUE,1)='}' --  geojson force 2154 ( TODO adapt with srid input -> srid dest) 
         THEN coalesce(quote_literal(st_setsrid(ST_GeomFromGeoJSON(j.VALUE),2154)::text),'NULL'::text)::text 
         ELSE 
         coalesce (quote_literal(j.VALUE),'NULL')
         END::text


        ||'::' || j.datatype ||
         case when  j.datatype::name = 'geometry' AND  right(j.VALUE,1)='}' 
         THEN '(geometry,2154) '
         ELSE ''
         END
         ,',') 
        FROM json_lines_and_pgtype j
        where j.key not in (%s )
       $SQL$
         , sql_json_line_and_pg_type
         , sql_unnest_ignore_col_in_json
    --     , sql_unnest_matching_col_list  
     );

   if debugmode then  raise notice 'sql_string_set_values : % ', sql_string_set_values;
   END IF;

   -- liste des values à setter dans un update :   col1=val1::type, col2=val2::type
   execute sql_string_set_values into string_set_values;

   
   skeep_update_set = false;
 -- init
   if string_set_values is  null THEN -- -- matching col list and ignore col cover all the json key list (there is no cols to set)
      if operation_mode ilike 'UPDATE%' THEN -- update
         raise notice 'Pas de colonne à seter dans un update : % ', string_set_values;

         skeep_update_set = true;

       
      END IF;

   END IF;


   if debugmode then  raise notice 'string_set_values : % ', string_set_values;
   END IF;




  sql_string_set_where :=   format (
   $SQL$
      WITH 
      %s
      
      select string_agg('d.' || quote_ident(j.key) ||'=' ||coalesce(quote_literal(j.value), 'NULL')||'::' || j.datatype , ' AND ') 
         from json_lines_and_pgtype j
        where j.key  in (%s ) 
    $SQL$
    , sql_json_line_and_pg_type
    , sql_unnest_matching_col_list  
  );


   if debugmode then  raise notice 'sql_string_set_where : % ', sql_string_set_where;
   END IF;


   execute sql_string_set_where into string_set_where;

   if debugmode then  raise notice 'string_set_where : % ', string_set_where;
   END IF;


  IF skeep_update_set is true THEN
   CASE  
      WHEN (operation_mode ='UPDATE ON CONFLICT' OR operation_mode ='NO UPDATE ON CONFLICT') THEN 
         operation_mode='NO UPDATE ON CONFLICT';

         raise notice 'Reset operation_mode to %', operation_mode;

      WHEN  (operation_mode ='UPDATE ON SAME' OR operation_mode ='NO UPDATE ON SAME')  THEN 
         operation_mode='NO UPDATE ON SAME';

         raise notice 'Reset operation_mode to %', operation_mode;

      ELSE 
      raise notice '%', msg_precise_mode;

   END CASE;
 
  END IF;


  CASE  operation_mode 
  WHEN  ('UPDATE ON CONFLICT' ) THEN 
   
        sql_ins_up := format (
        $SQL$
        WITH upsert as ( 
           INSERT INTO %I.%I AS d (%s) values (%s)
             ON CONFLICT (%s) DO UPDATE SET %s 
           WHERE  %s
           RETURNING 'UPSERT UPDATE POSSIBLE' as mod_mod, d.*
        )
        select upsert FROM upsert
          $SQL$
       , schemadest
       , tabledest

       , string_col_list_to_input
       , string_val_list_to_input
       , string_matching_col_list
       , string_set_values
       , string_set_where
       )
      ;

 
   WHEN  'NO UPDATE ON CONFLICT' THEN 
     sql_ins_up := format (
     $SQL$
     WITH upsert as (
        INSERT INTO %I.%I AS d (%s) values (%s)
          ON CONFLICT (%s) DO NOTHING
          RETURNING 'UPSERT NO UPDATE :' as mod_mod, d.*
      )
      select upsert FROM upsert
       $SQL$
    , schemadest
    , tabledest

    , string_col_list_to_input
    , string_val_list_to_input
    , string_matching_col_list

    )
   ;


   WHEN  'UPDATE ON SAME' THEN 
     sql_ins_up := format (
     $SQL$

     WITH 
     identiq as (
        SELECT * from %I.%I AS d
        WHERE  %s 
      )
     , nb_identiq as (select count(*) as nb from  identiq)
     , up as (
        UPDATE  %I.%I AS d 
        SET %s 
        WHERE  (
         (select nb from nb_identiq)=1 AND (%s)
         )
         RETURNING 'UPDATE'::text as mod_mod, d.*
      )
     , ins as (
      INSERT INTO %I.%I AS d (%s) SELECT * from 
      (SELECT %s 
         WHERE (
         (select nb from nb_identiq)=0
         )) as foo
        
        RETURNING 'INSERT'::text as mod_mod, d.*
     )

     SELECT  up  from up union all SELECT ins from ins
  
  
       $SQL$
       -- select identiq
       , schemadest
       , tabledest
       , string_set_where

       -- update
       , schemadest
       , tabledest
       , string_set_values
       , string_set_where


       -- insert
      , schemadest
      , tabledest
      , string_col_list_to_input
      , string_val_list_to_input
       
     

    )
   ;


    WHEN  'NO UPDATE ON SAME' THEN 
     sql_ins_up := format (
     $SQL$
     WITH identiq as (
        SELECT * from %I.%I AS d
        WHERE  %s 
      )
     , nb_identiq as (select count(*) as nb from  identiq)
     , no_up as (SELECT 'NO UPDATE ON SAME'::text 
        WHERE (
            (select nb from nb_identiq)=1
            )
            )
     , ins as (
      INSERT INTO %I.%I AS d (%s) SELECT * from 
      (SELECT %s
         WHERE (
         (select nb from nb_identiq)=0
         )) as foo
        
        RETURNING 'INSERT :'::text as mod_mod, d.*
     )
     SELECT  no_up  from no_up union all SELECT ins from ins
     
  
  
       $SQL$
       -- select identiq
       , schemadest
       , tabledest
       , string_set_where

       -- insert
      , schemadest
      , tabledest
      , string_col_list_to_input
      , string_val_list_to_input
       
     

    )
   ;

   ELSE 
      sql_ins_up := 'SELECT ''preciser le mode operatoire''';
 -- ne fait rien
      raise notice '%', msg_precise_mode;

   END CASE ;

   
if debugmode then 
raise notice 'sql_ins_up :
%', sql_ins_up;

END IF;

execute sql_ins_up into rtn_txt;


--return sql_ins_up::text;

return rtn_txt;


END;

$BODY$;


;
-- FUNCTION: xx_99_utils.pg_insert_or_update(name, name, name[], name[], json, text, boolean)

-- DROP FUNCTION IF EXISTS xx_99_utils.pg_insert_or_update(name, name, name[], name[], json, text, boolean);


CREATE OR REPLACE FUNCTION xx_99_utils.pg_insert_or_update(
	schemadest name,
	tabledest name,
	ignore_col_list_in_json name[],
	matching_col_list_or_null_for_keeping_all name[],
	json_raw json,
	operation_mode text,
	debugmode boolean DEFAULT false)
    RETURNS text
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$

/*
permet d'insérer ou mettre à jour une ligne ilike insert or update

select  xx_99_utils.pg_insert_or_update(
    schemadest:='xx_99_utils' :: name,
    tabledest:= 'pg_insert_or_update_test_dest' ::name,
    ignore_col_list_in_json := string_to_array('cc3',',')::NAME[]::name[],
    matching_col_list_or_null_for_keeping_all:= string_to_array('cc1,cc2',',')::NAME[],
    json_raw := to_json (s) ::json,
    operation_mode := 'UPDATE ON SAME'::text,

    debugmode := true ::boolean
    ) from xx_99_utils.pg_insert_or_update_test_source s where s.cc1 = 1
    ;


    SELECT 
      '01_source' as table_type
       , * 
      FROM  xx_99_utils.pg_insert_or_update_test_source 
    UNION ALL 
    SELECT
    '02_dest' 
    , * 
    FROM  xx_99_utils.pg_insert_or_update_test_dest 
    ORDER BY  table_type,cc1

-- préparation des tables de test
drop table if exists xx_99_utils.pg_insert_or_update_test_source;

create table xx_99_utils.pg_insert_or_update_test_source as (
SELECT 1::int as cc1, 't1 up'::text as cc2 , true::boolean cc3
union all 

SELECT 1::int as cc1, 't1bis up'::text as cc2 ,  false::boolean cc3
union all 

SELECT 2::int as cc1, 'toto2 tutu up'::text as cc2 ,  false::boolean cc3

union all 

SELECT 3::int as cc1, 'toto3 tutu up'::text as cc2 ,  false::boolean cc3

union all 

SELECT 4::int as cc1, 'toto4 tutu up'::text as cc2 ,  false::boolean cc3
);


drop table if exists xx_99_utils.pg_insert_or_update_test_dest;

create table xx_99_utils.pg_insert_or_update_test_dest  (
cc1 serial primary key , cc2 text, cc3 boolean)
;

INSERT into xx_99_utils.pg_insert_or_update_test_dest (cc1, cc2, cc3) 

SELECT 1::int as cc1, 'toto titi init'::text as cc2 ,  true
union all 

SELECT 2::int as cc1, 'toto2 tutu init'::text as cc2 , true

note de version :
V03 : DEBUG geometry geojson in json raw ( TODO adapter srid)
      DEBUD array type json -> array pg 
V02 : ADD capability to chose insert mode :  between use :
   UPDATE ON CONFLICT (need index on columns sometimes to heavy for loading in cache (exemple hub eau 'qualuié cours d'eau' ) or 
   UPDATE ON SAME (no need index ... slower)
   NO UPDATE ON CONFLICT
   NO UPDATE ON SAME
V01 : first implementation

*/
DECLARE
 

    rtn_txt TEXT;

    sql_ins_up text;

   sql_matching_col_list TEXT;
 
   matching_col_list NAME[];

   sql_unnest_ignore_col_in_json text;

   sql_unnest_matching_col_list TEXT;

   string_matching_col_list TEXT;

   sql_string_col_list_to_input text;

   string_col_list_to_input TEXT;

   sql_string_val_list_to_input TEXT;

   string_val_list_to_input TEXT;

   sql_string_set_values TEXT;

   string_set_values TEXT;


   sql_string_set_where TEXT;

   string_set_where TEXT;


   msg text;

   msg_precise_mode TEXT;

   skeep_update_set boolean;


   toto TEXT;

   sql_json_line_and_pg_type text;

   
   
    
BEGIN

  msg_precise_mode = 'preciser le mode operatoire : 
      UPDATE ON CONFLICT 
      NO UPDATE ON CONFLICT
      UPDATE ON SAME
      NO UPDATE ON SAME';

  -- s'il n'y a pas de colonnes à ignorer : select null , sinon liste les colonnes:  
  if ignore_col_list_in_json::name[] is not null  THEN 
      sql_unnest_ignore_col_in_json := 'SELECT unnest( ' || quote_literal(ignore_col_list_in_json::TEXT) || '::NAME[] )::TEXT as k';

  else 
      sql_unnest_ignore_col_in_json :='SELECT ''''::text as k';

  END IF;

  if debugmode then  raise notice 'sql_unnest_ignore_col_in_json : % ', sql_unnest_ignore_col_in_json;
   END IF;


  -- Liste des colonnes à utiliser pour la comparaison (retire les colonnes à ignorer s'il y en a), utilise toutes les colonnes si pas de restriction ( matching_col_list = NULL:name[])
  IF matching_col_list_or_null_for_keeping_all is null THEN
      
      sql_matching_col_list := format (
         $SQL$
         WITH jk AS (SELECT json_object_keys(%L::json) as k)
         SELECT string_to_array(string_agg(k,','),',')::name[]
         FROM jk 
         $SQL$
         , json_raw
         );

      execute sql_matching_col_list into matching_col_list;

      --matching_col_list := string_to_array(string_agg(json_object_keys(json_raw::json),','),',')::name[];
 
  ELSE 
      matching_col_list := matching_col_list_or_null_for_keeping_all;

  END IF;


  -- remove ignore cols
  matching_col_list :=   xx_99_utils.f_array_subtraction(
    _array_a:= matching_col_list::anyarray,
    _array_b:= ignore_col_list_in_json::anyarray,
    _strict_number_of_uniq_values:=false)::name[] ;

    

  -- sql de select :  table qui liste des colonnes dont on recherche la correspondance des valeurs (<>quasi pk) 
  sql_unnest_matching_col_list :=  'SELECT unnest( ' || quote_literal(matching_col_list::text) || '::NAME[])::TEXT ';


  -- liste des cols qui définissent en text (a, b, c...) 
  string_matching_col_list :=  array_to_string(matching_col_list::name[], ',');


  -- SQL qui permet d'obtenir la liste les colonnes à insérer
  sql_string_col_list_to_input :=  format (
     $SQL$
        WITH 
         json_lines as (select * from json_each_text(%L::json))
	     ,json_lines_ordering as (select * from json_lines
				ORDER BY key -- ajout 2022 10 04
								 )
        SELECT string_agg(quote_ident(key),',')   
            FROM json_lines_ordering  
            WHERE key  NOT in (%s)  
	  		

      $SQL$
      , json_raw
      , sql_unnest_ignore_col_in_json
      ) ;


   
  if debugmode then  raise notice 'sql_string_col_list_to_input : % ', sql_string_col_list_to_input;
   END IF;

  execute sql_string_col_list_to_input into string_col_list_to_input ;

  if debugmode then  raise notice 'string_col_list_to_input : % ', string_col_list_to_input;
   END IF;

  IF sql_string_col_list_to_input IS NULL THEN 
      msg := 'problème string_col_list_to_input : est null ';

      raise notice '%', msg;

      RETURN 'SELECT ' ||quote_ident (msg) ;

  END IF;


sql_json_line_and_pg_type := format (
$SQL$
   dest_table_cols_pg_types as (
         SELECT * from xx_99_utils.f_table_columns_list ('%s', '%s') 
         )
      , json_lines as (
         select * from json_each_text(%L::json)
         )
      , json_lines_and_pgtype as (
        SELECT j.key
        ,  case when t.datatype::text ilike'character varying[]' 
           THEN  replace(replace(j.value::text , '[','{'),']','}')
            ELSE j.value
            END
        ,t.datatype 
        FROM json_lines j 
        LEFT JOIN dest_table_cols_pg_types t on (t.col::name= j.key::name)
		ORDER by j.key -- ajout 2022 10 04
        )

$SQL$
        , schemadest
         , tabledest
         , json_raw
);


  -- SQL qui permet d'obtenir la liste des values à insérer (val1::type, val2::type)
  sql_string_val_list_to_input :=  format (
     $SQL$
         WITH 
       %s

        SELECT string_agg(
         CASE WHEN j.datatype::name = 'geometry' AND  right(j.VALUE,1)='}' -- geojson force 2154 ( TODO adapt with srid input -> srid dest)
         THEN coalesce(quote_literal(st_setsrid(ST_GeomFromGeoJSON(j.VALUE),2154)::text),'NULL'::text)::text 
         ELSE 
         coalesce (quote_literal(j.VALUE),'NULL')
         END::text

         ||'::' || j.datatype ||
         case when  j.datatype::name = 'geometry' AND  right(j.VALUE,1)='}' 
         THEN '(geometry,2154) '
         ELSE ''
         END

         || '/*' || j.key ||  '*/' ,',')   
         FROM json_lines_and_pgtype j
         WHERE j.key  NOT in (%s)  
      $SQL$
      , sql_json_line_and_pg_type
      , sql_unnest_ignore_col_in_json
      ) ;

   if debugmode then  raise notice 'sql_string_val_list_to_input : % ', sql_string_val_list_to_input;
   END IF;


   -- liste des values à insérer (val1::type, val2::type)
   execute sql_string_val_list_to_input into string_val_list_to_input ;

   if debugmode then  raise notice 'string_val_list_to_input : % ', string_val_list_to_input;
   END IF;


   -- SQL qui permet d'obtenir la liste des values à setter dans un update :   col1=val1::type, col2=val2::type
   sql_string_set_values :=   format (
      $SQL$
      WITH 
       %s
        
        select string_agg(quote_ident(j.key) ||'=' ||
         CASE WHEN j.datatype::name = 'geometry' AND  right(j.VALUE,1)='}' --  geojson force 2154 ( TODO adapt with srid input -> srid dest) 
         THEN coalesce(quote_literal(st_setsrid(ST_GeomFromGeoJSON(j.VALUE),2154)::text),'NULL'::text)::text 
         ELSE 
         coalesce (quote_literal(j.VALUE),'NULL')
         END::text

        ||'::' || j.datatype ||
         case when  j.datatype::name = 'geometry' AND  right(j.VALUE,1)='}' 
         THEN '(geometry,2154) '
         ELSE ''
         END
         ,',') 
        FROM json_lines_and_pgtype j
        where j.key not in (%s )
       $SQL$
         , sql_json_line_and_pg_type
         , sql_unnest_ignore_col_in_json
    --     , sql_unnest_matching_col_list  
     );

   if debugmode then  raise notice 'sql_string_set_values : % ', sql_string_set_values;
   END IF;

   -- liste des values à setter dans un update :   col1=val1::type, col2=val2::type
   execute sql_string_set_values into string_set_values;

   
   skeep_update_set = false;
 -- init
   if string_set_values is  null THEN -- -- matching col list and ignore col cover all the json key list (there is no cols to set)
      if operation_mode ilike 'UPDATE%' THEN -- update
         raise notice 'Pas de colonne à seter dans un update : % ', string_set_values;

         skeep_update_set = true;

       
      END IF;

   END IF;


   if debugmode then  raise notice 'string_set_values : % ', string_set_values;
   END IF;


  sql_string_set_where :=   format (
   $SQL$
      WITH 
      %s
      
      select string_agg('d.' || quote_ident(j.key) ||'=' ||coalesce(quote_literal(j.value), 'NULL')||'::' || j.datatype , ' AND ') 
         from json_lines_and_pgtype j
        where j.key  in (%s ) 
    $SQL$
    , sql_json_line_and_pg_type
    , sql_unnest_matching_col_list  
  );


   if debugmode then  raise notice 'sql_string_set_where : % ', sql_string_set_where;
   END IF;


   execute sql_string_set_where into string_set_where;

   if debugmode then  raise notice 'string_set_where : % ', string_set_where;
   END IF;


  IF skeep_update_set is true THEN
   CASE  
      WHEN (operation_mode ='UPDATE ON CONFLICT' OR operation_mode ='NO UPDATE ON CONFLICT') THEN 
         operation_mode='NO UPDATE ON CONFLICT';

         raise notice 'Reset operation_mode to %', operation_mode;

      WHEN  (operation_mode ='UPDATE ON SAME' OR operation_mode ='NO UPDATE ON SAME')  THEN 
         operation_mode='NO UPDATE ON SAME';

         raise notice 'Reset operation_mode to %', operation_mode;

      ELSE 
      raise notice '%', msg_precise_mode;

   END CASE;
 
  END IF;


  CASE  operation_mode 
  WHEN  ('UPDATE ON CONFLICT' ) THEN 
   
        sql_ins_up := format (
        $SQL$
        WITH upsert as ( 
           INSERT INTO %I.%I AS d (%s) values (%s)
             ON CONFLICT (%s) DO UPDATE SET %s 
           WHERE  %s
           RETURNING 'UPSERT UPDATE POSSIBLE' as mod_mod, d.*
        )
        select upsert FROM upsert
          $SQL$
       , schemadest
       , tabledest

       , string_col_list_to_input
       , string_val_list_to_input
       , string_matching_col_list
       , string_set_values
       , string_set_where
       )
      ;

 
   WHEN  'NO UPDATE ON CONFLICT' THEN 
     sql_ins_up := format (
     $SQL$
     WITH upsert as (
        INSERT INTO %I.%I AS d (%s) values (%s)
          ON CONFLICT (%s) DO NOTHING
          RETURNING 'UPSERT NO UPDATE :' as mod_mod, d.*
      )
      select upsert FROM upsert
       $SQL$
    , schemadest
    , tabledest

    , string_col_list_to_input
    , string_val_list_to_input
    , string_matching_col_list

    )
   ;


   WHEN  'UPDATE ON SAME' THEN 
     sql_ins_up := format (
     $SQL$

     WITH 
     identiq as (
        SELECT * from %I.%I AS d
        WHERE  %s 
      )
     , nb_identiq as (select count(*) as nb from  identiq)
     , up as (
        UPDATE  %I.%I AS d 
        SET %s 
        WHERE  (
         (select nb from nb_identiq)=1 AND (%s)
         )
         RETURNING 'UPDATE'::text as mod_mod, d.*
      )
     , ins as (
      INSERT INTO %I.%I AS d (%s) SELECT * from 
      (SELECT %s 
         WHERE (
         (select nb from nb_identiq)=0
         )) as foo
        
        RETURNING 'INSERT'::text as mod_mod, d.*
     )

     SELECT  up  from up union all SELECT ins from ins
  
  
       $SQL$
       -- select identiq
       , schemadest
       , tabledest
       , string_set_where

       -- update
       , schemadest
       , tabledest
       , string_set_values
       , string_set_where

       -- insert
      , schemadest
      , tabledest
      , string_col_list_to_input
      , string_val_list_to_input
       
     

    )
   ;


    WHEN  'NO UPDATE ON SAME' THEN 
     sql_ins_up := format (
     $SQL$
     WITH identiq as (
        SELECT * from %I.%I AS d
        WHERE  %s 
      )
     , nb_identiq as (select count(*) as nb from  identiq)
     , no_up as (SELECT 'NO UPDATE ON SAME'::text 
        WHERE (
            (select nb from nb_identiq)=1
            )
            )
     , ins as (
      INSERT INTO %I.%I AS d (%s) SELECT * from 
      (SELECT %s
         WHERE (
         (select nb from nb_identiq)=0
         )) as foo
        
        RETURNING 'INSERT :'::text as mod_mod, d.*
     )
     SELECT  no_up  from no_up union all SELECT ins from ins
     
  
  
       $SQL$
       -- select identiq
       , schemadest
       , tabledest
       , string_set_where

       -- insert
      , schemadest
      , tabledest
      , string_col_list_to_input
      , string_val_list_to_input
       
     

    )
   ;

   ELSE 
      sql_ins_up := 'SELECT ''preciser le mode operatoire''';
 -- ne fait rien
      raise notice '%', msg_precise_mode;

   END CASE ;

   
if debugmode then 
raise notice 'sql_ins_up :
%', sql_ins_up;

END IF;

execute sql_ins_up into rtn_txt;


--return sql_ins_up::text;

return rtn_txt;


END;

$BODY$;


;
-- FUNCTION: sysma.f_dictionary_get_relation_child(name, text, bigint)

-- DROP FUNCTION IF EXISTS sysma.f_dictionary_get_relation_child(name, text, bigint);


CREATE OR REPLACE FUNCTION sysma.f_dictionary_get_relation_child(
	source_schema name,
	relation_type text,
	id_parent bigint)
    RETURNS SETOF bigint 
    LANGUAGE 'plpgsql'
    COST 100
    STABLE STRICT PARALLEL UNSAFE
    ROWS 1000

AS $BODY$


/*
permet de lister les ids qui dépendent d'un objet 
   object_type->bject_type_param->object_type_param_choice
ou 
   object_type->action_type->action_type_param->action_type_param_choice 

Exemple d'appel :

select sysma.f_dictionary_get_relation_child(
     source_schema := 'rx000_sysma_referentiel_mutualise_fdw':: NAME
   , relation_type := 'sysma_object_type->sysma_object_type_params':: TEXT
   , id_parent := '158507'::BIGINT
   )


*/

declare 
dico_table NAME;

id_parent_name NAME ;

id_child_name NAME ;

sql text;


col_list_ins TEXT;

r Bigint;

BEGIN


CASE  relation_type 
   WHEN 'object_type->object_type_param' THEN 
      dico_table := 'sysma_object_type_parameter'::NAME;

      id_parent_name := 'sysma_object_type_id';

      id_child_name := 'parameter_id';

   WHEN 'object_type_param->object_type_param_choice' THEN 
      dico_table := 'sysma_object_type_parameter_choice'::NAME;

      id_parent_name := 'parameter_id';

      id_child_name := 'choice_id';



   WHEN 'object_type->action_type' THEN 
      dico_table := 'sysma_action_type'::NAME;

      id_parent_name := 'sysma_object_type_id';

      id_child_name := 'sysma_action_type_id';

      
   WHEN 'action_type->action_type_param' THEN 
      dico_table := 'sysma_action_type_parameter'::NAME;

      id_parent_name := 'sysma_action_type_id';

      id_child_name := 'parameter_id';

      
   WHEN 'action_type_param->action_type_param_choice' THEN 
      dico_table := 'sysma_action_type_parameter_choice'::NAME;

      id_parent_name := 'parameter_id';

      id_child_name := 'choice_id';
 
   ELSE
   dico_table := '??';

END CASE;



sql := format (
$SQL$ 
SELECT  %I  -- dest_parent_id , col_list_ins
FROM %I.%I 
WHERE %I = %s
$SQL$
, id_child_name
, source_schema, dico_table
, id_parent_name, id_parent
);


raise notice '%', sql ;
  -- debug
--EXECUTE sql;


FOR r IN
        Execute sql
    LOOP
        -- can do some processing here
        RETURN NEXT r;
 -- return current row of SELECT
    END LOOP;

RETURN;

--RETURN QUERY '%', sql;

--return NEXT EXECUTE QUERY (sql);






END

$BODY$;


;
-- FUNCTION: sysma.f_dictionary_get_relation_child(name, text, bigint)

-- DROP FUNCTION IF EXISTS sysma.f_dictionary_get_relation_child(name, text, bigint);


CREATE OR REPLACE FUNCTION sysma.f_dictionary_get_relation_child(
	source_schema name,
	relation_type text,
	id_parent bigint)
    RETURNS SETOF bigint 
    LANGUAGE 'plpgsql'
    COST 100
    STABLE STRICT PARALLEL UNSAFE
    ROWS 1000

AS $BODY$


/*
permet de lister les ids qui dépendent d'un objet 
   object_type->bject_type_param->object_type_param_choice
ou 
   object_type->action_type->action_type_param->action_type_param_choice 

Exemple d'appel :

select sysma.f_dictionary_get_relation_child(
     source_schema := 'rx000_sysma_referentiel_mutualise_fdw':: NAME
   , relation_type := 'sysma_object_type->sysma_object_type_params':: TEXT
   , id_parent := '158507'::BIGINT
   )


*/

declare 
dico_table NAME;

id_parent_name NAME ;

id_child_name NAME ;

sql text;


col_list_ins TEXT;

r Bigint;

BEGIN


CASE  relation_type 
   WHEN 'object_type->object_type_param' THEN 
      dico_table := 'sysma_object_type_parameter'::NAME;

      id_parent_name := 'sysma_object_type_id';

      id_child_name := 'parameter_id';

   WHEN 'object_type_param->object_type_param_choice' THEN 
      dico_table := 'sysma_object_type_parameter_choice'::NAME;

      id_parent_name := 'parameter_id';

      id_child_name := 'choice_id';



   WHEN 'object_type->action_type' THEN 
      dico_table := 'sysma_action_type'::NAME;

      id_parent_name := 'sysma_object_type_id';

      id_child_name := 'sysma_action_type_id';

      
   WHEN 'action_type->action_type_param' THEN 
      dico_table := 'sysma_action_type_parameter'::NAME;

      id_parent_name := 'sysma_action_type_id';

      id_child_name := 'parameter_id';

      
   WHEN 'action_type_param->action_type_param_choice' THEN 
      dico_table := 'sysma_action_type_parameter_choice'::NAME;

      id_parent_name := 'parameter_id';

      id_child_name := 'choice_id';
 
   ELSE
   dico_table := 'dico_table??';

   id_parent_name := 'parameter_id??';

   id_child_name := 'choice_id??';
 
END CASE;



sql := format (
$SQL$ 
SELECT  %I  -- dest_parent_id , col_list_ins
FROM %I.%I 
WHERE %I = %s
$SQL$
, id_child_name
, source_schema, dico_table
, id_parent_name, id_parent
);


raise notice '%', sql ;
  -- debug
--EXECUTE sql;


FOR r IN
        Execute sql
    LOOP
        -- can do some processing here
        RETURN NEXT r;
 -- return current row of SELECT
    END LOOP;

RETURN;

--RETURN QUERY '%', sql;

--return NEXT EXECUTE QUERY (sql);






END

$BODY$;


;
-- FUNCTION: sysma.f_dictionary_get_relation_child(name, text, bigint)

-- DROP FUNCTION IF EXISTS sysma.f_dictionary_get_relation_child(name, text, bigint);


CREATE OR REPLACE FUNCTION sysma.f_dictionary_get_relation_child(
	source_schema name,
	relation_type text,
	id_parent bigint)
    RETURNS SETOF bigint 
    LANGUAGE 'plpgsql'
    COST 100
    STABLE STRICT PARALLEL UNSAFE
    ROWS 1000

AS $BODY$


/*
permet de lister les ids qui dépendent d'un objet 
   object_type->bject_type_param->object_type_param_choice
ou 
   object_type->action_type->action_type_param->action_type_param_choice 

Exemple d'appel :

select sysma.f_dictionary_get_relation_child(
     source_schema := 'rx000_sysma_referentiel_mutualise_fdw':: NAME
   , relation_type := 'sysma_object_type->sysma_object_type_params':: TEXT
   , id_parent := '158507'::BIGINT
   )


*/

declare 
dico_table NAME;

id_parent_name NAME ;

id_child_name NAME ;

sql text;


col_list_ins TEXT;

r Bigint;

BEGIN


CASE  relation_type 
   WHEN 'object_type->object_type_param' THEN 
      dico_table := 'sysma_object_type_parameter'::NAME;

      id_parent_name := 'sysma_object_type_id';

      id_child_name := 'parameter_id';

   WHEN 'object_type_param->object_type_param_choice' THEN 
      dico_table := 'sysma_object_type_parameter_choice'::NAME;

      id_parent_name := 'parameter_id';

      id_child_name := 'choice_id';



   WHEN 'object_type->action_type' THEN 
      dico_table := 'sysma_action_type'::NAME;

      id_parent_name := 'sysma_object_type_id';

      id_child_name := 'sysma_action_type_id';

      
   WHEN 'action_type->action_type_param' THEN 
      dico_table := 'sysma_action_type_parameter'::NAME;

      id_parent_name := 'sysma_action_type_id';

      id_child_name := 'parameter_id';

      
   WHEN 'action_type_param->action_type_param_choice' THEN 
      dico_table := 'sysma_action_type_parameter_choice'::NAME;

      id_parent_name := 'parameter_id';

      id_child_name := 'choice_id';
 
   ELSE
   dico_table := 'dico_table??';

   id_parent_name := 'parameter_id??';

   id_child_name := 'choice_id??';
 
END CASE;



sql := format (
$SQL$ 
SELECT  %I  -- dest_parent_id , col_list_ins
FROM %I.%I 
WHERE %I = %s
$SQL$
, id_child_name
, source_schema, dico_table
, id_parent_name, id_parent
);


--raise notice '%', sql ;
  -- debug
--EXECUTE sql;


FOR r IN
        Execute sql
    LOOP
        -- can do some processing here
        RETURN NEXT r;
 -- return current row of SELECT
    END LOOP;

RETURN;

--RETURN QUERY '%', sql;

--return NEXT EXECUTE QUERY (sql);






END

$BODY$;


;
-- FUNCTION: sysma.f_dictionary_sync_v04(name, name, name, name, name, text, name, bigint, bigint, boolean, bigint)

-- DROP FUNCTION IF EXISTS sysma.f_dictionary_sync_v04(name, name, name, name, name, text, name, bigint, bigint, boolean, bigint);


CREATE OR REPLACE FUNCTION sysma.f_dictionary_sync_v04(
	source_schema name,
	source_table name,
	dest_schema name,
	dest_table name,
	id_col_name name,
	col_list text,
	parent_col_name name,
	source_id bigint,
	dest_parent_id bigint,
	new_dest_id boolean DEFAULT false,
	modificateur_id bigint DEFAULT 1)
    RETURNS json
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$


/*
permet de copier une entrée du dico de sysma depuis les tables fdw vers sysma des territoires mutualisés

V04 : compatibilité avec sysma V03 modificateur_id => modified_by
V03 : attribue id_modificateur = modificateur_id, 1 par défaut = init sysma
      
V02 : si new_dest_id := FALSE et id existe déjà dans le dictionnaire l'entrée ne sera pas ajoutée, et l'id existant sera retourné.

Exemple d'appel :

select sysma.f_dictionary_sync_v04(
     source_schema := 'rx000_sysma_referentiel_mutualise_fdw':: NAME
   , source_table := 'sysma_object_type_parameter':: NAME
   , dest_schema := 'sysma'::NAME
   , dest_table := 'sysma_object_type_parameter'::NAME
   , id_col_name := 'parameter_id'::NAME
   , col_list := ' parameter, description, data_type, ordered_by, created_at, modified_at, parameter_source, data_source, unit, required, parameter_alias, time_tracking, required_field_parameter, hidden_in_form '::text
   , parent_col_name := 'sysma_object_type_id'::NAME
   , source_id := '100'::bigint
   , dest_parent_id := '1'::bigint 
   , new_dest_id := FALSE::boolean  
   , modificateur_id :=1 -- init data (mutualisation)
   )

*/

declare 
sql text;


sql_test_exist TEXT;


bigint_test_exist bigint;

rtn json;


col_list_ins TEXT;

parent_col_name_or_not text;

dest_parent_id_or_not  text;


BEGIN

IF new_dest_id IS FALSE THEN
   sql_test_exist := format  (
   $SQL$ 

   SELECT %I   -- dest_parent_id , col_list_ins
   FROM %I.%I 
   WHERE %I = %s

   $SQL$
   , id_col_name
   , dest_schema, dest_table
   , id_col_name , source_id

   );


   execute sql_test_exist into bigint_test_exist;


   IF (bigint_test_exist::bigint = source_id::bigint) THEN

   rtn :=  to_json(t) FROM (SELECT bigint_test_exist as res_id, 'ID existe deja, entrée non ajoutée' as  res_comment) t;

      
   return   rtn;



   ELSE 

   if new_dest_id IS TRUE THEN 
      col_list_ins = col_list ;

   ELSE 
      col_list_ins = id_col_name || ', '|| col_list ;

   END IF ;


   if parent_col_name IS NULL THEN 
      parent_col_name_or_not = '' ;

      dest_parent_id_or_not = '';

      
   ELSE 
      parent_col_name_or_not = parent_col_name || ' , ' ;

      dest_parent_id_or_not = dest_parent_id || ' , ';


   END IF ;



   sql := format (
   $SQL$ 
   WITH ins as (
   INSERT INTO %I.%I   -- table dest
      (%s  %s, modified_by) -- parent_col_name , col_list_ins
   SELECT %s   %s , %s -- dest_parent_id , col_list_ins, modificateur_id
   FROM %I.%I 
   WHERE %I = %s
   -- LIMIT 1

   RETURNING  %I as res_id, 'ajout ok'::text as res_comment

  )
  select to_json (ins) from ins
   $SQL$
   , dest_schema, dest_table
   , parent_col_name_or_not, col_list_ins


   , dest_parent_id_or_not, col_list_ins , modificateur_id
   , source_schema, source_table
   , id_col_name , source_id

   , id_col_name
   );

   raise notice 'sql % ', sql;

   execute sql into rtn;


   return   rtn;


   END IF;


END IF;


END

$BODY$;


;
-- FUNCTION: sysma.f_dictionary_sync_v04(name, name, name, name, name, text, name, bigint, bigint, boolean, bigint)

-- DROP FUNCTION IF EXISTS sysma.f_dictionary_sync_v04(name, name, name, name, name, text, name, bigint, bigint, boolean, bigint);


CREATE OR REPLACE FUNCTION sysma.f_dictionary_sync_v04(
	source_schema name,
	source_table name,
	dest_schema name,
	dest_table name,
	id_col_name name,
	col_list text,
	parent_col_name name,
	source_id bigint,
	dest_parent_id bigint,
	new_dest_id boolean DEFAULT false,
	modificateur_id bigint DEFAULT 1)
    RETURNS json
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$


/*
permet de copier une entrée du dico de sysma depuis les tables fdw vers sysma des territoires mutualisés

V04 : compatibilité avec sysma V03 modificateur_id => modified_by
V03 : attribue id_modificateur = modificateur_id, 1 par défaut = init sysma
      
V02 : si new_dest_id := FALSE et id existe déjà dans le dictionnaire l'entrée ne sera pas ajoutée, et l'id existant sera retourné.

Exemple d'appel :

select sysma.f_dictionary_sync_v04(
     source_schema := 'rx000_sysma_referentiel_mutualise_fdw':: NAME
   , source_table := 'sysma_object_type_parameter':: NAME
   , dest_schema := 'sysma'::NAME
   , dest_table := 'sysma_object_type_parameter'::NAME
   , id_col_name := 'parameter_id'::NAME
   , col_list := ' parameter, description, data_type, ordered_by, created_at, modified_at, parameter_source, data_source, unit, required, parameter_alias, time_tracking, required_field_parameter, hidden_in_form '::text
   , parent_col_name := 'sysma_object_type_id'::NAME
   , source_id := '100'::bigint
   , dest_parent_id := '1'::bigint 
   , new_dest_id := FALSE::boolean  
   , modificateur_id :=1 -- init data (mutualisation)
   )

*/

declare 
sql text;


sql_test_exist TEXT;


bigint_test_exist bigint;

rtn json;


col_list_ins TEXT;

parent_col_name_or_not text;

dest_parent_id_or_not  text;


BEGIN

IF new_dest_id IS FALSE THEN
   sql_test_exist := format  (
   $SQL$ 

   SELECT %I   -- dest_parent_id , col_list_ins
   FROM %I.%I 
   WHERE %I = %s

   $SQL$
   , id_col_name
   , dest_schema, dest_table
   , id_col_name , source_id

   );


   execute sql_test_exist into bigint_test_exist;


   IF (bigint_test_exist::bigint = source_id::bigint) THEN

   rtn :=  to_json(t) FROM (SELECT bigint_test_exist as res_id, 'ID existe deja, entrée non ajoutée' as  res_comment) t;

   raise notice 'sql % ', sql;
   
   return   rtn;



   ELSE 

   if new_dest_id IS TRUE THEN 
      col_list_ins = col_list ;

   ELSE 
      col_list_ins = id_col_name || ', '|| col_list ;

   END IF ;


   if parent_col_name IS NULL THEN 
      parent_col_name_or_not = '' ;

      dest_parent_id_or_not = '';

      
   ELSE 
      parent_col_name_or_not = parent_col_name || ' , ' ;

      dest_parent_id_or_not = dest_parent_id || ' , ';


   END IF ;



   sql := format (
   $SQL$ 
   WITH ins as (
   INSERT INTO %I.%I   -- table dest
      (%s  %s, modified_by) -- parent_col_name , col_list_ins
   SELECT %s   %s , %s -- dest_parent_id , col_list_ins, modificateur_id
   FROM %I.%I 
   WHERE %I = %s
   -- LIMIT 1

   RETURNING  %I as res_id, 'ajout ok'::text as res_comment

  )
  select to_json (ins) from ins
   $SQL$
   , dest_schema, dest_table
   , parent_col_name_or_not, col_list_ins


   , dest_parent_id_or_not, col_list_ins , modificateur_id
   , source_schema, source_table
   , id_col_name , source_id

   , id_col_name
   );

   raise notice 'sql % ', sql;

   execute sql into rtn;


   return   rtn;


   END IF;


END IF;


END

$BODY$;


;
-- FUNCTION: sysma.f_dictionary_sync_v04(name, name, name, name, name, text, name, bigint, bigint, boolean, bigint)

-- DROP FUNCTION IF EXISTS sysma.f_dictionary_sync_v04(name, name, name, name, name, text, name, bigint, bigint, boolean, bigint);


CREATE OR REPLACE FUNCTION sysma.f_dictionary_sync_v04(
	source_schema name,
	source_table name,
	dest_schema name,
	dest_table name,
	id_col_name name,
	col_list text,
	parent_col_name name,
	source_id bigint,
	dest_parent_id bigint,
	new_dest_id boolean DEFAULT false,
	modificateur_id bigint DEFAULT 1)
    RETURNS json
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$


/*
permet de copier une entrée du dico de sysma depuis les tables fdw vers sysma des territoires mutualisés

V04 : compatibilité avec sysma V03 modificateur_id => modified_by
V03 : attribue id_modificateur = modificateur_id, 1 par défaut = init sysma
      
V02 : si new_dest_id := FALSE et id existe déjà dans le dictionnaire l'entrée ne sera pas ajoutée, et l'id existant sera retourné.

Exemple d'appel :

select sysma.f_dictionary_sync_v04(
     source_schema := 'rx000_sysma_referentiel_mutualise_fdw':: NAME
   , source_table := 'sysma_object_type_parameter':: NAME
   , dest_schema := 'sysma'::NAME
   , dest_table := 'sysma_object_type_parameter'::NAME
   , id_col_name := 'parameter_id'::NAME
   , col_list := ' parameter, description, data_type, ordered_by, created_at, modified_at, parameter_source, data_source, unit, required, parameter_alias, time_tracking, required_field_parameter, hidden_in_form '::text
   , parent_col_name := 'sysma_object_type_id'::NAME
   , source_id := '100'::bigint
   , dest_parent_id := '1'::bigint 
   , new_dest_id := FALSE::boolean  
   , modificateur_id :=1 -- init data (mutualisation)
   )

*/

declare 
sql text;


sql_test_exist TEXT;


bigint_test_exist bigint;

rtn json;


col_list_ins TEXT;

parent_col_name_or_not text;

dest_parent_id_or_not  text;


BEGIN

IF new_dest_id IS FALSE THEN
   sql_test_exist := format  (
   $SQL$ 

   SELECT %I   -- dest_parent_id , col_list_ins
   FROM %I.%I 
   WHERE %I = %s

   $SQL$
   , id_col_name
   , dest_schema, dest_table
   , id_col_name , source_id

   );

   raise notice 'sql_test_exist % ', sql_test_exist;
   
   execute sql_test_exist into bigint_test_exist;


   IF (bigint_test_exist::bigint = source_id::bigint) THEN

   rtn :=  to_json(t) FROM (SELECT bigint_test_exist as res_id, 'ID existe deja, entrée non ajoutée' as  res_comment) t;

   
   return   rtn;



   ELSE 

   if new_dest_id IS TRUE THEN 
      col_list_ins = col_list ;

   ELSE 
      col_list_ins = id_col_name || ', '|| col_list ;

   END IF ;


   if parent_col_name IS NULL THEN 
      parent_col_name_or_not = '' ;

      dest_parent_id_or_not = '';

      
   ELSE 
      parent_col_name_or_not = parent_col_name || ' , ' ;

      dest_parent_id_or_not = dest_parent_id || ' , ';


   END IF ;



   sql := format (
   $SQL$ 
   WITH ins as (
   INSERT INTO %I.%I   -- table dest
      (%s  %s, modified_by) -- parent_col_name , col_list_ins
   SELECT %s   %s , %s -- dest_parent_id , col_list_ins, modificateur_id
   FROM %I.%I 
   WHERE %I = %s
   -- LIMIT 1

   RETURNING  %I as res_id, 'ajout ok'::text as res_comment

	  )
	  select to_json (ins) from ins
	   $SQL$
	   , dest_schema, dest_table
	   , parent_col_name_or_not, col_list_ins


	   , dest_parent_id_or_not, col_list_ins , modificateur_id
	   , source_schema, source_table
	   , id_col_name , source_id

	   , id_col_name
	   );

   raise notice 'sql % ', sql;

   execute sql into rtn;


   return   rtn;


   END IF;


END IF;


END

$BODY$;


;
-- FUNCTION: sysma.f_dictionary_sync_v04(name, name, name, name, name, text, name, bigint, bigint, boolean, bigint)

-- DROP FUNCTION IF EXISTS sysma.f_dictionary_sync_v04(name, name, name, name, name, text, name, bigint, bigint, boolean, bigint);


CREATE OR REPLACE FUNCTION sysma.f_dictionary_sync_v04(
	source_schema name,
	source_table name,
	dest_schema name,
	dest_table name,
	id_col_name name,
	col_list text,
	parent_col_name name,
	source_id bigint,
	dest_parent_id bigint,
	new_dest_id boolean DEFAULT false,
	modificateur_id bigint DEFAULT 1)
    RETURNS json
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$


/*
permet de copier une entrée du dico de sysma depuis les tables fdw vers sysma des territoires mutualisés

V04 : compatibilité avec sysma V03 modificateur_id => modified_by
V03 : attribue id_modificateur = modificateur_id, 1 par défaut = init sysma
      
V02 : si new_dest_id := FALSE et id existe déjà dans le dictionnaire l'entrée ne sera pas ajoutée, et l'id existant sera retourné.

Exemple d'appel :

select sysma.f_dictionary_sync_v04(
     source_schema := 'rx000_sysma_referentiel_mutualise_fdw':: NAME
   , source_table := 'sysma_object_type_parameter':: NAME
   , dest_schema := 'sysma'::NAME
   , dest_table := 'sysma_object_type_parameter'::NAME
   , id_col_name := 'parameter_id'::NAME
   , col_list := ' parameter, description, data_type, ordered_by, created_at, modified_at, parameter_source, data_source, unit, required, parameter_alias, time_tracking, required_field_parameter, hidden_in_form '::text
   , parent_col_name := 'sysma_object_type_id'::NAME
   , source_id := '100'::bigint
   , dest_parent_id := '1'::bigint 
   , new_dest_id := FALSE::boolean  
   , modificateur_id :=1 -- init data (mutualisation)
   )

*/

declare 
sql text;


sql_test_exist TEXT;


bigint_test_exist bigint;

rtn json;


col_list_ins TEXT;

parent_col_name_or_not text;

dest_parent_id_or_not  text;


BEGIN

IF new_dest_id IS FALSE THEN
   sql_test_exist := format  (
   $SQL$ 

   SELECT %I   -- dest_parent_id , col_list_ins
   FROM %I.%I 
   WHERE %I = %s

   $SQL$
   , id_col_name
   , dest_schema, dest_table
   , id_col_name , source_id

   );

   raise notice 'sql_test_exist % ', sql_test_exist;
   
   execute sql_test_exist into bigint_test_exist;


   IF (bigint_test_exist::bigint = source_id::bigint) THEN

	   rtn :=  to_json(t) FROM (SELECT bigint_test_exist as res_id, 'ID existe deja, entrée non ajoutée' as  res_comment) t;


	   return  rtn;
						
   END IF;


   ELSE 

	   IF new_dest_id IS TRUE THEN 
		  col_list_ins = col_list ;

	   ELSE 
		  col_list_ins = id_col_name || ', '|| col_list ;

	   END IF ;


	   IF parent_col_name IS NULL THEN 
		  parent_col_name_or_not = '' ;

		  dest_parent_id_or_not = '';


	   ELSE 
		  parent_col_name_or_not = parent_col_name || ' , ' ;

		  dest_parent_id_or_not = dest_parent_id || ' , ';


	   END IF ;



	   sql := format (
	   $SQL$ 
	   WITH ins as (
		   INSERT INTO %I.%I   -- table dest
			  (%s  %s, modified_by) -- parent_col_name , col_list_ins
		   SELECT %s   %s , %s -- dest_parent_id , col_list_ins, modificateur_id
		   FROM %I.%I 
		   WHERE %I = %s
		   -- LIMIT 1
		   RETURNING  %I as res_id, 'ajout ok'::text as res_comment
		)
	  SELECT to_json (ins) from ins
	   $SQL$
	   , dest_schema, dest_table
	   , parent_col_name_or_not, col_list_ins


	   , dest_parent_id_or_not, col_list_ins , modificateur_id
	   , source_schema, source_table
	   , id_col_name , source_id

	   , id_col_name
	   );

	   raise notice 'sql % ', sql;

	   execute sql into rtn;


	   return   rtn;



END IF;


END

$BODY$;


;
-- FUNCTION: sysma.f_dictionary_sync_v04(name, name, name, name, name, text, name, bigint, bigint, boolean, bigint)

-- DROP FUNCTION IF EXISTS sysma.f_dictionary_sync_v04(name, name, name, name, name, text, name, bigint, bigint, boolean, bigint);


CREATE OR REPLACE FUNCTION sysma.f_dictionary_sync_v04(
	source_schema name,
	source_table name,
	dest_schema name,
	dest_table name,
	id_col_name name,
	col_list text,
	parent_col_name name,
	source_id bigint,
	dest_parent_id bigint,
	new_dest_id boolean DEFAULT false,
	modificateur_id bigint DEFAULT 1)
    RETURNS json
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$


/*
permet de copier une entrée du dico de sysma depuis les tables fdw vers sysma des territoires mutualisés

V04 : compatibilité avec sysma V03 modificateur_id => modified_by
V03 : attribue id_modificateur = modificateur_id, 1 par défaut = init sysma
      
V02 : si new_dest_id := FALSE et id existe déjà dans le dictionnaire l'entrée ne sera pas ajoutée, et l'id existant sera retourné.

Exemple d'appel :

select sysma.f_dictionary_sync_v04(
     source_schema := 'rx000_sysma_referentiel_mutualise_fdw':: NAME
   , source_table := 'sysma_object_type_parameter':: NAME
   , dest_schema := 'sysma'::NAME
   , dest_table := 'sysma_object_type_parameter'::NAME
   , id_col_name := 'parameter_id'::NAME
   , col_list := ' parameter, description, data_type, ordered_by, created_at, modified_at, parameter_source, data_source, unit, required, parameter_alias, time_tracking, required_field_parameter, hidden_in_form '::text
   , parent_col_name := 'sysma_object_type_id'::NAME
   , source_id := '100'::bigint
   , dest_parent_id := '1'::bigint 
   , new_dest_id := FALSE::boolean  
   , modificateur_id :=1 -- init data (mutualisation)
   )

*/

declare 
sql text;

rtn json ='{"mode":"debug"}';

sql_test_exist TEXT;


bigint_test_exist bigint;


col_list_ins TEXT;

parent_col_name_or_not text;

dest_parent_id_or_not  text;


BEGIN

IF new_dest_id IS FALSE THEN
   sql_test_exist := format  (
   $SQL$ 

   SELECT %I   -- dest_parent_id , col_list_ins
   FROM %I.%I 
   WHERE %I = %s

   $SQL$
   , id_col_name
   , dest_schema, dest_table
   , id_col_name , source_id

   );

   raise notice 'sql_test_exist % ', sql_test_exist;
   
   execute sql_test_exist into bigint_test_exist;


   IF (bigint_test_exist::bigint = source_id::bigint) THEN

	   rtn :=  to_json(t) FROM (SELECT bigint_test_exist as res_id, 'ID existe deja, entrée non ajoutée' as  res_comment) t;


	   return  rtn;
						
   END IF;


   ELSE 

	   IF new_dest_id IS TRUE THEN 
		  col_list_ins = col_list ;

	   ELSE 
		  col_list_ins = id_col_name || ', '|| col_list ;

	   END IF ;


	   IF parent_col_name IS NULL THEN 
		  parent_col_name_or_not = '' ;

		  dest_parent_id_or_not = '';


	   ELSE 
		  parent_col_name_or_not = parent_col_name || ' , ' ;

		  dest_parent_id_or_not = dest_parent_id || ' , ';


	   END IF ;



	   sql := format (
	   $SQL$ 
	   WITH ins as (
		   INSERT INTO %I.%I   -- table dest
			  (%s  %s, modified_by) -- parent_col_name , col_list_ins
		   SELECT %s   %s , %s -- dest_parent_id , col_list_ins, modificateur_id
		   FROM %I.%I 
		   WHERE %I = %s
		   -- LIMIT 1
		   RETURNING  %I as res_id, 'ajout ok'::text as res_comment
		)
	  SELECT to_json (ins) from ins
	   $SQL$
	   , dest_schema, dest_table
	   , parent_col_name_or_not, col_list_ins


	   , dest_parent_id_or_not, col_list_ins , modificateur_id
	   , source_schema, source_table
	   , id_col_name , source_id

	   , id_col_name
	   );

	   raise notice 'sql % ', sql;

	   --execute sql into rtn;


	   return   rtn;



END IF;


END

$BODY$;


;
-- FUNCTION: sysma.f_dictionary_sync_v04(name, name, name, name, name, text, name, bigint, bigint, boolean, bigint)

-- DROP FUNCTION IF EXISTS sysma.f_dictionary_sync_v04(name, name, name, name, name, text, name, bigint, bigint, boolean, bigint);


CREATE OR REPLACE FUNCTION sysma.f_dictionary_sync_v04(
	source_schema name,
	source_table name,
	dest_schema name,
	dest_table name,
	id_col_name name,
	col_list text,
	parent_col_name name,
	source_id bigint,
	dest_parent_id bigint,
	new_dest_id boolean DEFAULT false,
	modificateur_id bigint DEFAULT 1)
    RETURNS json
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$


/*
permet de copier une entrée du dico de sysma depuis les tables fdw vers sysma des territoires mutualisés

V04 : compatibilité avec sysma V03 modificateur_id => modified_by
V03 : attribue id_modificateur = modificateur_id, 1 par défaut = init sysma
      
V02 : si new_dest_id := FALSE et id existe déjà dans le dictionnaire l'entrée ne sera pas ajoutée, et l'id existant sera retourné.

Exemple d'appel :

select sysma.f_dictionary_sync_v04(
     source_schema := 'rx000_sysma_referentiel_mutualise_fdw':: NAME
   , source_table := 'sysma_object_type_parameter':: NAME
   , dest_schema := 'sysma'::NAME
   , dest_table := 'sysma_object_type_parameter'::NAME
   , id_col_name := 'parameter_id'::NAME
   , col_list := ' parameter, description, data_type, ordered_by, created_at, modified_at, parameter_source, data_source, unit, required, parameter_alias, time_tracking, required_field_parameter, hidden_in_form '::text
   , parent_col_name := 'sysma_object_type_id'::NAME
   , source_id := '100'::bigint
   , dest_parent_id := '1'::bigint 
   , new_dest_id := FALSE::boolean  
   , modificateur_id :=1 -- init data (mutualisation)
   )

*/

declare 
sql text;

rtn json ='{"mode":"debug"}';

sql_test_exist TEXT;


bigint_test_exist bigint;


col_list_ins TEXT;

parent_col_name_or_not text;

dest_parent_id_or_not  text;


BEGIN

IF new_dest_id IS FALSE THEN
   sql_test_exist := format  (
   $SQL$ 

   SELECT %I   -- dest_parent_id , col_list_ins
   FROM %I.%I 
   WHERE %I = %s

   $SQL$
   , id_col_name
   , dest_schema, dest_table
   , id_col_name , source_id

   );

   raise notice 'sql_test_exist % ', sql_test_exist;
   
   execute sql_test_exist into bigint_test_exist;


   IF (bigint_test_exist::bigint = source_id::bigint) THEN

	   rtn :=  to_json(t) FROM (SELECT bigint_test_exist as res_id, 'ID existe deja, entrée non ajoutée' as  res_comment) t;


	   return  rtn;
						
   END IF;


ELSE 

	   IF new_dest_id IS TRUE THEN 
		  col_list_ins = col_list ;

	   ELSE 
		  col_list_ins = id_col_name || ', '|| col_list ;

	   END IF ;


	   IF parent_col_name IS NULL THEN 
	      raise notice 'parent_col_name est null';

		  parent_col_name_or_not = '' ;

		  dest_parent_id_or_not = '';


	   ELSE 
	   	  
		  parent_col_name_or_not = parent_col_name || ' , ' ;

		  dest_parent_id_or_not = dest_parent_id || ' , ';

		  raise notice 'parent_col_name NOT null , 
		  parent_col_name_or_not %
		  dest_parent_id_or_not %', parent_col_name_or_not, dest_parent_id_or_not  ;


	   END IF ;



	   sql := format (
	   $SQL$ 
	   WITH ins as (
		   INSERT INTO %I.%I   -- table dest
			  (%s  %s, modified_by) -- parent_col_name , col_list_ins
		   SELECT %s   %s , %s -- dest_parent_id , col_list_ins, modificateur_id
		   FROM %I.%I 
		   WHERE %I = %s
		   -- LIMIT 1
		   RETURNING  %I as res_id, 'ajout ok'::text as res_comment
		)
	  SELECT to_json (ins) from ins
	   $SQL$
	   , dest_schema, dest_table
	   , parent_col_name_or_not, col_list_ins


	   , dest_parent_id_or_not, col_list_ins , modificateur_id
	   , source_schema, source_table
	   , id_col_name , source_id

	   , id_col_name
	   );

	   raise notice 'sql % ', sql;

	   --execute sql into rtn;


	   return   rtn;



END IF;


END

$BODY$;


;
-- FUNCTION: sysma.f_dictionary_sync_v04(name, name, name, name, name, text, name, bigint, bigint, boolean, bigint)

-- DROP FUNCTION IF EXISTS sysma.f_dictionary_sync_v04(name, name, name, name, name, text, name, bigint, bigint, boolean, bigint);


CREATE OR REPLACE FUNCTION sysma.f_dictionary_sync_v04(
	source_schema name,
	source_table name,
	dest_schema name,
	dest_table name,
	id_col_name name,
	col_list text,
	parent_col_name name,
	source_id bigint,
	dest_parent_id bigint,
	new_dest_id boolean DEFAULT false,
	modificateur_id bigint DEFAULT 1)
    RETURNS json
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$


/*
permet de copier une entrée du dico de sysma depuis les tables fdw vers sysma des territoires mutualisés

V04 : compatibilité avec sysma V03 modificateur_id => modified_by
V03 : attribue id_modificateur = modificateur_id, 1 par défaut = init sysma
      
V02 : si new_dest_id := FALSE et id existe déjà dans le dictionnaire l'entrée ne sera pas ajoutée, et l'id existant sera retourné.

Exemple d'appel :

select sysma.f_dictionary_sync_v04(
     source_schema := 'rx000_sysma_referentiel_mutualise_fdw':: NAME
   , source_table := 'sysma_object_type_parameter':: NAME
   , dest_schema := 'sysma'::NAME
   , dest_table := 'sysma_object_type_parameter'::NAME
   , id_col_name := 'parameter_id'::NAME
   , col_list := ' parameter, description, data_type, ordered_by, created_at, modified_at, parameter_source, data_source, unit, required, parameter_alias, time_tracking, required_field_parameter, hidden_in_form '::text
   , parent_col_name := 'sysma_object_type_id'::NAME
   , source_id := '100'::bigint
   , dest_parent_id := '1'::bigint 
   , new_dest_id := FALSE::boolean  
   , modificateur_id :=1 -- init data (mutualisation)
   )

*/

declare 
sql text;

rtn json ='{"mode":"debug"}';

sql_test_exist TEXT;


bigint_test_exist bigint;


col_list_ins TEXT;

parent_col_name_or_not text;

dest_parent_id_or_not  text;


BEGIN

IF new_dest_id IS FALSE THEN
   sql_test_exist := format  (
   $SQL$ 

   SELECT %I   -- dest_parent_id , col_list_ins
   FROM %I.%I 
   WHERE %I = %s

   $SQL$
   , id_col_name
   , dest_schema, dest_table
   , id_col_name , source_id

   );

   raise notice 'sql_test_exist % ', sql_test_exist;
   
   execute sql_test_exist into bigint_test_exist;


   IF (bigint_test_exist::bigint = source_id::bigint) THEN

	   rtn :=  to_json(t) FROM (SELECT bigint_test_exist as res_id, 'ID existe deja, entrée non ajoutée' as  res_comment) t;


	   return  rtn;
						
   END IF;


ELSE 

	   IF new_dest_id IS TRUE THEN 
		  col_list_ins = col_list ;

	   ELSE 
		  col_list_ins = id_col_name || ', '|| col_list ;

	   END IF ;


	   IF parent_col_name IS NULL THEN 
	      raise notice 'parent_col_name est null';

		  parent_col_name_or_not = '' ;

		  dest_parent_id_or_not = '';


	   ELSE 
	   	  
		  parent_col_name_or_not = parent_col_name || ' , ' ;

		  dest_parent_id_or_not = dest_parent_id || ' , ';

		  --raise notice 'parent_col_name NOT null , 
		  --parent_col_name_or_not %
		  --dest_parent_id_or_not %', parent_col_name_or_not, dest_parent_id_or_not  ;


	   END IF ;



	   sql := format (
	   $SQL$ 
	   WITH ins as (
		   INSERT INTO %I.%I   -- table dest
			  (%s  %s, modified_by) -- parent_col_name , col_list_ins
		   SELECT %s   %s , %s -- dest_parent_id , col_list_ins, modificateur_id
		   FROM %I.%I 
		   WHERE %I = %s
		   -- LIMIT 1
		   RETURNING  %I as res_id, 'ajout ok'::text as res_comment
		)
	  SELECT to_json (ins) from ins
	   $SQL$
	   , dest_schema, dest_table
	   , parent_col_name_or_not, col_list_ins


	   , dest_parent_id_or_not, col_list_ins , modificateur_id
	   , source_schema, source_table
	   , id_col_name , source_id

	   , id_col_name
	   );

	   -- raise notice 'sql % ', sql;

	   execute sql into rtn;


	   return   rtn;



END IF;


END

$BODY$;


;
