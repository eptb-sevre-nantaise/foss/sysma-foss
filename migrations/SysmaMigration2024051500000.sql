/*
TABLE sysma.sysma_action_type_parameter_choice
*/
/* ADD columns */
ALTER TABLE IF EXISTS sysma.sysma_action_type_parameter_choice ADD COLUMN IF NOT EXISTS value_type character varying DEFAULT 'choice'::character varying ; 
/* Set defaults */
ALTER TABLE IF EXISTS sysma.sysma_action_type_parameter_choice
    ALTER COLUMN value_type  SET DEFAULT 'choice'::character varying;


/*
TABLE sysma.photo_object
*/
/* ADD columns */
ALTER TABLE IF EXISTS sysma.photo_object ADD COLUMN IF NOT EXISTS created_at timestamp DEFAULT NULL ; 
ALTER TABLE IF EXISTS sysma.photo_object ADD COLUMN IF NOT EXISTS modified_by integer DEFAULT NULL ;  
ALTER TABLE IF EXISTS sysma.photo_object ADD COLUMN IF NOT EXISTS modified_at timestamp DEFAULT NULL ; 

/* Set defaults */
ALTER TABLE IF EXISTS sysma.photo_object
    ALTER COLUMN created_at  SET DEFAULT now() ; 
ALTER TABLE IF EXISTS sysma.photo_object
    ALTER COLUMN modified_at SET DEFAULT now() ;  


/* add constraint */
ALTER TABLE IF EXISTS sysma.photo_object DROP CONSTRAINT IF EXISTS fk_photo_object_modified_by ; 
ALTER TABLE IF EXISTS sysma.photo_object
    ADD CONSTRAINT fk_photo_object_modified_by FOREIGN KEY (modified_by)
    REFERENCES sysma."user" (user_id) MATCH SIMPLE
    ON UPDATE CASCADE
    ON DELETE RESTRICT ; 
