/* SYSMA DATA MODEL MIGRATION FILE */;
/* -- Data Model Version from  3.5 -> 3.6 */;
/* -- Generated on : 2023-03-02 13:03:38.102782+01 */;
INSERT INTO  sysma.versions_schema (version_schema_num,version_schema_comment ) VALUES (3.6, 'ADD offline');
;
-- SYSMA OFFLINE : backup offline datas
-- Table metadata
create table sysma.backup_import_metadata (
backup_import_metadata_id bigserial primary key,
created_by bigint not null,
backup_import_metadata_comment text,
backup_import_metadata_log text,
created_at timestamp without time zone DEFAULT now() ,
    CONSTRAINT fk_backup_import_metadata_created_by FOREIGN KEY (created_by)
        REFERENCES sysma."user" (user_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE RESTRICT
	);


-- Table metadata

CREATE TABLE sysma.backup_import_data (
backup_import_data_id bigserial primary key,
backup_import_metadata_id bigint ,
backup_import_data_origin character varying (255), -- exemple :layer-import-geojson
backup_import_data_type character varying (255), -- exmple save_all
backup_import_data jsonb,
created_by bigint not null,
backup_import_data_comment text,
backup_import_data_log text,
created_at timestamp without time zone DEFAULT now() ,
    CONSTRAINT fk_backup_import_data_backup_import_metadata_id FOREIGN KEY (backup_import_metadata_id)
        REFERENCES sysma."backup_import_metadata" (backup_import_metadata_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE RESTRICT,
	CONSTRAINT fk_backup_import_data_backup_created_by FOREIGN KEY (created_by)
        REFERENCES sysma."user" (user_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE RESTRICT
	);

;
