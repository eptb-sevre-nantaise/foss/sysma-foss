<?php
/*
v02 configure with conf_process agrs.
*/
function get_watershed_area_from_linegeom_v02(
    $app,
    $sysma_object_id,
    $calculation_date,
    $stream_schemaname = 'm060_milieux_continuite_tetes_de_bv',
    $stream_tablename_4000 = 't_mnt_sc_sk_stream_4000' ,
    $stream_tablename_500 = 't_mnt_sc_sk_stream_500',
    $stream_tablename_50 = 't_mnt_sc_sk_stream_50'
) {

    $qry = "SELECT
    sysma_object_id,(
    xx_99_utils.f_reseau_hydro_get_oriented_ls_val_from_theo_stream_table (
    _geom := geom::geometry --geometrie linetrsing oriented
    , _stream_schemaname := :schemaName::NAME --nom du schema de la table contenant le réseau hydro theo
    , _stream_tablename := :tableName::NAME  --nom de la table contenant le réseau hydro theo
    , _stream_geomname := 'wkb_geometry'::NAME --nom de la colonne geom du réseau hydro theo
    , _stream_pkname := 'ogc_fid' --nom de la colonne primary key
    , _stream_valuename := 'flow_accum_km2' --nom de la colonne de value a extraire
    , _args := 'find_dist=100 cover_buffer=5 value_factor=1 stream_rankname=shreve'::TEXT --[optionnel] : suffixe qui sera ajouté aux noms des colonnes ajoutées
    , _debug_mode := FALSE --[optionnel] : mode débug TRUE / FALSE
    )).val as surface_bv
    from sysma.sysma_object where sysma_object_id = :id";

    $sttmt = $app['pdo']->prepare($qry);
    $sttmt->bindParam(':schemaName', $stream_schemaname, PDO::PARAM_STR);
    $sttmt->bindParam(':tableName', $stream_tablename_4000, PDO::PARAM_STR);
    $sttmt->bindParam(':id', $sysma_object_id);
    $sttmt->execute();
    $res = $sttmt->fetch();
    $res['type_analyse'] = 't_mnt_sc_ddtm_sk_stream_4000';

    if (empty($res['surface_bv'])) {



        $qry2 = "SELECT
    sysma_object_id,(
    xx_99_utils.f_reseau_hydro_get_oriented_ls_val_from_theo_stream_table (
    _geom := geom::geometry --geometrie linetrsing oriented
    , _stream_schemaname := :schemaName::NAME --nom du schema de la table contenant le réseau hydro theo
    , _stream_tablename := :tableName::NAME  --nom de la table contenant le réseau hydro theo   
    , _stream_geomname := 'wkb_geometry'::NAME --nom de la colonne geom du réseau hydro theo
    , _stream_pkname := 'ogc_fid'::NAME --nom de la colonne primary key
    , _stream_valuename := 'flow_accum_km2'::NAME --nom de la colonne de value a extraire
    , _args := 'find_dist=30 cover_buffer=5 value_factor=1 stream_rankname=shreve'::TEXT --[optionnel] : suffixe qui sera ajouté aux noms des colonnes ajoutées
    , _debug_mode := FALSE::BOOLEAN --[optionnel] : mode débug TRUE / FALSE
    )).val as surface_bv
    from sysma.sysma_object where sysma_object_id = :id";

        $sttmt2 = $app['pdo']->prepare($qry2);
        $sttmt2->bindParam(':schemaName', $stream_schemaname, PDO::PARAM_STR);
        $sttmt2->bindParam(':tableName', $stream_tablename_500, PDO::PARAM_STR);
        $sttmt2->bindParam(':id', $sysma_object_id);
        $sttmt2->execute();
        $res2 = $sttmt2->fetch();
        $res2['type_analyse'] = 't_mnt_sc_ddtm_sk_stream_500';

        if (empty($res2['surface_bv'])) {


            $qry3 = "SELECT
    sysma_object_id,(
    xx_99_utils.f_reseau_hydro_get_oriented_ls_val_from_theo_stream_table (
    _geom := geom::geometry --geometrie linetrsing oriented
    , _stream_schemaname := :schemaName::NAME --nom du schema de la table contenant le réseau hydro theo
    , _stream_tablename := :tableName::NAME  --nom de la table contenant le réseau hydro theo   , _stream_geomname := 'wkb_geometry'::NAME --nom de la colonne geom du réseau hydro theo
    , _stream_geomname := 'wkb_geometry'::NAME --nom de la colonne geom du réseau hydro theo
    , _stream_pkname := 'ogc_fid'::NAME --nom de la colonne primary key
    , _stream_valuename := 'flow_accum_km2'::NAME --nom de la colonne de value a extraire
    , _args := 'find_dist=50 cover_buffer=5 value_factor=1 stream_rankname=shreve'::TEXT --[optionnel] : suffixe qui sera ajouté aux noms des colonnes ajoutées
    , _debug_mode := FALSE::BOOLEAN --[optionnel] : mode débug TRUE / FALSE
    )).val as surface_bv
    from sysma.sysma_object where sysma_object_id = :id";

            $sttmt3 = $app['pdo']->prepare($qry3);
            $sttmt3->bindParam(':schemaName', $stream_schemaname, PDO::PARAM_STR);
            $sttmt3->bindParam(':tableName', $stream_tablename_50, PDO::PARAM_STR);
            $sttmt3->bindParam(':id', $sysma_object_id);
            $sttmt3->execute();
            $res3 = $sttmt3->fetch();
            $res3['type_analyse'] = 't_mnt_sc_sk_stream_v_50';

            if (empty($res3['surface_bv'])) {


                $qry4 = "SELECT
    sysma_object_id,(
    xx_99_utils.f_reseau_hydro_get_oriented_ls_val_from_theo_stream_table (
    _geom := geom::geometry --geometrie linetrsing oriented
    , _stream_schemaname := :schemaName::NAME --nom du schema de la table contenant le réseau hydro theo
    , _stream_tablename := :tableName::NAME  --nom de la table contenant le réseau hydro theo   , _stream_geomname := 'wkb_geometry'::NAME --nom de la colonne geom du réseau hydro theo
    , _stream_geomname := 'wkb_geometry'::NAME --nom de la colonne geom du réseau hydro theo
    , _stream_pkname := 'ogc_fid'::NAME --nom de la colonne primary key
    , _stream_valuename := 'flow_accum_km2'::NAME --nom de la colonne de value a extraire
    , _args := 'find_dist=30 cover_buffer=30 value_factor=1 stream_rankname=shreve use_ratio_stream_length=TRUE'::TEXT --[optionnel] : suffixe qui sera ajouté aux noms des colonnes ajoutées
    , _debug_mode := FALSE::BOOLEAN --[optionnel] : mode débug TRUE / FALSE
    )).val as surface_bv
    from sysma.sysma_object where sysma_object_id = :id";

                $sttmt4 = $app['pdo']->prepare($qry4);
                $sttmt4->bindParam(':schemaName', $stream_schemaname, PDO::PARAM_STR);
                $sttmt4->bindParam(':tableName', $stream_tablename_50, PDO::PARAM_STR);
                $sttmt4->bindParam(':id', $sysma_object_id);
                $sttmt4->execute();
                $res4 = $sttmt4->fetch();
                $res4['type_analyse'] = $stream_tablename_50 .' - use_ratio_stream_length=TRUE';

                return $res4;

                /*
                  if (empty($res4['surface_bv'])) {


                  $qry5 = 'SELECT
                  sysma_object_id,(
                  xx_99_utils.f_reseau_hydro_get_oriented_ls_val_from_theo_stream_table (
                  _geom := geom::geometry --geometrie linetrsing oriented
                  , _stream_schemaname := \'m060_milieux_continuite_tetes_de_bv\' --nom du schema de la table contenant le réseau hydro theo
                  , _stream_tablename := \'t_mnt_sc_sk_stream_v_50\' --nom de la table contenant le réseau hydro theo t_mnt_sc_ddtm_sk_stream_500
                  , _stream_geomname := \'wkb_geometry\' --nom de la colonne geom du réseau hydro theo
                  , _stream_pkname := \'ogc_fid\' --nom de la colonne primary key
                  , _stream_valuename := \'flow_accum_km2\' --nom de la colonne de value a extraire
                  , _args := \'find_dist=30 cover_buffer=30 value_factor=1 stream_rankname=shreve use_ratio_stream_length=TRUE\'::TEXT --[optionnel] : suffixe qui sera ajouté aux noms des colonnes ajoutées
                  , _debug_mode := FALSE --[optionnel] : mode débug TRUE / FALSE
                  )).val as surface_bv
                  from sysma.sysma_object where sysma_object_id = :id';

                  $sttmt5 = $app['pdo']->prepare($qry5);
                  $sttmt5->bindParam(':id', $sysma_object_id);
                  $sttmt5->execute();
                  $res5 = $sttmt5->fetch();
                  $res5['type_analyse'] = 't_mnt_sc_sk_stream_v_50 - use_ratio_stream_length=TRUE - cover_buffer:30';

                  return $res5;
                  } else {
                  return $res4;
                  } */
            } else {
                return $res3;
            }
        } else {
            return $res2;
        }
    } else {
        return $res;
    }
}
