<?php

/*
* Description : Process de calcul des indicateurs lit mineur  
* Methodology : Méthode TBV adapté localement par Eaux & Vilaine dans le cadre des inventaires d'état des lieux
* Author : A. Coudart - Eaux & Vilaine sur la base du travail de S. Renou et A. Rivière 
* Date : 01/2024
* Version : v.1
*/ 


function degre_irregularite_berges($app, $calculation_date, $sysma_object_id)
{


    $CEM = new SysmaObjectParameterValue();
    $CEM->buildFromParameters($sysma_object_id, 75656867, $calculation_date, $app);

    $Erosion = new SysmaObjectParameterValue();
    $Erosion->buildFromParameters($sysma_object_id, 127847986, $calculation_date, $app);


    if (substr($CEM->value,0,8) == 'Classe I' or substr($CEM->value,0,9) == 'Classe VI') {
        return 'Irrégularités importantes';
    } else {

        if ($Erosion->value == 'Nulle')
            return 'Parois lisses';
        if ($Erosion->value == 'Faible')
            return 'Irrégularités légères';
        if ($Erosion->value == 'Moyenne')
            return 'Irrégularités modérées';
        if ($Erosion->value == 'Forte')
            return 'Irrégularités importantes';
    }
}

function n0_materiaux_berge($app, $calculation_date, $sysma_object_id)
{


    $MB = new SysmaObjectParameterValue();
    $MB->buildFromParameters($sysma_object_id, 75656882, $calculation_date, $app);

    if ($MB->value == 'Béton')
        return (0.012 + 0.018) / 2;
    if ($MB->value == 'Terre-Argile')
        return (0.025 + 0.032) / 2;
    if ($MB->value == 'Sable')
        return (0.026 + 0.035) / 2;
    if ($MB->value == 'Graviers')
        return (0.028 + 0.035) / 2;
    if ($MB->value == 'Galets')
        return (0.030 + 0.050) / 2;
    if ($MB->value == 'Blocs')
        return (0.040 + 0.070) / 2;
}

function n1_degre_irregularite($app, $calculation_date, $sysma_object_id)
{


    $V = degre_irregularite_berges($app, $calculation_date, $sysma_object_id);

    if ($V == 'Parois lisses')
        return 0;
    if ($V == 'Irrégularités légères')
        return (0.001 + 0.005) / 2;
    if ($V == 'Irrégularités modérées')
        return (0.006 + 0.010) / 2;
    if ($V == 'Irrégularités importantes')
        return (0.011 + 0.020) / 2;
}

function n2_variation_section($app, $calculation_date, $sysma_object_id)
{

    $MB = new SysmaObjectParameterValue();
    $MB->buildFromParameters($sysma_object_id, 75656866, $calculation_date, $app);

    if ($MB->value == 'Progressive')
        return 0;
    if ($MB->value == 'Alternant occasionnellement')
        return (0.001 + 0.005) / 2;
    if ($MB->value == 'Alternant fréquemment')
        return (0.010 + 0.015) / 2;
}

function n3_quantite_elements_naturels($app, $calculation_date, $sysma_object_id)
{

    $MB = new SysmaObjectParameterValue();
    $MB->buildFromParameters($sysma_object_id, 75656869, $calculation_date, $app);

    if ($MB->value == 'Négligeable')
        return (0 + 0.004) / 2;
    if ($MB->value == 'Faible')
        return (0.005 + 0.015) / 2;
    if ($MB->value == 'Sensible')
        return (0.020 + 0.030) / 2;
    if ($MB->value == 'Très marqué')
        return (0.040 + 0.050) / 2;
}

function n4_quantite_vegetation_lit($app, $calculation_date, $sysma_object_id)
{

    $MB = new SysmaObjectParameterValue();
    $MB->buildFromParameters($sysma_object_id, 75656872, $calculation_date, $app);

    if ($MB->value == 'Négligeable')
        return 0;
    if ($MB->value == 'Faible')
        return (0.002 + 0.010) / 2;
    if ($MB->value == 'Moyenne')
        return (0.010 + 0.025) / 2;
    if ($MB->value == 'Importante')
        return (0.025 + 0.050) / 2;
    if ($MB->value == 'Très importante')
        return (0.050 + 0.100) / 2;
}

function m_sinuosite($app, $calculation_date, $sysma_object_id)
{

    $MB = new SysmaObjectParameterValue();
    $MB->buildFromParameters($sysma_object_id, 75656865, $calculation_date, $app);

    if ($MB->value == 'Rectiligne')
        return 1;
    if ($MB->value == 'Sinuosité réduite par rapport à la sinuosité naturelle')
        return 1.15;
    if ($MB->value == 'Tracé naturel')
        return 1.3;
}

function k_rugosite($app, $calculation_date, $sysma_object_id)
{

    $n0 = n0_materiaux_berge($app, $calculation_date, $sysma_object_id);
    $n1 = n1_degre_irregularite($app, $calculation_date, $sysma_object_id);
    $n2 = n2_variation_section($app, $calculation_date, $sysma_object_id);
    $n3 = n3_quantite_elements_naturels($app, $calculation_date, $sysma_object_id);
    $n4 = n4_quantite_vegetation_lit($app, $calculation_date, $sysma_object_id);
    $m = m_sinuosite($app, $calculation_date, $sysma_object_id);
   
    $K = 1 / (($n0 + $n1 + $n2 + $n3 + $n4) * $m);

    return ['K' => $K, 'n0' => $n0, 'n1' => $n1, 'n2' => $n2, 'n3' => $n3, 'n4' => $n4, 'm' => $m];
}

function k_classe_rugosite($k)
{

    if ($k == null)
        return null;
    if ($k < 20)
        return 'Coefficient de rugosité K entre 5 et 20';
    if ($k >= 20 and $k < 30)
        return 'Coefficient de rugosité K entre 20 et 30';
    if ($k >= 30)
        return 'Coefficient de rugosité K > 30';
    return null;
}

function surface_plein_bord($hauteur, $lpb, $lbase)
{

    $surf = $hauteur * ($lpb + $lbase) / 2;
    return $surf;
}


function perimetre_plein_bord($hauteur, $lpb, $lbase)
{

    $ppb = 2 * sqrt(pow((($lpb - $lbase) / 2), 2) + pow($hauteur, 2)) + $lbase;
    return $ppb;
}

function rayon_hydraulique($ppb, $spb)
{

    return $spb / $ppb;
}

function indice_artificialisation_lit_mineur($app, $calculation_date, $sysma_object_id, $classe_K)
{


    $T = new SysmaObjectParameterValue();
    // talweg
    $T->buildFromParameters($sysma_object_id, 75656881, $calculation_date, $app);
    $talweg = null;
    if ($T->value == 'Dans le talweg') {
        $talweg = 0;
    } elseif ($T->value == 'Hors du talweg') {
        $talweg = 1;
    }
    //sinuosité
    $T->buildFromParameters($sysma_object_id, 75656865, $calculation_date, $app);
    $sinuosite = null;
    if ($T->value == 'Rectiligne') {
        $sinuosite = 1;
    } elseif ($T->value == 'Sinuosité réduite par rapport à la sinuosité naturelle') {
        $sinuosite = 0.5;
    } elseif ($T->value == 'Tracé naturel') {
        $sinuosite = 0;
    }
    // CEM
    $T->buildFromParameters($sysma_object_id, 75656867, $calculation_date, $app);
    $cem = null;
    if ($T->value == 'Classe I : Absence d’altération') {
        $cem = 0;
    } elseif ($T->value == 'Classe VI : Quasi-équilibre') {
        $cem = 0.5;
    } elseif ($T->value == 'Classe II : Chenalisé') {
        $cem = 1;
    }
    elseif ($T->value == 'Classe III : Incisé') {
        $cem = 1;
    }
    elseif ($T->value == 'Classe IV : Incisé et élargi') {
        $cem = 1;
    }
    elseif ($T->value == 'Classe V : Accumulation et élargissement') {
        $cem = 1;
    }
    // rugosité

    $rug = null;
    if ($classe_K == 'Coefficient de rugosité K entre 5 et 20') {
        $rug = 0;
    } elseif ($classe_K == 'Coefficient de rugosité K entre 20 et 30') {
        $rug = 0.5;
    } elseif ($classe_K == 'Coefficient de rugosité K > 30') {
        $rug = 1;
    }
    // protec berge
    $T->buildFromParameters($sysma_object_id, 75656883, $calculation_date, $app);
    $pb = null;
    if ($T->value == 'Absence de protection de berges') {
        $pb = 0;
    } elseif ($T->value == 'Présence de protection de berges de type génie végétal') {
        $pb = 0.25;
    } elseif ($T->value == 'Présence de protection de berges sur les berges du lit mineur') {
        $pb = 0.5;
    } elseif ($T->value == 'Présence de protection de berges sur les berges et le fond du lit mineur') {
        $pb = 0.75;
    } elseif ($T->value == '100% d\'enterrement (par couverture, busage, drainage ou comblement)') {
        $pb = 1;
    }

    $ia = null;
    
    // Check if any of the variables is null before performing the calculation
    if ($talweg !== null && $sinuosite !== null && $cem !== null && $rug !== null) {
        $ia = ($talweg + ($sinuosite * 2) + ($cem * 2) + $rug + $pb) / 7;
    }

    return ['ia' => $ia, 'i_talweg' => $talweg, 'i_sinuosite' => $sinuosite, 'i_cem' => $cem, 'i_rugosite' => $rug, 'i_protection_berge' => $pb];
}

function indice_artificialisation_lit_mineur_classe($ia)
{
    if ($ia === null) {
        return 'Non calculé';
    } elseif ($ia == 0) {
        return 'Cours d\'eau de référence';
    } elseif ($ia > 0 and $ia <= 0.25) {
        return 'Cours d\'eau naturel';
    } elseif ($ia > 0.25 and $ia <= 0.50) {
        return 'Cours d\'eau semi-artificiel';
    } elseif ($ia > 0.50 and $ia <= 0.75) {
        return 'Cours d\'eau artificiel';
    } elseif ($ia > 0.75 and $ia < 1) {
        return 'Cours d\'eau très artificiel';
    } elseif ($ia == 1) {
        return 'Cours d\'eau enterré ou plan d\'eau';
    }
}

function lit_mineur_calculations(
    $app,
    $sysma_object_id,
    $calculation_date
) {


    $TypeSeq = new SysmaObjectParameterValue();
    $TypeSeq->buildFromParameters($sysma_object_id, 75656875, $calculation_date, $app);
    if ($TypeSeq->value == 'Cours d\'eau enterré (supérieur à 10m)' or $TypeSeq->value == 'Plan d’eau en barrage') {


        $res = [
            'type_sequence' => $TypeSeq->value,
            'indice_artificialisation_note' => 1,
            'indice_artificialisation_classe' => indice_artificialisation_lit_mineur_classe(1),
        ];

        return $res;
    }

    $Hauteur = new SysmaObjectParameterValue();
    $Hauteur->buildFromParameters($sysma_object_id, 75656860, $calculation_date, $app);
    $hauteur = $Hauteur->value;
    $Lpb = new SysmaObjectParameterValue();
    $Lpb->buildFromParameters($sysma_object_id, 75656849, $calculation_date, $app);
    $lpb = $Lpb->value;
    $Lbase = new SysmaObjectParameterValue();
    $Lbase->buildFromParameters($sysma_object_id, 75656870, $calculation_date, $app);
    $lbase = $Lbase->value;

    $k = k_rugosite($app, $calculation_date, $sysma_object_id);

    $spb = surface_plein_bord($hauteur, $lpb, $lbase);
    $ppb = perimetre_plein_bord($hauteur, $lpb, $lbase);
    $rh = rayon_hydraulique($ppb, $spb);

    $ia = indice_artificialisation_lit_mineur($app, $calculation_date, $sysma_object_id, k_classe_rugosite($k['K']));

    if ($TypeSeq->value == 'Réseau diffus') {
        $ia['ia'] = 0;
    }

    $res = [
        'type_sequence' => $TypeSeq->value,
        'degres_irregularite_berge' => degre_irregularite_berges($app, $calculation_date, $sysma_object_id),
        'n0' => $k['n0'],
        'n1' => $k['n1'],
        'n2' => $k['n2'],
        'n3' => $k['n3'],
        'n4' => $k['n4'],
        'm' => $k['m'],
        'K' => $k['K'],
        'classe_K' => k_classe_rugosite($k['K']),
        'surface_plein_bord' => $spb,
        'perimetre_plein_bord' => $ppb,
        'rayon_hydraulique' => $rh,
        'indice_talweg' => $ia['i_talweg'],
        'indice_sinuosite' => $ia['i_sinuosite'],
        'indice_cem' => $ia['i_cem'],
        'indice_rugosite' => $ia['i_rugosite'],
        'indice_protection_berge' => $ia['i_protection_berge'],
        'indice_artificialisation_note' => $ia['ia'],
        'indice_artificialisation_classe' => indice_artificialisation_lit_mineur_classe($ia['ia']),
    ];

    return $res;
}
