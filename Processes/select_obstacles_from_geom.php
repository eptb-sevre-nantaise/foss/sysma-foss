<?php

function select_obstacles_from_geom($app, $sysma_object_id, $calculation_date, $buffer = 10)
{


    $res = null;

    if ($sysma_object_id == null or $calculation_date == null or $buffer == null)
        die('Paramètres manquants');
    if (!isADate($calculation_date))
        die('La date fournie est incorrecte ' . $calculation_date);


    $sommeHC = 0;
    $nbOuvragesPrimaires = 0;
    $tabObs = $tabObsPrim = array();
    $pdo = $app['pdo'];


    $iab = "select * from " . SCHEMA . ".sysma_object where "
        . "sysma_object_type_id = 46 and " // type objet
        . "(sysma_object.end_date > :date or sysma_object.end_date = '0') and " // date de fin postérieure à la date de calcul (ou nulle)
        . "(sysma_object.start_date < :date  or sysma_object.start_date = '0') and " // date de fin postérieure à la date de calcul (ou nulle)
        . "ST_Intersects(ST_Buffer((select o.geom from " . SCHEMA . ".sysma_object o where o.sysma_object_id = :sysma_object_id),:buffer),geom)"; // intersection segment avec tampon de  100 mètres 

    $qryob = $pdo->prepare($iab);
    $qryob->bindParam(':sysma_object_id', $sysma_object_id);
    $qryob->bindParam(':date', $calculation_date);
    $qryob->bindParam(':buffer', $buffer);

    $qryob->execute() or die();


    while ($Ob = $qryob->fetchObject()) {


        $V = new SysmaObjectParameterValue();
        $V->buildFromParameters($Ob->sysma_object_id, 243, $calculation_date, $app);

        if ($V->value == 'primaire') {

            $V2 = new SysmaObjectParameterValue();
            $V2->buildFromParameters($Ob->sysma_object_id, 257, $calculation_date, $app);


            $tabObsPrim[] = $Ob->sysma_object . ' [<a target="_blank" href="/carte/objet/' . $Ob->sysma_object_id . '">' . $Ob->sysma_object_id . '</a>]';

            $sommeHC += floatval($V2->value);
        }

        $tabObs[] = $Ob->sysma_object . ' [<a target="_blank" href="/carte/objet/' . $Ob->sysma_object_id . '">' . $Ob->sysma_object_id . '</a>]';
    }

    $res['Liste des obstacles'] = implode(',', $tabObs);
    $res['nb_obstacl'] = $qryob->rowCount();
    $res['Liste des obstacles primaires'] = implode(',', $tabObsPrim);
    $res['nb_obsprim'] = count($tabObsPrim);
    $res['liste_obsprim'] = implode(',', $tabObsPrim);
    $res['haut_chut'] = $sommeHC;


    return $res;
}
