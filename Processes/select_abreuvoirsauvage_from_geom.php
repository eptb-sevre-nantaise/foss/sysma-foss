<?php



/*
 * calcule le nombre d'objet de type $id_object_type dans un tampon de  $buffer mètres autour du segment $sysma_object_id pour la $calculation_date précisée.
 * enregistre la value dans l'objet $sysma_object_id avec le paramètre $parameter_id (et archives l'ancienne) si $rec est vrai
 */

function select_abreuvoirsauvage_from_geom($app, $sysma_object_id, $calculation_date, $buffer = 100)
{


    if ($sysma_object_id == null or $calculation_date == null or $buffer == null)
        die('Paramètres manquants');
    if (!isADate($calculation_date))
        die('La date fournie est incorrecte ' . $calculation_date);

    $f = null;

    $pdo = $app['pdo'];


    //  $f .= '<h1>Segment  ' . $sseg['sysma_object'] . ' <small>[' . $sysma_object_id . ']</small></h1>';
    // sélection des abreuvoirs sauvages qui intersectent un tampon de x mètres autour des segments reh dont la date de fin est postérieure à la date de calcul et la date de début antérieur (ou bien 0 en date de début / fin)

    $iab = "select * from " . SCHEMA . ".sysma_object where "
        . "sysma_object_type_id = 41  and " // type abreuvoir sauvage
        . "(end_date > :date or end_date = '0') and " // date de fin postérieure à la date de calcul (ou nulle)
        . "(start_date <=  :date or start_date = '0') and " // date de fin postérieure à la date de calcul (ou nulle)
        . "ST_Intersects(ST_Buffer((select o.geom from " . SCHEMA . ".sysma_object o where o.sysma_object_id = :sysma_object_id),:buffer),geom)"; // intersection segment avec tampon de  100 mètres 

    $qryob = $pdo->prepare($iab);
    $qryob->bindParam(':sysma_object_id', $sysma_object_id);
    $qryob->bindParam(':date', $calculation_date);
    $qryob->bindParam(':buffer', $buffer);

    if ($qryob->execute() == false) {
        if (DEBUG) {
            die(pg_last_error());
        } else {
            die("Erreur SQL. Veuillez créer un rapport de bug pour l'équipe de developpement de Sysma-FOSS <a href=https://gitlab.sevre-nantaise.com/eptbsn/sysma-foss/-/issues>ici</a>");
        }
    }


    $res['nb_ab_sauv'] = $qryob->rowCount();
    return $res;
}
