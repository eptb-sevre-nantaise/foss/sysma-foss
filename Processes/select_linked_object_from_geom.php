<?php

function select_linked_object_from_geom(
    $app,
    $sysma_object_id,
    $calculation_date,
    $linked_geoobjecttype_id,
    $linked_parameter_id,
    $buffer = 1
) {


    $qry = 'select seq.sysma_object_id 
        from sysma.sysma_object seq         
        where 
        st_within(
            st_PointOnSurface(st_buffer(seq.geom,0.1)), 
            (select st_buffer(troncon.geom,:buffer) from sysma.sysma_object troncon where troncon.sysma_object_id = :sysma_object_id)
            ) 
        and seq.sysma_object_type_id = :sysma_object_type_id';

    $pdo = $app['pdo'];
    $sttmt = $pdo->prepare($qry);
    $sttmt->bindParam(':sysma_object_id', $sysma_object_id);
    $sttmt->bindParam(':sysma_object_type_id', $linked_geoobjecttype_id);
    $buffer = floatval($buffer);
    $sttmt->bindParam(':buffer', $buffer);
    $sttmt->execute();

    $res = $sttmt->fetchAll(PDO::FETCH_ASSOC);

    // var_dump(['ids_sysma_object_list' => $res]) ;
    return ['ids_sysma_object_list' => $res];
}
