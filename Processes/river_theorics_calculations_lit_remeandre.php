<?php

function degre_irregularite_berges($app, $calculation_date, $sysma_object_id)
{


    $CEM = new SysmaObjectParameterValue();
    $CEM->buildFromParameters($sysma_object_id, 962626, $calculation_date, $app);

    $Erosion = new SysmaObjectParameterValue();
    $Erosion->buildFromParameters($sysma_object_id, 962604, $calculation_date, $app);


    if ($CEM->value == 'Classe 1 - Absence' or $CEM->value == 'Classe 6 - Quasi-équilibre') {
        return 'Irrégularités importantes';
    } else {

        if ($Erosion->value == 'Nulle')
            return 'Parois lisses';
        if ($Erosion->value == 'Faible')
            return 'Irrégularités légères';
        if ($Erosion->value == 'Moyenne')
            return 'Irrégularités modérées';
        if ($Erosion->value == 'Forte')
            return 'Irrégularités importantes';
    }
}

function n0_materiaux_berge($app, $calculation_date, $sysma_object_id)
{


    $MB = new SysmaObjectParameterValue();
    $MB->buildFromParameters($sysma_object_id, 962619, $calculation_date, $app);

    if ($MB->value == 'Béton')
        return (0.012 + 0.018) / 2;
    if ($MB->value == 'Terre')
        return (0.025 + 0.032) / 2;
    if ($MB->value == 'Sable')
        return (0.026 + 0.035) / 2;
    if ($MB->value == 'Graviers')
        return (0.028 + 0.035) / 2;
    if ($MB->value == 'Galets')
        return (0.030 + 0.050) / 2;
    if ($MB->value == 'Blocs rocheux')
        return (0.040 + 0.070) / 2;
}

function n1_degre_irregularite($app, $calculation_date, $sysma_object_id)
{


    $V = degre_irregularite_berges($app, $calculation_date, $sysma_object_id);

    if ($V == 'Parois lisses')
        return 0;
    if ($V == 'Irrégularités légères')
        return (0.001 + 0.005) / 2;
    if ($V == 'Irrégularités modérées')
        return (0.006 + 0.010) / 2;
    if ($V == 'Irrégularités importantes')
        return (0.011 + 0.020) / 2;
}

function n2_variation_section($app, $calculation_date, $sysma_object_id)
{

    $MB = new SysmaObjectParameterValue();
    $MB->buildFromParameters($sysma_object_id, 962633, $calculation_date, $app);

    if ($MB->value == 'Progressive')
        return 0;
    if ($MB->value == 'Alternant occasionnellement')
        return (0.001 + 0.005) / 2;
    if ($MB->value == 'Alternant fréquemment')
        return (0.010 + 0.015) / 2;
}

function n3_quantite_elements_naturels($app, $calculation_date, $sysma_object_id)
{

    $MB = new SysmaObjectParameterValue();
    $MB->buildFromParameters($sysma_object_id, 962578, $calculation_date, $app);

    if ($MB->value == 'Négligeable')
        return (0 + 0.004) / 2;
    if ($MB->value == 'Faible')
        return (0.005 + 0.015) / 2;
    if ($MB->value == 'Sensible')
        return (0.020 + 0.030) / 2;
    if ($MB->value == 'Très marqué')
        return (0.040 + 0.050) / 2;
}

function n4_quantite_vegetation_lit($app, $calculation_date, $sysma_object_id)
{

    $MB = new SysmaObjectParameterValue();
    $MB->buildFromParameters($sysma_object_id, 962946, $calculation_date, $app);

    if ($MB->value == 'Négligeable')
        return 0;
    if ($MB->value == 'Faible')
        return (0.002 + 0.010) / 2;
    if ($MB->value == 'Moyenne')
        return (0.010 + 0.025) / 2;
    if ($MB->value == 'Importante')
        return (0.025 + 0.050) / 2;
    if ($MB->value == 'Très importante')
        return (0.050 + 0.100) / 2;
}

function m_sinuosite($app, $calculation_date, $sysma_object_id)
{

    $MB = new SysmaObjectParameterValue();
    $MB->buildFromParameters($sysma_object_id, 962952, $calculation_date, $app);

    if ($MB->value == 'Rectiligne')
        return 1;
    if ($MB->value == 'Sinuosité réduite par rapport à la sinuosité naturelle')
        return 1;
    if ($MB->value == 'Tracé naturel : Rectiligne naturel')
        return 1;
    if ($MB->value == 'Tracé naturel : Sinueux')
        return 1.06;
    if ($MB->value == 'Tracé naturel : Très sinueux')
        return 1.35;
    if ($MB->value == 'Tracé naturel : Méandriforme')
        return 1.72;
}

function k_rugosite($app, $calculation_date, $sysma_object_id)
{

    $n0 = n0_materiaux_berge($app, $calculation_date, $sysma_object_id);
    $n1 = n1_degre_irregularite($app, $calculation_date, $sysma_object_id);
    $n2 = n2_variation_section($app, $calculation_date, $sysma_object_id);
    $n3 = n3_quantite_elements_naturels($app, $calculation_date, $sysma_object_id);
    $n4 = n4_quantite_vegetation_lit($app, $calculation_date, $sysma_object_id);
    $m = m_sinuosite($app, $calculation_date, $sysma_object_id);
    /*
      echo 'n0 : ' . $n0 . '<br>';
      echo 'n1 : ' . $n1 . '<br>';
      echo 'n2 : ' . $n2 . '<br>';
      echo 'n3 : ' . $n3 . '<br>';
      echo 'n4 : ' . $n4 . '<br>';
      echo 'm : ' . $m . '<br>';
     */
    $K = 1 / (($n0 + $n1 + $n2 + $n3 + $n4) * $m);

    return ['K' => $K, 'n0' => $n0, 'n1' => $n1, 'n2' => $n2, 'n3' => $n3, 'n4' => $n4, 'm' => $m];
}

function k_classe_rugosite($k)
{

    if ($k == null)
        return null;
    if ($k < 20)
        return 'Coefficient de rugosité K entre 5 et 20';
    if ($k >= 20 and $k < 30)
        return 'Coefficient de rugosité K entre 20 et 30';
    if ($k >= 30)
        return 'Coefficient de rugosité K > 30';
    return null;
}

function surface_plein_bord($hauteur, $lpb, $lbase)
{

    // hauteur totale * (largeur plein bord + largeur base) / 2
    $surf = $hauteur * ($lpb + $lbase) / 2;
    return $surf;
}

/*
 * Périmètre mouillé pour la hauteur plein bord :
 * Ppb = 2 * √( ( (l_pb - l_base)/2)² + hauteur_pb² ) + l_base
 */

function perimetre_plein_bord($hauteur, $lpb, $lbase)
{

    $ppb = 2 * sqrt(pow((($lpb - $lbase) / 2), 2) + pow($hauteur, 2)) + $lbase;
    return $ppb;
}

function rayon_hydraulique($ppb, $spb)
{

    return $spb / $ppb;
}

/*
 * MANNING STRICKLER
 * Debit_Vu = (coef_rugosite)*(Surface_Vu)*(Rayon_hydraulique_Vu)^(2/3)*Racine(pente))
 */

function debit_plein_bord($k, $spb, $rh, $pente)
{


    $dpb = $k * $spb * pow($rh, (2 / 3)) * sqrt($pente);
    return $dpb;
}

function puissance($dpb, $pente, $lpb)
{

    if ($pente == null)
        return null;
    return $dpb * $pente * 9810 / $lpb;
}

function river_theorics_calculations_lit_remeandre(
    $app,
    $sysma_object_id,
    $calculation_date,
    $surf_bv_ref,
    $q2_bv_ref
) {

    $Objet = new SysmaObject($sysma_object_id, $app);
    $Objet->loadActualInfosSysmaObjectWithIndex();


    $Hauteur = new SysmaObjectParameterValue();
    $Hauteur->buildFromParameters($sysma_object_id, 962601, $calculation_date, $app);
    $hauteur = $Hauteur->value;
    $Lpb = new SysmaObjectParameterValue();
    $Lpb->buildFromParameters($sysma_object_id, 962603, $calculation_date, $app);
    $lpb = $Lpb->value;
    $Lbase = new SysmaObjectParameterValue();
    $Lbase->buildFromParameters($sysma_object_id, 962602, $calculation_date, $app);
    $lbase = $Lbase->value;

    $k = k_rugosite($app, $calculation_date, $sysma_object_id);

    $Pente = new SysmaObjectParameterValue();
    $Pente->buildFromParameters($sysma_object_id, 966565, $calculation_date, $app);
    $pente = $Pente->value;


    $spb = surface_plein_bord($hauteur, $lpb, $lbase);
    $ppb = perimetre_plein_bord($hauteur, $lpb, $lbase);
    $rh = rayon_hydraulique($ppb, $spb);

    $dpb = debit_plein_bord($k['K'], $spb, $rh, $pente);
    $puissance = puissance($dpb, $pente, $lpb);


    $q2_extrapo = $q2_bv_ref * pow(($Objet->dataIndex['surface_bv']->value / $surf_bv_ref), 0.8);
    $lpb_theo = 1.44 * pow($Objet->dataIndex['surface_bv']->value, 0.41);
    $puis_extra = ($Objet->dataIndex['pente']->value * 9810) / $lpb_theo;
    $prof_theo = 0.41 * pow($Objet->dataIndex['surface_bv']->value, 0.25);
    $rap_lp_the = 3.56 * pow($Objet->dataIndex['surface_bv']->value, 0.16);
    $surfm_theo = 0.4 * pow($Objet->dataIndex['surface_bv']->value, 0.68);
    $largb_theo = 2 * ($surfm_theo / $prof_theo) - $lpb_theo;
    $permo_theo = 2 * sqrt(0.5 * ($lpb_theo - $largb_theo) * 0.5 * ($lpb_theo - $largb_theo) + $prof_theo * $prof_theo) + $largb_theo;

    $k['K'] != null ? $q2_theo = $surfm_theo * pow($surfm_theo / $permo_theo, 2 / 3) * sqrt($Objet->dataIndex['pente']->value) * $k['K'] : $q2_theo = null;
    $q2_theo != null ? $puis_theo = ($Objet->dataIndex['pente']->value * $q2_theo * 9810) / $lpb_theo : $puis_theo = null;



    $res = [
        'degres_irregularite_berge' => degre_irregularite_berges($app, $calculation_date, $sysma_object_id),
        'n0' => $k['n0'],
        'n1' => $k['n1'],
        'n2' => $k['n2'],
        'n3' => $k['n3'],
        'n4' => $k['n4'],
        'm' => $k['m'],
        'K' => $k['K'],
        'classe_K' => k_classe_rugosite($k['K']),
        'surface_plein_bord' => $spb,
        'perimetre_plein_bord' => $ppb,
        'rayon_hydraulique' => $rh,
        'pente' => $pente,
        'debit_plein_bord' => $dpb,
        'puissance' => $puissance,
        'q2_extrapo' => $q2_extrapo,
        'lpb_theo' => $lpb_theo,
        'prof_theo' => $prof_theo,
        'rap_lp_the' => $rap_lp_the,
        'surfm_theo' => $surfm_theo,
        'largb_theo' => $largb_theo,
        'permo_theo' => $permo_theo,
        'puis_extra' => $puis_extra,
        'q2_theo' => $q2_theo,
        'puis_theo' => $puis_theo,
    ];

    return $res;
}
