<?php
/*
Get elevation and slope f
Use PG Raster and function f_mnt_rast_get_linestring_slope 
This function dont need pg_raster=>vector table (heavy table), so ,
prefer using this function than "altitude_and_slope"

v02 :  use args from conf_processes
*/
function altitude_and_slope_from_rast_v02(
    $app,
    $sysma_object_id,
    $calculation_date,
    $buffer_find_m = 15,

    $mntschemaname = 'r020_territoire_physique',
    $mnttablename = 'r_mnt_5x5_',  // ne pas commiter


    $mntrastername = 'rast',
    $mntaltibande  = '1',   
    $mntaltinodata = '-99999.0000000000',
    $mntrasterdim  = '5', // raster pixel size in meters

    $function_find = 'MIN',
    $slope_rtn_unit = 'mPERm'

) {

  
    //  nouvelle version qui se contente du rater pg
    $qry = 'SELECT sysma_object_id, geom, (xx_99_utils.f_mnt_rast_get_linestring_slope (( 
            \'{  
                "mntschemaname" : "' . $mntschemaname . '",
                "mnttablename" : "' . $mnttablename . '",
                "mntrastername" : "' . $mntrastername . '",
                "mntaltibande" : "' . $mntaltibande . '",
                "mntaltinodata" : "' . $mntaltinodata . '",
                "mntrasterdim" : "' . $mntrasterdim . '",
                "geom" :  "\'|| foo.geom::text ||\'",
                "buffer_find_m" : "' . $buffer_find_m . '",
                "function_find" : "' . $function_find . '",
                "slope_rtn_unit" : "' . $slope_rtn_unit . '" } 
            \')::text::json
            )).* FROM  (select sysma_object_id,  geom from sysma.sysma_object where sysma_object_id = :id ) as foo';


    $sttmt = $app['pdo']->prepare($qry);
    $sttmt->bindParam(':id', $sysma_object_id);
    $sttmt->execute();

    $res = $sttmt->fetchAll(PDO::FETCH_ASSOC);

    return $res[0];
}
