<?php

function select_plansdeau_from_geom($app, $sysma_object_id, $calculation_date, $linked_parameter_id, $buffer = 20)
{



    $res = null;

    if ($sysma_object_id == null or $calculation_date == null or $buffer == null)
        die('Paramètres manquants');
    if (!isADate($calculation_date))
        die('La date fournie est incorrecte ' . $calculation_date);

    $pdo = $app['pdo'];


    $compteur = 0;


    $iab = "select * from " . SCHEMA . ".sysma_object where "
        . "sysma_object_type_id = 45 and " // type objet
        . "(sysma_object.end_date > :date or sysma_object.end_date = '0') and " // date de fin postérieure à la date de calcul (ou nulle)
        . "(sysma_object.start_date < :date or sysma_object.start_date = '0') and " // date de fin postérieure à la date de calcul (ou nulle)
        . "ST_Intersects(ST_Buffer((select o.geom from " . SCHEMA . ".sysma_object o where o.sysma_object_id = :sysma_object_id),:buffer),geom)"; // intersection segment avec tampon de  100 mètres 

    $qryob = $pdo->prepare($iab);
    $qryob->bindParam(':sysma_object_id', $sysma_object_id);
    $qryob->bindParam(':date', $calculation_date);
    $qryob->bindParam(':buffer', $buffer);



    $qryob->execute() or die();


    while ($Ob = $qryob->fetchObject()) {

        // TERRAIN
        $V = new SysmaObjectParameterValue();
        $V->buildFromParameters($Ob->sysma_object_id, 234, $calculation_date, $app);
        // SIG
        $V2 = new SysmaObjectParameterValue();
        $V2->buildFromParameters($Ob->sysma_object_id, 233, $calculation_date, $app);


        if ($V->value == 'sur cours d&#039;eau' or $V->value == 'sur cours d eau') {
            $compteur++;
        }
        // si l'info terrain est nulle
        elseif ($V->value == null and $V2->value == 1) {
            $compteur++;
        }
    }


    $res['nb total plans eau à proximité'] = $qryob->rowCount();
    $res['nb_pdo_con'] = $compteur;



    return $res;
}
