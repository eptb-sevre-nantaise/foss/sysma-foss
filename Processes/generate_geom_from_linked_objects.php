<?php

function generate_geom_from_linked_objects(
    $app,
    $sysma_object_id,
    $calculation_date,
    $linked_parameter_id,
    $snap_dist = 20
) {


    $qry = 'SELECT xx_99_utils.ST_LinemergeGetLongestPath (o.geom_union, \'snapdist=' . intval($snap_dist) . '\') as union_geom 
from (select st_union(geom) as geom_union from sysma.sysma_object o inner join sysma.sysma_object_data d on d.sysma_object_id = o.sysma_object_id 
where d.parameter_id = ' . $linked_parameter_id . ' and d.value ilike \'%o' . $sysma_object_id . '%\'  ) as o';


    $pdo = $app['pdo'];

    $sttmt = $pdo->query($qry);
    $sttmt->execute();
    $res = $sttmt->fetchAll(PDO::FETCH_ASSOC);

    return $res[0];
}
