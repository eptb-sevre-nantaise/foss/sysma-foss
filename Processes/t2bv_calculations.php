<?php

function degre_irregularite_berges($app, $calculation_date, $sysma_object_id)
{


    $CEM = new SysmaObjectParameterValue();
    $CEM->buildFromParameters($sysma_object_id, 158542, $calculation_date, $app);

    $Erosion = new SysmaObjectParameterValue();
    $Erosion->buildFromParameters($sysma_object_id, 158565, $calculation_date, $app);


    if (substr($CEM->value,0,8) == 'Classe 1' or substr($CEM->value,0,8) == 'Classe 6') {
        return 'Irrégularités importantes';
    } else {

        if ($Erosion->value == 'Nulle')
            return 'Parois lisses';
        if ($Erosion->value == 'Faible')
            return 'Irrégularités légères';
        if ($Erosion->value == 'Moyenne')
            return 'Irrégularités modérées';
        if ($Erosion->value == 'Forte')
            return 'Irrégularités importantes';
    }
}

function n0_materiaux_berge($app, $calculation_date, $sysma_object_id)
{


    $MB = new SysmaObjectParameterValue();
    $MB->buildFromParameters($sysma_object_id, 158551, $calculation_date, $app);

    if ($MB->value == 'Béton')
        return (0.012 + 0.018) / 2;
    if ($MB->value == 'Terre')
        return (0.025 + 0.032) / 2;
    if ($MB->value == 'Sable')
        return (0.026 + 0.035) / 2;
    if ($MB->value == 'Graviers')
        return (0.028 + 0.035) / 2;
    if ($MB->value == 'Galets')
        return (0.030 + 0.050) / 2;
    if ($MB->value == 'Blocs rocheux')
        return (0.040 + 0.070) / 2;
}

function n1_degre_irregularite($app, $calculation_date, $sysma_object_id)
{


    $V = degre_irregularite_berges($app, $calculation_date, $sysma_object_id);

    if ($V == 'Parois lisses')
        return 0;
    if ($V == 'Irrégularités légères')
        return (0.001 + 0.005) / 2;
    if ($V == 'Irrégularités modérées')
        return (0.006 + 0.010) / 2;
    if ($V == 'Irrégularités importantes')
        return (0.011 + 0.020) / 2;
}

function n2_variation_section($app, $calculation_date, $sysma_object_id)
{

    $MB = new SysmaObjectParameterValue();
    $MB->buildFromParameters($sysma_object_id, 158788, $calculation_date, $app);

    if ($MB->value == 'Progressive')
        return 0;
    if ($MB->value == 'Alternant occasionnellement')
        return (0.001 + 0.005) / 2;
    if ($MB->value == 'Alternant fréquemment')
        return (0.010 + 0.015) / 2;
}

function n3_quantite_elements_naturels($app, $calculation_date, $sysma_object_id)
{

    $MB = new SysmaObjectParameterValue();
    $MB->buildFromParameters($sysma_object_id, 158609, $calculation_date, $app);

    if ($MB->value == 'Négligeable')
        return (0 + 0.004) / 2;
    if ($MB->value == 'Faible')
        return (0.005 + 0.015) / 2;
    if ($MB->value == 'Sensible')
        return (0.020 + 0.030) / 2;
    if ($MB->value == 'Très marqué')
        return (0.040 + 0.050) / 2;
}

function n4_quantite_vegetation_lit($app, $calculation_date, $sysma_object_id)
{

    $MB = new SysmaObjectParameterValue();
    $MB->buildFromParameters($sysma_object_id, 158614, $calculation_date, $app);

    if ($MB->value == 'Négligeable')
        return 0;
    if ($MB->value == 'Faible')
        return (0.002 + 0.010) / 2;
    if ($MB->value == 'Moyenne')
        return (0.010 + 0.025) / 2;
    if ($MB->value == 'Importante')
        return (0.025 + 0.050) / 2;
    if ($MB->value == 'Très importante')
        return (0.050 + 0.100) / 2;
}

function m_sinuosite($app, $calculation_date, $sysma_object_id)
{

    $MB = new SysmaObjectParameterValue();
    $MB->buildFromParameters($sysma_object_id, 158530, $calculation_date, $app);

    if ($MB->value == 'Rectiligne')
        return 1;
    if ($MB->value == 'Sinuosité réduite par rapport à la sinuosité naturelle')
        return 1;
    if ($MB->value == 'Tracé naturel : Rectiligne naturel')
        return 1;
    if ($MB->value == 'Tracé naturel : Sinueux')
        return 1.06;
    if ($MB->value == 'Tracé naturel : Très sinueux')
        return 1.35;
    if ($MB->value == 'Tracé naturel : Méandriforme')
        return 1.72;
}

function k_rugosite($app, $calculation_date, $sysma_object_id)
{

    $n0 = n0_materiaux_berge($app, $calculation_date, $sysma_object_id);
    $n1 = n1_degre_irregularite($app, $calculation_date, $sysma_object_id);
    $n2 = n2_variation_section($app, $calculation_date, $sysma_object_id);
    $n3 = n3_quantite_elements_naturels($app, $calculation_date, $sysma_object_id);
    $n4 = n4_quantite_vegetation_lit($app, $calculation_date, $sysma_object_id);
    $m = m_sinuosite($app, $calculation_date, $sysma_object_id);
    /*
      echo 'n0 : ' . $n0 . '<br>';
      echo 'n1 : ' . $n1 . '<br>';
      echo 'n2 : ' . $n2 . '<br>';
      echo 'n3 : ' . $n3 . '<br>';
      echo 'n4 : ' . $n4 . '<br>';
      echo 'm : ' . $m . '<br>';
     */
    $K = 1 / (($n0 + $n1 + $n2 + $n3 + $n4) * $m);

    return ['K' => $K, 'n0' => $n0, 'n1' => $n1, 'n2' => $n2, 'n3' => $n3, 'n4' => $n4, 'm' => $m];
}

function k_classe_rugosite($k)
{

    if ($k == null)
        return null;
    if ($k < 20)
        return 'Coefficient de rugosité K entre 5 et 20';
    if ($k >= 20 and $k < 30)
        return 'Coefficient de rugosité K entre 20 et 30';
    if ($k >= 30)
        return 'Coefficient de rugosité K > 30';
    return null;
}

function surface_plein_bord($hauteur, $lpb, $lbase)
{

    // hauteur totale * (largeur plein bord + largeur base) / 2
    $surf = $hauteur * ($lpb + $lbase) / 2;
    return $surf;
}

/*
 * Périmètre mouillé pour la hauteur plein bord :
 * Ppb = 2 * √( ( (l_pb - l_base)/2)² + hauteur_pb² ) + l_base
 */

function perimetre_plein_bord($hauteur, $lpb, $lbase)
{

    $ppb = 2 * sqrt(pow((($lpb - $lbase) / 2), 2) + pow($hauteur, 2)) + $lbase;
    return $ppb;
}

function rayon_hydraulique($ppb, $spb)
{

    return $spb / $ppb;
}

/*
 * MANNING STRICKLER
 * Debit_Vu = (coef_rugosite)*(Surface_Vu)*(Rayon_hydraulique_Vu)^(2/3)*Racine(pente))
 */

function debit_plein_bord($k, $spb, $rh, $pente)
{


    $dpb = $k * $spb * pow($rh, (2 / 3)) * sqrt($pente);
    return $dpb;
}

function puissance($dpb, $pente, $lpb)
{

    if ($pente == null)
        return null;
    return $dpb * $pente * 9810 / $lpb;
}

function indice_artificialisation_lit_mineur($app, $calculation_date, $sysma_object_id, $classe_K)
{


    $T = new SysmaObjectParameterValue();
    // talweg
    $T->buildFromParameters($sysma_object_id, 158513, $calculation_date, $app);
    $talweg = null;
    if ($T->value == 'Dans le talweg') {
        $talweg = 0;
    } elseif ($T->value == 'Hors talweg') {
        $talweg = 1;
    }
    //sinuosité
    $T->buildFromParameters($sysma_object_id, 158530, $calculation_date, $app);
    $sinuosite = null;
    if ($T->value == 'Rectiligne') {
        $sinuosite = 1;
    } elseif ($T->value == 'Sinuosité réduite par rapport à la sinuosité naturelle') {
        $sinuosite = 0.5;
    } elseif ($T->value != null) {
        $sinuosite = 0;
    }
    // CEM
    $T->buildFromParameters($sysma_object_id, 158542, $calculation_date, $app);
    $cem = null;
    if (substr($T->value,0,8) == 'Classe 1') {
        $cem = 0;
    } elseif (substr($T->value,0,8) == 'Classe 6') {
        $cem = 0.5;
    } elseif ($T->value != null) {
        $cem = 1;
    }
    // rugosité

    $rug = null;
    if ($classe_K == 'Coefficient de rugosité K entre 5 et 20') {
        $rug = 0;
    } elseif ($classe_K == 'Coefficient de rugosité K entre 20 et 30') {
        $rug = 0.5;
    } elseif ($classe_K == 'Coefficient de rugosité K > 30') {
        $rug = 1;
    }
    // protec berge
    $T->buildFromParameters($sysma_object_id, 159901, $calculation_date, $app);
    $pb = null;
    if ($T->value == 'Absence de protection de berges') {
        $pb = 0;
    } elseif ($T->value == 'Présence de protection de berges de type génie végétal') {
        $pb = 0.25;
    } elseif ($T->value == 'Présence de protection de berges sur les berges du lit mineur') {
        $pb = 0.5;
    } elseif ($T->value == 'Présence de protection de berges sur les berges et le fond du lit mineur') {
        $pb = 0.75;
    } elseif ($T->value == '100% d\'enterrement (par couverture, busage, drainage ou comblement') {
        $pb = 1;
    }

    $ia = ($talweg + $sinuosite * 2 + $cem * 2 + $rug + $pb) / 7;
    return ['ia' => $ia, 'i_talweg' => $talweg, 'i_sinuosite' => $sinuosite, 'i_cem' => $cem, 'i_rugosite' => $rug, 'i_protection_berge' => $pb];
}

function indice_artificialisation_lit_mineur_classe($ia)
{


    if ($ia == 0) {
        return 'Cours d\'eau de référence';
    } elseif ($ia > 0 and $ia <= 0.25) {
        return 'Cours d\'eau naturel';
    } elseif ($ia > 0.25 and $ia <= 0.50) {
        return 'Cours d\'eau semi-artificiel';
    } elseif ($ia > 0.50 and $ia <= 0.75) {
        return 'Cours d\'eau artificiel';
    } elseif ($ia > 0.75 and $ia < 1) {
        return 'Cours d\'eau très artificiel';
    } elseif ($ia == 1) {
        return 'Cours d\'eau enterré ou plan d\'eau';
    }
}

/*
 * basée sur occupation du sol 5 m
 */

function indice_pression_bande_riveraine($app, $calculation_date, $sysma_object_id)
{

    // rive gauche
    $T = new SysmaObjectParameterValue();

    $T->buildFromParameters($sysma_object_id, 158719, $calculation_date, $app);
    $ods['RG'] = $T->value;
    $T->buildFromParameters($sysma_object_id, 158739, $calculation_date, $app);
    $ripi['RG'] = $T->value;
    $T->buildFromParameters($sysma_object_id, 158750, $calculation_date, $app);
    $drainage['RG'] = $T->value;
    $T->buildFromParameters($sysma_object_id, 158751, $calculation_date, $app);
    $pieti['RG'] = $T->value;

    // rive droite

    $T->buildFromParameters($sysma_object_id, 158753, $calculation_date, $app);
    $ods['RD'] = $T->value;
    $T->buildFromParameters($sysma_object_id, 158755, $calculation_date, $app);
    $ripi['RD'] = $T->value;
    $T->buildFromParameters($sysma_object_id, 158766, $calculation_date, $app);
    $drainage['RD'] = $T->value;
    $T->buildFromParameters($sysma_object_id, 158767, $calculation_date, $app);
    $pieti['RD'] = $T->value;

    // type plan d'eau
    $T->buildFromParameters($sysma_object_id, 158509, $calculation_date, $app);
    $typeReseau = $T->value;


    $rives = ['RG', 'RD'];
    foreach ($rives as $rive) {


        if (in_array($ods[$rive], ['Forêt feuillus', 'Autre milieu forestier'])) {

            if ($drainage[$rive] == 1) {
                $note[$rive] = 0.2;
            } elseif ($drainage[$rive] == 0 or $drainage[$rive] == null) {
                $note[$rive] = 0.1;
            }
        } elseif ($ods[$rive] == 'Surface enherbée') {
            if ($ripi[$rive] == 1) {
                $note[$rive] = 0.3;
            } elseif ($ripi[$rive] == 0 or $ripi[$rive] == null) {
                $note[$rive] = 0.4;
            }
            if ($drainage[$rive] == 1 or $pieti[$rive] == 1) {
                $note[$rive] = 0.5;
            }
        } elseif (in_array($ods[$rive], ['Terres labourables', 'Forêt résineux', 'Peupleraie'])) {


            // non drainé avec ripisylve
            if ($ripi[$rive] == 1 and ($drainage[$rive] == 0 or $drainage[$rive] == null)) {
                $note[$rive] = 0.6;
            }
            // non drainé sans ripisylve
            elseif (in_array($ods[$rive], ['Terres labourables']) and ($ripi[$rive] == 0 or $ripi[$rive] == null) and ($drainage[$rive] == 0 or $drainage[$rive] == null)) {
                $note[$rive] = 0.7;
            }
            // résineux ou peupleraie non drainée
            elseif (in_array($ods[$rive], ['Forêt résineux', 'Peupleraie']) and ($drainage[$rive] == 0 or $drainage[$rive] == null)) {
                $note[$rive] = 0.7;
            }
            // drainé avec ripisylve
            elseif (in_array($ods[$rive], ['Terres labourables']) and ($ripi[$rive] == 1) and $drainage[$rive] == 1) {
                $note[$rive] = 0.8;
            }
            // drainé sans ripisylve
            elseif (in_array($ods[$rive], ['Terres labourables']) and ($ripi[$rive] == 0 or $ripi[$rive] == null) and $drainage[$rive] == 1) {
                $note[$rive] = 0.9;
            }
            // résineux ou peupleraie drainé
            elseif (in_array($ods[$rive], ['Forêt résineux', 'Peupleraie']) and $drainage[$rive] == 1) {
                $note[$rive] = 0.9;
            }
        } elseif (in_array($ods[$rive], ['Zones d\'habitat', 'Zones industrielles', 'Urbain mixte']) or $typeReseau == 'Plan d\'eau') {
            $note[$rive] = 1;
        }
    }

    return ['indice_pression_RG' => $note['RG'], 'indice_pression_RD' => $note['RD'], 'indice_pression' => ($note['RG'] + $note['RD']) / 2];
}

function indice_resilience($puissance, $erodabilite_berge, $potentiel_apport_solide, $emprise)
{

    if ($puissance == null)
        return null;

    if ($puissance < 10) {
        $ir['puissance'] = 0;
    } elseif ($puissance >= 10 and $puissance < 30) {
        $ir['puissance'] = 0.3;
    } elseif ($puissance >= 30 and $puissance < 100) {
        $ir['puissance'] = 0.7;
    } elseif ($puissance >= 100) {
        $ir['puissance'] = 1.0;
    }


    if ($erodabilite_berge == 'nulle') {
        $ir['erodabilite_berge'] = 0;
    } elseif ($erodabilite_berge == 'faible') {
        $ir['erodabilite_berge'] = 0.3;
    } elseif ($erodabilite_berge == 'moyenne') {
        $ir['erodabilite_berge'] = 0.7;
    } elseif ($erodabilite_berge == 'forte') {
        $ir['erodabilite_berge'] = 1.0;
    }



    if ($potentiel_apport_solide == 'nul') {
        $ir['potentiel_apport_solide'] = 0;
    } elseif ($potentiel_apport_solide == 'faible') {
        $ir['potentiel_apport_solide'] = 0.3;
    } elseif ($potentiel_apport_solide == 'moyen') {
        $ir['potentiel_apport_solide'] = 0.7;
    } elseif ($potentiel_apport_solide == 'fort') {
        $ir['potentiel_apport_solide'] = 1.0;
    }



    if ($emprise == '< 1 largeur de lit') {
        $ir['emprise'] = 0;
    } elseif ($emprise == '1 à 3 largeur(s) de lit') {
        $ir['emprise'] = 0.3;
    } elseif ($emprise == '3 à 10 largeurs de lit') {
        $ir['emprise'] = 0.7;
    } elseif ($emprise == '> 10 largeurs de lit') {
        $ir['emprise'] = 1.0;
    }

    $ir['indice_resilience'] = $ir['puissance'] + $ir['erodabilite_berge'] + $ir['potentiel_apport_solide'] + $ir['emprise'];
    return $ir;
}

function t2bv_calculations(
    $app,
    $sysma_object_id,
    $calculation_date
) {


    $Exclu = new SysmaObjectParameterValue();
    $Exclu->buildFromParameters($sysma_object_id, 550312, $calculation_date, $app);

    if ($Exclu->value == 1) {
        $res = ['exclus_des_calculs' => 'oui'];
        return $res;
    }


    $ip = indice_pression_bande_riveraine($app, $calculation_date, $sysma_object_id);


    $TypeSeq = new SysmaObjectParameterValue();
    $TypeSeq->buildFromParameters($sysma_object_id, 158509, $calculation_date, $app);
    if ($TypeSeq->value == 'Cours d\'eau enterré' or $TypeSeq->value == 'Plan d\'eau') {


        $res = [
            'type_sequence' => $TypeSeq->value,
            'indice_artificialisation_note' => 1,
            'indice_artificialisation_classe' => indice_artificialisation_lit_mineur_classe(1),
            'indice_pression_RG' => $ip['indice_pression_RG'],
            'indice_pression_RD' => $ip['indice_pression_RD'],
            'indice_pression' => $ip['indice_pression']
        ];

        return $res;
    }



    if ($TypeSeq->value == 'Réseau diffus') {


        $res = [
            'type_sequence' => $TypeSeq->value,
            'indice_pression_RG' => $ip['indice_pression_RG'],
            'indice_pression_RD' => $ip['indice_pression_RD'],
            'indice_pression' => $ip['indice_pression']
        ];

        return $res;
    }





    $Hauteur = new SysmaObjectParameterValue();
    $Hauteur->buildFromParameters($sysma_object_id, 324202, $calculation_date, $app);
    $hauteur = $Hauteur->value;
    $Lpb = new SysmaObjectParameterValue();
    $Lpb->buildFromParameters($sysma_object_id, 324200, $calculation_date, $app);
    $lpb = $Lpb->value;
    $Lbase = new SysmaObjectParameterValue();
    $Lbase->buildFromParameters($sysma_object_id, 324201, $calculation_date, $app);
    $lbase = $Lbase->value;

    $k = k_rugosite($app, $calculation_date, $sysma_object_id);

    // pente du tronçon
    $IdTroncon = new SysmaObjectParameterValue();
    $IdTroncon->buildFromParameters($sysma_object_id, 158512, $calculation_date, $app);
    $id_objet_troncon = substr($IdTroncon->value, (strpos($IdTroncon->value, '[') + 2), -1);
    $Pente = new SysmaObjectParameterValue();
    $Pente->buildFromParameters($id_objet_troncon, 315287, $calculation_date, $app);
    $pente = $Pente->value;


    $spb = surface_plein_bord($hauteur, $lpb, $lbase);
    $ppb = perimetre_plein_bord($hauteur, $lpb, $lbase);
    $rh = rayon_hydraulique($ppb, $spb);

    $dpb = debit_plein_bord($k['K'], $spb, $rh, $pente);
    $puissance = puissance($dpb, $pente, $lpb);

    $ia = indice_artificialisation_lit_mineur($app, $calculation_date, $sysma_object_id, k_classe_rugosite($k['K']));

    if ($TypeSeq->value == 'Réseau diffus') {
        $ia['ia'] = 0;
    }


    // erodabilite
    $T = new SysmaObjectParameterValue();
    $T->buildFromParameters($sysma_object_id, 158558, $calculation_date, $app);
    $erodabilite_berge = strtolower($T->value);
    // potentiel apport solide
    $T2 = new SysmaObjectParameterValue();
    $T2->buildFromParameters($sysma_object_id, 324190, $calculation_date, $app);
    $potentiel_apport_solide = strtolower($T2->value);
    // emprise
    $T3 = new SysmaObjectParameterValue();
    $T3->buildFromParameters($sysma_object_id, 324195, $calculation_date, $app);
    $emprise = strtolower($T3->value);

    $ir = indice_resilience($puissance, $erodabilite_berge, $potentiel_apport_solide, $emprise);


    $res = [
        'type_sequence' => $TypeSeq->value,
        'degres_irregularite_berge' => degre_irregularite_berges($app, $calculation_date, $sysma_object_id),
        'n0' => $k['n0'],
        'n1' => $k['n1'],
        'n2' => $k['n2'],
        'n3' => $k['n3'],
        'n4' => $k['n4'],
        'm' => $k['m'],
        'K' => $k['K'],
        'classe_K' => k_classe_rugosite($k['K']),
        'surface_plein_bord' => $spb,
        'perimetre_plein_bord' => $ppb,
        'rayon_hydraulique' => $rh,
        'pente' => $pente,
        'debit_plein_bord' => $dpb,
        'puissance' => $puissance,
        'indice_talweg' => $ia['i_talweg'],
        'indice_sinuosite' => $ia['i_sinuosite'],
        'indice_cem' => $ia['i_cem'],
        'indice_rugosite' => $ia['i_rugosite'],
        'indice_protection_berge' => $ia['i_protection_berge'],
        'indice_artificialisation_note' => $ia['ia'],
        'indice_artificialisation_classe' => indice_artificialisation_lit_mineur_classe($ia['ia']),
        'indice_pression_RG' => $ip['indice_pression_RG'],
        'indice_pression_RD' => $ip['indice_pression_RD'],
        'indice_pression' => $ip['indice_pression'],
        'indice_resilience_puissance' => $ir['puissance'],
        'erodabilite_berge' => $erodabilite_berge,
        'indice_resilience_erodabilite_berge' => $ir['erodabilite_berge'],
        'potentiel_apport_solide' => $potentiel_apport_solide,
        'indice_resilience_potentiel_apport_solide' => $ir['potentiel_apport_solide'],
        'emprise' => $emprise,
        'indice_resilience_emprise' => $ir['emprise'],
        'indice_resilience' => $ir['indice_resilience'],
    ];

    return $res;
}
