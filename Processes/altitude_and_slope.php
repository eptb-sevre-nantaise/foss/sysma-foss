<?php

function altitude_and_slope(
    $app,
    $sysma_object_id,
    $calculation_date,
    $buffer_find_m = 15,
    $mntschemaname = 'r020_territoire_physique',
    $mnttablename = 't_mnt_alti_aspect_slope_percent_grass',
    $mntgeomname = 'geom',
    $mntaltiname = 'alti',
    $function_find = 'MIN',
    $slope_rtn_unit = 'mPERm'
) {


    $qry = 'SELECT sysma_object_id, geom, (xx_99_utils.f_mnt_get_linestring_slope (( \'{  
           "mntschemaname" : "' . $mntschemaname . '",
               "mnttablename" : "' . $mnttablename . '",
                   "mntgeomname" : "' . $mntgeomname . '",
                   "mntaltiname" : "' . $mntaltiname . '",
                       "geom" :  "\'|| foo.geom::text ||\'",
                           "buffer_find_m" : "' . $buffer_find_m . '",
                               "function_find" : "' . $function_find . '",
                                   "slope_rtn_unit" : "' . $slope_rtn_unit . '" } \')::text::json
            )).* FROM  (select sysma_object_id,  geom from sysma.sysma_object where sysma_object_id = ' . $sysma_object_id . ' ) as foo';


    $pdo = $app['pdo'];

    $sttmt = $pdo->query($qry);
    $sttmt->execute();
    $res = $sttmt->fetchAll(PDO::FETCH_ASSOC);

    return $res[0];
}
