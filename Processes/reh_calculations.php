<?php

/*
 * comprend les fonctions de calcul des altérations REH
 */


/*
 * calcul une altération à partir des paramètres de base
 * arguments :
 * $alteration = parameter_id correspondant à une alteration REH
 * $tab = tableau de paramètres :
 *      $tab['parameter_id'] = $value
 */

function calculeREH($id_alteration, $tab)
{

    $res = null;



    if ($id_alteration == null or $tab == null) {

        die('Paramètres incomplets');
    }


    // Altération élevation de la ligne d'eau (186)
    /*
     * basé :
     * 
     * Diversité des faciès d'écoulement (étiage) (184)
     * 
     */ elseif ($id_alteration == '186') {

        switch ($tab['184']) {

            case 'faible':
                $res['lettres'] = 'moyenne';
                break;
            case 'moyenne':
                $res['lettres'] = 'faible';
                break;
            case 'forte':
                $res['lettres'] = 'pas d\'altération';
                break;
            case 'nulle':
                $res['lettres'] = 'forte';
                break;
            default:
                $res['lettres'] = null;
                break;
        }
    }

    // Altération modification du profil en long (197)
    /*
     * basé :
     * 
     * Sinuosité (188)
     * 
     */ elseif ($id_alteration == '197') {

        switch ($tab['188']) {

            case 'faible':
                $res['lettres'] = 'moyenne';
                break;
            case 'moyenne':
                $res['lettres'] = 'faible';
                break;
            case 'forte':
                $res['lettres'] = 'pas d\'altération';
                break;
            case 'nulle':
                $res['lettres'] = 'forte';
                break;
            default:
                $res['lettres'] = null;
                break;
        }
    }


    // Altération réduction de la granulométrie grossière (198)
    /*
     * basé :
     * 
     * Diversité de la granulométrie (191)
     * 
     */ elseif ($id_alteration == '198') {

        switch ($tab['191']) {

            case 'faible':
                $res['lettres'] = 'moyenne';
                break;
            case 'moyenne':
                $res['lettres'] = 'faible';
                break;
            case 'forte':
                $res['lettres'] = 'pas d\'altération';
                break;
            case 'nulle':
                $res['lettres'] = 'forte';
                break;
            default:
                $res['lettres'] = null;
                break;
        }
    }

    // Altération déstabilisation du substrat (199)
    /*
     * basé :
     * 
     * Stabilité du substrat (des fonds) (192)
     * 
     */ elseif ($id_alteration == '199') {

        switch ($tab['192']) {

            case 'faible':
                $res['lettres'] = 'moyenne';
                break;
            case 'moyenne':
                $res['lettres'] = 'faible';
                break;
            case 'forte':
                $res['lettres'] = 'pas d\'altération';
                break;
            case 'nulle':
                $res['lettres'] = 'forte';
                break;
            default:
                $res['lettres'] = null;
                break;
        }
    }

    // Altération modification du profil en travers (200)
    /*
     * basé :
     * 
     * Incision du lit (193)
     * 
     */ elseif ($id_alteration == '200') {

        switch ($tab['193']) {

            case 'faible':
                $res['lettres'] = 'faible';
                break;
            case 'moyenne':
                $res['lettres'] = 'moyenne';
                break;
            case 'forte':
                $res['lettres'] = 'forte';
                break;
            case 'nulle':
                $res['lettres'] = 'pas d\'altération';
                break;
            default:
                $res['lettres'] = null;
                break;
        }
    }


    // Altération colmatage du substrat (201)
    /*
     * basé :
     * 
     * Accumulation de dépôts fins (194)
     * 
     */ elseif ($id_alteration == '201') {

        switch ($tab['194']) {

            case 'faible':
                $res['lettres'] = 'faible';
                break;
            case 'moyenne':
                $res['lettres'] = 'moyenne';
                break;
            case 'forte':
                $res['lettres'] = 'forte';
                break;
            case 'nulle':
                $res['lettres'] = 'pas d\'altération';
                break;
            default:
                $res['lettres'] = null;
                break;
        }
    }


    // Altération uniformisation, artificialisation des berges (210)
    /*
     * basé :
     * 
     * Diversité, densité des habitats de berges (204)
     * Diversité de la forme des berges (pentes, hauteur...) (205)
     * 
     */ elseif ($id_alteration == '210') {

        $res204 = $res205 = null;

        //  conversion des parametres en alteration

        switch ($tab['204']) {

            case 'faible':
                $res204['lettres'] = 'moyenne';
                break;
            case 'moyenne':
                $res204['lettres'] = 'faible';
                break;
            case 'forte':
                $res204['lettres'] = 'pas d\'altération';
                break;
            case 'nulle':
                $res204['lettres'] = 'forte';
                break;
            default:
                $res204['lettres'] = null;
                break;
        }


        switch ($tab['205']) {

            case 'faible':
                $res205['lettres'] = 'moyenne';
                break;
            case 'moyenne':
                $res205['lettres'] = 'faible';
                break;
            case 'forte':
                $res205['lettres'] = 'pas d\'altération';
                break;
            case 'nulle':
                $res205['lettres'] = 'forte';
                break;
            default:
                $res205['lettres'] = null;
                break;
        }

        $resultatEnNombre = null;
        $resultatEnNombre = max(array(convertitAlterationEnNombre($res204['lettres']), convertitAlterationEnNombre($res205['lettres'])));

        $alteration = convertitNombreEnalteration($resultatEnNombre);
        $res['lettres'] = $alteration;
    }


    // Altération réduction du linéaire de berges (développé) (211)
    /*
     * basé :
     * 
     * Stabilité des berges (206)
     * 
     */ elseif ($id_alteration == '211') {

        switch ($tab['206']) {

            case 'faible':
                $res['lettres'] = 'moyenne';
                break;
            case 'moyenne':
                $res['lettres'] = 'faible';
                break;
            case 'forte':
                $res['lettres'] = 'pas d\'altération';
                break;
            case 'nulle':
                $res['lettres'] = 'forte';
                break;
            default:
                $res['lettres'] = null;
                break;
        }
    }




    // Altération réduction de la végétation aquatique [202]
    /*
     * basé :
     * 
     * Stabilité des berges (206)
     * 
     */ elseif ($id_alteration == '202') {


        if ($tab['196'] == 'nulle') {
            if ($tab['621236'] == null) {
                $res['lettres'] = 'pas d altération';
            }
            if ($tab['621236'] == 'Faucardage') {
                $res['lettres'] = 'forte';
            }
        }

        if ($tab['196'] == 'faible') {
            if ($tab['621236'] == null) {
                $res['lettres'] = 'pas d altération';
            }
            if ($tab['621236'] == 'Faucardage') {
                $res['lettres'] = 'forte';
            }
        }


        if ($tab['196'] == 'moyenne') {
            if ($tab['621236'] == null) {
                $res['lettres'] = 'pas d altération';
            }
            if ($tab['621236'] == 'Faucardage') {
                $res['lettres'] = 'moyenne';
            }
            if ($tab['621236'] == 'Présence d invasives') {
                $res['lettres'] = 'moyenne';
            }
            if ($tab['621236'] == 'Présence d algues filamenteuses') {
                $res['lettres'] = 'moyenne';
            }
        }


        if ($tab['196'] == 'forte') {
            if ($tab['621236'] == null) {
                $res['lettres'] = 'pas d altération';
            }
            if ($tab['621236'] == 'Présence d invasives') {
                $res['lettres'] = 'forte';
            }
            if ($tab['621236'] == 'Présence d algues filamenteuses') {
                $res['lettres'] = 'forte';
            }
        }
    }




    // Altération réduction, uniformisation de la ripisylve (212)
    /*
     * basé :
     * 
     * Continuité de la végétation de rive (207)
     * Densité de la ripisylve (208)
     * Diversité de la végétation de rive (209)
     * 
     * requiert une premiere converion en alteration puis un agrégagt des sous alterations
     * 
     */ elseif ($id_alteration == '212') {


        $res207 = $res208 = $res209 = null;

        //  conversion des parametres en alteration

        switch ($tab['207']) {

            case 'faible':
                $res207['lettres'] = 'moyenne';
                break;
            case 'moyenne':
                $res207['lettres'] = 'faible';
                break;
            case 'forte':
                $res207['lettres'] = 'pas d\'altération';
                break;
            case 'nulle':
                $res207['lettres'] = 'forte';
                break;
            default:
                $res207['lettres'] = null;
                break;
        }


        switch ($tab['208']) {

            case 'faible':
                $res208['lettres'] = 'moyenne';
                break;
            case 'moyenne':
                $res208['lettres'] = 'faible';
                break;
            case 'forte':
                $res208['lettres'] = 'pas d\'altération';
                break;
            case 'nulle':
                $res208['lettres'] = 'forte';
                break;
            default:
                $res208['lettres'] = null;
                break;
        }


        switch ($tab['209']) {

            case 'faible':
                $res209['lettres'] = 'moyenne';
                break;
            case 'moyenne':
                $res209['lettres'] = 'faible';
                break;
            case 'forte':
                $res209['lettres'] = 'pas d\'altération';
                break;
            case 'nulle':
                $res209['lettres'] = 'forte';
                break;
            default:
                $res209['lettres'] = null;
                break;
        }

        $resultatEnNombre = null;
        $resultatEnNombre = max(array(convertitAlterationEnNombre($res207['lettres']), convertitAlterationEnNombre($res208['lettres']), convertitAlterationEnNombre($res209['lettres'])));

        $alteration = convertitNombreEnalteration($resultatEnNombre);
        $res['lettres'] = $alteration;
    }

    return $res;
}

function convertitAlterationEnNombre($alt)
{


    switch ($alt) {

        case 'pas d\'altération':
            $nb = 1;
            break;
        case 'faible':
            $nb = 2;
            break;
        case 'moyenne':
            $nb = 3;
            break;
        case 'forte':
            $nb = 4;
            break;
        default:
            $nb = null;
    }

    return $nb;
}

function convertitNombreEnalteration($nb)
{


    switch ($nb) {

        case 1:
            $alt = 'pas d\'altération';
            break;
        case 2:
            $alt = 'faible';
            break;
        case 3:
            $alt = 'moyenne';
            break;
        case 4:
            $alt = 'forte';
            break;
        default:
            $alt = null;
    }

    return $alt;
}

function convertitNombreEnalterationCompartiment($nb)
{


    switch ($nb) {

        case 1:
            $alt = 'tres bon';
            break;
        case 2:
            $alt = 'bon';
            break;
        case 3:
            $alt = 'moyen';
            break;
        case 4:
            $alt = 'mauvais';
            break;
        default:
            $alt = null;
    }

    return $alt;
}


/*
 * ce fichier permet de déclencher le calcul des altérations REH pour un segement (ou tous) et une date donnée
 * il est appelé en post depuis une fiche segment ou bien en get depuis le menu requêtes
 * 
 * il stoppe si les données du segment sont incomplètes
 * 
 */

function reh_calculations($app, $sysma_object_id, $calculation_date)
{

    $f = null;
    $incomplet = false;
    $listeParametresManquants = null;



    if ($sysma_object_id == null or $calculation_date == null)
        die('Paramètres manquants');
    if (!isADate($calculation_date))
        die('La date fournie est incorrecte ' . $date);



    // ETAPE 1 / PARAMETRES -> ALTERATIONS UNITAIRES
    // on passe en revue les paramètres utiles pour le calcul
    // 
    // 
    // Altération élevation de la ligne d'eau (186)
    /*
     * basé :
     * 
     * Diversité des faciès d'écoulement (étiage) (184)
     * 
     */

    $V = new SysmaObjectParameterValue('');
    $V->buildFromParameters($sysma_object_id, 184, $calculation_date, $app);
    if ($V->value == null) {
        $incomplet = true;
        $listeParametresManquants .= 'Diversité des faciès d\'écoulement (étiage) (184),';
    } else {
        $resultat = calculeREH(186, array('184' => $V->value));
        $res['a_elevlign'] = $resultat['lettres'];
    }


    // Altération modification du profil en long (197)
    /*
     * basé :
     * 
     * Sinuosité (188)
     * 
     */
    $V = new SysmaObjectParameterValue('');
    $V->buildFromParameters($sysma_object_id, 188, $calculation_date, $app);
    if ($V->value == null) {
        $incomplet = true;
        $listeParametresManquants .= 'Sinuosité (188),';
    } else {
        $resultat = calculeREH(197, array('188' => $V->value));
        $res['a_modprofl'] = $resultat['lettres'];
    }


    // Altération réduction de la granulométrie grossière (198)
    /*
     * basé :
     * 
     * Diversité de la granulométrie (191)
     * 
     */
    $V = new SysmaObjectParameterValue('');
    $V->buildFromParameters($sysma_object_id, 191, $calculation_date, $app);
    if ($V->value == null) {
        $incomplet = true;
        $listeParametresManquants .= 'Diversité de la granulométrie (191),';
    } else {
        $resultat = calculeREH(198, array('191' => $V->value));
        $res['a_redgranu'] = $resultat['lettres'];
    }

    // Altération déstabilisation du substrat (199)
    /*
     * basé :
     * 
     * Stabilité du substrat (des fonds) (192)
     * 
     */
    $V = new SysmaObjectParameterValue('');
    $V->buildFromParameters($sysma_object_id, 192, $calculation_date, $app);
    if ($V->value == null) {
        //$incomplet = true; // parametre non obligatoire
        $listeParametresManquants .= 'OPTIONNEL / Stabilité du substrat (des fonds) (192),';
    } else {
        $resultat = calculeREH(199, array('192' => $V->value));
        $res['a_destsubs'] = $resultat['lettres'];
    }

    // Altération modification du profil en travers (200)
    /*
     * basé :
     * 
     * Incision du lit (193)
     * 
     */
    $V = new SysmaObjectParameterValue('');
    $V->buildFromParameters($sysma_object_id, 193, $calculation_date, $app);
    if ($V->value == null) {
        $incomplet = true;
        $listeParametresManquants .= 'Incision du lit (193),';
    } else {
        $resultat = calculeREH(200, array('193' => $V->value));
        $res['a_modproft'] = $resultat['lettres'];
    }

    // Altération colmatage du substrat (201)
    /*
     * basé :
     * 
     * Accumulation de dépôts fins (194)
     * 
     */
    $V = new SysmaObjectParameterValue('');
    $V->buildFromParameters($sysma_object_id, 194, $calculation_date, $app);
    if ($V->value == null) {
        //$incomplet = true; // parametre non obligatoire
        $listeParametresManquants .= 'OPTIONNEL / Accumulation de dépôts fins (194),';
    } else {
        $resultat = calculeREH(201, array('194' => $V->value));
        $res['a_colmsubs'] = $resultat['lettres'];
    }

    // Altération uniformisation, artificialisation des berges (210)
    /*
     * basé :
     * 
     * Diversité, densité des habitats de berges (204)
     * Diversité de la forme des berges (pentes, hauteur...) (205)
     * 
     */
    $V = new SysmaObjectParameterValue('');
    $V->buildFromParameters($sysma_object_id, 204, $calculation_date, $app);
    if ($V->value == null) {
        $incomplet = true;
        $listeParametresManquants .= 'Diversité, densité des habitats de berges (204),';
    } else {
        $V2 = new SysmaObjectParameterValue('');
        $V2->buildFromParameters($sysma_object_id, 205, $calculation_date, $app);
        if ($V->value == null) {
            $incomplet = true;
            $listeParametresManquants .= 'Diversité de la forme des berges (pentes, hauteur...) (205),';
        } else {
            $resultat = calculeREH(210, array('204' => $V->value, '205' => $V2->value));
            $res['a_unifberg'] = $resultat['lettres'];
        }
    }


    // Altération réduction du linéaire de berges (développé) (211)
    /*
     * basé :
     * 
     * Stabilité des berges (206)
     * 
     */
    $V = new SysmaObjectParameterValue('');
    $V->buildFromParameters($sysma_object_id, 206, $calculation_date, $app);
    if ($V->value == null) {
        $incomplet = true;
        $listeParametresManquants .= 'Stabilité des berges (206),';
    } else {
        $resultat = calculeREH(211, array('206' => $V->value));
        $res['a_redlinbe'] = $resultat['lettres'];
    }

    // Altération réduction, uniformisation de la ripisylve (212)
    /*
     * basé :
     * 
     * Continuité de la végétation de rive (207)
     * Densité de la ripisylve (208)
     * Diversité de la végétation de rive (209)
     * 
     * requiert une premiere converion en alteration puis un agrégagt des sous alterations
     * 
     */
    $V = new SysmaObjectParameterValue('');
    $V->buildFromParameters($sysma_object_id, 207, $calculation_date, $app);
    if ($V->value == null) {
        $incomplet = true;
        $listeParametresManquants .= 'Continuité de la végétation de rive (207),';
    } else {
        $V2 = new SysmaObjectParameterValue('');
        $V2->buildFromParameters($sysma_object_id, 208, $calculation_date, $app);
        if ($V2->value == null) {
            $incomplet = true;
            $listeParametresManquants .= 'Densité de la ripisylve (208),';
        } else {
            $V3 = new SysmaObjectParameterValue('');
            $V3->buildFromParameters($sysma_object_id, 209, $calculation_date, $app);
            if ($V3->value == null) {
                $incomplet = true;
                $listeParametresManquants .= 'Diversité de la végétation de rive (209),';
            } else {

                $resultat = calculeREH(212, array('207' => $V->value, '208' => $V2->value, '209' => $V3->value));
                $res['a_unifripi'] = $resultat['lettres'];
            }
        }
    }



    // Altération Réduction de la végétation du lit (202)
    /*
     * basé :
     * 
     * Densité de la végétation aquatique (196)
     * Détail sur le végétation aquatique (621236)   
     * 
     * 
     */

    $V = new SysmaObjectParameterValue('');
    $V->buildFromParameters($sysma_object_id, 196, $calculation_date, $app);
    if ($V->value == null) {
        // $incomplet = true;
        $listeParametresManquants .= 'Densité de la végétation aquatique (196),';
    } else {
        $V2 = new SysmaObjectParameterValue('');
        $V2->buildFromParameters($sysma_object_id, 621236, $calculation_date, $app);
        if ($V2->value == null) {
            // $incomplet = true;
            $listeParametresManquants .= 'Détail sur le végétation aquatique (621236)   ,';
        }

        $resultat = calculeREH(202, array('196' => $V->value, '621236' => $V2->value));
        $res['a_redvegaq'] = $resultat['lettres'];
    }



    if ($incomplet == false) {

        $listeP = array(186, 187, 197, 198, 199, 200, 201, 202, 203, 210, 211, 212, 213);

        // ETAPE 2 / ALTERATIONS UNITAIRES -> ALTERATION DES COMPARTIMENTS
        // ALT. COMP. LIGNE D'EAU (187)
        /*
         * basé sur 
         * 
         * Altération élevation de la ligne d'eau (186)
         */

        $res['altc_lignd'] = convertitNombreEnAlterationCompartiment(convertitAlterationEnNombre($res['a_elevlign']));

        // ALT. COMP. LIT (203)
        /*
         * basé sur
         * 
         * Altération modification du profil en long (197)
         * Altération réduction de la granulométrie grossière (198)
         * Altération déstabilisation du substrat (199)
         * Altération modification du profil en travers (200)
         * Altération colmatage du substrat (201)
         * 
         * 
         */

        $res['altc_lit'] = convertitNombreEnAlterationCompartiment(
            max(
                array(
                    convertitAlterationEnNombre($res['a_modprofl']),
                    convertitAlterationEnNombre($res['a_redgranu']),
                    convertitAlterationEnNombre($res['a_destsubs']),
                    convertitAlterationEnNombre($res['a_modproft']),
                    convertitAlterationEnNombre($res['a_colmsubs']),
                    convertitAlterationEnNombre($res['a_redvegaq'])
                )
            )
        );


        // ALT. COMP. BERGES (213)
        /*
         * basé sur
         * Altération uniformisation, artificialisation des berges (210)
         * Altération réduction du linéaire de berges (développé) (211)
         * Altération réduction, uniformisation de la ripisylve (212)
         * 
         */

        $res['altc_berg'] = convertitNombreEnAlterationCompartiment(
            max(
                array(
                    convertitAlterationEnNombre($res['a_unifberg']),
                    convertitAlterationEnNombre($res['a_redlinbe']),
                    convertitAlterationEnNombre($res['a_unifripi'])
                )
            )
        );



        return $res;
    } else {
        return ['res' => 'Paramètres incomplets'];
    }
}
