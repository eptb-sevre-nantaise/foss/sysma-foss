<?php

function chargeSequences($app, $Troncon)
{

    // init
    $totalL = 0;
    $totalLNonEnterre = 0;
    $totalLNonEnterreNonPlanEauNonDiffus = 0;
    $totalKPondere = 0;
    $potAS = $erodB = $empriseD = $puissanceS = null;


    $qry = 'select sysma_object_id from sysma.sysma_object_data where parameter_id = 158512 and value ilike \'%o' . $Troncon->id . '%\'';
    $sttmt = $app['pdo']->query($qry);
    $sttmt->execute();
    while ($ids = $sttmt->fetchObject()) {

        echo $ids->sysma_object_id . ' : ';

        $Seq = new SysmaObject($ids->sysma_object_id, $app);
        $Seq->loadActualInfosSysmaObjectWithIndex();
        $Seq->loadGeoInfos('ST_Linestring');


        echo $Seq->dataIndex['iartif_cla']->value . '<br>';


        if ($Seq->dataIndex['excluscalc']->value != 1) {

            $totalL += $Seq->infosGeom['perimetre'];

            if ($Seq->dataIndex['type_res']->value != 'Cours d\'eau enterré') {


                $totalLNonEnterre += $Seq->infosGeom['perimetre'];
                $totalKPondere += $Seq->dataIndex['rugos_note']->value * $Seq->infosGeom['perimetre'];

                if ($Seq->dataIndex['type_res']->value != 'Plan d\'eau' and $Seq->dataIndex['type_res']->value != 'Réseau diffus') {

                    $totalLNonEnterreNonPlanEauNonDiffus += $Seq->infosGeom['perimetre'];
                    @$potAS[$Seq->dataIndex['pot_ap_sol']->value] += $Seq->infosGeom['perimetre'];
                    @$erodB[$Seq->dataIndex['berg_eroda']->value] += $Seq->infosGeom['perimetre'];
                    @$empriseD[$Seq->dataIndex['empr_dispo']->value] += $Seq->infosGeom['perimetre'];
                    @$puissanceS += $Seq->dataIndex['puis_spe']->value * $Seq->infosGeom['perimetre'];
                }
            }
        }
    }

    $totalLNonEnterreNonPlanEauNonDiffus == 0 ? $rugos_note = null : $rugos_note = $totalKPondere / $totalLNonEnterreNonPlanEauNonDiffus;
    return [
        'tr_lg_ppal' => $totalL,
        'lg_nonent' => $totalLNonEnterre,
        'lgnonnon' => $totalLNonEnterreNonPlanEauNonDiffus,
        'rugos_note' => $rugos_note,
        'potAS' => $potAS,
        'erodB' => $erodB,
        'empriseD' => $empriseD,
        'puissanceS' => $puissanceS
    ];
}

function array_key_first(array $arr)
{
    foreach ($arr as $key => $unused) {
        return $key;
    }
    return NULL;
}

function puissance($puissance)
{

    if ($puissance == null) {
        $p['puissance'] = null;
        $p['classep'] = null;
    } elseif ($puissance < 10) {
        $p['puissance'] = 0;
        $p['classep'] = '<10';
    } elseif ($puissance >= 10 and $puissance < 30) {
        $p['puissance'] = 0.3;
        $p['classep'] = '10-30';
    } elseif ($puissance >= 30 and $puissance < 100) {
        $p['puissance'] = 0.7;
        $p['classep'] = '30-100';
    } elseif ($puissance >= 100) {
        $p['puissance'] = 1.0;
        $p['classep'] = '>100';
    }

    return $p;
}

function erodabilite_berge($erodabilite_berge)
{
    if ($erodabilite_berge == 'nulle') {
        $erodabilite_berge_indice = 0;
    } elseif ($erodabilite_berge == 'faible') {
        $erodabilite_berge_indice = 0.3;
    } elseif ($erodabilite_berge == 'moyenne') {
        $erodabilite_berge_indice = 0.7;
    } elseif ($erodabilite_berge == 'forte') {
        $erodabilite_berge_indice = 1.0;
    }
    return $erodabilite_berge_indice;
}

function potentiel_apport_solide($potentiel_apport_solide)
{
    if ($potentiel_apport_solide == 'nul') {
        $potentiel_apport_solide = 0;
    } elseif ($potentiel_apport_solide == 'faible') {
        $potentiel_apport_solide = 0.3;
    } elseif ($potentiel_apport_solide == 'moyen') {
        $potentiel_apport_solide = 0.7;
    } elseif ($potentiel_apport_solide == 'fort') {
        $potentiel_apport_solide = 1.0;
    }

    return $potentiel_apport_solide;
}

function emprise($emprise)
{

    if ($emprise == '< 1 largeur de lit') {
        $emprise = 0;
    } elseif ($emprise == '1 à 3 largeur(s) de lit') {
        $emprise = 0.3;
    } elseif ($emprise == '3 à 10 largeurs de lit') {
        $emprise = 0.7;
    } elseif ($emprise == '> 10 largeurs de lit') {
        $emprise = 1.0;
    }
    return $emprise;
}

function t2bv_troncon_calculations(
    $app,
    $sysma_object_id,
    $calculation_date,
    $surf_bv_ref,
    $q2_bv_ref
) {

    echo '<p>' . $sysma_object_id . '</p>';

    $Troncon = new SysmaObject($sysma_object_id, $app);
    $Troncon->loadActualInfosSysmaObjectWithIndex();

    $tabSeq = chargeSequences($app, $Troncon);

    if ($surf_bv_ref == null or $q2_bv_ref == null or $Troncon->dataIndex['surface_bv']->value == null or $Troncon->dataIndex['pente']->value == null)
        return ['Erreur' => 'Au moins un champ obligatoire est manquant (surface et Q2 du BV de référence, surface du BV et pente du tronçon)'];



    $q2_extrapo = $q2_bv_ref * pow(($Troncon->dataIndex['surface_bv']->value / $surf_bv_ref), 0.8);
    $lpb_theo = 1.44 * pow($Troncon->dataIndex['surface_bv']->value, 0.41);
    $puis_extra = ($Troncon->dataIndex['pente']->value * 9810 * $q2_extrapo) / $lpb_theo;
    $prof_theo = 0.41 * pow($Troncon->dataIndex['surface_bv']->value, 0.25);
    $rap_lp_the = 3.56 * pow($Troncon->dataIndex['surface_bv']->value, 0.16);
    $surfm_theo = 0.4 * pow($Troncon->dataIndex['surface_bv']->value, 0.68);
    $largb_theo = 2 * ($surfm_theo / $prof_theo) - $lpb_theo;
    $permo_theo = 2 * sqrt(0.5 * ($lpb_theo - $largb_theo) * 0.5 * ($lpb_theo - $largb_theo) + $prof_theo * $prof_theo) + $largb_theo;

    $tabSeq['rugos_note'] != null ? $q2_theo = $surfm_theo * pow($surfm_theo / $permo_theo, 2 / 3) * sqrt($Troncon->dataIndex['pente']->value) * $tabSeq['rugos_note'] : $q2_theo = null;
    $q2_theo != null ? $puis_theo = ($Troncon->dataIndex['pente']->value * $q2_theo * 9810) / $lpb_theo : $puis_theo = null;


    // moyenne de paramètres "non moyennables" sur le tronçon, on prend donc le plus représenté
    if (!empty($tabSeq['potAS'])) {
        array_multisort($tabSeq['potAS'], SORT_DESC);
        $potentiel_apport_solide_classe = array_key_first($tabSeq['potAS']);
        $potentiel_apport_solide_indice = potentiel_apport_solide($potentiel_apport_solide_classe);
    } else {
        $potentiel_apport_solide_classe = $potentiel_apport_solide_indice = null;
    }
    if (!empty($tabSeq['erodB'])) {
        array_multisort($tabSeq['erodB'], SORT_DESC);
        $erodabilite_berge_classe = array_key_first($tabSeq['erodB']);
        $erodabilite_berge_indice = erodabilite_berge(strtolower($erodabilite_berge_classe));
    } else {
        $erodabilite_berge_classe = $erodabilite_berge_indice = null;
    }
    if (!empty($tabSeq['empriseD'])) {
        array_multisort($tabSeq['empriseD'], SORT_DESC);
        $emprise_disponible_classe = array_key_first($tabSeq['empriseD']);
        $emprise_disponible_indice = emprise($emprise_disponible_classe);
    } else {
        $emprise_disponible_classe = $emprise_disponible_indice = null;
    }
    // classe puissance spécifique
    $tabSeq['lgnonnon'] == 0 ? $puissance = null : $puissance = $tabSeq['puissanceS'] / $tabSeq['lgnonnon'];
    $P = puissance($puissance);



    return array_merge(
        $tabSeq,
        [
            'pente' => $Troncon->dataIndex['pente']->value,
            'q2_extrapo' => $q2_extrapo,
            'lpb_theo' => $lpb_theo,
            'prof_theo' => $prof_theo,
            'rap_lp_the' => $rap_lp_the,
            'surfm_theo' => $surfm_theo,
            'largb_theo' => $largb_theo,
            'permo_theo' => $permo_theo,
            'puis_extra' => $puis_extra,
            'q2_theo' => $q2_theo,
            'puis_theo' => $puis_theo,
            'pot_ap_sol' => $potentiel_apport_solide_classe,
            'potaps_ind' => $potentiel_apport_solide_indice,
            'eroda_berg' => $erodabilite_berge_classe,
            'erodb_ind' => $erodabilite_berge_indice,
            'empr_dispo' => $emprise_disponible_classe,
            'emprd_ind' => $emprise_disponible_indice,
            'puis_spe' => $P['classep'],
            'puissp_not' => $puissance,
            'puissp_ind' => $P['puissance'],
            'indiresnot' => $potentiel_apport_solide_indice + $erodabilite_berge_indice + $emprise_disponible_indice + $P['puissance']
        ]
    );
}
