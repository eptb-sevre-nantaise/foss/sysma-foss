<?php

function chargeSequences($app, $Troncon)
{

    // init
    $totalL = 0;
    $totalLNonEnterre = 0;
    $totalLEnterre = 0;
    $totalLPlanEau = 0;
    $totalLDiffus = 0;
    $totalLNonEnterreNonPlanEauNonDiffus = 0;

    $resIA = array();
    $resIAPerc = array();
    $resIP = array();
    $resIPPerc = array();
    $classeIA = ['Cours d\'eau de référence', 'Cours d\'eau naturel', 'Cours d\'eau semi-artificiel', 'Cours d\'eau artificiel', 'Cours d\'eau très artificiel', 'Cours d\'eau enterré ou plan d\'eau', ''];
    $colorIA = ['Cours d\'eau de référence' => 'DodgerBlue', 'Cours d\'eau naturel' => 'YellowGreen', 'Cours d\'eau semi-artificiel' => 'Gold', 'Cours d\'eau artificiel' => 'GoldenRod', 'Cours d\'eau très artificiel' => 'FireBrick', 'Cours d\'eau enterré ou plan d\'eau' => 'Silver', '' => 'Snow'];
    foreach ($classeIA as $classe) {
        $resIA[$classe] = 0;
        $resIAPerc[$classe] = 0;
    }
    $classeIP = ['Terres labourables', 'Surface enherbée', 'Forêt résineux', 'Forêt feuillus', 'Autre milieu forestier', 'Peupleraie', 'Zones d\'habitat', 'Zones industrielles', 'Urbain mixte'];
    $colorIP = ['Terres labourables' => 'Khaki', 'Surface enherbée' => 'YellowGreen', 'Forêt résineux' => 'DarkGreen', 'Forêt feuillus' => 'OliveDrab', 'Autre milieu forestier' => 'DarkSlateGray', 'Peupleraie' => 'LightGreen', 'Zones d\'habitat' => 'LightCoral', 'Zones industrielles' => 'SlateGray', 'Urbain mixte' => 'Tomato'];
    foreach ($classeIP as $classe) {
        $resIP['RG'][$classe] = 0;
        $resIP['RD'][$classe] = 0;
        $resIP['2rives'][$classe] = 0;
        $resIPPerc[$classe] = 0;
        $resIP10m['RG'][$classe] = 0;
        $resIP10m['RD'][$classe] = 0;
        $resIP10m['2rives'][$classe] = 0;
        $resIPPerc10m[$classe] = 0;
    }
    $classeMatBerg = ['Béton', 'Terre', 'Sable', 'Graviers', 'Galets', 'Blocs rocheux'];
    $colorMatBerg = ['Béton' => 'Silver', 'Terre' => 'Sienna', 'Sable' => 'LightGoldenRodYellow', 'Graviers' => 'LightSlateGray', 'Galets' => 'Thistle', 'Blocs rocheux' => 'Grey'];
    foreach ($classeMatBerg as $classe) {
        $resMB[$classe] = 0;
        $resMBPerc[$classe] = 0;
    }
    $classeErodBerg = ['Nulle', 'Faible', 'Moyenne', 'Forte'];
    $colorErodBerg = ['Nulle' => 'PaleGoldenRod', 'Faible' => 'YellowGreen', 'Moyenne' => 'SeaGreen', 'Forte' => 'DarkGreen'];
    foreach ($classeErodBerg as $classe) {
        $resEB[$classe] = 0;
        $resEBPerc[$classe] = 0;
    }

    $qry = 'select o.sysma_object_id from sysma.sysma_object_data d inner join sysma.sysma_object o on o.sysma_object_id = d.sysma_object_id where parameter_id = 158512 and value ilike \'%o' . $Troncon->id . '%\' order by o.sysma_object asc';
    $sttmt = $app['pdo']->query($qry);
    $sttmt->execute();
    while ($ids = $sttmt->fetchObject()) {
        //echo $ids->sysma_object_id . '<br>';

        $Seq = new SysmaObject($ids->sysma_object_id, $app);
        $Seq->loadActualInfosSysmaObjectWithIndex();
        $Seq->loadGeoInfos('ST_Linestring');

        $tabS[] = $Seq;


        //echo $Seq->dataIndex['type_res']->value . '<br>';

        if ($Seq->dataIndex['excluscalc']->value != 1) {

            $totalL += $Seq->infosGeom['perimetre'];
            $resIA[$Seq->dataIndex['iartif_cla']->value] += $Seq->infosGeom['perimetre'];



            if ($Seq->dataIndex['type_res']->value !== 'Cours d\'eau enterré') {

                $totalLNonEnterre += $Seq->infosGeom['perimetre'];


                $rives = ['RG', 'RD'];
                foreach ($rives as $rive) {
                    $resIP[$rive][$Seq->dataIndex['br' . strtolower($rive) . '_ods5']->value] += $Seq->infosGeom['perimetre'];
                    $resIP10m[$rive][$Seq->dataIndex['br' . strtolower($rive) . '_ods10']->value] += $Seq->infosGeom['perimetre'];
                }


                if ($Seq->dataIndex['type_res']->value !== 'Plan d\'eau' and $Seq->dataIndex['type_res']->value !== 'Réseau diffus') {

                    $totalLNonEnterreNonPlanEauNonDiffus += $Seq->infosGeom['perimetre'];
                    $resMB[$Seq->dataIndex['berg_mat']->value] += $Seq->infosGeom['perimetre'];
                    $resEB[$Seq->dataIndex['berg_eroda']->value] += $Seq->infosGeom['perimetre'];
                } else {
                    if ($Seq->dataIndex['type_res']->value == 'Plan d\'eau') {
                        $totalLPlanEau += $Seq->infosGeom['perimetre'];
                    } elseif ($Seq->dataIndex['type_res']->value == 'Réseau diffus') {
                        $totalLDiffus += $Seq->infosGeom['perimetre'];
                    }
                }
            } else {
                $totalLEnterre += $Seq->infosGeom['perimetre'];
            }
        }
    }

    foreach ($resIA as $key => $IA) {
        $resIAPerc[$key] = $IA * 100 / $totalL;
    }
    foreach ($resMB as $key => $i) {
        $resMBPerc[$key] = $i * 100 / $totalLNonEnterreNonPlanEauNonDiffus;
    }
    foreach ($resEB as $key => $i) {
        $resEBPerc[$key] = $i * 100 / $totalLNonEnterreNonPlanEauNonDiffus;
    }
    foreach ($resIP['RG'] as $key => $i) {
        $resIP['2rives'][$key] = $i;
    }
    foreach ($resIP['RD'] as $key => $i) {
        $resIP['2rives'][$key] += $i;
    }
    foreach ($resIP10m['RG'] as $key => $i) {
        $resIP10m['2rives'][$key] = $i;
    }
    foreach ($resIP10m['RD'] as $key => $i) {
        $resIP10m['2rives'][$key] += $i;
    }
    foreach ($resIP['2rives'] as $key => $i) {
        $resIPPerc[$key] += $i * 100 / (2 * $totalL);
    }
    foreach ($resIP10m['2rives'] as $key => $i) {
        $resIPPerc10m[$key] += $i * 100 / (2 * $totalL);
    }


    return [
        'tr_lg_ppal' => $totalL,
        'lg_enterre' => $totalLEnterre,
        'lg_pleau' => $totalLPlanEau,
        'lg_diffus' => $totalLDiffus,
        'lg_nonent' => $totalLNonEnterre,
        'lgnonnon' => $totalLNonEnterreNonPlanEauNonDiffus,
        'resIA' => $resIA,
        'resIAPerc' => $resIAPerc,
        'colorIA' => $colorIA,
        'resIP' => $resIP,
        'resIP10m' => $resIP10m,
        'colorIP' => $colorIP,
        'resIPPerc' => $resIPPerc,
        'resIPPerc10m' => $resIPPerc10m,
        'resMB' => $resMB,
        'colorMB' => $colorMatBerg,
        'resMBPerc' => $resMBPerc,
        'resEB' => $resEB,
        'colorEB' => $colorErodBerg,
        'resEBPerc' => $resEBPerc,
        'tabS' => $tabS
    ];
}

function puissance($puissance)
{

    if ($puissance == null) {
        $p['puissance'] = null;
    } elseif ($puissance < '10') {
        $p['puissance'] = 0;
    } elseif ($puissance == '10-30') {
        $p['puissance'] = 0.3;
    } elseif ($puissance == '30-100') {
        $p['puissance'] = 0.7;
    } elseif ($puissance == '>100') {
        $p['puissance'] = 1.0;
    }

    return $p['puissance'];
}

function erodabilite_berge($erodabilite_berge)
{
    if ($erodabilite_berge == 'nulle') {
        $erodabilite_berge_indice = 0;
    } elseif ($erodabilite_berge == 'faible') {
        $erodabilite_berge_indice = 0.3;
    } elseif ($erodabilite_berge == 'moyenne') {
        $erodabilite_berge_indice = 0.7;
    } elseif ($erodabilite_berge == 'forte') {
        $erodabilite_berge_indice = 1.0;
    }
    return $erodabilite_berge_indice;
}

function potentiel_apport_solide($potentiel_apport_solide)
{
    if ($potentiel_apport_solide == 'nul') {
        $potentiel_apport_solide = 0;
    } elseif ($potentiel_apport_solide == 'faible') {
        $potentiel_apport_solide = 0.3;
    } elseif ($potentiel_apport_solide == 'moyen') {
        $potentiel_apport_solide = 0.7;
    } elseif ($potentiel_apport_solide == 'fort') {
        $potentiel_apport_solide = 1.0;
    }

    return $potentiel_apport_solide;
}

function emprise($emprise)
{

    if ($emprise == '< 1 largeur de lit') {
        $emprise = 0;
    } elseif ($emprise == '1 à 3 largeur(s) de lit') {
        $emprise = 0.3;
    } elseif ($emprise == '3 à 10 largeurs de lit') {
        $emprise = 0.7;
    } elseif ($emprise == '> 10 largeurs de lit') {
        $emprise = 1.0;
    }
    return $emprise;
}

function t2bv_troncon_detailsequence_calculations(
    $app,
    $sysma_object_id,
    $calculation_date
) {

    $Troncon = new SysmaObject($sysma_object_id, $app);
    $Troncon->loadActualInfosSysmaObjectWithIndex();

    $tabSeq = chargeSequences($app, $Troncon);



    $potentiel_apport_solide_indice = potentiel_apport_solide($Troncon->dataIndex['pot_ap_sol']->value);
    $erodabilite_berge_indice = erodabilite_berge(strtolower($Troncon->dataIndex['eroda_berg']->value));
    $emprise_disponible_indice = emprise($Troncon->dataIndex['empr_dispo']->value);
    $puissance = puissance($Troncon->dataIndex['puis_spe']->value);

    return array_merge(
        $tabSeq,
        [
            'puissp_ind' => $puissance,
            'erodb_ind' => $erodabilite_berge_indice,
            'potaps_ind' => $potentiel_apport_solide_indice,
            'emprd_ind' => $emprise_disponible_indice
        ]
    );
}
