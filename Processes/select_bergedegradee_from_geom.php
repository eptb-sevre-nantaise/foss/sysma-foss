<?php

function select_bergedegradee_from_geom($app, $sysma_object_id, $calculation_date, $buffer = 80)
{


    $f = null;

    if ($sysma_object_id == null or $calculation_date == null or $buffer == null)
        die('Paramètres manquants');
    if (!isADate($calculation_date))
        die('La date fournie est incorrecte ' . $calculation_date);

    $pdo = $app['pdo'];


    $iab = "select sum(ST_Length(ST_Intersection(ST_Buffer((select o.geom from " . SCHEMA . ".sysma_object o where o.sysma_object_id = :sysma_object_id),:buffer),geom))) as longueur from " . SCHEMA . ".sysma_object where "
        . "sysma_object_type_id = 42 and "
        . "(end_date > :date or end_date = '0') and " // date de fin postérieure à la date de calcul (ou nulle)
        . "(start_date <= :date or start_date = '0')"; // date de fin postérieure à la date de calcul (ou nulle)


    $qryob = $pdo->prepare($iab);
    $qryob->bindParam(':sysma_object_id', $sysma_object_id);
    $qryob->bindParam(':date', $calculation_date);
    $qryob->bindParam(':buffer', $buffer);

    if ($qryob->execute() == false) {
        if (DEBUG) {
            die(pg_last_error());
        } else {
            die("Erreur SQL. Veuillez créer un rapport de bug pour l'équipe de developpement de Sysma-FOSS <a href=https://gitlab.sevre-nantaise.com/eptbsn/sysma-foss/-/issues>ici</a>");
        }
    }
    $Ob = $qryob->fetchObject();

    $res['lin_bergde'] = $Ob->longueur;
    return $res;
}
