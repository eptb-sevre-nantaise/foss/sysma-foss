<?php

function generate_t2bvtroncon_from_sequenceid(
    $app,
    $sysma_object_id,
    $calculation_date,
    $snap_dist = 10
) {

    if ($sysma_object_id == null)
        return null;


    $pdo = $app['pdo'];

    // ID TRONçON

    $qry0 = 'select sysma_object from sysma.sysma_object where sysma_object_id = :id';
    $sttmt0 = $app['pdo']->prepare($qry0);
    $sttmt0->bindParam(':id', $sysma_object_id, PDO::PARAM_INT);
    $sttmt0->execute();
    $O = $sttmt0->fetchObject();

    $id_troncon = substr($O->sysma_object, 0, -3);

    // Verification que le tronçon n'existe pas déja

    $qry00 = 'select * from sysma.sysma_object where sysma_object = :id_troncon and sysma_object_type_id = 315280' ;
    $sttmt00 = $app['pdo']->prepare($qry00) ;
    $sttmt00->bindParam(':id_troncon', $id_troncon);
    $sttmt00->execute();
    if ($sttmt00->rowCount() > 0){
        $res00 = $sttmt00->fetchObject();
        return ['Erreur' => 'Le tronçon <a href="/carte/objet/'.$res00->sysma_object_id.'">'.$id_troncon.'</a> existe déjà'];
    }


    // selection des séquences secondaires

    $qry1 = 'select d.sysma_object_id from sysma.sysma_object_data d 
        inner join sysma.sysma_object o on o.sysma_object_id = d.sysma_object_id        
        where 
        upper(sysma_object) ilike \'' . strtoupper($id_troncon) . '%\'
        and d.parameter_id = 159908 
        and d.value = \'0\' ';

    $sttmt1 = $pdo->prepare($qry1);
    $sttmt1->execute();
    $exclude = $sttmt1->fetchAll(PDO::FETCH_COLUMN);




    $qry = 'SELECT xx_99_utils.ST_LinemergeGetLongestPath (o.geom_union, \'snapdist=' . intval($snap_dist) . '\') as union_geom 
from ( select st_union(geom) as geom_union from sysma.sysma_object where ';

    if (count($exclude) > 0) {
        $qry .= ' sysma_object_id not in (' . implode(',', $exclude) . ') and ';
    }
    $qry .= '  upper(sysma_object) ilike \'' . strtoupper($id_troncon) . '%\' and sysma_object_type_id =  158507) as o';





    $sttmt = $pdo->prepare($qry);
    $sttmt->execute();
    $geom = $sttmt->fetchAll(PDO::FETCH_ASSOC);
    $troncon_geom = $geom[0]['union_geom'];

    if ($troncon_geom == null) {
        return ['erreur' => 'géométrie nulle'];
    }


    $res['sysma_object'] = strtoupper($id_troncon);
    $res['start_date'] = $calculation_date;
    $res['end_date'] = 0;
    $res['status'] = 'existant';
    $res['sysma_object_type_id'] = 315280;
    $res['created_by'] = $app['session']->get('user_id');
    $res['organisation_id'] = $app['session']->get('organisation_id');
    $res['modified_by'] = $app['session']->get('user_id');
    $date = date('Y-m-d H:i:s');
    $res['created_at'] = $date;
    $res['modified_at'] = $date;
    $res['geom'] = $troncon_geom;


    return ['res_object' => $res];
}
