#!/bin/bash

GREEN='\033[0;32m'
NOCOLOR='\033[0m'

# do not work... oO
# watch -n 3 echo "Last git pull at - `date`"

while true;
    do echo -e $GREEN"git pull at - `date`..." $NOCOLOR
    git pull
    echo
    sleep 2;
done
