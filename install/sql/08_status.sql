--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.17
-- Dumped by pg_dump version 13.2

-- Started on 2021-12-09 15:29:56

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
/*
SELECT pg_catalog.set_config('search_path', '', false);
*/
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- TOC entry 700 (class 1259 OID 119649820)
-- Name: status; Type: TABLE; Schema: sysma; Owner: eptbsn_dba_antoine
--
/*
CREATE TABLE sysma.status (
    status_id integer NOT NULL,
    status character varying(50),
    status_value character varying(50),
    description character varying(200),
    object_type character varying(255),
    display_order integer,
    style character varying(20),
    track_historytables_id bigint DEFAULT nextval('sysma.increment_x99_t_track_historytables'::regclass) NOT NULL
);


ALTER TABLE sysma.status OWNER TO eptbsn_dba_antoine;

--
-- TOC entry 5775 (class 0 OID 0)
-- Dependencies: 700
-- Name: TABLE status; Type: COMMENT; Schema: sysma; Owner: eptbsn_dba_antoine
--

COMMENT ON TABLE sysma.status IS '{
   "display_groupe_order":140,
   "groupe_name":"Configuration de sysma",
   "table_comment":"Liste des statuts possibles des objets et des travaux"
}';


--
-- TOC entry 5776 (class 0 OID 0)
-- Dependencies: 700
-- Name: COLUMN status.style; Type: COMMENT; Schema: sysma; Owner: eptbsn_dba_antoine
--

COMMENT ON COLUMN sysma.status.style IS 'style CSS';


--
-- TOC entry 5777 (class 0 OID 0)
-- Dependencies: 700
-- Name: COLUMN status.track_historytables_id; Type: COMMENT; Schema: sysma; Owner: eptbsn_dba_antoine
--

COMMENT ON COLUMN sysma.status.track_historytables_id IS 'identifiant unique de ligne utilisé pour le suivi historique des enregistrement dans la table x99_track_historiquetables';


--
-- TOC entry 701 (class 1259 OID 119649824)
-- Name: status_status_id_seq; Type: SEQUENCE; Schema: sysma; Owner: eptbsn_dba_antoine
--

CREATE SEQUENCE sysma.status_status_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sysma.status_status_id_seq OWNER TO eptbsn_dba_antoine;

--
-- TOC entry 5779 (class 0 OID 0)
-- Dependencies: 701
-- Name: status_status_id_seq; Type: SEQUENCE OWNED BY; Schema: sysma; Owner: eptbsn_dba_antoine
--

ALTER SEQUENCE sysma.status_status_id_seq OWNED BY sysma.status.status_id;


--
-- TOC entry 5547 (class 2604 OID 119649901)
-- Name: status status_id; Type: DEFAULT; Schema: sysma; Owner: eptbsn_dba_antoine
--

ALTER TABLE ONLY sysma.status ALTER COLUMN status_id SET DEFAULT nextval('sysma.status_status_id_seq'::regclass);

*/
--
-- TOC entry 5768 (class 0 OID 119649820)
-- Dependencies: 700
-- Data for Name: status; Type: TABLE DATA; Schema: sysma; Owner: eptbsn_dba_antoine
--

COPY sysma.status (status_id, status, status_value, description, object_type, display_order, style, track_historytables_id) FROM stdin;
4	Existant	existant	l'objet existe, son installation est terminée	sysma_object	4	statutExistant	0
12	En cours	en_cours		sysma_relation	2	statutEnCours	192368
11	En projet	en_projet		sysma_relation	1	statutEnProjet	192367
1	Préconisé	preconise	prévu dans une programmation	sysma_object	1	statutPreconise	0
2	1er contact	1ercontact	1er échange avec l'exploitant sans avoir fixé les détails	sysma_object	2	statut1erContact	0
5	Annulé	annule	l'objet était prévu mais il a été annulé	sysma_object	5	statutAnnule	0
6	En projet	en_projet		sysma_action	1	statutEnProjet	0
7	En cours	en_cours		sysma_action	2	statutEnCours	0
8	Terminé	termine		sysma_action	3	statutTermine	0
9	Annulé	annule		sysma_action	4	statutAnnule	0
3	En préparation	en_preparation	les détails de l'objet sont définis	sysma_object	3	statutEnPreparation	0
10	Programmé	programme	prévu dans une programmation	sysma_action	0	statutProgramme	0
13	Terminé	termine		sysma_relation	3	statutTermine	192369
14	Annulé	annule		sysma_relation	4	statutAnnule	192370
\.

/*
--
-- TOC entry 5781 (class 0 OID 0)
-- Dependencies: 701
-- Name: status_status_id_seq; Type: SEQUENCE SET; Schema: sysma; Owner: eptbsn_dba_antoine
--

SELECT pg_catalog.setval('sysma.status_status_id_seq', 14, true);


--
-- TOC entry 5549 (class 2606 OID 119649965)
-- Name: status status_pkey; Type: CONSTRAINT; Schema: sysma; Owner: eptbsn_dba_antoine
--

ALTER TABLE ONLY sysma.status
    ADD CONSTRAINT status_pkey PRIMARY KEY (status_id);


--
-- TOC entry 5778 (class 0 OID 0)
-- Dependencies: 700
-- Name: TABLE status; Type: ACL; Schema: sysma; Owner: eptbsn_dba_antoine
--

GRANT ALL ON TABLE sysma.status TO grp_db_ext_mut_98_demo_dev_sysma_rw;
GRANT ALL ON TABLE sysma.status TO grp_db_ext_mut_98_demo_dev_sysma_admin;


--
-- TOC entry 5780 (class 0 OID 0)
-- Dependencies: 701
-- Name: SEQUENCE status_status_id_seq; Type: ACL; Schema: sysma; Owner: eptbsn_dba_antoine
--

GRANT SELECT,USAGE ON SEQUENCE sysma.status_status_id_seq TO grp_db_ext_mut_98_demo_dev_sysma_rw;
GRANT ALL ON SEQUENCE sysma.status_status_id_seq TO grp_db_ext_mut_98_demo_dev_sysma_admin;


-- Completed on 2021-12-09 15:29:58

--
-- PostgreSQL database dump complete
--
*/
