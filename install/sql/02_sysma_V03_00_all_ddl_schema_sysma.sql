--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.17
-- Dumped by pg_dump version 9.6.3

-- Started on 2021-07-23 14:57:45

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 401 (class 2615 OID 7559726)
-- Name: sysma; Type: SCHEMA; Schema: -; Owner: -
--

-- CREATE SCHEMA sysma;


SET search_path = sysma, pg_catalog;

--
-- TOC entry 10052 (class 1247 OID 7561648)
-- Name: json_style; Type: TYPE; Schema: sysma; Owner: -
--

CREATE TYPE json_style AS (
	nom text,
	min numeric,
	max numeric,
	color text
);


--
-- TOC entry 8914 (class 1255 OID 7561781)
-- Name: _memo_ajout_fonction_trackhistotable(); Type: FUNCTION; Schema: sysma; Owner: -
--

CREATE FUNCTION _memo_ajout_fonction_trackhistotable() RETURNS void
    LANGUAGE sql IMMUTABLE
    AS $_$
/*
Pour ajouter sur tout le schéma la fonctionnalité qui permet de tracer toutes les modifications des tables du schéma

--création de ta table qui va stocker les données :

--DROP TABLE sysma.x99_track_historiquetables
CREATE TABLE sysma.x99_track_historiquetables
(
  id serial NOT NULL PRIMARY KEY,
  change_date timestamp with time zone DEFAULT now(),
  
  change_op character varying(15),
  schema_name name,
  table_name name,
  
  row_id_track_historiquetables integer,
  id_modificateur integer,
  old_row json,
  new_row json
)

-- création d'une séquence unique (qui permettra de repérer toutes les lignes)

CREATE SEQUENCE sysma.increment_id_track_historiquetables
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE sysma.increment_id_track_historiquetables
  OWNER TO eptbsn;
GRANT ALL ON SEQUENCE sysma.increment_id_track_historiquetables TO eptbsn;
GRANT ALL ON SEQUENCE sysma.increment_id_track_historiquetables TO grp_eptbsn_interne_up_sysma;
GRANT SELECT, USAGE ON SEQUENCE sysma.increment_id_track_historiquetables TO grp_eptbsn_moulinettes;
COMMENT ON SEQUENCE sysma.increment_id_track_historiquetables
  IS 'Est appelée pour incrémenter un id_track_historiquetables pour le suivi de toutes les tables';


-- création de la fonction trigger :
CREATE OR REPLACE FUNCTION sysma.trg_f_track_historiquetables()
  RETURNS trigger AS
--mettre BODY entre $
begin
  IF TG_OP = 'INSERT' THEN
  
  INSERT INTO sysma.x99_track_historiquetables(
       change_op, schema_name, table_name, row_id_track_historiquetables, 
       id_modificateur, new_row)
   SELECT 
       TG_OP,
       TG_TABLE_SCHEMA,
       TG_TABLE_NAME,
       new.id_track_historiquetables,
       new.id_modificateur,
       row_to_json(new);
   

  return new;
  

  ELSIF TG_OP = 'UPDATE' THEN
    INSERT INTO sysma.x99_track_historiquetables(
       change_op, schema_name, table_name, row_id_track_historiquetables, 
       id_modificateur, old_row, new_row)
   SELECT 
       TG_OP,
       TG_TABLE_SCHEMA,
       TG_TABLE_NAME,
       new.id_track_historiquetables,
       new.id_modificateur,
       row_to_json(old),
       row_to_json(new);
   

  return new;
  

 ELSIF TG_OP = 'DELETE' THEN
    INSERT INTO sysma.x99_track_historiquetables(
       change_op, schema_name, table_name, row_id_track_historiquetables, 
       id_modificateur, old_row)
   SELECT 
       TG_OP,
       TG_TABLE_SCHEMA, 
       TG_TABLE_NAME,
       old.id_track_historiquetables,
       NULL,
       row_to_json(old);

   

  return old;
  END IF;

  
end;
--mettre BODY entre $
  LANGUAGE plpgsql VOLATILE
  COST 100;

-- Ajout d'une colonne avec le même nom id_track_historiquetables + peuplement + default value + ajout trigger (attention ne pas les pos"er pour tous : il y a les code sql pour la pose des triggers à la suite) dans toutes les tables 
-- copier les colonnes et les executer en SQL

with lstable as (
SELECT
 pg_tables.schemaname,
pg_tables.tablename
from pg_tables  WHERE  schemaname = 'sysma'  -- à adapter
AND NOT (tablename  LIKE 'xx_a_eff%'  OR tablename  LIKE 'x99_t_track_historiquetables') -- à adapter ou commenter
 )

--select * from lstable
SELECT *, 
'ALTER TABLE ' || schemaname || '.'|| tablename || ' ADD COLUMN id_track_historiquetables bigint;',
'UPDATE ' || schemaname || '.'|| tablename || ' SET id_track_historiquetables=nextval(''sysma.increment_id_track_historiquetables''::regclass);',
' ALTER TABLE ' || schemaname || '.'|| tablename ||' ALTER COLUMN id_track_historiquetables SET NOT NULL;',
' ALTER TABLE ' || schemaname || '.'|| tablename ||' ALTER COLUMN id_track_historiquetables SET DEFAULT nextval(''sysma.increment_id_track_historiquetables''::regclass);',
'  COMMENT ON COLUMN ' || schemaname || '.'|| tablename ||'.id_track_historiquetables IS ''identifiant unique de ligne utilisé pour le suivi historique des enregistrement dans la table x99_track_historiquetables'';',
'  CREATE TRIGGER trackhisto_'|| tablename || '  BEFORE INSERT OR UPDATE OR DELETE    ON ' || schemaname || '.'|| tablename ||'   FOR EACH ROW  EXECUTE PROCEDURE sysma.trg_f_track_historiquetables();'

from lstable order by tablename;

-- pose des triggers (seulement certaines tables car il faut un id_modificateur)
CREATE TRIGGER trackhisto_choix_parametre_objetgeo   BEFORE INSERT OR UPDATE OR DELETE    ON sysma.choix_parametre_objetgeo   FOR EACH ROW  EXECUTE PROCEDURE sysma.trg_f_track_historiquetables();
CREATE TRIGGER trackhisto_choix_parametre_travaux   BEFORE INSERT OR UPDATE OR DELETE    ON sysma.choix_parametre_travaux   FOR EACH ROW  EXECUTE PROCEDURE sysma.trg_f_track_historiquetables();
CREATE TRIGGER trackhisto_donnees_objetgeo   BEFORE INSERT OR UPDATE OR DELETE    ON sysma.donnees_objetgeo   FOR EACH ROW  EXECUTE PROCEDURE sysma.trg_f_track_historiquetables();
CREATE TRIGGER trackhisto_donnees_travaux   BEFORE INSERT OR UPDATE OR DELETE    ON sysma.donnees_travaux   FOR EACH ROW  EXECUTE PROCEDURE sysma.trg_f_track_historiquetables();
CREATE TRIGGER trackhisto_l_objetgeo_travaux   BEFORE INSERT OR UPDATE OR DELETE    ON sysma.l_objetgeo_travaux   FOR EACH ROW  EXECUTE PROCEDURE sysma.trg_f_track_historiquetables();
CREATE TRIGGER trackhisto_objetsgeo   BEFORE INSERT OR UPDATE OR DELETE    ON sysma.objetsgeo   FOR EACH ROW  EXECUTE PROCEDURE sysma.trg_f_track_historiquetables();
CREATE TRIGGER trackhisto_travaux   BEFORE INSERT OR UPDATE OR DELETE    ON sysma.travaux   FOR EACH ROW  EXECUTE PROCEDURE sysma.trg_f_track_historiquetables();
CREATE TRIGGER trackhisto_types_objetgeo   BEFORE INSERT OR UPDATE OR DELETE    ON sysma.types_objetgeo   FOR EACH ROW  EXECUTE PROCEDURE sysma.trg_f_track_historiquetables();
CREATE TRIGGER trackhisto_types_objetgeo_parametres   BEFORE INSERT OR UPDATE OR DELETE    ON sysma.types_objetgeo_parametres   FOR EACH ROW  EXECUTE PROCEDURE sysma.trg_f_track_historiquetables();
CREATE TRIGGER trackhisto_types_travaux   BEFORE INSERT OR UPDATE OR DELETE    ON sysma.types_travaux   FOR EACH ROW  EXECUTE PROCEDURE sysma.trg_f_track_historiquetables();
CREATE TRIGGER trackhisto_types_travaux_parametres   BEFORE INSERT OR UPDATE OR DELETE    ON sysma.types_travaux_parametres   FOR EACH ROW  EXECUTE PROCEDURE sysma.trg_f_track_historiquetables();
CREATE TRIGGER trackhisto_utilisateurs   BEFORE INSERT OR UPDATE OR DELETE    ON sysma.utilisateurs   FOR EACH ROW  EXECUTE PROCEDURE sysma.trg_f_track_historiquetables();
CREATE TRIGGER trackhisto_utilisateurs_droits   BEFORE INSERT OR UPDATE OR DELETE    ON sysma.utilisateurs_droits   FOR EACH ROW  EXECUTE PROCEDURE sysma.trg_f_track_historiquetables();

*/


$_$;


--
-- TOC entry 9335 (class 1255 OID 91153020)
-- Name: f_dictionary_get_relation_child(name, text, bigint); Type: FUNCTION; Schema: sysma; Owner: -
--

CREATE FUNCTION f_dictionary_get_relation_child(source_schema name, relation_type text, id_parent bigint) RETURNS SETOF bigint
    LANGUAGE plpgsql STABLE STRICT
    AS $_$

/*
permet de lister les ids qui dépendent d'un objet 
   object_type->bject_type_param->object_type_param_choice
ou 
   object_type->action_type->action_type_param->action_type_param_choice 

Exemple d'appel :

select sysma.f_dictionary_get_relation_child(
     source_schema := 'rx000_sysma_referentiel_mutualise_fdw':: NAME
   , relation_type := 'sysma_object_type->sysma_object_type_params':: TEXT
   , id_parent := '158507'::BIGINT
   )


*/

declare 
dico_table NAME;
id_parent_name NAME ;
id_child_name NAME ;
sql text;

col_list_ins TEXT;
r Bigint;
BEGIN


CASE  relation_type 
   WHEN 'object_type->object_type_param' THEN 
      dico_table := 'sysma_object_type_parameter'::NAME;
      id_parent_name := 'sysma_object_type_id';
      id_child_name := 'parameter_id';
   WHEN 'object_type_param->object_type_param_choice' THEN 
      dico_table := 'sysma_object_type_parameter_choice'::NAME;
      id_parent_name := 'parameter_id';
      id_child_name := 'choice_id';


   WHEN 'object_type->action_type' THEN 
      dico_table := 'sysma_action_type'::NAME;
      id_parent_name := 'sysma_object_type_id';
      id_child_name := 'sysma_action_type_id';
      
   WHEN 'action_type->action_type_param' THEN 
      dico_table := 'sysma_action_type_parameter'::NAME;
      id_parent_name := 'sysma_action_type_id';
      id_child_name := 'parameter_id';
      
   WHEN 'action_type_param->action_type_param_choice' THEN 
      dico_table := 'sysma_action_type_parameter_choice'::NAME;
      id_parent_name := 'parameter_id';
      id_child_name := 'choice_id'; 
   ELSE
   dico_table := '??';
END CASE;


sql := format (
$SQL$ 
SELECT  %I  -- dest_parent_id , col_list_ins
FROM %I.%I 
WHERE %I = %s
$SQL$
, id_child_name
, source_schema, dico_table
, id_parent_name, id_parent
);

-- raise notice '%', sql ;  -- debug
--EXECUTE sql;

FOR r IN
        Execute sql
    LOOP
        -- can do some processing here
        RETURN NEXT r; -- return current row of SELECT
    END LOOP;
RETURN;
--RETURN QUERY '%', sql;
--return NEXT EXECUTE QUERY (sql);





END

$_$;


--
-- TOC entry 9564 (class 1255 OID 177338453)
-- Name: f_dictionary_sync(name, name, name, name, name, text, name, bigint, bigint, boolean, bigint); Type: FUNCTION; Schema: sysma; Owner: -
--

CREATE FUNCTION f_dictionary_sync(source_schema name, source_table name, dest_schema name, dest_table name, id_col_name name, col_list text, parent_col_name name, source_id bigint, dest_parent_id bigint, new_dest_id boolean DEFAULT false, modified_by bigint DEFAULT 1) RETURNS json
    LANGUAGE plpgsql
    AS $_$

/*
permet de copier une entrée du dico de sysma depuis les tables fdw vers sysma des territoires mutualisés

V04 : adapt to new model schema
V03 : attribue id_modificateur = modificateur_id, 1 par défaut = init sysma
V02 : si new_dest_id := FALSE et id existe déjà dans le dictionnaire l'entrée ne sera pas ajoutée, et l'id existant sera retourné.

Exemple d'appel :

select sysma.f_dictionary_sync(
     source_schema := 'rx000_sysma_referentiel_mutualise_fdw':: NAME
   , source_table := 'sysma_object_type_parameter':: NAME
   , dest_schema := 'sysma'::NAME
   , dest_table := 'sysma_object_type_parameter'::NAME
   , id_col_name := 'id_parametre'::NAME
   , col_list := ' parametre, description, data_type, regroupement, date_creation, date_modification, source_parametre, source_donnees, unite, obligatoire, col_sig, sdlt, id_modificateur, terrain, cache '::text
   , parent_col_name := 'sysma_object_type_id'::NAME
   , source_id := '100'::bigint
   , dest_parent_id := '1'::bigint 
   , new_dest_id := FALSE::boolean  
   , modified_by:=1 )

*/

declare 
sql text;

sql_test_exist TEXT;

bigint_test_exist bigint;
rtn json;

col_list_ins TEXT;
parent_col_name_or_not text;
dest_parent_id_or_not  text;

BEGIN

IF new_dest_id IS FALSE THEN
   sql_test_exist := format  (
   $SQL$ 

   SELECT %I   -- dest_parent_id , col_list_ins
   FROM %I.%I 
   WHERE %I = %s

   $SQL$
   , id_col_name
   , dest_schema, dest_table
   , id_col_name , source_id

   );

   execute sql_test_exist into bigint_test_exist;

   IF (bigint_test_exist::bigint = source_id::bigint) THEN

   rtn :=  to_json(t) FROM (SELECT bigint_test_exist as res_id, 'ID existe deja, entrée non ajoutée' as  res_comment) t;
      
   return   rtn;


   ELSE 

   if new_dest_id IS TRUE THEN 
      col_list_ins = col_list ;
   ELSE 
      col_list_ins = id_col_name || ', '|| col_list ;
   END IF ;

   if parent_col_name IS NULL THEN 
      parent_col_name_or_not = '' ;
      dest_parent_id_or_not = '';
      
   ELSE 
      parent_col_name_or_not = parent_col_name || ' , ' ;
      dest_parent_id_or_not = dest_parent_id || ' , ';

   END IF ;


   sql := format (
   $SQL$ 
   WITH ins as (
   INSERT INTO %I.%I   -- table dest
      (%s  %s, modified_by) -- parent_col_name , col_list_ins
   SELECT %s   %s , %s -- dest_parent_id , col_list_ins, modificateur_id
   FROM %I.%I 
   WHERE %I = %s
   -- LIMIT 1

   RETURNING  %I as res_id, 'ajout ok'::text as res_comment

  )
  select to_json (ins) from ins
   $SQL$
   , dest_schema, dest_table
   , parent_col_name_or_not, col_list_ins


   , dest_parent_id_or_not, col_list_ins , modified_by
   , source_schema, source_table
   , id_col_name , source_id

   , id_col_name
   );

   execute sql into rtn;

   return   rtn;

   END IF;

END IF;

END

$_$;


--
-- TOC entry 9336 (class 1255 OID 91153021)
-- Name: f_dictionary_sync_v01(name, name, name, name, name, text, name, bigint, bigint, boolean); Type: FUNCTION; Schema: sysma; Owner: -
--

CREATE FUNCTION f_dictionary_sync_v01(source_schema name, source_table name, dest_schema name, dest_table name, id_col_name name, col_list text, parent_col_name name, source_id bigint, dest_parent_id bigint, new_dest_id boolean DEFAULT false) RETURNS bigint
    LANGUAGE plpgsql
    AS $_$

/*
permet de copiee une entrée du dico de sysma depuis les tables fdw vers sysma des territoires mutualisés

Exemple d'appel :

select sysma.f_dictionary_sync_v01(
     source_schema := 'rx000_sysma_referentiel_mutualise_fdw':: NAME
   , source_table := 'types_objetgeo_parametres':: NAME
   , dest_schema := 'sysma'::NAME
   , dest_table := 'types_objetgeo_parametres'::NAME
   , id_col_name := 'id_parametre'::NAME
   , col_list := ' parametre, description, type_donnee, regroupement, date_creation, date_modification, source_parametre, source_donnees, unite, obligatoire, col_sig, sdlt, id_modificateur, terrain, cache '::text
   , parent_col_name := 'id_type_objetgeo'::NAME
   , source_id := '100'::bigint
   , dest_parent_id := '1'::bigint 
   , new_dest_id := FALSE::boolean   )

*/

declare 
sql text;

rtn BIGINT;

col_list_ins TEXT;
parent_col_name_or_not text;
dest_parent_id_or_not  text;

BEGIN

if new_dest_id IS TRUE THEN 
   col_list_ins = col_list ;
ELSE 
   col_list_ins = id_col_name || ', '|| col_list ;
END IF ;

if parent_col_name IS NULL THEN 
   parent_col_name_or_not = '' ;
   dest_parent_id_or_not = '';
   
ELSE 
   parent_col_name_or_not = parent_col_name || ' , ' ;
   dest_parent_id_or_not = dest_parent_id || ' , ';

END IF ;


sql := format (
$SQL$ 
INSERT INTO %I.%I   -- table dest
   (%s  %s) -- parent_col_name , col_list_ins
SELECT %s   %s  -- dest_parent_id , col_list_ins
FROM %I.%I 
WHERE %I = %s
-- LIMIT 1

RETURNING  %I
$SQL$
, dest_schema, dest_table
, parent_col_name_or_not, col_list_ins


, dest_parent_id_or_not, col_list_ins 
, source_schema, source_table
, id_col_name , source_id

, id_col_name
);

execute sql into rtn;

return  rtn;



END

$_$;


--
-- TOC entry 9350 (class 1255 OID 107712557)
-- Name: f_dictionary_sync_v02(name, name, name, name, name, text, name, bigint, bigint, boolean); Type: FUNCTION; Schema: sysma; Owner: -
--

CREATE FUNCTION f_dictionary_sync_v02(source_schema name, source_table name, dest_schema name, dest_table name, id_col_name name, col_list text, parent_col_name name, source_id bigint, dest_parent_id bigint, new_dest_id boolean DEFAULT false) RETURNS json
    LANGUAGE plpgsql
    AS $_$

/*
permet de copier une entrée du dico de sysma depuis les tables fdw vers sysma des territoires mutualisés


V02 : si new_dest_id := FALSE et id existe déjà dans le dictionnaire l'entrée ne sera pas ajoutée, et l'id existant sera retourné.

Exemple d'appel :

select sysma.f_dictionary_sync_v02(
     source_schema := 'rx000_sysma_referentiel_mutualise_fdw':: NAME
   , source_table := 'types_objetgeo_parametres':: NAME
   , dest_schema := 'sysma'::NAME
   , dest_table := 'types_objetgeo_parametres'::NAME
   , id_col_name := 'id_parametre'::NAME
   , col_list := ' parametre, description, type_donnee, regroupement, date_creation, date_modification, source_parametre, source_donnees, unite, obligatoire, col_sig, sdlt, id_modificateur, terrain, cache '::text
   , parent_col_name := 'id_type_objetgeo'::NAME
   , source_id := '100'::bigint
   , dest_parent_id := '1'::bigint 
   , new_dest_id := FALSE::boolean   )

*/

declare 
sql text;

sql_test_exist TEXT;

bigint_test_exist bigint;
rtn json;

col_list_ins TEXT;
parent_col_name_or_not text;
dest_parent_id_or_not  text;

BEGIN

IF new_dest_id IS FALSE THEN
   sql_test_exist := format  (
   $SQL$ 

   SELECT %I   -- dest_parent_id , col_list_ins
   FROM %I.%I 
   WHERE %I = %s

   $SQL$
   , id_col_name
   , dest_schema, dest_table
   , id_col_name , source_id

   );

   execute sql_test_exist into bigint_test_exist;

   IF (bigint_test_exist::bigint = source_id::bigint) THEN

   rtn :=  to_json(t) FROM (SELECT bigint_test_exist as res_id, 'ID existe deja, entrée non ajoutée' as  res_comment) t;
      
   return   rtn;


   ELSE 

   if new_dest_id IS TRUE THEN 
      col_list_ins = col_list ;
   ELSE 
      col_list_ins = id_col_name || ', '|| col_list ;
   END IF ;

   if parent_col_name IS NULL THEN 
      parent_col_name_or_not = '' ;
      dest_parent_id_or_not = '';
      
   ELSE 
      parent_col_name_or_not = parent_col_name || ' , ' ;
      dest_parent_id_or_not = dest_parent_id || ' , ';

   END IF ;


   sql := format (
   $SQL$ 
   WITH ins as (
   INSERT INTO %I.%I   -- table dest
      (%s  %s) -- parent_col_name , col_list_ins
   SELECT %s   %s  -- dest_parent_id , col_list_ins
   FROM %I.%I 
   WHERE %I = %s
   -- LIMIT 1

   RETURNING  %I as res_id, 'ajout ok'::text as res_comment

  )
  select to_json (ins) from ins
   $SQL$
   , dest_schema, dest_table
   , parent_col_name_or_not, col_list_ins


   , dest_parent_id_or_not, col_list_ins 
   , source_schema, source_table
   , id_col_name , source_id

   , id_col_name
   );

   execute sql into rtn;

   return   rtn;

   END IF;

END IF;

END

$_$;


--
-- TOC entry 9358 (class 1255 OID 115799902)
-- Name: f_dictionary_sync_v03(name, name, name, name, name, text, name, bigint, bigint, boolean, bigint); Type: FUNCTION; Schema: sysma; Owner: -
--

CREATE FUNCTION f_dictionary_sync_v03(source_schema name, source_table name, dest_schema name, dest_table name, id_col_name name, col_list text, parent_col_name name, source_id bigint, dest_parent_id bigint, new_dest_id boolean DEFAULT false, modificateur_id bigint DEFAULT 1) RETURNS json
    LANGUAGE plpgsql
    AS $_$

/*
permet de copier une entrée du dico de sysma depuis les tables fdw vers sysma des territoires mutualisés


V03 : attribue id_modificateur = modificateur_id, 1 par défaut = init sysma
V02 : si new_dest_id := FALSE et id existe déjà dans le dictionnaire l'entrée ne sera pas ajoutée, et l'id existant sera retourné.

Exemple d'appel :

select sysma.f_dictionary_sync_v03(
     source_schema := 'rx000_sysma_referentiel_mutualise_fdw':: NAME
   , source_table := 'types_objetgeo_parametres':: NAME
   , dest_schema := 'sysma'::NAME
   , dest_table := 'types_objetgeo_parametres'::NAME
   , id_col_name := 'id_parametre'::NAME
   , col_list := ' parametre, description, type_donnee, regroupement, date_creation, date_modification, source_parametre, source_donnees, unite, obligatoire, col_sig, sdlt, id_modificateur, terrain, cache '::text
   , parent_col_name := 'id_type_objetgeo'::NAME
   , source_id := '100'::bigint
   , dest_parent_id := '1'::bigint 
   , new_dest_id := FALSE::boolean  
   , modificateur_id )

*/

declare 
sql text;

sql_test_exist TEXT;

bigint_test_exist bigint;
rtn json;

col_list_ins TEXT;
parent_col_name_or_not text;
dest_parent_id_or_not  text;

BEGIN

IF new_dest_id IS FALSE THEN
   sql_test_exist := format  (
   $SQL$ 

   SELECT %I   -- dest_parent_id , col_list_ins
   FROM %I.%I 
   WHERE %I = %s

   $SQL$
   , id_col_name
   , dest_schema, dest_table
   , id_col_name , source_id

   );

   execute sql_test_exist into bigint_test_exist;

   IF (bigint_test_exist::bigint = source_id::bigint) THEN

   rtn :=  to_json(t) FROM (SELECT bigint_test_exist as res_id, 'ID existe deja, entrée non ajoutée' as  res_comment) t;
      
   return   rtn;


   ELSE 

   if new_dest_id IS TRUE THEN 
      col_list_ins = col_list ;
   ELSE 
      col_list_ins = id_col_name || ', '|| col_list ;
   END IF ;

   if parent_col_name IS NULL THEN 
      parent_col_name_or_not = '' ;
      dest_parent_id_or_not = '';
      
   ELSE 
      parent_col_name_or_not = parent_col_name || ' , ' ;
      dest_parent_id_or_not = dest_parent_id || ' , ';

   END IF ;


   sql := format (
   $SQL$ 
   WITH ins as (
   INSERT INTO %I.%I   -- table dest
      (%s  %s, id_modificateur) -- parent_col_name , col_list_ins
   SELECT %s   %s , %s -- dest_parent_id , col_list_ins, modificateur_id
   FROM %I.%I 
   WHERE %I = %s
   -- LIMIT 1

   RETURNING  %I as res_id, 'ajout ok'::text as res_comment

  )
  select to_json (ins) from ins
   $SQL$
   , dest_schema, dest_table
   , parent_col_name_or_not, col_list_ins


   , dest_parent_id_or_not, col_list_ins , modificateur_id
   , source_schema, source_table
   , id_col_name , source_id

   , id_col_name
   );

   execute sql into rtn;

   return   rtn;

   END IF;

END IF;

END

$_$;


--
-- TOC entry 9584 (class 1255 OID 177340640)
-- Name: f_test_action_parameter_is_ld(bigint); Type: FUNCTION; Schema: sysma; Owner: -
--

CREATE FUNCTION f_test_action_parameter_is_ld(parameter_id bigint) RETURNS boolean
    LANGUAGE sql IMMUTABLE
    AS $_$
   select true
   from sysma.sysma_action_type_parameter 
   where data_type  in ('textChoiceType', 'multipleTextChoiceType')
   AND parameter_id = $1
   $_$;


--
-- TOC entry 9587 (class 1255 OID 177340649)
-- Name: f_test_action_parameter_is_multild(bigint); Type: FUNCTION; Schema: sysma; Owner: -
--

CREATE FUNCTION f_test_action_parameter_is_multild(parameter_id bigint) RETURNS boolean
    LANGUAGE sql IMMUTABLE
    AS $_$
   select true
   from sysma.sysma_action_type_parameter 
   where data_type  in ( 'multipleTextChoiceType')
   AND parameter_id = $1
   $_$;


--
-- TOC entry 9583 (class 1255 OID 177340637)
-- Name: f_test_object_parameter_is_ld(bigint); Type: FUNCTION; Schema: sysma; Owner: -
--

CREATE FUNCTION f_test_object_parameter_is_ld(parameter_id bigint) RETURNS boolean
    LANGUAGE sql IMMUTABLE
    AS $_$
   select true
   from sysma.sysma_object_type_parameter 
   where data_type  in ('textChoiceType', 'multipleTextChoiceType')
   AND parameter_id = $1
   $_$;


--
-- TOC entry 9586 (class 1255 OID 177340646)
-- Name: f_test_object_parameter_is_multild(bigint); Type: FUNCTION; Schema: sysma; Owner: -
--

CREATE FUNCTION f_test_object_parameter_is_multild(parameter_id bigint) RETURNS boolean
    LANGUAGE sql IMMUTABLE
    AS $_$
   select true
   from sysma.sysma_object_type_parameter 
   where data_type  in ('multipleTextChoiceType')
   AND parameter_id = $1
   $_$;


--
-- TOC entry 9585 (class 1255 OID 177340643)
-- Name: f_test_relation_parameter_is_ld(bigint); Type: FUNCTION; Schema: sysma; Owner: -
--

CREATE FUNCTION f_test_relation_parameter_is_ld(parameter_id bigint) RETURNS boolean
    LANGUAGE sql IMMUTABLE
    AS $_$
   select true
   from sysma.sysma_relation_type_parameter 
   where data_type  in ('textChoiceType', 'multipleTextChoiceType')
   AND parameter_id = $1
   $_$;


--
-- TOC entry 9588 (class 1255 OID 177340652)
-- Name: f_test_relation_parameter_is_multild(bigint); Type: FUNCTION; Schema: sysma; Owner: -
--

CREATE FUNCTION f_test_relation_parameter_is_multild(parameter_id bigint) RETURNS boolean
    LANGUAGE sql IMMUTABLE
    AS $_$
   select true
   from sysma.sysma_relation_type_parameter 
   where data_type  in ( 'multipleTextChoiceType')
   AND parameter_id = $1
   $_$;


--
-- TOC entry 9582 (class 1255 OID 177338498)
-- Name: get_sysma_action_ids_from_action_type_id(bigint); Type: FUNCTION; Schema: sysma; Owner: -
--

CREATE FUNCTION get_sysma_action_ids_from_action_type_id(_action_id bigint) RETURNS SETOF bigint
    LANGUAGE sql
    AS $_$

/*
permet de lister les id des actions enregistrées pour un type d'action 


Exemple d'appel :

select sysma.get_sysma_action_ids_from_action_type_id(3) ;

select sysma.get_sysma_action_ids_from_action_type_id(1) ;

*/

SELECT sysma_action_id::bigint
from sysma.sysma_action
WHERE sysma_action_type_id = $1;

$_$;


--
-- TOC entry 9581 (class 1255 OID 177338497)
-- Name: get_sysma_object_ids_from_object_type_id(bigint); Type: FUNCTION; Schema: sysma; Owner: -
--

CREATE FUNCTION get_sysma_object_ids_from_object_type_id(_object_id bigint) RETURNS SETOF bigint
    LANGUAGE sql
    AS $_$

/*
permet de lister les ids des objets enregistrés pour un type d'objet 

Exemple d'appel :

select sysma.get_sysma_object_ids_from_object_type_id(1) ;




*/

SELECT sysma_object_id::bigint
from sysma.sysma_object
WHERE sysma_object_type_id = $1;

$_$;


--
-- TOC entry 9265 (class 1255 OID 74464105)
-- Name: trg_f_01_set_default_dates_values(); Type: FUNCTION; Schema: sysma; Owner: -
--

CREATE FUNCTION trg_f_01_set_default_dates_values() RETURNS trigger
    LANGUAGE plpgsql
    AS $$

/*
Correction of date values
*/
begin
  IF TG_OP = 'INSERT' OR TG_OP = 'UPDATE' THEN
   IF new.end_date ='' OR new.end_date is null
    THEN new.end_date = '0';
   END IF;
  return new;
 
  END IF;

  
end;
$$;


--
-- TOC entry 8915 (class 1255 OID 7561786)
-- Name: trg_f_track_historiquetables(); Type: FUNCTION; Schema: sysma; Owner: -
--

CREATE FUNCTION trg_f_track_historiquetables() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
begin
  IF TG_OP = 'INSERT' THEN
  
  INSERT INTO sysma.x99_t_track_historytables(
       change_op, schema_name, table_name, row_id_track_historytables, 
       modified_by, new_row)
   SELECT 
       TG_OP,
       'sysma',
       TG_TABLE_NAME,
       new.track_historytables_id,
       new.modified_by,
       row_to_json(new);
   

  return new;
  

  ELSIF TG_OP = 'UPDATE' THEN
    INSERT INTO sysma.x99_t_track_historytables(
       change_op, schema_name, table_name, row_id_track_historytables, 
       modified_by, old_row, new_row)
   SELECT 
       TG_OP,
       'sysma',
       TG_TABLE_NAME,
       new.track_historytables_id,
       new.modified_by,
       row_to_json(old),
       row_to_json(new);
   

  return new;
  

 ELSIF TG_OP = 'DELETE' THEN
    INSERT INTO sysma.x99_t_track_historytables(
       change_op, schema_name, table_name, row_id_track_historytables, 
       modified_by, old_row)
   SELECT 
       TG_OP,
       'sysma', 
       TG_TABLE_NAME,
       old.track_historytables_id,
       NULL,
       row_to_json(old);

   

  return old;
  END IF;

  
end;
$$;


--
-- TOC entry 1407 (class 1259 OID 7569067)
-- Name: increment_sysma_entity_seq; Type: SEQUENCE; Schema: sysma; Owner: -
--

CREATE SEQUENCE increment_sysma_entity_seq
    START WITH 100000
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 29457 (class 0 OID 0)
-- Dependencies: 1407
-- Name: SEQUENCE increment_sysma_entity_seq; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON SEQUENCE increment_sysma_entity_seq IS 'Est ppelée pour incrémenter toutes les tables de sysma';


--
-- TOC entry 1406 (class 1259 OID 7569065)
-- Name: increment_x99_t_track_historytables; Type: SEQUENCE; Schema: sysma; Owner: -
--

CREATE SEQUENCE increment_x99_t_track_historytables
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 29458 (class 0 OID 0)
-- Dependencies: 1406
-- Name: SEQUENCE increment_x99_t_track_historytables; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON SEQUENCE increment_x99_t_track_historytables IS 'Est appelée pour incrémenter un id_track_historiquetables pour le suivi de toutes les tables';


SET default_with_oids = false;

--
-- TOC entry 1408 (class 1259 OID 7569069)
-- Name: l_sysma_object_sysma_action; Type: TABLE; Schema: sysma; Owner: -
--

CREATE TABLE l_sysma_object_sysma_action (
    sysma_object_id bigint NOT NULL,
    sysma_action_id bigint NOT NULL,
    track_historytables_id bigint DEFAULT nextval('increment_x99_t_track_historytables'::regclass) NOT NULL,
    modified_by bigint
);


--
-- TOC entry 29459 (class 0 OID 0)
-- Dependencies: 1408
-- Name: TABLE l_sysma_object_sysma_action; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON TABLE l_sysma_object_sysma_action IS '{
   "display_groupe_order":500,
   "groupe_name":"Données saisies",
   "table_comment":"Liaison en les objets cre et les travaux qui s''y appliquent"
}';


--
-- TOC entry 29460 (class 0 OID 0)
-- Dependencies: 1408
-- Name: COLUMN l_sysma_object_sysma_action.sysma_object_id; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN l_sysma_object_sysma_action.sysma_object_id IS 'identifiant de l''objet CRE';


--
-- TOC entry 29461 (class 0 OID 0)
-- Dependencies: 1408
-- Name: COLUMN l_sysma_object_sysma_action.sysma_action_id; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN l_sysma_object_sysma_action.sysma_action_id IS 'identifiant des travaux';


--
-- TOC entry 29462 (class 0 OID 0)
-- Dependencies: 1408
-- Name: COLUMN l_sysma_object_sysma_action.track_historytables_id; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN l_sysma_object_sysma_action.track_historytables_id IS 'identifiant unique de ligne utilisé pour le suivi historique des enregistrement dans la table x99_track_historiquetables';


--
-- TOC entry 1410 (class 1259 OID 7569085)
-- Name: organisation; Type: TABLE; Schema: sysma; Owner: -
--

CREATE TABLE organisation (
    organisation_id integer NOT NULL,
    organisation character varying(250),
    acronym character varying(50),
    track_historytables_id bigint DEFAULT nextval('increment_x99_t_track_historytables'::regclass) NOT NULL
);


--
-- TOC entry 29463 (class 0 OID 0)
-- Dependencies: 1410
-- Name: TABLE organisation; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON TABLE organisation IS '{
   "display_groupe_order":125,
   "groupe_name":"Configuration de sysma",
   "table_comment":"Liste des structures intervenant dans la gestion des milieux aquatiques et pouvant être maître d''ouvrage"
 }';


--
-- TOC entry 29464 (class 0 OID 0)
-- Dependencies: 1410
-- Name: COLUMN organisation.organisation_id; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN organisation.organisation_id IS 'identifiant de la structure';


--
-- TOC entry 29465 (class 0 OID 0)
-- Dependencies: 1410
-- Name: COLUMN organisation.organisation; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN organisation.organisation IS 'intitulé de la structure';


--
-- TOC entry 29466 (class 0 OID 0)
-- Dependencies: 1410
-- Name: COLUMN organisation.acronym; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN organisation.acronym IS 'sigle de la structure';


--
-- TOC entry 29467 (class 0 OID 0)
-- Dependencies: 1410
-- Name: COLUMN organisation.track_historytables_id; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN organisation.track_historytables_id IS 'identifiant unique de ligne utilisé pour le suivi historique des enregistrement dans la table x99_track_historiquetables';


--
-- TOC entry 1411 (class 1259 OID 7569089)
-- Name: sysma_action; Type: TABLE; Schema: sysma; Owner: -
--

CREATE TABLE sysma_action (
    sysma_action_id bigint DEFAULT nextval('increment_sysma_entity_seq'::regclass) NOT NULL,
    sysma_action character varying(250),
    sysma_action_type_id bigint NOT NULL,
    start_date character varying(15),
    end_date character varying(15),
    organisation_id bigint,
    created_by bigint,
    program_year integer,
    status character varying(20),
    created_at timestamp without time zone DEFAULT now(),
    modified_by integer,
    modified_at timestamp without time zone DEFAULT now(),
    track_historytables_id bigint DEFAULT nextval('increment_x99_t_track_historytables'::regclass) NOT NULL,
    contracts character varying[],
    unit_cost_thesaurus numeric,
    unit_cost_estimated_user numeric,
    total_cost_estimated_user numeric,
    unit_measure_estimated_user character varying,
    total_cost_final_user numeric
);


--
-- TOC entry 29468 (class 0 OID 0)
-- Dependencies: 1411
-- Name: TABLE sysma_action; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON TABLE sysma_action IS '{
   "display_groupe_order":500,
   "groupe_name":"Données saisies",
   "table_comment":"Les travaux menés sur les objets physiques"
}';


--
-- TOC entry 29469 (class 0 OID 0)
-- Dependencies: 1411
-- Name: COLUMN sysma_action.sysma_action_id; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_action.sysma_action_id IS 'identifiant des travaux';


--
-- TOC entry 29470 (class 0 OID 0)
-- Dependencies: 1411
-- Name: COLUMN sysma_action.sysma_action; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_action.sysma_action IS 'intitulé des travaux';


--
-- TOC entry 29471 (class 0 OID 0)
-- Dependencies: 1411
-- Name: COLUMN sysma_action.sysma_action_type_id; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_action.sysma_action_type_id IS 'id du type de travaux';


--
-- TOC entry 29472 (class 0 OID 0)
-- Dependencies: 1411
-- Name: COLUMN sysma_action.start_date; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_action.start_date IS 'date de début des travaux';


--
-- TOC entry 29473 (class 0 OID 0)
-- Dependencies: 1411
-- Name: COLUMN sysma_action.end_date; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_action.end_date IS 'date de fin des travaux';


--
-- TOC entry 29474 (class 0 OID 0)
-- Dependencies: 1411
-- Name: COLUMN sysma_action.organisation_id; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_action.organisation_id IS 'id de la structure maître d''ouvrage des travaux';


--
-- TOC entry 29475 (class 0 OID 0)
-- Dependencies: 1411
-- Name: COLUMN sysma_action.created_by; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_action.created_by IS 'id de la personne ajoutant l''info dans la bdd';


--
-- TOC entry 29476 (class 0 OID 0)
-- Dependencies: 1411
-- Name: COLUMN sysma_action.program_year; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_action.program_year IS 'année de programmation des travaux dans le cre';


--
-- TOC entry 29477 (class 0 OID 0)
-- Dependencies: 1411
-- Name: COLUMN sysma_action.status; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_action.status IS 'projet, en cours ou terminé';


--
-- TOC entry 29478 (class 0 OID 0)
-- Dependencies: 1411
-- Name: COLUMN sysma_action.created_at; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_action.created_at IS 'date de création de la fiche travaux';


--
-- TOC entry 29479 (class 0 OID 0)
-- Dependencies: 1411
-- Name: COLUMN sysma_action.track_historytables_id; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_action.track_historytables_id IS 'identifiant unique de ligne utilisé pour le suivi historique des enregistrement dans la table x99_track_historiquetables';


--
-- TOC entry 1413 (class 1259 OID 7569109)
-- Name: sysma_action_type; Type: TABLE; Schema: sysma; Owner: -
--

CREATE TABLE sysma_action_type (
    sysma_action_type_id bigint DEFAULT nextval('increment_sysma_entity_seq'::regclass) NOT NULL,
    sysma_action_type character varying(250),
    description text,
    sysma_object_type_id bigint,
    modified_at date DEFAULT now(),
    created_at date DEFAULT now(),
    style json,
    sysma_action_type_alias character varying,
    track_historytables_id bigint DEFAULT nextval('increment_x99_t_track_historytables'::regclass) NOT NULL,
    modified_by bigint NOT NULL,
    zoom_min integer DEFAULT 0,
    zoom_max integer DEFAULT 20,
    load_on_pan integer DEFAULT 0,
    unit_cost numeric DEFAULT 0,
    unit_measure character varying,
    dig_justification text,
    dig_incidences_phase_travaux text,
    dig_incidences_fonctionnement text,
    dig_prescriptions_et_mesures_accompagnement text,
    dig_reference_nomenclature text,
    extra_data jsonb
);


--
-- TOC entry 29480 (class 0 OID 0)
-- Dependencies: 1413
-- Name: TABLE sysma_action_type; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON TABLE sysma_action_type IS '{
   "display_groupe_order":300,
   "groupe_name":"Gestion du dictionnaire de données",
   "table_comment":"Liste des types de travaux pouvant être menés"
}';


--
-- TOC entry 29481 (class 0 OID 0)
-- Dependencies: 1413
-- Name: COLUMN sysma_action_type.sysma_action_type_id; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_action_type.sysma_action_type_id IS 'identifiant du type de travaux';


--
-- TOC entry 29482 (class 0 OID 0)
-- Dependencies: 1413
-- Name: COLUMN sysma_action_type.sysma_action_type; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_action_type.sysma_action_type IS 'nom du type de travaux';


--
-- TOC entry 29483 (class 0 OID 0)
-- Dependencies: 1413
-- Name: COLUMN sysma_action_type.description; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_action_type.description IS 'description détaillée du type de travaux';


--
-- TOC entry 29484 (class 0 OID 0)
-- Dependencies: 1413
-- Name: COLUMN sysma_action_type.sysma_object_type_id; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_action_type.sysma_object_type_id IS 'id du type d''objet cre sur lesquels le type de travaux s''applique';


--
-- TOC entry 29485 (class 0 OID 0)
-- Dependencies: 1413
-- Name: COLUMN sysma_action_type.modified_at; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_action_type.modified_at IS 'date de la dernière modification du type';


--
-- TOC entry 29486 (class 0 OID 0)
-- Dependencies: 1413
-- Name: COLUMN sysma_action_type.style; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_action_type.style IS 'style par defaut du type de travaux';


--
-- TOC entry 29487 (class 0 OID 0)
-- Dependencies: 1413
-- Name: COLUMN sysma_action_type.track_historytables_id; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_action_type.track_historytables_id IS 'identifiant unique de ligne utilisé pour le suivi historique des enregistrement dans la table x99_track_historiquetables';


--
-- TOC entry 29488 (class 0 OID 0)
-- Dependencies: 1413
-- Name: COLUMN sysma_action_type.extra_data; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_action_type.extra_data IS 'json data used by plugins or others functions';


--
-- TOC entry 1409 (class 1259 OID 7569073)
-- Name: sysma_object; Type: TABLE; Schema: sysma; Owner: -
--

CREATE TABLE sysma_object (
    sysma_object_id bigint DEFAULT nextval('increment_sysma_entity_seq'::regclass) NOT NULL,
    start_date character varying(15),
    end_date character varying(15),
    organisation_id bigint,
    created_by bigint,
    status character varying(20),
    sysma_object_type_id bigint NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    sysma_object character varying(255),
    geom public.geometry(Geometry,2154),
    modified_by integer,
    modified_at timestamp without time zone DEFAULT now(),
    track_historytables_id bigint DEFAULT nextval('increment_x99_t_track_historytables'::regclass) NOT NULL,
    CONSTRAINT enforce_geotype_geometrie CHECK (((public.st_geometrytype(geom) = ANY (ARRAY['ST_Point'::text, 'ST_LineString'::text, 'ST_Polygon'::text])) OR (geom IS NULL)))
);


--
-- TOC entry 29489 (class 0 OID 0)
-- Dependencies: 1409
-- Name: TABLE sysma_object; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON TABLE sysma_object IS '{
   "display_groupe_order":500,
   "groupe_name":"Données saisies",
   "table_comment":"Les objets géographiques positionnables sur une carte (physiques)"
}';


--
-- TOC entry 29490 (class 0 OID 0)
-- Dependencies: 1409
-- Name: COLUMN sysma_object.sysma_object_id; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_object.sysma_object_id IS 'identifiant de l''objet';


--
-- TOC entry 29491 (class 0 OID 0)
-- Dependencies: 1409
-- Name: COLUMN sysma_object.start_date; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_object.start_date IS 'date de création sur le terrain';


--
-- TOC entry 29492 (class 0 OID 0)
-- Dependencies: 1409
-- Name: COLUMN sysma_object.end_date; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_object.end_date IS 'date de suppression sur le terratin';


--
-- TOC entry 29493 (class 0 OID 0)
-- Dependencies: 1409
-- Name: COLUMN sysma_object.organisation_id; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_object.organisation_id IS 'id de la structure maître d''ouvrage de la création de l''objet dans le cas où il s''agit d''objets installés lors de travaux';


--
-- TOC entry 29494 (class 0 OID 0)
-- Dependencies: 1409
-- Name: COLUMN sysma_object.created_by; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_object.created_by IS 'id du créateur de l''objet dans la base de données';


--
-- TOC entry 29495 (class 0 OID 0)
-- Dependencies: 1409
-- Name: COLUMN sysma_object.status; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_object.status IS 'statut de l''objet (à préciser)';


--
-- TOC entry 29496 (class 0 OID 0)
-- Dependencies: 1409
-- Name: COLUMN sysma_object.sysma_object_type_id; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_object.sysma_object_type_id IS 'id du type d''objet cre';


--
-- TOC entry 29497 (class 0 OID 0)
-- Dependencies: 1409
-- Name: COLUMN sysma_object.created_at; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_object.created_at IS 'date de création de l''objet dans la base de données';


--
-- TOC entry 29498 (class 0 OID 0)
-- Dependencies: 1409
-- Name: COLUMN sysma_object.sysma_object; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_object.sysma_object IS 'nom de l''objet géographique';


--
-- TOC entry 29499 (class 0 OID 0)
-- Dependencies: 1409
-- Name: COLUMN sysma_object.geom; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_object.geom IS 'géométrie de l''objet en 2154';


--
-- TOC entry 29500 (class 0 OID 0)
-- Dependencies: 1409
-- Name: COLUMN sysma_object.modified_by; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_object.modified_by IS 'id du dernier user ayant modifié l''objet';


--
-- TOC entry 29501 (class 0 OID 0)
-- Dependencies: 1409
-- Name: COLUMN sysma_object.modified_at; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_object.modified_at IS 'date de la dernière modification de l''objet';


--
-- TOC entry 29502 (class 0 OID 0)
-- Dependencies: 1409
-- Name: COLUMN sysma_object.track_historytables_id; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_object.track_historytables_id IS 'identifiant unique de ligne utilisé pour le suivi historique des enregistrement dans la table x99_track_historiquetables';


--
-- TOC entry 1412 (class 1259 OID 7569099)
-- Name: sysma_object_type; Type: TABLE; Schema: sysma; Owner: -
--

CREATE TABLE sysma_object_type (
    sysma_object_type_id bigint DEFAULT nextval('increment_sysma_entity_seq'::regclass) NOT NULL,
    sysma_object_type character varying(250),
    description text,
    geometry_type character varying(50),
    created_at date DEFAULT now(),
    modified_at date DEFAULT now(),
    data_source text,
    definition_source text,
    digitization_layer character varying(50),
    style json,
    sysma_object_type_alias character varying(100) NOT NULL,
    group_alias character varying(50),
    track_historytables_id bigint DEFAULT nextval('increment_x99_t_track_historytables'::regclass) NOT NULL,
    modified_by bigint NOT NULL,
    zoom_min integer DEFAULT 0,
    zoom_max integer DEFAULT 20,
    load_on_pan integer DEFAULT 0,
    hidden integer DEFAULT 0 NOT NULL
);


--
-- TOC entry 29503 (class 0 OID 0)
-- Dependencies: 1412
-- Name: TABLE sysma_object_type; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON TABLE sysma_object_type IS '{
   "display_groupe_order":300,
   "groupe_name":"Gestion du dictionnaire de données",
   "table_comment":"Types d''objets CRE renseignables dans la BDD, équivaut en terme de SIG aux couches"
}';


--
-- TOC entry 29504 (class 0 OID 0)
-- Dependencies: 1412
-- Name: COLUMN sysma_object_type.sysma_object_type_id; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_object_type.sysma_object_type_id IS 'id du type d''objet cre';


--
-- TOC entry 29505 (class 0 OID 0)
-- Dependencies: 1412
-- Name: COLUMN sysma_object_type.sysma_object_type; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_object_type.sysma_object_type IS 'intitulé de l''objet cre';


--
-- TOC entry 29506 (class 0 OID 0)
-- Dependencies: 1412
-- Name: COLUMN sysma_object_type.description; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_object_type.description IS 'description du type d''objet geo';


--
-- TOC entry 29507 (class 0 OID 0)
-- Dependencies: 1412
-- Name: COLUMN sysma_object_type.geometry_type; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_object_type.geometry_type IS 'polygone, polyligne ou point';


--
-- TOC entry 29508 (class 0 OID 0)
-- Dependencies: 1412
-- Name: COLUMN sysma_object_type.created_at; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_object_type.created_at IS 'date de création du type';


--
-- TOC entry 29509 (class 0 OID 0)
-- Dependencies: 1412
-- Name: COLUMN sysma_object_type.modified_at; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_object_type.modified_at IS 'date de modification du type';


--
-- TOC entry 29510 (class 0 OID 0)
-- Dependencies: 1412
-- Name: COLUMN sysma_object_type.data_source; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_object_type.data_source IS 'source de la donnée, peut être multiple';


--
-- TOC entry 29511 (class 0 OID 0)
-- Dependencies: 1412
-- Name: COLUMN sysma_object_type.definition_source; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_object_type.definition_source IS 'source de la définition du type d''objet géo';


--
-- TOC entry 29512 (class 0 OID 0)
-- Dependencies: 1412
-- Name: COLUMN sysma_object_type.digitization_layer; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_object_type.digitization_layer IS 'support de numérisation des objets (fond de carte)';


--
-- TOC entry 29513 (class 0 OID 0)
-- Dependencies: 1412
-- Name: COLUMN sysma_object_type.style; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_object_type.style IS 'style par défaut du type d''objet au format json';


--
-- TOC entry 29514 (class 0 OID 0)
-- Dependencies: 1412
-- Name: COLUMN sysma_object_type.track_historytables_id; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_object_type.track_historytables_id IS 'identifiant unique de ligne utilisé pour le suivi historique des enregistrement dans la table x99_track_historiquetables';


--
-- TOC entry 1414 (class 1259 OID 7569118)
-- Name: user; Type: TABLE; Schema: sysma; Owner: -
--

CREATE TABLE "user" (
    user_id integer NOT NULL,
    firstname character varying,
    lastname character varying,
    "user" character varying,
    registration_date character varying,
    organisation_id integer,
    default_center character varying DEFAULT 'LatLng(46.9, -0.89)'::character varying,
    base_layer character varying DEFAULT 'OpenStreetMap'::character varying,
    password text,
    last_access timestamp without time zone,
    default_zoom integer DEFAULT 10,
    map_width integer DEFAULT 60,
    track_historytables_id bigint DEFAULT nextval('increment_x99_t_track_historytables'::regclass) NOT NULL,
    modified_by bigint NOT NULL,
    privileges character varying[],
    active boolean DEFAULT true,
    user_schema character varying,
    snap_options json DEFAULT '{"active":false,"layer_id":null,"distance":15}'::json
);


--
-- TOC entry 29515 (class 0 OID 0)
-- Dependencies: 1414
-- Name: TABLE "user"; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON TABLE "user" IS '{
   "display_groupe_order":200,
   "groupe_name":"Gestion des utilisateurs",
   "table_comment":"Liste des utilisateurs et contributeurs à la bdd"
}
';


--
-- TOC entry 29516 (class 0 OID 0)
-- Dependencies: 1414
-- Name: COLUMN "user"."user"; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN "user"."user" IS 'login';


--
-- TOC entry 29517 (class 0 OID 0)
-- Dependencies: 1414
-- Name: COLUMN "user".organisation_id; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN "user".organisation_id IS 'Attention ce champ doit être modifié (renommé, retypé en texte ?).
identitiant de la structure de l''utilisateur dans la BD contact (purement interne à l''EPTBSN)?. Ne pas considérer qu''il y a un lien lien avec le table sysma id structure.
';


--
-- TOC entry 29518 (class 0 OID 0)
-- Dependencies: 1414
-- Name: COLUMN "user".default_center; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN "user".default_center IS 'coordonnées du centre de la carte du user (EPSG 4326)';


--
-- TOC entry 29519 (class 0 OID 0)
-- Dependencies: 1414
-- Name: COLUMN "user".track_historytables_id; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN "user".track_historytables_id IS 'identifiant unique de ligne utilisé pour le suivi historique des enregistrement dans la table x99_track_historiquetables';


--
-- TOC entry 29520 (class 0 OID 0)
-- Dependencies: 1414
-- Name: COLUMN "user".privileges; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN "user".privileges IS 'droits admin et cadastre uniquement';


--
-- TOC entry 29521 (class 0 OID 0)
-- Dependencies: 1414
-- Name: COLUMN "user".user_schema; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN "user".user_schema IS 'PG schema used for user layers imports';


--
-- TOC entry 1415 (class 1259 OID 7569144)
-- Name: sysma_object_data; Type: TABLE; Schema: sysma; Owner: -
--

CREATE TABLE sysma_object_data (
    sysma_object_id bigint NOT NULL,
    parameter_id bigint NOT NULL,
    start_date character varying(15),
    end_date character varying(15) NOT NULL,
    value text NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    user_id bigint,
    data_index bigint NOT NULL,
    sysma_object_data_id bigint DEFAULT nextval('increment_sysma_entity_seq'::regclass) NOT NULL,
    modified_by bigint,
    modified_at timestamp without time zone DEFAULT now(),
    track_historytables_id bigint DEFAULT nextval('increment_x99_t_track_historytables'::regclass) NOT NULL
);


--
-- TOC entry 29522 (class 0 OID 0)
-- Dependencies: 1415
-- Name: TABLE sysma_object_data; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON TABLE sysma_object_data IS '{
   "display_groupe_order":500,
   "groupe_name":"Données saisies",
   "table_comment":"Contient les données pour chaque paramètre descriptif des objets"
}';


--
-- TOC entry 29523 (class 0 OID 0)
-- Dependencies: 1415
-- Name: COLUMN sysma_object_data.sysma_object_id; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_object_data.sysma_object_id IS 'identifiant de l''objet CRE';


--
-- TOC entry 29524 (class 0 OID 0)
-- Dependencies: 1415
-- Name: COLUMN sysma_object_data.parameter_id; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_object_data.parameter_id IS 'identifiant du paramètre descriptif de l''objet cre';


--
-- TOC entry 29525 (class 0 OID 0)
-- Dependencies: 1415
-- Name: COLUMN sysma_object_data.start_date; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_object_data.start_date IS 'date de début de validité de l''information';


--
-- TOC entry 29526 (class 0 OID 0)
-- Dependencies: 1415
-- Name: COLUMN sysma_object_data.end_date; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_object_data.end_date IS 'date de fin de validité de l''information';


--
-- TOC entry 29527 (class 0 OID 0)
-- Dependencies: 1415
-- Name: COLUMN sysma_object_data.value; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_object_data.value IS 'contient la valeur du parametre';


--
-- TOC entry 29528 (class 0 OID 0)
-- Dependencies: 1415
-- Name: COLUMN sysma_object_data.created_at; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_object_data.created_at IS 'date d''ajout de l''info dans la bdd';


--
-- TOC entry 29529 (class 0 OID 0)
-- Dependencies: 1415
-- Name: COLUMN sysma_object_data.user_id; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_object_data.user_id IS 'utilisateur ayant ajouté cette donnée';


--
-- TOC entry 29530 (class 0 OID 0)
-- Dependencies: 1415
-- Name: COLUMN sysma_object_data.data_index; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_object_data.data_index IS 'index de la table';


--
-- TOC entry 29531 (class 0 OID 0)
-- Dependencies: 1415
-- Name: COLUMN sysma_object_data.sysma_object_data_id; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_object_data.sysma_object_data_id IS 'pk de la table';


--
-- TOC entry 29532 (class 0 OID 0)
-- Dependencies: 1415
-- Name: COLUMN sysma_object_data.modified_by; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_object_data.modified_by IS 'dernier utilisateur ayant modifié la donnée ou créateur s''il n''y a pas eu de modification';


--
-- TOC entry 29533 (class 0 OID 0)
-- Dependencies: 1415
-- Name: COLUMN sysma_object_data.track_historytables_id; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_object_data.track_historytables_id IS 'identifiant unique de ligne utilisé pour le suivi historique des enregistrement dans la table x99_track_historiquetables';


--
-- TOC entry 2471 (class 1259 OID 7574768)
-- Name: sysma_object_type_parameter; Type: TABLE; Schema: sysma; Owner: -
--

CREATE TABLE sysma_object_type_parameter (
    parameter_id bigint DEFAULT nextval('increment_sysma_entity_seq'::regclass) NOT NULL,
    sysma_object_type_id bigint NOT NULL,
    parameter character varying(250),
    description text,
    data_type character varying(50),
    ordered_by character varying(250),
    created_at date,
    modified_at date,
    parameter_source text,
    data_source text,
    unit character varying(50),
    required smallint,
    parameter_alias character varying(50),
    time_tracking smallint,
    track_historytables_id bigint DEFAULT nextval('increment_x99_t_track_historytables'::regclass) NOT NULL,
    modified_by bigint NOT NULL,
    required_field_parameter character varying,
    hidden_in_form character varying
);


--
-- TOC entry 29534 (class 0 OID 0)
-- Dependencies: 2471
-- Name: TABLE sysma_object_type_parameter; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON TABLE sysma_object_type_parameter IS '{
   "display_groupe_order":300,
   "groupe_name":"Gestion du dictionnaire de données",
   "table_comment":"Liste des paramètres pour chaque type d''objet"
}';


--
-- TOC entry 29535 (class 0 OID 0)
-- Dependencies: 2471
-- Name: COLUMN sysma_object_type_parameter.parameter_id; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_object_type_parameter.parameter_id IS 'identifiant du paramètre';


--
-- TOC entry 29536 (class 0 OID 0)
-- Dependencies: 2471
-- Name: COLUMN sysma_object_type_parameter.sysma_object_type_id; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_object_type_parameter.sysma_object_type_id IS 'identifiant du type d''objet cre';


--
-- TOC entry 29537 (class 0 OID 0)
-- Dependencies: 2471
-- Name: COLUMN sysma_object_type_parameter.parameter; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_object_type_parameter.parameter IS 'nom du paramètre';


--
-- TOC entry 29538 (class 0 OID 0)
-- Dependencies: 2471
-- Name: COLUMN sysma_object_type_parameter.description; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_object_type_parameter.description IS 'description détaillée du parametre';


--
-- TOC entry 29539 (class 0 OID 0)
-- Dependencies: 2471
-- Name: COLUMN sysma_object_type_parameter.data_type; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_object_type_parameter.data_type IS 'type de donnée : texte, entier, flottant etc.';


--
-- TOC entry 29540 (class 0 OID 0)
-- Dependencies: 2471
-- Name: COLUMN sysma_object_type_parameter.ordered_by; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_object_type_parameter.ordered_by IS 'regroupement possible pour la présentation des paramètres';


--
-- TOC entry 29541 (class 0 OID 0)
-- Dependencies: 2471
-- Name: COLUMN sysma_object_type_parameter.created_at; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_object_type_parameter.created_at IS 'date de création du parametre dans la base de données';


--
-- TOC entry 29542 (class 0 OID 0)
-- Dependencies: 2471
-- Name: COLUMN sysma_object_type_parameter.modified_at; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_object_type_parameter.modified_at IS 'date de modification du paramètre';


--
-- TOC entry 29543 (class 0 OID 0)
-- Dependencies: 2471
-- Name: COLUMN sysma_object_type_parameter.parameter_source; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_object_type_parameter.parameter_source IS 'étude ayant définie ce paramètre';


--
-- TOC entry 29544 (class 0 OID 0)
-- Dependencies: 2471
-- Name: COLUMN sysma_object_type_parameter.data_source; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_object_type_parameter.data_source IS 'source des données, complété au fil du temps';


--
-- TOC entry 29545 (class 0 OID 0)
-- Dependencies: 2471
-- Name: COLUMN sysma_object_type_parameter.unit; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_object_type_parameter.unit IS 'unité';


--
-- TOC entry 29546 (class 0 OID 0)
-- Dependencies: 2471
-- Name: COLUMN sysma_object_type_parameter.required; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_object_type_parameter.required IS 'champ obligatoire ou non';


--
-- TOC entry 29547 (class 0 OID 0)
-- Dependencies: 2471
-- Name: COLUMN sysma_object_type_parameter.parameter_alias; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_object_type_parameter.parameter_alias IS 'equivalence du parametre dans un intitule de colonne de table sig';


--
-- TOC entry 29548 (class 0 OID 0)
-- Dependencies: 2471
-- Name: COLUMN sysma_object_type_parameter.time_tracking; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_object_type_parameter.time_tracking IS 'Suivi dans le temps';


--
-- TOC entry 29549 (class 0 OID 0)
-- Dependencies: 2471
-- Name: COLUMN sysma_object_type_parameter.track_historytables_id; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_object_type_parameter.track_historytables_id IS 'identifiant unique de ligne utilisé pour le suivi historique des enregistrement dans la table x99_track_historiquetables';


--
-- TOC entry 29550 (class 0 OID 0)
-- Dependencies: 2471
-- Name: COLUMN sysma_object_type_parameter.required_field_parameter; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_object_type_parameter.required_field_parameter IS 'oui obligatoire, oui, non';


--
-- TOC entry 3036 (class 1259 OID 7577384)
-- Name: connection; Type: TABLE; Schema: sysma; Owner: -
--

CREATE TABLE connection (
    user_id integer,
    "user" character varying(100),
    app character varying(25),
    connection_date date,
    connection_time time without time zone
);


--
-- TOC entry 29551 (class 0 OID 0)
-- Dependencies: 3036
-- Name: TABLE connection; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON TABLE connection IS '{
   "display_groupe_order":600,
   "groupe_name":"Surveillance",
   "table_comment":"Suivi des connexions des utilisateurs"
}';


--
-- TOC entry 29552 (class 0 OID 0)
-- Dependencies: 3036
-- Name: COLUMN connection."user"; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN connection."user" IS 'login';


--
-- TOC entry 29553 (class 0 OID 0)
-- Dependencies: 3036
-- Name: COLUMN connection.app; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN connection.app IS 'appli concernée';


--
-- TOC entry 3037 (class 1259 OID 7577388)
-- Name: contract; Type: TABLE; Schema: sysma; Owner: -
--

CREATE TABLE contract (
    contract_id integer NOT NULL,
    contract character varying,
    display_order integer,
    track_historytables_id bigint DEFAULT nextval('increment_x99_t_track_historytables'::regclass) NOT NULL
);


--
-- TOC entry 29554 (class 0 OID 0)
-- Dependencies: 3037
-- Name: TABLE contract; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON TABLE contract IS '{
   "display_groupe_order":130,
   "groupe_name":"Configuration de sysma",
   "table_comment":"Liste des contrats"
}';


--
-- TOC entry 29555 (class 0 OID 0)
-- Dependencies: 3037
-- Name: COLUMN contract.track_historytables_id; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN contract.track_historytables_id IS 'identifiant unique de ligne utilisé pour le suivi historique des enregistrement dans la table x99_track_historiquetables';


--
-- TOC entry 3038 (class 1259 OID 7577394)
-- Name: contract_contract_id_seq; Type: SEQUENCE; Schema: sysma; Owner: -
--

CREATE SEQUENCE contract_contract_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 29556 (class 0 OID 0)
-- Dependencies: 3038
-- Name: contract_contract_id_seq; Type: SEQUENCE OWNED BY; Schema: sysma; Owner: -
--

ALTER SEQUENCE contract_contract_id_seq OWNED BY contract.contract_id;


--
-- TOC entry 5433 (class 1259 OID 68959236)
-- Name: data_type_choice_data_type_id_seq; Type: SEQUENCE; Schema: sysma; Owner: -
--

CREATE SEQUENCE data_type_choice_data_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3035 (class 1259 OID 7577377)
-- Name: data_type_choice; Type: TABLE; Schema: sysma; Owner: -
--

CREATE TABLE data_type_choice (
    data_type_id integer DEFAULT nextval('data_type_choice_data_type_id_seq'::regclass) NOT NULL,
    data_type character varying(100),
    description text,
    track_historytables_id bigint DEFAULT nextval('increment_x99_t_track_historytables'::regclass) NOT NULL
);


--
-- TOC entry 29557 (class 0 OID 0)
-- Dependencies: 3035
-- Name: TABLE data_type_choice; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON TABLE data_type_choice IS '{
   "display_groupe_order":300,
   "groupe_name":"Gestion du dictionnaire de données",
   "table_comment":"Liste des choix de types de données disponibles"
}';


--
-- TOC entry 29558 (class 0 OID 0)
-- Dependencies: 3035
-- Name: COLUMN data_type_choice.data_type; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN data_type_choice.data_type IS 'intitulé du type';


--
-- TOC entry 29559 (class 0 OID 0)
-- Dependencies: 3035
-- Name: COLUMN data_type_choice.description; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN data_type_choice.description IS 'description du type';


--
-- TOC entry 29560 (class 0 OID 0)
-- Dependencies: 3035
-- Name: COLUMN data_type_choice.track_historytables_id; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN data_type_choice.track_historytables_id IS 'identifiant unique de ligne utilisé pour le suivi historique des enregistrement dans la table x99_track_historiquetables';


--
-- TOC entry 3040 (class 1259 OID 7577421)
-- Name: filter; Type: TABLE; Schema: sysma; Owner: -
--

CREATE TABLE filter (
    filter_id integer NOT NULL,
    sql text,
    object_type character varying,
    filter character varying
);


--
-- TOC entry 29561 (class 0 OID 0)
-- Dependencies: 3040
-- Name: TABLE filter; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON TABLE filter IS '{
   "display_groupe_order":150,
   "groupe_name":"Configuration de sysma",
   "table_comment":"Liste de filtres possibles à appliquer sur les type d''objet ou types de travaux"
}';


--
-- TOC entry 29562 (class 0 OID 0)
-- Dependencies: 3040
-- Name: COLUMN filter.sql; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN filter.sql IS 'code sql du filtre';


--
-- TOC entry 29563 (class 0 OID 0)
-- Dependencies: 3040
-- Name: COLUMN filter.object_type; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN filter.object_type IS 'le filtre d''applique sur les objets ou travaux';


--
-- TOC entry 29564 (class 0 OID 0)
-- Dependencies: 3040
-- Name: COLUMN filter.filter; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN filter.filter IS 'intitulé du filtre';


--
-- TOC entry 3041 (class 1259 OID 7577427)
-- Name: filter_filter_id_seq; Type: SEQUENCE; Schema: sysma; Owner: -
--

CREATE SEQUENCE filter_filter_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 29565 (class 0 OID 0)
-- Dependencies: 3041
-- Name: filter_filter_id_seq; Type: SEQUENCE OWNED BY; Schema: sysma; Owner: -
--

ALTER SEQUENCE filter_filter_id_seq OWNED BY filter.filter_id;


--
-- TOC entry 3058 (class 1259 OID 7577524)
-- Name: import_report; Type: TABLE; Schema: sysma; Owner: -
--

CREATE TABLE import_report (
    report_id integer NOT NULL,
    report_date character varying(20),
    user_id bigint,
    organisation_id integer,
    data_date character varying(20),
    report text,
    sysma_object_type_id integer,
    track_historytables_id bigint DEFAULT nextval('increment_x99_t_track_historytables'::regclass) NOT NULL
);


--
-- TOC entry 29566 (class 0 OID 0)
-- Dependencies: 3058
-- Name: TABLE import_report; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON TABLE import_report IS '{
   "display_groupe_order":600,
   "groupe_name":"Surveillance",
   "table_comment":"Rapport d''import de données par lot"
}';


--
-- TOC entry 29567 (class 0 OID 0)
-- Dependencies: 3058
-- Name: COLUMN import_report.track_historytables_id; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN import_report.track_historytables_id IS 'identifiant unique de ligne utilisé pour le suivi historique des enregistrement dans la table x99_track_historiquetables';


--
-- TOC entry 3059 (class 1259 OID 7577531)
-- Name: import_report_report_id_seq; Type: SEQUENCE; Schema: sysma; Owner: -
--

CREATE SEQUENCE import_report_report_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 29568 (class 0 OID 0)
-- Dependencies: 3059
-- Name: import_report_report_id_seq; Type: SEQUENCE OWNED BY; Schema: sysma; Owner: -
--

ALTER SEQUENCE import_report_report_id_seq OWNED BY import_report.report_id;


--
-- TOC entry 3048 (class 1259 OID 7577478)
-- Name: l_filter_user; Type: TABLE; Schema: sysma; Owner: -
--

CREATE TABLE l_filter_user (
    l_filter_user_id integer NOT NULL,
    filter_id integer,
    user_id bigint,
    sysma_object_type_id bigint,
    sysma_action_type_id bigint,
    condition character varying
);


--
-- TOC entry 29569 (class 0 OID 0)
-- Dependencies: 3048
-- Name: TABLE l_filter_user; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON TABLE l_filter_user IS '{
   "display_groupe_order":550,
   "groupe_name":"Données saisies UI",
   "table_comment":"Choix de l''utilisateur pour l''usage de filtres"
}';


--
-- TOC entry 29570 (class 0 OID 0)
-- Dependencies: 3048
-- Name: COLUMN l_filter_user.condition; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN l_filter_user.condition IS 'condition d''enchainement avec le filtre suivant (et,ou)';


--
-- TOC entry 3049 (class 1259 OID 7577484)
-- Name: l_filter_user_l_filter_user_id_seq; Type: SEQUENCE; Schema: sysma; Owner: -
--

CREATE SEQUENCE l_filter_user_l_filter_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 29571 (class 0 OID 0)
-- Dependencies: 3049
-- Name: l_filter_user_l_filter_user_id_seq; Type: SEQUENCE OWNED BY; Schema: sysma; Owner: -
--

ALTER SEQUENCE l_filter_user_l_filter_user_id_seq OWNED BY l_filter_user.l_filter_user_id;


--
-- TOC entry 5428 (class 1259 OID 68884082)
-- Name: l_group_user; Type: TABLE; Schema: sysma; Owner: -
--

CREATE TABLE l_group_user (
    l_group_user_id integer NOT NULL,
    group_name character varying,
    user_id bigint,
    l_group_user_action character varying
);


--
-- TOC entry 29572 (class 0 OID 0)
-- Dependencies: 5428
-- Name: TABLE l_group_user; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON TABLE l_group_user IS '{
   "display_groupe_order":550,
   "groupe_name":"Données saisies UI",
   "table_comment":"Choix de l''utilisateur pour l''affichage des groupes de couches sysma"
}';


--
-- TOC entry 5427 (class 1259 OID 68884080)
-- Name: l_group_user_l_group_user_id_seq; Type: SEQUENCE; Schema: sysma; Owner: -
--

CREATE SEQUENCE l_group_user_l_group_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 29573 (class 0 OID 0)
-- Dependencies: 5427
-- Name: l_group_user_l_group_user_id_seq; Type: SEQUENCE OWNED BY; Schema: sysma; Owner: -
--

ALTER SEQUENCE l_group_user_l_group_user_id_seq OWNED BY l_group_user.l_group_user_id;


--
-- TOC entry 3044 (class 1259 OID 7577444)
-- Name: l_other_layer_user; Type: TABLE; Schema: sysma; Owner: -
--

CREATE TABLE l_other_layer_user (
    l_other_layer_user_id integer NOT NULL,
    layer character varying NOT NULL,
    user_id bigint NOT NULL,
    track_historytables_id bigint DEFAULT nextval('increment_x99_t_track_historytables'::regclass) NOT NULL,
    layer_type character varying NOT NULL
);


--
-- TOC entry 29574 (class 0 OID 0)
-- Dependencies: 3044
-- Name: TABLE l_other_layer_user; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON TABLE l_other_layer_user IS '{
   "display_groupe_order":550,
   "groupe_name":"Données saisies UI",
   "table_comment":"Table de lien des autres couches à afficher dans le layermanager pour chaque utilisateur"
}';


--
-- TOC entry 29575 (class 0 OID 0)
-- Dependencies: 3044
-- Name: COLUMN l_other_layer_user.track_historytables_id; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN l_other_layer_user.track_historytables_id IS 'identifiant unique de ligne utilisé pour le suivi historique des enregistrement dans la table x99_track_historiquetables';


--
-- TOC entry 3045 (class 1259 OID 7577451)
-- Name: l_other_layer_user_id_seq; Type: SEQUENCE; Schema: sysma; Owner: -
--

CREATE SEQUENCE l_other_layer_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 29576 (class 0 OID 0)
-- Dependencies: 3045
-- Name: l_other_layer_user_id_seq; Type: SEQUENCE OWNED BY; Schema: sysma; Owner: -
--

ALTER SEQUENCE l_other_layer_user_id_seq OWNED BY l_other_layer_user.l_other_layer_user_id;


--
-- TOC entry 3046 (class 1259 OID 7577465)
-- Name: l_ref_layer_user; Type: TABLE; Schema: sysma; Owner: -
--

CREATE TABLE l_ref_layer_user (
    user_id bigint NOT NULL,
    layer character varying,
    l_ref_layer_user_id integer NOT NULL
);


--
-- TOC entry 29577 (class 0 OID 0)
-- Dependencies: 3046
-- Name: TABLE l_ref_layer_user; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON TABLE l_ref_layer_user IS '{
   "display_groupe_order":550,
   "groupe_name":"Données saisies UI",
   "table_comment":"Table des couches référentiels choisies par l''utilisateur pour être affichable sur sa carte"
}';


--
-- TOC entry 3047 (class 1259 OID 7577471)
-- Name: l_ref_layer_user_l_other_layer_user_id_seq; Type: SEQUENCE; Schema: sysma; Owner: -
--

CREATE SEQUENCE l_ref_layer_user_l_other_layer_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 29578 (class 0 OID 0)
-- Dependencies: 3047
-- Name: l_ref_layer_user_l_other_layer_user_id_seq; Type: SEQUENCE OWNED BY; Schema: sysma; Owner: -
--

ALTER SEQUENCE l_ref_layer_user_l_other_layer_user_id_seq OWNED BY l_ref_layer_user.l_ref_layer_user_id;


--
-- TOC entry 3050 (class 1259 OID 7577486)
-- Name: l_style_user; Type: TABLE; Schema: sysma; Owner: -
--

CREATE TABLE l_style_user (
    l_style_user_id integer NOT NULL,
    object_type character varying,
    type_id integer,
    style json,
    user_id bigint
);


--
-- TOC entry 29579 (class 0 OID 0)
-- Dependencies: 3050
-- Name: TABLE l_style_user; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON TABLE l_style_user IS '{
   "display_groupe_order":550,
   "groupe_name":"Données saisies UI",
   "table_comment":"Liste de styles à appliquer pour l''utilisateur"
}';


--
-- TOC entry 29580 (class 0 OID 0)
-- Dependencies: 3050
-- Name: COLUMN l_style_user.object_type; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN l_style_user.object_type IS 'objetgeo, travaux';


--
-- TOC entry 3051 (class 1259 OID 7577492)
-- Name: l_style_user_l_style_user_id_seq; Type: SEQUENCE; Schema: sysma; Owner: -
--

CREATE SEQUENCE l_style_user_l_style_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 29581 (class 0 OID 0)
-- Dependencies: 3051
-- Name: l_style_user_l_style_user_id_seq; Type: SEQUENCE OWNED BY; Schema: sysma; Owner: -
--

ALTER SEQUENCE l_style_user_l_style_user_id_seq OWNED BY l_style_user.l_style_user_id;


--
-- TOC entry 3054 (class 1259 OID 7577500)
-- Name: l_sysma_action_type_user; Type: TABLE; Schema: sysma; Owner: -
--

CREATE TABLE l_sysma_action_type_user (
    l_sysma_action_type_user_id integer NOT NULL,
    sysma_action_type_id bigint NOT NULL,
    user_id bigint NOT NULL,
    track_historytables_id bigint DEFAULT nextval('increment_x99_t_track_historytables'::regclass) NOT NULL
);


--
-- TOC entry 29582 (class 0 OID 0)
-- Dependencies: 3054
-- Name: TABLE l_sysma_action_type_user; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON TABLE l_sysma_action_type_user IS '{
   "display_groupe_order":550,
   "groupe_name":"Données saisies UI",
   "table_comment":"Table de lien des types de travaux à afficher dans le layermanager pour chaque utilisateur"
}';


--
-- TOC entry 29583 (class 0 OID 0)
-- Dependencies: 3054
-- Name: COLUMN l_sysma_action_type_user.track_historytables_id; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN l_sysma_action_type_user.track_historytables_id IS 'identifiant unique de ligne utilisé pour le suivi historique des enregistrement dans la table x99_track_historiquetables';


--
-- TOC entry 3055 (class 1259 OID 7577504)
-- Name: l_sysma_action_type_user_l_sysma_action_type_user_id_seq; Type: SEQUENCE; Schema: sysma; Owner: -
--

CREATE SEQUENCE l_sysma_action_type_user_l_sysma_action_type_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 29584 (class 0 OID 0)
-- Dependencies: 3055
-- Name: l_sysma_action_type_user_l_sysma_action_type_user_id_seq; Type: SEQUENCE OWNED BY; Schema: sysma; Owner: -
--

ALTER SEQUENCE l_sysma_action_type_user_l_sysma_action_type_user_id_seq OWNED BY l_sysma_action_type_user.l_sysma_action_type_user_id;


--
-- TOC entry 7256 (class 1259 OID 177338635)
-- Name: l_sysma_object_sysma_relation; Type: TABLE; Schema: sysma; Owner: -
--

CREATE TABLE l_sysma_object_sysma_relation (
    sysma_object_id bigint NOT NULL,
    parent_child character varying NOT NULL,
    sysma_relation_id bigint NOT NULL,
    track_historytables_id bigint NOT NULL,
    modified_by bigint
);


--
-- TOC entry 7255 (class 1259 OID 177338633)
-- Name: l_sysma_object_sysma_relation_track_historytables_id_seq; Type: SEQUENCE; Schema: sysma; Owner: -
--

CREATE SEQUENCE l_sysma_object_sysma_relation_track_historytables_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 29585 (class 0 OID 0)
-- Dependencies: 7255
-- Name: l_sysma_object_sysma_relation_track_historytables_id_seq; Type: SEQUENCE OWNED BY; Schema: sysma; Owner: -
--

ALTER SEQUENCE l_sysma_object_sysma_relation_track_historytables_id_seq OWNED BY l_sysma_object_sysma_relation.track_historytables_id;


--
-- TOC entry 7259 (class 1259 OID 177338663)
-- Name: l_sysma_object_type_sysma_relation_type; Type: TABLE; Schema: sysma; Owner: -
--

CREATE TABLE l_sysma_object_type_sysma_relation_type (
    sysma_object_type_id_parent bigint NOT NULL,
    sysma_object_type_id_child bigint NOT NULL,
    sysma_relation_type_id bigint NOT NULL,
    display_order character varying(250),
    track_historytables_id bigint NOT NULL,
    modified_by bigint,
    l_sysma_object_type_sysma_relation_type_id integer NOT NULL
);


--
-- TOC entry 7258 (class 1259 OID 177338661)
-- Name: l_sysma_object_type_sysma_rel_l_sysma_object_type_sysma_rel_seq; Type: SEQUENCE; Schema: sysma; Owner: -
--

CREATE SEQUENCE l_sysma_object_type_sysma_rel_l_sysma_object_type_sysma_rel_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 29586 (class 0 OID 0)
-- Dependencies: 7258
-- Name: l_sysma_object_type_sysma_rel_l_sysma_object_type_sysma_rel_seq; Type: SEQUENCE OWNED BY; Schema: sysma; Owner: -
--

ALTER SEQUENCE l_sysma_object_type_sysma_rel_l_sysma_object_type_sysma_rel_seq OWNED BY l_sysma_object_type_sysma_relation_type.l_sysma_object_type_sysma_relation_type_id;


--
-- TOC entry 7257 (class 1259 OID 177338659)
-- Name: l_sysma_object_type_sysma_relation_t_track_historytables_id_seq; Type: SEQUENCE; Schema: sysma; Owner: -
--

CREATE SEQUENCE l_sysma_object_type_sysma_relation_t_track_historytables_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 29587 (class 0 OID 0)
-- Dependencies: 7257
-- Name: l_sysma_object_type_sysma_relation_t_track_historytables_id_seq; Type: SEQUENCE OWNED BY; Schema: sysma; Owner: -
--

ALTER SEQUENCE l_sysma_object_type_sysma_relation_t_track_historytables_id_seq OWNED BY l_sysma_object_type_sysma_relation_type.track_historytables_id;


--
-- TOC entry 3052 (class 1259 OID 7577494)
-- Name: l_sysma_object_type_user; Type: TABLE; Schema: sysma; Owner: -
--

CREATE TABLE l_sysma_object_type_user (
    l_sysma_object_type_user_id integer NOT NULL,
    sysma_object_type_id bigint NOT NULL,
    user_id bigint NOT NULL,
    track_historytables_id bigint DEFAULT nextval('increment_x99_t_track_historytables'::regclass) NOT NULL
);


--
-- TOC entry 29588 (class 0 OID 0)
-- Dependencies: 3052
-- Name: TABLE l_sysma_object_type_user; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON TABLE l_sysma_object_type_user IS '{
   "display_groupe_order":550,
   "groupe_name":"Données saisies UI",
   "table_comment":"Table de lien des types d''objet à afficher dans le layermanager pour chaque utilisateur"
}';


--
-- TOC entry 29589 (class 0 OID 0)
-- Dependencies: 3052
-- Name: COLUMN l_sysma_object_type_user.track_historytables_id; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN l_sysma_object_type_user.track_historytables_id IS 'identifiant unique de ligne utilisé pour le suivi historique des enregistrement dans la table x99_track_historiquetables';


--
-- TOC entry 3053 (class 1259 OID 7577498)
-- Name: l_sysma_object_type_user_l_sysma_object_type_user_id_seq; Type: SEQUENCE; Schema: sysma; Owner: -
--

CREATE SEQUENCE l_sysma_object_type_user_l_sysma_object_type_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 29590 (class 0 OID 0)
-- Dependencies: 3053
-- Name: l_sysma_object_type_user_l_sysma_object_type_user_id_seq; Type: SEQUENCE OWNED BY; Schema: sysma; Owner: -
--

ALTER SEQUENCE l_sysma_object_type_user_l_sysma_object_type_user_id_seq OWNED BY l_sysma_object_type_user.l_sysma_object_type_user_id;


--
-- TOC entry 3042 (class 1259 OID 7577438)
-- Name: l_theme_analysis_user; Type: TABLE; Schema: sysma; Owner: -
--

CREATE TABLE l_theme_analysis_user (
    theme_analysis_id integer,
    user_id bigint,
    l_theme_analysis_user_id integer NOT NULL,
    track_historytables_id bigint DEFAULT nextval('increment_x99_t_track_historytables'::regclass) NOT NULL
);


--
-- TOC entry 29591 (class 0 OID 0)
-- Dependencies: 3042
-- Name: TABLE l_theme_analysis_user; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON TABLE l_theme_analysis_user IS '{
   "display_groupe_order":550,
   "groupe_name":"Données saisies UI",
   "table_comment":"Table de lien entre les analyses thématiques pour chaque utilisateur"
}';


--
-- TOC entry 29592 (class 0 OID 0)
-- Dependencies: 3042
-- Name: COLUMN l_theme_analysis_user.track_historytables_id; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN l_theme_analysis_user.track_historytables_id IS 'identifiant unique de ligne utilisé pour le suivi historique des enregistrement dans la table x99_track_historiquetables';


--
-- TOC entry 3043 (class 1259 OID 7577442)
-- Name: l_theme_analysis_user_l_theme_analysis_user_id_seq; Type: SEQUENCE; Schema: sysma; Owner: -
--

CREATE SEQUENCE l_theme_analysis_user_l_theme_analysis_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 29593 (class 0 OID 0)
-- Dependencies: 3043
-- Name: l_theme_analysis_user_l_theme_analysis_user_id_seq; Type: SEQUENCE OWNED BY; Schema: sysma; Owner: -
--

ALTER SEQUENCE l_theme_analysis_user_l_theme_analysis_user_id_seq OWNED BY l_theme_analysis_user.l_theme_analysis_user_id;


--
-- TOC entry 5426 (class 1259 OID 68884041)
-- Name: layer_group; Type: TABLE; Schema: sysma; Owner: -
--

CREATE TABLE layer_group (
    layer_group_id integer NOT NULL,
    group_alias character varying,
    group_name character varying,
    group_display_order character varying
);


--
-- TOC entry 29594 (class 0 OID 0)
-- Dependencies: 5426
-- Name: TABLE layer_group; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON TABLE layer_group IS '{
   "display_groupe_order":160,
   "groupe_name":"Configuration de sysma",
   "table_comment":"Liste des groupes de couches sysma"
}';


--
-- TOC entry 5425 (class 1259 OID 68884039)
-- Name: layer_group_layer_group_id_seq; Type: SEQUENCE; Schema: sysma; Owner: -
--

CREATE SEQUENCE layer_group_layer_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 29595 (class 0 OID 0)
-- Dependencies: 5425
-- Name: layer_group_layer_group_id_seq; Type: SEQUENCE OWNED BY; Schema: sysma; Owner: -
--

ALTER SEQUENCE layer_group_layer_group_id_seq OWNED BY layer_group.layer_group_id;


--
-- TOC entry 3031 (class 1259 OID 7577352)
-- Name: other_layer; Type: TABLE; Schema: sysma; Owner: -
--

CREATE TABLE other_layer (
    other_layer_id integer NOT NULL,
    layer_id character varying,
    geolayer_id character varying,
    layer_type character varying,
    style json,
    geometry_type character varying,
    mapping json,
    "group" character varying,
    access character varying DEFAULT 'restricted'::character varying,
    sql_filter character varying,
    track_historytables_id bigint DEFAULT nextval('increment_x99_t_track_historytables'::regclass) NOT NULL,
    zoom_min integer DEFAULT 0,
    zoom_max integer DEFAULT 20,
    load_on_pan integer DEFAULT 0,
    other_layer_display_order character varying,
    options character varying,
    layer_alias character varying
);


--
-- TOC entry 29596 (class 0 OID 0)
-- Dependencies: 3031
-- Name: TABLE other_layer; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON TABLE other_layer IS '{
   "display_groupe_order":101,
   "groupe_name":"Configuration de sysma",
   "table_comment":"Liste des couches au format SIG standard utilisées par exemple en affichage dans la zone couches utiles..."
}';


--
-- TOC entry 29597 (class 0 OID 0)
-- Dependencies: 3031
-- Name: COLUMN other_layer.layer_id; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN other_layer.layer_id IS 'id de la couche dans Sysma';


--
-- TOC entry 29598 (class 0 OID 0)
-- Dependencies: 3031
-- Name: COLUMN other_layer.geolayer_id; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN other_layer.geolayer_id IS 'id de la couche dans l''outil externe';


--
-- TOC entry 29599 (class 0 OID 0)
-- Dependencies: 3031
-- Name: COLUMN other_layer.mapping; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN other_layer.mapping IS 'correspondance des champs de la table et des champs attenuds';


--
-- TOC entry 29600 (class 0 OID 0)
-- Dependencies: 3031
-- Name: COLUMN other_layer."group"; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN other_layer."group" IS 'nom du groupe pour le layer manager';


--
-- TOC entry 29601 (class 0 OID 0)
-- Dependencies: 3031
-- Name: COLUMN other_layer.access; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN other_layer.access IS 'public ou restreint (restricted) aux personnes connectées';


--
-- TOC entry 29602 (class 0 OID 0)
-- Dependencies: 3031
-- Name: COLUMN other_layer.sql_filter; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN other_layer.sql_filter IS 'pour affiner la requête de sélection des objets de la couche';


--
-- TOC entry 29603 (class 0 OID 0)
-- Dependencies: 3031
-- Name: COLUMN other_layer.track_historytables_id; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN other_layer.track_historytables_id IS 'identifiant unique de ligne utilisé pour le suivi historique des enregistrement dans la table x99_track_historiquetables';


--
-- TOC entry 3032 (class 1259 OID 7577359)
-- Name: other_layer_other_layer_id_seq; Type: SEQUENCE; Schema: sysma; Owner: -
--

CREATE SEQUENCE other_layer_other_layer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 29604 (class 0 OID 0)
-- Dependencies: 3032
-- Name: other_layer_other_layer_id_seq; Type: SEQUENCE OWNED BY; Schema: sysma; Owner: -
--

ALTER SEQUENCE other_layer_other_layer_id_seq OWNED BY other_layer.other_layer_id;


--
-- TOC entry 3056 (class 1259 OID 7577515)
-- Name: photo_object; Type: TABLE; Schema: sysma; Owner: -
--

CREATE TABLE photo_object (
    photo_object_id integer NOT NULL,
    photo_id integer,
    photo_type character varying(15),
    sysma_object_id bigint,
    display_order integer,
    before_after character varying,
    track_historytables_id bigint DEFAULT nextval('increment_x99_t_track_historytables'::regclass) NOT NULL,
    photo_file character varying,
    photo_title character varying,
    photo_date date
);


--
-- TOC entry 29605 (class 0 OID 0)
-- Dependencies: 3056
-- Name: TABLE photo_object; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON TABLE photo_object IS '{
   "display_groupe_order":500,
   "groupe_name":"Données saisies",
   "table_comment":"Liste des fichiers photo liés"
}';


--
-- TOC entry 29606 (class 0 OID 0)
-- Dependencies: 3056
-- Name: COLUMN photo_object.track_historytables_id; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN photo_object.track_historytables_id IS 'identifiant unique de ligne utilisé pour le suivi historique des enregistrement dans la table x99_track_historiquetables';


--
-- TOC entry 29607 (class 0 OID 0)
-- Dependencies: 3056
-- Name: COLUMN photo_object.photo_file; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN photo_object.photo_file IS 'name of the file if the photo type is ''photo_upload''';


--
-- TOC entry 29608 (class 0 OID 0)
-- Dependencies: 3056
-- Name: COLUMN photo_object.photo_date; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN photo_object.photo_date IS 'only for the ''photo_upload'' types';


--
-- TOC entry 3057 (class 1259 OID 7577522)
-- Name: photo_object_photo_object_id_seq; Type: SEQUENCE; Schema: sysma; Owner: -
--

CREATE SEQUENCE photo_object_photo_object_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 29609 (class 0 OID 0)
-- Dependencies: 3057
-- Name: photo_object_photo_object_id_seq; Type: SEQUENCE OWNED BY; Schema: sysma; Owner: -
--

ALTER SEQUENCE photo_object_photo_object_id_seq OWNED BY photo_object.photo_object_id;


--
-- TOC entry 3060 (class 1259 OID 7577549)
-- Name: status; Type: TABLE; Schema: sysma; Owner: -
--

CREATE TABLE status (
    status_id integer NOT NULL,
    status character varying(50),
    status_value character varying(50),
    description character varying(200),
    object_type character varying(255),
    display_order integer,
    style character varying(20),
    track_historytables_id bigint DEFAULT nextval('increment_x99_t_track_historytables'::regclass) NOT NULL
);


--
-- TOC entry 29610 (class 0 OID 0)
-- Dependencies: 3060
-- Name: TABLE status; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON TABLE status IS '{
   "display_groupe_order":140,
   "groupe_name":"Configuration de sysma",
   "table_comment":"Liste des statuts possibles des objets et des travaux"
}';


--
-- TOC entry 29611 (class 0 OID 0)
-- Dependencies: 3060
-- Name: COLUMN status.style; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN status.style IS 'style CSS';


--
-- TOC entry 29612 (class 0 OID 0)
-- Dependencies: 3060
-- Name: COLUMN status.track_historytables_id; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN status.track_historytables_id IS 'identifiant unique de ligne utilisé pour le suivi historique des enregistrement dans la table x99_track_historiquetables';


--
-- TOC entry 3061 (class 1259 OID 7577553)
-- Name: status_status_id_seq; Type: SEQUENCE; Schema: sysma; Owner: -
--

CREATE SEQUENCE status_status_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 29613 (class 0 OID 0)
-- Dependencies: 3061
-- Name: status_status_id_seq; Type: SEQUENCE OWNED BY; Schema: sysma; Owner: -
--

ALTER SEQUENCE status_status_id_seq OWNED BY status.status_id;


--
-- TOC entry 3039 (class 1259 OID 7577411)
-- Name: sysma_action_data; Type: TABLE; Schema: sysma; Owner: -
--

CREATE TABLE sysma_action_data (
    sysma_action_id bigint NOT NULL,
    parameter_id bigint NOT NULL,
    value text NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    user_id bigint,
    data_index bigint NOT NULL,
    sysma_action_data_id bigint DEFAULT nextval('increment_sysma_entity_seq'::regclass) NOT NULL,
    modified_by bigint,
    modified_at timestamp without time zone DEFAULT now(),
    track_historytables_id bigint DEFAULT nextval('increment_x99_t_track_historytables'::regclass) NOT NULL
);


--
-- TOC entry 29614 (class 0 OID 0)
-- Dependencies: 3039
-- Name: TABLE sysma_action_data; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON TABLE sysma_action_data IS '{
   "display_groupe_order":500,
   "groupe_name":"Données saisies",
   "table_comment":"Contient les données sur les travaux menés"
}';


--
-- TOC entry 29615 (class 0 OID 0)
-- Dependencies: 3039
-- Name: COLUMN sysma_action_data.sysma_action_id; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_action_data.sysma_action_id IS 'identifiant des travaux';


--
-- TOC entry 29616 (class 0 OID 0)
-- Dependencies: 3039
-- Name: COLUMN sysma_action_data.parameter_id; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_action_data.parameter_id IS 'identifiant du paramètre de suivi des travaux';


--
-- TOC entry 29617 (class 0 OID 0)
-- Dependencies: 3039
-- Name: COLUMN sysma_action_data.value; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_action_data.value IS 'valeur pour le paramètre';


--
-- TOC entry 29618 (class 0 OID 0)
-- Dependencies: 3039
-- Name: COLUMN sysma_action_data.created_at; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_action_data.created_at IS 'date d''ajout dans la base de données';


--
-- TOC entry 29619 (class 0 OID 0)
-- Dependencies: 3039
-- Name: COLUMN sysma_action_data.user_id; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_action_data.user_id IS 'utilisateur ayant ajouté cette donnée';


--
-- TOC entry 29620 (class 0 OID 0)
-- Dependencies: 3039
-- Name: COLUMN sysma_action_data.data_index; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_action_data.data_index IS 'index de la table';


--
-- TOC entry 29621 (class 0 OID 0)
-- Dependencies: 3039
-- Name: COLUMN sysma_action_data.sysma_action_data_id; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_action_data.sysma_action_data_id IS 'pk de la table';


--
-- TOC entry 29622 (class 0 OID 0)
-- Dependencies: 3039
-- Name: COLUMN sysma_action_data.modified_by; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_action_data.modified_by IS 'dernier utilisateur ayant modifié la donnée ou créateur s''il n''y a pas eu de modification';


--
-- TOC entry 29623 (class 0 OID 0)
-- Dependencies: 3039
-- Name: COLUMN sysma_action_data.track_historytables_id; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_action_data.track_historytables_id IS 'identifiant unique de ligne utilisé pour le suivi historique des enregistrement dans la table x99_track_historiquetables';


--
-- TOC entry 3064 (class 1259 OID 7577586)
-- Name: sysma_action_type_parameter; Type: TABLE; Schema: sysma; Owner: -
--

CREATE TABLE sysma_action_type_parameter (
    parameter_id bigint DEFAULT nextval('increment_sysma_entity_seq'::regclass) NOT NULL,
    parameter character varying(250),
    description text,
    data_type character varying(25),
    ordered_by character varying(100),
    created_at date DEFAULT now(),
    modified_at date DEFAULT now(),
    sysma_action_type_id bigint NOT NULL,
    parameter_source text,
    data_source text,
    unit character varying(50),
    required smallint,
    parameter_alias character varying,
    track_historytables_id bigint DEFAULT nextval('increment_x99_t_track_historytables'::regclass) NOT NULL,
    modified_by bigint NOT NULL
);


--
-- TOC entry 29624 (class 0 OID 0)
-- Dependencies: 3064
-- Name: TABLE sysma_action_type_parameter; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON TABLE sysma_action_type_parameter IS '{
   "display_groupe_order":300,
   "groupe_name":"Gestion du dictionnaire de données",
   "table_comment":"liste des paramètres à suivre pour chaque type de travaux"
}';


--
-- TOC entry 29625 (class 0 OID 0)
-- Dependencies: 3064
-- Name: COLUMN sysma_action_type_parameter.parameter_id; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_action_type_parameter.parameter_id IS 'identifiant du paramètre';


--
-- TOC entry 29626 (class 0 OID 0)
-- Dependencies: 3064
-- Name: COLUMN sysma_action_type_parameter.parameter; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_action_type_parameter.parameter IS 'intitulé du paramètre';


--
-- TOC entry 29627 (class 0 OID 0)
-- Dependencies: 3064
-- Name: COLUMN sysma_action_type_parameter.description; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_action_type_parameter.description IS 'description détaillée du paramètre';


--
-- TOC entry 29628 (class 0 OID 0)
-- Dependencies: 3064
-- Name: COLUMN sysma_action_type_parameter.data_type; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_action_type_parameter.data_type IS 'type de donnée caracteres, entier etc..';


--
-- TOC entry 29629 (class 0 OID 0)
-- Dependencies: 3064
-- Name: COLUMN sysma_action_type_parameter.ordered_by; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_action_type_parameter.ordered_by IS 'regroupement possible par cette colonne pour présenter les paramètres';


--
-- TOC entry 29630 (class 0 OID 0)
-- Dependencies: 3064
-- Name: COLUMN sysma_action_type_parameter.created_at; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_action_type_parameter.created_at IS 'date de création du paramètre';


--
-- TOC entry 29631 (class 0 OID 0)
-- Dependencies: 3064
-- Name: COLUMN sysma_action_type_parameter.modified_at; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_action_type_parameter.modified_at IS 'date de modification du paramètre';


--
-- TOC entry 29632 (class 0 OID 0)
-- Dependencies: 3064
-- Name: COLUMN sysma_action_type_parameter.sysma_action_type_id; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_action_type_parameter.sysma_action_type_id IS 'id du type de travaux sur lequel s''applique ce paramètre';


--
-- TOC entry 29633 (class 0 OID 0)
-- Dependencies: 3064
-- Name: COLUMN sysma_action_type_parameter.parameter_source; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_action_type_parameter.parameter_source IS 'Source de la définition du paramètre';


--
-- TOC entry 29634 (class 0 OID 0)
-- Dependencies: 3064
-- Name: COLUMN sysma_action_type_parameter.data_source; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_action_type_parameter.data_source IS 'Sources des données';


--
-- TOC entry 29635 (class 0 OID 0)
-- Dependencies: 3064
-- Name: COLUMN sysma_action_type_parameter.unit; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_action_type_parameter.unit IS 'unité';


--
-- TOC entry 29636 (class 0 OID 0)
-- Dependencies: 3064
-- Name: COLUMN sysma_action_type_parameter.required; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_action_type_parameter.required IS 'le champ est-il obligatoire';


--
-- TOC entry 29637 (class 0 OID 0)
-- Dependencies: 3064
-- Name: COLUMN sysma_action_type_parameter.track_historytables_id; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_action_type_parameter.track_historytables_id IS 'identifiant unique de ligne utilisé pour le suivi historique des enregistrement dans la table x99_track_historiquetables';


--
-- TOC entry 3034 (class 1259 OID 7577369)
-- Name: sysma_action_type_parameter_choice; Type: TABLE; Schema: sysma; Owner: -
--

CREATE TABLE sysma_action_type_parameter_choice (
    choice_id bigint DEFAULT nextval('increment_sysma_entity_seq'::regclass) NOT NULL,
    parameter_id bigint NOT NULL,
    choice character varying(200),
    description text,
    display_order integer,
    track_historytables_id bigint DEFAULT nextval('increment_x99_t_track_historytables'::regclass) NOT NULL,
    modified_by bigint NOT NULL
);


--
-- TOC entry 29638 (class 0 OID 0)
-- Dependencies: 3034
-- Name: TABLE sysma_action_type_parameter_choice; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON TABLE sysma_action_type_parameter_choice IS '{
   "display_groupe_order":300,
   "groupe_name":"Gestion du dictionnaire de données",
   "table_comment":"Liste des choix de valeur possibles pour un paramètre travaux"
}';


--
-- TOC entry 29639 (class 0 OID 0)
-- Dependencies: 3034
-- Name: COLUMN sysma_action_type_parameter_choice.choice_id; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_action_type_parameter_choice.choice_id IS 'identifiant';


--
-- TOC entry 29640 (class 0 OID 0)
-- Dependencies: 3034
-- Name: COLUMN sysma_action_type_parameter_choice.parameter_id; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_action_type_parameter_choice.parameter_id IS 'id du paramètre';


--
-- TOC entry 29641 (class 0 OID 0)
-- Dependencies: 3034
-- Name: COLUMN sysma_action_type_parameter_choice.choice; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_action_type_parameter_choice.choice IS 'inditulé du choix';


--
-- TOC entry 29642 (class 0 OID 0)
-- Dependencies: 3034
-- Name: COLUMN sysma_action_type_parameter_choice.description; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_action_type_parameter_choice.description IS 'description du choix';


--
-- TOC entry 29643 (class 0 OID 0)
-- Dependencies: 3034
-- Name: COLUMN sysma_action_type_parameter_choice.track_historytables_id; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_action_type_parameter_choice.track_historytables_id IS 'identifiant unique de ligne utilisé pour le suivi historique des enregistrement dans la table x99_track_historiquetables';


--
-- TOC entry 6372 (class 1259 OID 101936414)
-- Name: sysma_conf; Type: TABLE; Schema: sysma; Owner: -
--

CREATE TABLE sysma_conf (
    sysma_conf_id integer NOT NULL,
    parameter character varying,
    alias character varying,
    data_type character varying,
    value text,
    display_order integer DEFAULT 0 NOT NULL,
    value_choices json
);


--
-- TOC entry 29644 (class 0 OID 0)
-- Dependencies: 6372
-- Name: TABLE sysma_conf; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON TABLE sysma_conf IS '{
   "display_groupe_order":100,
   "groupe_name":"Configuration de sysma",
   "table_comment":"Liste des paramètres de l''application"
}';


--
-- TOC entry 29645 (class 0 OID 0)
-- Dependencies: 6372
-- Name: COLUMN sysma_conf.value_choices; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_conf.value_choices IS 'key : valeur
value : comment';


--
-- TOC entry 6371 (class 1259 OID 101936412)
-- Name: sysma_conf_sysma_conf_id_seq; Type: SEQUENCE; Schema: sysma; Owner: -
--

CREATE SEQUENCE sysma_conf_sysma_conf_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 29646 (class 0 OID 0)
-- Dependencies: 6371
-- Name: sysma_conf_sysma_conf_id_seq; Type: SEQUENCE OWNED BY; Schema: sysma; Owner: -
--

ALTER SEQUENCE sysma_conf_sysma_conf_id_seq OWNED BY sysma_conf.sysma_conf_id;


--
-- TOC entry 3033 (class 1259 OID 7577361)
-- Name: sysma_object_type_parameter_choice; Type: TABLE; Schema: sysma; Owner: -
--

CREATE TABLE sysma_object_type_parameter_choice (
    choice_id bigint DEFAULT nextval('increment_sysma_entity_seq'::regclass) NOT NULL,
    parameter_id bigint NOT NULL,
    choice character varying(200),
    description text,
    display_order integer,
    track_historytables_id bigint DEFAULT nextval('increment_x99_t_track_historytables'::regclass) NOT NULL,
    modified_by bigint NOT NULL,
    value_type character varying DEFAULT 'choice'::character varying
);


--
-- TOC entry 29647 (class 0 OID 0)
-- Dependencies: 3033
-- Name: TABLE sysma_object_type_parameter_choice; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON TABLE sysma_object_type_parameter_choice IS '{
   "display_groupe_order":300,
   "groupe_name":"Gestion du dictionnaire de données",
   "table_comment":"Liste de choix pour un parametre d''un type d''objet"
}';


--
-- TOC entry 29648 (class 0 OID 0)
-- Dependencies: 3033
-- Name: COLUMN sysma_object_type_parameter_choice.display_order; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_object_type_parameter_choice.display_order IS 'ordre de présentation des choix';


--
-- TOC entry 29649 (class 0 OID 0)
-- Dependencies: 3033
-- Name: COLUMN sysma_object_type_parameter_choice.track_historytables_id; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN sysma_object_type_parameter_choice.track_historytables_id IS 'identifiant unique de ligne utilisé pour le suivi historique des enregistrement dans la table x99_track_historiquetables';


--
-- TOC entry 7252 (class 1259 OID 177338547)
-- Name: sysma_relation; Type: TABLE; Schema: sysma; Owner: -
--

CREATE TABLE sysma_relation (
    sysma_relation_id bigint DEFAULT nextval('increment_sysma_entity_seq'::regclass) NOT NULL,
    start_date character varying(15),
    end_date character varying(15),
    organisation_id bigint,
    created_by bigint,
    status character varying(20),
    sysma_relation_type_id bigint NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    sysma_relation character varying(255),
    modified_by integer,
    modified_at timestamp without time zone DEFAULT now(),
    track_historytables_id bigint DEFAULT nextval('increment_x99_t_track_historytables'::regclass) NOT NULL,
    contracts character varying[]
);


--
-- TOC entry 7253 (class 1259 OID 177338580)
-- Name: sysma_relation_data; Type: TABLE; Schema: sysma; Owner: -
--

CREATE TABLE sysma_relation_data (
    sysma_relation_id bigint NOT NULL,
    parameter_id bigint NOT NULL,
    value text NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    user_id bigint,
    data_index bigint NOT NULL,
    sysma_relation_data_id bigint DEFAULT nextval('increment_sysma_entity_seq'::regclass) NOT NULL,
    modified_by bigint,
    modified_at timestamp without time zone DEFAULT now(),
    track_historytables_id bigint DEFAULT nextval('increment_x99_t_track_historytables'::regclass) NOT NULL
);


--
-- TOC entry 7250 (class 1259 OID 177338501)
-- Name: sysma_relation_type; Type: TABLE; Schema: sysma; Owner: -
--

CREATE TABLE sysma_relation_type (
    sysma_relation_type_id bigint DEFAULT nextval('increment_sysma_entity_seq'::regclass) NOT NULL,
    sysma_relation_type_from_parent character varying(250),
    sysma_relation_type_from_child character varying(250),
    sysma_relation_type_alias character varying(250),
    description text,
    relation_type character varying(50),
    created_at date DEFAULT now(),
    modified_at date DEFAULT now(),
    data_source text,
    definition_source text,
    track_historytables_id bigint DEFAULT nextval('increment_x99_t_track_historytables'::regclass) NOT NULL,
    modified_by bigint NOT NULL,
    hidden integer DEFAULT 0 NOT NULL,
    sysma_relation_type character varying
);


--
-- TOC entry 7251 (class 1259 OID 177338521)
-- Name: sysma_relation_type_parameter; Type: TABLE; Schema: sysma; Owner: -
--

CREATE TABLE sysma_relation_type_parameter (
    parameter_id bigint DEFAULT nextval('increment_sysma_entity_seq'::regclass) NOT NULL,
    sysma_relation_type_id bigint NOT NULL,
    parameter character varying(250),
    description text,
    data_type character varying(50),
    ordered_by character varying(250),
    created_at date,
    modified_at date,
    parameter_source text,
    data_source text,
    unit character varying(50),
    required smallint,
    parameter_alias character varying(50),
    time_tracking smallint,
    track_historytables_id bigint DEFAULT nextval('increment_x99_t_track_historytables'::regclass) NOT NULL,
    modified_by bigint NOT NULL,
    required_field_parameter character varying,
    hidden_in_form character varying
);


--
-- TOC entry 7254 (class 1259 OID 177338614)
-- Name: sysma_relation_type_parameter_choice; Type: TABLE; Schema: sysma; Owner: -
--

CREATE TABLE sysma_relation_type_parameter_choice (
    choice_id bigint DEFAULT nextval('increment_sysma_entity_seq'::regclass) NOT NULL,
    parameter_id bigint NOT NULL,
    choice character varying(200),
    description text,
    display_order integer,
    track_historytables_id bigint DEFAULT nextval('increment_x99_t_track_historytables'::regclass) NOT NULL,
    modified_by bigint NOT NULL,
    value_type character varying DEFAULT 'choice'::character varying
);


--
-- TOC entry 7249 (class 1259 OID 177338499)
-- Name: sysma_relation_type_sysma_relation_type_id_seq; Type: SEQUENCE; Schema: sysma; Owner: -
--

CREATE SEQUENCE sysma_relation_type_sysma_relation_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 29650 (class 0 OID 0)
-- Dependencies: 7249
-- Name: sysma_relation_type_sysma_relation_type_id_seq; Type: SEQUENCE OWNED BY; Schema: sysma; Owner: -
--

ALTER SEQUENCE sysma_relation_type_sysma_relation_type_id_seq OWNED BY sysma_relation_type.sysma_relation_type_id;


--
-- TOC entry 3029 (class 1259 OID 7577343)
-- Name: theme_analysis; Type: TABLE; Schema: sysma; Owner: -
--

CREATE TABLE theme_analysis (
    theme_analysis_id integer NOT NULL,
    theme_analysis character varying,
    style json,
    type_id integer,
    object_type character varying,
    parameter_id integer,
    track_historytables_id bigint DEFAULT nextval('increment_x99_t_track_historytables'::regclass) NOT NULL,
    theme_analysis_type character varying
);


--
-- TOC entry 29651 (class 0 OID 0)
-- Dependencies: 3029
-- Name: TABLE theme_analysis; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON TABLE theme_analysis IS '{
   "display_groupe_order":500,
   "groupe_name":"Données saisies",
   "table_comment":"Analyses thématiques"
}';


--
-- TOC entry 29652 (class 0 OID 0)
-- Dependencies: 3029
-- Name: COLUMN theme_analysis.type_id; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN theme_analysis.type_id IS 'id du type de couche (objetgeo ou travaux)';


--
-- TOC entry 29653 (class 0 OID 0)
-- Dependencies: 3029
-- Name: COLUMN theme_analysis.object_type; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN theme_analysis.object_type IS 'type_objetgeo, type_travaux';


--
-- TOC entry 29654 (class 0 OID 0)
-- Dependencies: 3029
-- Name: COLUMN theme_analysis.track_historytables_id; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN theme_analysis.track_historytables_id IS 'identifiant unique de ligne utilisé pour le suivi historique des enregistrement dans la table x99_track_historiquetables';


--
-- TOC entry 3030 (class 1259 OID 7577350)
-- Name: theme_analysis_theme_analysis_id_seq; Type: SEQUENCE; Schema: sysma; Owner: -
--

CREATE SEQUENCE theme_analysis_theme_analysis_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 29655 (class 0 OID 0)
-- Dependencies: 3030
-- Name: theme_analysis_theme_analysis_id_seq; Type: SEQUENCE OWNED BY; Schema: sysma; Owner: -
--

ALTER SEQUENCE theme_analysis_theme_analysis_id_seq OWNED BY theme_analysis.theme_analysis_id;


--
-- TOC entry 3065 (class 1259 OID 7577599)
-- Name: user_privilege; Type: TABLE; Schema: sysma; Owner: -
--

CREATE TABLE user_privilege (
    user_privilege_id integer NOT NULL,
    user_id bigint,
    sysma_object_organisation_id_list bigint[],
    sysma_object_status_list character varying[],
    sysma_action_type_id_list bigint[],
    sysma_action_status_list character varying[],
    sysma_action_organisation_id_list bigint[],
    privilege_list character varying[],
    sysma_object_type_id_list bigint[],
    track_historytables_id bigint DEFAULT nextval('increment_x99_t_track_historytables'::regclass) NOT NULL,
    modified_by bigint NOT NULL
);


--
-- TOC entry 29656 (class 0 OID 0)
-- Dependencies: 3065
-- Name: TABLE user_privilege; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON TABLE user_privilege IS '{
   "display_groupe_order":200,
   "groupe_name":"Gestion des utilisateurs",
   "table_comment":"Gestion des droits des utilisateurs sur les objets sysma"
}';


--
-- TOC entry 29657 (class 0 OID 0)
-- Dependencies: 3065
-- Name: COLUMN user_privilege.track_historytables_id; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON COLUMN user_privilege.track_historytables_id IS 'identifiant unique de ligne utilisé pour le suivi historique des enregistrement dans la table x99_track_historiquetables';


--
-- TOC entry 3066 (class 1259 OID 7577613)
-- Name: user_privilege_user_privilege_id_seq; Type: SEQUENCE; Schema: sysma; Owner: -
--

CREATE SEQUENCE user_privilege_user_privilege_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 29658 (class 0 OID 0)
-- Dependencies: 3066
-- Name: user_privilege_user_privilege_id_seq; Type: SEQUENCE OWNED BY; Schema: sysma; Owner: -
--

ALTER SEQUENCE user_privilege_user_privilege_id_seq OWNED BY user_privilege.user_privilege_id;


--
-- TOC entry 5999 (class 1259 OID 96740675)
-- Name: user_views; Type: TABLE; Schema: sysma; Owner: -
--

CREATE TABLE user_views (
    user_view_id integer NOT NULL,
    user_id integer,
    view character varying,
    view_json json,
    modified_by integer,
    modified_at timestamp without time zone DEFAULT now()
);


--
-- TOC entry 29659 (class 0 OID 0)
-- Dependencies: 5999
-- Name: TABLE user_views; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON TABLE user_views IS '{
   "display_groupe_order":550,
   "groupe_name":"Données saisies UI",
   "table_comment":"Vues (sélection de couches visibles) enregistrées par l''utilisateur"
}';


--
-- TOC entry 5998 (class 1259 OID 96740673)
-- Name: user_views_user_view_id_seq; Type: SEQUENCE; Schema: sysma; Owner: -
--

CREATE SEQUENCE user_views_user_view_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 29660 (class 0 OID 0)
-- Dependencies: 5998
-- Name: user_views_user_view_id_seq; Type: SEQUENCE OWNED BY; Schema: sysma; Owner: -
--

ALTER SEQUENCE user_views_user_view_id_seq OWNED BY user_views.user_view_id;


--
-- TOC entry 5598 (class 1259 OID 73690602)
-- Name: versions_schema; Type: TABLE; Schema: sysma; Owner: -
--

CREATE TABLE versions_schema (
    version_id bigint NOT NULL,
    version_schema_num character varying,
    version_schema_date timestamp with time zone DEFAULT now(),
    version_schema_comment text
);


--
-- TOC entry 29661 (class 0 OID 0)
-- Dependencies: 5598
-- Name: TABLE versions_schema; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON TABLE versions_schema IS '{
   "display_groupe_order":900,
   "groupe_name":"ADMINITRATION EPTBSN",
   "table_comment":"Stocke la version du schema de sysma installée sur cette instance"
}

';


--
-- TOC entry 5600 (class 1259 OID 73690665)
-- Name: versions_schema_ddl; Type: TABLE; Schema: sysma; Owner: -
--

CREATE TABLE versions_schema_ddl (
    versions_schema_ddl_id integer NOT NULL,
    tg_tag text,
    object_type text,
    schema_name name,
    object_name name,
    object_identity text,
    in_extension boolean,
    classid oid,
    objid oid,
    objsubid bigint,
    original boolean,
    normal boolean,
    is_temporary boolean,
    address_names text[],
    address_args text[],
    ddl_sql text DEFAULT current_query(),
    ddl_user text DEFAULT "current_user"(),
    ddl_date timestamp without time zone DEFAULT now()
);


--
-- TOC entry 29662 (class 0 OID 0)
-- Dependencies: 5600
-- Name: TABLE versions_schema_ddl; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON TABLE versions_schema_ddl IS '{
   "display_groupe_order":900,
   "groupe_name":"ADMINITRATION EPTBSN",
   "table_comment":"Track ddl actions usefull for schema versioning (Data definition language)"
}

 ';


--
-- TOC entry 5599 (class 1259 OID 73690663)
-- Name: versions_schema_ddl_id_seq; Type: SEQUENCE; Schema: sysma; Owner: -
--

CREATE SEQUENCE versions_schema_ddl_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 29663 (class 0 OID 0)
-- Dependencies: 5599
-- Name: versions_schema_ddl_id_seq; Type: SEQUENCE OWNED BY; Schema: sysma; Owner: -
--

ALTER SEQUENCE versions_schema_ddl_id_seq OWNED BY versions_schema_ddl.versions_schema_ddl_id;


--
-- TOC entry 5597 (class 1259 OID 73690600)
-- Name: versions_schema_version_id_seq; Type: SEQUENCE; Schema: sysma; Owner: -
--

CREATE SEQUENCE versions_schema_version_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 29664 (class 0 OID 0)
-- Dependencies: 5597
-- Name: versions_schema_version_id_seq; Type: SEQUENCE OWNED BY; Schema: sysma; Owner: -
--

ALTER SEQUENCE versions_schema_version_id_seq OWNED BY versions_schema.version_id;


--
-- TOC entry 3062 (class 1259 OID 7577555)
-- Name: x99_t_track_historytables; Type: TABLE; Schema: sysma; Owner: -
--

CREATE TABLE x99_t_track_historytables (
    x99_t_track_historytables_id integer NOT NULL,
    change_date timestamp with time zone DEFAULT now(),
    change_op character varying(15),
    schema_name name,
    table_name name,
    row_id_track_historytables integer,
    modified_by integer,
    old_row json,
    new_row json
);


--
-- TOC entry 29665 (class 0 OID 0)
-- Dependencies: 3062
-- Name: TABLE x99_t_track_historytables; Type: COMMENT; Schema: sysma; Owner: -
--

COMMENT ON TABLE x99_t_track_historytables IS '{
   "display_groupe_order":600,
   "groupe_name":"Surveillance",
   "table_comment":"Suivi des modifications réalisées dans sysma"
}';


--
-- TOC entry 3063 (class 1259 OID 7577562)
-- Name: x99_t_track_historytables_id_seq; Type: SEQUENCE; Schema: sysma; Owner: -
--

CREATE SEQUENCE x99_t_track_historytables_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 29666 (class 0 OID 0)
-- Dependencies: 3063
-- Name: x99_t_track_historytables_id_seq; Type: SEQUENCE OWNED BY; Schema: sysma; Owner: -
--

ALTER SEQUENCE x99_t_track_historytables_id_seq OWNED BY x99_t_track_historytables.x99_t_track_historytables_id;


--
-- TOC entry 28630 (class 2604 OID 151230128)
-- Name: contract contract_id; Type: DEFAULT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY contract ALTER COLUMN contract_id SET DEFAULT nextval('contract_contract_id_seq'::regclass);


--
-- TOC entry 28636 (class 2604 OID 151230129)
-- Name: filter filter_id; Type: DEFAULT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY filter ALTER COLUMN filter_id SET DEFAULT nextval('filter_filter_id_seq'::regclass);


--
-- TOC entry 28649 (class 2604 OID 151230139)
-- Name: import_report report_id; Type: DEFAULT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY import_report ALTER COLUMN report_id SET DEFAULT nextval('import_report_report_id_seq'::regclass);


--
-- TOC entry 28642 (class 2604 OID 151230133)
-- Name: l_filter_user l_filter_user_id; Type: DEFAULT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY l_filter_user ALTER COLUMN l_filter_user_id SET DEFAULT nextval('l_filter_user_l_filter_user_id_seq'::regclass);


--
-- TOC entry 28662 (class 2604 OID 151230134)
-- Name: l_group_user l_group_user_id; Type: DEFAULT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY l_group_user ALTER COLUMN l_group_user_id SET DEFAULT nextval('l_group_user_l_group_user_id_seq'::regclass);


--
-- TOC entry 28639 (class 2604 OID 151230131)
-- Name: l_other_layer_user l_other_layer_user_id; Type: DEFAULT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY l_other_layer_user ALTER COLUMN l_other_layer_user_id SET DEFAULT nextval('l_other_layer_user_id_seq'::regclass);


--
-- TOC entry 28641 (class 2604 OID 151230132)
-- Name: l_ref_layer_user l_ref_layer_user_id; Type: DEFAULT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY l_ref_layer_user ALTER COLUMN l_ref_layer_user_id SET DEFAULT nextval('l_ref_layer_user_l_other_layer_user_id_seq'::regclass);


--
-- TOC entry 28643 (class 2604 OID 151230135)
-- Name: l_style_user l_style_user_id; Type: DEFAULT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY l_style_user ALTER COLUMN l_style_user_id SET DEFAULT nextval('l_style_user_l_style_user_id_seq'::regclass);


--
-- TOC entry 28646 (class 2604 OID 151230137)
-- Name: l_sysma_action_type_user l_sysma_action_type_user_id; Type: DEFAULT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY l_sysma_action_type_user ALTER COLUMN l_sysma_action_type_user_id SET DEFAULT nextval('l_sysma_action_type_user_l_sysma_action_type_user_id_seq'::regclass);


--
-- TOC entry 28691 (class 2604 OID 177338638)
-- Name: l_sysma_object_sysma_relation track_historytables_id; Type: DEFAULT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY l_sysma_object_sysma_relation ALTER COLUMN track_historytables_id SET DEFAULT nextval('l_sysma_object_sysma_relation_track_historytables_id_seq'::regclass);


--
-- TOC entry 28692 (class 2604 OID 177338666)
-- Name: l_sysma_object_type_sysma_relation_type track_historytables_id; Type: DEFAULT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY l_sysma_object_type_sysma_relation_type ALTER COLUMN track_historytables_id SET DEFAULT nextval('l_sysma_object_type_sysma_relation_t_track_historytables_id_seq'::regclass);


--
-- TOC entry 28693 (class 2604 OID 177338667)
-- Name: l_sysma_object_type_sysma_relation_type l_sysma_object_type_sysma_relation_type_id; Type: DEFAULT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY l_sysma_object_type_sysma_relation_type ALTER COLUMN l_sysma_object_type_sysma_relation_type_id SET DEFAULT nextval('l_sysma_object_type_sysma_rel_l_sysma_object_type_sysma_rel_seq'::regclass);


--
-- TOC entry 28644 (class 2604 OID 151230136)
-- Name: l_sysma_object_type_user l_sysma_object_type_user_id; Type: DEFAULT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY l_sysma_object_type_user ALTER COLUMN l_sysma_object_type_user_id SET DEFAULT nextval('l_sysma_object_type_user_l_sysma_object_type_user_id_seq'::regclass);


--
-- TOC entry 28637 (class 2604 OID 151230130)
-- Name: l_theme_analysis_user l_theme_analysis_user_id; Type: DEFAULT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY l_theme_analysis_user ALTER COLUMN l_theme_analysis_user_id SET DEFAULT nextval('l_theme_analysis_user_l_theme_analysis_user_id_seq'::regclass);


--
-- TOC entry 28661 (class 2604 OID 151230138)
-- Name: layer_group layer_group_id; Type: DEFAULT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY layer_group ALTER COLUMN layer_group_id SET DEFAULT nextval('layer_group_layer_group_id_seq'::regclass);


--
-- TOC entry 28617 (class 2604 OID 151230127)
-- Name: other_layer other_layer_id; Type: DEFAULT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY other_layer ALTER COLUMN other_layer_id SET DEFAULT nextval('other_layer_other_layer_id_seq'::regclass);


--
-- TOC entry 28651 (class 2604 OID 151230140)
-- Name: status status_id; Type: DEFAULT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY status ALTER COLUMN status_id SET DEFAULT nextval('status_status_id_seq'::regclass);


--
-- TOC entry 28671 (class 2604 OID 151230141)
-- Name: sysma_conf sysma_conf_id; Type: DEFAULT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_conf ALTER COLUMN sysma_conf_id SET DEFAULT nextval('sysma_conf_sysma_conf_id_seq'::regclass);


--
-- TOC entry 28615 (class 2604 OID 151230126)
-- Name: theme_analysis theme_analysis_id; Type: DEFAULT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY theme_analysis ALTER COLUMN theme_analysis_id SET DEFAULT nextval('theme_analysis_theme_analysis_id_seq'::regclass);


--
-- TOC entry 28659 (class 2604 OID 151230143)
-- Name: user_privilege user_privilege_id; Type: DEFAULT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY user_privilege ALTER COLUMN user_privilege_id SET DEFAULT nextval('user_privilege_user_privilege_id_seq'::regclass);


--
-- TOC entry 28669 (class 2604 OID 151230142)
-- Name: user_views user_view_id; Type: DEFAULT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY user_views ALTER COLUMN user_view_id SET DEFAULT nextval('user_views_user_view_id_seq'::regclass);


--
-- TOC entry 28663 (class 2604 OID 151230144)
-- Name: versions_schema version_id; Type: DEFAULT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY versions_schema ALTER COLUMN version_id SET DEFAULT nextval('versions_schema_version_id_seq'::regclass);


--
-- TOC entry 28665 (class 2604 OID 151230145)
-- Name: versions_schema_ddl versions_schema_ddl_id; Type: DEFAULT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY versions_schema_ddl ALTER COLUMN versions_schema_ddl_id SET DEFAULT nextval('versions_schema_ddl_id_seq'::regclass);


--
-- TOC entry 28653 (class 2604 OID 151230146)
-- Name: x99_t_track_historytables x99_t_track_historytables_id; Type: DEFAULT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY x99_t_track_historytables ALTER COLUMN x99_t_track_historytables_id SET DEFAULT nextval('x99_t_track_historytables_id_seq'::regclass);


--
-- TOC entry 28751 (class 2606 OID 8534954)
-- Name: contract contract_pkey; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY contract
    ADD CONSTRAINT contract_pkey PRIMARY KEY (contract_id);


--
-- TOC entry 28747 (class 2606 OID 8535030)
-- Name: data_type_choice data_type_choice_uniq; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY data_type_choice
    ADD CONSTRAINT data_type_choice_uniq UNIQUE (data_type);


--
-- TOC entry 28749 (class 2606 OID 8534952)
-- Name: data_type_choice data_type_id_pkey; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY data_type_choice
    ADD CONSTRAINT data_type_id_pkey PRIMARY KEY (data_type_id);


--
-- TOC entry 28759 (class 2606 OID 8534968)
-- Name: filter filter_pkey; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY filter
    ADD CONSTRAINT filter_pkey PRIMARY KEY (filter_id);


--
-- TOC entry 28799 (class 2606 OID 68884079)
-- Name: layer_group geoobject_type_group_group_alias_key; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY layer_group
    ADD CONSTRAINT geoobject_type_group_group_alias_key UNIQUE (group_alias);


--
-- TOC entry 28801 (class 2606 OID 68884077)
-- Name: layer_group geoobject_type_group_group_name_key; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY layer_group
    ADD CONSTRAINT geoobject_type_group_group_name_key UNIQUE (group_name);


--
-- TOC entry 28803 (class 2606 OID 68884049)
-- Name: layer_group geoobject_type_group_pkey; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY layer_group
    ADD CONSTRAINT geoobject_type_group_pkey PRIMARY KEY (layer_group_id);


--
-- TOC entry 28786 (class 2606 OID 8535002)
-- Name: import_report import_report_pkey; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY import_report
    ADD CONSTRAINT import_report_pkey PRIMARY KEY (report_id);


--
-- TOC entry 28805 (class 2606 OID 68884090)
-- Name: l_group_user l_group_user_pkey; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY l_group_user
    ADD CONSTRAINT l_group_user_pkey PRIMARY KEY (l_group_user_id);


--
-- TOC entry 28763 (class 2606 OID 8534976)
-- Name: l_other_layer_user l_other_layer_user_pkey; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY l_other_layer_user
    ADD CONSTRAINT l_other_layer_user_pkey PRIMARY KEY (l_other_layer_user_id);


--
-- TOC entry 28765 (class 2606 OID 177342159)
-- Name: l_other_layer_user l_other_layer_user_uniq; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY l_other_layer_user
    ADD CONSTRAINT l_other_layer_user_uniq UNIQUE (layer, user_id);


--
-- TOC entry 28768 (class 2606 OID 8534972)
-- Name: l_ref_layer_user l_ref_layer_user_l_ref_layer_user_id_idx; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY l_ref_layer_user
    ADD CONSTRAINT l_ref_layer_user_l_ref_layer_user_id_idx PRIMARY KEY (l_ref_layer_user_id);


--
-- TOC entry 28770 (class 2606 OID 8534984)
-- Name: l_filter_user l_ref_layer_user_pkey; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY l_filter_user
    ADD CONSTRAINT l_ref_layer_user_pkey PRIMARY KEY (l_filter_user_id);


--
-- TOC entry 28772 (class 2606 OID 8534986)
-- Name: l_style_user l_style_user_pkey; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY l_style_user
    ADD CONSTRAINT l_style_user_pkey PRIMARY KEY (l_style_user_id);


--
-- TOC entry 28779 (class 2606 OID 8534990)
-- Name: l_sysma_action_type_user l_sysma_action_type_user_pkey; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY l_sysma_action_type_user
    ADD CONSTRAINT l_sysma_action_type_user_pkey PRIMARY KEY (l_sysma_action_type_user_id);


--
-- TOC entry 28781 (class 2606 OID 177342206)
-- Name: l_sysma_action_type_user l_sysma_action_type_user_pkey_uniq; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY l_sysma_action_type_user
    ADD CONSTRAINT l_sysma_action_type_user_pkey_uniq UNIQUE (sysma_action_type_id, user_id);


--
-- TOC entry 28695 (class 2606 OID 177341761)
-- Name: l_sysma_object_sysma_action l_sysma_object_sysma_action_pkey; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY l_sysma_object_sysma_action
    ADD CONSTRAINT l_sysma_object_sysma_action_pkey PRIMARY KEY (sysma_object_id, sysma_action_id);


--
-- TOC entry 28837 (class 2606 OID 177342247)
-- Name: l_sysma_object_sysma_relation l_sysma_object_sysma_relation_pk; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY l_sysma_object_sysma_relation
    ADD CONSTRAINT l_sysma_object_sysma_relation_pk PRIMARY KEY (sysma_object_id, parent_child, sysma_relation_id);


--
-- TOC entry 28839 (class 2606 OID 177342289)
-- Name: l_sysma_object_type_sysma_relation_type l_sysma_object_type_sysma_relation_type_pkey; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY l_sysma_object_type_sysma_relation_type
    ADD CONSTRAINT l_sysma_object_type_sysma_relation_type_pkey PRIMARY KEY (sysma_object_type_id_parent, sysma_object_type_id_child, sysma_relation_type_id);


--
-- TOC entry 28774 (class 2606 OID 8534988)
-- Name: l_sysma_object_type_user l_sysma_object_type_user_pkey; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY l_sysma_object_type_user
    ADD CONSTRAINT l_sysma_object_type_user_pkey PRIMARY KEY (l_sysma_object_type_user_id);


--
-- TOC entry 28776 (class 2606 OID 177342308)
-- Name: l_sysma_object_type_user l_sysma_object_type_user_uniq; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY l_sysma_object_type_user
    ADD CONSTRAINT l_sysma_object_type_user_uniq UNIQUE (sysma_object_type_id, user_id);


--
-- TOC entry 28761 (class 2606 OID 8534974)
-- Name: l_theme_analysis_user l_theme_analysis_user_l_theme_analysis_user_id_idex; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY l_theme_analysis_user
    ADD CONSTRAINT l_theme_analysis_user_l_theme_analysis_user_id_idex PRIMARY KEY (l_theme_analysis_user_id);


--
-- TOC entry 28701 (class 2606 OID 8535010)
-- Name: organisation organisation_pkey; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY organisation
    ADD CONSTRAINT organisation_pkey PRIMARY KEY (organisation_id);


--
-- TOC entry 28703 (class 2606 OID 8535046)
-- Name: organisation organisation_uniq; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY organisation
    ADD CONSTRAINT organisation_uniq UNIQUE (organisation);


--
-- TOC entry 28734 (class 2606 OID 8534946)
-- Name: other_layer other_layer_pkey; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY other_layer
    ADD CONSTRAINT other_layer_pkey PRIMARY KEY (other_layer_id);


--
-- TOC entry 28784 (class 2606 OID 8534996)
-- Name: photo_object photo_object_pkey; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY photo_object
    ADD CONSTRAINT photo_object_pkey PRIMARY KEY (photo_object_id);


--
-- TOC entry 28788 (class 2606 OID 8535008)
-- Name: status status_pkey; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY status
    ADD CONSTRAINT status_pkey PRIMARY KEY (status_id);


--
-- TOC entry 28753 (class 2606 OID 177343747)
-- Name: sysma_action_data sysma_action_data_non_ld_uniq; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_action_data
    ADD CONSTRAINT sysma_action_data_non_ld_uniq EXCLUDE USING btree (sysma_action_id WITH =, parameter_id WITH =, created_at WITH =) WHERE ((f_test_action_parameter_is_ld(parameter_id) IS NOT TRUE));


--
-- TOC entry 28755 (class 2606 OID 177341608)
-- Name: sysma_action_data sysma_action_id_pkey; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_action_data
    ADD CONSTRAINT sysma_action_id_pkey PRIMARY KEY (sysma_action_data_id);


--
-- TOC entry 28757 (class 2606 OID 177342394)
-- Name: sysma_action_data sysma_action_id_uniq; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_action_data
    ADD CONSTRAINT sysma_action_id_uniq UNIQUE (sysma_action_id, parameter_id, value, created_at);


--
-- TOC entry 28705 (class 2606 OID 177341521)
-- Name: sysma_action sysma_action_pkey; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_action
    ADD CONSTRAINT sysma_action_pkey PRIMARY KEY (sysma_action_id);


--
-- TOC entry 28713 (class 2606 OID 177342451)
-- Name: sysma_action_type sysma_action_type_id_alias_pkey; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_action_type
    ADD CONSTRAINT sysma_action_type_id_alias_pkey UNIQUE (sysma_object_type_id, sysma_action_type_alias);


--
-- TOC entry 28743 (class 2606 OID 177341626)
-- Name: sysma_action_type_parameter_choice sysma_action_type_parameter_choice_pkey; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_action_type_parameter_choice
    ADD CONSTRAINT sysma_action_type_parameter_choice_pkey PRIMARY KEY (choice_id);


--
-- TOC entry 28745 (class 2606 OID 177342507)
-- Name: sysma_action_type_parameter_choice sysma_action_type_parameter_choice_uniq; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_action_type_parameter_choice
    ADD CONSTRAINT sysma_action_type_parameter_choice_uniq UNIQUE (parameter_id, choice);


--
-- TOC entry 28792 (class 2606 OID 177341849)
-- Name: sysma_action_type_parameter sysma_action_type_parameter_id_alias_pkey; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_action_type_parameter
    ADD CONSTRAINT sysma_action_type_parameter_id_alias_pkey UNIQUE (sysma_action_type_id, parameter_alias);


--
-- TOC entry 28794 (class 2606 OID 177341822)
-- Name: sysma_action_type_parameter sysma_action_type_parameter_pkey; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_action_type_parameter
    ADD CONSTRAINT sysma_action_type_parameter_pkey PRIMARY KEY (parameter_id);


--
-- TOC entry 28715 (class 2606 OID 177341547)
-- Name: sysma_action_type sysma_action_type_pkey; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_action_type
    ADD CONSTRAINT sysma_action_type_pkey PRIMARY KEY (sysma_action_type_id);


--
-- TOC entry 28813 (class 2606 OID 101936423)
-- Name: sysma_conf sysma_conf_pkey; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_conf
    ADD CONSTRAINT sysma_conf_pkey PRIMARY KEY (sysma_conf_id);


--
-- TOC entry 28722 (class 2606 OID 177343751)
-- Name: sysma_object_data sysma_object_data_non_ld_uniq; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_object_data
    ADD CONSTRAINT sysma_object_data_non_ld_uniq EXCLUDE USING btree (sysma_object_id WITH =, parameter_id WITH =, end_date WITH =, start_date WITH =) WHERE ((f_test_object_parameter_is_ld(parameter_id) IS NOT TRUE));


--
-- TOC entry 28724 (class 2606 OID 177341482)
-- Name: sysma_object_data sysma_object_data_pkey; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_object_data
    ADD CONSTRAINT sysma_object_data_pkey PRIMARY KEY (sysma_object_data_id);


--
-- TOC entry 28726 (class 2606 OID 177343311)
-- Name: sysma_object_data sysma_object_data_uniq; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_object_data
    ADD CONSTRAINT sysma_object_data_uniq UNIQUE (sysma_object_id, parameter_id, end_date, start_date, value);


--
-- TOC entry 28699 (class 2606 OID 177340657)
-- Name: sysma_object sysma_object_pkey; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_object
    ADD CONSTRAINT sysma_object_pkey PRIMARY KEY (sysma_object_id);


--
-- TOC entry 28707 (class 2606 OID 104209977)
-- Name: sysma_object_type sysma_object_type_alias_pkey; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_object_type
    ADD CONSTRAINT sysma_object_type_alias_pkey UNIQUE (sysma_object_type_alias);


--
-- TOC entry 28709 (class 2606 OID 8535024)
-- Name: sysma_object_type sysma_object_type_alias_uniq; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_object_type
    ADD CONSTRAINT sysma_object_type_alias_uniq UNIQUE (sysma_object_type_alias);


--
-- TOC entry 28739 (class 2606 OID 177341502)
-- Name: sysma_object_type_parameter_choice sysma_object_type_parameter_choice_pkey; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_object_type_parameter_choice
    ADD CONSTRAINT sysma_object_type_parameter_choice_pkey PRIMARY KEY (choice_id);


--
-- TOC entry 28741 (class 2606 OID 177343440)
-- Name: sysma_object_type_parameter_choice sysma_object_type_parameter_choice_uniq; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_object_type_parameter_choice
    ADD CONSTRAINT sysma_object_type_parameter_choice_uniq UNIQUE (parameter_id, choice);


--
-- TOC entry 28728 (class 2606 OID 177341895)
-- Name: sysma_object_type_parameter sysma_object_type_parameter_id_alias_pkey; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_object_type_parameter
    ADD CONSTRAINT sysma_object_type_parameter_id_alias_pkey UNIQUE (sysma_object_type_id, parameter_alias);


--
-- TOC entry 28730 (class 2606 OID 177341868)
-- Name: sysma_object_type_parameter sysma_object_type_parameter_pkey; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_object_type_parameter
    ADD CONSTRAINT sysma_object_type_parameter_pkey PRIMARY KEY (parameter_id);


--
-- TOC entry 28711 (class 2606 OID 177343380)
-- Name: sysma_object_type sysma_object_type_pkey; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_object_type
    ADD CONSTRAINT sysma_object_type_pkey PRIMARY KEY (sysma_object_type_id);


--
-- TOC entry 28826 (class 2606 OID 177343755)
-- Name: sysma_relation_data sysma_relation_data_non_ld_uniq; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_relation_data
    ADD CONSTRAINT sysma_relation_data_non_ld_uniq EXCLUDE USING btree (sysma_relation_id WITH =, parameter_id WITH =, created_at WITH =) WHERE ((f_test_relation_parameter_is_ld(parameter_id) IS NOT TRUE));


--
-- TOC entry 28828 (class 2606 OID 177341710)
-- Name: sysma_relation_data sysma_relation_id_pkey; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_relation_data
    ADD CONSTRAINT sysma_relation_id_pkey PRIMARY KEY (sysma_relation_data_id);


--
-- TOC entry 28830 (class 2606 OID 177343517)
-- Name: sysma_relation_data sysma_relation_id_uniq; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_relation_data
    ADD CONSTRAINT sysma_relation_id_uniq UNIQUE (sysma_relation_id, parameter_id, value, created_at);


--
-- TOC entry 28833 (class 2606 OID 177341720)
-- Name: sysma_relation_type_parameter_choice sysma_relation_parameter_choice_pkey; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_relation_type_parameter_choice
    ADD CONSTRAINT sysma_relation_parameter_choice_pkey PRIMARY KEY (choice_id);


--
-- TOC entry 28835 (class 2606 OID 177343619)
-- Name: sysma_relation_type_parameter_choice sysma_relation_parameter_choice_uniq; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_relation_type_parameter_choice
    ADD CONSTRAINT sysma_relation_parameter_choice_uniq UNIQUE (parameter_id, choice);


--
-- TOC entry 28823 (class 2606 OID 177341644)
-- Name: sysma_relation sysma_relation_pkey; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_relation
    ADD CONSTRAINT sysma_relation_pkey PRIMARY KEY (sysma_relation_id);


--
-- TOC entry 28815 (class 2606 OID 177338515)
-- Name: sysma_relation_type sysma_relation_type_alias_pkey; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_relation_type
    ADD CONSTRAINT sysma_relation_type_alias_pkey UNIQUE (sysma_relation_type_alias);


--
-- TOC entry 28819 (class 2606 OID 177341941)
-- Name: sysma_relation_type_parameter sysma_relation_type_parameter_id_alias_pkey; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_relation_type_parameter
    ADD CONSTRAINT sysma_relation_type_parameter_id_alias_pkey UNIQUE (sysma_relation_type_id, parameter_alias);


--
-- TOC entry 28821 (class 2606 OID 177341914)
-- Name: sysma_relation_type_parameter sysma_relation_type_parameter_pkey; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_relation_type_parameter
    ADD CONSTRAINT sysma_relation_type_parameter_pkey PRIMARY KEY (parameter_id);


--
-- TOC entry 28817 (class 2606 OID 177343575)
-- Name: sysma_relation_type sysma_relation_type_pkey; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_relation_type
    ADD CONSTRAINT sysma_relation_type_pkey PRIMARY KEY (sysma_relation_type_id);


--
-- TOC entry 28732 (class 2606 OID 8534944)
-- Name: theme_analysis theme_analysis_theme_analysis_id_pkey; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY theme_analysis
    ADD CONSTRAINT theme_analysis_theme_analysis_id_pkey PRIMARY KEY (theme_analysis_id);


--
-- TOC entry 28790 (class 2606 OID 8534998)
-- Name: x99_t_track_historytables track_historytables_id_pkey; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY x99_t_track_historytables
    ADD CONSTRAINT track_historytables_id_pkey PRIMARY KEY (x99_t_track_historytables_id);


--
-- TOC entry 28736 (class 2606 OID 181750227)
-- Name: other_layer uniq_other_layer_layer_alias; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY other_layer
    ADD CONSTRAINT uniq_other_layer_layer_alias UNIQUE (layer_alias);


--
-- TOC entry 28717 (class 2606 OID 8535052)
-- Name: user user_pkey; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (user_id);


--
-- TOC entry 28719 (class 2606 OID 107178679)
-- Name: user user_uniq; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_uniq UNIQUE ("user");


--
-- TOC entry 28797 (class 2606 OID 8535048)
-- Name: user_privilege user_uniq_pkey; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY user_privilege
    ADD CONSTRAINT user_uniq_pkey PRIMARY KEY (user_privilege_id);


--
-- TOC entry 28811 (class 2606 OID 96740684)
-- Name: user_views user_views_id_pk; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY user_views
    ADD CONSTRAINT user_views_id_pk PRIMARY KEY (user_view_id);


--
-- TOC entry 28809 (class 2606 OID 73690676)
-- Name: versions_schema_ddl version_schema_ddl_pkey; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY versions_schema_ddl
    ADD CONSTRAINT version_schema_ddl_pkey PRIMARY KEY (versions_schema_ddl_id);


--
-- TOC entry 28807 (class 2606 OID 73690611)
-- Name: versions_schema versions_schema_pkey; Type: CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY versions_schema
    ADD CONSTRAINT versions_schema_pkey PRIMARY KEY (version_id);


--
-- TOC entry 28766 (class 1259 OID 177342160)
-- Name: l_other_layer_user_user_id_idx; Type: INDEX; Schema: sysma; Owner: -
--

CREATE INDEX l_other_layer_user_user_id_idx ON sysma.l_other_layer_user USING btree (user_id);


--
-- TOC entry 28782 (class 1259 OID 177342207)
-- Name: l_sysma_action_type_user_user_id_idx; Type: INDEX; Schema: sysma; Owner: -
--

CREATE INDEX l_sysma_action_type_user_user_id_idx ON sysma.l_sysma_action_type_user USING btree (user_id);


--
-- TOC entry 28777 (class 1259 OID 177342309)
-- Name: l_sysma_object_type_user_user_id_idx; Type: INDEX; Schema: sysma; Owner: -
--

CREATE INDEX l_sysma_object_type_user_user_id_idx ON sysma.l_sysma_object_type_user USING btree (user_id);


--
-- TOC entry 28720 (class 1259 OID 177343292)
-- Name: sysma_data_object_object_id_idx; Type: INDEX; Schema: sysma; Owner: -
--

CREATE INDEX sysma_data_object_object_id_idx ON sysma.sysma_object_data USING btree (sysma_object_id);


--
-- TOC entry 28696 (class 1259 OID 8536786)
-- Name: sysma_object_geom_sidx; Type: INDEX; Schema: sysma; Owner: -
--

CREATE INDEX sysma_object_geom_sidx ON sysma.sysma_object USING gist (geom);


--
-- TOC entry 28697 (class 1259 OID 177341099)
-- Name: sysma_object_object_type_id_idx; Type: INDEX; Schema: sysma; Owner: -
--

CREATE INDEX sysma_object_object_type_id_idx ON sysma.sysma_object USING btree (sysma_object_type_id);


--
-- TOC entry 28737 (class 1259 OID 177343441)
-- Name: sysma_object_type_parameter_choice_parameter_id_fk; Type: INDEX; Schema: sysma; Owner: -
--

CREATE INDEX sysma_object_type_parameter_choice_parameter_id_fk ON sysma.sysma_object_type_parameter_choice USING btree (parameter_id);


--
-- TOC entry 28831 (class 1259 OID 177343620)
-- Name: sysma_relation_parameter_choice_parameter_id_fk; Type: INDEX; Schema: sysma; Owner: -
--

CREATE INDEX sysma_relation_parameter_choice_parameter_id_fk ON sysma.sysma_relation_type_parameter_choice USING btree (parameter_id);


--
-- TOC entry 28824 (class 1259 OID 177341690)
-- Name: sysma_relation_relation_type_id_idx; Type: INDEX; Schema: sysma; Owner: -
--

CREATE INDEX sysma_relation_relation_type_id_idx ON sysma.sysma_relation USING btree (sysma_relation_type_id);


--
-- TOC entry 28795 (class 1259 OID 177343731)
-- Name: user_privilege_user_id_fkey; Type: INDEX; Schema: sysma; Owner: -
--

CREATE INDEX user_privilege_user_id_fkey ON sysma.user_privilege USING btree (user_id);


--
-- TOC entry 28917 (class 2620 OID 74464106)
-- Name: sysma_object_data a01_check_data_donnees_objetgeo; Type: TRIGGER; Schema: sysma; Owner: -
--

CREATE TRIGGER a01_check_data_donnees_objetgeo BEFORE INSERT OR UPDATE ON sysma.sysma_object_data FOR EACH ROW EXECUTE PROCEDURE trg_f_01_set_default_dates_values();


--
-- TOC entry 28919 (class 2620 OID 8537295)
-- Name: sysma_object_type_parameter_choice trackhisto_choix_parametre_objetgeo; Type: TRIGGER; Schema: sysma; Owner: -
--

CREATE TRIGGER trackhisto_choix_parametre_objetgeo BEFORE INSERT OR DELETE OR UPDATE ON sysma.sysma_object_type_parameter_choice FOR EACH ROW EXECUTE PROCEDURE trg_f_track_historiquetables();


--
-- TOC entry 28920 (class 2620 OID 8537296)
-- Name: sysma_action_type_parameter_choice trackhisto_choix_parametre_travaux; Type: TRIGGER; Schema: sysma; Owner: -
--

CREATE TRIGGER trackhisto_choix_parametre_travaux BEFORE INSERT OR DELETE OR UPDATE ON sysma.sysma_action_type_parameter_choice FOR EACH ROW EXECUTE PROCEDURE trg_f_track_historiquetables();


--
-- TOC entry 28916 (class 2620 OID 8537297)
-- Name: sysma_object_data trackhisto_donnees_objetgeo; Type: TRIGGER; Schema: sysma; Owner: -
--

CREATE TRIGGER trackhisto_donnees_objetgeo BEFORE INSERT OR DELETE OR UPDATE ON sysma.sysma_object_data FOR EACH ROW EXECUTE PROCEDURE trg_f_track_historiquetables();


--
-- TOC entry 28921 (class 2620 OID 8537298)
-- Name: sysma_action_data trackhisto_donnees_travaux; Type: TRIGGER; Schema: sysma; Owner: -
--

CREATE TRIGGER trackhisto_donnees_travaux BEFORE INSERT OR DELETE OR UPDATE ON sysma.sysma_action_data FOR EACH ROW EXECUTE PROCEDURE trg_f_track_historiquetables();


--
-- TOC entry 28910 (class 2620 OID 8537299)
-- Name: l_sysma_object_sysma_action trackhisto_l_objetgeo_travaux; Type: TRIGGER; Schema: sysma; Owner: -
--

CREATE TRIGGER trackhisto_l_objetgeo_travaux BEFORE INSERT OR DELETE OR UPDATE ON sysma.l_sysma_object_sysma_action FOR EACH ROW EXECUTE PROCEDURE trg_f_track_historiquetables();


--
-- TOC entry 28911 (class 2620 OID 8537300)
-- Name: sysma_object trackhisto_objetsgeo; Type: TRIGGER; Schema: sysma; Owner: -
--

CREATE TRIGGER trackhisto_objetsgeo BEFORE INSERT OR DELETE OR UPDATE ON sysma.sysma_object FOR EACH ROW EXECUTE PROCEDURE trg_f_track_historiquetables();


--
-- TOC entry 28926 (class 2620 OID 177343784)
-- Name: sysma_relation trackhisto_sysma_relation; Type: TRIGGER; Schema: sysma; Owner: -
--

CREATE TRIGGER trackhisto_sysma_relation BEFORE INSERT OR DELETE OR UPDATE ON sysma.sysma_relation FOR EACH ROW EXECUTE PROCEDURE trg_f_track_historiquetables();


--
-- TOC entry 28927 (class 2620 OID 177343785)
-- Name: sysma_relation_data trackhisto_sysma_relation_data; Type: TRIGGER; Schema: sysma; Owner: -
--

CREATE TRIGGER trackhisto_sysma_relation_data BEFORE INSERT OR DELETE OR UPDATE ON sysma.sysma_relation_data FOR EACH ROW EXECUTE PROCEDURE trg_f_track_historiquetables();


--
-- TOC entry 28924 (class 2620 OID 177343786)
-- Name: sysma_relation_type trackhisto_sysma_relation_type; Type: TRIGGER; Schema: sysma; Owner: -
--

CREATE TRIGGER trackhisto_sysma_relation_type BEFORE INSERT OR DELETE OR UPDATE ON sysma.sysma_relation_type FOR EACH ROW EXECUTE PROCEDURE trg_f_track_historiquetables();


--
-- TOC entry 28925 (class 2620 OID 177343787)
-- Name: sysma_relation_type_parameter trackhisto_sysma_relation_type_parameter; Type: TRIGGER; Schema: sysma; Owner: -
--

CREATE TRIGGER trackhisto_sysma_relation_type_parameter BEFORE INSERT OR DELETE OR UPDATE ON sysma.sysma_relation_type_parameter FOR EACH ROW EXECUTE PROCEDURE trg_f_track_historiquetables();


--
-- TOC entry 28928 (class 2620 OID 177343788)
-- Name: sysma_relation_type_parameter_choice trackhisto_sysma_relation_type_parameter_choice; Type: TRIGGER; Schema: sysma; Owner: -
--

CREATE TRIGGER trackhisto_sysma_relation_type_parameter_choice BEFORE INSERT OR DELETE OR UPDATE ON sysma.sysma_relation_type_parameter_choice FOR EACH ROW EXECUTE PROCEDURE trg_f_track_historiquetables();


--
-- TOC entry 28912 (class 2620 OID 8537301)
-- Name: sysma_action trackhisto_travaux; Type: TRIGGER; Schema: sysma; Owner: -
--

CREATE TRIGGER trackhisto_travaux BEFORE INSERT OR DELETE OR UPDATE ON sysma.sysma_action FOR EACH ROW EXECUTE PROCEDURE trg_f_track_historiquetables();


--
-- TOC entry 28913 (class 2620 OID 8537302)
-- Name: sysma_object_type trackhisto_types_objetgeo; Type: TRIGGER; Schema: sysma; Owner: -
--

CREATE TRIGGER trackhisto_types_objetgeo BEFORE INSERT OR DELETE OR UPDATE ON sysma.sysma_object_type FOR EACH ROW EXECUTE PROCEDURE trg_f_track_historiquetables();


--
-- TOC entry 28918 (class 2620 OID 8537303)
-- Name: sysma_object_type_parameter trackhisto_types_objetgeo_parametres; Type: TRIGGER; Schema: sysma; Owner: -
--

CREATE TRIGGER trackhisto_types_objetgeo_parametres BEFORE INSERT OR DELETE OR UPDATE ON sysma.sysma_object_type_parameter FOR EACH ROW EXECUTE PROCEDURE trg_f_track_historiquetables();


--
-- TOC entry 28914 (class 2620 OID 8537304)
-- Name: sysma_action_type trackhisto_types_travaux; Type: TRIGGER; Schema: sysma; Owner: -
--

CREATE TRIGGER trackhisto_types_travaux BEFORE INSERT OR DELETE OR UPDATE ON sysma.sysma_action_type FOR EACH ROW EXECUTE PROCEDURE trg_f_track_historiquetables();


--
-- TOC entry 28922 (class 2620 OID 8537305)
-- Name: sysma_action_type_parameter trackhisto_types_travaux_parametres; Type: TRIGGER; Schema: sysma; Owner: -
--

CREATE TRIGGER trackhisto_types_travaux_parametres BEFORE INSERT OR DELETE OR UPDATE ON sysma.sysma_action_type_parameter FOR EACH ROW EXECUTE PROCEDURE trg_f_track_historiquetables();


--
-- TOC entry 28915 (class 2620 OID 8537306)
-- Name: user trackhisto_utilisateurs; Type: TRIGGER; Schema: sysma; Owner: -
--

CREATE TRIGGER trackhisto_utilisateurs BEFORE INSERT OR DELETE OR UPDATE ON sysma."user" FOR EACH ROW EXECUTE PROCEDURE trg_f_track_historiquetables();

ALTER TABLE "user" DISABLE TRIGGER trackhisto_utilisateurs;


--
-- TOC entry 28923 (class 2620 OID 8537307)
-- Name: user_privilege trackhisto_utilisateurs_droits; Type: TRIGGER; Schema: sysma; Owner: -
--

CREATE TRIGGER trackhisto_utilisateurs_droits BEFORE INSERT OR DELETE OR UPDATE ON sysma.user_privilege FOR EACH ROW EXECUTE PROCEDURE trg_f_track_historiquetables();


--
-- TOC entry 28886 (class 2606 OID 177343737)
-- Name: user_privilege droits_util_admin_id_util_fkey; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY user_privilege
    ADD CONSTRAINT droits_util_admin_id_util_fkey FOREIGN KEY (user_id) REFERENCES "user"(user_id);


--
-- TOC entry 28861 (class 2606 OID 177343442)
-- Name: sysma_object_type_parameter_choice fk_choix_parametre_objetgeo_id_parametre; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_object_type_parameter_choice
    ADD CONSTRAINT fk_choix_parametre_objetgeo_id_parametre FOREIGN KEY (parameter_id) REFERENCES sysma_object_type_parameter(parameter_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 28903 (class 2606 OID 177343621)
-- Name: sysma_relation_type_parameter_choice fk_choix_parametre_relation_id_parametre; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_relation_type_parameter_choice
    ADD CONSTRAINT fk_choix_parametre_relation_id_parametre FOREIGN KEY (parameter_id) REFERENCES sysma_relation_type_parameter(parameter_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 28862 (class 2606 OID 177342508)
-- Name: sysma_action_type_parameter_choice fk_choix_parametre_travaux_id_parametre; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_action_type_parameter_choice
    ADD CONSTRAINT fk_choix_parametre_travaux_id_parametre FOREIGN KEY (parameter_id) REFERENCES sysma_action_type_parameter(parameter_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 28854 (class 2606 OID 177343347)
-- Name: sysma_object_data fk_donnees_objetgeo_id_modificateur; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_object_data
    ADD CONSTRAINT fk_donnees_objetgeo_id_modificateur FOREIGN KEY (modified_by) REFERENCES "user"(user_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 28857 (class 2606 OID 177343293)
-- Name: sysma_object_data fk_donnees_objetgeo_id_objetgeo; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_object_data
    ADD CONSTRAINT fk_donnees_objetgeo_id_objetgeo FOREIGN KEY (sysma_object_id) REFERENCES sysma_object(sysma_object_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 28856 (class 2606 OID 177343312)
-- Name: sysma_object_data fk_donnees_objetgeo_id_parametre; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_object_data
    ADD CONSTRAINT fk_donnees_objetgeo_id_parametre FOREIGN KEY (parameter_id) REFERENCES sysma_object_type_parameter(parameter_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 28855 (class 2606 OID 177343329)
-- Name: sysma_object_data fk_donnees_objetgeo_id_utilisateur; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_object_data
    ADD CONSTRAINT fk_donnees_objetgeo_id_utilisateur FOREIGN KEY (user_id) REFERENCES "user"(user_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 28899 (class 2606 OID 177343548)
-- Name: sysma_relation_data fk_donnees_relation_id_modificateur; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_relation_data
    ADD CONSTRAINT fk_donnees_relation_id_modificateur FOREIGN KEY (modified_by) REFERENCES "user"(user_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 28901 (class 2606 OID 177343518)
-- Name: sysma_relation_data fk_donnees_relation_id_parametre; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_relation_data
    ADD CONSTRAINT fk_donnees_relation_id_parametre FOREIGN KEY (parameter_id) REFERENCES sysma_relation_type_parameter(parameter_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 28902 (class 2606 OID 177343501)
-- Name: sysma_relation_data fk_donnees_relation_id_relation; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_relation_data
    ADD CONSTRAINT fk_donnees_relation_id_relation FOREIGN KEY (sysma_relation_id) REFERENCES sysma_relation(sysma_relation_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 28900 (class 2606 OID 177343533)
-- Name: sysma_relation_data fk_donnees_relation_id_utilisateur; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_relation_data
    ADD CONSTRAINT fk_donnees_relation_id_utilisateur FOREIGN KEY (user_id) REFERENCES "user"(user_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 28863 (class 2606 OID 177342425)
-- Name: sysma_action_data fk_donnees_travaux_id_modificateur; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_action_data
    ADD CONSTRAINT fk_donnees_travaux_id_modificateur FOREIGN KEY (modified_by) REFERENCES "user"(user_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 28865 (class 2606 OID 177342395)
-- Name: sysma_action_data fk_donnees_travaux_id_parametre; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_action_data
    ADD CONSTRAINT fk_donnees_travaux_id_parametre FOREIGN KEY (parameter_id) REFERENCES sysma_action_type_parameter(parameter_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 28866 (class 2606 OID 177342378)
-- Name: sysma_action_data fk_donnees_travaux_id_travaux; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_action_data
    ADD CONSTRAINT fk_donnees_travaux_id_travaux FOREIGN KEY (sysma_action_id) REFERENCES sysma_action(sysma_action_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 28864 (class 2606 OID 177342410)
-- Name: sysma_action_data fk_donnees_travaux_id_utilisateur; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_action_data
    ADD CONSTRAINT fk_donnees_travaux_id_utilisateur FOREIGN KEY (user_id) REFERENCES "user"(user_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 28844 (class 2606 OID 177342911)
-- Name: sysma_object fk_id_createur; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_object
    ADD CONSTRAINT fk_id_createur FOREIGN KEY (created_by) REFERENCES "user"(user_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 28896 (class 2606 OID 177343484)
-- Name: sysma_relation fk_id_createur; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_relation
    ADD CONSTRAINT fk_id_createur FOREIGN KEY (created_by) REFERENCES "user"(user_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 28845 (class 2606 OID 177342533)
-- Name: sysma_object fk_id_structure; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_object
    ADD CONSTRAINT fk_id_structure FOREIGN KEY (organisation_id) REFERENCES organisation(organisation_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 28897 (class 2606 OID 177343469)
-- Name: sysma_relation fk_id_structure; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_relation
    ADD CONSTRAINT fk_id_structure FOREIGN KEY (organisation_id) REFERENCES organisation(organisation_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 28843 (class 2606 OID 177343381)
-- Name: sysma_object fk_id_type_objetgeo; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_object
    ADD CONSTRAINT fk_id_type_objetgeo FOREIGN KEY (sysma_object_type_id) REFERENCES sysma_object_type(sysma_object_type_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 28895 (class 2606 OID 177343576)
-- Name: sysma_relation fk_id_type_relation; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_relation
    ADD CONSTRAINT fk_id_type_relation FOREIGN KEY (sysma_relation_type_id) REFERENCES sysma_relation_type(sysma_relation_type_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 28880 (class 2606 OID 177343820)
-- Name: import_report fk_import_report_user_id; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY import_report
    ADD CONSTRAINT fk_import_report_user_id FOREIGN KEY (user_id) REFERENCES "user"(user_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 28871 (class 2606 OID 177342177)
-- Name: l_ref_layer_user fk_l_coucheref_utilisateur_id_utilisateur; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY l_ref_layer_user
    ADD CONSTRAINT fk_l_coucheref_utilisateur_id_utilisateur FOREIGN KEY (user_id) REFERENCES "user"(user_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 28888 (class 2606 OID 177343825)
-- Name: l_group_user fk_l_group_user_user_id; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY l_group_user
    ADD CONSTRAINT fk_l_group_user_user_id FOREIGN KEY (user_id) REFERENCES "user"(user_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 28870 (class 2606 OID 181750228)
-- Name: l_ref_layer_user fk_l_l_ref_layer_user_layer; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY l_ref_layer_user
    ADD CONSTRAINT fk_l_l_ref_layer_user_layer FOREIGN KEY (layer) REFERENCES other_layer(layer_alias) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 28905 (class 2606 OID 177342232)
-- Name: l_sysma_object_sysma_relation fk_l_objetgeo_relation_id_modificateur; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY l_sysma_object_sysma_relation
    ADD CONSTRAINT fk_l_objetgeo_relation_id_modificateur FOREIGN KEY (modified_by) REFERENCES "user"(user_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 28904 (class 2606 OID 177342248)
-- Name: l_sysma_object_sysma_relation fk_l_objetgeo_relation_id_relation; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY l_sysma_object_sysma_relation
    ADD CONSTRAINT fk_l_objetgeo_relation_id_relation FOREIGN KEY (sysma_relation_id) REFERENCES sysma_relation(sysma_relation_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 28842 (class 2606 OID 177342221)
-- Name: l_sysma_object_sysma_action fk_l_objetgeo_travaux_id_modificateur; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY l_sysma_object_sysma_action
    ADD CONSTRAINT fk_l_objetgeo_travaux_id_modificateur FOREIGN KEY (modified_by) REFERENCES "user"(user_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 28909 (class 2606 OID 177342296)
-- Name: l_sysma_object_type_sysma_relation_type fk_l_objetgeo_travaux_id_modificateur; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY l_sysma_object_type_sysma_relation_type
    ADD CONSTRAINT fk_l_objetgeo_travaux_id_modificateur FOREIGN KEY (modified_by) REFERENCES "user"(user_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 28869 (class 2606 OID 177343815)
-- Name: l_other_layer_user fk_l_other_layer_user_user_id; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY l_other_layer_user
    ADD CONSTRAINT fk_l_other_layer_user_user_id FOREIGN KEY (user_id) REFERENCES "user"(user_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 28841 (class 2606 OID 177343830)
-- Name: l_sysma_object_sysma_action fk_l_sysma_object_sysma_action_sysma_action_id; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY l_sysma_object_sysma_action
    ADD CONSTRAINT fk_l_sysma_object_sysma_action_sysma_action_id FOREIGN KEY (sysma_action_id) REFERENCES sysma_action(sysma_action_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 28840 (class 2606 OID 177343835)
-- Name: l_sysma_object_sysma_action fk_l_sysma_object_sysma_action_sysma_object_id; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY l_sysma_object_sysma_action
    ADD CONSTRAINT fk_l_sysma_object_sysma_action_sysma_object_id FOREIGN KEY (sysma_object_id) REFERENCES sysma_object(sysma_object_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 28875 (class 2606 OID 177343386)
-- Name: l_sysma_object_type_user fk_l_type_objetgeo_utilisateur_id_type_objetgeo; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY l_sysma_object_type_user
    ADD CONSTRAINT fk_l_type_objetgeo_utilisateur_id_type_objetgeo FOREIGN KEY (sysma_object_type_id) REFERENCES sysma_object_type(sysma_object_type_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 28876 (class 2606 OID 177342310)
-- Name: l_sysma_object_type_user fk_l_type_objetgeo_utilisateur_id_utilisateur; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY l_sysma_object_type_user
    ADD CONSTRAINT fk_l_type_objetgeo_utilisateur_id_utilisateur FOREIGN KEY (user_id) REFERENCES "user"(user_id);


--
-- TOC entry 28878 (class 2606 OID 177341730)
-- Name: l_sysma_action_type_user fk_l_type_travaux_utilisateur_id_type_travaux; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY l_sysma_action_type_user
    ADD CONSTRAINT fk_l_type_travaux_utilisateur_id_type_travaux FOREIGN KEY (sysma_action_type_id) REFERENCES sysma_action_type(sysma_action_type_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 28877 (class 2606 OID 177342208)
-- Name: l_sysma_action_type_user fk_l_type_travaux_utilisateur_id_utilisateur; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY l_sysma_action_type_user
    ADD CONSTRAINT fk_l_type_travaux_utilisateur_id_utilisateur FOREIGN KEY (user_id) REFERENCES "user"(user_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 28846 (class 2606 OID 107178992)
-- Name: sysma_object fk_objetsgeo_id_modificateur; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_object
    ADD CONSTRAINT fk_objetsgeo_id_modificateur FOREIGN KEY (modified_by) REFERENCES "user"(user_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 28898 (class 2606 OID 177338574)
-- Name: sysma_relation fk_objetsgeo_id_modificateur; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_relation
    ADD CONSTRAINT fk_objetsgeo_id_modificateur FOREIGN KEY (modified_by) REFERENCES "user"(user_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 28879 (class 2606 OID 177342334)
-- Name: photo_object fk_objetsphotos_id_objetgeo; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY photo_object
    ADD CONSTRAINT fk_objetsphotos_id_objetgeo FOREIGN KEY (sysma_object_id) REFERENCES sysma_object(sysma_object_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 28882 (class 2606 OID 8537723)
-- Name: import_report fk_rapports_import_id_structure; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY import_report
    ADD CONSTRAINT fk_rapports_import_id_structure FOREIGN KEY (organisation_id) REFERENCES organisation(organisation_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 28881 (class 2606 OID 177341959)
-- Name: import_report fk_rapports_import_id_utilisateur; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY import_report
    ADD CONSTRAINT fk_rapports_import_id_utilisateur FOREIGN KEY (user_id) REFERENCES "user"(user_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 28883 (class 2606 OID 177343760)
-- Name: sysma_action_type_parameter fk_sysma_action_type_parameter_data_type; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_action_type_parameter
    ADD CONSTRAINT fk_sysma_action_type_parameter_data_type FOREIGN KEY (data_type) REFERENCES data_type_choice(data_type) MATCH FULL ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 28907 (class 2606 OID 177343401)
-- Name: l_sysma_object_type_sysma_relation_type fk_sysma_object_type_id_child; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY l_sysma_object_type_sysma_relation_type
    ADD CONSTRAINT fk_sysma_object_type_id_child FOREIGN KEY (sysma_object_type_id_child) REFERENCES sysma_object_type(sysma_object_type_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 28908 (class 2606 OID 177343396)
-- Name: l_sysma_object_type_sysma_relation_type fk_sysma_object_type_id_parent; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY l_sysma_object_type_sysma_relation_type
    ADD CONSTRAINT fk_sysma_object_type_id_parent FOREIGN KEY (sysma_object_type_id_parent) REFERENCES sysma_object_type(sysma_object_type_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 28858 (class 2606 OID 177343767)
-- Name: sysma_object_type_parameter fk_sysma_object_type_parameter_data_type; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_object_type_parameter
    ADD CONSTRAINT fk_sysma_object_type_parameter_data_type FOREIGN KEY (data_type) REFERENCES data_type_choice(data_type) MATCH FULL ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 28894 (class 2606 OID 177338532)
-- Name: sysma_relation_type_parameter fk_sysma_relation_type_parameter_data_type; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_relation_type_parameter
    ADD CONSTRAINT fk_sysma_relation_type_parameter_data_type FOREIGN KEY (data_type) REFERENCES data_type_choice(data_type) MATCH FULL ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 28847 (class 2606 OID 177342362)
-- Name: sysma_action fk_travaux_id_createur; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_action
    ADD CONSTRAINT fk_travaux_id_createur FOREIGN KEY (created_by) REFERENCES "user"(user_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 28850 (class 2606 OID 107178930)
-- Name: sysma_action fk_travaux_id_modificateur; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_action
    ADD CONSTRAINT fk_travaux_id_modificateur FOREIGN KEY (modified_by) REFERENCES "user"(user_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 28848 (class 2606 OID 177342348)
-- Name: sysma_action fk_travaux_id_structure; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_action
    ADD CONSTRAINT fk_travaux_id_structure FOREIGN KEY (organisation_id) REFERENCES organisation(organisation_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 28849 (class 2606 OID 177341590)
-- Name: sysma_action fk_travaux_id_type_travaux; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_action
    ADD CONSTRAINT fk_travaux_id_type_travaux FOREIGN KEY (sysma_action_type_id) REFERENCES sysma_action_type(sysma_action_type_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 28851 (class 2606 OID 177343408)
-- Name: sysma_object_type fk_types_objetgeo_id_modificateur; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_object_type
    ADD CONSTRAINT fk_types_objetgeo_id_modificateur FOREIGN KEY (modified_by) REFERENCES "user"(user_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 28859 (class 2606 OID 177343424)
-- Name: sysma_object_type_parameter fk_types_objetgeo_parametres_id_modificateur; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_object_type_parameter
    ADD CONSTRAINT fk_types_objetgeo_parametres_id_modificateur FOREIGN KEY (modified_by) REFERENCES "user"(user_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 28860 (class 2606 OID 177343391)
-- Name: sysma_object_type_parameter fk_types_objetgeo_parametres_id_type_objetgeo; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_object_type_parameter
    ADD CONSTRAINT fk_types_objetgeo_parametres_id_type_objetgeo FOREIGN KEY (sysma_object_type_id) REFERENCES sysma_object_type(sysma_object_type_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 28891 (class 2606 OID 177343588)
-- Name: sysma_relation_type fk_types_relation_id_modificateur; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_relation_type
    ADD CONSTRAINT fk_types_relation_id_modificateur FOREIGN KEY (modified_by) REFERENCES "user"(user_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 28892 (class 2606 OID 177343603)
-- Name: sysma_relation_type_parameter fk_types_relation_parametres_id_modificateur; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_relation_type_parameter
    ADD CONSTRAINT fk_types_relation_parametres_id_modificateur FOREIGN KEY (modified_by) REFERENCES "user"(user_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 28893 (class 2606 OID 177343581)
-- Name: sysma_relation_type_parameter fk_types_relation_parametres_id_type_relation; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_relation_type_parameter
    ADD CONSTRAINT fk_types_relation_parametres_id_type_relation FOREIGN KEY (sysma_relation_type_id) REFERENCES sysma_relation_type(sysma_relation_type_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 28852 (class 2606 OID 177342469)
-- Name: sysma_action_type fk_types_travaux_id_modificateur; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_action_type
    ADD CONSTRAINT fk_types_travaux_id_modificateur FOREIGN KEY (modified_by) REFERENCES "user"(user_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 28884 (class 2606 OID 177342491)
-- Name: sysma_action_type_parameter fk_types_travaux_parametres_id_modificateur; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_action_type_parameter
    ADD CONSTRAINT fk_types_travaux_parametres_id_modificateur FOREIGN KEY (modified_by) REFERENCES "user"(user_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 28885 (class 2606 OID 177341850)
-- Name: sysma_action_type_parameter fk_types_travaux_parametres_id_type_travaux; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY sysma_action_type_parameter
    ADD CONSTRAINT fk_types_travaux_parametres_id_type_travaux FOREIGN KEY (sysma_action_type_id) REFERENCES sysma_action_type(sysma_action_type_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 28853 (class 2606 OID 177343810)
-- Name: user fk_user_organisation_id; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT fk_user_organisation_id FOREIGN KEY (organisation_id) REFERENCES organisation(organisation_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 28887 (class 2606 OID 177343732)
-- Name: user_privilege id_util_droits_fkey; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY user_privilege
    ADD CONSTRAINT id_util_droits_fkey FOREIGN KEY (user_id) REFERENCES "user"(user_id);


--
-- TOC entry 28868 (class 2606 OID 8537768)
-- Name: l_theme_analysis_user l_at_util_id_at_fkey; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY l_theme_analysis_user
    ADD CONSTRAINT l_at_util_id_at_fkey FOREIGN KEY (theme_analysis_id) REFERENCES theme_analysis(theme_analysis_id);


--
-- TOC entry 28867 (class 2606 OID 177342323)
-- Name: l_theme_analysis_user l_at_util_util_id_util_fkey; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY l_theme_analysis_user
    ADD CONSTRAINT l_at_util_util_id_util_fkey FOREIGN KEY (user_id) REFERENCES "user"(user_id);


--
-- TOC entry 28873 (class 2606 OID 8537778)
-- Name: l_filter_user l_filtre_utilisateur_id_filtre_fkey; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY l_filter_user
    ADD CONSTRAINT l_filtre_utilisateur_id_filtre_fkey FOREIGN KEY (filter_id) REFERENCES filter(filter_id);


--
-- TOC entry 28872 (class 2606 OID 177342117)
-- Name: l_filter_user l_filtre_utilisateur_id_utilisateur_fkey; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY l_filter_user
    ADD CONSTRAINT l_filtre_utilisateur_id_utilisateur_fkey FOREIGN KEY (user_id) REFERENCES "user"(user_id);


--
-- TOC entry 28889 (class 2606 OID 177343805)
-- Name: l_group_user l_group_user_group_name_fkey; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY l_group_user
    ADD CONSTRAINT l_group_user_group_name_fkey FOREIGN KEY (group_name) REFERENCES layer_group(group_name) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 28874 (class 2606 OID 177342191)
-- Name: l_style_user l_style_utilisateur_id_utilisateur_fkey; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY l_style_user
    ADD CONSTRAINT l_style_utilisateur_id_utilisateur_fkey FOREIGN KEY (user_id) REFERENCES "user"(user_id);


--
-- TOC entry 28906 (class 2606 OID 177341795)
-- Name: l_sysma_object_sysma_relation l_sysma_object_sysma_relation_fk; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY l_sysma_object_sysma_relation
    ADD CONSTRAINT l_sysma_object_sysma_relation_fk FOREIGN KEY (sysma_object_id) REFERENCES sysma_object(sysma_object_id);


--
-- TOC entry 28890 (class 2606 OID 96740685)
-- Name: user_views user_views_fk; Type: FK CONSTRAINT; Schema: sysma; Owner: -
--

ALTER TABLE ONLY user_views
    ADD CONSTRAINT user_views_fk FOREIGN KEY (user_id) REFERENCES "user"(user_id);


-- Completed on 2021-07-23 14:58:11

--
-- PostgreSQL database dump complete
--



-- puis pose contrainte :

ALTER TABLE sysma.organisation
  ADD CONSTRAINT organisation_organisation_id_sup_0
  CHECK (
	organisation_id > 0
	
);