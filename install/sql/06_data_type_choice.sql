--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.17
-- Dumped by pg_dump version 13.2

-- Started on 2021-12-09 15:10:32

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
/*
SELECT pg_catalog.set_config('search_path', '', false);
*/
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- TOC entry 671 (class 1259 OID 119649674)
-- Name: data_type_choice; Type: TABLE; Schema: sysma; Owner: eptbsn_dba_antoine
--
/*
CREATE TABLE sysma.data_type_choice (
    data_type_id integer DEFAULT nextval('sysma.data_type_choice_data_type_id_seq'::regclass) NOT NULL,
    data_type character varying(100),
    description text,
    track_historytables_id bigint DEFAULT nextval('sysma.increment_x99_t_track_historytables'::regclass) NOT NULL
);


ALTER TABLE sysma.data_type_choice OWNER TO eptbsn_dba_antoine;
*/
--
-- TOC entry 5776 (class 0 OID 0)
-- Dependencies: 671
-- Name: TABLE data_type_choice; Type: COMMENT; Schema: sysma; Owner: eptbsn_dba_antoine
--
/*
COMMENT ON TABLE sysma.data_type_choice IS '{
   "display_groupe_order":300,
   "groupe_name":"Gestion du dictionnaire de données",
   "table_comment":"Liste des choix de types de données disponibles"
}';


--
-- TOC entry 5777 (class 0 OID 0)
-- Dependencies: 671
-- Name: COLUMN data_type_choice.data_type; Type: COMMENT; Schema: sysma; Owner: eptbsn_dba_antoine
--

COMMENT ON COLUMN sysma.data_type_choice.data_type IS 'intitulé du type';


--
-- TOC entry 5778 (class 0 OID 0)
-- Dependencies: 671
-- Name: COLUMN data_type_choice.description; Type: COMMENT; Schema: sysma; Owner: eptbsn_dba_antoine
--

COMMENT ON COLUMN sysma.data_type_choice.description IS 'description du type';


--
-- TOC entry 5779 (class 0 OID 0)
-- Dependencies: 671
-- Name: COLUMN data_type_choice.track_historytables_id; Type: COMMENT; Schema: sysma; Owner: eptbsn_dba_antoine
--

COMMENT ON COLUMN sysma.data_type_choice.track_historytables_id IS 'identifiant unique de ligne utilisé pour le suivi historique des enregistrement dans la table x99_track_historiquetables';


--
-- TOC entry 5770 (class 0 OID 119649674)
-- Dependencies: 671
-- Data for Name: data_type_choice; Type: TABLE DATA; Schema: sysma; Owner: eptbsn_dba_antoine
--
*/
COPY sysma.data_type_choice (data_type_id, data_type, description, track_historytables_id) FROM stdin;
10	photo	Photo	743
4	floatType	numérique avec décimale	734
5	booleanType	Vrai ou faux	735
6	monthYearDateType	Date mois/année	736
7	dayMonthYearDateType	Date jour/mois/année	737
1	textType	texte	738
2	textChoiceType	liste de choix	739
9	longTextType	texte sur plusieurs lignes	742
11	sysmaObjectLinkType	Lien avec objet Sysma	192904
3	integerType	numérique entier	740
8	multipleTextChoiceType	Liste de choix multiples	741
\.


--
-- TOC entry 5549 (class 2606 OID 119649985)
-- Name: data_type_choice data_type_choice_uniq; Type: CONSTRAINT; Schema: sysma; Owner: eptbsn_dba_antoine
--
/*
ALTER TABLE ONLY sysma.data_type_choice
    ADD CONSTRAINT data_type_choice_uniq UNIQUE (data_type);
*/

--
-- TOC entry 5551 (class 2606 OID 119649917)
-- Name: data_type_choice data_type_id_pkey; Type: CONSTRAINT; Schema: sysma; Owner: eptbsn_dba_antoine
--
/*
ALTER TABLE ONLY sysma.data_type_choice
    ADD CONSTRAINT data_type_id_pkey PRIMARY KEY (data_type_id);
*/

--
-- TOC entry 5780 (class 0 OID 0)
-- Dependencies: 671
-- Name: TABLE data_type_choice; Type: ACL; Schema: sysma; Owner: eptbsn_dba_antoine
--

/*
GRANT ALL ON TABLE sysma.data_type_choice TO grp_db_ext_mut_98_demo_dev_sysma_rw;
GRANT ALL ON TABLE sysma.data_type_choice TO grp_db_ext_mut_98_demo_dev_sysma_admin;
*/

-- Completed on 2021-12-09 15:10:34

--
-- PostgreSQL database dump complete
--

