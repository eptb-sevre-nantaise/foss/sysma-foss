
INSERT INTO sysma.sysma_conf ( sysma_conf_id, parameter, alias, data_type, value, display_order, value_choices)
 VALUES 
 (1,'Nom de la structure','ORGANISATION','text',:org_name,0, NULL)
,(2,'Sigle de la structure','ORGANISATION_ACRONYM','text',:org_acron,0, NULL)
,(3,'Site web de la structure', 'ORGANISATION_WEBSITE', 'text', :org_website, 0, NULL)
;

/*
1	Nom de la structure	ORGANISATION	text	orgname	0	\N
2	Sigle de la structure	ORGANISATION_ACRONYM	text	:org_acron	0	\N

*/
COPY sysma.sysma_conf (sysma_conf_id, parameter, alias, data_type, value, display_order, value_choices) FROM stdin;
4	Zoom par défaut	ZOOMDEFAULT	int	8	0	\N
6	id user anonyme	ANONYMID	int	0	0	\N
7	Lien vers la page d'aide	HELPPAGE_URL	text	https://sysma.io/doc	0	\N
8	Couche de base	BASELAYER	text	OpenStreetMap	0	\N
9	id structure anonyme	ANONYMSTRUCTUREID	int	0	0	\N
11	Fonction à utiliser pour récupérer les propriétairs du cadastre	CADPGFUNCTION	text	f_cad_rtn_info_parc_et_proprio_from_geom_plugcad_v_1_8_1	0	\N
12	Fonction à utiliser pour réaliser un tableau de publipostage des propriétairs du cadastre	CADPGFUNCTIONPUBLIPOST	text	f_cad_rtn_pubpostage_proprio_from_geom_plugcad_v_1_8_1	0	\N
15	Taux de TVA	TVA	numeric	1.2	0	\N
16	Schema contenant les données d'Atlas	ATLAS_SCHEMA	text	sysma_atlas	0	\N
17	Table des communes 	REF_PG_SCHEMA_TABLE_CITIES	text	my_shema.my_table	0	\N
18	Champ identifiant unique de la table des communes	REF_PG_SCHEMA_TABLE_CITIES_GID	text	code_insee	0	\N
19	Champ libellé de la table des communes	REF_PG_SCHEMA_TABLE_CITIES_NAME	text	nom	0	\N
20	Champ geométrique de la table des communes	REF_PG_SCHEMA_TABLE_CITIES_GEOM	text	geom	0	\N
21	Table des bassins versants	REF_PG_SCHEMA_TABLE_WATERSHED	text	my_shema.my_table	0	\N
22	Champ identifiant unique de la table des bassins versants	REF_PG_SCHEMA_TABLE_WATERSHED_GID	text	cdzonehydro	0	\N
23	Champ libellé de la table des bassins versants	REF_PG_SCHEMA_TABLE_WATERSHED_NAME	text	lbzonehydro	0	\N
24	Champ geométrique de la table des bassins versants	REF_PG_SCHEMA_TABLE_WATERSHED_GEOM	text	geom	0	\N
25	Table des interco	REF_PG_SCHEMA_TABLE_INTERCOMMUNAL	text	my_shema.my_table	0	\N
26	Champ identifiant unique de la table des interco	REF_PG_SCHEMA_TABLE_INTERCOMMUNAL_GID	text	ogc_fid	0	\N
27	Champ libellé de la table des interco	REF_PG_SCHEMA_TABLE_INTERCOMMUNAL_NAME	text	raison_soc	0	\N
28	Champ geométrique de la table des interco	REF_PG_SCHEMA_TABLE_INTERCOMMUNAL_GEOM	text	wkb_geometry	0	\N
29	Champ identifiant unique de la table des masses d'eau	REF_PG_SCHEMA_TABLE_MASSEEAU	text	my_shema.my_table	0	\N
30	Champ identifiant unique de la table  des masses d'eau	REF_PG_SCHEMA_TABLE_MASSEEAU_GID	text	cdeumassed	0	\N
31	Champ libellé de la table des masses d'eau	REF_PG_SCHEMA_TABLE_MASSEEAU_NAME	text	nommassede	0	\N
32	Champ geométrique de la table  des masses d'eau	REF_PG_SCHEMA_TABLE_MASSEEAU_GEOM	text	geom	0	\N
33	Table des secteurs (définition libre)	REF_PG_SCHEMA_TABLE_SECTEUR	text	my_shema.my_table	0	\N
34	Champ identifiant unique de la table des secteurs (définition libre)	REF_PG_SCHEMA_TABLE_SECTEUR_GID	text	cdeumassed	0	\N
35	Champ libellé de la table des secteurs (définition libre)	REF_PG_SCHEMA_TABLE_SECTEUR_NAME	text	nommassede	0	\N
36	Champ geométrique de la table des secteurs (définition libre)	REF_PG_SCHEMA_TABLE_SECTEUR_GEOM	text	geom	0	\N
38	Schema par défaut pour définition des couches à afficher 	SCHEMAOBSERVATOIRE	text	my_shema	0	\N
5	Centre de la carte par défault	CENTERDEFAULT	text	LatLng(47.09,-1.28)	0	\N
39	Nature des coûts suivis dans Sysma (HT ou TTC)	HT_TTC	text	HT	0	{"HT":"HT", "TTC":"TTC"}
37	Clé de l'API IGN (dépendant du nom de domaine)	IGN_API_KEY	text	\N	0	\N
10	Activation de la recherche cadastrale	CADASTRE_MODE	text	off	0	{"on":"Activé", "off":"Desactivé"}
14	Gestion des couts des travaux	COST_MANAGEMENT	text	activated_by_default	0	{"activated_by_default":"Activé par defaut","activated_by_user":"Activé par l'utilisateur", "desactivated":"Desactivé"}
13	Schéma contenant les données cadastrales	CADPGSCHEMA	text	cadastre_actuel_mutualise	0	\N
\.


