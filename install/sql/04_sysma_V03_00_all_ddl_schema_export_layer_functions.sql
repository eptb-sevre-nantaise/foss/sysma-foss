CREATE SCHEMA IF NOT EXISTS sysma_export_layer;
CREATE OR REPLACE FUNCTION sysma_export_layer.get_params_dictionary_info_from_sysma_object_type_id(sysma_object_type_id bigint)
 RETURNS TABLE(parameter_id bigint, parameter character varying, parameter_alias character varying, data_type character varying, unit character varying, description text, required integer)
 LANGUAGE sql
 STABLE
AS $function$
/*
Retourne les parametres d'un type d'objet geo
Exemple d'appel :
select * from sysma_export_layer.get_params_dictionary_info_from_sysma_object_type_id(1)
*/
 SELECT parameter_id::bigint
 , xx_99_utils.f_html_no_html_specials_chars_and_balises(parameter) as parameter
 , parameter_alias
  ,data_type
 , unit
 , COALESCE(replace(xx_99_utils.f_html_no_html_specials_chars_and_balises(description),'''',''''''), 'Non renseigné')::text  description 
 , required::int
  FROM sysma.sysma_object_type_parameter 
   where sysma_object_type_id = $1
      AND parameter_alias IS not NULL
      AND parameter_alias <>''
 ORDER BY  parameter_id
$function$
 ;
CREATE OR REPLACE FUNCTION sysma_export_layer.get_params_dictionary_info_from_sysma_action_type_id(_sysma_action_type_id bigint)
 RETURNS TABLE(parameter_id bigint, parameter character varying, parameter_alias character varying, data_type character varying, unit character varying, description text, required integer)
 LANGUAGE sql
 STABLE
AS $function$

-- select *  from sysma_export_layer.get_params_dictionary_info_from_sysma_action_type_id(1)
 SELECT parameter_id::bigint
 , parameter
 , parameter_alias
 , data_type
 , unit
 ,  COALESCE(replace(xx_99_utils.f_html_no_html_specials_chars_and_balises(description),'''',''''''), 'Non renseigné')::text  description 
 ,  required::int
  FROM sysma.sysma_action_type_parameter 
   where sysma_action_type_id = $1
      AND parameter_alias IS not NULL
      AND parameter_alias <>''
 ORDER BY parameter_id
$function$
 ;
CREATE OR REPLACE FUNCTION sysma_export_layer.get_pg_data_type_from_sysma_data_type(data_type character varying)
 RETURNS character varying
 LANGUAGE sql
 STABLE STRICT
AS $function$


    
    SELECT 
    CASE 
       WHEN $1 ilike 'integerType' THEN  'INT'
       WHEN $1 ilike 'floatType' THEN  'NUMERIC'
       WHEN $1 ilike 'booleanType' THEN  'BOOLEAN'

    --   WHEN $1 ilike 'dayMonthYearDateType' THEN  'DATE'
    --WHEN $1 ilike 'geoObjectTypeLink' THEN  'BIGINT'

    
    ELSE 'TEXT'
    END as type_pg
  
   
  
$function$
 ;
CREATE OR REPLACE FUNCTION sysma_export_layer.get_pg_geom_type_from_sysma_geom_type(type_geometrie character varying)
 RETURNS character varying
 LANGUAGE sql
 STABLE STRICT
AS $function$    
       SELECT 
       CASE 
       WHEN $1 ilike 'ST_Point' THEN  'Point'
       WHEN $1 ilike 'ST_Linestring' THEN  'Linestring'
       WHEN $1 ilike 'ST_Polygon' THEN  'Polygon'
       --WHEN $1 ilike 'flottant' THEN  'NUMERIC'
       ELSE 'PAS DE CORRESPONDANCE DE GEOMETRIE'
       END as geom_pg 
   $function$
 ;
CREATE OR REPLACE FUNCTION sysma_export_layer.get_pg_default_value_from_sysma_data_type(data_type character varying)
 RETURNS character varying
 LANGUAGE sql
 STABLE STRICT
AS $function$
    SELECT 
    CASE 

      WHEN $1 ilike 'integerType' THEN  'NULL'
      WHEN $1 ilike 'floatType' THEN  'NULL'
      WHEN $1 ilike 'booleanType' THEN  'NULL'

    ELSE ''
    END as type_pg
  
$function$
 ;
CREATE OR REPLACE FUNCTION sysma_export_layer.create_table_from_sysma_object_type_id(_sysma_object_type_id bigint, _objet_actuel boolean DEFAULT false, _url_base text DEFAULT 'https://sysma.sevre-nantaise.com'::text)
 RETURNS boolean
 LANGUAGE plpgsql
AS $function$


/*
Note de version :
- VO4 : adapt to new model schema and code factory
- V04 : DELETE geometry_old and en_projet
- V03 : add id_modificateur and date_modification_objet
- V02 : add base url link in args


constantes : sysma_export_layer , sysma.sysma_object_type , sysma.objetsgeo, temp_sysma
exemple d'appel:
SELECT  sysma_export_layer.f_sysma_create_table_from_sysma_object_type_id(1::int);
SELECT  sysma_export_layer.f_sysma_create_table_from_sysma_object_type_id(100833::int);


SELECT  *, sysma_export_layer.f_sysma_create_table_from_sysma_object_type_id(sysma_object_type_id)
from   sysma.sysma_object_type
WHERE 
alias_groupe is not null AND sysma_object_type_alias IS not null
order by sysma_object_type_id;

*/
DECLARE

--sysma_object_type_id int := sysma_export_layer.f_sysma_rt_sysma_object_type_id_from_sysma_object_id(sysma_object_id);
t_text text;
t_text_c text;
_sysma_object_type_alias TEXT;
_description_objetgeo TEXT;
_geometry_type TEXT ;


schema_export_tout name:='sysma_export_layer' ; -- attention revoir les noms ??? (fait en urgence pour les besoins du CT 2015 -2019)
schema_export_actu name:='sysma_export_layer' ; -- attention revoir les noms ??? (fait en urgence pour les besoins du CT 2015 -2019)

schema_export name;

BEGIN
-- raise notice '_sysma_object_type_id : %', _sysma_object_type_id; -- debug

IF _objet_actuel is true 
         then schema_export:=schema_export_tout ;
      ELSE schema_export:=schema_export_tout;
END IF;

DROP TABLE IF EXISTS temp_sysma ;
-- RAISE NOTICE '_sysma_object_type_id :  %', $1;  -- debug

_sysma_object_type_alias := sysma_object_type_alias from   sysma.sysma_object_type  WHERE sysma_object_type_id = _sysma_object_type_id AND sysma_object_type_alias IS not null;


IF _sysma_object_type_alias is not null AND _sysma_object_type_alias <>'' THEN
_description_objetgeo := description from   sysma.sysma_object_type  WHERE sysma_object_type_id = _sysma_object_type_id ;
_geometry_type := geometry_type from   sysma.sysma_object_type  WHERE sysma_object_type_id = _sysma_object_type_id ;
      t_text := 
                $$ DROP TABLE IF EXISTS $$ ||schema_export || $$.$$ ||  _sysma_object_type_alias ||$$;$$ ||  --alias_groupe ||'_' ||
                $$ CREATE TABLE $$  ||schema_export || $$ .$$  ||  _sysma_object_type_alias ||  --alias_groupe ||'_' ||
                     '  AS SELECT                       
                          o.sysma_object_id
                        , o.start_date
                        , o.end_date
                        , o.organisation_id
                        , xx_99_utils.f_html_no_html_specials_chars_and_balises(obj_organisation.organisation) ::character varying(250) organisation_obj
                        , o.created_by
                        , o.status
                        , o.sysma_object_type_id
                        , o.created_at 
                        , replace(xx_99_utils.f_html_no_html_specials_chars_and_balises(o.sysma_object),'''''''','''''''''''') AS sysma_object  
                        , o.modified_by AS modified_by
                        , o.modified_at AS modified_at
                        , o.geom ';
               if  sysma_export_layer.create_temptable_object_anayelement(_sysma_object_type_id ) then --  if  exists parameter whith valid alias ( L'objet a des parametres avec des alias SIG)
                     t_text := t_text  ||'
                        ,  p.* ' ;
               END IF;

               t_text := t_text  ||
                       ' FROM  sysma.sysma_object o ';

               if  sysma_export_layer.create_temptable_object_anayelement(_sysma_object_type_id ) then --  if  exists parameter whith valid alias ( L'objet a des parametres avec des alias SIG)
                     t_text := t_text  ||'
                        LEFT JOIN LATERAL sysma_export_layer.get_params_values_from_sysma_object_id(o.sysma_object_id, null::temp_sysma) p on true' ;
               END IF;

               t_text := t_text  ||'                           
                         LEFT JOIN sysma.organisation obj_organisation on (o.organisation_id=obj_organisation.organisation_id)
                         WHERE o.sysma_object_type_id = ' ||_sysma_object_type_id || 
                         
                           ' AND  (CASE WHEN ' || _objet_actuel || ' IS TRUE
                            
                           THEN   (o.end_date = ''0'' OR o.end_date ='''')  
                           ELSE 1=1
                           END )'
                         
                         || ' AND o.geom is not null ; ' ||
                  ' COMMENT ON TABLE ' ||schema_export || '.' ||  _sysma_object_type_alias || ' IS '' Thème : '  ||  _sysma_object_type_alias ||  ' - 
                  description_objetgeo : ' || coalesce(replace(xx_99_utils.f_html_no_html_specials_chars_and_balises( _description_objetgeo),'''',''''''), 'Non renseigné')::text || ' 
                   MAJ :' || now()|| ' user : ' || current_user || ''';' ||
                '  ALTER TABLE ' ||schema_export || '.' ||  _sysma_object_type_alias || ' ADD CONSTRAINT ' ||  _sysma_object_type_alias || '_pkey PRIMARY KEY(sysma_object_id);
                  
                     ALTER TABLE ' ||schema_export || '.' ||  _sysma_object_type_alias || ' 
                     ALTER COLUMN geom TYPE geometry(' || sysma_export_layer.get_pg_geom_type_from_sysma_geom_type (_geometry_type) || ', 2154) ; 
                     
                     ALTER TABLE ' ||schema_export || '.' ||  _sysma_object_type_alias || ' ADD column sysma_link character varying(510) ;
                     COMMENT ON COLUMN ' ||schema_export || '.' ||  _sysma_object_type_alias || '.sysma_link  IS '' Lien vers sysma web'' ;


                     
                     COMMENT ON COLUMN ' ||schema_export || '.' ||  _sysma_object_type_alias || '.organisation_obj  IS ''intitulé de la structure maître d''''ouvrage de la création de l''''objet dans le cas où il s''''agit d''''objets installés lors de travaux'' ;

                     UPDATE ' ||schema_export || '.' ||  _sysma_object_type_alias || ' set sysma_link = (select '''|| _url_base || '/carte/objet/''|| sysma_object_id) ; 

                     ALTER TABLE ' ||schema_export || '.' ||  _sysma_object_type_alias || ' ADD column phototheque_json jsonb ;
                     COMMENT ON COLUMN ' ||schema_export || '.' ||  _sysma_object_type_alias || '.phototheque_json  IS '' Json contenant les identifiants de la photothèque'' ;
                     UPDATE ' ||schema_export || '.' ||  _sysma_object_type_alias || ' set phototheque_json = (select sysma_export_layer.get_json_photos_from_sysma_object_id(sysma_object_id))::jsonb ; 
                     
    
                 CREATE INDEX sx_' ||  _sysma_object_type_alias || '_geom  ON ' ||schema_export || '.' ||  _sysma_object_type_alias || '   USING gist  (geom) ; '

        
      ;
      -- Ajout des commentaire de champs à partir des donnnées de paramètres
      t_text := t_text || coalesce(string_agg( 
      'COMMENT ON COLUMN ' ||schema_export || '.' ||  o.sysma_object_type_alias || '.' || p.parameter_alias || 
      '   IS ''' || coalesce(p.description, 'Non renseigné') || '''; '
      , ';'), ' -- Pas de description de champs')
         from   sysma.sysma_object_type o LEFT JOIN  LATERAL sysma_export_layer.get_params_dictionary_info_from_sysma_object_type_id(_sysma_object_type_id) p on true
         WHERE o.sysma_object_type_id = _sysma_object_type_id ;
         
  

      EXECUTE format(
         $sql$
            %s
         $sql$
       , t_text)
      ;
      -- return true;
-- Ajout des commentaire de champs à partir des tables objet geo
t_text_c := '';
t_text_c := t_text_c || 
'COMMENT ON COLUMN ' ||schema_export || '.' ||  o.sysma_object_type_alias || '.sysma_object_id'
'   IS '''  || coalesce (replace(xx_99_utils.f_html_no_html_specials_chars_and_balises( 
       xx_99_utils.f_rtn_column_comment('sysma', 'sysma_object', 'sysma_object_id'))
      ,'''',''''''), 'Non renseigné')::text
      || '''; '
      from   sysma.sysma_object_type o
   WHERE o.sysma_object_type_id = _sysma_object_type_id 
   --AND o.alias_groupe is not null 
   AND o.sysma_object_type_alias IS not null
;
      t_text_c := t_text_c || 
'COMMENT ON COLUMN ' ||schema_export || '.' ||  o.sysma_object_type_alias || '.start_date'
'   IS '''  || coalesce (replace(xx_99_utils.f_html_no_html_specials_chars_and_balises( 
       xx_99_utils.f_rtn_column_comment('sysma', 'sysma_object', 'start_date'))
      ,'''',''''''), 'Non renseigné')::text
      || '''; '
      from   sysma.sysma_object_type o
   WHERE o.sysma_object_type_id = _sysma_object_type_id 
   --AND o.alias_groupe is not null 
   AND o.sysma_object_type_alias IS not null
;
      t_text_c := t_text_c || 
'COMMENT ON COLUMN ' ||schema_export || '.' ||  o.sysma_object_type_alias || '.end_date'
'   IS '''  || coalesce (replace(xx_99_utils.f_html_no_html_specials_chars_and_balises( 
       xx_99_utils.f_rtn_column_comment('sysma', 'sysma_object', 'end_date'))
      ,'''',''''''), 'Non renseigné')::text
      || '''; '
      from   sysma.sysma_object_type o
   WHERE o.sysma_object_type_id = _sysma_object_type_id 
   --AND o.alias_groupe is not null 
   AND o.sysma_object_type_alias IS not null
   ;
               t_text_c := t_text_c || 
'COMMENT ON COLUMN ' ||schema_export || '.' ||  o.sysma_object_type_alias || '.organisation_id'
'   IS '''  || coalesce (replace(xx_99_utils.f_html_no_html_specials_chars_and_balises( 
       xx_99_utils.f_rtn_column_comment('sysma', 'sysma_object', 'organisation_id'))
      ,'''',''''''), 'Non renseigné')::text
      || '''; '
      from   sysma.sysma_object_type o
   WHERE o.sysma_object_type_id = _sysma_object_type_id 
   --AND o.alias_groupe is not null 
   AND o.sysma_object_type_alias IS not null
;
      t_text_c := t_text_c || 
'COMMENT ON COLUMN ' ||schema_export || '.' ||  o.sysma_object_type_alias || '.created_by'
'   IS '''  || coalesce (replace(xx_99_utils.f_html_no_html_specials_chars_and_balises( 
       xx_99_utils.f_rtn_column_comment('sysma', 'sysma_object', 'created_by'))
      ,'''',''''''), 'Non renseigné')::text
      || '''; '
      from   sysma.sysma_object_type o
   WHERE o.sysma_object_type_id = _sysma_object_type_id 
   --AND o.alias_groupe is not null 
   AND o.sysma_object_type_alias IS not null
;
      t_text_c := t_text_c || 
'COMMENT ON COLUMN ' ||schema_export || '.' ||  o.sysma_object_type_alias || '.status'
'   IS '''  || coalesce (replace(xx_99_utils.f_html_no_html_specials_chars_and_balises( 
       xx_99_utils.f_rtn_column_comment('sysma', 'sysma_object', 'status'))
      ,'''',''''''), 'Non renseigné')::text
      || '''; '
      from   sysma.sysma_object_type o
   WHERE o.sysma_object_type_id = _sysma_object_type_id 
   --AND o.alias_groupe is not null 
   AND o.sysma_object_type_alias IS not null
;
      t_text_c := t_text_c || 
'COMMENT ON COLUMN ' ||schema_export || '.' ||  o.sysma_object_type_alias || '.sysma_object_type_id'
'   IS '''  || coalesce (replace(xx_99_utils.f_html_no_html_specials_chars_and_balises( 
       xx_99_utils.f_rtn_column_comment('sysma', 'sysma_object', 'sysma_object_type_id'))
      ,'''',''''''), 'Non renseigné')::text
      || '''; '
      from   sysma.sysma_object_type o
   WHERE o.sysma_object_type_id = _sysma_object_type_id 
   --AND o.alias_groupe is not null 
   AND o.sysma_object_type_alias IS not null
;
 
            t_text_c := t_text_c || 
'COMMENT ON COLUMN ' ||schema_export || '.' ||  o.sysma_object_type_alias || '.created_at'
'   IS '''  || coalesce (replace(xx_99_utils.f_html_no_html_specials_chars_and_balises( 
       xx_99_utils.f_rtn_column_comment('sysma', 'sysma_object', 'created_at'))
      ,'''',''''''), 'Non renseigné')::text
      || '''; '
      from   sysma.sysma_object_type o
   WHERE o.sysma_object_type_id = _sysma_object_type_id 
   --AND o.alias_groupe is not null 
   AND o.sysma_object_type_alias IS not null
;
            t_text_c := t_text_c || 
'COMMENT ON COLUMN ' ||schema_export || '.' ||  o.sysma_object_type_alias || '.sysma_object'
'   IS '''  || coalesce (replace(xx_99_utils.f_html_no_html_specials_chars_and_balises( 
       xx_99_utils.f_rtn_column_comment('sysma', 'sysma_object', 'sysma_object'))
      ,'''',''''''), 'Non renseigné')::text
      || '''; '
      from   sysma.sysma_object_type o
   WHERE o.sysma_object_type_id = _sysma_object_type_id 
   --AND o.alias_groupe is not null 
   AND o.sysma_object_type_alias IS not null
;


   t_text_c := t_text_c || 
'COMMENT ON COLUMN ' ||schema_export || '.' ||  o.sysma_object_type_alias || '.modified_by'
'   IS '''  || coalesce (replace(xx_99_utils.f_html_no_html_specials_chars_and_balises( 
       xx_99_utils.f_rtn_column_comment('sysma', 'sysma_object', 'modified_by'))
      ,'''',''''''), 'Non renseigné')::text
      || '''; '
      from   sysma.sysma_object_type o
   WHERE o.sysma_object_type_id = _sysma_object_type_id 
   --AND o.alias_groupe is not null 
   AND o.sysma_object_type_alias IS not null
;

   t_text_c := t_text_c || 
'COMMENT ON COLUMN ' ||schema_export || '.' ||  o.sysma_object_type_alias || '.modified_at'
'   IS '''  || coalesce (replace(xx_99_utils.f_html_no_html_specials_chars_and_balises( 
       xx_99_utils.f_rtn_column_comment('sysma', 'sysma_object', 'modified_at'))
      ,'''',''''''), 'Non renseigné')::text
      || '''; '
      from   sysma.sysma_object_type o
   WHERE o.sysma_object_type_id = _sysma_object_type_id 
   --AND o.alias_groupe is not null 
   AND o.sysma_object_type_alias IS not null
;



       t_text_c := t_text_c || 
'COMMENT ON COLUMN ' ||schema_export || '.' ||  o.sysma_object_type_alias || '.geom'
'   IS '''  || coalesce (replace(xx_99_utils.f_html_no_html_specials_chars_and_balises( 
       xx_99_utils.f_rtn_column_comment('sysma', 'sysma_object', 'geom'))
      ,'''',''''''), 'Non renseigné')::text
      || '''; '
      from   sysma.sysma_object_type o
   WHERE o.sysma_object_type_id = _sysma_object_type_id 
   --AND o.alias_groupe is not null 
   AND o.sysma_object_type_alias IS not null
;

--RAISE NOTICE 'SQL  c: %', t_text_c;


EXECUTE format(
   $sql$
   %s 
   
   $sql$
 , t_text_c)
 ;

return true;
ELSE 
return FALSE;
END IF; -- alias exists
END
$function$
 ;
CREATE OR REPLACE FUNCTION sysma_export_layer.create_temptable_object_anayelement(sysma_object_type_id bigint)
 RETURNS boolean
 LANGUAGE plpgsql
AS $function$
/*
Création d'une table temporaire qui permet de structurer les données
*/
DECLARE 
t_text text;

-- SELECT sysma_export_layer.create_temptable_object_anayelement(1)
BEGIN
  
   t_text := string_agg('NULL::' || sysma_export_layer.get_pg_data_type_from_sysma_data_type(data_type) || ' as ' || (parameter_alias),',')
      FROM   sysma_export_layer.get_params_dictionary_info_from_sysma_object_type_id($1);

--RAISE NOTICE 'SQL : %', t_text;
   
   IF t_text IS NOT NULL THEN   
   EXECUTE format(
      $sql$
      DROP TABLE IF EXISTS  temp_sysma;
      CREATE temporary table temp_sysma as select %s 
   
      $sql$
    , t_text)
   ;
   RETURN TRUE;
   ELSE 
   
   RETURN FALSE;
   END IF;


END;
$function$
 ;
CREATE OR REPLACE FUNCTION sysma_export_layer.get_params_values_from_sysma_object_id(_sysma_object_id bigint, _type_rt anyelement)
 RETURNS SETOF anyelement
 LANGUAGE plpgsql
 STABLE
AS $function$
/*
exemple d'appel:
SELECT sysma_export_layer.create_temptable_object_anayelement(1);
SELECT * FROM  sysma_export_layer.get_params_values_from_sysma_object_id(4, null::temp_sysma) 


Apres construction de la structure de la table pour creation du type anyelement , injection des données dans la table
*/
DECLARE

sysma_object_type_id BIGINT := sysma_export_layer.get_sysma_object_type_id_from_sysma_object_id(_sysma_object_id);
t_text text;

BEGIN
--raise notice '_sysma_object_id: %', sysma_object_id; -- debug
t_text := string_agg( 
   coalesce('''' || CASE WHEN sysma_export_layer.get_param_value_from_sysma_object_param_id($1, parameter_id)::text <>''
                        THEN sysma_export_layer.get_param_value_from_sysma_object_param_id($1, parameter_id) 
                        ELSE NULL
                        END|| '''', --sysma_export_layer.get_pg_data_type_from_sysma_data_type_default_val(data_type)
   ''||'NULL' || ''

   ) 
   || '::' || 
   sysma_export_layer.get_pg_data_type_from_sysma_data_type(data_type) || ' as ' || (parameter_alias) || '  /* sysma_object_id =' || _sysma_object_id || ' AND  parameter_id=' || parameter_id ||'*/' , ',')
from   sysma_export_layer.get_params_dictionary_info_from_sysma_object_type_id(sysma_object_type_id);


   --RAISE NOTICE 'SQL : %', t_text;  -- debug
   RETURN QUERY EXECUTE format(
      $sql$
      select %s 
   
      $sql$
    , t_text)
   --USING  _param::int
   ;
END
$function$
 ;
CREATE OR REPLACE FUNCTION sysma_export_layer.get_sysma_object_type_id_from_sysma_object_id(sysma_object_id bigint)
 RETURNS bigint
 LANGUAGE sql
AS $function$
 SELECT sysma_object_type_id::bigint FROM sysma.sysma_object
   where sysma_object_id = $1
$function$
 ;
CREATE OR REPLACE FUNCTION sysma_export_layer.get_param_value_from_sysma_object_param_id(_sysma_object_id bigint, _parameter_id bigint)
 RETURNS text
 LANGUAGE sql
 STABLE
AS $function$
/*
Exemple d'appel :

*/
SELECT
--CASE WHEN sysma.f_test_objetgeo_parametre_is_ld($2) IS TRUE THEN
 string_agg(replace(xx_99_utils.f_html_no_html_specials_chars_and_balises(value),'''','''''')::text,' [] ') 
/*
ELSE
 valeur::text 

END
*/
FROM sysma.sysma_object_data
   where sysma_object_id = $1
   AND parameter_id = $2
   AND (end_date = '0' OR end_date ='')


$function$
 ;
CREATE OR REPLACE FUNCTION sysma_export_layer.create_temptable_action_anayelement(_sysma_action_type_id bigint)
 RETURNS boolean
 LANGUAGE plpgsql
AS $function$
DECLARE 
t_text text;

-- SELECT sysma_export_layer.create_temptable_action_anayelement(1); select * from temp_sysma;
-- SELECT sysma_export_layer.create_temptable_action_anayelement(95)

BEGIN
  
   t_text := string_agg('NULL::' || sysma_export_layer.get_pg_data_type_from_sysma_data_type(data_type) || ' as ' || (parameter_alias),',')
      FROM   sysma_export_layer.get_params_dictionary_info_from_sysma_action_type_id($1);

   IF t_text IS NOT NULL THEN   
   EXECUTE format(
      $sql$
      drop table if exists temp_sysma;
      CREATE temporary table temp_sysma as select %s 
   
      $sql$
    , t_text)
   ;
   RETURN TRUE;
   ELSE 
   RETURN FALSE; -- no parameter for this action type
   END IF;


END;
$function$
 ;
CREATE OR REPLACE FUNCTION sysma_export_layer.get_param_value_from_sysma_action_id(_sysma_action_id bigint, _parameter_id bigint)
 RETURNS text
 LANGUAGE sql
 STABLE
AS $function$
SELECT

 string_agg(replace(xx_99_utils.f_html_no_html_specials_chars_and_balises(value),'''','''''')::text,' [] ') 

FROM sysma.sysma_action_data
   where sysma_action_id= $1
   AND parameter_id = $2
  -- AND date_fin = '0' OR date_fin =''


$function$
 ;
CREATE OR REPLACE FUNCTION sysma_export_layer.get_sysma_action_type_id_from_sysma_action_id(_sysma_action_id bigint)
 RETURNS bigint
 LANGUAGE sql
 STABLE
AS $function$
 SELECT sysma_action_type_id::bigint FROM sysma.sysma_action
   where sysma_action_id = $1
$function$
 ;
CREATE OR REPLACE FUNCTION sysma_export_layer.get_params_values_from_sysma_action_id(_sysma_action_id bigint, _type_rt anyelement)
 RETURNS SETOF anyelement
 LANGUAGE plpgsql
AS $function$
/*
exemple d'appel:
drop table if exists temp_sysma;
SELECT sysma_export_layer.create_temptable_action_anayelement(sysma_export_layer.get_sysma_action_type_id_from_sysma_action_id(12));
SELECT * FROM  sysma_export_layer.get_params_values_from_sysma_action_id(12, null::temp_sysma) 


drop table if exists temp_sysma;
SELECT sysma_export_layer.create_temptable_action_anayelement(sysma_export_layer.get_sysma_action_type_id_from_sysma_action_id(3));
SELECT * FROM  sysma_export_layer.get_params_values_from_sysma_action_id(3, null::temp_sysma) 

SELECT sysma_export_layer.create_temptable_action_anayelement(sysma_export_layer.get_sysma_action_type_id_from_sysma_action_id(24));
SELECT * FROM  sysma_export_layer.get_params_values_from_sysma_action_id(24, null::temp_sysma) 
*/
DECLARE

_sysma_action_type_id BIGINT := sysma_export_layer.get_sysma_action_type_id_from_sysma_action_id(_sysma_action_id);
t_text text;

BEGIN

t_text := string_agg( 

  coalesce('''' || CASE WHEN sysma_export_layer.get_param_value_from_sysma_action_id($1, parameter_id)::text <>''
                        THEN sysma_export_layer.get_param_value_from_sysma_action_id($1, parameter_id) 
                        ELSE NULL
                        END|| '''', 
   ''||'NULL' || ''

   ) 

   || '::' || 
   sysma_export_layer.get_pg_data_type_from_sysma_data_type(data_type) || ' as ' || (parameter_alias) || '  /* sysma_action_id =' || _sysma_action_id || ' AND  parameter_id=' || parameter_id ||'*/' , ','
   )
from   sysma_export_layer.get_params_dictionary_info_from_sysma_action_type_id(_sysma_action_type_id);

-- select * from sysma_export_layer.get_params_dictionary_info_from_sysma_action_type_id(1);
-- select * from sysma_export_layer.get_param_value_from_sysma_action_id(3,12);
-- select * from sysma_export_layer.get_param_value_from_sysma_action_id(3,147);
-- select * from sysma_export_layer.get_pg_data_type_from_sysma_data_type('texte')

   -- RAISE NOTICE 'SQL : %', t_text; -- debug
   RETURN QUERY EXECUTE format(
      $sql$
      select %s 
   
      $sql$
    , t_text)
   --USING  _param::int
   ;
END
$function$
 ;
CREATE OR REPLACE FUNCTION sysma_export_layer.get_json_photos_from_sysma_object_id(_sysma_object_id bigint)
 RETURNS json
 LANGUAGE sql
 STABLE
AS $function$


WITH list as (
SELECT 
photo_object_id, photo_id, photo_type, sysma_object_id, display_order, 
       before_after, track_historytables_id, photo_file, photo_title, 
       photo_date

       FROM sysma.photo_object where sysma_object_id =$1
       order by photo_type asc, display_order asc

       )

,list2 as (
  SELECT  (photo_type || '_array')::text as k, jsonb_agg(row_to_json(l))::jsonb  as v from list l
       group by photo_type

)
select json_object_agg(k, v)::json  from list2



$function$
 ;
CREATE OR REPLACE FUNCTION sysma_export_layer.create_table_from_sysma_action_type_id(_sysma_action_type_id bigint, _url_base text DEFAULT 'https://sysma.sevre-nantaise.com'::text, _debugmode boolean DEFAULT false)
 RETURNS boolean
 LANGUAGE plpgsql
AS $function$
/*
constantes : sysma_export_layer , sysma.sysma_action_type , sysma.sysma_action, temp_sysma
exemple d'appel:
SELECT  sysma_export_layer.create_table_from_sysma_action_type_id(1, 'https://sysma.sevre-nantaise.com', FALSE);
SELECT  sysma_export_layer.create_table_from_sysma_action_type_id(10266796, 'https://eptb-vilaine.sysma.io', FALSE);

SELECT  sysma_export_layer.create_table_from_sysma_action_type_id(95);
SELECT  sysma_export_layer.create_table_from_sysma_action_type_id(100838);

SELECT  sysma_export_layer.create_table_from_sysma_action_type_id(sysma_action_type_id), *
from   sysma.sysma_action_type
WHERE 
  sysma_action_type_alias IS not null
order by sysma_action_type_id;


*/
DECLARE

--sysma_object_type_id int := sysma_export_layer.create_table_from_sysma_action_type_id(id_objetgeo);
t_text text;
is_parameter_action_exists boolean default false;

--lst_c_travaux text;

BEGIN
--lst_c_travaux ='sysma_action_id, travaux, sysma_action_type_id, date_debut, date_fin, id_organisation,  id_createur, annee_prog, statut, date_creation_travaux, id_modificateur, date_modification_travaux'
DROP TABLE IF EXISTS temp_sysma ;
-- RAISE NOTICE 'id type travaux %', $1; -- debug
is_parameter_action_exists:= sysma_export_layer.create_temptable_action_anayelement(_sysma_action_type_id );
         
         t_text := 
                
               ' DROP TABLE IF EXISTS sysma_export_layer.' || sysma_object_type_alias || '_act_' || sysma_action_type_alias || ';' ||
               
 
                ' CREATE TABLE sysma_export_layer.' || sysma_object_type_alias || '_act_' || sysma_action_type_alias || 
                     ' AS SELECT ' ||
                       'row_number() over() as gid ' ||
                        

                       '
                       , xx_99_utils.f_html_no_html_specials_chars_and_balises(tstructure.organisation) ::character varying(250) organisation_act

                       , t.* 
                        
                       ' 
         || CASE WHEN is_parameter_action_exists = true then ', p.*' ELSE '' END ||
         /*if is_parameter_action_exists = true then
            t_text :=  t_text || ', p.*' ;
         END IF;
         */
          ' , null::text as sysma_link,  ' ||  c_geo.cols_objgeo || 
                       ', o.geom  ' ||
                       ' FROM  sysma.sysma_action t '
         || CASE WHEN is_parameter_action_exists = true then ' LEFT JOIN LATERAL sysma_export_layer.get_params_values_from_sysma_action_id(t.sysma_action_id, null::temp_sysma) p on true ' ELSE ''  END ||

         /*
         if is_parameter_action_exists = true then
            t_text :=  t_text || ' LEFT JOIN LATERAL sysma_export_layer.get_params_values_from_sysma_action_id(t.sysma_action_id, null::temp_sysma) p on true ' ;
         END IF;
         */
                           
         '  
          LEFT JOIN sysma.organisation tstructure on (t.organisation_id=tstructure.organisation_id)
         

         LEFT JOIN sysma.l_sysma_object_sysma_action l ON (l.sysma_action_id = t.sysma_action_id) ' ||
                           
                           '  JOIN sysma_export_layer.'  || sysma_object_type_alias || ' o ON (o.sysma_object_id = l.sysma_object_id)
                           
     
                         WHERE t.sysma_action_type_id = ' ||_sysma_action_type_id||' ; '
                        -- GROUP BY ||
                  --'ALTER TABLE sysma_export_layer.' || tto.alias_groupe ||'_' || sysma_object_type_alias || '_act_' || sysma_action_type_alias || ' ADD CONSTRAINT pk_' || tto.alias_groupe ||'_' || sysma_object_type_alias || '_act_' || sysma_action_type_alias || ' PRIMARY KEY(gid);' ||
                  'ALTER TABLE sysma_export_layer.'  || sysma_object_type_alias || '_act_' || sysma_action_type_alias || ' ADD CONSTRAINT pk_' ||'_' || sysma_object_type_alias || '_act_' || sysma_action_type_alias || ' PRIMARY KEY(gid);' ||
                  
                  ' COMMENT ON TABLE sysma_export_layer.' || sysma_object_type_alias || '_act_' || sysma_action_type_alias || ' IS '' Thème : ' || sysma_object_type_alias || '_act_' || sysma_action_type_alias ||  ' - 
                  description_sysma_object : ' || coalesce(replace(xx_99_utils.f_html_no_html_specials_chars_and_balises( tto.description),'''',''''''), 'Non renseigné')::text || '  
                  description_sysma_action : ' || coalesce(replace(xx_99_utils.f_html_no_html_specials_chars_and_balises( tt.description),'''',''''''), 'Non renseigné')::text || '  
                   MAJ :' || now()|| ' user : ' || current_user || ''';'
                ' ALTER TABLE sysma_export_layer.' || sysma_object_type_alias || '_act_' || sysma_action_type_alias || ' 
                     ALTER COLUMN geom TYPE geometry(' || sysma_export_layer.get_pg_geom_type_from_sysma_geom_type (geometry_type) || ', 2154) ; 
                  CREATE INDEX sidx_' || sysma_object_type_alias || '_act_' || sysma_action_type_alias || '_geom  ON sysma_export_layer.' || sysma_object_type_alias || '_act_' || sysma_action_type_alias || '   USING gist  (geom) ; '

                ' COMMENT ON COLUMN sysma_export_layer.' || sysma_object_type_alias || '_act_' || sysma_action_type_alias || '.gid IS ''identifiant unique de la couche'';' 
                ||  c_geo.col_comment ||  --  ajout des commentaires des colonnes des objets geo
                ';' ||  c_travaux.col_comment ||  --  ajout des commentaires des colonnes des travaux
                ';' ||
                ' COMMENT ON COLUMN sysma_export_layer.' || sysma_object_type_alias || '_act_' || sysma_action_type_alias || '.organisation_act  IS ''intitulé de la structure maître d''''ouvrage des travaux'' ;
 
                 COMMENT ON COLUMN sysma_export_layer.' || sysma_object_type_alias || '_act_' || sysma_action_type_alias || '.sysma_link  IS '' Lien vers les travaux sur sysma web'' ;' ||
                  'UPDATE sysma_export_layer.' || sysma_object_type_alias || '_act_' || sysma_action_type_alias || ' set sysma_link = (select '''|| _url_base || '/carte/objet/''|| obj_sysma_object_id ||''/travaux/''|| sysma_action_id) ;'  

                  'UPDATE sysma_export_layer.' || sysma_object_type_alias || '_act_' || sysma_action_type_alias || ' set sysma_action = (select xx_99_utils.f_html_no_html_specials_chars_and_balises(sysma_action)::text) ;'  
    

                /*||
                ' COMMENT ON COLUMN sysma_export_layer.' || sysma_object_type_alias || '_act_' || sysma_action_type_alias || '.sysma_object_id IS ''identifiant de l''objet geographique'';' ||
                ' COMMENT ON COLUMN sysma_export_layer.' || sysma_object_type_alias || '_act_' || sysma_action_type_alias || '.ogeo_date_creation IS ''date de création de l''objet geographique'';' ||
                ' COMMENT ON COLUMN sysma_export_layer.' || sysma_object_type_alias || '_act_' || sysma_action_type_alias || '.ogeo_date_fin IS ''date de fin de validité de l''objet geographique'';' ||  
                -- TODO continuer...
 */
                  

         from   sysma.sysma_action_type tt 
         LEFT JOIN sysma.sysma_object_type tto ON (tto.sysma_object_type_id = tt.sysma_object_type_id)
         LEFT JOIN LATERAL 
          (WITH w_col as (SELECT column_name  as col 
            , col_description(
            ('sysma_export_layer.' || table_name)::regclass::oid
            , c.ordinal_position
            ) as description FROM information_schema.columns c WHERE c.table_schema= 'sysma_export_layer' AND c.table_name = tto.sysma_object_type_alias 
               AND column_name <>'geom') 
            SELECT string_agg( 'o.' || col || ' AS obj_' || col , ',') cols_objgeo, 
            coalesce(string_agg('COMMENT ON COLUMN sysma_export_layer.' || tto.sysma_object_type_alias || '_act_' || tt.sysma_action_type_alias || '.obj_' || col ||
            '   IS ''' ||  coalesce(replace(xx_99_utils.f_html_no_html_specials_chars_and_balises( w_col.description),'''',''''''), 'Non renseigné') || '''; '
            , ';'), ' -- Pas de description de champs') as col_comment
            FROM  w_col
            )  c_geo on true

            LEFT JOIN LATERAL 
          (WITH 
            w_col as (
            SELECT 
                  column_name  as col 
               , col_description(
                  ('sysma.' || table_name)::regclass::oid
                  , c.ordinal_position
                  ) 
               as description 
               FROM information_schema.columns c WHERE c.table_schema= 'sysma' AND c.table_name = 'sysma_action'
               ) 
            SELECT 
            coalesce(string_agg('COMMENT ON COLUMN sysma_export_layer.' || tto.sysma_object_type_alias || '_act_' || tt.sysma_action_type_alias || '.' || col ||
            '   IS ''' ||  coalesce(replace(xx_99_utils.f_html_no_html_specials_chars_and_balises( w_col.description),'''',''''''), 'Non renseigné') || '''; '
            , ';'), ' -- Pas de description de champs') as col_comment
            FROM  w_col
            )  c_travaux on true
         WHERE sysma_action_type_id = _sysma_action_type_id AND sysma_action_type_alias IS not null
      ;

      IF _debugmode IS TRUE THEN
         RAISE NOTICE 'SQL part 1  : %', t_text; -- debug
      END IF;

      if is_parameter_action_exists = true then   

              -- Ajout des commentaire de champs des fiches travaux
         t_text := t_text || coalesce(string_agg( 
         ' COMMENT ON COLUMN sysma_export_layer.' || tto.sysma_object_type_alias || '_act_' || tt.sysma_action_type_alias || '.' || p.parameter_alias || 
         '   IS ''' || coalesce(p.description, 'Non renseigné') || '''; '
         , ';'), ' -- Pas de description de champs')
            from   sysma.sysma_action_type tt 
            LEFT JOIN sysma.sysma_object_type tto ON (tto.sysma_object_type_id = tt.sysma_object_type_id)
             LEFT JOIN  LATERAL sysma_export_layer.get_params_dictionary_info_from_sysma_action_type_id(_sysma_action_type_id ) p on true
            WHERE tt.sysma_action_type_id = _sysma_action_type_id AND tt.sysma_action_type_alias IS not null;
      END IF;
      
      IF _debugmode IS TRUE THEN
         RAISE NOTICE 'SQL part 1 et 2  : %', t_text; -- debug
      END IF;

      if t_text is not null  then 
         EXECUTE format(
            $sql$
            %s 
            $sql$
          , t_text)
         --USING  _param::int
         ;

    
       
            return true;
        ELSE
            return FALSE;
        END IF;


END
$function$
 ;
