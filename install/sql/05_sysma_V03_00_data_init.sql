/* liste des tables
    contract
  , data_type_choice
  , filter  
  , l_sysma_object_sysma_action
  , l_sysma_object_sysma_relation
  , l_sysma_object_type_sysma_relation_type
  , layer_group
  , sysma.organisation
  , statuts
  , sysma_action_type
  , sysma_action_type_parameter
  , sysma_action_type_parameter_choice
  , sysma_conf
  , sysma_object_type
  , sysma_object_type_parameter
  , sysma_object_type_parameter_choice
  , sysma_relation_type
  , sysma_relation_type_parameter
  , sysma_relation_type_parameter_choice
  , theme_analysis

*/

/*
\echo :org_name
\echo :org_acron
*/
-- organisation
INSERT INTO sysma.organisation (
organisation_id, organisation, acronym, track_historytables_id
)
VALUES (1, :org_name, :org_acron, 0)
, (2, 'Pas de structure', 'Pas de structure', 0)
, (3, 'Non renseigné', 'Non renseigné', 0)
;

-- users :
INSERT INTO sysma.user (
user_id, firstname, lastname, "user", registration_date, organisation_id, default_center, base_layer, password, last_access, default_zoom,  map_width, modified_by, privileges, active,user_schema)
 VALUES 
 (0,'Utilisateur','anonyme','nonconnecte','2016-06-21 12:00:00',NULL,NULL,'OpenStreetMap',NULL,NULL,NULL,60,1,NULL,true,'_agent_nonconnecte')
,(1,'Init_Sysma','Initialisation de sysma','init_sysma',NULL,1,NULL,NULL,NULL,NULL,10,NULL,1,NULL,true,'_agent_moulinettephp') 
-- CAUTION user_id must equal 2 for now. (sysma.user_privilege below)  
,(2,:appadmin_firstname,:appadmin_lastname,:appadmin_nickname,:date,1,NULL,'OpenStreetMap',:appadmin_pswdhash,:date,17,60,2,'{administrateur,cadastre,gis_export}',true,'{"active":false,"layer_id":null,"distance":15}')
;

-- user_privilege
INSERT INTO sysma.user_privilege(
             user_id, sysma_object_organisation_id_list, 
            sysma_object_status_list, sysma_action_type_id_list, sysma_action_status_list, 
            sysma_action_organisation_id_list, privilege_list, sysma_object_type_id_list, 
            track_historytables_id, modified_by)
    VALUES 
-- CAUTION user_id must equal 2 for now. (sysma.user above)  
    -- droits 
    (2, '{0}', 
            '{tous}', '{0}', '{tous}', 
            '{0}', '{lecteur,contributeur,gestionnaire}', '{0}', 
            0, 1)
;


COPY sysma.other_layer (other_layer_id, layer_id, geolayer_id, layer_type, style, geometry_type, mapping, "group", access, sql_filter, track_historytables_id, zoom_min, zoom_max, load_on_pan, other_layer_display_order, options, layer_alias) FROM stdin;
1	Open Street Map	https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png	tile_layer	{}		{}	baseLayers	public	\N	192649	0	0	0	00	 maxZoom: 19, attribution: 'Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors'	OpenStreetMap
\.

-- INSERT INTO sysma.other_layer(other_layer_id, layer_id, geolayer_id, layer_type, style, geometry_type, mapping, "group", access, sql_filter, track_historytables_id, zoom_min, zoom_max, load_on_pan, other_layer_display_order, options, layer_alias)
--     VALUES 
-- -- CAUTION user_id must equal 2 for now. (sysma.user above)  
--     -- droits 
--     (1, 
--         'Open Street Map', 
--         'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
--         'tile_layer',
--         '{}', 
--         '',
--         '{}',
--         'baseLayers', 
--         'public',
--         NULL,
--         NULL,
--         0,
--         0,
--         0,
--         '00',
--         " maxZoom: 19, attribution: 'Map data © <a href='http://openstreetmap.org'>OpenStreetMap</a> contributors",
--         'OpenStreetMap')
-- ;


-- contract
INSERT INTO sysma.contract (contract_id, contract, display_order, track_historytables_id) 
    VALUES (1, 'Exemple de contrat', 1, 0);

-- data_type_choice
/*
WITH to_ins as (
    SELECT * FROM  rx000_sysma_referentiel_mutualise_fdw.data_type_choice
)
SELECT xx_99_utils.pg_insert_or_update(
    schemadest:='sysma' :: name,
    tabledest:= 'data_type_choice' ::name,
    ignore_col_list_in_json := '{track_historytables_id}'::name[],
    matching_col_list_or_null_for_keeping_all:= '{data_type_id}'::NAME[],
    json_raw := to_json (s) ::json,
    operation_mode := 'NO UPDATE ON SAME'::text,
    debugmode := FALSE ::boolean
   ) 
   FROM to_ins  s  
    ;
*/
	
	
-- filter @SR est ce que filter id_ est utlisé dans les vues ?
INSERT INTO sysma.filter (
 sql, object_type, filter
) VALUES ('o.status = ''existant''', 'sysma_object', 'Statut "Existant"')
, ('t.status = ''programme''','sysma_action' , 'Statut "Programmé"')
;



-- layer_group 
/*
WITH to_ins as (
    SELECT * FROM  rx000_sysma_referentiel_mutualise_fdw.layer_group
)
SELECT xx_99_utils.pg_insert_or_update(
    schemadest:='sysma' :: name,
    tabledest:= 'layer_group' ::name,
    ignore_col_list_in_json := NULL::name[],
    matching_col_list_or_null_for_keeping_all:= '{layer_group_id}'::NAME[],
    json_raw := to_json (s) ::json,
    operation_mode := 'NO UPDATE ON SAME'::text,
    debugmode := FALSE ::boolean
   ) 
   FROM to_ins  s  
    ;
*/



-- status
/*
WITH to_ins as (
    SELECT * FROM  rx000_sysma_referentiel_mutualise_fdw.status
)
SELECT xx_99_utils.pg_insert_or_update(
    schemadest:='sysma' :: name,
    tabledest:= 'status' ::name,
    ignore_col_list_in_json := '{track_historytables_id}'::name[],
    matching_col_list_or_null_for_keeping_all:= '{status_id}'::NAME[],
    json_raw := to_json (s) ::json,
    operation_mode := 'NO UPDATE ON SAME'::text,
    debugmode := FALSE ::boolean
   ) 
   FROM to_ins  s  
    ;
*/
/*
Avec maj Dico
*/
-- sysma_action_type
-- sysma_action_type_parameter
-- sysma_action_type_parameter_choice

-- sysma_object_type
-- sysma_object_type_parameter
-- sysma_object_type_parameter_choice
--  sysma_relation_type
--  sysma_relation_type_parameter
--  sysma_relation_type_parameter_choice

--  l_sysma_object_sysma_action
--  l_sysma_object_sysma_relation
--  l_sysma_object_type_sysma_relation_type
--    TODO

-- sysma_conf
/* avec process dedié */

--  theme_analysis => non
