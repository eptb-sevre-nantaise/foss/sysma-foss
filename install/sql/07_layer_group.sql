--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.17
-- Dumped by pg_dump version 13.2

-- Started on 2021-12-09 15:29:10

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
/*
SELECT pg_catalog.set_config('search_path', '', false);
*/
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- TOC entry 694 (class 1259 OID 119649794)
-- Name: layer_group; Type: TABLE; Schema: sysma; Owner: eptbsn_dba_antoine
--
/*
CREATE TABLE sysma.layer_group (
    layer_group_id integer NOT NULL,
    group_alias character varying,
    group_name character varying,
    group_display_order character varying
);


ALTER TABLE sysma.layer_group OWNER TO eptbsn_dba_antoine;

--
-- TOC entry 5778 (class 0 OID 0)
-- Dependencies: 694
-- Name: TABLE layer_group; Type: COMMENT; Schema: sysma; Owner: eptbsn_dba_antoine
--

COMMENT ON TABLE sysma.layer_group IS '{
   "display_groupe_order":160,
   "groupe_name":"Configuration de sysma",
   "table_comment":"Liste des groupes de couches sysma"
}';


--
-- TOC entry 695 (class 1259 OID 119649800)
-- Name: layer_group_layer_group_id_seq; Type: SEQUENCE; Schema: sysma; Owner: eptbsn_dba_antoine
--

CREATE SEQUENCE sysma.layer_group_layer_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sysma.layer_group_layer_group_id_seq OWNER TO eptbsn_dba_antoine;

--
-- TOC entry 5780 (class 0 OID 0)
-- Dependencies: 695
-- Name: layer_group_layer_group_id_seq; Type: SEQUENCE OWNED BY; Schema: sysma; Owner: eptbsn_dba_antoine
--

ALTER SEQUENCE sysma.layer_group_layer_group_id_seq OWNED BY sysma.layer_group.layer_group_id;


--
-- TOC entry 5546 (class 2604 OID 119649899)
-- Name: layer_group layer_group_id; Type: DEFAULT; Schema: sysma; Owner: eptbsn_dba_antoine
--

ALTER TABLE ONLY sysma.layer_group ALTER COLUMN layer_group_id SET DEFAULT nextval('sysma.layer_group_layer_group_id_seq'::regclass);

*/
--
-- TOC entry 5771 (class 0 OID 119649794)
-- Dependencies: 694
-- Data for Name: layer_group; Type: TABLE DATA; Schema: sysma; Owner: eptbsn_dba_antoine
--

COPY sysma.layer_group (layer_group_id, group_alias, group_name, group_display_order) FROM stdin;
1	administratif	Administratif	010
2	agriculture	Agriculture	020
3	autres	Autres	030
4	crue_inondation	Crues et inondations	040
5	ecoulements	Ecoulements	050
6	etat_des_lieux	Etat des lieux	060
7	suivis	Suivis	070
11	zh_lit_majeur	ZH, lit majeur	110
9	travaux_courants	Travaux courants	090
10	valorisation	Valorisation	100
8	test	Test	000
12	a_supprimer	A supprimer	999
13	photo	Photos	500
15	couches_utiles	Couches utiles	800
14	trace_cours_eau	Tracés des cours d'eau	600
16	tetes_bv	Têtes de bassin	065
\.

/*
--
-- TOC entry 5782 (class 0 OID 0)
-- Dependencies: 695
-- Name: layer_group_layer_group_id_seq; Type: SEQUENCE SET; Schema: sysma; Owner: eptbsn_dba_antoine
--

SELECT pg_catalog.setval('sysma.layer_group_layer_group_id_seq', 16, true);


--
-- TOC entry 5548 (class 2606 OID 119649929)
-- Name: layer_group geoobject_type_group_group_alias_key; Type: CONSTRAINT; Schema: sysma; Owner: eptbsn_dba_antoine
--

ALTER TABLE ONLY sysma.layer_group
    ADD CONSTRAINT geoobject_type_group_group_alias_key UNIQUE (group_alias);


--
-- TOC entry 5550 (class 2606 OID 119649931)
-- Name: layer_group geoobject_type_group_group_name_key; Type: CONSTRAINT; Schema: sysma; Owner: eptbsn_dba_antoine
--

ALTER TABLE ONLY sysma.layer_group
    ADD CONSTRAINT geoobject_type_group_group_name_key UNIQUE (group_name);


--
-- TOC entry 5552 (class 2606 OID 119649933)
-- Name: layer_group geoobject_type_group_pkey; Type: CONSTRAINT; Schema: sysma; Owner: eptbsn_dba_antoine
--

ALTER TABLE ONLY sysma.layer_group
    ADD CONSTRAINT geoobject_type_group_pkey PRIMARY KEY (layer_group_id);


--
-- TOC entry 5779 (class 0 OID 0)
-- Dependencies: 694
-- Name: TABLE layer_group; Type: ACL; Schema: sysma; Owner: eptbsn_dba_antoine
--

GRANT ALL ON TABLE sysma.layer_group TO grp_db_ext_mut_98_demo_dev_sysma_rw;
GRANT ALL ON TABLE sysma.layer_group TO grp_db_ext_mut_98_demo_dev_sysma_admin;


--
-- TOC entry 5781 (class 0 OID 0)
-- Dependencies: 695
-- Name: SEQUENCE layer_group_layer_group_id_seq; Type: ACL; Schema: sysma; Owner: eptbsn_dba_antoine
--

GRANT SELECT,USAGE ON SEQUENCE sysma.layer_group_layer_group_id_seq TO grp_db_ext_mut_98_demo_dev_sysma_rw;
GRANT ALL ON SEQUENCE sysma.layer_group_layer_group_id_seq TO grp_db_ext_mut_98_demo_dev_sysma_admin;


-- Completed on 2021-12-09 15:29:12

--
-- PostgreSQL database dump complete
--

*/