-- increment sequences after COPY or inserts

SELECT setval('sysma.other_layer_other_layer_id_seq', coalesce(max(other_layer_id),0)+1) FROM sysma.other_layer;

SELECT setval('sysma.contract_contract_id_seq', coalesce(max(contract_id),0)+1) FROM sysma.contract;
SELECT setval('sysma.filter_filter_id_seq', coalesce(max(filter_id),0)+1) FROM sysma.filter;
SELECT setval('sysma.user_privilege_user_privilege_id_seq', coalesce(max(user_privilege_id),0)+1) FROM sysma.user_privilege; 

SELECT setval('sysma.data_type_choice_data_type_id_seq', coalesce(max(data_type_id),0)+1) FROM sysma.data_type_choice;

SELECT setval('sysma.layer_group_layer_group_id_seq', coalesce(max(layer_group_id),0)+1) FROM sysma.layer_group;
SELECT setval('sysma.status_status_id_seq', coalesce(max(status_id),0)+1) FROM sysma.status;
SELECT setval('sysma.sysma_conf_sysma_conf_id_seq', coalesce(max(sysma_conf_id),0)+1) FROM sysma.sysma_conf; 



