/*
HERE IS A COMMENT
*/


/* add Extension Postgis */

CREATE EXTENSION IF NOT EXISTS postgis
  SCHEMA public;

/* create schemas */

create schema sysma ;
create schema sysma_export_layer;
create schema sysma_atlas;
create schema xx_99_utils;

/*
\echo :org_name
\echo :org_acron
*/
