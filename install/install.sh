#!/bin/bash


##########################################################################################################################################
# You can edit the following default values according to your needs
##########################################################################################################################################

# The php bin executable to use
PHP='php'
# PHP='php7.2' # requested on Alteo atm

DEFAULT_ORG_NAME='Etablissement Public Territorial du Bassin de la Sevre-Nantaise'
DEFAULT_ORG_ACRON='EPTB-SN'
DEFAULT_ORG_WEBSITE='https://sevre-nantaise.com'
DEFAULT_DB_HOST='localhost'
DEFAULT_DB_PORT='5432'
DEFAULT_SYSMA_DB_NAME='sysma'













##########################################################################################################################################
# DO NOT EDIT BELOW UNLESS YOU KNOW WHAT YOU'RE DOING
##########################################################################################################################################
# consts
BLUE='\033[0;34m'
CYAN='\033[0;36m'
ORANGE='\033[0;32m'
LIGHTPURPLE='\033[1;35m'
LIGHTGREEN='\033[1;32m'


GREEN='\033[0;32m'
YELLOW='\033[0;33m'
RED='\033[0;31m'
NOCOLOR='\033[0m'
BOLD="\033[1m"

ASK_COLOR=$LIGHTPURPLE
INPUT_COLOR=$LIGHTGREEN

# Little helpers functions
checkError () {
    if [ $1 -ne '0' ]; then
        echo 
        echo -e $RED"Install exist."$NOCOLOR
        exit
    fi
}


##########################################################################################################################################
# Install Sysma App
##########################################################################################################################################

echo
echo -e $NOCOLOR"******************************************************************************"
echo Sysma App installer v1.0
echo

##########################################################################################################################################
# Download/Update Sysma app
# 
# It doesn't make sens to clone sysma atm since this install script comes from sysma itself
#    git clone https://gitlab.sevre-nantaise.com/eptbsn/sysma-dev-eptbsn.git ../
read -p "$(echo -e $ASK_COLOR"Update Sysma to the lastest code version? y/n [n]: "$INPUT_COLOR)"  updatesysma
echo -e $NOCOLOR
updatesysma=${updatesysma:-n}
if [ "$updatesysma" == y ]; then
   echo -n "Updating Sysma..."
   cd ..
   git pull
   cd install
fi

echo "Installing default .htaccess..."
cp ../public/.htaccess.template ../public/.htaccess
echo -e $YELLOW"WARNING! PLEASE REVIEW AND ADJUST THIS FILE IF NECESSARY: public/.htaccess."$NOCOLOR

##########################################################################################################################################
# Check if composer.phar is already downloaded or ask to download it
if [ -e composer.phar ]; then
    echo "composer.phar file found in this /install directory."
else 
    echo "composer.phar file not found but required."
    read -p "$(echo -e $ASK_COLOR"Download composer? y/n [y]: "$INPUT_COLOR)"  dlcomposer
    echo -e $NOCOLOR

    dlcomposer=${dlcomposer:-y}
    if [ "$dlcomposer" == y ]; then
        echo -n "Downloading composer.phar..."
        wget https://getcomposer.org/download/latest-stable/composer.phar
    else
        echo -e $RED"Missing required composer.phar file."
        # echo -e $RED"Install exist."$NOCOLOR
        checkError 1
        # exit
    fi
fi

##########################################################################################################################################
# Sysma composer install
# 
cd ..
$PHP install/composer.phar install
cd install





##########################################################################################################################################
# Sysma DB install script
##########################################################################################################################################
# 1) Incoming data deletion warning
echo
echo -e $NOCOLOR"******************************************************************************"
echo Sysma DB installer v1.0
echo
read -p "$(echo -e $YELLOW"WARNING!"$ASK_COLOR" The following procedure will clear/delete all Sysma data. Are your sure? Y/n [n]: "$INPUT_COLOR)"  installdb
echo -e -n $NOCOLOR
installdb=${installdb:-$n}
if [ "$installdb" != Y ]; then
    echo -e $RED"Install exist."$NOCOLOR
    exit
fi

# 2) get inputs info from Sysma/DB admin
read -p "$(echo -e $ASK_COLOR"Enter your organisation name ["$DEFAULT_ORG_NAME"]: "$INPUT_COLOR)"  orgname
echo -e -n $NOCOLOR
orgname=${orgname:-$DEFAULT_ORG_NAME}

read -p "$(echo -e $ASK_COLOR"Enter your organisation acronym ["$DEFAULT_ORG_ACRON"]: "$INPUT_COLOR)"  orgacron
echo -e -n $NOCOLOR
orgacron=${orgacron:-$DEFAULT_ORG_ACRON}

read -p "$(echo -e $ASK_COLOR"Enter your organisation website URL ["$DEFAULT_ORG_WEBSITE"]: "$INPUT_COLOR)"  orgwebsite
echo -e -n $NOCOLOR
orgwebsite=${orgwebsite:-$DEFAULT_ORG_WEBSITE}

read -p "$(echo -e $ASK_COLOR"Enter your Sysma DB host ["$DEFAULT_DB_HOST"]: "$INPUT_COLOR)"  dbhost
echo -e -n $NOCOLOR
dbhost=${dbhost:-$DEFAULT_DB_HOST}

read -p "$(echo -e $ASK_COLOR"Enter your Sysma DB server port ["$DEFAULT_DB_PORT"]: "$INPUT_COLOR)"  dbport
echo -e -n $NOCOLOR
dbport=${dbport:-$DEFAULT_DB_PORT}
echo 

read -p "$(echo -e $ASK_COLOR"Enter your Sysma DB name ["$DEFAULT_SYSMA_DB_NAME"]: "$INPUT_COLOR)"  sysmadbname
echo -e -n $NOCOLOR
sysmadbname=${sysmadbname:-$DEFAULT_SYSMA_DB_NAME}

read -p "$(echo -e $ASK_COLOR"Enter your Sysma DB user: "$INPUT_COLOR)"  dbuser
echo -e -n $NOCOLOR
read -s -p "$(echo -e $ASK_COLOR"  --> Enter its password: "$NOCOLOR)"  dbpswd
echo 
echo 

read -p "$(echo -e $ASK_COLOR"Enter the 1st Sysma App admin user: "$INPUT_COLOR)"  appadminnickname
echo -e -n $NOCOLOR
read -s -p "$(echo -e $ASK_COLOR"  --> Enter its password: "$NOCOLOR)"  appadminpswd
echo
# appadminpswdhash=$(php7.2 -r "echo password_hash('$appadminpswd', PASSWORD_DEFAULT, ['cost' => 14]);")
appadminpswdhash=$($PHP -r "echo password_hash('$appadminpswd', PASSWORD_DEFAULT, ['cost' => 14]);")
# test=$($PHP -r "echo password_verify('$appadminpswd', '$appadminpswdhash');")
# echo $test
echo

read -p "$(echo -e $ASK_COLOR"Enter Sysma layer group [default]: "$INPUT_COLOR)"  layergroup
echo -e -n $NOCOLOR
layergroup=${layergroup:-default}

read -p "$(echo -e $ASK_COLOR"Enter Sysma common layers [default_fr]: "$INPUT_COLOR)"  commonlayers
# echo -e $RED"TODO" 
echo -e -n $NOCOLOR
default_fr=${commonlayers:-default_fr}

read -p "$(echo -e $ASK_COLOR"Enter Sysma status [default]: "$INPUT_COLOR)"  status
echo -e -n $NOCOLOR
status=${status:-default}

read -p "$(echo -e $ASK_COLOR"If you are a developper: enable this to install front build toolchain. Yes, no? Y/n [n]: "$INPUT_COLOR)"  installdb
echo -e -n $NOCOLOR
installDevTk=${installDevTk:-$n}

echo -e $NOCOLOR



##########################################################################################################################################
# 3) execute SQL scripts according to the above infos
export PGPASSWORD=$dbpswd
export PGOPTIONS='--client-min-messages=warning'

# echo -e $GREEN
echo -e $NOCOLOR"*******************************************************************************************"
echo -e "Clean up/delete all previous Sysma DB schemas..." $RED
psql $dbuser -h $dbhost -p $dbport -d $sysmadbname -f sql/00_sysma_delete_all.sql --quiet -v "ON_ERROR_STOP=1"
checkError $?
echo -e $GREEN"done"
echo $1

echo -e $NOCOLOR"*******************************************************************************************"
echo -e "Install Sysma DB base..." $RED
psql $dbuser -h $dbhost -p $dbport -d $sysmadbname -f sql/01_sysma_base_install.sql --quiet -v "ON_ERROR_STOP=1"
checkError $?
echo -e $GREEN"done"

echo -e $NOCOLOR"*******************************************************************************************"
echo -e "Install Sysma DB schemas..." $RED
psql $dbuser -h $dbhost -p $dbport -d $sysmadbname -f sql/02_sysma_V03_00_all_ddl_schema_sysma.sql --quiet -v "ON_ERROR_STOP=1"
checkError $?
echo -e $GREEN"done"

echo -e $NOCOLOR"*******************************************************************************************"
echo -e "Install DB common tool functions ..." $RED
psql $dbuser -h $dbhost -p $dbport -d $sysmadbname -f sql/03_sysma_V03_00_toolkit_funcs.sql --quiet -v "ON_ERROR_STOP=1"
checkError $?
echo -e $GREEN"done"

echo -e $NOCOLOR"*******************************************************************************************"
echo -e "Install DB export layer functions ..." $RED
psql $dbuser -h $dbhost -p $dbport -d $sysmadbname -f sql/04_sysma_V03_00_all_ddl_schema_export_layer_functions.sql --quiet -v "ON_ERROR_STOP=1"
checkError $?
echo -e $GREEN"done"

echo -e $NOCOLOR"*******************************************************************************************"
echo -e "Init Sysma DB application data..." $RED
now=$(date '+%Y-%m-%d %H:%M:%S')
psql $dbuser -h $dbhost -p $dbport -d $sysmadbname -f sql/05_sysma_V03_00_data_init.sql --quiet -v org_name="'$orgname'" -v org_acron="'$orgacron'" -v appadmin_firstname="'$appadminfirstname'" -v appadmin_lastname="'$appadminlastname'" -v appadmin_nickname="'$appadminnickname'" -v appadmin_pswdhash="'$appadminpswdhash'" -v date="'$now'" -v "ON_ERROR_STOP=1"
checkError $?
echo -e $GREEN

echo -e $NOCOLOR"Init Sysma DB application data : data_type_choice..." $RED
psql $dbuser -h $dbhost -p $dbport -d $sysmadbname -f sql/06_data_type_choice.sql --quiet -v "ON_ERROR_STOP=1"
checkError $?
echo -e $GREEN"done"

echo -e $NOCOLOR"Init Sysma DB application data : layer_group..." $RED
psql $dbuser -h $dbhost -p $dbport -d $sysmadbname -f sql/07_layer_group.sql --quiet -v "ON_ERROR_STOP=1"
checkError $?
echo -e $GREEN"done"

echo -e $NOCOLOR"Init Sysma DB application data : status..." $RED
psql $dbuser -h $dbhost -p $dbport -d $sysmadbname -f sql/08_status.sql --quiet -v "ON_ERROR_STOP=1"
checkError $?
echo -e $GREEN"done"

echo -e $NOCOLOR"Init Sysma DB application data : sysma_conf..." $RED
psql $dbuser -h $dbhost -p $dbport -d $sysmadbname -f sql/09_sysma_conf.sql --quiet -v org_name="'$orgname'" -v org_acron="'$orgacron'" -v org_website="'$orgwebsite'"
checkError $?
echo -e $GREEN"done"

echo -e $NOCOLOR"Init Sysma DB application data : sysma_conf..." $RED
psql $dbuser -h $dbhost -p $dbport -d $sysmadbname -f sql/20_increment_seq.sql --quiet -v org_name="'$orgname'" -v org_acron="'$orgacron'" -v org_website="'$orgwebsite'"
checkError $?
echo -e $GREEN"done"


##########################################################################################################################################
# 4) set local secrets into php Sysma access conf file 
echo -e  $NOCOLOR"Setting up Sysma app to Sysma DB access local private file : ..." $YELLOW
CONF_ACCES_PRIVATE_FILE_PATH='../conf/private/conf_access.private.php';

if [ -e $CONF_ACCES_PRIVATE_FILE_PATH ]; then
    echo $CONF_ACCES_PRIVATE_FILE_PATH" file already exist."
    echo "Updating private conf access file..."
else 
    echo "Creating private conf access file..."
    cp ../conf/conf_access.template.php $CONF_ACCES_PRIVATE_FILE_PATH
fi

# editing conf_access.private.php
sed -i "s/define('DBNAME',.*);/define('DBNAME', '$sysmadbname');/g" $CONF_ACCES_PRIVATE_FILE_PATH
sed -i "s/define('DBUSER',.*);/define('DBUSER', '$dbuser');/g" $CONF_ACCES_PRIVATE_FILE_PATH
sed -i "s/define('DBPWD',.*);/define('DBPWD', '$dbpswd');/g" $CONF_ACCES_PRIVATE_FILE_PATH
sed -i "s/define('DBHOST',.*);/define('DBHOST', '$dbhost');/g" $CONF_ACCES_PRIVATE_FILE_PATH
sed -i "s/define('DBPORT',.*);/define('DBPORT', '$dbport');/g" $CONF_ACCES_PRIVATE_FILE_PATH

echo -e $GREEN"done"


# Update DB structures by migrating DB evolutions
cd ../migrations
rm already_migrated/*.sql
./migrate_up_to_date.sh

# Install Vue and Vite deps for Vue build toolchain. Introduced by Sysma-offline feature.
if [ "$installDevTk" == Y ]; then
    echo "Installing dev tools..."
    cd ../offline
    npm install
    # Then build Vue stuff
    echo "Building some front stuff..."
    npm run build
fi
echo -e $NOCOLOR


cd ../install

# End of install
echo "Sysma install well done."
echo -e $NOCOLOR
