<?php

/**************************************************************************************
  1) Default App global vars should be set in this section at first 
  DO NOT AD CODE IN THIS SECTION PLEASE
**************************************************************************************/

// TODO doc
$DEFAULT_PHP_SET_TIME_LIMIT = 600;
// TODO doc
$DEFAULT_PHP_MEMORY_LIMIT = '5000M';
//  Welcome message
$DEFAULT_WELCOME_MESSAGE = "<h1>Sysma " . ORGANISATION_ACRONYM . "</h1>
<h3><a href=\"#\" onclick=\"sendData(null, '/login' , 'modal-content' , event, null);\" data-toggle=\"modal\" data-target=\"#loginModal\">Connectez-vous</a> pour accéder aux couches Sysma.</h3>";


/////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////
// TODO: faire le menage et du rangement pour respecter la nouvelle organisations et initialisation des globals vars de l'App
// cf. poitns 1) 2) 3) ans ce fichier 

define('DEFAULT_PARAMETER_DEFINITION_SOURCE', ORGANISATION_ACRONYM);
define('DEFAULT_PARAMETER_DATA_SOURCE', ORGANISATION_ACRONYM);

define('BASEURL', $_SERVER['SERVER_NAME']);

$protocol = 'http';
if (isset($_SERVER['HTTPS']) and $_SERVER['HTTPS'] == 'on') {
  $protocol = 'https';
}
define('PROTOCOL', $protocol);
define('WEBURL', PROTOCOL . "://" . BASEURL);

define('SESSIONTIME', 1 * 24 * 60 * 60); // 1 day
define('COOKIEEXPIRE', 365 * 24 * 3600); // 1 year

define('REP_BIN_GDAL', '/usr/bin');

//  UNIT COSTS
$tabUnitMeasures = ['unit' => 'unitaire', 'object' => 'objet', 'm' => 'mètre', 'm²' => 'm²', 'm³' => 'm³'];
$GLOBALS['UNIT_MEASURES'] = $tabUnitMeasures;



//  ZOOM
$tabZ = ['0' => 1000000000, '1' => 100000000, '2' => 6000000, '3' => 5000000, '4' => 4000000, '5' => 3000000, '6' => 3000000, '7' => 1000000, '8' => 300000, '9' => 120000, '10' => 50000, '11' => 40000, '12' => 15000, '13' => 8000, '14' => 4000, '15' => 2750, '16' => 1500, '17' => 1000, '18' => 900, '19' => 500, '20' => 200];
$GLOBALS['ZOOM_CONVERSION_ARRAY1'] = $tabZ;
$tabZ2 = ['0' => 500, '1' => 100, '2' => 90, '3' => 80, '4' => 70, '5' => 60, '6' => 50, '7' => 40, '8' => 30, '9' => 20, '10' => 10, '11' => 15, '12' => 6, '13' => 5, '14' => 2, '15' => 1, '16' => 1, '17' => 0.1, '18' => 0.1, '19' => 0.1, '20' => 0.1];
$GLOBALS['ZOOM_CONVERSION_ARRAY2'] = $tabZ2;

// PRIVILEGE LEVELS
$GLOBALS['PRIVILEGES_LEVELS'] = ['lecteur' => 'Lecture', 'contributeur' => 'Contribution', 'gestionnaire' => 'Gestion'];

// end TODO
/////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////




/**************************************************************************************
  2) local global vars values can be set in this file to override default global vars set above
  DO NOT EDIT THOSE LINE ABOVE UNLESS YOU KNOW WHAT YOU'RE DOING!
**************************************************************************************/
if (file_exists(ROOT_URI . 'conf/conf_app_default.php.local')) require_once(ROOT_URI . 'conf/conf_app_default.php.local');

/**************************************************************************************
  3) local global vars values can be set in this file to override default global vars set above
  DO NOT EDIT THOSE LINe ABOVe UNLESS YOU KNOW WHAT YOU'RE DOING!
  Feel free to add new vars though :)
**************************************************************************************/
define('WELCOME_MESSAGE', $DEFAULT_WELCOME_MESSAGE);
define('PHP_SET_TIME_LIMIT', $DEFAULT_PHP_SET_TIME_LIMIT);
define('PHP_MEMORY_LIMIT', $DEFAULT_PHP_MEMORY_LIMIT);
