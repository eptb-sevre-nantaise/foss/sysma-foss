<?php

/*****************************************************************************
 * This is a template processes configuration file.
 * 1) You must have a copy this file in the current folder and rename to conf_processes.php.
 * 2) Modify conf_processes.php with proper configuration
 ****************************************************************************/

// PROCESSES

// debut process calcul indicateurs lit mineur E&V
$P['lit_mineur_calculations'] = [
    'id' => 'lit_mineur_calculations',
    'title' => 'Indicateurs lit mineurs en état des lieux',
    'path' => '/lit_mineur_calculations.php',
    'args' =>
    [
    ],
    'return_args' =>
    [ // id a adapter au besoin
        'degres_irregularite_berge' => ['parameter_id' => 127890068],
        'n0' => ['parameter_id' => 127890044],
        'n1' => ['parameter_id' => 127890045],
        'n2' => ['parameter_id' => 127890046],
        'n3' => ['parameter_id' => 127890047],
        'n4' => ['parameter_id' => 127890048],
        'm' => ['parameter_id' => 127890049],
        'K' => ['parameter_id' => 127890067],
        'classe_K' => ['parameter_id' => 127890073],
        'surface_plein_bord' => ['parameter_id' => 75656871],
        'perimetre_plein_bord' => ['parameter_id' => 127890050],
        'rayon_hydraulique' => ['parameter_id' => 127890051],
        'indice_talweg' => ['parameter_id' => 127890052],
        'indice_sinuosite' => ['parameter_id' => 127890053],
        'indice_cem' => ['parameter_id' => 127890054],
        'indice_rugosite' => ['parameter_id' => 127890055],
        'indice_protection_berge' => ['parameter_id' => 127890056],
        'indice_artificialisation_note' => ['parameter_id' => 127890057],
        'indice_artificialisation_classe' => ['parameter_id' => 127890058],
        ]
    ];
    // fin process calcul indicateurs lit mineur E&V
// Generic definitions 
    
$P['altitude_and_slope_from_rast_v02'] = [
    'id' => 'altitude_and_slope_from_rast_v02',
    'title' => 'Altitude et pente depuis raster pg',
    'path' => '/altitude_and_slope_from_rast_v02.php',
    'args' =>
    [
        'buffer_find_m' => [
            'type' => 'numeric',
            'parameter' => 'Taille du rayon de recherche en m (proposition : 3x taille maille raster)',
            'hidden' => true,
            'default_value' => 15
        ],
        'mntschemaname' => [
            'type' => 'text',
            'parameter' => 'schema du raster',
            'hidden' => true,
            'default_value' => 'r020_territoire_physique'
        ],
        'mnttablename' => [
            'type' => 'text',
            'parameter' => 'table du raster',
            'hidden' => true,
            'default_value' => 'r_mnt_alti_aspect_slope_percent_grass'
            ]       
    ],
    'return_args' =>
    [
        'alti_up' => ['parameter_id' => 315285],
        'alti_down' => ['parameter_id' => 315286],
        'slope' => ['parameter_id' => 315287],
        'length' => ['parameter_id' => 315284]
        ]
];
    

$P['get_watershed_area_from_linegeom_v02'] = [
    'id' => 'get_watershed_area_from_linegeom_v02',
    'title' => 'Récupérer la surface du bassin amont',
    'path' => '/get_watershed_area_from_linegeom_v02.php',
    'args' =>
    [
        'stream_schemaname' => [
            'type' => 'text',
            'parameter' => 'schema contenant les réseaux théoriques',
            'hidden' => true,
            'default_value' => 'r020_territoire_physique'
        ],
        'stream_tablename_4000' => [
            'type' => 'text',
            'parameter' => 'table réseaux théoriques grossiers (ex threshold 4000)',
            'hidden' => true,
            'default_value' => 't_mnt_sc_sk_stream_4000'
        ],
        'stream_tablename_500' => [
            'type' => 'text',
            'parameter' => 'table réseaux théoriques moyens (ex threshold 500)',
            'hidden' => true,
            'default_value' => 't_mnt_sc_sk_stream_500'
        ],
        'stream_tablename_50' => [
            'type' => 'text',
            'parameter' => 'table réseaux théoriques fins (ex threshold 50)',
            'hidden' => true,
            'default_value' => 't_mnt_sc_sk_stream_50'
        ]

    ],
    'return_args' => [
        'surface_bv' => ['parameter_id' => 315288]
    ]
];
    
$P['altitude_and_slope_troncon'] = $P['altitude_and_slope_from_rast_v02'];
$P['altitude_and_slope_troncon']['return_args'] = [
    'alti_up' => ['parameter_id' => 315285],
    'alti_down' => ['parameter_id' => 315286],
    'slope' => ['parameter_id' => 315287],
    'length' => ['parameter_id' => 315284]
];



$P['altitude_and_slope_reh'] = $P['altitude_and_slope_from_rast_v02'];
$P['altitude_and_slope_reh']['return_args'] = [
    'alti_up' => ['parameter_id' => 292],
    'alti_down' => ['parameter_id' => 291],
    'slope' => ['parameter_id' => 623998],
    'length' => null
];


$P['altitude_and_slope_lit_recharge'] = $P['altitude_and_slope_from_rast_v02'];
$P['altitude_and_slope_lit_recharge']['return_args'] = [
    'alti_up' => ['parameter_id' => 962733],
    'alti_down' => ['parameter_id' => 962734],
    'slope' => ['parameter_id' => 962735],
    'length' => null
];
$P['altitude_and_slope_lit_remeandre'] = $P['altitude_and_slope_from_rast_v02'];
$P['altitude_and_slope_lit_remeandre']['return_args'] = [
    'alti_up' => ['parameter_id' => 966563],
    'alti_down' => ['parameter_id' => 966564],
    'slope' => ['parameter_id' => 966565],
    'length' => null
];
$P['altitude_and_slope_lit_restaure'] = $P['altitude_and_slope_from_rast_v02'];
$P['altitude_and_slope_lit_restaure']['return_args'] = [
    'alti_up' => ['parameter_id' => 967052],
    'alti_down' => ['parameter_id' => 967053],
    'slope' => ['parameter_id' => 967054],
    'length' => null
];




$P['t2bv_calculations'] = [
    'id' => 't2bv_calculations',
    'title' => 'Indicateurs séquences têtes de BV',
    'path' => '/t2bv_calculations.php',
    'args' =>
    [],
    'return_args' =>
    [
        'degres_irregularite_berge' => ['parameter_id' => 158641],
        'n0' => ['parameter_id' => 415828],
        'n1' => ['parameter_id' => 415837],
        'n2' => ['parameter_id' => 415838],
        'n3' => ['parameter_id' => 415839],
        'n4' => ['parameter_id' => 415841],
        'm' => ['parameter_id' => 415842],
        'K' => ['parameter_id' => 158646],
        'classe_K' => ['parameter_id' => 158647],
        'surface_plein_bord' => ['parameter_id' => 324203],
        'perimetre_plein_bord' => ['parameter_id' => 324204],
        'rayon_hydraulique' => ['parameter_id' => 324205],
        'debit_plein_bord' => ['parameter_id' => 158685],
        'puissance' => ['parameter_id' => 158686],
        'indice_talweg' => ['parameter_id' => 415860],
        'indice_sinuosite' => ['parameter_id' => 415863],
        'indice_cem' => ['parameter_id' => 415864],
        'indice_rugosite' => ['parameter_id' => 415867],
        'indice_protection_berge' => ['parameter_id' => 415869],
        'indice_artificialisation_note' => ['parameter_id' => 158687],
        'indice_artificialisation_classe' => ['parameter_id' => 158688],
        'indice_pression_RG' => ['parameter_id' => 415870],
        'indice_pression_RD' => ['parameter_id' => 415871],
        'indice_pression' => ['parameter_id' => 159907],
        'indice_resilience_erodabilite_berge' => ['parameter_id' => 415890],
        'indice_resilience_potentiel_apport_solide' => ['parameter_id' => 415933],
        'indice_resilience_emprise' => ['parameter_id' => 415934],
        'indice_resilience' => ['parameter_id' => 158695]
    ]
];


$P['reh_calculations'] = [
    'id' => 'reh_calculations',
    'title' => 'Calcul des altérations REH',
    'path' => '/reh_calculations.php',
    'args' =>
    [],
    'return_args' =>
    [
        'a_elevlign' => ['parameter_id' => 186],
        'a_modprofl' => ['parameter_id' => 197],
        'a_redgranu' => ['parameter_id' => 198],
        'a_destsubs' => ['parameter_id' => 199],
        'a_modproft' => ['parameter_id' => 200],
        'a_colmsubs' => ['parameter_id' => 201],
        'a_unifberg' => ['parameter_id' => 210],
        'a_redlinbe' => ['parameter_id' => 211],
        'a_unifripi' => ['parameter_id' => 212],
        'altc_lignd' => ['parameter_id' => 187],
        'altc_lit' => ['parameter_id' => 203],
        'altc_berg' => ['parameter_id' => 213]
    ]
];


$P['t2bv_troncon_calculations'] = [
    'id' => 't2bv_troncon_calculations',
    'title' => 'Calcul des indicateurs des tronçons de tête de bassin',
    'path' => '/t2bv_troncon_calculations.php',
    'args' =>
    [
        'surf_bv_ref' => [
            'type' => 'numeric',
            'parameter' => 'Taille du bassin de référence',
            'default_value' => 60.4
        ],
        'q2_bv_ref' => [
            'type' => 'numeric',
            'parameter' => 'Débit de crue biennale  du bassin de référence',
            'default_value' => 9.6
        ]
    ],
    'return_args' =>
    [
        'tr_lg_ppal' => ['parameter_id' => 315284],
        'lg_nonent' => ['parameter_id' => 673274],
        'lgnonnon' => ['parameter_id' => 673327],
        'rugos_note' => ['parameter_id' => 315289],
        'q2_extrapo' => ['parameter_id' => 315290],
        'puis_extra' => ['parameter_id' => 315291],
        'lpb_theo' => ['parameter_id' => 315292],
        'prof_theo' => ['parameter_id' => 315293],
        'rap_lp_the' => ['parameter_id' => 315294],
        'surfm_theo' => ['parameter_id' => 315295],
        'largb_theo' => ['parameter_id' => 315302],
        'permo_theo' => ['parameter_id' => 315305],
        'q2_theo' => ['parameter_id' => 315303],
        'puis_theo' => ['parameter_id' => 315304],
        'pot_ap_sol' => ['parameter_id' => 315321],
        'potaps_ind' => ['parameter_id' => 902736],
        'empr_dispo' => ['parameter_id' => 315307],
        'emprd_ind' => ['parameter_id' => 902737],
        'eroda_berg' => ['parameter_id' => 315316],
        'erodb_ind' => ['parameter_id' => 902735],
        'puis_spe' => ['parameter_id' => 315306],
        'puissp_not' => ['parameter_id' => 902733],
        'puissp_ind' => ['parameter_id' => 902734],
        'indiresnot' => ['parameter_id' => 902738]
    ]
];


$P['t2bv_troncon_detailsequence_calculations'] = [
    'id' => 't2bv_troncon_detailsequence_calculations',
    'title' => 'Calcul des indicateurs des tronçons de tête de bassin, détail séquences',
    'path' => '/t2bv_troncon_detailsequence_calculations.php',
    'args' =>
    [],
    'return_args' =>
    []
];

$P['river_theorics_calculations'] = [
    'id' => 'river_theorics_calculations',
    'title' => 'Calcul de données théoriques sur cours d eau',
    'path' => '/river_theorics_calculations.php',
    'args' =>
    [
        'surf_bv_ref' => [
            'type' => 'numeric',
            'parameter' => 'Taille du bassin de référence',
            'default_value' => 60.4
        ],
        'q2_bv_ref' => [
            'type' => 'numeric',
            'parameter' => 'Débit de crue biennale  du bassin de référence',
            'default_value' => 9.6
        ]
    ],
    'return_args' =>
    [
        'degres_irregularite_berge' => ['parameter_id' => 962737],
        'n0' => ['parameter_id' => 962742],
        'n1' => ['parameter_id' => 962743],
        'n2' => ['parameter_id' => 962744],
        'n3' => ['parameter_id' => 962745],
        'n4' => ['parameter_id' => 962746],
        'm' => ['parameter_id' => 962747],
        'K' => ['parameter_id' => 962748],
        'classe_K' => ['parameter_id' => 962749],
        'surface_plein_bord' => ['parameter_id' => 962753],
        'perimetre_plein_bord' => ['parameter_id' => 962754],
        'rayon_hydraulique' => ['parameter_id' => 962755],
        'debit_plein_bord' => ['parameter_id' => 962756],
        'puissance' => ['parameter_id' => 962757],
        'q2_extrapo' => ['parameter_id' => 962758],
        'puis_extra' => ['parameter_id' => 962759],
        'lpb_theo' => ['parameter_id' => 962760],
        'prof_theo' => ['parameter_id' => 962761],
        'rap_lp_the' => ['parameter_id' => 962762],
        'surfm_theo' => ['parameter_id' => 962763],
        'largb_theo' => ['parameter_id' => 962764],
        'permo_theo' => ['parameter_id' => 962765],
        'q2_theo' => ['parameter_id' => 962775],
        'puis_theo' => ['parameter_id' => 962766]
    ]
];


$P['river_theorics_calculations_lit_remeandre'] = [
    'id' => 'river_theorics_calculations_lit_remeandre',
    'title' => 'Calcul de données théoriques sur cours d eau',
    'path' => '/river_theorics_calculations_lit_remeandre.php',
    'args' =>
    [
        'surf_bv_ref' => [
            'type' => 'numeric',
            'parameter' => 'Taille du bassin de référence',
            'default_value' => 60.4
        ],
        'q2_bv_ref' => [
            'type' => 'numeric',
            'parameter' => 'Débit de crue biennale  du bassin de référence',
            'default_value' => 9.6
        ]
    ],
    'return_args' =>
    [
        'degres_irregularite_berge' => ['parameter_id' => 966567],
        'n0' => ['parameter_id' => 966572],
        'n1' => ['parameter_id' => 966573],
        'n2' => ['parameter_id' => 966574],
        'n3' => ['parameter_id' => 966575],
        'n4' => ['parameter_id' => 966576],
        'm' => ['parameter_id' => 966577],
        'K' => ['parameter_id' => 966578],
        'classe_K' => ['parameter_id' => 966579],
        'surface_plein_bord' => ['parameter_id' => 966583],
        'perimetre_plein_bord' => ['parameter_id' => 966584],
        'rayon_hydraulique' => ['parameter_id' => 966585],
        'debit_plein_bord' => ['parameter_id' => 966586],
        'puissance' => ['parameter_id' => 966588],
        'q2_extrapo' => ['parameter_id' => 966589],
        'puis_extra' => ['parameter_id' => 966589],
        'lpb_theo' => ['parameter_id' => 966590],
        'prof_theo' => ['parameter_id' => 966591],
        'rap_lp_the' => ['parameter_id' => 966592],
        'surfm_theo' => ['parameter_id' => 966593],
        'largb_theo' => ['parameter_id' => 966594],
        'permo_theo' => ['parameter_id' => 966596],
        'q2_theo' => ['parameter_id' => 966595],
        'puis_theo' => ['parameter_id' => 966597]
    ]
];




$P['river_theorics_calculations_lit_restaure'] = [
    'id' => 'river_theorics_calculations_lit_restaure',
    'title' => 'Calcul de données théoriques sur cours d eau',
    'path' => '/river_theorics_calculations_lit_restaure.php',
    'args' =>
    [
        'surf_bv_ref' => [
            'type' => 'numeric',
            'parameter' => 'Taille du bassin de référence',
            'default_value' => 60.4
        ],
        'q2_bv_ref' => [
            'type' => 'numeric',
            'parameter' => 'Débit de crue biennale  du bassin de référence',
            'default_value' => 9.6
        ]
    ],
    'return_args' =>
    [
        'degres_irregularite_berge' => ['parameter_id' => 967056],
        'n0' => ['parameter_id' => 967061],
        'n1' => ['parameter_id' => 967062],
        'n2' => ['parameter_id' => 967063],
        'n3' => ['parameter_id' => 967064],
        'n4' => ['parameter_id' => 967065],
        'm' => ['parameter_id' => 967066],
        'K' => ['parameter_id' => 967067],
        'classe_K' => ['parameter_id' => 967068],
        'surface_plein_bord' => ['parameter_id' => 967072],
        'perimetre_plein_bord' => ['parameter_id' => 967073],
        'rayon_hydraulique' => ['parameter_id' => 967074],
        'debit_plein_bord' => ['parameter_id' => 967075],
        'puissance' => ['parameter_id' => 967076],
        'q2_extrapo' => ['parameter_id' => 967077],
        'puis_extra' => ['parameter_id' => 967078],
        'lpb_theo' => ['parameter_id' => 967079],
        'prof_theo' => ['parameter_id' => 967080],
        'rap_lp_the' => ['parameter_id' => 967081],
        'surfm_theo' => ['parameter_id' => 967082],
        'largb_theo' => ['parameter_id' => 967083],
        'permo_theo' => ['parameter_id' => 967085],
        'q2_theo' => ['parameter_id' => 967084],
        'puis_theo' => ['parameter_id' => 967086]
    ]
];



$P['select_linked_object_from_geom'] = [
    'id' => 'select_linked_object_from_geom',
    'title' => 'Selectionner des objets liés depuis la géométrie (selection FLUO)',
    'path' => '/select_linked_object_from_geom.php',
    'args' =>
    [
        'linked_geoobjecttype_id' =>
        [
            'type' => 'sysma_parameter',
            'parameter' => 'Type objet lié',
            'parameters_list' =>
            [
                ['id_parameter' => '158507', 'parameter' => 'Séquences T2BV']
            ]
        ],
        'linked_parameter_id_to_update' =>
        [
            'type' => 'sysma_parameter',
            'parameter' => 'Champ à renseigner',
            'parameters_list' =>
            [
                ['id_parameter' => '158512', 'parameter' => 'ID tronçons des séquences T2BV à mettre à jour']
            ]
        ],
        'buffer' => [
            'type' => 'int',
            'parameter' => 'Taille du buffer de selection',
            'default_value' => 1
        ]
    ],
    'return_args' =>
    [
        'ids_sysma_object_list' => ['parameter_id' => 'ids_sysma_object_list']
    ]
];




$P['select_obstacles_from_geom'] = [
    'id' => 'select_obstacles_from_geom',
    'title' => 'Selectionner des obstacles depuis la géométrie',
    'path' => '/select_obstacles_from_geom.php',
    'args' =>
    [
        'buffer' => [
            'type' => 'int',
            'parameter' => 'Taille du buffer de selection',
            'default_value' => 10
        ]
    ],
    'return_args' =>
    [
        'nb_obstacl' => ['parameter_id' => 288],
        'nb_obsprim' => ['parameter_id' => 622440],
        'haut_chut' => ['parameter_id' => 289],
    ]
];

$P['select_plansdeau_from_geom'] = [
    'id' => 'select_plansdeau_from_geom',
    'title' => 'Selectionner des plans d\'eau depuis la géométrie',
    'path' => '/select_plansdeau_from_geom.php',
    'args' =>
    [
        'buffer' => [
            'type' => 'int',
            'parameter' => 'Taille du buffer de selection',
            'default_value' => 10
        ]
    ],
    'return_args' =>
    [
        'nb_pdo_con' => ['parameter_id' => 287],
    ]
];


$P['select_litartificialise_from_geom'] = [
    'id' => 'select_litartificialise_from_geom',
    'title' => 'Calculer le linéaire de lit artificialisé depuis la géométrie',
    'path' => '/select_litartificialise_from_geom.php',
    'args' =>
    [
        'buffer' => [
            'type' => 'int',
            'parameter' => 'Taille du buffer de selection',
            'default_value' => 10
        ]
    ],
    'return_args' =>
    [
        'lin_litart' => ['parameter_id' => 285],
    ]
];



$P['select_bergedegradee_from_geom'] = [
    'id' => 'select_bergedegradee_from_geom',
    'title' => 'Calculer le linéaire de  berges dégradées depuis la géométrie',
    'path' => '/select_bergedegradee_from_geom.php',
    'args' =>
    [
        'buffer' => [
            'type' => 'int',
            'parameter' => 'Taille du buffer de selection',
            'default_value' => 10
        ]
    ],
    'return_args' =>
    [
        'lin_bergde' => ['parameter_id' => 286],
    ]
];



$P['select_abreuvoirsauvage_from_geom'] = [
    'id' => 'select_abreuvoirsauvage_from_geom',
    'title' => 'Calculer le nombre d\'abreuvoirs sauvages depuis la géométrie',
    'path' => '/select_abreuvoirsauvage_from_geom.php',
    'args' =>
    [
        'buffer' => [
            'type' => 'int',
            'parameter' => 'Taille du buffer de selection',
            'default_value' => 10
        ]
    ],
    'return_args' =>
    [
        'nb_ab_sauv' => ['parameter_id' => 284],
    ]
];



$P['generate_geom_from_linked_objects'] = [
    'id' => 'generate_geom_from_linked_objects',
    'title' => 'Générer une géométrie unique depuis des objets liés',
    'path' => '/generate_geom_from_linked_objects.php',
    'args' =>
    [
        'linked_parameter_id' =>
        [
            'type' => 'sysma_parameter',
            'parameter' => 'Paramètre contenant le lien',
            'parameters_list' =>
            [
                ['id_parameter' => '158512', 'parameter' => 'Identifiant du tronçon']
            ]
        ],
        'snap_dist' => [
            'type' => 'int',
            'parameter' => 'Distance max pour l\'accrochage des objets entre eux',
            'default_value' => 20
        ]
    ],
    'return_args' =>
    [
        'union_geom' => ['parameter_id' => 'geoobject_geom']
    ]
];





$P['generate_t2bvtroncon_from_sequenceid'] = [
    'id' => 'generate_t2bvtroncon_from_sequenceid',
    'title' => 'Générer le tronçon depuis l\'id séquence',
    'path' => '/generate_t2bvtroncon_from_sequenceid.php',
    'args' =>
    [
        'snap_dist' => [
            'type' => 'int',
            'parameter' => 'Distance max pour l\'accrochage des objets entre eux',
            'default_value' => 10
        ]
    ],
    'return_args' => [
        'res_object' => ['parameter_id' => 'new_object']
    ]
];



// for troncon tdBV 
$P['get_watershed_area_from_linegeom_troncon'] = $P['get_watershed_area_from_linegeom_v02'];
$P['get_watershed_area_from_linegeom_troncon']['return_args'] = ['surface_bv' => ['parameter_id' => 315288]];

// same for sequences

$P['get_watershed_area_from_linegeom_seq'] = $P['get_watershed_area_from_linegeom_v02'];
$P['get_watershed_area_from_linegeom_seq']['return_args'] = ['surface_bv' => ['parameter_id' => 926073]];

// same for lit atif
$P['get_watershed_area_from_linegeom_lit_recharge'] = $P['get_watershed_area_from_linegeom_v02'];
$P['get_watershed_area_from_linegeom_lit_recharge']['return_args'] = ['surface_bv' => ['parameter_id' => 962736]];

// same for lit remeandre
$P['get_watershed_area_from_linegeom_lit_remeandre'] = $P['get_watershed_area_from_linegeom_v02'];
$P['get_watershed_area_from_linegeom_lit_remeandre']['return_args'] = ['surface_bv' => ['parameter_id' => 966566]];

// same for lit restaure
$P['get_watershed_area_from_linegeom_lit_restaure'] = $P['get_watershed_area_from_linegeom_v02'];
$P['get_watershed_area_from_linegeom_lit_restaure']['return_args'] = ['surface_bv' => ['parameter_id' => 967055]];



$tabP = [
    // debut declaration lit mineur process EV
    '75656847' =>
    [
        'lit-mineur-calculations' => $P['lit_mineur_calculations']
    ],
    // fin declaration lit mineur process EV


    '315280' => // troncon de lit minieur TdBV
    [
        'select-linked-object-from-geom' => $P['select_linked_object_from_geom'],
        'generate-geom-from-linked-objects' => $P['generate_geom_from_linked_objects'],
        'altitude-and-slope' => $P['altitude_and_slope_troncon'],
        'get-watershed-area-from-linegeom' => $P['get_watershed_area_from_linegeom_troncon'],
        't2bv-troncon-calculations' => $P['t2bv_troncon_calculations']
    ],
    '158507' => // sequence de lit minieur TdBV
    [
        't2bv-calculations' => $P['t2bv_calculations'],
        'generate_t2bvtroncon_from_sequenceid' => $P['generate_t2bvtroncon_from_sequenceid'],
        'get-watershed-area-from-linegeom-seq' => $P['get_watershed_area_from_linegeom_seq'],
    ],
    '39' =>
    [
        'reh_calculations' => $P['reh_calculations'],
        'select_obstacles_from_geom' => $P['select_obstacles_from_geom'],
        'select_plansdeau_from_geom' => $P['select_plansdeau_from_geom'],
        'select_litartificialise_from_geom' => $P['select_litartificialise_from_geom'],
        'select_bergedegradee_from_geom' => $P['select_bergedegradee_from_geom'],
        'select_abreuvoirsauvage_from_geom' => $P['select_abreuvoirsauvage_from_geom'],
        'altitude_and_slope_reh' => $P['altitude_and_slope_reh'],
    ],
    '20' =>
    [
        'altitude-and-slope' => $P['altitude_and_slope_lit_recharge'],
        'get-watershed-area-from-linegeom' => $P['get_watershed_area_from_linegeom_lit_recharge'],
        'river_theorics_calculations' => $P['river_theorics_calculations']
    ],
    '21' =>
    [
        'altitude-and-slope' => $P['altitude_and_slope_lit_remeandre'],
        'get-watershed-area-from-linegeom' => $P['get_watershed_area_from_linegeom_lit_remeandre'],
        'river_theorics_calculations' => $P['river_theorics_calculations_lit_remeandre']
    ],
    '22' =>
    [
        'altitude-and-slope' => $P['altitude_and_slope_lit_restaure'],
        'get-watershed-area-from-linegeom' => $P['get_watershed_area_from_linegeom_lit_restaure'],
        'river_theorics_calculations' => $P['river_theorics_calculations_lit_restaure']
    ]
];






$GLOBALS['PROCESSES'] = $tabP;
