<?php

/*****************************************************************************
 * !!! WARNING !!!
 * DO NOT WRITE/ADD ANY SECRET DOWN IN THIS FILE!!!
 * BECAUSE THIS FILE IS PART OF THE SOURCE AND THEREFORE CAN/WILL BE COMMITED TO PUBLIC AUDIENCE
 *
 * This is a template access configuration file.
 * You must have a copy this file in the ./private folder where your Sysma App acess secrets have to be.
 ****************************************************************************/

// Sysma app access template secret consts
define('DBUSER', 'your_sysma_db_user');
define('DBPWD', 'your_sysma_db_password');
define('DBNAME', 'your_sysma_db_name');
define('DBHOST', 'localhost');
define('DBPORT', 5432);
define('SCHEMA', 'sysma');

// MOULINETTES POUR LES COUCHES
define('MOULDBPWD', DBPWD);
define('MOULDBUSER', DBUSER);
define('MOULDBNAME', DBNAME);
define('MOULDBHOST', DBHOST);
define('MOULDBPORT', DBPORT);
define('MOULSCHEMA', 'sysma_export_layer');

//
// PHOTO
//
// Alternative 1 : PIWIGO PHOTOTHEQUE
// Database infos of the piwigo instance :
define('PHOTO_PWG_MODE', 'off');
define('DBPHPWD', null);
define('DBPHUSER', null);
define('DBPHNAME', null);
define('DBPHHOST', null);
define('DBPHPORT', 3306);
define('PWGURL', null);

// Alternative 2 : UPLOAD FROM SYSMA INTERFACE
define('PHOTO_UPLOAD_MODE', 'on');
define('PHOTO_UPLOAD_PATH', 'upload'); // path of the upload folder, relative to the ROOT_URI/public/ // web user (www-data) must be the owner of this folder, and the rights must be set to -777



// DB CADASTRE
define('CADDBPWD', MOULDBPWD);
define('CADDBUSER', MOULDBUSER);
define('CADDBNAME', DBNAME);
define('CADDBHOST', DBHOST);
define('CADDBPORT', DBPORT);


// WEBHOOCK_LOGS 
// (uncomment and adapt values)
// define('WEBHOOKURL_LOGS_DISCORD', 'your_discord_webhoock_url');  // Discord
// END WEBHOOCK_LOGS


// WEBSITE_ANALYTICS_CODE
// Add your JS analytics code here (Matomo...)

// END WEBSITE_ANALYTICS_CODE