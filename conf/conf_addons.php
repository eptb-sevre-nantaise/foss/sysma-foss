<?php

// Report sheet

$R['synthese_sequence_tbv'] = [
    'id' => 'synthese_sequence_tbv',
    'url' => 'synthese-sequence-tbv',
    'title' => 'Fiche séquence',
    'icon' => 'glyphicon glyphicon-list-alt',
    'twig_file_uniq' => 'addons/TbvSequenceSynthese.twig',
    'twig_file_mutli' => 'addons/TbvSequenceSyntheses.twig',
    'twig_file_content' => 'addons/TbvSequenceContent.twig',
    'processes' => [
    // 't2bv-calculations' => $P['t2bv_calculations']
    ]
];

$R['synthese_troncon_tbv'] = [
    'id' => 'synthese_troncon_tbv',
    'url' => 'synthese-troncon-tbv',
    'title' => 'Fiche tronçon',
    'icon' => 'glyphicon glyphicon-list-alt',
    'twig_file_uniq' => 'addons/TbvTronconSynthese.twig',
    'twig_file_mutli' => 'addons/TbvTronconSynthese.twig',
    'twig_file_content' => 'addons/TbvTronconContent.twig',
    'processes' => [
        't2bv-troncon-detailsequence-calculations' => $P['t2bv_troncon_detailsequence_calculations']
    ]
];


$tabR = [
    '158507' =>
    [
        'synthese-sequence-tbv' => $R['synthese_sequence_tbv']
    ],
    '315280' =>
    [
        'synthese-troncon-tbv' => $R['synthese_troncon_tbv']
    ]
];


$GLOBALS['ADDONS'] = $tabR;
