<?php

////// --- PROD READY? TRACING CONFIGURATION BEGIN ---//////
// VERY USEFUL DEV SQL ERRORS LOG FILE :
// NEW FEATURE in testing preriod : 
//     - Try activated all time in prod 
define('DEBUG_LOG_SQL_QUERIES_ERRORS', true); // Could be enable all times (file is cleared every  DEBUG_LOG_SQL_QUERIES_ERRORS_MAX_LINES)
define('DEBUG_LOG_SQL_QUERIES_ERRORS_MAX_LINES', 10000);  // clear sql_queries_errors.log (keep relative high value to prevent spamming email ex: 10000 )
define('DEBUG_LOG_SQL_QUERIES_ERRORS_NB_EMAIL_SENT_PER_DAY', 1);    // limit sending mail per day to this value (warning keep small value )
//                                                                  // NO SENDING MAIL IF : Set to 0
//                                                                  // NO SENDING MAIL IF : DEBUG_DEV_MAIL  (in sysma_conf table) is not defined or set to null
//                                                                  // BASED ON : sql_queries_errors.log content (remember this file could be clear every DEBUG_LOG_SQL_QUERIES_ERRORS_MAX_LINES)
////// --- PROD READY? TRACING CONFIGURATION END ---//////

////// --- TEMPORARY DEBUG CONFIGURATION BEGIN ---//////
//// CLIENT SIDE FOR TEMPORARY PHP DEBUGING :
define('DEBUG', false); // main debug/log level. Enable main debug log messages on the client.

//// FILES LOGGER FOR TEMPORARY DEBUG :
//        // CAUTION! Do NOT enable this file logger unless you know what you are doing and how to adapt it to your needs.
//        // Since this may not work on your server due to access privilege restriction.
//        // Log files as "debug.log" are located in the log folder of your-sysma-install-path.
//        //   ie. /your-sysma-install-path/log/debug.log 
//        // It can be watched in a terminal like this:
//        //   >tail -f /your-sysma-install-path/log/debug.log 
define('DEBUG_ENABLE_LOG_FILE', false); // Enable logs to file
define('DEBUG_ENABLE_LOG_FILE_MAX_LINES', 10000); // clear logs file when the number of lines exceeds this limit
// Options, can stay to true :
define('DEBUG_LOG_ALL_SQL_QUERIES', true); // Enable also sql logging
define('DEBUG_SPLIT_GENERAL_LOGS_AND_SQL_LOGS_IN_2_FILES', true); // Sql logs will be in log/sql-queries.log

//// --- TEMPORARY DEBUG CONFIGURATION END ---////


//// --- ONLY ON ONE PARTICULATY DEV BASE FOSS CONFIGURATION BEGIN ---////
// ONLY ON DEV BASE FOSS !!! 
// ONLY AVAILABLE ON ONE PARTICULARY DEV BASE FOSS (need private pg function for tracing for profiling)
// For security reason DEBUG_LVL2 must only be set to true on dev/test DB ONLY! It may reveal too much DB infos on the client side.
// At the moment, DEBUG_LVL2 is used for logging/profiling a sequence of PG queries (using PG features/special tables/funcs IIRC). See utils/debug.php to know more. 
// uncomment this line below on dev_foss_base if needed
define('DEBUG_LVL2', false);  // debug/log level 2. Enable level 2 debug log messages on the client.
// ONLY ON DEV BASE FOSS !!! END
//// --- ONLY ON ONE PARTICULATY DEV BASE FOSS CONFIGURATION END ---////



// CAUTION!!! if debug is set before creating pdo with "new SysmaPDO(...' it may REVEAL secrets if anything goes wrong.
// if (DEBUG) {
//     error_reporting(E_ALL);
//     ini_set("display_errors", 1);
// }

$docRoot = $_SERVER['DOCUMENT_ROOT'];
// this to deal with DOCUMENT_ROOT ending with / or not since it depends on infra/apache path policy.
if (substr($docRoot, strlen($docRoot) - 1) == '/') {
    $subDirLen = strlen('public/');
} else {
    $subDirLen = strlen('public');
}

define('ROOT_URI', substr($docRoot, 0, -$subDirLen)); //'/var/www/sysma/dev/dev-ar/sysma-dev-eptbsn/');

$CONF_ACCES_PRIVATE_FILE = 'conf/private/conf_access.private.php';
if (file_exists(ROOT_URI . $CONF_ACCES_PRIVATE_FILE)) {
    require_once(ROOT_URI . $CONF_ACCES_PRIVATE_FILE);
} else {
    echo "ERR | " . ROOT_URI . $CONF_ACCES_PRIVATE_FILE . " file not found!";
    echo "INFO| It means the Sysma access conf file has not been set properly.";
    echo "      Run the Sysma installer script.";
    echo "      Alternatively, if you know what you're doing, ";
    echo "      you can create " . $CONF_ACCES_PRIVATE_FILE . " from it's ";
    echo "      template file: 'conf/conf_access.template.php''";
    echo "      then edit manualy the private one you created.";
    die(-1);
}
require_once(dirname(__DIR__) . '/utils/utils.php');
require_once(dirname(__DIR__) . '/Sysma/Classes/SysmaPDOStatement.php');
require_once(dirname(__DIR__) . '/Sysma/Classes/SysmaPDO.php');

// chargement des constantes via la table sysma_conf

// $app['pdo'] = new PDO('pgsql:dbname=' . DBNAME . ';host=' . DBHOST . ';port=' . DBPORT . '', DBUSER, DBPWD, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING, PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC));
$app['pdo'] = new SysmaPDO('pgsql:dbname=' . DBNAME . ';host=' . DBHOST . ';port=' . DBPORT . '', DBUSER, DBPWD, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING, PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC));

if (DEBUG) {
    error_reporting(E_ALL);
    ini_set("display_errors", 1);
}


$conf = loadAppParameters($app);
foreach ($conf as $p) {
    define($p['alias'], $p['value']);
}



///////////////////////////////////////////////////////////////////
// TODO WIP Continuer la nouvelle organisations et initialisation des globals vars de l'App
// 1) load App default global vars and overwrite them with custom ones if requested
require(ROOT_URI . 'conf/conf_app_default.php');
// 2) execute specifi codes with some of these global vars here
set_time_limit(PHP_SET_TIME_LIMIT);
ini_set('memory_limit', PHP_MEMORY_LIMIT);

// get the version
require_once(ROOT_URI . 'conf/conf_app_version.php');
///////////////////////////////////////////////////////////////////

if (DEBUG_ENABLE_LOG_FILE) {
    Debug::initLogFile();
    
}

if (DEBUG_LOG_SQL_QUERIES_ERRORS){
    Debug::initLogSqlErrorFile();
}



//require(ROOT_URI . 'conf/conf_app_user.php');
//require(ROOT_URI . 'conf/conf_app_user_eptbsn.php');  //  juste pour test sysma eptbsn
