<?php

/*****************************************************************************
 * This is a template addons configuration file.
 * 1) You must have a copy this file in the current folder and rename to conf_addons.php.
 * 2) Modify conf_addons.php with proper configuration
 ****************************************************************************/

// Report sheet

// fiche synthèse lit mineur E&V
$R['synthese_lit_mineur'] = [
    'id' => 'synthese_lit_mineur',
    'url' => 'synthese-lit-mineur',
    'title' => 'Fiche lit mineur',
    'icon' => 'glyphicon glyphicon-list-alt',
    'twig_file_uniq' => 'addons/LitMineurSynthese.twig',
    'twig_file_mutli' => 'addons/LitMineurSyntheses.twig',
    'twig_file_content' => 'addons/LitMineurContent.twig',
    'processes' => [
        'lit-mineur-calculations' => $P['lit_mineur_calculations']
    ]
];
// fin fiche synthèse lit mineur E&V

$R['synthese_sequence_tbv'] = [
    'id' => 'synthese_sequence_tbv',
    'url' => 'synthese-sequence-tbv',
    'title' => 'Fiche séquence',
    'icon' => 'glyphicon glyphicon-list-alt',
    'twig_file_uniq' => 'addons/TbvSequenceSynthese.twig',
    'twig_file_mutli' => 'addons/TbvSequenceSyntheses.twig',
    'twig_file_content' => 'addons/TbvSequenceContent.twig',
    'processes' => [
    // 't2bv-calculations' => $P['t2bv_calculations']
    ]
];

$R['synthese_troncon_tbv'] = [
    'id' => 'synthese_troncon_tbv',
    'url' => 'synthese-troncon-tbv',
    'title' => 'Fiche tronçon',
    'icon' => 'glyphicon glyphicon-list-alt',
    'twig_file_uniq' => 'addons/TbvTronconSynthese.twig',
    'twig_file_mutli' => 'addons/TbvTronconSynthese.twig',
    'twig_file_content' => 'addons/TbvTronconContent.twig',
    'processes' => [
        't2bv-troncon-detailsequence-calculations' => $P['t2bv_troncon_detailsequence_calculations']
    ]
];


$tabR = [
    // fiche synthese lit mineur EV
    '75656847' =>
    [
        'synthese-lit-mineur' => $R['synthese_lit_mineur']
    ],
    // fin fiche synthese lit mineur EV
    '158507' =>
    [
        'synthese-sequence-tbv' => $R['synthese_sequence_tbv']
    ],
    '315280' =>
    [
        'synthese-troncon-tbv' => $R['synthese_troncon_tbv']
    ]
];


$GLOBALS['ADDONS'] = $tabR;
