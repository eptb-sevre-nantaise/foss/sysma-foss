Dépot d'origine, pour ajout de tickets : https://gitlab.sevre-nantaise.com/eptbsn/sysma-foss/



 |  |  |    |||
| ------ | ------ | ------ |------ |------ |
![image](https://sysma.io/img/sysma_tablette.jpg)|   ![image](https://sysma.io/img/sysma_001.png) | ![image](https://sysma.io/img/sysma_002_dico.png)| ![image](https://sysma.io/img/sysma_005_historique.png)|  ![image](https://sysma.io/img/sysma_003_saisie.png)|




# [Sysma](https://sysma.io/)

[Sysma](https://sysma.io/) est un webSIG open source créé par l'Etablissement Public du Bassin de la Sèvre Nantaise. 

Ses principales caractéristiques sont : 
* Un module dictionnaire flexible et personnalisable, permettant de décrire tout type d'objets (géographiques et non géographiques) et de travaux (ou actions) associés
* Une interface cartographique simplifiée facilitant la saisie d'informations par des utilisateurs non SIGistes
* La possibilité de fonctionner en mode "hors ligne" sur tablette ou smartphone sur le terrain
* Le suivi dans le temps des objets, de leurs descripteurs et des travaux associés
* Une gestion fine des droits des utilisateurs
* Des dépendances techniques limitées facilitant le déploiement et la maintenance de l'outil
* La possibilité d'ajouter des extensions (automatisations et pages bilans métiers)

Sysma est notamment utilisé par des structures gestionnaires de bassin versant, pour le suivi des programmations et travaux liés à l'eau et aux milieux aquatiques, mais son dictionnaire peut être adapté à toute thématique métier.

## Tutoriel d'installation rapide [ici &#10148;](https://gitlab.sevre-nantaise.com/eptbsn/sysma-foss/-/wikis/Tutoriel-d'installation-de-Sysma-sur-AlwaysData)



## Changelog
<details>

<summary>Cliquer pour voir le détail</summary>

### Version 4.1 (août 2023):
* Cartes imprimables
* Analyses thématiques sur les couches travaux
* Rotation automatique des photos
* Export Géopackage
* Chapitrage des paramètres actions
* Fonction de zoom auto sur l'étendue des couches
* Nouveau type de paramètre hyperlien
* Admin : historique des dernières modifications dans la base (global ou par user)
* Désactivation du load on pan lors de l'édition des objets
* Corrections mineures (liste complète : https://gitlab.sevre-nantaise.com/eptbsn/sysma-foss/-/merge_requests/65)

### Version 4 (avril 2023):
* Sysma offline

### Version 3.6 (decembre 2022):
* ajout des couches observatoire
* fix photo afficahe du status pendant travaux
* ajout affichage contenance cadastrale + surface géométrie

### Version 3.5 (octobre 2022):
* correction de bugs mineurs

### Version 3.4 (aout 2022):
* ajout des couches observatoire

### Version 3.3 (juillet 2022):
* import/export de dictionnaire
* fonctionnalités de suppression des éléments de dictionnaire
* divers debug et améliorations

</details>


## Pré-requis
* Système exploitation: linux ou assimilé
* Php : >= 5.6 et < 8.0
* PostgreSQL >= 9.6
* PostGIS >= 3.1
* git
* npm

_Notes:_
 - _git n'est pas nécessaire pour le fonctionnement de Sysma. git est utilisé dans cette procédure d'installation et fortement recommander pour bénéficier des mises à jour._
 - _npm est utilisé pour installer automatiquement _Vite_ et pour compiler les codes Vue. Si vous n'avez pas npm sur votre serveur, il est sans doute possible d'utiliser manuellement yarn à la place de npm (non testé)_


## Téléchargement de Sysma

Dans un terminal et dans le répertoire racine (vide) de votre site internet dédié pour Sysma,

lancer la commande:

> **git clone https://gitlab.sevre-nantaise.com/eptbsn/sysma-foss.git .**

_Note: le code source est aussi disponible ici: [https://gitlab.sevre-nantaise.com/eptbsn/sysma-foss](https://gitlab.sevre-nantaise.com/eptbsn/sysma-foss )_

## Installation

Sysma est composé d'une base de données Sysma et de l'application Sysma.


### 1) Configurer la base de donnée Sysma.

Créer (ou demander) le compte et la base sysma postgreSQL pour l'application Sysma.

#### 1.1) Administrateur postgres : créer un user pour sysma sur votre serveur postgres 
``` sql
  -- Ajout du user pour sysma sur votre serveur postgres 
  -- modifier [nom_de_votre_user_sysma] , exemple : sysma_user
  -- modifier '[super_mot_de_passe]' ,  exemple : 'IOPLSkenzbqyvhsb_A_VOUS_DE_CREER_VOTRE_PROPRE_MOT_DE_PASS_566890324*iuffeegZG!698475' (attention , à vous de créer votre propose mot de passe )

  CREATE ROLE [nom_de_votre_user_sysma] LOGIN  
  ENCRYPTED PASSWORD '[super_mot_de_passe]'
  NOSUPERUSER INHERIT NOCREATEDB NOCREATEROLE NOREPLICATION;
``` 

#### 1.2) Administrateur postgres : créer une base de données pour l'application sysma
``` sql
  -- ajout de la base de données
  -- modifier [nom_de_votre_base_de_donnee_sysma] , exemple sysma
  -- modifier [nom_de_votre_user_sysma] , exemple : sysma_user
  
  CREATE DATABASE [nom_de_votre_base_de_donnee_sysma]
  WITH OWNER = [nom_de_votre_user_sysma]
  ENCODING = 'UTF8';
  GRANT ALL ON DATABASE [nom_de_votre_base_de_donnee_sysma] TO [nom_de_votre_user_sysma];
  REVOKE ALL ON DATABASE [nom_de_votre_base_de_donnee_sysma] FROM public;
``` 

#### 1.3) Administrateur postgres  : Ajouter l'extension postgis dans la base de données sysma
``` sql
  -- se connecter sur la base sysma
 CREATE EXTENSION postgis
  SCHEMA public
``` 



### 2) Installer et configurer l'application Sysma.
_Pour la suite, voici les informations nécessaires:_
- _le nom de votre organisation et eventuellement son sigle, l'adresse de son site internet._
- _l'adresse internet ou IP du serveur postgreSQL. ex: localhost, votre_serveur.com_
- _le port du service postgreSQL sur ce serveur. ex: 5432_
- _le nom de la base de données crée pour Sysma_
- _le nom de l'utilisateur postgres et son mot de passe qui permettra à l'application Sysma de se connecter à la base Sysma_
- _le nom de compte administrateur/installeur de Sysma application que vous aurez choii ainsi que son mot de passe_



Dans un terminal et dans le répertoire racine (vide) de votre site internet dédié pour Sysma, lancer les commandes:

pour aller dans le dossier _install_ :

> **cd install**

rendre executable le script d'installation et de configuration de Sysma _install.sh_ :

> **chmod +x install.sh**

puis executer ce script :

> **./install.sh**

Suivre le guide d'installation en répondant aux questions posées à l'aide des informations nécessaires.

(voir exemple dans Etape 4 du [tutoriel d'installation](https://gitlab.sevre-nantaise.com/eptbsn/sysma-foss/-/wikis/Tutoriel-d'installation-de-Sysma-sur-AlwaysData))

A la fin de l'exécution du script d'installation, Sysma est configuré et opérationnel.

Vous devrez configurer le nom de domaine que vous souhaitez utiliser pour Sysma pour qu'il pointe vers /repertoire_racine/sysma/public

#### SSL - Forçage du https
Il est recommander de forcer le https pour Sysma.
Il est probable que vous puissiez le faire directement au niveau de votre infrastruture (voir avec l'administrateur de celle-ci).

Dans d'autres cas, et si votre infrastructure le permet, il est possible de forcer le https dans Sysma.
<details><summary>Voir comment</summary>

- Editer le fichier _.htaccess_ situé dans le répertoire _/public_.
- Décommenter ces 2 lignes (retirer les _#_ en début de ligne) :

```
#    RewriteCond %{HTTPS} !=on
#    RewriteRule .* https://%{HTTP_HOST}%{REQUEST_URI} [R=301,L]
```

</details>

## Mise à jour de sysma
Avant toute opération de mise à jour de Sysma, nous vous recommandons fortement d'effectuer une sauvegarde de votre base de données et de vos fichiers.

### 1) Mettre à jour les fichiers sources

Dans un terminal, lancer les commandes suivantes:
pour aller dans le dossier principal _sysma-foss_ :

> **cd sysma-foss**

pour mettre à jour la base de code :

> **git pull**


### 2) Effectuter les migrations au niveau de la base de données

Dans un terminal, lancer les commandes suivantes:
pour aller dans le dossier contenant les fichiers de migration _migrations_ :

> **cd migrations_**
puis executer ce script :

> **./migrate_up_to_date.sh**

## En cas problème

En cas problème lors de l'installation ou autres, merci de nous aider à corriger/améliorer en soumettant un ticket `[ici](https://gitlab.sevre-nantaise.com/eptbsn/sysma-foss/-/issues) qui décrit le problème ou en soumettant directement un correctif au code.


## Documentation

### Informations générales sur l'application

https://sysma.io

### Documentation

https://sysma.io/doc


### Forum utilisateurs, dépôts de tickets

https://gitlab.sevre-nantaise.com/eptbsn/sysma-tickets/-/issues

## Contributions

Créez un compte sur notre plateforme Gitlab pour ouvrir des tickets (signaler les bugs) ou proposer des améliorations du code : http://gitlab.sevre-nantaise.com/users/sign_in

## Auteurs

Sébastien RENOU et Antoine RIVIERE - EPTB Sèvre Nantaise.

Si vous utilisez l'outil ou si vous avez des questions, n'hésitez pas à nous contacter : https://www.sevre-nantaise.com/contact?contact=srenou

## Contributeurs

Alexandre RIVIERE, Antoine SIMON

## Licence

Ce projet est sous licence AGPL V3

## Disclaimer

Des consolidations du code et de la procédure d'installation de Sysma sont en cours, nous vous invitons à tester l'application. 

**Nous vous recommandons toutefois d'attendre l'ouverture officielle du code avant toute mise en production.**

