<?php

/*
 * comprend les fonctions de calcul des altérations REH
 */


/*
 * calcul une altération à partir des paramètres de base
 * arguments :
 * $alteration = parameter_id correspondant à une alteration REH
 * $tab = tableau de paramètres :
 *      $tab['parameter_id'] = $value
 */

function calculeREH($id_alteration, $tab)
{

    $res = null;



    if ($id_alteration == null or $tab == null) {

        die('Paramètres incomplets');
    }


    // Altération élevation de la ligne d'eau (186)
    /*
     * basé :
     * 
     * Diversité des faciès d'écoulement (étiage) (184)
     * 
     */ elseif ($id_alteration == '186') {

        switch ($tab['184']) {

            case 'faible':
                $res['lettres'] = 'moyenne';
                break;
            case 'moyenne':
                $res['lettres'] = 'faible';
                break;
            case 'forte':
                $res['lettres'] = 'pas d\'altération';
                break;
            case 'nulle':
                $res['lettres'] = 'forte';
                break;
            default:
                $res['lettres'] = null;
                break;
        }
    }

    // Altération modification du profil en long (197)
    /*
     * basé :
     * 
     * Sinuosité (188)
     * 
     */ elseif ($id_alteration == '197') {

        switch ($tab['188']) {

            case 'faible':
                $res['lettres'] = 'moyenne';
                break;
            case 'moyenne':
                $res['lettres'] = 'faible';
                break;
            case 'forte':
                $res['lettres'] = 'pas d\'altération';
                break;
            case 'nulle':
                $res['lettres'] = 'forte';
                break;
            default:
                $res['lettres'] = null;
                break;
        }
    }


    // Altération réduction de la granulométrie grossière (198)
    /*
     * basé :
     * 
     * Diversité de la granulométrie (191)
     * 
     */ elseif ($id_alteration == '198') {

        switch ($tab['191']) {

            case 'faible':
                $res['lettres'] = 'moyenne';
                break;
            case 'moyenne':
                $res['lettres'] = 'faible';
                break;
            case 'forte':
                $res['lettres'] = 'pas d\'altération';
                break;
            case 'nulle':
                $res['lettres'] = 'forte';
                break;
            default:
                $res['lettres'] = null;
                break;
        }
    }

    // Altération déstabilisation du substrat (199)
    /*
     * basé :
     * 
     * Stabilité du substrat (des fonds) (192)
     * 
     */ elseif ($id_alteration == '199') {

        switch ($tab['192']) {

            case 'faible':
                $res['lettres'] = 'moyenne';
                break;
            case 'moyenne':
                $res['lettres'] = 'faible';
                break;
            case 'forte':
                $res['lettres'] = 'pas d\'altération';
                break;
            case 'nulle':
                $res['lettres'] = 'forte';
                break;
            default:
                $res['lettres'] = null;
                break;
        }
    }

    // Altération modification du profil en travers (200)
    /*
     * basé :
     * 
     * Incision du lit (193)
     * 
     */ elseif ($id_alteration == '200') {

        switch ($tab['193']) {

            case 'faible':
                $res['lettres'] = 'faible';
                break;
            case 'moyenne':
                $res['lettres'] = 'moyenne';
                break;
            case 'forte':
                $res['lettres'] = 'forte';
                break;
            case 'nulle':
                $res['lettres'] = 'pas d\'altération';
                break;
            default:
                $res['lettres'] = null;
                break;
        }
    }


    // Altération colmatage du substrat (201)
    /*
     * basé :
     * 
     * Accumulation de dépôts fins (194)
     * 
     */ elseif ($id_alteration == '201') {

        switch ($tab['194']) {

            case 'faible':
                $res['lettres'] = 'faible';
                break;
            case 'moyenne':
                $res['lettres'] = 'moyenne';
                break;
            case 'forte':
                $res['lettres'] = 'forte';
                break;
            case 'nulle':
                $res['lettres'] = 'pas d\'altération';
                break;
            default:
                $res['lettres'] = null;
                break;
        }
    }


    // Altération uniformisation, artificialisation des berges (210)
    /*
     * basé :
     * 
     * Diversité, densité des habitats de berges (204)
     * Diversité de la forme des berges (pentes, hauteur...) (205)
     * 
     */ elseif ($id_alteration == '210') {

        $res204 = $res205 = null;

        //  conversion des parameters en alteration

        switch ($tab['204']) {

            case 'faible':
                $res204['lettres'] = 'moyenne';
                break;
            case 'moyenne':
                $res204['lettres'] = 'faible';
                break;
            case 'forte':
                $res204['lettres'] = 'pas d\'altération';
                break;
            case 'nulle':
                $res204['lettres'] = 'forte';
                break;
            default:
                $res204['lettres'] = null;
                break;
        }


        switch ($tab['205']) {

            case 'faible':
                $res205['lettres'] = 'moyenne';
                break;
            case 'moyenne':
                $res205['lettres'] = 'faible';
                break;
            case 'forte':
                $res205['lettres'] = 'pas d\'altération';
                break;
            case 'nulle':
                $res205['lettres'] = 'forte';
                break;
            default:
                $res205['lettres'] = null;
                break;
        }

        $resultatEnNombre = null;
        $resultatEnNombre = max(array(convertitAlterationEnNombre($res204['lettres']), convertitAlterationEnNombre($res205['lettres'])));

        $alteration = convertitNombreEnalteration($resultatEnNombre);
        $res['lettres'] = $alteration;
    }


    // Altération réduction du linéaire de berges (développé) (211)
    /*
     * basé :
     * 
     * Stabilité des berges (206)
     * 
     */ elseif ($id_alteration == '211') {

        switch ($tab['206']) {

            case 'faible':
                $res['lettres'] = 'moyenne';
                break;
            case 'moyenne':
                $res['lettres'] = 'faible';
                break;
            case 'forte':
                $res['lettres'] = 'pas d\'altération';
                break;
            case 'nulle':
                $res['lettres'] = 'forte';
                break;
            default:
                $res['lettres'] = null;
                break;
        }
    }


    // Altération réduction, uniformisation de la ripisylve (212)
    /*
     * basé :
     * 
     * Continuité de la végétation de rive (207)
     * Densité de la ripisylve (208)
     * Diversité de la végétation de rive (209)
     * 
     * requiert une premiere converion en alteration puis un agrégagt des sous alterations
     * 
     */ elseif ($id_alteration == '212') {


        $res207 = $res208 = $res209 = null;

        //  conversion des parameters en alteration

        switch ($tab['207']) {

            case 'faible':
                $res207['lettres'] = 'moyenne';
                break;
            case 'moyenne':
                $res207['lettres'] = 'faible';
                break;
            case 'forte':
                $res207['lettres'] = 'pas d\'altération';
                break;
            case 'nulle':
                $res207['lettres'] = 'forte';
                break;
            default:
                $res207['lettres'] = null;
                break;
        }


        switch ($tab['208']) {

            case 'faible':
                $res208['lettres'] = 'moyenne';
                break;
            case 'moyenne':
                $res208['lettres'] = 'faible';
                break;
            case 'forte':
                $res208['lettres'] = 'pas d\'altération';
                break;
            case 'nulle':
                $res208['lettres'] = 'forte';
                break;
            default:
                $res208['lettres'] = null;
                break;
        }


        switch ($tab['209']) {

            case 'faible':
                $res209['lettres'] = 'moyenne';
                break;
            case 'moyenne':
                $res209['lettres'] = 'faible';
                break;
            case 'forte':
                $res209['lettres'] = 'pas d\'altération';
                break;
            case 'nulle':
                $res209['lettres'] = 'forte';
                break;
            default:
                $res209['lettres'] = null;
                break;
        }

        $resultatEnNombre = null;
        $resultatEnNombre = max(array(convertitAlterationEnNombre($res207['lettres']), convertitAlterationEnNombre($res208['lettres']), convertitAlterationEnNombre($res209['lettres'])));

        $alteration = convertitNombreEnalteration($resultatEnNombre);
        $res['lettres'] = $alteration;
    }

    return $res;
}

function convertitAlterationEnNombre($alt)
{


    switch ($alt) {

        case 'pas d\'altération':
            $nb = 1;
            break;
        case 'faible':
            $nb = 2;
            break;
        case 'moyenne':
            $nb = 3;
            break;
        case 'forte':
            $nb = 4;
            break;
        default:
            $nb = null;
    }

    return $nb;
}

function convertitNombreEnalteration($nb)
{


    switch ($nb) {

        case 1:
            $alt = 'pas d\'altération';
            break;
        case 2:
            $alt = 'faible';
            break;
        case 3:
            $alt = 'moyenne';
            break;
        case 4:
            $alt = 'forte';
            break;
        default:
            $alt = null;
    }

    return $alt;
}

function convertitNombreEnalterationCompartiment($nb)
{


    switch ($nb) {

        case 1:
            $alt = 'très bon';
            break;
        case 2:
            $alt = 'bon';
            break;
        case 3:
            $alt = 'moyenne';
            break;
        case 4:
            $alt = 'mauvais';
            break;
        default:
            $alt = null;
    }

    return $alt;
}

/*
 * charge les segments ou le segment
 */

function segment($app, $id_seg)
{

    $pdo = $app['pdo'];

    if ($id_seg == 'tous') {

        // selection de tous les segments (type 39) dont la date_création_objet est récente (date de tracé que le segment soit ancien (OCRE) ou bien récent (terrain 2014)
        // et avec le champs diversité facies écoulement non null
        // Rq on prend aussi les segments qui sont terminés (date de fin != 0)

        $iseg = "select * from " . SCHEMA . ".sysma_object where "
            . "sysma_object_type_id = 39 ";
    } else {

        $iseg = "select * from " . SCHEMA . ".sysma_object where sysma_object_type_id = 39 and sysma_object_id = " . $id_seg;
    }

    $qry0 = $pdo->prepare($iseg);
    $qry0->execute() or die();
    return $qry0->fetchAll();
}

/*
 * recherche un value pour l'objet et paramètre données, archive cette value et enregistre une nouvelle
 */

function renseigneEtArchive($app, $sysma_object_id, $parameter_id, $date, $newval)
{

    $f = null;
    $pdo = $app['pdo'];
    $V = new SysmaObjectParameterValue('');
    $V->buildFromParameters($sysma_object_id, $parameter_id, '', $app);

    // cas 1 : pas de value pour cet objet et parameter
    if ($V->value == null) {

        $inst = "insert into " . SCHEMA . ".sysma_object_data
		(sysma_object_id, parameter_id, start_date, end_date, value, start_date, user_id, data_index) 
		VALUES
		('" . $sysma_object_id . "'," . $parameter_id . ",'" . $date . "',0," . $newval . ",'" . date('Y-m-d H:i:s') . "',	'" . $app['session']->get('user_id') . "'," . dataIndex($app['pdo']) . ")";

        $qry = $pdo->prepare($inst);
        if ($qry->execute() == false) {
            if (DEBUG) {
                die($inst);
            } else {
                die("Erreur SQL. Veuillez créer un rapport de bug pour l'équipe de developpement de Sysma-FOSS <a href=https://gitlab.sevre-nantaise.com/eptbsn/sysma-foss/-/issues>ici</a>");
            }
        }

        $f .= '<h3>Enregistrement OK</h3>';
    }

    // cas 2 : il y a une value actuelle
    if ($V->value != null) {


        if ($V->value != $newval) {

            $inst = $inst2 = null;

            // update de la date de fin de l'ancienne données avec la nouvelle date de calcul
            $inst2 = "update " . SCHEMA . ".sysma_object_data SET end_date = '" . $date . "' WHERE data_index = " . $V->data_index;
            $f .= $inst2 . '<br/>';
            $qryinst2 = $pdo->prepare($inst2);
            if ($qryinst2->execute() == false) {
                if (DEBUG) {
                    die($inst2);
                } else {
                    die("Erreur SQL. Veuillez créer un rapport de bug pour l'équipe de developpement de Sysma-FOSS <a href=https://gitlab.sevre-nantaise.com/eptbsn/sysma-foss/-/issues>ici</a>");
                }
            }


            $inst = "insert into " . SCHEMA . ".sysma_object_data
		(sysma_object_id, parameter_id, start_date, end_date, value, start_date, user_id, data_index) 
		VALUES
		('" . $sysma_object_id . "'," . $parameter_id . ",'" . $date . "','" . $V->end_date . "'," . $newval . ",'" . date('Y-m-d H:i:s') . "',	'" . $app['session']->get('user_id') . "'," . dataIndex($app['pdo']) . ")";

            $f .= $inst . '<br/>';
            $qryinst = $pdo->prepare($inst);
            if ($qryinst->execute() == false) {
                if (DEBUG) {
                    die($inst);
                } else {
                    die("Erreur SQL. Veuillez créer un rapport de bug pour l'équipe de developpement de Sysma-FOSS <a href=https://gitlab.sevre-nantaise.com/eptbsn/sysma-foss/-/issues>ici</a>");
                }
            }

            $f .= '<h3>Enregistrement et mise à jour OK</h3>';
        } else {

            $f .= '<h3>Pas de changement</h3>';
        }
    }

    return $f;
}

function calculeEtEnregistreObstacles($app, $sysma_object_id, $date, $rec, $id_object_type, $parameter_id, $tampon = 20)
{


    $f = null;

    if ($sysma_object_id == null or $date == null or $tampon == null)
        die('Paramètres manquants');
    if (!isADate($date))
        die('La date fournie est incorrecte ' . $date);

    $pdo = $app['pdo'];


    // pour chaque segment
    foreach (segment($app, $sysma_object_id) as $sseg) {


        $sommeHC = 0;
        $nbOuvragesPrimaires = 0;

        // $f .= '<h1>Segment  ' . $sseg['sysma_object'] . ' <small>[' . $sysma_object_id . ']</small></h1>';
        // sélection des berges dégradées qui intersectent un tampon de 10mètres autour des segments reh dont la date de fin est postérieure à la date de calcul et la date de début antérieur (ou bien 0 en date de début / fin)

        $iab = "select * from " . SCHEMA . ".sysma_object where "
            . "sysma_object_type_id = 46 and " // type objet
            . "(sysma_object.end_date > '" . $date . "' or sysma_object.end_date = '0') and " // date de fin postérieure à la date de calcul (ou nulle)
            . "(sysma_object.start_date < '" . $date . "' or sysma_object.start_date = '0') and " // date de fin postérieure à la date de calcul (ou nulle)
            . "ST_Intersects(ST_Buffer('" . $sseg['geom'] . "'," . $tampon . "),geom)"; // intersection segment avec tampon de  100 mètres 

        $qryob = $pdo->prepare($iab);
        $qryob->execute() or die();
        $f .= $qryob->rowCount() . ' obstacles';
        $f .= '<br>';

        while ($resab = $qryob->fetchObject()) {


            // on a les obstacles on regarde les primaires

            $ipdo1 = "select * from " . SCHEMA . ".sysma_object_data where sysma_object_id = " . $resab->sysma_object_id . " and parameter_id = 243 and (end_date > '" . $date . "' or end_date = '0') and (start_date < '" . $date . "' or start_date = '0')";
            $qryipdo1 = $pdo->prepare($ipdo1);
            $qryipdo1->execute() or die();
            $pdo1 = $qryipdo1->fetchObject();

            // on a les obstacles on regarde leur HC actuelle

            $ipdo2 = "select * from " . SCHEMA . ".sysma_object_data where sysma_object_id = " . $resab->sysma_object_id . " and parameter_id = 257 and (end_date > '" . $date . "' or end_date = '0') and (start_date < '" . $date . "' or start_date = '0')";
            $qryipdo2 = $pdo->prepare($ipdo2);
            $qryipdo2->execute() or die();
            $pdo2 = $qryipdo2->fetchObject();

            if ($pdo1->value == 'primaire') {

                $nbOuvragesPrimaires++;
                $sommeHC = $sommeHC + $pdo2->value;
            }


            echo '<br>';
        }

        $f .= $nbOuvragesPrimaires . ' ouvrages primaires<br>';
        $f .= $sommeHC . ' mètres de hauteur de chute cumulée<br>';


        if ($rec == 'true') {

            $f .= renseigneEtArchive($app, $sysma_object_id, 288, $date, $nbOuvragesPrimaires);
            $f .= renseigneEtArchive($app, $sysma_object_id, 289, $date, $sommeHC);
        }
    }

    return $f;
}

function calculeEtEnregistrePlansDEau($app, $sysma_object_id, $date, $rec, $id_object_type, $parameter_id, $tampon = 80)
{


    $f = null;

    if ($sysma_object_id == null or $date == null or $tampon == null)
        die('Paramètres manquants');
    if (!isADate($date))
        die('La date fournie est incorrecte ' . $date);

    $pdo = $app['pdo'];


    // pour chaque segment
    foreach (segment($app, $sysma_object_id) as $sseg) {

        $compteur = 0;
        // $f .= '<h1>Segment  ' . $sseg['sysma_object'] . ' <small>[' . $sysma_object_id . ']</small></h1>';
        // sélection des berges dégradées qui intersectent un tampon de 10mètres autour des segments reh dont la date de fin est postérieure à la date de calcul et la date de début antérieur (ou bien 0 en date de début / fin)

        $iab = "select * from " . SCHEMA . ".sysma_object where "
            . "sysma_object_type_id = 45 and " // type objet
            . "(sysma_object.end_date > '" . $date . "' or sysma_object.end_date = '0') and " // date de fin postérieure à la date de calcul (ou nulle)
            . "(sysma_object.start_date < '" . $date . "' or sysma_object.start_date = '0') and " // date de fin postérieure à la date de calcul (ou nulle)
            . "ST_Intersects(ST_Buffer('" . $sseg['geom'] . "'," . $tampon . "),geom)"; // intersection segment avec tampon de  100 mètres 

        $qryob = $pdo->prepare($iab);
        $qryob->execute() or die();
        $f .= $qryob->rowCount() . ' plans d\'eau sont situés à proximité du segment';
        $f .= '<br>';

        while ($resab = $qryob->fetchObject()) {

            // on a les plans d'eau, on regarde s'ils sont connectés avec les données terrain
            $ipdo1 = "select * from " . SCHEMA . ".sysma_object_data where sysma_object_id = " . $resab->sysma_object_id . " and parameter_id = 234 and (end_date > '" . $date . "' or end_date = '0') and (start_date < '" . $date . "' or start_date = '0')";
            $qryipdo1 = $pdo->prepare($ipdo1);
            $qryipdo1->execute() or die();
            $pdo1 = $qryipdo1->fetchObject();
            //var_dump($pdo1) ;
            // on a les plans d'eau, on regarde s'ils sont connectés avec les données SIG
            $ipdo2 = "select * from " . SCHEMA . ".sysma_object_data where sysma_object_id = " . $resab->sysma_object_id . " and parameter_id = 233 and (end_date > '" . $date . "' or end_date = '0') and (start_date < '" . $date . "' or start_date = '0')";
            $qryipdo2 = $pdo->prepare($ipdo2);
            $qryipdo2->execute() or die();
            $pdo2 = $qryipdo2->fetchObject();
            // var_dump($pdo2) ;


            $f .= 'Plan d\'eau ' . $resab->sysma_object_id . '<br>';
            $f .= '- connexion terrain : ' . @$pdo1->value . '<br>';
            $f .= '- connexion sig : ' . @$pdo2->value . '<br>';


            if (@$pdo1->value == 'sur cours d&#039;eau') {
                $compteur++;
            }

            // si l'info terrain est nulle
            elseif (@$pdo1->value == null and @$pdo2->value == 1) {
                $compteur++;
            }
        }

        $f .= 'Au total, ' . $compteur . ' plans d\'eau connectés';

        if ($rec == 'true') {

            $f .= renseigneEtArchive($app, $sysma_object_id, 287, $date, $compteur);
        }

        return $f;
    }
}

function calculeEtEnregistreLineaires($app, $sysma_object_id, $date, $rec, $id_object_type, $parameter_id, $tampon = 80)
{


    $f = null;

    if ($sysma_object_id == null or $date == null or $tampon == null or $id_object_type == null or $parameter_id == null)
        die('Paramètres manquants');
    if (!isADate($date))
        die('La date fournie est incorrecte ' . $date);

    $pdo = $app['pdo'];


    // pour chaque segment
    foreach (segment($app, $sysma_object_id) as $sseg) {


        // $f .= '<h1>Segment  ' . $sseg['sysma_object'] . ' <small>[' . $sysma_object_id . ']</small></h1>';

        $iab = "select sum(ST_Length(ST_Intersection(ST_Buffer('" . $sseg['geom'] . "',$tampon),geom ))) as longueur from " . SCHEMA . ".sysma_object where "
            . "sysma_object_type_id = $id_object_type and " // type abreuvoir sauvage
            . "(end_date > '" . $date . "' or end_date = '0') and " // date de fin postérieure à la date de calcul (ou nulle)
            . "(start_date <= '" . $date . "' or start_date = '0')"; // date de fin postérieure à la date de calcul (ou nulle)

        $qryob = $pdo->prepare($iab);
        $qryob->execute() or die();
        $res = $qryob->fetchObject();
        $f .= round($res->longueur) . ' mètres de  linéaire';
        $f .= '<br>';


        if ($rec == 'true') {

            $f .= renseigneEtArchive($app, $sysma_object_id, $parameter_id, $date, round($res->longueur));
        }
    }

    return $f;
}

/*
 * calcule le nombre d'objet de type $id_object_type dans un tampon de  $tampon mètres autour du segment $sysma_object_id pour la $date précisée.
 * enregistre la value dans l'objet $sysma_object_id avec le paramètre $parameter_id (et archives l'ancienne) si $rec est vrai
 */

function calculeEtEnregistreNbObjets($app, $sysma_object_id, $date, $rec, $id_object_type, $parameter_id, $tampon = 100)
{


    if ($sysma_object_id == null or $date == null or $id_object_type == null or $parameter_id == null or $tampon == null)
        die('Paramètres manquants');
    if (!isADate($date))
        die('La date fournie est incorrecte ' . $date);

    $f = null;

    $pdo = $app['pdo'];

    // pour chaque segment
    foreach (segment($app, $sysma_object_id) as $sseg) {

        //  $f .= '<h1>Segment  ' . $sseg['sysma_object'] . ' <small>[' . $sysma_object_id . ']</small></h1>';
        // sélection des abreuvoirs sauvages qui intersectent un tampon de x mètres autour des segments reh dont la date de fin est postérieure à la date de calcul et la date de début antérieur (ou bien 0 en date de début / fin)

        $iab = "select * from " . SCHEMA . ".sysma_object where "
            . "sysma_object_type_id = " . $id_object_type . "  and " // type abreuvoir sauvage
            . "(end_date > '" . $date . "' or end_date = '0') and " // date de fin postérieure à la date de calcul (ou nulle)
            . "(start_date <= '" . $date . "' or start_date = '0') and " // date de fin postérieure à la date de calcul (ou nulle)
            . "ST_Intersects(ST_Buffer('" . $sseg['geom'] . "'," . $tampon . "),geom)"; // intersection segment avec tampon de  100 mètres 


        $qryab = $pdo->prepare($iab);
        $qryab->execute() or die();
        $resab = $qryab->rowCount();

        $f .= $resab . ' objets';
        $f .= '<br>';


        if ($rec == 'true') {

            $f .= renseigneEtArchive($app, $sysma_object_id, $parameter_id, $date, $resab);
        }
    }

    return $f;
}

/*
 * ce fichier permet de déclencher le calcul des altérations REH pour un segement (ou tous) et une date donnée
 * il est appelé en post depuis une fiche segment ou bien en get depuis le menu requêtes
 * 
 * il stoppe si les données du segment sont incomplètes
 * 
 */

function calculeEtEnregistreAlterationsREH($app, $sysma_object_id, $date, $rec)
{

    $f = null;



    if ($sysma_object_id == null or $date == null)
        die('Paramètres manquants');
    if (!isADate($date))
        die('La date fournie est incorrecte ' . $date);

    $pdo = $app['pdo'];

    if ($sysma_object_id == 'tous') {

        $iseg = "select * from " . SCHEMA . ".sysma_object," . SCHEMA . ".sysma_object_data where "
            . "sysma_object_type_id = 39 and "
            . "created_at > '2014-01-01' and "
            . "sysma_object.sysma_object_id = sysma_object_data.sysma_object_id and "
            . "sysma_object_data.parameter_id = 184 and "
            . "sysma_object_data.value <> ''";
    } else {

        $iseg = "select * from " . SCHEMA . ".sysma_object where sysma_object_type_id = 39 and sysma_object_id = " . $sysma_object_id;
    }
    $qry = $pdo->prepare($iseg);
    $qry->execute();



    while ($sseg = $qry->fetchObject()) {


        $incomplet = false;
        $listeparametersManquants = null;

        // ETAPE 1 / parameterS -> ALTERATIONS UNITAIRES
        // on passe en revue les paramètres utiles pour le calcul
        // 
        // 
        // Altération élevation de la ligne d'eau (186)
        /*
         * basé :
         * 
         * Diversité des faciès d'écoulement (étiage) (184)
         * 
         */

        $V = new SysmaObjectParameterValue('');
        $V->buildFromParameters($sysma_object_id, 184, $date, $app);
        if ($V->value == null) {
            $incomplet = true;
            $listeparametersManquants .= 'Diversité des faciès d\'écoulement (étiage) (184),';
        } else {
            $resultat = calculeREH(186, array('184' => $V->value));
            $res['186'] = $resultat['lettres'];
        }


        // Altération modification du profil en long (197)
        /*
         * basé :
         * 
         * Sinuosité (188)
         * 
         */
        $V = new SysmaObjectParameterValue('');
        $V->buildFromParameters($sysma_object_id, 188, $date, $app);
        if ($V->value == null) {
            $incomplet = true;
            $listeparametersManquants .= 'Sinuosité (188),';
        } else {
            $resultat = calculeREH(197, array('188' => $V->value));
            $res['197'] = $resultat['lettres'];
        }


        // Altération réduction de la granulométrie grossière (198)
        /*
         * basé :
         * 
         * Diversité de la granulométrie (191)
         * 
         */
        $V = new SysmaObjectParameterValue('');
        $V->buildFromParameters($sysma_object_id, 191, $date, $app);
        if ($V->value == null) {
            $incomplet = true;
            $listeparametersManquants .= 'Diversité de la granulométrie (191),';
        } else {
            $resultat = calculeREH(198, array('191' => $V->value));
            $res['198'] = $resultat['lettres'];
        }

        // Altération déstabilisation du substrat (199)
        /*
         * basé :
         * 
         * Stabilité du substrat (des fonds) (192)
         * 
         */
        $V = new SysmaObjectParameterValue('');
        $V->buildFromParameters($sysma_object_id, 192, $date, $app);
        if ($V->value == null) {
            //$incomplet = true; // parameter non obligatoire
            $listeparametersManquants .= 'OPTIONNEL / Stabilité du substrat (des fonds) (192),';
        } else {
            $resultat = calculeREH(199, array('192' => $V->value));
            $res['199'] = $resultat['lettres'];
        }

        // Altération modification du profil en travers (200)
        /*
         * basé :
         * 
         * Incision du lit (193)
         * 
         */
        $V = new SysmaObjectParameterValue('');
        $V->buildFromParameters($sysma_object_id, 193, $date, $app);
        if ($V->value == null) {
            $incomplet = true;
            $listeparametersManquants .= 'Incision du lit (193),';
        } else {
            $resultat = calculeREH(200, array('193' => $V->value));
            $res['200'] = $resultat['lettres'];
        }

        // Altération colmatage du substrat (201)
        /*
         * basé :
         * 
         * Accumulation de dépôts fins (194)
         * 
         */
        $V = new SysmaObjectParameterValue('');
        $V->buildFromParameters($sysma_object_id, 194, $date, $app);
        if ($V->value == null) {
            //$incomplet = true; // parameter non obligatoire
            $listeparametersManquants .= 'OPTIONNEL / Accumulation de dépôts fins (194),';
        } else {
            $resultat = calculeREH(201, array('194' => $V->value));
            $res['201'] = $resultat['lettres'];
        }

        // Altération uniformisation, artificialisation des berges (210)
        /*
         * basé :
         * 
         * Diversité, densité des habitats de berges (204)
         * Diversité de la forme des berges (pentes, hauteur...) (205)
         * 
         */
        $V = new SysmaObjectParameterValue('');
        $V->buildFromParameters($sysma_object_id, 204, $date, $app);
        if ($V->value == null) {
            $incomplet = true;
            $listeparametersManquants .= 'Diversité, densité des habitats de berges (204),';
        } else {
            $V2 = new SysmaObjectParameterValue('');
            $V2->buildFromParameters($sysma_object_id, 205, $date, $app);
            if ($V->value == null) {
                $incomplet = true;
                $listeparametersManquants .= 'Diversité de la forme des berges (pentes, hauteur...) (205),';
            } else {
                $resultat = calculeREH(210, array('204' => $V->value, '205' => $V2->value));
                $res['210'] = $resultat['lettres'];
            }
        }


        // Altération réduction du linéaire de berges (développé) (211)
        /*
         * basé :
         * 
         * Stabilité des berges (206)
         * 
         */
        $V = new SysmaObjectParameterValue('');
        $V->buildFromParameters($sysma_object_id, 206, $date, $app);
        if ($V->value == null) {
            $incomplet = true;
            $listeparametersManquants .= 'Stabilité des berges (206),';
        } else {
            $resultat = calculeREH(211, array('206' => $V->value));
            $res['211'] = $resultat['lettres'];
        }

        // Altération réduction, uniformisation de la ripisylve (212)
        /*
         * basé :
         * 
         * Continuité de la végétation de rive (207)
         * Densité de la ripisylve (208)
         * Diversité de la végétation de rive (209)
         * 
         * requiert une premiere converion en alteration puis un agrégagt des sous alterations
         * 
         */
        $V = new SysmaObjectParameterValue('');
        $V->buildFromParameters($sysma_object_id, 207, $date, $app);
        if ($V->value == null) {
            $incomplet = true;
            $listeparametersManquants .= 'Continuité de la végétation de rive (207),';
        } else {
            $V2 = new SysmaObjectParameterValue('');
            $V2->buildFromParameters($sysma_object_id, 208, $date, $app);
            if ($V->value == null) {
                $incomplet = true;
                $listeparametersManquants .= 'Densité de la ripisylve (208),';
            } else {
                $V3 = new SysmaObjectParameterValue('');
                $V3->buildFromParameters($sysma_object_id, 209, $date, $app);
                if ($V->value == null) {
                    $incomplet = true;
                    $listeparametersManquants .= 'Diversité de la végétation de rive (209),';
                } else {

                    $resultat = calculeREH(212, array('207' => $V->value, '208' => $V2->value, '209' => $V3->value));
                    $res['212'] = $resultat['lettres'];
                }
            }
        }


        if ($incomplet == false) {

            $listeP = array(186, 187, 197, 198, 199, 200, 201, 203, 210, 211, 212, 213);

            // ETAPE 2 / ALTERATIONS UNITAIRES -> ALTERATION DES COMPARTIMENTS
            // ALT. COMP. LIGNE D'EAU (187)
            /*
             * basé sur 
             * 
             * Altération élevation de la ligne d'eau (186)
             */

            $res['187'] = convertitNombreEnAlterationCompartiment(convertitAlterationEnNombre($res['186']));

            // ALT. COMP. LIT (203)
            /*
             * basé sur
             * 
             * Altération modification du profil en long (197)
             * Altération réduction de la granulométrie grossière (198)
             * Altération déstabilisation du substrat (199)
             * Altération modification du profil en travers (200)
             * Altération colmatage du substrat (201)
             * 
             * 
             */

            $res['203'] = convertitNombreEnAlterationCompartiment(
                max(
                    array(
                        convertitAlterationEnNombre($res['197']),
                        convertitAlterationEnNombre($res['198']),
                        convertitAlterationEnNombre($res['199']),
                        convertitAlterationEnNombre($res['200']),
                        convertitAlterationEnNombre($res['201'])
                    )
                )
            );


            // ALT. COMP. BERGES (213)
            /*
             * basé sur
             * Altération uniformisation, artificialisation des berges (210)
             * Altération réduction du linéaire de berges (développé) (211)
             * Altération réduction, uniformisation de la ripisylve (212)
             * 
             */

            $res['213'] = convertitNombreEnAlterationCompartiment(
                max(
                    array(
                        convertitAlterationEnNombre($res['210']),
                        convertitAlterationEnNombre($res['211']),
                        convertitAlterationEnNombre($res['212'])
                    )
                )
            );

            //var_dump($res);

            $f = '<h3>Résultats ' . $sysma_object_id . ' ' . $sseg->sysma_object . '</h3>';

            $f .= '<table border="1" class="tableauHistorique">';

            for ($c = 0; $c < count($listeP); $c++) {

                $P = new SysmaObjectParameter($listeP[$c], $app);
                $f .= '<tr><td>' . $P->parameter . ' ' . $P->parameter_id . '</td><td>' . $res[$P->parameter_id] . '</td></tr>';
            }

            $f .= '</table>';


            // enregistrement des données dans la base

            if ($rec == 'true') {

                /*
                 * 
                 * la nouvelle donnée est calculée pour une date
                 * si il y a des données antérieures :
                 * il faut leur mettre une date de fin 
                 * si il y a des données postérieures :
                 * il faut mettre une date de fin à la nouvelle donnée
                 * si il y a des données à la meme date
                 * 
                 */



                // on recherche la donnée valable pour cet objet et ce parameter

                for ($c = 0; $c < count($listeP); $c++) {

                    $param = $listeP[$c];

                    $i5 = "select * from " . SCHEMA . ".sysma_object_data where sysma_object_id = " . $sysma_object_id . " and parameter_id = " . $param . " and (start_date  < '" . $date . "' or start_date = '0') and (end_date > '" . $date . "' or end_date = '0') order by start_date asc";

                    $qry5 = $pdo->prepare($i5);
                    if ($qry5->execute() == false) {
                        if (DEBUG) {
                            die($i5);
                        } else {
                            die("Erreur SQL. Veuillez créer un rapport de bug pour l'équipe de developpement de Sysma-FOSS <a href=https://gitlab.sevre-nantaise.com/eptbsn/sysma-foss/-/issues>ici</a>");
                        }
                    }

                    if ($qry5->rowCount() > 0) {

                        $inst = $inst2 = null;

                        $s5 = $qry5->fetchObject();
                        // update de la date de fin de l'ancienne données avec la nouvelle date de calcul
                        $inst2 = "update " . SCHEMA . ".sysma_object_data SET end_date = '" . $date . "' WHERE data_index = " . $s5->data_index;
                        $f .= $inst2 . '<br/>';
                        $qryinst2 = $pdo->prepare($inst2);
                        if ($qryinst2->execute() == false) {
                            if (DEBUG) {
                                die($inst2);
                            } else {
                                die("Erreur SQL. Veuillez créer un rapport de bug pour l'équipe de developpement de Sysma-FOSS <a href=https://gitlab.sevre-nantaise.com/eptbsn/sysma-foss/-/issues>ici</a>");
                            }
                        }

                        $inst = "insert into " . SCHEMA . ".sysma_object_data
		(sysma_object_id, parameter_id, start_date, end_date, value, start_date, user_id, data_index) 
		VALUES
		('" . $sysma_object_id . "','" . $param . "','" . $date . "','" . $s5->end_date . "','" . htmlspecialchars($res[$param], ENT_QUOTES) . "','" . date('Y-m-d H:i:s') . "',	'" . $app['session']->get('user_id') . "'," . dataIndex($app['pdo']) . ")";

                        $f .= $inst . '<br/>';
                        $qryinst = $pdo->prepare($inst);
                        if ($qryinst->execute() == false) {
                            if (DEBUG) {
                                die($inst);
                            } else {
                                die("Erreur SQL. Veuillez créer un rapport de bug pour l'équipe de developpement de Sysma-FOSS <a href=https://gitlab.sevre-nantaise.com/eptbsn/sysma-foss/-/issues>ici</a>");
                            }
                        }
                    } elseif ($qry5->rowCount() == 0) {

                        $inst = null;

                        $inst = "insert into " . SCHEMA . ".sysma_object_data
		(sysma_object_id, parameter_id, start_date, end_date, value, start_date, user_id, data_index) 
		VALUES
		('" . $sysma_object_id . "','" . $param . "','" . $date . "','0','" . htmlspecialchars($res[$param], ENT_QUOTES) . "','" . date('Y-m-d H:i:s') . "',	'" . $app['session']->get('user_id') . "'," . dataIndex($app['pdo']) . ")";

                        $f .= $inst . '<br/>';
                        $qryinst = $pdo->prepare($inst);
                        if ($qryinst->execute() == false) {
                            if (DEBUG) {
                                die($inst);
                            } else {
                                die("Erreur SQL. Veuillez créer un rapport de bug pour l'équipe de developpement de Sysma-FOSS <a href=https://gitlab.sevre-nantaise.com/eptbsn/sysma-foss/-/issues>ici</a>");
                            }
                        }
                    }
                }

                $f . '<h2>Données enregistrées</h2>';
            }
        } else {

            $f .= '
        <h1> Calcul impossible pour le segment ' . $sysma_object_id . ' ' . $sseg->sysma_object . '</h1>  
        
        <p>Les données du segment ne permettent pas de calculer les altérations des compartiments.<br/>
        <p>Données manquantes :</p><ul><li> ' . str_replace(',', '</li><li>', substr($listeparametersManquants, 0, -1)) . '</li></ul>';
        }
    }

    return $f;
}
