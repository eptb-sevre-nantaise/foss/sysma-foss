<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;


class Debug
{

    protected static $startTime=[];
    protected static $nbProfile=0;
    protected static $logFileReady = false;
    protected static $logFile = null;
    protected static $logFilePath = null;
    protected static $logFileMaxLines = DEBUG_ENABLE_LOG_FILE_MAX_LINES;
    protected static $logSqlFile = null;
    protected static $logSqlFilePath = null;
    protected static $logSqlFileMaxLines = DEBUG_ENABLE_LOG_FILE_MAX_LINES;
    protected static $logSqlErrorFileReady = false;
    protected static $logSqlErrorFileMaxLines = DEBUG_LOG_SQL_QUERIES_ERRORS_MAX_LINES;
    protected static $logSqlErrorFile = null;
    protected static $logSqlErrorFilePath = null;
    // protected static $endTime;

    public static function initLogSqlErrorFile($_filename = 'sql_queries_errors.log')
    {
        if (self::$logSqlErrorFileReady == false) {
            $now =  new DateTime();

            // sqlErrorlog file
            self::$logSqlErrorFilePath = ROOT_URI . 'log/' . $_filename;
            self::$logSqlErrorFile = fopen(self::$logSqlErrorFilePath, "a") or die("Unable to open file: " . self::$logSqlErrorFilePath);

            if (count(file(self::$logSqlErrorFilePath))> self::$logSqlErrorFileMaxLines) {
                file_put_contents(self::$logSqlErrorFilePath, "");
                fwrite(self::$logSqlErrorFile, "---------- RESET log file content due to exceeding the maximum number of lines (" . self::$logSqlErrorFileMaxLines .  ") \n");
                fwrite(self::$logSqlErrorFile, "---------- log start at " . $now->format('Y-m-d H:i:s') . "\n");
                            }
         
            self::$logSqlErrorFileReady = true;
        }
    }

    public static function initLogFile($_filename = 'debug.log') {
        $now =  new DateTime();
        if (self::$logFileReady == false) {
            // $now =  new DateTime();

            // general log file
            self::$logFilePath = ROOT_URI.'log/'.$_filename;
            self::$logFile = fopen(self::$logFilePath, "a") or die("Unable to open file: " . self::$logFilePath);         

            // Sql log file
            if (DEBUG_SPLIT_GENERAL_LOGS_AND_SQL_LOGS_IN_2_FILES) {
                
                self::$logSqlFilePath = ROOT_URI.'log/sql_queries.log';
                self::$logSqlFile = fopen(self::$logSqlFilePath, "a") or die("Unable to open file: " . self::$logSqlFilePath);
            }
            self::$logFileReady = true;
        }
        // clear general log file when the number of lines exceeds constant limit
        if (count(file(self::$logFilePath)) > self::$logFileMaxLines) {
            file_put_contents(self::$logFilePath, "");
            fwrite(self::$logFile, "---------- RESET log file content due to exceeding the maximum number of lines (" . self::$logFileMaxLines .  ")\n");
        }
        fwrite(self::$logFile, "---------- log start at " . $now->format('Y-m-d H:i:s') . "\n");
        if (DEBUG_SPLIT_GENERAL_LOGS_AND_SQL_LOGS_IN_2_FILES) {
            // clear sql  log file when the number of lines exceeds constant limit
            if (count(file(self::$logSqlFilePath)) > self::$logSqlFileMaxLines) {
                file_put_contents(self::$logSqlFilePath, "");
                fwrite(self::$logSqlFile, "---------- RESET log file content due to exceeding the maximum number of lines (" . self::$logSqlFileMaxLines .  ") \n");
            }
            fwrite(self::$logSqlFile, "---------- log start at " . $now->format('Y-m-d H:i:s') . "\n");
        }
    }


//DEBUG_ENABLE_LOG_FILE_MAX_LINES
    public static function clearLogFile() {
        if (self::$logFileReady == true) {
            $now =  new DateTime();

            // general log file
            file_put_contents(self::$logFilePath, "");
            fwrite(self::$logFile, "---------- log cleared at " . $now->format('Y-m-d H:i:s') . "\n");

            // file_put_contents(self::$logSqlErrorFilePath, "");
            // fwrite(self::$logSqlErrorFile, "---------- log cleared at " . $now->format('Y-m-d H:i:s') . "\n");

            if (DEBUG_SPLIT_GENERAL_LOGS_AND_SQL_LOGS_IN_2_FILES) {
                // Sql log file
                file_put_contents(self::$logSqlFilePath, "");
                fwrite(self::$logSqlFile, "---------- log cleared at " . $now->format('Y-m-d H:i:s') . "\n");
            }

        }
    }

    // generic logger
    // TODO: enhance this with several log levels + logging into log file
    // note: this could be mv into its own logger class instead... TBD 
    public static function log($_msg, $_logLvl=DEBUG) {
        if (is_array($_msg)) {
            $_msg = json_encode($_msg, JSON_PRETTY_PRINT);
        }
        if (self::$logFileReady) {
            fwrite(self::$logFile, $_msg."\n");
        // } else {
        //     echo $_msg;
        }
   
    }

    public static function timeStampedLog($_msg, $_logLvl=DEBUG) {
        if (is_array($_msg)) {
            $_msg = json_encode($_msg, JSON_PRETTY_PRINT);
        }
        if (self::$logFileReady) {
            $now = new DateTime();
            fwrite(self::$logFile, $now->format('Y-m-d H:i:s') . "| " . $_msg."\n");
        // } else {
        //     echo $_msg;
        }
    }

    public static function logJson($_json, $_logLvl=DEBUG) {
        if (self::$logFileReady) {
            fwrite(self::$logFile, json_encode($_json, JSON_PRETTY_PRINT) ."\n");
        // } else {
        //     echo json_encode($_json, JSON_PRETTY_PRINT);
        }
    }

    // TODO
    public static function logCallStackTrace() {
        if (self::$logFileReady) {
            fwrite(self::$logFile, json_encode(debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS), JSON_PRETTY_PRINT )."\n");
        // } else {
        //     echo $_msg;
        }
    }


    //////////////////////////////////////////////////////////////////////////
    // Log in reserved file: sql_queries.log. Should be used exclusively with SysmaPDO's debug stuff
    public static function logSqlError($_sql, $_error_sql,  $_logLvl=DEBUG) {
        if (DEBUG_LOG_SQL_QUERIES_ERRORS) {
            if (self::$logSqlErrorFileReady) {
                $SendEmail = true;
                $now =  new DateTime();
                $nowStringStandardised = $now->format('Y-m-d H:i:s');
                $nowStringStandardisedPaternRepeatedPerDay = substr($nowStringStandardised, 0, 10);
                $msgDateLineStandardisedPart01 = "/*---- ERROR at ";
                $msgDateLineStandardisedPart02 = $nowStringStandardised;
                $msgDateLineStandardised = $msgDateLineStandardisedPart01 . $msgDateLineStandardisedPart02 . " \n";
                $msgDateLinePaternRepeatedPerDay = $msgDateLineStandardisedPart01 . $nowStringStandardisedPaternRepeatedPerDay;
                $nbOfErrorsToday = 1; // init with current error
                $msgExplanationNoEmail = "";
                // format trace message
                $url = "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
                $msg = "\n";
                $msg .= "---------------------------------------- \n";
                $msg .= $msgDateLineStandardised; // DO NOT modify this line without checking pattern matching for conting message per day
                $msg .= "/*---- url : " . $url . " \n";
                // $msg .= "/*---- user_id : " . $this->$app['session']->get('user_id') . " \n"; // TODO need app
                $msg .= "/*---- SERVER RETURN " . $_error_sql . " */ \n";
                $msg .= "/*---- SQL STRING START :" . " */ \n";
                $msg .= $_sql . "\n";
                $msg .= "/*---- SQL STRING END ----" . " */ \n";
                $msgWebhookLog = $msg; // no email adress
                $msg .= "---------------------------------------- Envoi par courriel : ";
  


                // avoids spam linked to too few lines in the log file
                $msgNbLines = substr_count($msg, "\n");
                if ((($msgNbLines + 1) * DEBUG_LOG_SQL_QUERIES_ERRORS_NB_EMAIL_SENT_PER_DAY) > self::$logSqlErrorFileMaxLines){
                    $SendEmail = false;
                    $msgExplanationNoEmail = "The limit on the number of lines in the log file compared to the number of emails to send does not limit the risk of spam. Fix : increase DEBUG_LOG_SQL_QUERIES_ERRORS_MAX_LINES constant value .";
                }

                if (defined('DEBUG_DEV_MAIL')) {
                    if (!preg_match("/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/", DEBUG_DEV_MAIL)) {
                        $msgExplanationNoEmail = 'Invalid email : ' . DEBUG_DEV_MAIL . '. Check DEBUG_DEV_MAIL in sysma_conf table.';
                        $SendEmail = false;
                    }
                } else {
                    $msgExplanationNoEmail = 'Constant DEBUG_DEV_MAIL not defined. Check DEBUG_DEV_MAIL in sysma_conf table.';
                    $SendEmail = false;
                }

                if (DEBUG_LOG_SQL_QUERIES_ERRORS_NB_EMAIL_SENT_PER_DAY == 0) {
                    $msgExplanationNoEmail = 'Constant DEBUG_LOG_SQL_QUERIES_ERRORS_NB_EMAIL_SENT_PER_DAY is set to 0.';
                    $SendEmail = false;
                }

                if ($SendEmail === true ) {
                    foreach (file(self::$logSqlErrorFilePath) as $line) {
                        if (strpos($line, $msgDateLinePaternRepeatedPerDay) === 0) {
                            $nbOfErrorsToday++;
                        }
                        if ($nbOfErrorsToday> DEBUG_LOG_SQL_QUERIES_ERRORS_NB_EMAIL_SENT_PER_DAY) {
                            $SendEmail = false;
                            $msgExplanationNoEmail = "the number of daily emails is exceeded.";
                        }
                    }
                }

                if ($SendEmail) {
                    $msg .= ' YES @: ' . DEBUG_DEV_MAIL;
                } else {
                    $msg .= ' NO. ' . $msgExplanationNoEmail;
                }

                $msg .= "\n";
                
                if (count(file(self::$logSqlErrorFilePath)) > self::$logSqlErrorFileMaxLines) {
                    file_put_contents(self::$logSqlErrorFilePath, "");
                    fwrite(self::$logSqlErrorFile, "---------- log start at " . $msgDateLineStandardised . "\n");
                    fwrite(self::$logSqlErrorFile, "---------- RESET log file content due to exceeding the maximum number of lines (" . self::$logSqlErrorFileMaxLines .  ") \n");
                }
                
                fwrite(self::$logSqlErrorFile, $msg);

                if ($SendEmail) {
                    $mailSubject = "[AUTO] [SYSMA] [ERROR] [SQL] Sysma SQL Error";
                    $mailContent = "Il y a une erreur SQL dans sysma :\n";
                    $mailContent .= "Vous recevez ce courriel car il y a eu " . $nbOfErrorsToday ." erreur(s) aujourd'hui. \n";
                    $mailContent .= "Les notifications par courriel, pour cette instance, cesseront au bout de ". DEBUG_LOG_SQL_QUERIES_ERRORS_NB_EMAIL_SENT_PER_DAY . " erreur(s) /jour \n";
                    $mailContent .= "Les fichiers de logs d'erreur sont ici :" . self::$logSqlErrorFilePath ." \n";
                    $mailContent .= "\n";
                    $mailContent .=  $msg ;
                    mail(DEBUG_DEV_MAIL, $mailSubject, $mailContent);                     
                }

                
                if (defined('WEBHOOKURL_LOGS_DISCORD')) {
                    $webhookurl = WEBHOOKURL_LOGS_DISCORD;
                    $webhookMsg = json_encode([
                        "thread_name"  => ORGANISATION .' - '. ORGANISATION_ACRONYM ,
                        "content" => substr($msgWebhookLog,0,1999), // < 2000 char
                    ], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
                
                    $ch = curl_init($webhookurl);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $webhookMsg);
                    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
                    curl_setopt($ch, CURLOPT_HEADER, 0);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

                    $response = curl_exec($ch);
                    // If you need to debug, or find out why you can't send message uncomment line below, and execute script.
                    echo $response;
                    curl_close($ch);
                }
                            
            }
        }
    }

    //////////////////////////////////////////////////////////////////////////
    // Log in reserved file: sql_queries_errors.log. Should be used exclusively with SysmaPDO's debug stuff
    public static function logSql($_msg, $_logLvl = DEBUG)
    {
        if (self::$logFileReady) {
            if (DEBUG_SPLIT_GENERAL_LOGS_AND_SQL_LOGS_IN_2_FILES) {
                fwrite(self::$logSqlFile, $_msg . "\n");
            } else {
                fwrite(self::$logFile, $_msg . "\n");
            }
        }
    }

    //////////////////////////////////////////////////////////////////////////
    // special PG log funcs
    public static function getPgLog($_app, $_startTime, $_endTime) {

        $qry = $_app['pdo']->prepare("SELECT * FROM xx_dev_debug.f_log_access( :starttime::timestamp , :endtime::timestamp)");
        $qry->bindParam(':starttime', $_startTime);
        $qry->bindParam(':endtime', $_endTime);
        $qry->execute();
        return $qry->fetchAll();
    }

    public static function startPgLog() {
        if (DEBUG_LVL2) {
            $now = new DateTime();
            self::$startTime[self::$nbProfile++] = $now->format('Y-m-d H:i:s');
        }
    }

    public static function endPgLogAndGetResults($_app) {
        if (DEBUG_LVL2) {
            $now = new DateTime();
            $now->add(DateInterval::createFromDateString('1 seconds'));

            return Debug::getPgLog($_app, self::$startTime[--self::$nbProfile], $now->format('Y-m-d H:i:s'));
        }
    }

    public static function endPgLogAndPrintResults($_app) {
        if (DEBUG_LVL2) {
            // $now = gettimeofday();
            // $now = date('Y-m-d H:i:s');
            // date_add($now, DateInterval::createFromDateString('PT1S'));
            $res  = Debug::endPgLogAndGetResults($_app);
            foreach ($res as $r) {
                // echo $r;
                // print_r($r);
                // echo $r['id_line'];
                if (
                    startsWith($r['log_statmt'], 'parse pdo_stmt')
                    or startsWith($r['log_statmt'], 'bind pdo_stmt')
                    or startsWith($r['log_statmt'], 'statement: DEALLOCATE pdo_stmt')
                ) {
                    continue;
                }
                for ($i = 0; $i < self::$nbProfile; $i++) {
                    echo '-';
                }
                echo $r['log_date'];
                echo "  |  ";
                echo $r['log_statmt'];
                echo '<br>';
            }
        }
    }
} // End of class Debug

//little helpers
function startsWith($haystack, $needle) {
    $length = strlen($needle);
    return substr($haystack, 0, $length) === $needle;
}
