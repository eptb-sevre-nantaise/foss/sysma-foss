<?php

/*
 * récupère la liste des paramètres de l'application, leurs types et values
 */

function loadAppParameters($app)
{

    $qry = $app['pdo']->prepare('select * from ' . SCHEMA . '.sysma_conf order by "display_order" asc');
    $qry->execute();
    return $qry->fetchAll(PDO::FETCH_ASSOC);
}

function loadAppParameter($app, $alias)
{

    $qry = $app['pdo']->prepare('select * from ' . SCHEMA . '.sysma_conf where alias = :alias');
    $qry->bindParam(':alias', $alias, PDO::PARAM_STR);
    $qry->execute();
    return $qry->fetch(PDO::FETCH_ASSOC);
}

function updateAppParameter($app, $alias, $value)
{

    $qry = $app['pdo']->prepare('update ' . SCHEMA . '.sysma_conf set value = :value where alias = :alias');
    $qry->bindParam(':value', $value, PDO::PARAM_STR);
    $qry->bindParam(':alias', $alias, PDO::PARAM_STR);
    return $qry->execute();
}

function loadMultiGeom($app, $array_geoobjects_id)
{

    if (count($array_geoobjects_id) > 0) {
        $geoobjects_id = implode(',', $array_geoobjects_id);


        $qry = $app['pdo']->prepare("select "
            . "st_union(geom) as geom, "
            . "sum(st_length(geom)) as longueur, "
            . "sum(st_area(geom)) as surface, "
            . "st_perimeter(st_union(geom)) as perimetre, "
            . "ST_AsText(ST_Centroid(ST_Transform(st_union(geom),4326))) as centroid, "
            . "ST_Perimeter(ST_Extent(geom)) as perimetre "
            . "from " . SCHEMA . ".sysma_object where sysma_object_id in (" . $geoobjects_id . ")");
        $qry->execute();
        $res = $qry->fetchObject();

        $tabC = explode(' ', $res->centroid);
        $centre = 'LatLng(' . substr($tabC[1], 0, -1) . ',' . substr($tabC[0], 6, -1) . ')';
        $res->surface != 0 ? $type_geom = 'ST_Polygon' : $res->longueur != 0 ? $type_geom = 'ST_Linestring' : $type_geom = 'ST_Point';

        return ['centre' => $centre, 'geom' => $res->geom, 'perimetre' => $res->perimetre, 'longueur' => $res->longueur, 'surface' => $res->surface, 'perimetre' => $res->perimetre, 'type_geom' => $type_geom];
    } else {
        return false;
    }
}

function resize_image($file, $suffix, $w, $h, $crop = FALSE)
{
    list($width, $height) = getimagesize($file);
    $r = $width / $height;
    if ($crop) {
        if ($width > $height) {
            $width = ceil($width - ($width * abs($r - $w / $h)));
            $width = $height;
        } else {
            $height = ceil($height - ($height * abs($r - $w / $h)));
            $height = $width;
        }
        $newwidth = $w;
        $newheight = $h;
    } else {
        if ($w / $h > $r) {
            $newwidth = $h * $r;
            $newheight = $h;
        } else {
            $newheight = $w / $r;
            $newwidth = $w;
        }
    }
    $src = imagecreatefromjpeg($file);
    $dst = imagecreatetruecolor($newwidth, $newheight);
    imagecopyresampled($dst, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
    imagejpeg($dst, substr($file, 0, -4) . '_' . $suffix . '.jpg');

    return true;
}

function loadUserFromSession($app, $request)
{

    /* cookie ? */
    $cookies = $request->cookies;

    if ($cookies->has('uid') and $cookies->has('sid') and (($app['session']->get('is_known') === null) or ($app['session']->get('user_id') == ANONYMID))) {
        // echo "WIP Arl: user is not know BUT we have a cookie!<br>";
        $user = findCookieUser($cookies->get('uid'), $app);
        if (is_object($user) and passwordVerifySysma($user->password, $cookies->get('sid'))) {
            // echo "WIP Arl: cookie's sid valid<br>";

            $U = new User($user->user_id, $app);
            $U->updateLastConnexion();
            $U->tracksql_filter('sysma_foss_v3_cookie');
            $app['session']->set('user_id', $U->user_id);
            $app['session']->set('user', $U->user);
            $app['session']->set('is_known', true);
            $app['session']->set('map_width', $U->map_width);
            $app['session']->set('is_admin', $U->isAdmin());
            $app['session']->set('has_cadastre_access', hasCadastreAccess($app, $U->user_id));
            $app['session']->set('organisation_id', $U->organisation_id);
            $app['session']->set('default_center', $U->default_center);
            $app['session']->set('default_zoom', $U->default_zoom);
            $app['session']->set('has_cadastre_access', hasCadastreAccess($app, $U->user_id));
            $app['session']->set('has_rpg_access', hasRpgAccess($app, $U->user_id));
            $app['session']->set('has_gis_export_privilege', hasGISExportPrivilege($app, $U->user_id));
            $app['session']->set('snap_options', $U->snap_options);
            // echo "WIP Arl: cookie recover user: ".$app['session']->get('user')."<br>";
        } else {
            // echo "WIP Arl: anonymous user<br>";
            $app['session']->set('user_id', ANONYMID);
            $app['session']->set('is_known', false);
            $U = new User(ANONYMID, $app);
        }
    } elseif (($app['session']->get('user_id') !== null) and ($app['session']->get('user_id') != ANONYMID)) {
        // echo "WIP Arl: user is known by the session<br>";
        // var_dump($app['session']);
        $U = new User($app['session']->get('user_id'), $app);
        $app['session']->set('user_id', $U->user_id);
        $app['session']->set('user', $U->user);
        $app['session']->set('is_known', true);
        $app['session']->set('map_width', $U->map_width);
        $app['session']->set('is_admin', $U->isAdmin());
        $app['session']->set('has_cadastre_access', hasCadastreAccess($app, $U->user_id));
        $app['session']->set('has_rpg_access', hasRpgAccess($app, $U->user_id));
        $app['session']->set('organisation_id', $U->organisation_id);
        $app['session']->set('default_center', $U->default_center);
        $app['session']->set('default_zoom', $U->default_zoom);
        $app['session']->set('has_cadastre_access', hasCadastreAccess($app, $U->user_id));
        $app['session']->set('has_gis_export_privilege', hasGISExportPrivilege($app, $U->user_id));
        $app['session']->set('snap_options', $U->snap_options);
        // echo "WIP Arl: session recover user: ".$app['session']->get('user')."<br>";
    } else {
        // echo "WIP Arl: anonymous user<br>";
        $app['session']->set('user_id', ANONYMID);
        $app['session']->set('is_known', false);
        $U = new User(ANONYMID, $app);
    }
    return $U;
}

function userThematicAnalysis($app, $user_id)
{

    $tabAT = null;

    $qry = $app['pdo']->prepare("select at.theme_analysis_id as theme_analysis_id, at.object_type as object_type, at.type_id as type_id from " . SCHEMA . ".l_theme_analysis_user l LEFT JOIN " . SCHEMA . ".theme_analysis at ON at.theme_analysis_id = l.theme_analysis_id where l.user_id = :id");
    $qry->bindParam(':id', $user_id, PDO::PARAM_INT);
    $qry->execute();
    while ($u = $qry->fetchObject()) {
        $tabAT[$u->object_type][$u->type_id] = $u->theme_analysis_id;
    }

    return $tabAT;
}

function userFilters($app, $user_id)
{

    $tabFiltres = $tabFiltresCond = array();

    $qry = $app['pdo']->prepare("select f.filter_id as filter_id, f.object_type as object_type, l.sysma_object_type_id as sysma_object_type_id, l.sysma_action_type_id as sysma_action_type_id, l.condition as condition from " . SCHEMA . ".l_filter_user l LEFT JOIN " . SCHEMA . ".filter f ON f.filter_id = l.filter_id where l.user_id = :id order by l_filter_user_id asc");
    $qry->bindParam(':id', $user_id, PDO::PARAM_INT);
    $qry->execute();
    while ($u = $qry->fetchObject()) {
        if ($u->object_type === 'sysma_object') {
            $tabFiltres[$u->object_type][$u->sysma_object_type_id][] = $u->filter_id;
            $tabFiltresCond[$u->object_type][$u->sysma_object_type_id][$u->filter_id]['condition'] = $u->condition;
        }
        if ($u->object_type === 'sysma_action') {
            $tabFiltres[$u->object_type][$u->sysma_action_type_id][] = $u->filter_id;
            $tabFiltresCond[$u->object_type][$u->sysma_action_type_id][$u->filter_id]['condition'] = $u->condition;
        }
    }

    return ['tabFiltres' => $tabFiltres, 'tabFiltresCond' => $tabFiltresCond];
}

function hasCadastreAccess($app, $id_user)
{

    $qry = $app['pdo']->prepare("select * from " . SCHEMA . ".user where user_id = :id and 'cadastre' = ANY(privileges)");
    $qry->bindParam(':id', $id_user, PDO::PARAM_INT);
    $qry->execute();
    return $qry->rowCount() > 0;
}

function hasRpgAccess($app, $id_user)
{

    $qry = $app['pdo']->prepare("select * from " . SCHEMA . ".user where user_id = :id and 'rpg' = ANY(privileges)");
    $qry->bindParam(':id', $id_user, PDO::PARAM_INT);
    $qry->execute();
    return $qry->rowCount() > 0;
}

function hasGISExportPrivilege($app, $id_user)
{

    $qry = $app['pdo']->prepare("select * from " . SCHEMA . ".user where user_id = :id and 'gis_export' = ANY(privileges)");
    $qry->bindParam(':id', $id_user, PDO::PARAM_INT);
    $qry->execute();
    return $qry->rowCount() > 0;
}

function hasPGExportPrivilege($app, $id_user)
{

    $qry = $app['pdo']->prepare("select * from " . SCHEMA . ".user where user_id = :id and 'pg_export' = ANY(privileges)");
    $qry->bindParam(':id', $id_user, PDO::PARAM_INT);
    $qry->execute();
    return $qry->rowCount() > 0;
}

function objectTypeLayerLoadAndZoomInfos($app, $layer)
{


    $qry = $app['pdo']->prepare("select load_on_pan,zoom_min,zoom_max from " . SCHEMA . ".sysma_object_type where sysma_object_type_id = :id");
    $qry->bindParam(':id', $layer, PDO::PARAM_INT);
    $qry->execute();
    $res = $qry->fetch();
    return $res;
}

function objectTypeLayerLoadOnPan($app, $layer)
{

    $qry = $app['pdo']->prepare("select load_on_pan, zoom_min, zoom_max, geometry_type from " . SCHEMA . ".sysma_object_type where sysma_object_type_id = :id");
    $qry->bindParam(':id', $layer, PDO::PARAM_INT);
    $qry->execute();
    $res = $qry->fetch();
    return $res;
}

function workTypeLayerLoadAndZoomInfos($app, $layer)
{

    $qry = $app['pdo']->prepare("select  load_on_pan,zoom_min,zoom_max  from " . SCHEMA . ".sysma_action_type where sysma_action_type_id = :id");
    $qry->bindParam(':id', $layer, PDO::PARAM_INT);
    $qry->execute();
    $res = $qry->fetch();
    return $res;
}

function workTypeLayerLoadOnPan($app, $layer)
{

    $qry = $app['pdo']->prepare("select load_on_pan from " . SCHEMA . ".sysma_action_type where sysma_action_type_id = :id");
    $qry->bindParam(':id', $layer, PDO::PARAM_INT);
    $qry->execute();
    $res = $qry->fetch();
    return $res['load_on_pan'];
}

function otherLayerLoadAndZoomInfos($app, $layer)
{

    // var_dump($layer) ;
    $qry = $app['pdo']->prepare("select  load_on_pan,zoom_min,zoom_max  from " . SCHEMA . ".other_layer where other_layer_id = :id");
    $qry->bindParam(':id', $layer, PDO::PARAM_INT);
    $qry->execute();
    $res = $qry->fetch();
    return $res;
}

function otherLayerLoadOnPan($app, $layer)
{

    // var_dump($layer) ;
    $qry = $app['pdo']->prepare("select load_on_pan from " . SCHEMA . ".other_layer where other_layer_id = :id");
    $qry->bindParam(':id', $layer, PDO::PARAM_INT);
    $qry->execute();
    $res = $qry->fetch();
    return $res['load_on_pan'];
}

function otherLayerType($app, $layer)
{

    $qry = $app['pdo']->prepare("select layer_type from " . SCHEMA . ".other_layer where other_layer_id = :id");
    $qry->bindParam(':id', $layer, PDO::PARAM_INT);
    $qry->execute();
    $res = $qry->fetch();
    return $res['layer_type'];
}

function countSysmaObjects($app, $layerId)
{

    $qry = $app['pdo']->prepare("select count(o.sysma_object_id) as compte from " . SCHEMA . ".sysma_object o
        inner join  " . SCHEMA . ".sysma_object_type t on t.sysma_object_type_id = o.sysma_object_type_id 
       where o.sysma_object_type_id = :id");

    $qry->bindParam(':id', $layerId, PDO::PARAM_INT);
    $qry->execute();
    $res = $qry->fetchObject();
    // var_dump($res);
    return $res->compte;
}

function countSysmaActions($app, $layerId)
{

    $qry = $app['pdo']->prepare("select count(o.sysma_action_id) as compte from " . SCHEMA . ".sysma_action o
        inner join  " . SCHEMA . ".sysma_action_type t on t.sysma_action_type_id = o.sysma_action_type_id 
       where o.sysma_action_type_id = :id");

    $qry->bindParam(':id', $layerId, PDO::PARAM_INT);
    $qry->execute();
    $res = $qry->fetchObject();
    return $res->compte;
}

function countSysmaRelations($app, $layerId)
{

    $qry = $app['pdo']->prepare("select count(o.sysma_relation_id) as compte from " . SCHEMA . ".sysma_relation o
        inner join  " . SCHEMA . ".sysma_relation_type t on t.sysma_relation_type_id = o.sysma_relation_type_id 
       where o.sysma_relation_type_id = :id");

    $qry->bindParam(':id', $layerId, PDO::PARAM_INT);
    $qry->execute();
    $res = $qry->fetchObject();
    // var_dump($res);
    return $res->compte;
}

function reloadGeoLayers($app, $center, $zoom)
{


    $toReturn = null;




    foreach ((array) $app['session']->get('userObjectTypeSelection') as $layer => $infos) {

        $layerInfos = objectTypeLayerLoadAndZoomInfos($app, $layer);



        if (
            $layerInfos['load_on_pan'] === 1
            and ($layerInfos['zoom_min'] == 0 or ($layerInfos['zoom_min'] != 0 and intval($zoom) >= $layerInfos['zoom_min']))
            and ($layerInfos['zoom_max'] == 0 or ($layerInfos['zoom_max'] != 0 and intval($zoom) <= $layerInfos['zoom_max']))
        ) {

            $toReturn .= "refreshLayer('objecttype', '$layer', null, null, { 'center' : '$center', 'zoom' : $zoom });
                ";
        } elseif ($layerInfos['load_on_pan'] === 1 and (($layerInfos['zoom_min'] != 0 and intval($zoom) > $layerInfos['zoom_min']) or ($layerInfos['zoom_max'] != 0 and intval($zoom) < $layerInfos['zoom_max']))) {
            $toReturn = "removeLayerAndKeepItSelected('objecttype', '$layer', null, null,null,null);";
            /* $toReturn .= "
              $('#layerobjecttype" . $layer . "LayerController').attr('checked',false);
              removeLayer('objecttype',$layer,'" . $infos['layer'] . "'," . $infos['leaflet_id'] . "," . $infos['layerstyle'] . "," . $infos['parentid'] . ");
              "; */
        } else {
            /* $toReturn .= "$('#layerobjecttype" . $layer . "LayerController').attr('disabled',false); "; */
        }
    }

    foreach ((array) $app['session']->get('userSysmaActionTypeSelection') as $layer => $infos) {

        $layerInfos = workTypeLayerLoadAndZoomInfos($app, $layer);

        if (
            $layerInfos['load_on_pan'] === 1
            and ($layerInfos['zoom_min'] == 0 or ($layerInfos['zoom_min'] != 0 and intval($zoom) >= $layerInfos['zoom_min']))
            and ($layerInfos['zoom_max'] == 0 or ($layerInfos['zoom_max'] != 0 and intval($zoom) <= $layerInfos['zoom_max']))
        ) {

            $toReturn .= "refreshLayer('actiontype', '$layer', null, null, { 'center' : '$center', 'zoom' : $zoom });
                ";
        } elseif ($layerInfos['load_on_pan'] === 1 and (($layerInfos['zoom_min'] != 0 and intval($zoom) > $layerInfos['zoom_min']) or ($layerInfos['zoom_max'] != 0 and intval($zoom) < $layerInfos['zoom_max']))) {
            $toReturn = "removeLayerAndKeepItSelected('actiontype', '$layer', null, null,null,null);";
            /*   $toReturn .= "
              $('#layertypesysma_action" . $layer . "LayerController').attr('checked',false);
              removeLayer('typesysma_action',$layer,'" . $infos['layer'] . "'," . $infos['leaflet_id'] . "," . $infos['layerstyle'] . "," . $infos['parentid'] . ");
              "; */
        } else {
            /* $toReturn .= "$('#layertypesysma_action" . $layer . "LayerController').attr('disabled',false);
              "; */
        }
    }

    foreach ((array) $app['session']->get('userOtherLayersSelection') as $layer => $infos) {


        $layerInfos = otherLayerLoadAndZoomInfos($app, $layer);


        if (
            $layerInfos['load_on_pan'] === 1
            and ($layerInfos['zoom_min'] == 0 or ($layerInfos['zoom_min'] != 0 and intval($zoom) >= $layerInfos['zoom_min']))
            and ($layerInfos['zoom_max'] == 0 or ($layerInfos['zoom_max'] != 0 and intval($zoom) <= $layerInfos['zoom_max']))
        ) {


            $toReturn .= "refreshLayer('" . otherLayerType($app, $layer) . "', '$layer', null, null, { 'center' : '$center', 'zoom' : $zoom });
                ";
        } elseif ($layerInfos['load_on_pan'] === 1 and (($layerInfos['zoom_min'] != 0 and intval($zoom) < $layerInfos['zoom_min']) or ($layerInfos['zoom_max'] != 0 and intval($zoom) > $layerInfos['zoom_max']))) {
            $toReturn = "removeLayerAndKeepItSelected('" . otherLayerType($app, $layer) . "', '$layer', null, null,null,null);";
            /*  $toReturn .= "
              $('#layer" . otherLayerType($app, $layer) . $layer . "LayerController').attr('checked',false);
              removeLayer('" . otherLayerType($app, $layer) . "',$layer,'" . $infos['layer'] . "'," . $infos['leaflet_id'] . "," . $infos['layerstyle'] . "," . $infos['parentid'] . ");
              "; */
        } else {
            /*  $toReturn .= "  $('#layer" . otherLayerType($app, $layer) . "" . $layer . "LayerController').attr('disabled',false);                "; */
        }
    }





    return "<script type='text/javascript'>$toReturn</script>";
}

function reloadGeoLayers2($app, $center, $zoom)
{

    $toReturn = null;
    $selectedSysmaObjectLayers = $app['session']->get('userObjectTypeSelection');
    //  var_dump($selectedSysmaObjectLayers) ;
    foreach ($selectedSysmaObjectLayers as $layer => $infos) {
        // var_dump($layer) ;
        //$TyO = new SysmaObjectType($layer, $app) ;
        $toReturn .= "\"/geojson/objecttype/$layer?center=$center&zoom=$zoom\",";
    }
    // echo $app['session']->get('update-map-settings') ;
    //  echo $toReturn ; 


    return "<script type='text/javascript'>refreshLayers([$toReturn]); console.log('Update map setting : " . $app['session']->get('update-map-settings') . "');</script>";
}

function updateDefaultCoords($app, $id_user, $center, $zoom)
{

    $qry = $app['pdo']->prepare('update ' . SCHEMA . '.user SET default_center = :c, default_zoom = :z where user_id = :userid');
    $qry->bindParam(':userid', $id_user, PDO::PARAM_INT);
    $qry->bindParam(':c', $center);
    $qry->bindParam(':z', $zoom);
    return $qry->execute();
}

function updateBaseLayer($app, $id_user, $baseLayer)
{

    $qry = $app['pdo']->prepare('update ' . SCHEMA . '.user SET base_layer = :l where user_id = :userid');
    $qry->bindParam(':userid', $id_user, PDO::PARAM_INT);
    $qry->bindParam(':l', $baseLayer);
    return $qry->execute();
}

function addOtherBaseLayer($app, $id_user, $baseLayer)
{

    $qry = $app['pdo']->prepare('insert into ' . SCHEMA . '.l_ref_layer_user (layer, user_id) select :l,:userid where not exists (select * from ' . SCHEMA . '.l_ref_layer_user where layer = :l2 and user_id = :userid)');
    $qry->bindParam(':userid', $id_user, PDO::PARAM_INT);
    $qry->bindParam(':l', $baseLayer);
    $qry->bindParam(':l2', $baseLayer);
    return $qry->execute();
}

function removeOtherBaseLayer($app, $id_user, $baseLayer)
{

    $qry = $app['pdo']->prepare('delete from ' . SCHEMA . '.l_ref_layer_user where layer = :l and user_id = :userid');
    $qry->bindParam(':userid', $id_user, PDO::PARAM_INT);
    $qry->bindParam(':l', $baseLayer);
    return $qry->execute();
}

/*
 * 
 * l'user a t'il accès aux objets de types et status fournis ?
 * @param integer $iduser, undefined = 0
 * @paraminteger $organisation_id, undefined = 0
 * @param integer $idobjecttype
 * @param text $statuobjet, undefined = tous
 * @param integer $idsysma_action, undefined = null
 * @param text statussysma_action, undefined = null
 * @param integer idssysma_action, id organisation mo des sysma_action
 * @param text $droit
 * @param silex app
 * @return boolean
 * 
 * 
 */

function hasAccess($app, $id_user, $organisation_id = null, $idobjecttype = null, $statusobjet = null, $idtypesysma_action = null, $statussysma_action = null, $organisation_id_sysma_action = null, $droit)
{




    $pdo = $app['pdo'];
    $condT = $condStObj = $condStTr = $condStr = $condStrTr = $condTyO = null;

    if ($organisation_id !== null) {
        $condStr = ' and (:organisation_id = ANY(sysma_object_organisation_id_list) or sysma_object_organisation_id_list = \'{0}\' ) ';
    }

    if ($statusobjet !== null) {
        $condStObj = ' and (:statusobjet = ANY(sysma_object_status_list) or sysma_object_status_list = \'{tous}\') ';
    }

    // ! les condition statuss et organisation de trvaaux étaient incluse dans le if tu ype sysma_action
    if ($idtypesysma_action !== null) {
        $condT = ' and ((:idtypesysma_action = ANY(sysma_action_type_id_list) or sysma_action_type_id_list = \'{0}\') and sysma_action_type_id_list != \'{NULL}\')  ';
    }

    if ($statussysma_action !== null) {
        $condStTr = ' and (:statussysma_action = ANY(sysma_action_status_list) or sysma_action_status_list = \'{tous}\') ';
    }
    // organisation sysma_action
    //echo 'id t dans ca access : '.$idssysma_action.'<br>' ;
    if ($organisation_id_sysma_action !== null) {
        $condStrTr = ' and (:organisation_id_sysma_action = ANY(sysma_action_organisation_id_list) or sysma_action_organisation_id_list = \'{0}\') ';
    }


    if ($idobjecttype !== null) {
        $condTyO = ' and (:idobjecttype = ANY(sysma_object_type_id_list) or sysma_object_type_id_list = \'{0}\')  ';
    }


    $qry = $pdo->prepare('select * from ' . SCHEMA . '.user_privilege where '
        . 'user_id = :iduser '
        . $condStr
        . $condTyO
        . ' and :droit = ANY(privilege_list) ' . $condStObj . $condT . $condStTr . $condStrTr);

    $qry->bindParam(':iduser', $id_user, PDO::PARAM_INT);
    $qry->bindParam(':droit', $droit);

    if ($organisation_id !== null) {
        $qry->bindParam(':organisation_id', $organisation_id, PDO::PARAM_INT);
    }

    if ($statusobjet !== null) {
        $qry->bindParam(':statusobjet', $statusobjet);
    }

    if ($idtypesysma_action !== null) {
        $qry->bindParam(':idtypesysma_action', $idtypesysma_action, PDO::PARAM_INT);
    }
    if ($statussysma_action !== null) {
        $qry->bindParam(':statussysma_action', $statussysma_action);
    }
    // organisation
    if ($organisation_id_sysma_action !== null) {
        $qry->bindParam(':organisation_id_sysma_action', $organisation_id_sysma_action);
    }

    if ($idobjecttype !== null) {
        $qry->bindParam(':idobjecttype', $idobjecttype, PDO::PARAM_INT);
    }


    $qry->execute() or die('erreur ' . $organisation_id_sysma_action . ' ' . $statussysma_action);
    //$qry->debugDumpParams();


    if ($qry->rowCount() > 0) {
        return true;
    } else {
        return false;
    }
}

/*
 * vérifie si le user peut accéder aux sysma_action (types de travaux) d'un type d'objet, quels qu'ils soient
 * c'est le cas si pour un type d'objet donné le user a le droit 'contributeur' ou 'gestionnaire'
 * et que le sysma_action_type_id_list est != {NULL}
 */

function hasAccessSysmaActions($app, $id_user, $idobjecttype, $organisation_id_objet, $droit)
{

    $condS = null;
    if ($organisation_id_objet != null)
        $condS = ' and (:idstobj = ANY(sysma_object_organisation_id_list) or sysma_object_organisation_id_list = \'{0}\') ';


    $qry = $app['pdo']->prepare('select * from ' . SCHEMA . '.user_privilege where '
        . 'user_id = :iduser '
        . $condS
        . ' and (:idobjecttype = ANY(sysma_object_type_id_list) or sysma_object_type_id_list = \'{0}\')  '
        . ' and :droit = ANY(privilege_list) and sysma_action_type_id_list != \'{NULL}\' ');
    $qry->bindParam(':iduser', $id_user, PDO::PARAM_INT);
    if ($organisation_id_objet != null)
        $qry->bindParam(':idstobj', $organisation_id_objet, PDO::PARAM_INT);
    $qry->bindParam(':idobjecttype', $idobjecttype, PDO::PARAM_INT);
    $qry->bindParam(':droit', $droit);

    $qry->execute();
    //$qry->debugDumpParams();


    if ($qry->rowCount() > 0) {
        return true;
    } else {
        return false;
    }
}

function findElementById($id, $table, $key_col, $classe_name, $app)
{

    $pdo = $app['pdo'];
    $req = 'select ' . $key_col . ' from ' . SCHEMA . '.' . $table . ' where ' . $key_col . ' = :id';
    $qry = $pdo->prepare($req);

    $qry->bindParam(':id', $id);
    $qry->execute();
    // var_dump($pdo->errorInfo()) ;
    $tab = array();
    while ($o = $qry->fetchObject()) {

        $tab[] = new $classe_name($o->$key_col, $app);
    }

    return $tab;
}

function findElementByName($name, $table, $name_col, $key_col, $classe_name, $app)
{

    $pdo = $app['pdo'];
    $req = 'select ' . $key_col . ' from ' . SCHEMA . '.' . $table . ' where lower(unaccent(' . $name_col . ')) ilike lower(unaccent(:name))';
    // echo $req;
    $qry = $pdo->prepare($req);

    $name_rec = '%' . $name . '%';
    $qry->bindParam(':name', $name_rec);
    $qry->execute();
    //  var_dump($pdo->errorInfo());
    $tab = array();
    while ($o = $qry->fetchObject()) {
        $Obj = null;
        $Obj = new $classe_name($o->$key_col, $app);
        if ($classe_name == 'SysmaAction') {
            $Obj->loadSysmaObjects();
        }

        $tab[] = $Obj;
    }

    return $tab;
}

/*
 * create shape layers with privilege of the users
 */

function createSysmaGISLayers($app, $res, $sysma_action, $geom, $folder, $buffer = 0, $gis_format)
{


    mkdir($folder);
    mkdir($folder . '_EXPORT_REPORT');
    $foldersToDelete[] = $folder;


    $zip = new ZipArchive;
    $zipFile = $folder . date('Ymd_His') . '_export_layers_sysma.zip';

    $its_the_first_layer = true;

    foreach ($res['resTO'] as $table) {

        if ($buffer == 0 and $geom == null) {
            $q = 'select o.*, null::numeric as "delete" from ' . MOULSCHEMA . '.' . $table['sysma_object_type_alias'] . ' o INNER JOIN ' . SCHEMA . '.user_privilege as d
            ON (o.sysma_object_type_id = ANY(d.sysma_object_type_id_list) or d.sysma_object_type_id_list = \'{0}\') and 
            d.user_id = ' . $app['session']->get('user_id') . ' and
            (d.sysma_object_status_list = \'{tous}\' or o.status = ANY(d.sysma_object_status_list)) and 
            (d.sysma_object_organisation_id_list = \'{0}\' or o.organisation_id = ANY(d.sysma_object_organisation_id_list)) and 
            (\'lecteur\' = ANY(d.privilege_list) or (\'contributeur\' = ANY(d.privilege_list) and o.modified_by = d.user_id))';
        } else {
            $q = 'select o.*, null::numeric as "delete" from ' . MOULSCHEMA . '.' . $table['sysma_object_type_alias'] . ' o INNER JOIN ' . SCHEMA . '.user_privilege as d
            ON (o.sysma_object_type_id = ANY(d.sysma_object_type_id_list) or d.sysma_object_type_id_list = \'{0}\') and 
            d.user_id = ' . $app['session']->get('user_id') . ' and
            (d.sysma_object_status_list = \'{tous}\' or o.status = ANY(d.sysma_object_status_list)) and 
            (d.sysma_object_organisation_id_list = \'{0}\' or o.organisation_id = ANY(d.sysma_object_organisation_id_list)) and 
            (\'lecteur\' = ANY(d.privilege_list) or (\'contributeur\' = ANY(d.privilege_list) and o.modified_by = d.user_id)) 
            where ST_intersects(ST_Buffer(\'' . $geom . '\',' . max([0.0001, $buffer]) . '),geom)';
        }


        if ($gis_format == 'shp') {
            $command = REP_BIN_GDAL . '/ogr2ogr   -overwrite -f PostgreSQL -f "ESRI Shapefile" ' . $folder . $table['sysma_object_type_alias'] . '.shp  PG:"host=' . MOULDBHOST . ' user=' . MOULDBUSER . ' password=' . MOULDBPWD . ' dbname=' . DBNAME . ' port=5432" -sql "' . $q . '" ';
        }

        if ($gis_format == 'gpkg') {

            if ($its_the_first_layer) {
                $cde = '-overwrite';
                $its_the_first_layer = false;
            } else {
                $cde = '-update';
            }

            $command = REP_BIN_GDAL . '/ogr2ogr   ' . $cde . '   -f PostgreSQL -f "GPKG" ' . $folder . 'export_layers_sysma.gpkg  PG:"host=' . MOULDBHOST . ' user=' . MOULDBUSER . ' password=' . MOULDBPWD . ' dbname=' . DBNAME . ' port=5432" -nln "' . $table['sysma_object_type_alias'] . '" -sql "' . $q . '" ';
        }


        $resExec = exec($command . ' 2>&1', $output, $return);


        if ($return === 0) {


            if ($gis_format == 'shp') {

                if ($zip->open($zipFile, ZipArchive::CREATE) == TRUE) {
                    $zip->addFile($folder . $table['sysma_object_type_alias'] . '.shp', $table['sysma_object_type_alias'] . '.shp');
                    $zip->addFile($folder . $table['sysma_object_type_alias'] . '.dbf', $table['sysma_object_type_alias'] . '.dbf');
                    $zip->addFile($folder . $table['sysma_object_type_alias'] . '.prj', $table['sysma_object_type_alias'] . '.prj');
                    $zip->addFile($folder . $table['sysma_object_type_alias'] . '.shx', $table['sysma_object_type_alias'] . '.shx');

                    file_put_contents($folder . '_EXPORT_REPORT/02_Informations.txt', implode($output, '\n'));
                    $zip->addFile($folder . '_EXPORT_REPORT/02_Informations.txt', '_EXPORT_REPORT/02_Informations.txt');
                }
            }
        } else {
            $zipFile = false;
        }
    }


    if ($gis_format == 'gpkg') {

        if ($zip->open($zipFile, ZipArchive::CREATE) == TRUE) {
            $zip->addFile($folder . 'export_layers_sysma.gpkg', 'export_layers_sysma.gpkg');
        } else {
            $zipFile = false;
        }
    }


    if ($sysma_action == 'true') {

        foreach ($res['resTT'] as $table) {

            // besoin de connaitre l'alias du type objet
            $TO = new SysmaObjectType($table['sysma_object_type_id'], $app);
            $nom_table_sysma_action = $TO->sysma_object_type_alias . '_act_' . $table['sysma_action_type_alias'];

            if ($buffer == 0 and $geom == null) {
                $q = 'select * from ' . MOULSCHEMA . '.' . $nom_table_sysma_action;
            } else {
                $q = 'select * from ' . MOULSCHEMA . '.' . $nom_table_sysma_action . ' where ST_intersects(ST_Buffer(\'' . $geom . '\',' . max([0.0001, $buffer]) . '),geom)';
            }


            if ($gis_format == 'shp') {

                $command = REP_BIN_GDAL . '/ogr2ogr   -overwrite -f PostgreSQL -f "ESRI Shapefile" ' . $folder . $nom_table_sysma_action . '.shp  PG:"host=' . MOULDBHOST . ' user=' . MOULDBUSER . ' password=' . MOULDBPWD . ' dbname=' . MOULDBNAME . ' port=' . MOULDBPORT . '" -sql "' . $q . '" ';
            }


            if ($gis_format == 'gpkg') {

                if ($its_the_first_layer) {
                    $cde = '-overwrite';
                    $its_the_first_layer = false;
                } else {
                    $cde = '-update';
                }

                $command = REP_BIN_GDAL . '/ogr2ogr  ' . $cde . ' -f PostgreSQL -f "GPKG" ' . $folder . 'export_layers_sysma.gpkg  PG:"host=' . MOULDBHOST . ' user=' . MOULDBUSER . ' password=' . MOULDBPWD . ' dbname=' . MOULDBNAME . ' port=' . MOULDBPORT . '" -nln "' . $nom_table_sysma_action . '" -sql "' . $q . '" ';
            }


            $resExec = exec($command . ' 2>&1', $output, $return);

            if ($return === 0) {


                if ($zip->open($zipFile, ZipArchive::CREATE) == TRUE) {
                    $zip->addFile($folder . $nom_table_sysma_action . '.shp', $nom_table_sysma_action . '.shp');
                    $zip->addFile($folder . $nom_table_sysma_action . '.dbf', $nom_table_sysma_action . '.dbf');
                    $zip->addFile($folder . $nom_table_sysma_action . '.prj', $nom_table_sysma_action . '.prj');
                    $zip->addFile($folder . $nom_table_sysma_action . '.shx', $nom_table_sysma_action . '.shx');

                    //  file_put_contents($folder . '_EXPORT_REPORT/02_Informations.txt', implode($output, '\n'));
                    // $zip->addFile($folder . '_EXPORT_REPORT/02_Informations.txt', '_EXPORT_REPORT/02_Informations.txt');
                } else {
                    $zipFile = false;
                }
            } else {
                $zipFile = false;
            }
        }

        if ($gis_format == 'gpkg') {

            if ($zip->open($zipFile, ZipArchive::CREATE) == TRUE) {
                $zip->addFile($folder . 'export_layers_sysma.gpkg', 'export_layers_sysma.gpkg');
            } else {
                $zipFile = false;
            }
        }
    }


    $zip->close();
    return ['zipFile' => $zipFile, 'foldersToDelete' => $foldersToDelete];
}

/*
 * créer un shape avec les infos du cadastre
 */

function createCadastreInfosObjectShape($app, $geom, $folder, $buffer = 0, $seulement_destinataire_avis_impot = 0)
{



    mkdir($folder);
    $foldersToDelete[] = $folder;

    if ($buffer == 0) {
        $q = 'select geo_parcelle,geom,parc_jdatat,parc_ccodep,parc_ccocom,parc_libcom,parc_ccosec,parc_dnupla,parc_dnvoiri,parc_cconvo,parc_dvoilib,parc_dnupro,proprio_ccodro_lib,proprio_ccodem_lib,proprio_gdesip,proprio_gtoper,proprio_dformjur,proprio_dqualp,proprio_ddenom,proprio_dnomus,proprio_dprnus,proprio_jdatnss,proprio_dldnss,proprio_epxnee,proprio_dnomcp,proprio_dprncp,proprio_dlign3,proprio_dlign4,proprio_dlign5,proprio_dlign6 
        from xx_99_utils.' . CADPGFUNCTION . '(\'' . $geom . '\'::geometry,' . $seulement_destinataire_avis_impot . "::boolean, '" . CADPGSCHEMA . "'::NAME) order by geo_parcelle asc ,proprio_gdesip desc";
    } else {
        $q = 'select  geo_parcelle,geom,parc_jdatat,parc_ccodep,parc_ccocom,parc_libcom,parc_ccosec,parc_dnupla,parc_dnvoiri,parc_cconvo,parc_dvoilib,parc_dnupro,proprio_ccodro_lib,proprio_ccodem_lib,proprio_gdesip,proprio_gtoper,proprio_dformjur,proprio_dqualp,proprio_ddenom,proprio_dnomus,proprio_dprnus,proprio_jdatnss,proprio_dldnss,proprio_epxnee,proprio_dnomcp,proprio_dprncp,proprio_dlign3,proprio_dlign4,proprio_dlign5,proprio_dlign6 
        from xx_99_utils.' . CADPGFUNCTION . '(ST_Buffer(\'' . $geom . '\', ' . $buffer . ')::geometry, ' . $seulement_destinataire_avis_impot . "::boolean, '" . CADPGSCHEMA . "'::NAME)  order by geo_parcelle asc ,proprio_gdesip desc";
    }
    //echo $q ;

    $command = REP_BIN_GDAL . '/ogr2ogr   -overwrite -f PostgreSQL -f "ESRI Shapefile" ' . $folder . 'export_cad_sysma.shp  PG:"host=' . MOULDBHOST . ' user=' . MOULDBUSER . ' password=' . MOULDBPWD . ' dbname=' . MOULDBNAME . ' port=' . MOULDBPORT . '" -sql "' . $q . '" ';


    $res = exec($command . ' 2>&1', $output, $return);

    if ($return === 0) {

        $zip = new ZipArchive;
        $zipFile = $folder . date('Ymd_His') . '_export_cad_sysma.zip';
        if ($zip->open($zipFile, ZipArchive::CREATE) == TRUE) {
            $zip->addFile($folder . 'export_cad_sysma.shp', 'export_cad_sysma.shp');
            $zip->addFile($folder . 'export_cad_sysma.dbf', 'export_cad_sysma.dbf');
            $zip->addFile($folder . 'export_cad_sysma.prj', 'export_cad_sysma.prj');
            $zip->addFile($folder . 'export_cad_sysma.shx', 'export_cad_sysma.shx');
            mkdir($folder . '_EXPORT_REPORT');
            file_put_contents($folder . '_EXPORT_REPORT/02_Informations.txt', implode($output, '\n'));
            $zip->addFile($folder . '_EXPORT_REPORT/02_Informations.txt', '_EXPORT_REPORT/02_Informations.txt');
            $zip->close();
        } else {
            $zipFile = false;
        }
    } else {
        $zipFile = false;
    }

    return ['zipFile' => $zipFile, 'foldersToDelete' => $foldersToDelete];
}

/*
 * éditer un listing complet avec n proprio par parcelle
 */

function findCadastreObjectInfos($app, $geom, $buffer = 0, $seulement_destinataire_avis_impot = 0)
{

    //echo $geom;
    $pdo = $app['pdo.cad'];

    if ($buffer == 0) {
        $q = 'select * , round(st_area(geom)::numeric / 10000.,2) as geom_ha from xx_99_utils.' . CADPGFUNCTION . "(:geom,:seulement_destinataire_avis_impot, '" . CADPGSCHEMA . "'::NAME ) order by geo_parcelle asc ,proprio_gdesip desc";
    } else {
        $q = 'select * , round(st_area(geom)::numeric / 10000.,2) as geom_ha from xx_99_utils.' . CADPGFUNCTION . "(ST_Buffer(:geom, :buffer), :seulement_destinataire_avis_impot,  '" . CADPGSCHEMA . "'::NAME ) order by geo_parcelle asc ,proprio_gdesip desc";
    }

    //  echo $q;

    $qry = $pdo->prepare($q);

    $qry->bindParam(':geom', $geom);
    $qry->bindParam(':seulement_destinataire_avis_impot', $seulement_destinataire_avis_impot);
    if ($buffer != 0) {
        $qry->bindParam(':buffer', $buffer, PDO::PARAM_INT);
    }
    $qry->execute();
    // var_dump($qry->fetchAll(PDO::FETCH_ASSOC)) ;
    //  var_dump($pdo->errorInfo());

    if ($qry->rowCount() > 0) {
        return $qry->fetchAll(PDO::FETCH_ASSOC);
    } else {
        return false;
    }
}


/*
 * recupérer l'année de version du cadastre
 */

function findCadastreVersionAnnee($app)
{

    //echo $geom;
    $pdo = $app['pdo.cad'];

    $cadastreAnneeSql = 'SELECT annee FROM ' . CADPGSCHEMA . '.commune limit 1';
    $qryCadastreAnnee = $pdo->prepare($cadastreAnneeSql);
    $qryCadastreAnnee->execute();
    

    if ($qryCadastreAnnee->rowCount() > 0) {
        $resCadastreAnnee = $qryCadastreAnnee->fetchObject();
        return $resCadastreAnnee->annee;
    } else {
        return false;
    }
}



function findMailingCadastreInfoObject($app, $geom, $buffer = 0, $seulement_destinataire_avis_impot = 0)
{
    //echo $geom;
    $pdo = $app['pdo.cad'];

    if ($buffer == 0) {
        $q = 'select * from xx_99_utils.' . CADPGFUNCTIONPUBLIPOST . "(:geom,:seulement_destinataire_avis_impot, '" . CADPGSCHEMA . "'::NAME )";
    } else {
        $q = 'select * from xx_99_utils.' . CADPGFUNCTIONPUBLIPOST . "(ST_Buffer(:geom, :buffer), :seulement_destinataire_avis_impot,  '" . CADPGSCHEMA . "'::NAME )";
    }

    $qry = $pdo->prepare($q);

    $qry->bindParam(':geom', $geom);
    $qry->bindParam(':seulement_destinataire_avis_impot', $seulement_destinataire_avis_impot);
    if ($buffer != 0) {
        $qry->bindParam(':buffer', $buffer, PDO::PARAM_INT);
    }
    $qry->execute();
    //var_dump($pdo->errorInfo());
    if ($qry->rowCount() > 0) {
        return $qry->fetchAll(PDO::FETCH_ASSOC);
    } else {
        return false;
    }
}

function findCadastreInfosSysmaObject($app, $geom, $buffer = 0, $seulement_destinataire_avis_impot = 1)
{
    //echo $geom;
    $pdo = $app['pdo.cad'];

    if ($buffer == 0) {
        $q = "select *
            ,'cS'||parc_ccosec||'P'||parc_dnupla as id
            ,'cadastre' as type
            ,publipost_prop_nom||'<br>'||parc_ccosec||'/'||parc_dnupla||'<br>'||round(st_area(geom)::numeric / 10000.,2) ||' ha' ||'<br>' as name
            ,'Cadastre' as object_type, ST_asGeojson(((ST_Transform(geom,4326)))) as geojson 
        from xx_99_utils." . CADPGFUNCTION . "(:geom,:seulement_destinataire_avis_impot, '" . CADPGSCHEMA . "'::NAME )";
        //  $q = 'select geo_parcelle as id,publipost_prop_nom as name,\'Cadastre\' as objecttype, ST_asGeojson((ST_Transform(geom,4326))) as geojson from xx_99_utils.f_cadastre_rtn_infos_parc_et_proprios_from_geom_v_plug_cad_1_3(:geom,:seulement_destinataire_avis_impot )';
    } else {
        $q = "select *
            ,'cS'||parc_ccosec||'P'||parc_dnupla as id
            ,'cadastre' as type
            ,publipost_prop_nom||'<br>'||parc_ccosec||'/'||parc_dnupla||'<br>'||round(st_area(geom)::numeric / 10000.,2) ||' ha' ||'<br>' as name
            ,'Cadastre' as object_type, ST_asGeojson(((ST_Transform(geom,4326)))) as geojson 
        from xx_99_utils." . CADPGFUNCTION . "(ST_Buffer(:geom, :buffer), :seulement_destinataire_avis_impot,  '" . CADPGSCHEMA . "'::NAME )";
        //    $q = 'select geo_parcelle as id,publipost_prop_nom as name,\'Cadastre\' as objecttype, ST_asGeojson((ST_Transform(geom,4326))) as geojson from xx_99_utils.f_cadastre_rtn_infos_parc_et_proprios_from_geom_v_plug_cad_1_3(ST_Buffer(:geom, :buffer), :seulement_destinataire_avis_impot )';
    }

    $qry = $pdo->prepare($q);

    $qry->bindParam(':geom', $geom);
    $qry->bindParam(':seulement_destinataire_avis_impot', $seulement_destinataire_avis_impot);

    if ($buffer != 0) {
        $qry->bindParam(':buffer', $buffer, PDO::PARAM_INT);
    }
    $qry->execute();
    // var_dump($pdo->errorInfo());
    if ($qry->rowCount() > 0) {
        return $qry->fetchAll(PDO::FETCH_ASSOC);
    } else {
        return false;
    }
}

/*
 * version de la fonction publiposage pour la conversion en geojson
 */

function findCadastreInfosMailingSysmaObject($app, $geom, $buffer = 0, $seulement_destinataire_avis_impot = 1)
{

    //echo $geom;
    $pdo = $app['pdo.cad'];
    if ($buffer == 0) {
        $q = 'select \'c\'||row_number() over()::int as id,publipost_prop_nom as name,\'Cadastre\' as object_type, ST_asGeojson(((ST_Transform(geom_union,4326)))) as geojson 
        from xx_99_utils.' . CADPGFUNCTIONPUBLIPOST . "(:geom,:seulement_destinataire_avis_impot, '" . CADPGSCHEMA . "'::NAME )";

        //  $q = 'select geo_parcelle as id,publipost_prop_nom as name,\'Cadastre\' as objecttype, ST_asGeojson((ST_Transform(geom,4326))) as geojson from xx_99_utils.f_cadastre_rtn_infos_parc_et_proprios_from_geom_v_plug_cad_1_3(:geom,:seulement_destinataire_avis_impot )';
    } else {
        $q = 'select \'c\'||row_number() over()::int as id,publipost_prop_nom as name,\'Cadastre\' as object_type, ST_asGeojson(((ST_Transform(geom_union,4326)))) as geojson 
        from xx_99_utils.' . CADPGFUNCTIONPUBLIPOST . "(ST_Buffer(:geom, :buffer), :seulement_destinataire_avis_impot,  '" . CADPGSCHEMA . "'::NAME )";


        //    $q = 'select geo_parcelle as id,publipost_prop_nom as name,\'Cadastre\' as objecttype, ST_asGeojson((ST_Transform(geom,4326))) as geojson from xx_99_utils.f_cadastre_rtn_infos_parc_et_proprios_from_geom_v_plug_cad_1_3(ST_Buffer(:geom, :buffer), :seulement_destinataire_avis_impot )';
    }

    $qry = $pdo->prepare($q);

    $qry->bindParam(':geom', $geom);
    $qry->bindParam(':seulement_destinataire_avis_impot', $seulement_destinataire_avis_impot);

    if ($buffer != 0) {
        $qry->bindParam(':buffer', $buffer, PDO::PARAM_INT);
    }
    $qry->execute();
    // var_dump($pdo->errorInfo());
    if ($qry->rowCount() > 0) {
        return $qry->fetchAll(PDO::FETCH_ASSOC);
    } else {
        return false;
    }
}

/* RPG */

function findRpgObjectInfos($app, $geom, $buffer = 0, $pacage = null)
{

    /*
{
    "gid" : "gid",
    "annee" : "annee" ,
    "pacage" : "pacage",
    "num_ilot" :"num_ilot",
    "num_parcel" : "num_parcel",
    "code_cultu" : "code_cultu",
    "code_grp_culture" : "code_grp_culture",
    "surf_adm" : "surf_adm",
    "precision" : "precision",
    "reconver_p" : "reconver_p",
    "retournmt_" : "retournmt_",
    "semence" : "semence",
    "commercial" : "commercial",
    "dest_ichn" : "dest_ichn",
    "culture_d1" : "culture_d1",
    "culture_d2" : "culture_d2",
    "bio" : "bio",
    "engagement" : "engagement",
    "maraichage" : "maraichage",
    "agroforest" : "agroforest",
    "force_maje" : "force_maje",
    "dep_ilot" : "dep_ilot",
    "geom" : "geom"
}
*/

    $mapping = json_decode(RPG_MAPPING, true) ;

    //var_dump($mapping);
    if ($mapping == null)
    return 'Erreur de formatage de RPG_MAPPING' ;

    // prepare 

    //echo $geom;
    //echo $pacage ;
    $pdo = $app['pdo'];

    $cond = null ;
    if (RPG_FILTER_TABLE != null) {
        $cond = ' and '.RPG_FILTER_TABLE ;
    }

    //params list
    $tabP = [] ;
    foreach($mapping as $key => $val) {
        
        if ($key == 'geom') {
            $tabP[] = 'st_asText(st_transform("'.$val.'",4326)) as geom' ;
        } else {
            $tabP[] = '"'.$val.'" as '.$key ;
        }
    }
    $listPForSql = implode(',', $tabP) ;

 
  
    if ($buffer == 0) {
        $q = 'SELECT 
        '.$listPForSql.' 
        ,st_within(:geom,"'.$mapping['geom'].'") as is_clicked  
        FROM '.RPG_SCHEMA_TABLE.'
        WHERE st_intersects("geom", :geom) '.$cond;
    } else {
        $q = 'SELECT 
        '.$listPForSql.' 
        ,st_within(:geom,"'.$mapping['geom'].'") as is_clicked   
        FROM '.RPG_SCHEMA_TABLE.'  
        WHERE ((st_intersects(st_buffer(:geom,:buffer),"geom") '.$cond.')';
    }

    
   
    if ($pacage != null) {
        $q .= ' or ("pacage" = :pacage '.$cond.')' ;
    }
    $q .= ') order by is_clicked desc, num_ilot asc' ;
    
   // echo $q ;

   // echo $pacage ;
   // echo '---------' ;
   // echo $geom ;
   // echo '---------' ;
   // echo $buffer ;

   //   echo $q;



    $qry = $pdo->prepare($q);

    $qry->bindParam(':geom', $geom);    
    if ($buffer != 0) {
        $qry->bindParam(':buffer', $buffer, PDO::PARAM_INT);
    }
    if ($pacage != null) {
        $qry->bindParam(':pacage', $pacage, PDO::PARAM_STR);
    }
    if (!$qry->execute()) {
        echo '<p class="text-danger">Erreur SQL lors de la requête du RPG, vérifier la correspondance de vos champs (RPG_MAPPING dans la configuration de Sysma)</p>';       
        return false ;
    }
    // var_dump($qry->fetchAll(PDO::FETCH_ASSOC)) ;
    //  var_dump($pdo->errorInfo());

    if ($qry->rowCount() > 0) {
        return $qry->fetchAll(PDO::FETCH_ASSOC);
    } else {
        return false;
    }
}



function findRpgInfosSysmaObject($app, $geom, $buffer = 0, $pacage)
{
    //echo $geom;
    //echo $pacage ;
    $pdo = $app['pdo'];

    $cond = null ;
    if (RPG_FILTER_TABLE != null) {
        $cond = ' and '.RPG_FILTER_TABLE ;
    }

    if ($buffer == 0) {
        $q = "SELECT 
            'rI'||\"num_ilot\"||'P'||\"num_parcel\" as id,
            'rpg' as type,
            '<b>Pacage '||\"pacage\"||'</b><br>Ilot '||\"num_ilot\"||', parcelle '||\"num_parcel\"||'<br>Code culture : '||\"code_cultu\"||coalesce(' <small>Précision : '||\"precision\"||'</small>','')||'<br>Surface : '||\"surf_adm\"||' ha'  as name,
            ST_asGeojson(((ST_Transform(geom,4326)))) as geojson  
            FROM ".RPG_SCHEMA_TABLE."
            WHERE (st_intersects(:geom,geom) ".$cond;
       
    } else {     


        $q = "SELECT 
            'rI'||\"num_ilot\"||'P'||\"num_parcel\" as id,
            'rpg' as type,
            '<b>Pacage '||\"pacage\"||'</b><br>Ilot '||\"num_ilot\"||', parcelle '||\"num_parcel\"||'<br>Code culture : '||\"code_cultu\"||coalesce(' <small>Précision : '||\"precision\"||'</small>','')||'<br>Surface : '||\"surf_adm\"||' ha'  as name,
            ST_asGeojson(((ST_Transform(\"geom\",4326)))) as geojson           
            FROM ".RPG_SCHEMA_TABLE."
            WHERE ((st_intersects(st_buffer(:geom,:buffer),\"geom\") ".$cond.")";

         

    }

    if ($pacage != null) {
        $q .= " or ( pacage = :pacage ".$cond.")" ;
    }

    $q .= ')' ;

    $mapping = json_decode(RPG_MAPPING, true) ;

    //var_dump($mapping);
    if ($mapping == null)
    return 'Erreur de formatage de RPG_MAPPING' ;
     
    foreach($mapping as $key => $val) {         
            $q = str_replace('\"'.$key.'\"', '\"'.$val.'\"' , $q) ;
       
    }
 
//echo $q ;


    $qry = $pdo->prepare($q);

    $qry->bindParam(':geom', $geom);
    

    if ($buffer != 0) {
        $qry->bindParam(':buffer', $buffer, PDO::PARAM_INT);
    }
    if ($pacage != null) {
        $qry->bindParam(':pacage', $pacage, PDO::PARAM_STR);
    }

    $qry->execute();
    // var_dump($pdo->errorInfo());
    if ($qry->rowCount() > 0) {
        return $qry->fetchAll(PDO::FETCH_ASSOC);
    } else {
        return false;
    }
}






function generateSysmaLayerFast($_TOid, $app)
{
    // $LIMIT = 100000;
    $SEPARATOR = '|';
    $NULL_AS = 'null';
    // $NULL_AS = "";


    $pdo = $app['pdo.export'];
    $sotIds = $_TOid;

    $t0 = microtime(TRUE);

    ///////////////////////////////////////////////////////////////////////////////////////////
    // Create output PG table where data will be exported
    $sql = 'SELECT   ' . MOULSCHEMA . '.create_empty_table_from_sysma_object_type_id(sysma_object_type_id, true), *
        FROM   sysma.sysma_object_type
        WHERE group_alias IS NOT null AND sysma_object_type_alias IS not null AND sysma_object_type_id IN (' . $sotIds . ') order by sysma_object_type_id';
       
    $qry = $pdo->prepare($sql);

    if ($qry->execute()) {
        $subArray['create_table_from_sysma_object_type_id'] = true;
        $subArray['table_generated_but_empty'] = true;
    } else {
        // Ugly hack!!! TODO: way better!!!
        $errorSqlState = $qry->errorInfo()[0];
        $errorCode = $qry->errorInfo()[1];
        $errorMsg = $qry->errorInfo()[2];
        if (strpos($errorMsg, 'ERROR:  syntax error at or near "DE"') !== false) {
            echo '<p>&nbsp;</p><p>&nbsp;</p><h1 class="text-danger">Les exports des types non-géographiques ne sont pas encore gérés. Désolé pour ce désagrément.</h1>';
        } else {
            echo '<p>&nbsp;</p><p>&nbsp;</p><h1 class="text-danger">Problème lors de la création des exports</h1><p></p><p>Détail de l\'erreur : </p>';
            // var_dump($qry->errorInfo());
            Debug::log($errorMsg);
        }
        // echo '<p>&nbsp;</p><p>&nbsp;</p><h1 class="text-danger">Problème lors de la création des exports</h1><p></p><p>Détail de l\'erreur : </p>' ;
        // var_dump($qry->errorInfo());
        // echo $errorMsg;
        return false;
    }

    // Get PG types of the fields of this export table 
    // $sql = 'select sysma_export_layer.get_pg_data_type_from_sysma_data_type(f.data_type) AS pg_type, f.*  from sysma_export_layer.get_params_dictionary_info_from_sysma_object_type_id(' . $sotIds . ') as f';
    $sql = 'select sysma_export_layer.get_pg_data_type_from_sysma_data_type(f.data_type) AS pg_type  from sysma_export_layer.get_params_dictionary_info_from_sysma_object_type_id(' . $sotIds . ') as f';
    $qry = $pdo->prepare($sql);
    if ($qry->execute()) {
        $exportTableFields = $qry->fetchAll();
    } else {
        echo '<p>&nbsp;</p><p>&nbsp;</p><h1 class="text-danger">Problème lors de la création des exports et de lecture de la table exportée</h1><p></p><p>Détail de l\'erreur : </p>';
        // var_dump($qry->errorInfo());
        Debug::log($qry->errorInfo()[2]);
        return false;
    }


    $t1 = microtime(TRUE);

    ///////////////////////////////////////////////////////////////////////////////////////////
    // Get objet type alias
    // $query = 'SELECT sysma_object_type_alias AS alias from   sysma.sysma_object_type  WHERE sysma_object_type_id = '.$sotIds.' AND sysma_object_type_alias IS not null;';
    $query = 'SELECT * from   sysma.sysma_object_type  WHERE sysma_object_type_id = ' . $sotIds . ' AND sysma_object_type_alias IS not null;';
    $qry = $pdo->prepare($query);
    if ($qry->execute()) {
        $sot = $qry->fetchObject();
        $sotAlias = $sot->sysma_object_type_alias;
        // @WARNING!!! Le type object devrait toujours contenir un ALIAS !!! (pour nommer la table au moins)
        if (empty($sotAlias)) {
            // @WARNING!!! Le type object devrait toujours contenir un ALIAS !!! (pour nommer la table au moins)
            echo '<p>&nbsp;</p><p>&nbsp;</p><h1 class="text-danger">Problème lors de la création des exports</h1><p>Aucun des types d\'objet séléctionnés ne contient d\'alias. Un alias est nécessaire pour export un type objet/une couche.</p><p>Détail de l\'erreur : </p>';
            return false;
        }
    } else {
        echo '<p>&nbsp;</p><p>&nbsp;</p><h1 class="text-danger">Problème lors de la création des exports</h1><p></p><p>Détail de l\'erreur : </p>';
        // var_dump($qry->errorInfo());
        Debug::log($qry->errorInfo()[2]);
        return false;
    }

    $t2 = microtime(TRUE);

    ///////////////////////////////////////////////////////////////////////////////////////////
    // Get object datas
    // TODO rm outputFields when ok
    // $outputFields = Array();
    $sql = "SELECT o.sysma_object_id
                , o.start_date
                , o.end_date
                , o.organisation_id
                , xx_99_utils.f_html_no_html_specials_chars_and_balises(org.organisation) ::character varying(250) organisation_obj
                , o.created_by
                , o.status
                , o.sysma_object_type_id
                , o.created_at 
                , replace(xx_99_utils.f_html_no_html_specials_chars_and_balises(o.sysma_object),'''''''','''''''''''') AS sysma_object  
                , o.modified_by AS modified_by
                , o.modified_at AS modified_at
                , o.geom
                ,  (select sysma_export_layer.get_json_photos_from_sysma_object_id(o.sysma_object_id))::jsonb as phototheque_json
            FROM sysma.sysma_object  o
            LEFT JOIN sysma.organisation org on (o.organisation_id=org.organisation_id)
            WHERE o.sysma_object_type_id = " . $sotIds . "
            ORDER BY o.sysma_object_id ASC
            ";

    // echo $sql;
    // die();
    $qry = $pdo->prepare($sql);
    $objectsData = null;
    if ($qry->execute()) {
        $objectsData = $qry->fetchAll(PDO::FETCH_ASSOC);
        if (empty($objectsData)) {
            foreach ($sot as $key => $val) {
                $subArray[$key] = $val;
            }
            $resTO = $subArray;

            $qry2 = $pdo->prepare('
                SELECT   ' . MOULSCHEMA . '.create_table_from_sysma_action_type_id(sysma_action_type_id, \'' . WEBURL . '\'::text, false::boolean), *
                from   sysma.sysma_action_type
                WHERE 
                sysma_action_type_alias IS not null
                and sysma_action_type.sysma_object_type_id in (' . $sotIds . ') 
                order by sysma_action_type_id');

            if ($qry2->execute()) {
                $resTT = $qry2->fetchAll(PDO::FETCH_ASSOC);

                for ($i = 0; $i < count($resTT); $i++) {
                    $resTT[$i]['create_table_from_sysma_action_type_id'] = true;
                    $resTT[$i]['table_generated_but_empty'] = true;
                    $resTT[$i]['sysma_object_type'] = $sot->sysma_object_type;
                }

                return array('resTO' => $resTO, 'resTT' => $resTT);
            } else {
                return array('resTO' => $resTO);
            }
        }
    } else {
        echo '<p>&nbsp;</p><p>&nbsp;</p><h1 class="text-danger">Problème lors de la création des exports</h1><p></p><p>Détail de l\'erreur : </p>';
        // var_dump($qry->errorInfo());
        Debug::log($qry->errorInfo()[2]);
        return false;
    }

    $t3 = microtime(TRUE);

    ///////////////////////////////////////////////////////////////////////////////////////////
    // Get list of parameter ids to export. It could be empty.
    // $sql = "SELECT  parameter_id AS id, parameter_alias AS alias
    $sql = "SELECT  parameter_id AS id
            FROM sysma_export_layer.get_params_dictionary_info_from_sysma_object_type_id(" . $sotIds . ")";
    $qry = $pdo->prepare($sql);
    $paramIdIndexes = array();
    // $paramIsMultipleChoiceList = Array();

    if ($qry->execute()) {
        $params = $qry->fetchAll(PDO::FETCH_ASSOC);
        // $i=0;
        $n = 0;
        foreach ($params as $param) {
            $paramIdIndexes[$param['id']] = $n++;
            // $outputFields[] = $param['alias'];
        }
        // echo '<br>paramIdIndexes:<br>';
        // echo json_encode($paramIdIndexes);
        // echo '<br>outputFields:<br>';
        // echo json_encode($outputFields);
    } else {
        echo '<p>&nbsp;</p><p>&nbsp;</p><h1 class="text-danger">Problème lors de la création des exports</h1><p></p><p>Détail de l\'erreur : </p>';
        // var_dump($qry->errorInfo());
        Debug::log($qry->errorInfo()[2]);
        return false;
    }

    $t4 = microtime(TRUE);

    ///////////////////////////////////////////////////////////////////////////////////////////
    // Get parameters data
    // $sql = "WITH lst_object as (
    //             SELECT sysma_object_id FROM sysma.sysma_object 
    //             WHERE sysma_object_type_id = ".$sotIds."
    //         )
    //         , lst_params_valides as (
    //             SELECT  parameter_id AS id
    //         FROM sysma_export_layer.get_params_dictionary_info_from_sysma_object_type_id(".$sotIds.")
    //         )
    //         SELECT l.sysma_object_id, p.parameter_id, p.start_date, p.end_date
    //             , string_agg(
    //                 replace(
    //                     xx_99_utils.f_html_no_html_specials_chars_and_balises(value)
    //                     ,'''',''''''
    //                     )::text
    //                 ,' [] ') as value
    //         FROM lst_object l  
    //         LEFT JOIN sysma.sysma_object_data p on (l.sysma_object_id = p.sysma_object_id AND p.end_date = '0')
    //         JOIN lst_params_valides pv on (pv.id=p.parameter_id)
    //         GROUP BY l.sysma_object_id, p.parameter_id, p.start_date, p.end_date 
    //         ORDER BY l.sysma_object_id ASC
    //         ";

    $sql = "WITH lst_object as (
        SELECT sysma_object_id FROM sysma.sysma_object 
        WHERE sysma_object_type_id = " . $sotIds . "
    )
    , lst_params_valides as (
        SELECT  parameter_id AS id
    FROM sysma_export_layer.get_params_dictionary_info_from_sysma_object_type_id(" . $sotIds . ")
    )
    SELECT l.sysma_object_id, p.parameter_id, p.start_date, p.end_date
        , string_agg(
            xx_99_utils.f_html_no_html_specials_chars_and_balises(value)
            ,' [] ') as value
    FROM lst_object l  
    LEFT JOIN sysma.sysma_object_data p on (l.sysma_object_id = p.sysma_object_id AND p.end_date = '0')
    JOIN lst_params_valides pv on (pv.id=p.parameter_id)
    GROUP BY l.sysma_object_id, p.parameter_id, p.start_date, p.end_date 
    ORDER BY l.sysma_object_id ASC
    ";
    // echo '<br><br>'.$sql.'<br><br>';
    // die();
    $qry = $pdo->prepare($sql);
    $paramsData = null;



    if ($qry->execute()) {
        $paramsData = $qry->fetchAll(PDO::FETCH_ASSOC);
        $t5 = microtime(TRUE);

        // echo '<br>paramsData:<br>';
        // echo json_encode($paramsData);


        // put object-data part length and param-data part length into local const
        $odLength = count($objectsData[0]) - 1;
        // $ndParamData = count($paramsData);
        $pdLength = count($paramIdIndexes);
        // $rowLength = $odLength + $pdLength;
        // if ($pdLength <= 0) {
        //     die ("WTF?"); // Allo houston. We have a situation here.
        // }
        // $n=0;
        // $row = new SplFixedArray($rowLength);


        $outputArray = array(); // array containing a complatible vector of "rows" for the func pgsqlCopyFrom...
        // $outputArray = new SplFixedArray(count($objectsData)); // array containing a complatible vector of "rows" for the func pgsqlCopyFrom...
        if ($pdLength > 0) {
            $rowParamPart = new SplFixedArray($pdLength);
        }
        $NULL_AS_SEPARATOR = $NULL_AS . $SEPARATOR;
        $pdIndex = 0;
        foreach ($objectsData as $od) {

            $rowStr = '';
            /*********************************************************/
            // 1st part : cpy object datas content
            $c = 0; // "column" index
            foreach ($od as $f) {
                if (!isset($f)) {
                    $rowStr .= $NULL_AS_SEPARATOR;
                } else {
                    if (is_string($f)) {  // deal with enoying CR and LF/EOL stuff
                        $f = addslashes($f);
                        // $f = str_replace('\r','\\\r', $f);  // les string \r deviennent des string counter-echappement \\\r
                        // $f = str_replace('\n','\\\n', $f);  // les string \n deviennent des string counter-echappement \\\n
                        $f = str_replace("\r", '', $f);      // les carriage-return deviennent des string vides. ie. unix-like EOL
                        $f = str_replace("\n", '\n', $f);    // les line-feed deviennent des string \n
                    }
                    $rowStr .= $f . $SEPARATOR;
                }
                $c++;
                if ($c >= $odLength) {
                    break;
                }
            }
            // echo '<br>'.$rowStr;

            $soid = $od['sysma_object_id'];

            /*********************************************************/
            // 2nd part : params data 
            for ($i = 0; $i < $pdLength; $i++) {
                $rowParamPart[$i] = $NULL_AS;
            }
            if ($pdLength > 0) {
                // if (!empty($paramsData[$pdIndex])) {
                while ((!empty($paramsData[$pdIndex])) and ($paramsData[$pdIndex]['sysma_object_id'] == $soid)) {
                    // while ( ($paramsData[$pdIndex]['sysma_object_id'] == $soid) ) {
                    $pVal = $paramsData[$pdIndex]['value'];
                    if ((isset($pVal)) and  (array_key_exists($paramsData[$pdIndex]['parameter_id'], $paramIdIndexes))) {
                        $paramIndex = $paramIdIndexes[$paramsData[$pdIndex]['parameter_id']];
                        // coherent filter to set special value in line with its pg type
                        if (($pVal == "") and ($exportTableFields[$paramIndex]['pg_type'] != 'TEXT')) {
                            $pVal = $NULL_AS;
                        }
                        // get rid of EOL chars annoyances
                        if (is_string($pVal)) { // deal with enoying CR and LF/EOL stuff
                            $pVal = addslashes($pVal);
                            // $pVal = str_replace('\r','\\\r', $pVal);  // les string \r deviennent des string counter-echappement \\\r
                            // $pVal = str_replace('\n','\\\n', $pVal);  // les string \n deviennent des string counter-echappement \\\n
                            $pVal = str_replace("\r", '', $pVal);      // les carriage-return deviennent des string vides
                            $pVal = str_replace("\n", '\n', $pVal);    // les line-feed deviennent des string \n
                        }
                        $rowParamPart[$paramIndex] = $pVal;
                    }

                    $pdIndex++;
                }

                ///////////////////////////////////////
                $rowStr .= $rowParamPart[0];
                for ($i = 1; $i < $pdLength; $i++) {
                    $rowStr .= $SEPARATOR . $rowParamPart[$i];
                }
                $rowStr .= $SEPARATOR;
            }
            // $rowStr = $row[0];
            // for ($i=1; $i<$rowLength; $i++) {
            //     $rowStr .= $SEPARATOR.$row[$i];
            // }
            // $rowStr .= "'".$row[$odLength]."'";
            // for ($i=$odLength+1; $i<$rowLength; $i++) {
            //     $rowStr .= $SEPARATOR."'".$row[$i]."'";
            // }
            // }
            // $rowStr.=$SEPARATOR.WEBURL.'/carte/objet/'.$soid; //sysma_link
            $rowStr .= WEBURL . '/carte/objet/' . $soid; //sysma_link
            if (empty($od['phototheque_json'])) {
                $rowStr .= $SEPARATOR . $NULL_AS;
            } else {
                $rowStr .= $SEPARATOR . $od['phototheque_json']; // phototheque_json
            }
            // $rowStr.=$SEPARATOR.$od['phototheque_json']; // phototheque_json
            // $rowStr.=$SEPARATOR.json_encode($od['phototheque_json']); // phototheque_json
            // echo '<br>'.$od['phototheque_json'].'<br>';
            // echo '<br>'.json_encode($od['phototheque_json']).'<br>';
            // $rowStr.="'".$SEPARATOR.WEBURL.'/carte/objet/'.$soid."'"; //sysma_link 
            // $rowStr.="'".$SEPARATOR.json_encode($od['phototheque_json'])."'"; // phototheque_json
            // echo '<br>';
            // echo '<br>';
            // echo '<br>';
            // echo '<br>';
            // echo $rowStr;
            // echo '<br>';
            $outputArray[] = $rowStr;
            // $outputArray[$nb_o] = $rowStr;
            // $nb_o++;
        }
        // echo '<br>-------------------------------------';
        // echo '<br>outputFields:<br>';
        // echo json_encode($outputFields);
        // echo '<br>-------------------------------------';
        // echo '<br>paramIdIndexes:<br>';
        // echo json_encode($paramIdIndexes);

        // echo '<br>';
        // echo '<br>outputArray:<br>';
        // echo json_encode($outputArray);
        // echo '<br>-------------------------------------';
        // echo count($objectsData[0]);

        // $outputArray = clone $objectsData;
        // foreach ($objectsData as $od) {
        //     $outputArray[];
        // }
        $t6 = microtime(TRUE);
    } else {
        echo '<p>&nbsp;</p><p>&nbsp;</p><h1 class="text-danger">Problème lors de la création des exports</h1><p></p><p>Détail de l\'erreur : </p>';
        // var_dump($qry->errorInfo());
        Debug::log($qry->errorInfo()[2]);
        return false;
    }

    // cpy outputArray to pg
    if ($pdo->pgsqlCopyFromArray(MOULSCHEMA . '.' . $sotAlias, $outputArray, $SEPARATOR, $NULL_AS)) {
        // $subArray['sysma_objects_copied_to_table'] = true;
        $subArray['table_generated_but_empty'] = false;

        foreach ($sot as $key => $val) {
            $subArray[$key] = $val;
        }
        $resTO = $subArray;

        $qry2 = $pdo->prepare('
        SELECT   ' . MOULSCHEMA . '.create_table_from_sysma_action_type_id(sysma_action_type_id, \'' . WEBURL . '\'::text, false::boolean), *
        from   sysma.sysma_action_type
        WHERE 
          sysma_action_type_alias IS not null
         and sysma_action_type.sysma_object_type_id in (' . $sotIds . ') 
        order by sysma_action_type_id');

        if ($qry2->execute()) {
            $resTT = $qry2->fetchAll(PDO::FETCH_ASSOC);
            for ($i = 0; $i < count($resTT); $i++) {
                $resTT[$i]['create_table_from_sysma_action_type_id'] = true;
                $resTT[$i]['table_generated_but_empty'] = false;
                $resTT[$i]['sysma_object_type'] = $sot->sysma_object_type;
            }

            return array('resTO' => $resTO, 'resTT' => $resTT);
        } else {
            echo '<p>&nbsp;</p><p>&nbsp;</p><h1 class="text-danger">Problème lors de la création des exports</h1><p>Le plus souvent, l\'export pose problème quand une valeur n\'est pas typée correctement. Le début du message d\'erreur permet d\'identifier la valeur à corriger.</p><p>Détail de l\'erreur : </p>';
            //var_dump($qry->errorInfo());
            Debug::log($qry->errorInfo()[2]);
            return array('resTO' => $resTO);
        }
    } else {
        echo '<p>&nbsp;</p><p>&nbsp;</p><h1 class="text-danger">Problème lors de la création des exports</h1><p>Le plus souvent, l\'export pose problème quand une valeur n\'est pas typée correctement. Le début du message d\'erreur permet d\'identifier la valeur à corriger.</p><p>Détail de l\'erreur : </p>';
        // var_dump($qry->errorInfo());
        Debug::log($qry->errorInfo()[2]);
        return false;
    }
    ///////////////////////////////////////////////////////////////////////////
    // profiling stuff: 
    // disable by default (see returns in the above code) 
    $t7 = microtime(TRUE);
    // $n--;
    // echo '<br>outputArray:<br>';
    // echo json_encode($outputArray);
    // echo '<br>-------------------------------------';

    if (false) {
        echo "The whole code took : " . ($t7 - $t0) . " seconds to complete for " . count($objectsData) . ' objects with ' . $pdLength . ' parameters (object_data size = ' . count($paramsData) . ').<br>';
        echo "Create table took               : " . ($t1 - $t0) . " seconds<br>";
        echo "Short list parameter alias took : " . ($t2 - $t1) . " seconds (TODO rm?)<br>";
        echo "Get objects data took           : " . ($t3 - $t2) . " seconds<br>";
        echo "Get params' indexes list        : " . ($t4 - $t3) . " seconds<br>";
        echo "Get params data took            : " . ($t5 - $t4) . " seconds<br>";
        echo "Pivot took                      : " . ($t6 - $t5) . " seconds<br>";
        echo "Cpy to PG took                  : " . ($t7 - $t6) . " seconds<br>";
    } else {
        // echo $n . ' objects with '.$pdLength.' parameters'."<br>";
        echo count($objectsData) . ' objects with ' . $pdLength . ' parameters (object_data size = ' . count($paramsData) . ')' . "<br>";
        echo ($t7 - $t0) . "<br>";
        echo ($t1 - $t0) . "<br>";
        echo ($t2 - $t1) . "<br>";
        echo ($t3 - $t2) . "<br>";
        echo ($t4 - $t3) . "<br>";
        echo ($t5 - $t4) . "<br>";
        echo ($t6 - $t5) . "<br>";
        echo ($t7 - $t6) . "<br>";
    }
    die();
}


function generateSysmaLayers($tabTO, $app)
{
    $pdo = $app['pdo.export'];

    $query = '
    SELECT   ' . MOULSCHEMA . '.create_table_from_sysma_object_type_id(sysma_object_type_id, false, \'' . WEBURL . '\'::text), *
    from   sysma.sysma_object_type
    WHERE 
    group_alias is not null AND sysma_object_type_alias IS not null AND sysma_object_type_id in (' . implode(',', $tabTO) . ') order by sysma_object_type_id';


    $qry = $pdo->prepare($query);
    $res = $qry->execute();
    if ($res) {


        $resTO = $qry->fetchAll(PDO::FETCH_ASSOC);

        $qry2 = $pdo->prepare('
        SELECT   ' . MOULSCHEMA . '.create_table_from_sysma_action_type_id(sysma_action_type_id, \'' . WEBURL . '\'::text), *
        from   sysma.sysma_action_type
        WHERE 
          sysma_action_type_alias IS not null
         and sysma_action_type.sysma_object_type_id in (' . implode(',', $tabTO) . ') 
        order by sysma_action_type_id');

        $res2 = $qry2->execute();

        if ($res2) {

            $resTT = $qry2->fetchAll(PDO::FETCH_ASSOC);
            return array('resTO' => $resTO, 'resTT' => $resTT);
        } else {
            return array('resTO' => $resTO);
        }
    } else {

        echo '<p>&nbsp;</p><p>&nbsp;</p><h1 class="text-danger">Problème lors de la création des exports</h1><p>Le plus souvent, l\'export pose problème quand une valeur n\'est pas typée correctement. Le début du message d\'erreur permet d\'identifier la valeur à corriger.</p><p>Détail de l\'erreur : </p>';
        var_dump($qry->errorInfo());

        return false;
    }
}

function contracts($app)
{

    $pdo = $app['pdo'];
    $qry = $pdo->prepare('select * from ' . SCHEMA . '.contract order by "display_order" asc');
    $qry->execute();
    return $qry->fetchAll(PDO::FETCH_ASSOC);
}

function groups($app)
{

    $pdo = $app['pdo'];
    $qry = $pdo->prepare('select * from ' . SCHEMA . '.layer_group order by group_display_order asc');
    $qry->execute();
    return $qry->fetchAll(PDO::FETCH_ASSOC);
}

function contractsNom($app)
{

    $pdo = $app['pdo'];
    $qry = $pdo->prepare('select contract from ' . SCHEMA . '.contract order by "display_order" asc');
    $qry->execute();
    $tabRes = array();
    while ($s = $qry->fetchObject()) {
        $tabRes[$s->contract] = $s->contract;
    }
    return $tabRes;
}

function listeEPCI($app)
{

    $pdo = $app['pdo.export'];
    $qry = $pdo->prepare('select * from ' . REF_PG_SCHEMA_TABLE_INTERCOMMUNAL . ' order by ' . REF_PG_SCHEMA_TABLE_INTERCOMMUNAL_NAME . ' asc');

    $qry->execute();
    $tabEPCI = array();
    while ($res = $qry->fetch()) {
        $tabEPCI[REF_PG_SCHEMA_TABLE_INTERCOMMUNAL . '.' . REF_PG_SCHEMA_TABLE_INTERCOMMUNAL_GID . '.' . REF_PG_SCHEMA_TABLE_INTERCOMMUNAL_NAME . '.' . REF_PG_SCHEMA_TABLE_INTERCOMMUNAL_GEOM . '.' . $res[REF_PG_SCHEMA_TABLE_INTERCOMMUNAL_GID]] = $res[REF_PG_SCHEMA_TABLE_INTERCOMMUNAL_NAME];
    }
    return $tabEPCI;
}

function listeME($app)
{

    $pdo = $app['pdo.export'];
    $qry = $pdo->prepare('select * from ' . REF_PG_SCHEMA_TABLE_MASSEEAU . ' order by ' . REF_PG_SCHEMA_TABLE_MASSEEAU_NAME . ' asc');
    // echo 'select * from ' . REF_PG_SCHEMA_TABLE_INTERCOMMUNAL . ' order by ' . REF_PG_SCHEMA_TABLE_INTERCOMMUNAL_NAME . ' asc';
    $qry->execute();
    $tab = array();
    while ($res = $qry->fetch()) {
        $tab[REF_PG_SCHEMA_TABLE_MASSEEAU . '.' . REF_PG_SCHEMA_TABLE_MASSEEAU_GID . '.' . REF_PG_SCHEMA_TABLE_MASSEEAU_NAME . '.' . REF_PG_SCHEMA_TABLE_MASSEEAU_GEOM . '.' . $res[REF_PG_SCHEMA_TABLE_MASSEEAU_GID]] = $res[REF_PG_SCHEMA_TABLE_MASSEEAU_NAME];
    }
    return $tab;
}



function findSysmaObjects($geometrie, $options, $app)
{


    $compl = $res = $q = null;


    if (!isset($options['tampon'])) {
        $options['tampon'] = 0.01;
    }

    $pdo = $app['pdo'];

    if (!isset($options['sysma_object_type_ids']) or  $options['sysma_object_type_ids'] == null) {
        return null;
    }

    // geom
    $theAreaGeom = null;
    if ($geometrie !== null) {
        $theAreaGeom = 'ST_Transform(ST_SetSRID(ST_MakePolygon(ST_MakeValid(ST_GeomFromText(\'LINESTRING(' . $geometrie . ')\'))),4326),2154)::geometry(polygon,2154)';
    } else if ($options['geom_from_secteur'] !== null) {
        $theAreaGeom = "ST_setSRID(ST_Multi('" . $options['geom_from_secteur'] . "')::geometry(MULTIPOLYGON,2154),2154)";
    }

    // sql union with condition for each sysma type
    foreach ($options['sysma_object_type_ids'] as $typeId) {

        $compl = null;
        if (isset($options['sysma_object_type_id_filters'][$typeId]) and $options['sysma_object_type_id_filters'][$typeId] != null) {
            $compl = $options['sysma_object_type_id_filters'][$typeId];
        }

        $q[] = '(select o.sysma_object_id as id from ' . SCHEMA . '.sysma_object o
        where ST_Intersects(' . $theAreaGeom . ',ST_Buffer(ST_SetSRID(o.geom,2154),' . $options['tampon'] . ')) 
        and o.sysma_object_type_id = ' . $typeId . ' 
        ' . $compl . ' order by o.sysma_object_type_id, o.sysma_object_id asc)';
    }


    $qry = $pdo->prepare(implode(' UNION ', $q));

    // echo implode(' UNION ', $q) ;


    //$qry->bindParam(':geometrie',$geometrie,PDO::PARAM_STR);
    //$qry->bindParam(':table',$table_objets,PDO::PARAM_STR);
    $qry->execute();

    //  $qry->debugDumpParams();
    //  echo $qry->rowCount() ;

    $c = 0;
    while ($s = $qry->fetchObject()) {
        $O = new SysmaObject($s->id, $app);
        $O->loadGeoInfos($O->geometry_type);
        if (hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'lecteur')) {
            $res[$O->sysma_object_type_id][$O->sysma_object_id] = $O;
        }
    }

    return $res;
}




/*
 * cette fonction recherche les objets intersectant une geometrie donnée
 * elle renvoie un tableau d'objets géogaphiques
 * 
 * ATTENTION aux droits du user en cours sur la table PG visée
 * 
 * @params
 * string table_objet nom de la table et schéma
 * geometry geometrie  
 * array options
 * tab['tampon'] : largeur du tampon en mètres ?
 * tab['nom_present'] booleanType : exclu ou pas les objets dont le nom est inconnu, par défaut faux
 * tab['distance_de'] sysma_object = pour le calcul de la distance entre l'objet trouvé et l'objet fourni (ex : on recherche une stt qualité sur un cours d'eau et on voit savoir la distance de la stt qualité avec une commune)
 * tab['exclus'] exclus ces objets de la recherche
 * silex app 
 * 
 * @return
 * 
 * array [compteur ]
 *              [sysma_object]
 *              [sysma_object_id]
 *              [geom]
 *              [distance_de]
 *              
 */

function findIntersectingObjects($table_objets, $geometrie, $options, $app)
{


    $compl = $res = $fields = null;

    if (isset($options['id'])) {
        $sysma_object_id = $options['id'];
    } else {
        $sysma_object_id = 'id';
    }

    if (isset($options['object'])) {
        $sysma_object = $options['object'];
    } else {
        $sysma_object = 'object';
    }


    if (isset($options['geom'])) {
        $geom = $options['geom'];
    } else {
        $geom = 'geom';
    }

    if (!isset($options['nom_present'])) {
        $options['nom_present'] = false;
    }
    if ($options['nom_present']) {
        $compl = ' and sysma_object is not null and sysma_object != \'\'';
    }
    if (isset($options['exclus']) and count($options['exclus']) > 0) {
        $compl .= ' and ' . $sysma_object_id . ' not in (\'' . implode("','", $options['exclus']) . '\')';
    }

    if (isset($options['seulement']) and count($options['seulement']) > 0) {
        $compl .= ' and ' . $sysma_object_id . '  in (\'' . implode("','", $options['seulement']) . '\')';
    }




    $pdo = $app['pdo.export'];


    // etape 1 : intersection des objets de la table fournie avec la géométrie fournie
    $qry = $pdo->prepare('select ' . $sysma_object_id . ' as sysma_object_id, ' . $sysma_object . ' as  sysma_object, ' . $geom . ' as geom from ' . $table_objets . ' t where ST_Intersects(ST_SetSRID(\'' . $geometrie . '\'::geometry,2154),ST_Buffer(ST_SetSRID(t.' . $geom . ',2154),' . $options['tampon'] . ')) ' . $compl . ' order by ' . $sysma_object_id . ' asc');



    //$qry->bindParam(':geometrie',$geometrie,PDO::PARAM_STR);
    //$qry->bindParam(':table',$table_objets,PDO::PARAM_STR);
    $qry->execute();

    //  $qry->debugDumpParams();
    //  echo $qry->rowCount() ;

    $c = 0;
    while ($s = $qry->fetchObject()) {
        $res[$c]['sysma_object'] = $s->sysma_object;
        $res[$c]['sysma_object_id'] = $s->sysma_object_id;


        // $res[$c]['geom'] = $s->geom;

        if (isset($options['distance_de'])) {
            // recherche de la distance
            $qry2 = $pdo->prepare('select ST_Distance(\'' . $s->geom . '\',ST_Centroid(ST_SetSRID(\'' . $options['distance_de'] . '\'::geometry,2154))) as distance');
            $qry2->execute();
            $sd = $qry2->fetchObject();
            $res[$c]['distance'] = $sd->distance;
        }

        $c++;
    }

    return $res;
}

/*
 * fonction spécifique pour le croisement avec les communes (2018 01 10)
 * cette fonction recherche les objets intersectant une geometrie donnée
 * elle renvoie un tableau d'objets géogaphiques
 *              
 */

function findIntersectingCities($geometrie, $options, $app)
{

    if (isset($options['sql'])) {
        $compl = $options['sql'];
    } else {
        $compl = null;
    }
    $pdo = $app['pdo.export'];
    $qry = $pdo->prepare('select ' . REF_PG_SCHEMA_TABLE_CITIES_GID . ' as sysma_object_id, ' . REF_PG_SCHEMA_TABLE_CITIES_NAME . ' as sysma_object from ' . REF_PG_SCHEMA_TABLE_CITIES . ' t where ST_Intersects(ST_Buffer(ST_SetSRID(\'' . $geometrie . '\'::geometry,2154),' . $options['tampon'] . '),t.' . REF_PG_SCHEMA_TABLE_CITIES_GEOM . ') ' . $compl . ' order by ' . REF_PG_SCHEMA_TABLE_CITIES_GID . ' asc');
    $qry->execute();

    $c = 0;
    while ($s = $qry->fetchObject()) {
        $res[$c]['sysma_object'] = $s->sysma_object;
        $res[$c]['sysma_object_id'] = $s->sysma_object_id;
        $c++;
    }
    return $res;
}

/*
 * créer un hash depuis un password pour le cookie
 */
function hashSysmaCookie($passWord)
{
    return password_hash($passWord, PASSWORD_DEFAULT);
}

/*
 * recherche le user dont le nom de user est celui fourni par cryptage simple
 */
function findCookieUser($usercrypt, $app)
{
    // echo "WIP Arl: ".__FUNCTION__.'<br>';

    $pdo = $app['pdo'];
    $qry = $pdo->prepare('select user_id, "user",registration_date, password from ' . SCHEMA . '."user" where md5("user") = :usercrypt');
    $qry->bindParam(':usercrypt', $usercrypt, PDO::PARAM_STR);
    $qry->execute();
    if ($qry->rowCount() === 1) {
        $u = $qry->fetchObject();
        // echo "WIP Arl: ".__FUNCTION__.' here <br>';
        return $u;
    } else {
        // echo "WIP Arl: ".__FUNCTION__.' null <br>';
        return null;
    }
}

/*
 * @param password
 * @return a std password hash
 */
function hashSysma($_passWord)
{
    return password_hash($_passWord, PASSWORD_DEFAULT, ["cost" => 12]);
}

/*
 * @return true if password is valid or false if not valid
 * TEMPORARLY: This func also update/replace every old_passwords hash by a new more secure one.
 * TODO: rm the $_DEPRECATED_user arg/param when retro-compatibility with old hash method become unnecessary.
 */
function passwordVerifySysma($_passwordAttempt, $_refHashedPassword, $_DEPRECATED_user = null)
{
    // For now let's attempt autho one more time with old method
    // TODO FIXME SECURITY_FLAW : rm this once there's no old account with old hash method anymore.
    $rc = false;

    if (password_verify($_passwordAttempt, $_refHashedPassword)) {
        if ((!empty($_DEPRECATED_user)) and (substr_compare($_refHashedPassword, '$6$rounds=5000000$', 0, 18) == 0)) {
            // echo "WIP Arl: passwordVerifySysma OLD version Authorize<br>";
            // TEMPORARLY update/replace every old_passwords hash by a new fresh and more secure one.
            // TODO: rm this when all pswds are updated
            $_DEPRECATED_user->updatePasswordHash(hashSysma($_passwordAttempt));
            // } else {
            //     echo "WIP Arl: passwordVerifySysma NEW VERSION Authorize<br>";
        }
        $rc = true;
    }
    return $rc;
}

function control($login, $passwordAttempt, $app)
{

    if (isset($login) and isset($passwordAttempt)) {
        $pdo = $app['pdo'];
        $qry = $pdo->prepare('select * from ' . SCHEMA . '."user" where "user" = :login and active is true');
        $qry->bindParam(':login', $login, PDO::PARAM_STR);
        $qry->execute();

        if ($qry->rowCount() > 0) {

            $access = $qry->fetchObject();
            $hashedPasswordRef = $access->password;

            $user = new User($access->user_id, $app);

            if (passwordVerifySysma($passwordAttempt, $hashedPasswordRef, $user)) {
                return $access->user_id;
            } else {
                return false;
            }
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function relationTypesList($app, $options = ['filter' => null])
{

    $pdo = $app['pdo'];
    $qry = $pdo->prepare('select sysma_relation_type_id, sysma_relation_type, description, hidden '
        . 'from  ' . SCHEMA . '.sysma_relation_type '
        . 'where 1=1 ' . $options['filter'] . ' order by sysma_relation_type asc');
    $qry->execute();
    return $qry->fetchAll(PDO::FETCH_ASSOC);
}

function relationTypesListForForm($app, $options = ['filter' => null])
{

    $relations = array();
    $pdo = $app['pdo'];
    $qry = $pdo->prepare('select sysma_relation_type_id, sysma_relation_type, description, hidden '
        . 'from  ' . SCHEMA . '.sysma_relation_type '
        . 'where 1=1 ' . $options['filter'] . ' order by sysma_relation_type asc');
    $qry->execute();
    while ($res = $qry->fetchObject()) {
        $relations[$res->sysma_relation_type_id] = $res->sysma_relation_type;
    }
    return $relations;
}

function objectsList($app, $sysma_object_type_id, $options = ['filter' => null])
{

    $pdo = $app['pdo'];
    $qry = $pdo->prepare('select sysma_object_id, sysma_object '
        . 'from  ' . SCHEMA . '.sysma_object '
        . 'where sysma_object_type_id = :typ ' . $options['filter'] . ' order by sysma_object_id asc');
    $qry->bindParam(':typ', $sysma_object_type_id, PDO::PARAM_INT);
    $qry->execute();
    return $qry->fetchAll(PDO::FETCH_ASSOC);
}

function objectTypesList($app, $options = ['filter' => null])
{

    $pdo = $app['pdo'];
    $qry = $pdo->prepare('select sysma_object_type_id, sysma_object_type, description, hidden, lg.group_name '
        . 'from  ' . SCHEMA . '.sysma_object_type '
        . 'left join ' . SCHEMA . '.layer_group lg on sysma_object_type.group_alias = lg.group_alias '
        . 'where 1=1 ' . $options['filter'] . ' order by sysma_object_type asc');
    $qry->execute();
    return $qry->fetchAll(PDO::FETCH_ASSOC);
}

function objectTypesListForSelect($app)
{

    $pdo = $app['pdo'];
    $qry = $pdo->prepare('select sysma_object_type_id||\'|\'||sysma_object_type as sysma_object_type_id, sysma_object_type from  ' . SCHEMA . '.sysma_object_type order by sysma_object_type asc');
    $qry->execute();
    return $qry->fetchAll(PDO::FETCH_KEY_PAIR);
}

function simpleObjectTypesListForSelect($app)
{

    $pdo = $app['pdo'];
    $qry = $pdo->prepare('select sysma_object_type_id,sysma_object_type from  ' . SCHEMA . '.sysma_object_type order by sysma_object_type asc');
    $qry->execute();
    return $qry->fetchAll(PDO::FETCH_KEY_PAIR);
}

function parametersList($type, $id_type, $app)
{

    $tab = array();
    $pdo = $app['pdo'];
    $qry = $pdo->prepare('select parameter_id, parameter from  ' . SCHEMA . '.types_' . $type . '_parameters where id_type_' . $type . ' = :id_type order by parameter asc');
    $qry->bindParam(':id_type', $id_type, PDO::PARAM_INT);
    $qry->execute();
    while ($s = $qry->fetchObject()) {

        $tab[$s->parameter_id] = $s->parameter;
    }

    return $tab;
}

function sysmaObjectTypesList($app)
{

    $choices = array();
    $pdo = $app['pdo'];
    $qry = $pdo->prepare("select sysma_object_type_id, sysma_object_type from " . SCHEMA . ".sysma_object_type order by sysma_object_type asc ");
    $qry->execute();
    while ($res = $qry->fetchObject()) {

        $choices[$res->sysma_object_type_id . '001'] = $res->sysma_object_type;
    }

    return $choices;
}

function sysmaObjectTypesListForForm($app)
{

    $choices = array();
    $pdo = $app['pdo'];
    $qry = $pdo->prepare("select sysma_object_type_id, sysma_object_type from " . SCHEMA . ".sysma_object_type order by sysma_object_type asc ");
    $qry->execute();
    while ($res = $qry->fetchObject()) {

        $choices[$res->sysma_object_type_id] = $res->sysma_object_type;
    }

    return $choices;
}

function sysmaActionTypesListForForm($app)
{

    $choices = array();
    $pdo = $app['pdo'];
    $qry = $pdo->prepare("select sa.sysma_action_type_id, sa.sysma_action_type, so.sysma_object_type from " . SCHEMA . ".sysma_action_type sa inner join " . SCHEMA . ".sysma_object_type so on so.sysma_object_type_id = sa.sysma_object_type_id order by so.sysma_object_type,sa.sysma_action_type asc ");
    $qry->execute();
    while ($res = $qry->fetchObject()) {
        $choices[$res->sysma_action_type_id] = $res->sysma_object_type . ' - ' . $res->sysma_action_type;
    }

    return $choices;
}

function sysmaObjectTypeGroupsList($app)
{

    $choices = ['' => 'Pas de groupe'];
    $pdo = $app['pdo'];
    $qry = $pdo->prepare("select group_name, group_alias from " . SCHEMA . ".layer_group order by group_display_order asc ");
    $qry->execute();
    while ($res = $qry->fetchObject()) {

        $choices[$res->group_alias] = $res->group_name;
    }

    return $choices;
}


function sysmaObjectTypeGroupsNameList($app)
{

    $choices = ['' => 'Pas de groupe'];
    $pdo = $app['pdo'];
    $qry = $pdo->prepare("select group_name, group_alias from " . SCHEMA . ".layer_group order by group_display_order asc ");
    $qry->execute();
    while ($res = $qry->fetchObject()) {

        $choices[$res->group_name] = $res->group_name;
    }

    return $choices;
}

function usersList($app)
{

    $tabU = array();
    $pdo = $app['pdo'];
    $qry = $pdo->prepare("select * from " . SCHEMA . ".user u LEFT JOIN " . SCHEMA . ".organisation s ON u.organisation_id = s.organisation_id order by u.active is true desc, u.lastname asc");
    $qry->execute();
    while ($res = $qry->fetchObject()) {

        array_push($tabU, $res);
    }

    return $tabU;
}

/*
 * liste les filter pour le type d'objet fourni
 */

function filtersList($object_type, $id_type, $id_user, $app)
{

    $pdo = $app['pdo'];
    if ($object_type === 'sysma_object') {
        $qry = $pdo->prepare('select f.filter_id as idfilter, * from ' . SCHEMA . '.filter f LEFT JOIN ' . SCHEMA . '.l_filter_user l ON l.filter_id = f.filter_id and user_id = :id_user and sysma_object_type_id = :id_type where object_type = :object_type order by f.filter');
    } elseif ($object_type === 'sysma_action') {
        $qry = $pdo->prepare('select f.filter_id as idfilter, * from ' . SCHEMA . '.filter f LEFT JOIN ' . SCHEMA . '.l_filter_user l ON l.filter_id = f.filter_id and user_id = :id_user and sysma_action_type_id = :id_type where object_type = :object_type order by f.filter');
    }
    $qry->bindParam(':object_type', $object_type, PDO::PARAM_STR);
    $qry->bindParam(':id_type', $id_type, PDO::PARAM_INT);
    $qry->bindParam(':id_user', $id_user, PDO::PARAM_INT);
    $qry->execute();
    return $qry->fetchAll();
}


/*
 * liste les filter, objets
*/

function filterObjectList($app)
{

    $tab = [];
    $pdo = $app['pdo'];

    $qry = $pdo->prepare('select filter_id from ' . SCHEMA . '.filter  order by filter');
    $qry->execute();
    while ($res = $qry->fetchObject()) {
        $tab[] = new SysmaFilter($res->filter_id, $app);
    }
    return $tab;
}


/*
 * liste les analyse thématiques pour le type d'objet fourni
 */

function thematicAnalysisList($object_type, $type_id, $id_user, $app)
{



    $pdo = $app['pdo'];
    $qry = $pdo->prepare('select at.theme_analysis_id  as id_ta, * '
        . 'from ' . SCHEMA . '.theme_analysis at '
        . 'LEFT JOIN ' . SCHEMA . '.l_theme_analysis_user l ON l.theme_analysis_id = at.theme_analysis_id and user_id = :id_user '
        . 'where object_type = :object_type and type_id = :type_id order by theme_analysis');


    $qry->bindParam(':object_type', $object_type, PDO::PARAM_STR);
    $qry->bindParam(':type_id', $type_id, PDO::PARAM_INT);
    $qry->bindParam(':id_user', $id_user, PDO::PARAM_INT);
    $qry->execute();
    return $qry->fetchAll();
}


/*
* liste des analyses thématiques, objets
*/

function thematicAnalysisObjectList($app)
{

    $tabRes = [];
    $pdo = $app['pdo'];
    $qry = $pdo->prepare('select theme_analysis_id from ' . SCHEMA . '.theme_analysis at');
    $qry->execute();
    while ($res = $qry->fetchObject()) {

        try {
            $TA = new ThematicAnalysis($res->theme_analysis_id, $app);
        } catch (Exception $e) {
        } finally {
            $tabRes[] = $TA;
        }
    }
    return $tabRes;
}



function findMinMax($type, $parameter_id, $app)
{

    $pdo = $app['pdo'];
    $qry = $pdo->prepare('select min(value::numeric) as min, max(value::numeric) as max from ' . SCHEMA . '.' . htmlentities($type, ENT_QUOTES) . '_data where parameter_id = :parameter_id');
    $qry->bindParam(':parameter_id', $parameter_id, PDO::PARAM_INT);
    $qry->execute();
    return $qry->fetchObject();
}

/*
 * liste des organisation sur lesquels le user a les droits
 */

function organisationWithUserAccessList($app)
{

    $pdo = $app['pdo'];
    $qry = $pdo->prepare('select * from ' . SCHEMA . '.organisation order by organisation asc');
    $qry->execute();
    $tabS = array();
    while ($s = $qry->fetchObject()) {

        if (hasAccess($app, $app['session']->get('user_id'), $s->organisation_id, null, null, null, null, null, 'lecteur')) {
            $tabS[$s->organisation_id] = html_entity_decode($s->organisation);
        }
    }

    return $tabS;
}

/*
 * liste des organisation pour lesquelles le user a des droits sur les sysma_action
 */

function organisationWithUserAccessToSysmaActionList($app)
{

    $pdo = $app['pdo'];
    $qry = $pdo->prepare('select * from ' . SCHEMA . '.organisation order by organisation asc');
    $qry->execute();
    $tabS = array();
    while ($s = $qry->fetchObject()) {

        if (hasAccess($app, $app['session']->get('user_id'), null, null, null, null, null, $s->organisation_id, 'lecteur')) {
            $tabS[$s->organisation_id] = html_entity_decode($s->organisation);
        }
    }

    return $tabS;
}

/*
 * liste lesquels le user a les droits
 */

function objectTypesFilteredList($app)
{

    $pdo = $app['pdo'];
    $qry = $pdo->prepare('select * from ' . SCHEMA . '.sysma_object_type order by sysma_object_type');
    $qry->execute();
    $tabS = array();
    while ($s = $qry->fetchObject()) {

        if (hasAccess($app, $app['session']->get('user_id'), null, $s->sysma_object_type_id, null, null, null, null, 'lecteur')) {
            $tabS[$s->sysma_object_type_id] = str_replace("&#039;", "'", $s->sysma_object_type);
        }
    }

    return $tabS;
}

/*
 * liste lesquels le user a les droits
 */

function userLayersList($schema, $pdo)
{


    $tabCouches = array();

    $qry = $pdo->prepare('select * from information_schema.tables where table_schema = :schema order by table_name');
    $qry->bindParam(':schema', $schema);

    if ($qry->execute()) {


        while ($layer = $qry->fetchObject()) {

            if (estUneCoucheGeographique($schema, $layer->table_name, $pdo)) {

                $tabCouches[$layer->table_name] = $layer->table_name . ' (' . geoLayerType($schema, $layer->table_name, $pdo) . ')';
            }
        }
        return $tabCouches;
    } else {
        return false;
    }
}

function estUneCoucheGeographique($schema, $layer, $pdo)
{


    $qry2 = $pdo->prepare('select column_name from information_schema.columns where table_name = :table and column_name = \'geom\' and table_schema = :schema');
    $qry2->bindParam(':table', $layer);
    $qry2->bindParam(':schema', $schema);
    $qry2->execute();
    return $qry2->rowCount();
}

function geoLayerType($schema, $layer, $pdo)
{

    $qry2 = $pdo->prepare('select ST_GeometryType(geom) as type from ' . $schema . '."' . $layer.'"');
    $qry2->execute();
    if ($qry2->rowCount() > 0) {
        $res = $qry2->fetchObject();
        return $res->type;
    } else {
        return false;
    }
}


function preSelect($array, $needle) {

    foreach($array as $key => $val) {       
        if (strpos(strtolower($val), strtolower($needle))!== false) {
            return $key ;
        }
    }

}

/*
 * charge une liste de type objet de type géométrique donné
 * filter ces types d'objet pour les contributeur
 */

function objectTypesOfGeomTypeList($typegeom, $niveau_acces, $app)
{

    $typegeom === 'ST_LineString' ? $typegeom = 'ST_Linestring' : null;
    $typegeom === 'ST_MultiLineString' ? $typegeom = 'ST_Linestring' : null;
    $typegeom === 'ST_MultiPolygon' ? $typegeom = 'ST_Polygon' : null;


    $pdo = $app['pdo'];
    $qry = $pdo->prepare('select sysma_object_type_id,sysma_object_type from ' . SCHEMA . '.sysma_object_type where geometry_type = :tg order by sysma_object_type');
    $qry->bindParam(':tg', $typegeom, PDO::PARAM_STR);
    $qry->execute();
    $tabS = array();
    while ($s = $qry->fetchObject()) {
        if (hasAccess($app, $app['session']->get('user_id'), null, $s->sysma_object_type_id, null, null, null, null, $niveau_acces)) {
            $tabS[$s->sysma_object_type_id] = str_replace("&#039;", "'", $s->sysma_object_type);
        }
    }
    return $tabS;
}

/*
 * cherche un paramètre du type d'objet geo fourni et d'alias fourni
 */

function parameterFromAlias($object_type, $id_type, $alias, $app)
{

    $pdo = $app['pdo'];
    $qry = $pdo->prepare('select * from ' . SCHEMA . '.' . $object_type . '_type_parameter where parameter_alias = :alias and ' . $object_type . '_type_id = :id_type');
    $qry->bindParam(':alias', $alias, PDO::PARAM_STR);
    $qry->bindParam(':id_type', $id_type, PDO::PARAM_STR);
    $qry->execute();
    if ($qry->rowCount() > 0) {
        return $qry->fetchObject();
    } else {
        return false;
    }
}

/*
 * charge la liste des paramètres de type donné pour un type d'objet géo
 * filter ces types d'objet pour les contributeur
 */

function parameterFromTypeList($type, $sysma_object_type_id, $niveau_acces, $app)
{

  
    $pdo = $app['pdo'];
    if ($type === 'integerType') {
        $qry = $pdo->prepare('select * from ' . SCHEMA . '.sysma_object_type_parameter where data_type in (\'integerType\',\'booleanType\',\'textType\') and sysma_object_type_id = :idto order by "ordered_by"');
    }  elseif ($type === 'floatType') {
        $qry = $pdo->prepare('select * from ' . SCHEMA . '.sysma_object_type_parameter where data_type in (\'floatType\',\'textType\') and sysma_object_type_id = :idto order by "ordered_by"');
    }  elseif ($type === 'booleanType') {
        $qry = $pdo->prepare('select * from ' . SCHEMA . '.sysma_object_type_parameter where data_type in (\'booleanType\') and sysma_object_type_id = :idto order by "ordered_by"');
    }
    else {
        $qry = $pdo->prepare('select * from ' . SCHEMA . '.sysma_object_type_parameter where (data_type ilike (\'%' . $type . '%\') or (\'' . $type . '\'=\'textType\' and (data_type=\'textChoiceType\' or data_type=\'multipleTextChoiceType\' or data_type=\'longTextType\'))) and sysma_object_type_id = :idto order by "ordered_by"');
    }
    // $qry->bindParam(':td', $type, PDO::PARAM_STR);
    $qry->bindParam(':idto', $sysma_object_type_id, PDO::PARAM_INT);
    $qry->execute();
    //var_dump($pdo->errorInfo()) ;
    $tabS = array();
    while ($s = $qry->fetchObject()) {
        if (hasAccess($app, $app['session']->get('user_id'), null, $s->sysma_object_type_id, null, null, null, null, $niveau_acces)) {
            $tabS['idp___' . $s->parameter_id] = str_replace("&#039;", "'", $s->parameter);
            if ($s->parameter_alias != '')
                $tabS['idp___' . $s->parameter_id] .= str_replace("&#039;", "'", ' [' . $s->parameter_alias . '] (' . $s->data_type . ') ');
                $tabS['idp___' . $s->parameter_id] = '[DICTIONNAIRE] '.$tabS['idp___' . $s->parameter_id] ;

        }
    }
   // var_dump($tabS);
    return $tabS;
}

function typeGeoIntegerPG($typeGeo)
{
    // type geom

    $typeg = null;
    switch ($typeGeo) {

        case 'ST_Point':
            $typeg = 1;
            break;
        case 'ST_LineString':
            $typeg = 2;
            break;
        case 'ST_MultiLineString':
            $typeg = 2;
            break;
        case 'ST_Polygon':
            $typeg = 3;
            break;
        case 'ST_MultiPolygon':
            $typeg = 3;
            break;
    }

    return $typeg;
}

function verifieObjetPreExistantSysma($sysma_object_id, $suppr, $paramArrayValue, $mygeom, $sysma_object_type_id_cible, $Valeurs, $app)
{

    
    $nbModif = 0;
    $compt = 0;
    $tabValeurs = $tabUpdates = array();

    // recherche d'un objet avec cet identifiant
    $O = new SysmaObject($sysma_object_id, $app);
    $O->LoadGeom();


    if (isset($O->id) or $sysma_object_id === 0) {

        if ($suppr === 1 or $suppr === '1') {
            return array('res' => 'to_delete');
        } else {

            // vérification des infos de l'objet et comparaison aux infos fournies dans le shape
            // on parcourt les infos du shape qui sont mappées à un parameter
            // colonne shape => nom du paramètre
            foreach ($paramArrayValue as $key => $p) {

               //echo $key.' => '.$p.'<br>' ;

                // champ mappé, recherche de value existante
                if (substr($p, 0, 6) === 'idp___') {

                    $idp = substr($p, 6); 
                    $colNameInGISTable = substr($key,11) ;                   
                    $V = new SysmaObjectParameterValue(null, $app);
                    $V->buildFromParameters($O->id, $idp, null, $app);

                    if (substr(str_replace("&#039;", "'", str_replace('&quot;', '"', trim($V->value, ' '))), 0, 250) == substr($Valeurs[$colNameInGISTable], 0, 250) or $Valeurs[$colNameInGISTable] == null) {
                        // value inchangée                       
                    } else {
                        // value modifiée
                        $nbModif++;

                        $tabValeurs[$O->id][$idp]['before'] = substr(str_replace("&#039;", "'", str_replace('&quot;', '"', trim($V->value, ' '))), 0, 250);
                        $tabValeurs[$O->id][$idp]['after'] = $Valeurs[$colNameInGISTable];

                        $tabUpdates[$compt]['sysma_object_id'] = $O->id;
                        $tabUpdates[$compt]['parameter_id'] = $idp;
                        $tabUpdates[$compt]['value'] = $Valeurs[$colNameInGISTable];
                        $compt++;
                    }
                }
            }

            if ($mygeom != $O->geomRaw) {
                $tabUpdates[$compt]['sysma_object_id'] = $O->id;
                $tabUpdates[$compt]['parameter_id'] = 'geom';
                $tabUpdates[$compt]['value'] = null;
                $nbModif++;
            }

            if ($nbModif > 0) {
                return array('res' => 'to_update', 'tabValeurs' => $tabValeurs, 'tabUpdates' => $tabUpdates);
            } else {
                return array('res' => 'no_change');
            }
        }
    } else {
       
        $todo = verifieGeomPreExistantSysma($mygeom, $sysma_object_type_id_cible, $app);
        return $todo;
    }
}

/*
 * vérifie s'il existe un objet sysma de même géom et de même type
 * permet de ne pas importer d'éventuels doublons de nouveaux objets
 * @param string geometry 
 * @param integer sysma_object_type_id
 * @param $app
 * @return array('res'=> 'new' / 'already imported')
 * 
 */

function verifieGeomPreExistantSysma($geom, $sysma_object_type_id, $app)
{

    $todo = array();
    $ids = array();
    $pdo = $app['pdo'];

    $rq = 'select * from ' . SCHEMA . '.sysma_object where geom = :geom and sysma_object_type_id = :idt';
    $qry = $pdo->prepare($rq);

    $qry->bindParam(':geom', $geom, PDO::PARAM_STR);
    $qry->bindParam(':idt', $sysma_object_type_id, PDO::PARAM_INT);
    $qry->execute();
    if ($qry->rowCount() > 0) {
        while ($res = $qry->fetchObject()) {
            $ids[] = $res->sysma_object_id;
        }
        $todo = array('res' => 'already_imported', 'ids' => implode(',', $ids), 'count' => $qry->rowCount());
    } else {
        $todo = array('res' => 'new');
    }
    return $todo;
}

/*
 * importe dans sysma une layer shape, gère les objets sysma déjà existants, à modifier, ou à supprimer (= date de fin)
 * @param string schema_source
 * @param string layer_source
 * param integer sysma_object_type_id_cible
 * @param array paramArrayCibles array('colonne_source' => 'colonne_cible')
 * @param $app
 * @return boolean
 */

function importShapeSysma2($schema_source, $layer_source, $sysma_object_type_id_cible, $paramArraysCibles, $organisation, $date_releve, $action_import, $tab_param_action, $app)
{


    var_dump($paramArraysCibles) ;

  //  die() ;


    $pdo = $app['pdo'];

    $name = $suppr = null;
    $sysma_object_id = null;
    $erreurs = 0;
    $rapport = null;
    foreach ($paramArraysCibles as $key => $p) {
        // colonne à utiliser comme nom de l'objet
        if ($p === 'name')
            $name = $key;
        if ($p === 'sys_sysma_object_id')
            $sysma_object_id = $key;
        if ($p === 'sys_suppr')
            $suppr = $key;
    }





    // pré vérif des infos utiles pour l'import des actions 

    if ($action_import) {

        $paramPrepActionIsOk = 0;

        // preparation des colonnes utiles

        // pour chaque colonne de la table à importer
        foreach ($paramArraysCibles as $key => $p) {

            // pour chaque paramètre action necessaire
            foreach ($tab_param_action as $key_param_action => $desc_param_action) {

                // la colonne est-elle utile ?
                if ($p == $key_param_action) {
                    // $sysact___sysma_action_type_alias = $colonne_contenant_le_type_alias
                    $$key_param_action = $key;
                    echo $key;
                    $paramPrepActionIsOk++;
                }
            }
        }



        //      die(); 

        // tout est OK pour traiter la fiche action
        // = autant d'infos que nécessaires
        if ($paramPrepActionIsOk < count($tab_param_action)) {
            return array('status' => 'error', 'nbErreurs' => 1, 'rapport' => 'Champs manquants pour l\'import des actions');
        }
    }



    // couche à importer
    $qry = $pdo->prepare('select *,(ST_Dump(ST_Force2D(ST_CollectionExtract(geom,:typeg)))).geom as mygeom from ' . $schema_source . '.' . $layer_source);
    $typeg = typeGeoIntegerPG(geoLayerType($schema_source, $layer_source, $pdo));
    $qry->bindParam(':typeg', $typeg, PDO::PARAM_INT);
    $qry->execute();

    // pour chaque objet de la layer à importer
    while ($s = $qry->fetchObject()) {


        // 1. l'objet est il nouveau, à modifier, à supprimer ?
        if ($sysma_object_id != null) {
            $suppr != null ? $s_sup = $s->$suppr : $s_sup = null;
            $todo = verifieObjetPreExistantSysma($s->$sysma_object_id, $s_sup, $paramArraysCibles, $s->mygeom, $sysma_object_type_id_cible, $s, $app);
        } else {
            $todo = array('res' => 'new');
        }


        // rapport
        if ($todo['res'] === 'no change') {
            $rapport .= '<p class="">Objet ' . $s->$sysma_object_id . ' : pas de changement</p>';
        } elseif ($todo['res'] === 'already imported') {
            $rapport .= '<p class="">' . $todo['count'] . ' objet(s) Sysma de type ' . $sysma_object_type_id_cible . ' et de même géométrie que l\'objet GID ' . $s->gid . ' existe(nt) déjà : pas de changement. Objet(s) existant(s) : ' . $todo['ids'] . '</p>';
        }


        // objet à supprimer (= update de la date de fin)
        elseif ($todo['res'] === 'to delete') {

            // on update la date de fin de l'objet
            $O = new SysmaObject($s->$sysma_object_id, $app);
            $tab = array();
            $tab = array_merge($tab, array('sysma_object_id' => $s->$sysma_object_id));
            $tab = array_merge($tab, array('end_date' => $date_releve));
            $res = $O->update($tab);
            $res = json_decode($res);
            if ($res->status === 'success') {
                $rapport .= '<p class="text-success">Objet ' . $s->$sysma_object_id . ' : date de fin = ' . $date_releve . '</p>';
            } else {
                $rapport .= '<p class="text-danger">Objet ' . $s->$sysma_object_id . ' : echec de la mise à jour de la date de fin = ' . $date_releve . '</p>';
                $erreurs++;
            }
        }

        // update à faire sur les paramètres de l'objet
        elseif ($todo['res'] === 'to update') {

            foreach ($todo['tabUpdates'] as $tab) {

                $rapport .= '<p class="">Objet ' . $s->$sysma_object_id . '';
                $rapport .= ' - Paramètre ' . $tab['parameter_id'] . ' - ';

                $V = new SysmaObjectParameterValue(null, $app);
                $V->buildFromParameters($tab['sysma_object_id'], $tab['parameter_id'], null, $app);


                $tabV = array();
                $tabV = array_merge($tabV, array('value' => $tab['value']));
                $tabV = array_merge($tabV, array('start_date' => $date_releve));
                $tabV = array_merge($tabV, array('start_date' => date('Y-m-d H:i:s')));
                $tabV = array_merge($tabV, array('end_date' => '0'));
                $tabV = array_merge($tabV, array('parameter_id' => $tab['parameter_id']));
                $tabV = array_merge($tabV, array('sysma_object_id' => $tab['sysma_object_id']));
                $tabV = array_merge($tabV, array('data_index' => $V->data_index));
                $tabV = array_merge($tabV, array('modified_by' => $app['session']->get('user_id')));
                $tabV = array_merge($tabV, array('user_id' => $app['session']->get('user_id')));

                if ($V->update($tabV)) {
                    $rapport .= '<span class="text-success">Valeur mise à jour : ' . $V->value . ' => ' . $s->$key . '</span>';
                } else {
                    $rapport .= '<span class="text-danger">Echec de la mise à jour : ' . $V->value . ' => ' . $s->$key . '</span>';
                    $erreurs++;
                }

                $rapport .= '</p>';
            }
        } elseif ($todo['res'] === 'new') {


            // die();


            // création de l'objet
            $id = dataIndex($app['pdo']);
            $O = new SysmaObject(null, $app);
            // tableau des paramètres de créations de l'objet
            $tab = array();
            if ($name !== null) {
                $tab = array_merge($tab, array('sysma_object' => $s->$name));
            }
            $tab = array_merge($tab, array('sysma_object_type_id' => $sysma_object_type_id_cible));
            $tab = array_merge($tab, array('geom' => $s->mygeom));
            $tab = array_merge($tab, array('start_date' => $date_releve));
            $tab = array_merge($tab, array('end_date' => '0'));
            $tab = array_merge($tab, array('status' => 'existant'));
            $tab = array_merge($tab, array('organisation_id' => $organisation));
            $tab = array_merge($tab, array('modified_by' => $app['session']->get('user_id')));
            $tab = array_merge($tab, array('created_by' => $app['session']->get('user_id')));
            $tab = array_merge($tab, array('sysma_object_id' => $id));

            $res = $O->create($tab);
            $res = json_decode($res);
            // $res = new stdClass();
            //   $res->status = 'success' ;


            if ($res->status === 'success') {


                $rapport .= '<p class="text-success">Objet ' . $id . ' créé</p>';

                // insertion des données
                foreach ($paramArraysCibles as $key => $p) {

                    if ($p != 'name' and $p != '0' and $s->$key !== null and substr($p, 0, 9) !== 'sysact___') {

                        $V = new SysmaObjectParameterValue(null, $app);
                        $tabV = array();
                        $tabV = array_merge($tabV, array('value' => $s->$key));
                        $tabV = array_merge($tabV, array('start_date' => $date_releve));
                        $tabV = array_merge($tabV, array('end_date' => '0'));
                        $tabV = array_merge($tabV, array('parameter_id' => substr($p, 6)));
                        $tabV = array_merge($tabV, array('sysma_object_id' => $id));
                        $tabV = array_merge($tabV, array('data_index' => dataIndex($app['pdo'])));
                        $tabV = array_merge($tabV, array('modified_by' => $app['session']->get('user_id')));
                        $tabV = array_merge($tabV, array('user_id' => $app['session']->get('user_id')));
                        $tab['value'] = explode(' [] ', $tabV['value']);


                        $V = new SysmaObjectParameterValue(null, $app);

                        if (count($tab['value']) == 1) {
                            $res2 = $V->create($tabV);
                        } elseif (count($tab['value']) > 1) {
                            $index = dataIndex($app['pdo']);
                            foreach ($tab['value'] as $value) {
                                $tabToInsert = $tabV;
                                $tabToInsert['data_index'] = $index;
                                $tabToInsert['value'] = $value;
                                $res2 = $V->create($tabToInsert);
                            }
                        }
                        $res2 = json_decode($res2);

                        if ($res2->status === 'success') {

                            $rapport .= '<p class="text-success">Objet ' . $id . '';
                            $rapport .= ' - Paramètre ' . substr($p, 6) . ' - ';
                            $rapport .= 'Valeur ajouté : ' . $s->$key . '</p>';
                        } else {

                            $rapport .= '<p class="text-danger">Objet ' . $id . '';
                            $rapport .= ' - Paramètre ' . substr($p, 6) . ' - ';
                            $rapport .= 'Erreur lors l\'ajout de la value : ' . $s->key . '</p>';
                            $erreurs++;
                        }
                    }
                }


                // action
/*
                if ($action_import) {

                    $paramPrepActionIsOk = 0;

                    // preparation des colonnes utiles

                    // pour chaque colonne de la table à importer
                    foreach ($paramArraysCibles as $key => $p) {

                        // pour chaque paramètre action necessaire
                        foreach ($tab_param_action as $key_param_action => $desc_param_action) {

                            // la colonne est-elle utile ?
                            if ($p == $key_param_action) {
                                // $sysact___sysma_action_type_alias = $colonne_contenant_le_type_alias
                                $$key_param_action = $key;
                                $paramPrepActionIsOk++;
                            }
                        }
                    }



                    //      die(); 

                    // tout est OK pour traiter la fiche action
                    // = autant d'infos que nécessaires
                    if ($paramPrepActionIsOk == count($tab_param_action)) {

                        // pas d'iD action et pas d'ID temp action
                        if (
                            // pas de colonne identifiée
                            (!isset($sysact___sysma_action_id) or $sysact___sysma_action_id === null
                                // l'id est nul
                                or (@$s->sysact___sysma_action_id == '' or @$s->$sysact___sysma_action_id == null))
                            // la fiche n'est pas déjà traitée
                            and !isset($nouvelleFADejaTraitee[$s->$sysact___sysma_action_temp_id])

                        ) {

                            // nouvelle FA


                            $FA = new SysmaAction(null, $app);
                            $sysma_action_type_id_cible = rechercheSysmaActionTypeIdDepuisAlias($app, $s->$sysact___sysma_action_type_alias, $sysma_object_type_id_cible);
                            $dataAction =
                                [
                                    'sysma_action_type_id' => $sysma_action_type_id_cible,
                                    'sysma_action' => $s->$sysact___sysma_action,
                                    'start_date' => $s->$sysact___sysma_action_start_date,
                                    'end_date' => $s->$sysact___sysma_action_end_date,
                                    'program_year' => $s->$sysact___sysma_program_year,
                                    'unit_cost_estimated_user' => $s->$sysact___unit_cost_estimated_user,
                                    'unit_measure_estimated_user' => $s->$sysact___unit_measure_estimated_user,
                                    'total_cost_estimated_user' => $s->$sysact___total_cost_estimated_user,
                                    'total_cost_final_user' => $s->$sysact___total_cost_final_user,
                                    'status' => $s->$sysact___sysma_action_status,
                                    'organisation_id' => $s->$sysact___sysma_action_structure_id,
                                    'contracts' => '{' . str_replace(' [] ', ',', $s->$sysact___sysma_action_contracts) . '}',
                                    'created_by' => $app['session']->get('id'),
                                    'modified_by' => $app['session']->get('id')
                                ];

                            $resFA = json_decode($FA->create($dataAction));

                            if ($resFA->status == 'success') {

                                $nouvelleFADejaTraitee[$s->sysact_sysma_action_temp_id] = $resFA->id;
                                // on recharge l'objet que l'on vient de créer
                                $leNouvelObjet = new SysmaObject($res->id, $app);
                                $leNouvelObjet->linkSysmaActionSheet($resFA->id, $app['session']->get('id'));

                                $rapport .= '<p class="text-success">Action ' . $resFA->id . ' créée';
                                $rapport .= ' - Lien avec Objet ' . $res->id . ' créé</p>';
                            } else {
                                $rapport .= '<p class="text-danger">Echec lors de la création de l\'action sur l\'objet ' . $res->id . '</p>';
                                $erreurs++;
                            }
                        } elseif (($s->sysact___sysma_action_id == '' or $s->sysact___sysma_action_id == null) and isset($nouvelleFADejaTraitee[$s->sysact_sysma_action_temp_id])) {

                            // la nouvelle action a déjà été créée
                            // on doit juste crée un lien avec l'objet en cours
                            $leNouvelObjet = new SysmaObject($res->id, $app);
                            $leNouvelObjet->linkSysmaActionSheet($nouvelleFADejaTraitee[$s->sysact_sysma_action_temp_id], $app['session']->get('id'));
                        } elseif ($s->sysact___sysma_action_id != null or $s->sysact___sysma_action_id == '') {

                            // l'action existe, on doit la mettre à jour si infos différentes



                        }
                    } else {
                        $rapport .= '<p class="text-danger">Des infos sont manquantes pour l\'import des actions</p>';
                        $erreurs++;
                    }
                }*/
            } else {
                $rapport .= '<p class="text-danger">Erreur lors de la création de l\'objet ' . $id . '</p>';
                $erreurs++;
            }
        }
    }

    if ($erreurs > 0) {
        $status = 'echec';
    } else {
        $status = 'succes';
    }

    $res_rapport = enregistreRapportImport($date_releve, $organisation, $sysma_object_type_id_cible, json_encode(array('schema' => $schema_source, 'layer_import' => $layer_source, 'status' => $status, 'nbErreurs' => $erreurs, 'rapport' => $rapport)), $app['session']->get('user_id'), $app);

    return array('status' => $status, 'nbErreurs' => $erreurs, 'rapport' => $rapport, 'res_rapport' => $res_rapport);
}


function rechercheSysmaActionTypeIdDepuisAlias($app, $sysma_action_type_alias, $sysma_object_type_id)
{

    $pdo = $app['pdo'];
    $qry = $pdo->prepare('select sysma_action_type_id from ' . SCHEMA . '.sysma_action_type where sysma_action_type_alias = :alias and sysma_object_type_id = :id');

    $qry->bindParam(':alias', $sysma_action_type_alias, PDO::PARAM_STR);
    $qry->bindParam(':id', $sysma_object_type_id, PDO::PARAM_INT);
    $qry->execute();
    if ($qry->rowCount() > 0) {
        $res = $qry->fetchObject();
        return $res->sysma_action_type_id;
    } else {
        return null;
    }
}

function enregistreRapportImport($date_releve, $organisation, $sysma_object_type_id, $data, $user_id, $app)
{

    $pdo = $app['pdo'];
    $qry = $pdo->prepare('insert into ' . SCHEMA . '.import_report (report_date, user_id, organisation_id, data_date, report, sysma_object_type_id) VALUES (\'' . date('Y-m-d H:i:s') . '\', :user_id, :organisation_id, :data_date, :rapport, :sysma_object_type_id) ');

    $qry->bindParam(':user_id', $user_id, PDO::PARAM_INT);
    $qry->bindParam(':organisation_id', $organisation, PDO::PARAM_INT);

    $qry->bindParam(':data_date', $date_releve, PDO::PARAM_STR);
    $qry->bindParam(':rapport', $data, PDO::PARAM_STR);
    $qry->bindParam(':sysma_object_type_id', $sysma_object_type_id, PDO::PARAM_INT);

    return $qry->execute();
}

/*
 * importe dans sysma une layer shape
 * @param string schema_source
 * @param string layer_source
 * param integer sysma_object_type_id_cible
 * @param array paramArrayCibles array('colonne_source' => 'colonne_cible')
 * @param $app
 * @return boolean
 */

function importShapeSysma($schema_source, $layer_source, $sysma_object_type_id_cible, $paramArraysCibles, $organisation, $date_releve, $app)
{


    // objets source
    $pdo = $app['pdo'];

    $name = null;
    foreach ($paramArraysCibles as $key => $p) {
        // colonne à utiliser comme nom de l'objet
        if ($p === 'name')
            $name = $key;
    }


    $qry = $pdo->prepare('select *,(ST_Dump(ST_Force_2D(ST_CollectionExtract(geom,:typeg)))).geom as mygeom from ' . $schema_source . '.' . $layer_source);
    $typeg = typeGeoIntegerPG(geoLayerType($schema_source, $layer_source, $pdo));
    $qry->bindParam(':typeg', $typeg, PDO::PARAM_INT);
    $qry->execute();
    while ($s = $qry->fetchObject()) {

        echo $s->gid . '<br>';

        $id = dataIndex($app['pdo']);
        $O = new SysmaObject(null, $app);
        // tableau des paramètres de créations de l'objet
        $tab = array();
        if ($name !== null) {
            $tab = array_merge($tab, array('sysma_object' => $s->$name));
        }
        $tab = array_merge($tab, array('sysma_object_type_id' => $sysma_object_type_id_cible));
        $tab = array_merge($tab, array('geom' => $s->mygeom));
        $tab = array_merge($tab, array('start_date' => $date_releve));
        $tab = array_merge($tab, array('end_date' => '0'));
        $tab = array_merge($tab, array('status' => 'existant'));
        $tab = array_merge($tab, array('organisation_id' => $organisation));
        $tab = array_merge($tab, array('modified_by' => $app['session']->get('user_id')));
        $tab = array_merge($tab, array('created_by' => $app['session']->get('user_id')));
        $tab = array_merge($tab, array('sysma_object_id' => $id));



        // var_dump($tab);
        //die() ;

        $res = $O->create($app, $tab);
        $res = json_decode($res);
        // var_dump($res);
        if ($res->status === 'success') {


            // var_dump($paramArraysCibles);
            // insertion des données
            foreach ($paramArraysCibles as $key => $p) {

                if ($p != 'name' and $p != '0' and $s->$key != null) {

                    $V = new Valeurparameter(null, $app);
                    $tabV = array();
                    $tabV = array_merge($tabV, array('value' => $s->$key));
                    $tabV = array_merge($tabV, array('start_date' => $date_releve));
                    $tabV = array_merge($tabV, array('end_date' => '0'));
                    $tabV = array_merge($tabV, array('parameter_id' => substr($p, 3)));
                    $tabV = array_merge($tabV, array('sysma_object_id' => $id));
                    $tabV = array_merge($tabV, array('data_index' => dataIndex($app['pdo'])));
                    $tabV = array_merge($tabV, array('modified_by' => $app['session']->get('user_id')));
                    $tabV = array_merge($tabV, array('user_id' => $app['session']->get('user_id')));
                    /*
                      $P = new SysmaObjectParameter(substr($p, 3),$app) ;

                      if ($P->dataType=='floatType' or $P->dataType=='integerType' and $s->$key == null) {
                      $tabV = array_merge($tabV, array('value' => 0));
                      } */

                    $V = new SysmaObjectParameterValue(null, $app);
                    $res2 = $V->create($tabV);
                    $res2 = json_decode($res2);
                    //   var_dump($res2);
                }
            }
        }
    }


    return true;
}

/*
 * charge la liste des colonnes d'une table
 */

function layerColumnsList($schema, $layer, $pdo)
{


    $qry2 = $pdo->prepare('select column_name,data_type from information_schema.columns where table_name = :table and table_schema = :schema and column_name != \'geom\' ');
    $qry2->bindParam(':table', $layer);
    $qry2->bindParam(':schema', $schema);
    $qry2->execute();
    while ($s = $qry2->fetchObject()) {

    $cols[] = array('name' => $s->column_name, 'type' => checkColumnDataType($pdo, $schema, $layer, $s->column_name) /*dataTypeSysma($s->data_type)*/ );
    }

    return $cols;
}


/*
* load distinct values form a column table, usefull for catergory choices creation
*/

function loadDistinctColumnValues($schema, $layer, $column, $pdo) {
    
    $qry2 = $pdo->prepare("select $column as value from $schema.\"$layer\" group by $column order by $column asc");  
    $qry2->execute();
    $vals = array() ;
    while ($s = $qry2->fetchObject()) {
        $vals[] = $s->value;
    }
    return $vals;

}


function checkColumnDataType($pdo, $schema, $table, $column)
{

    $q = '    
        SELECT xx_99_utils.f_column_scan_analyse_content (
           (\'{"schemaname" : "' . $schema . '"
            , "tablename" : "' . $table . '"
            , "column_name" : "'. $column.'"}\' :: json
         )) ';

    //echo $q;

    $qry = $pdo->prepare($q);
   
    if ($qry->execute()) {
        $res = $qry->fetchObject() ;
        
        $res2 = json_decode($res->f_column_scan_analyse_content) ;
       // echo $res2->return_analyse_detail->col_type_estimate_best.' -> '. dataTypeSysma($res2->return_analyse_detail->col_type_estimate_best) ;
        return dataTypeSysma($res2->return_analyse_detail->col_type_estimate_best) ;
    }
    else  {
        return false ;
    }

}



/*
 * convertit un type PG en type de donnée Sysma
 * integer -> entier
 * return null si type non sysma
 */

function dataTypeSysma($type)
{

    switch ($type) {
        case 'integer':
            return 'integerType';
        case 'bigint':
            return 'integerType';
        case 'int4':
            return 'integerType';
        case 'int8':
            return 'integerType';
        case 'integer':
            return 'integerType';
        case 'boolean':
            return 'booleanType';
        case 'character varying':
            return 'textType'; // return 'texte,texteLD,texteLong' ;
        case 'text':
            return 'textType';
        case 'numeric':
            return 'floatType';
        case 'NUMERIC':
                return 'floatType';
        case 'double precision':
            return 'floatType';
        default:
            return 'textType';
    }
}

function listeTypesTravauxFiltres($app)
{

    $pdo = $app['pdo'];
    $qry = $pdo->prepare('select * from ' . SCHEMA . '.sysma_action_type tt LEFT JOIN ' . SCHEMA . '.sysma_object_type tyo ON tyo.sysma_object_type_id = tt.sysma_object_type_id order by tyo.sysma_object_type, tt.sysma_action_type');
    $qry->execute();
    $tabS = array();
    while ($s = $qry->fetchObject()) {

        if (hasAccess($app, $app['session']->get('user_id'), null, $s->sysma_object_type_id, null, $s->sysma_action_type_id, null, null, 'lecteur')) {
            $tabS[$s->sysma_action_type_id] = str_replace("&#039;", "'", $s->sysma_object_type) . ' - ' . str_replace("&#039;", "'", $s->sysma_action_type);
        }
    }

    return $tabS;
}

/*
 * liste lesquels le user a les droits
 */

function objectStatusFilteredList($app)
{

    $pdo = $app['pdo'];
    $qry = $pdo->prepare('select * from ' . SCHEMA . '.status where object_type = \'sysma_object\' order by "display_order"');
    $qry->execute();
    $tabS = array();
    while ($s = $qry->fetchObject()) {

        if (hasAccess($app, $app['session']->get('user_id'), null, null, $s->status_value, null, null, null, 'lecteur')) {
            $tabS[$s->status_value] = $s->status;
        }
    }

    return $tabS;
}

/*
 * liste lesquels le user a les droits
 */

function listeStatutsTravauxFiltres($app)
{

    $pdo = $app['pdo'];
    $qry = $pdo->prepare('select * from ' . SCHEMA . '.status where object_type = \'sysma_action\' order by display_order');
    $qry->execute();
    $tabS = array();
    while ($s = $qry->fetchObject()) {

        if (hasAccess($app, $app['session']->get('user_id'), null, null, null, null, $s->status_value, null, 'lecteur')) {
            $tabS[$s->status_value] = $s->status;
        }
    }

    return $tabS;
}

function findObjects($idstype, $idsorganisation, $status, $app)
{

    $s_sql = $st_sql = null;
    if ($idsorganisation[0] !== 'all') {
        $s = htmlentities(implode(',', $idsorganisation), ENT_QUOTES);
        $s_sql = 'and organisation_id in (' . $s . ') ';
    }
    if ($status[0] !== 'all') {
        $st = "'" . implode("','", $status) . "'"; // !
        $st_sql = ' and status in (' . $st . ') ';
    }


    if ($idstype[0] == 'all') {
        $t = null;
    } else {
        $t = 'and sysma_object_type_id in (' . htmlentities(implode(',', $idstype), ENT_QUOTES) . ') ';
    }

    $tabO = array();

    $pdo = $app['pdo'];
    //echo 'select *,ST_length(geom) as perimetre,ST_area(geom) as surface from ' . SCHEMA . '.sysma_object where status in (' . $st . ') and sysma_object_type_id in (' . $t . ') and organisation_id in (' . $s . ') order by sysma_object_id desc';
    $qry = $pdo->prepare('select *,ST_length(geom) as perimetre,ST_area(geom) as surface from ' . SCHEMA . '.sysma_object where true '
        . $t
        . $st_sql . $s_sql
        . 'order by sysma_object_id desc');


    $qry->execute() or var_dump($pdo->errorInfo());

    while ($o = $qry->fetchObject()) {
        if (hasAccess($app, $app['session']->get('user_id'), $o->organisation_id, $o->sysma_object_type_id, $o->status, null, null, null, 'lecteur')) {
            $O = new SysmaObject($o->sysma_object_id, $app);
            $O->geomRaw = $o->geom;
            $O->infosGeom['perimetre'] = $o->perimetre;
            $O->infosGeom['surface'] = $o->surface;
            $tabO[] = $O;
        }
    }



    return $tabO;
}

function rechercheTravaux($idstype, $idsorganisation, $status, $app)
{


    $t = htmlentities(implode(',', $idstype), ENT_QUOTES);
    $s = htmlentities(implode(',', $idsorganisation), ENT_QUOTES);
    $st = "'" . implode("','", $status) . "'"; // !
    $tabT = $tabOG = array();

    $pdo = $app['pdo'];
    //echo 'select sysma_object_id from ' . SCHEMA . '.sysma_object where status in (' . $st . ') and sysma_object_type_id in (' . $t . ') and organisation_id in (' . $s . ') order by sysma_object_id desc';
    $qry = $pdo->prepare('select * from ' . SCHEMA . '.sysma_action t where '
        . 't.status in (' . $st . ') and '
        . 't.sysma_action_type_id in (' . $t . ') and '
        . 't.organisation_id in (' . $s . ') '
        . 'order by t.sysma_action_id desc');

    $qry->execute() or var_dump($pdo->errorInfo());

    while ($o = $qry->fetchObject()) {
        //echo 'id t '.$o->sysma_action_id.' - id organisation '.$o->organisation_id.'<br>' ;
        if (hasAccess($app, $app['session']->get('user_id'), $o->organisation_id, null, null, $o->sysma_action_type_id, $o->status, $o->organisation_id, 'lecteur')) {
            $T = new SysmaAction($o->sysma_action_id, $app);
            $T->loadSysmaObjects();
            $tabT[] = $T;
        }
    }

    return $tabT;
}

/*
 * version plus complète avec les années et contracts
 */

function findSysmaActions2($idstype, $idsorganisation, $status, $anneesysma_action, $anneeengagement, $contract, $app)
{

    $s_sql = $st_sql = null;
    if ($idsorganisation[0] !== 'all') {
        $s = htmlentities(implode(',', $idsorganisation), ENT_QUOTES);
        $s_sql = 'and organisation_id in (' . $s . ')';
    }
    if ($status[0] !== 'all') {
        $st = "'" . implode("','", $status) . "'"; // !
        $st_sql = ' and status in (' . $st . ')';
    }

    if ($idstype[0] == 'all') {
        $t = null;
    } else {
        $t =  ' and t.sysma_action_type_id in (' . htmlentities(implode(',', $idstype), ENT_QUOTES) . ') ';
    }

    $tabT = $tabOG = array();

    $pdo = $app['pdo'];

    $req = 'select * from ' . SCHEMA . '.sysma_action t where true '
        . $s_sql . $st_sql
        . $t;


    if ($anneesysma_action != null) {
        $req .= ' and left(start_date,4) = :anneesysma_action ';
    }
    if ($anneeengagement != null) {
        $req .= ' and program_year = :anneeengagement ';
    }
    if ($contract != null) {
        $req .= ' and :contract = ANY(contracts)';
    }

    $req .= 'order by t.sysma_action_id desc';



    $qry = $pdo->prepare($req);


    if ($anneesysma_action != null)
        $qry->bindParam(':anneesysma_action', $anneesysma_action);
    if ($anneeengagement != null)
        $qry->bindParam(':anneeengagement', $anneeengagement);
    if ($contract != null)
        $qry->bindParam(':contract', $contract);

    $qry->execute() or var_dump($pdo->errorInfo());

    while ($o = $qry->fetchObject()) {
        //echo 'id t '.$o->sysma_action_id.' - id organisation '.$o->organisation_id.'<br>' ;
        if (hasAccess($app, $app['session']->get('user_id'), $o->organisation_id, null, null, $o->sysma_action_type_id, $o->status, $o->organisation_id, 'lecteur')) {
            $T = new SysmaAction($o->sysma_action_id, $app);
            $T->loadSysmaObjects();
            $tabT[] = $T;
        }
    }

    return $tabT;
}

/*
 * version plus complète sysma_object qui est la surface d'intersection limitant la recherche
 * sysma_object est de la forme r010_administratif_reglementaire.t_interco_bvsn_actu_geofla.gid.100
 */

function findSysmaActions3($idstype, $idsorganisation, $status, $anneesysma_action, $anneeengagement, $contract, $sysma_object, $app)
{


    $s_sql = $st_sql = null;
    if ($idsorganisation[0] !== 'all') {
        $s = htmlentities(implode(',', $idsorganisation), ENT_QUOTES);
        $s_sql = 'and t.organisation_id in (' . $s . ') ';
    }
    if ($status[0] !== 'all') {
        $st = "'" . implode("','", $status) . "'"; // !
        $st_sql = ' and t.status in (' . $st . ') ';
    }
    if ($idstype[0] == 'all') {
        $t = null;
    } else {
        $t =  ' and t.sysma_action_type_id in (' . htmlentities(implode(',', $idstype), ENT_QUOTES) . ') ';
    }


    $tabT = $tabOG = $tabObjets = $tabTWk = array();

    $pdo = $app['pdo.export'];
    //echo 'select sysma_object_id from ' . SCHEMA . '.sysma_object where status in (' . $st . ') and sysma_object_type_id in (' . $t . ') and organisation_id in (' . $s . ') order by sysma_object_id desc';

    $req = null;

    if ($sysma_object != null) {

        /* $tabOG
         * 0 : schema
         * 1 : table
         * 2 : gid
         * 5 : value de gid
         */

        $tabOG = explode('.', $sysma_object);

        $req = 'select t.sysma_action_id, t.sysma_action_type_id, t.status, t.organisation_id, o.sysma_object_id from ' . SCHEMA . '.sysma_action t,' . SCHEMA . '.sysma_object o,' . SCHEMA . '.l_sysma_object_sysma_action l, ' . $tabOG[0] . '.' . $tabOG[1] . ' g where true '
            . $st_sql . $s_sql
            . $t
            . $s_sql
            . 'and l.sysma_action_id = t.sysma_action_id and '
            . 'l.sysma_object_id = o.sysma_object_id and '
            . 'g.' . $tabOG[2] . ' = :ido and '
            . 'ST_intersects(g.geom,o.geom) ';
    } else {

        $req = 'select * from ' . SCHEMA . '.sysma_action t where true '
            . $st_sql . $s_sql . $t;
    }

    if ($anneesysma_action != null) {
        $req .= ' and left(t.start_date,4) = :anneesysma_action ';
    }
    if ($anneeengagement != null) {
        $req .= ' and t.program_year = :anneeengagement ';
    }
    if ($contract != null) {
        $req .= ' and :contract = ANY(t.contracts) ';
    }

    if ($sysma_object != null) {
        //   $req .= 'group by t.sysma_action_id, t.sysma_action_type_id, t.status, t.organisation_id ';
    }

    $req .= 'order by t.sysma_action_id desc';

    // echo $req;

    $qry = $pdo->prepare($req);


    if ($anneesysma_action != null)
        $qry->bindParam(':anneesysma_action', $anneesysma_action);
    if ($anneeengagement != null)
        $qry->bindParam(':anneeengagement', $anneeengagement);
    if ($contract != null)
        $qry->bindParam(':contract', $contract);
    if ($sysma_object != null)
        $qry->bindParam(':ido', $tabOG[5]);


    $qry->execute() or var_dump($pdo->errorInfo());

    while ($o = $qry->fetchObject()) {
        //echo 'id t '.$o->sysma_action_id.' - id organisation '.$o->organisation_id.'<br>' ;
        if (hasAccess($app, $app['session']->get('user_id'), $o->organisation_id, null, null, $o->sysma_action_type_id, $o->status, $o->organisation_id, 'lecteur')) {

            if (!isset($tabT[$o->sysma_action_id])) {

                $T = new SysmaAction($o->sysma_action_id, $app);
                $tabTWk[$o->sysma_action_id] = $T;
            }
            $tabObjets[$o->sysma_action_id][] = new SysmaObject($o->sysma_object_id, $app);
        }
    }

    foreach ($tabTWk as $T) {
        foreach ($tabObjets[$T->id] as $O) {
            $T->setArraySysmaObjects($O);
        }
        $tabT[] = $T;
    }


    return $tabT;
}

/*
 * charge la liste des autres layers
 */

function otherLayersList($pdo, $options)
{

    if (isset($options['sql']) and $options['sql'] !== null) {
        $sql = ' AND ' . $options['sql'];
    } else {
        $sql = null;
    }

    $qry = $pdo->prepare('select * from ' . SCHEMA . '.other_layer l '
        . 'left join ' . SCHEMA . '.layer_group g on g.group_name = l.group '
        . ' where 1=1 ' . $sql
        . 'order by g.group_display_order::text,l.other_layer_display_order::text asc');
    $qry->execute();
    return $qry->fetchAll(PDO::FETCH_ASSOC);
}

/*
 * function de création d'un objet simple issue de la base de données
 * remplace les entities car les objets sysma sont trop complexes pour la construction des forms du dictionnaire
 */

function buildSimpleEntity($table, $keycol, $keyvalue, $app)
{


    $tab = array();
    if (is_numeric($keyvalue) and $keyvalue != null) {


        $pdo = $app['pdo'];
        $qry = $pdo->prepare("select * from " . SCHEMA . "." . $table . " where " . $keycol . " = :id");
        $qry->bindParam(':id', $keyvalue, PDO::PARAM_INT);
        $qry->execute();
        $t = $qry->fetchObject();

        if ($qry->rowCount() > 0) {
            foreach ($t as $key => $value) {
                $tab[$key] = html_entity_decode($value);
            }
        }
    }

    return $tab;
}

/*
 * 
 */
/*
  function prepMultipleInsertQuery($request) {

  $reqFields = $reqParams = null;
  $compt = 0;

  foreach ($request as $r) {

  foreach ($r as $key => $val) {

  if ($compt == 1)
  $reqFields .= $key . ', '; // liste des champs, à ne lister que la première fois

  if ($key === 'value') {
  $reqParams[$compt] .= ':' . $key . $compt . ', ';
  $tabD[$compt][':' . $key . $compt] = $val;

  } else {
  $reqParams[$compt] .= ':' . $key . ', ';
  $tabD[$compt][':' . $key] = $val;
  }
  $compt++;
  }
  }

  return array('reqFields' => substr($reqFields, 0, -2), 'reqParams' => substr($reqParams, 0, -2), 'tabD' => $tabD);
  }
 */


/*
 * function saveLastData
 * sauvegarde les dates de début et fin, organisation_id saisies par le user dans la session afin de les proposer à la prochaine saisie
 * @param silex request $request 
 * @return boolean
 */

function saveLastDataParam($request, $app)
{
    /*
      $dd = null ;
      $dd = $request->get('start_date') ;

      if ($dd != null)
      $app['session']->set('start_date_value', $request->get('start_date'));
      if ($request->get('end_date') != null)
      $app['session']->set('end_date_value', $request->get('end_date')); */
    return true;
}

/*
 * function saveLastData
 * sauvegarde les dates de début et fin, organisation_id saisies par le user dans la session afin de les proposer à la prochaine saisie
 * @param silex request $request 
 * @return boolean
 */

function saveLastData($request, $app)
{

    foreach ($request as $key => $val) {
        $r[$key] = $val;
    }

    if (@$r['start_date'] != null)
        $app['session']->set('start_date', $r['start_date']);
    if (@$r['start_date'] != null)
        $app['session']->set('start_date', $r['start_date']);

    if (@$r['start_date'] != null and @$r['value'] != null)
        $app['session']->set('start_date_value', $r['start_date']);
    if (@$r['end_date'] != null and @$r['value'] != null)
        $app['session']->set('end_date_value', $r['end_date']);
    if (@$r['organisation_id'] != null)
        $app['session']->set('organisation_id', $r['organisation_id']);
    if (@$r['status'] != null)
        $app['session']->set('status', $r['status']);
    if (@$r['status'] != null and @$r['sysma_action_type_id'])
        $app['session']->set('status_sysma_action', $r['status']);
    if (@$r['sysma_object_type_id'] != null) {
        $app['session']->set('sysma_object_type_id', $r['sysma_object_type_id']);
        if (@$r['end_date'] != null)
            $app['session']->set('end_date', $r['end_date']);
    }
    if (@$r['sysma_action_type_id'] != null) {
        $app['session']->set('sysma_action_type_id', $r['sysma_action_type_id']);
        $app['session']->set('end_date_sysma_action', $r['end_date']);
    }
    return true;
}

/*
 * function prepInsertQuery
 * prépare une liste de paramètre pour une requête PDO d'insert
 * et un tableau des données 
 * 
 * @param Silex request $request
 * @param array exclude tableau des param à exclure de la requête
 * 
 * @return array(liste des params à updater mis en forme , tableau param > value) 
 */

function prepInsertQuery($request, $exclude = null)
{

    $reqFields = $reqParams = null;
    $tabD = array();
    foreach ($request as $key => $val) {

        if (!isset($exclude[$key])) {
            $reqFields .= '"' . $key . '", ';
            $reqParams .= ':' . $key . ', ';
            $tabD[':' . $key] = $val;
        }
    }


    return array('reqFields' => substr($reqFields, 0, -2), 'reqParams' => substr($reqParams, 0, -2), 'tabD' => $tabD);
}

/*
 * function prepUpdateQuery
 * prépare une liste de paramètre pour une requête PDO d'update
 * et un tableau des données 
 * 
 * @param Silex request $request
 * @param string index index de la table à ne pas ajouter dans la liste des params à updater
 * @return array(liste des params à updater mis en forme , tableau param > value) 
 */

function prepUpdateQuery($request, $index)
{

    $req = null;
    foreach ($request as $key => $val) {
        if ($key != $index)
            $req .= '"' . $key . '" = :' . $key . ', ';
        $tabD[':' . $key] = $val;
    }

    return array('req' => substr($req, 0, -2), 'tabD' => $tabD);
}

/*
 * function returnResultQuery(
 * reçoit une requête PDO et se charge de l'executer puis de renvoyer un message selon le résultat
 * @param PDOStatement qry
 * 
 * $param PDO pdo
 * $param tabD données
 * $id integer last ID
 * @return json result
 * 
 */

function returnResultQuery($qry, $pdo, $tabD)
{
    // TODO : why not a try&catch block here... and maybe deal with BEGIN, COMMIT, ROLLBACK directly from here too?
    if ($qry->execute()) {
        $result['status'] = 'success';
        $res = @$qry->fetchObject();
        if (isset($res->id))
            $result['id'] = $res->id;
    } else {
        $result['status'] = 'error';
        // $result['message'] = $pdo->errorInfo();
        $result['message'] = json_encode($pdo->errorInfo());
        $result['data'] = json_encode($tabD); // . ' ' . $qry->debugDumpParams();
        if (DEBUG) {
            // $result['message'] .= json_encode($qry->debugDumpParams());
            $result['message'] .= $qry->debugDumpParams();
            //debug_print_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
        }
    }
    return json_encode($result);

    /*
      try {
      $qry->execute();
      $result['status'] = 'success';
      $res = $qry->fetchObject();
      if (isset($res->id))
      $result['id'] = $res->id;

      //var_dump($result) ;
      } catch (\Exception $e) {
      $result['status'] = 'error';
      $result['message'] = $pdo->errorInfo();
      $result['data'] = json_encode($tabD);

      }
      return json_encode($result);
     */
}

function history($sysma_object_id, $parameter_id, $app)
{

    $historique = array();

    // recherche des values pour le parameter et l'objet en cours

    $pdo = $app['pdo'];
    $qry = $pdo->prepare("select data_index from " . SCHEMA . ".sysma_object_data where 
	sysma_object_id = :sysma_object_id and parameter_id = :parameter_id 
        group by data_index,start_date
	order by start_date DESC");
    $qry->bindParam(':sysma_object_id', $sysma_object_id, PDO::PARAM_INT);
    $qry->bindParam(':parameter_id', $parameter_id, PDO::PARAM_INT);
    $qry->execute();


    while ($ST = $qry->fetchObject()) {

        $V = new SysmaObjectParameterValue($ST->data_index, $app);
        array_push($historique, $V);
    }

    return $historique;
}

function statusList($type, $app)
{

    $tabStatuts = array();

    $pdo = $app['pdo'];
    $qry = $pdo->prepare("select * from " . SCHEMA . ".status where object_type=:type order by \"display_order\" ASC");
    $qry->bindParam(':type', $type, PDO::PARAM_STR);
    $qry->execute();
    while ($ST = $qry->fetchObject()) {

        array_push($tabStatuts, $ST);
    }

    return $tabStatuts;
}

function statusListForForm($app, $type)
{

    $tabStatuts = array();

    $pdo = $app['pdo'];
    $qry = $pdo->prepare("select status, status_value from " . SCHEMA . ".status where object_type=:type order by \"display_order\" ASC");
    $qry->bindParam(':type', $type, PDO::PARAM_STR);
    $qry->execute();
    while ($ST = $qry->fetchObject()) {
        $tabStatuts[$ST->status_value] = $ST->status;
    }

    return $tabStatuts;
}



function maxOrganisationId($app)
{

    $qry = $app['pdo']->prepare("select (max(organisation_id)+1)::int as next_id from " . SCHEMA . ".organisation");
    $qry->execute();
    $res = $qry->fetchObject();
    return $res->next_id;
}


function organisationList($app)
{

    $taborganisation = array();
    $pdo = $app['pdo'];
    $qry = $pdo->prepare("select * from " . SCHEMA . ".organisation order by organisation ASC");
    $qry->execute();
    while ($t = $qry->fetchObject()) {
        array_push($taborganisation, $t);
    }
    return $taborganisation;
}


function organisationObjectList($app)
{

    $taborganisation = array();
    $pdo = $app['pdo'];
    $qry = $pdo->prepare("select organisation_id from " . SCHEMA . ".organisation order by organisation ASC");
    $qry->execute();
    while ($t = $qry->fetchObject()) {
        $O = new Organisation($app, $t->organisation_id);
        array_push($taborganisation, $O);
    }
    return $taborganisation;
}

function organisationListForForm($app)
{

    $taborganisation = array();
    $pdo = $app['pdo'];
    $qry = $pdo->prepare("select * from " . SCHEMA . ".organisation order by organisation ASC");
    $qry->execute();
    while ($t = $qry->fetchObject()) {
        $taborganisation[$t->organisation_id] = $t->organisation . ' (' . $t->organisation_id . ')';
    }
    return $taborganisation;
}

function tabTO($typegeo, $app)
{

    $tabTO = array();

    $pdo = $app['pdo'];
    $qry = $pdo->prepare("select * from " . SCHEMA . ".sysma_object_type where geometry_type = :typegeo order by sysma_object_type ASC");
    $qry->bindParam(':typegeo', $typegeo, PDO::PARAM_STR);
    $qry->execute();
    while ($t = $qry->fetchObject()) {
        array_push($tabTO, $t);
    }
    return $tabTO;
}

/*
 * @param array rs résultat d'une requête dans PG contenant des objets avec une géométrie
 * @return un geojson
 */

function toGeoJSON($rs)
{

    $output = '';
    $rowOutput = '';

    if (count($rs) > 0) {
        foreach ($rs as $row) {


            $rowOutput = (strlen($rowOutput) > 0 ? ',' : '') . '{"type": "Feature", "geometry": ' . $row['geojson'] . ', ';
            if (isset($row['style']) and $row['style'] != null)
                $rowOutput .= '"style":' . $row['style'] . ',';
            $rowOutput .= '"properties": {';
            $props = '';
            $id = '';
            foreach ($row as $key => $val) {
                if ($key != "geojson" and $key != "style") {
                    if (is_array($val)) {
                        $props .= (strlen($props) > 0 ? ',' : '') . '"' . $key . '":"' . escapeJsonString(implode(',', $val)) . '"';
                    } else {
                        $props .= (strlen($props) > 0 ? ',' : '') . '"' . $key . '":"' . escapeJsonString($val) . '"';
                    }
                }
                if ($key == "id") {
                    // on vire le préfixe 'o'
                    $id .= ',"id":"' . escapeJsonString($val) . '"';
                }
            }

            $rowOutput .= $props . '}';
            $rowOutput .= $id;
            $rowOutput .= '}';
            $output .= $rowOutput;
        }
        $output = '{ "type": "FeatureCollection", "features": [ ' . $output . ' ]}';
        return $output;
    }
}

function escapeJsonString($value)
{ # list from www.json.org: (\b backspace, \f formfeed)
    $escapers = array("\\", "/", "\"", "\n", "\r", "\t", "\x08", "\x0c");
    $replacements = array("\\\\", "\\/", "\\\"", "\\n", "\\r", "\\t", "\\f", "\\b");
    $result = str_replace($escapers, $replacements, $value);
    return $result;
}

function convertCoords($geostart)
{

    // coords
    // conversion
    $coords = null;
    $compt = 0;
    $geo = explode(',', $geostart);
    //var_dump($geo);
    foreach ($geo as $g) {
        if ($compt == 0) {

            $coords .= $g . ' ';
            $compt = 1;
        } elseif ($compt == 1) {

            $coords .= $g . ',';
            $compt = 0;
        }
    }
    $coords = substr($coords, 0, -1);
    return $coords;
}

function lineaireTravaux($tab)
{

    if (!verifySingleObjectType($tab)) {
        $res['erreur'] = 'Erreurs, objets de types différents';
        echo json_encode($res);
        die();
    }


    foreach ($tab as $t) {

        $T = new SysmaAction($t);

        $res['fiches-sysma_action'] .= $T->sysma_action . ' [' . $T->dateDebut . ' > ' . $T->end_date . '], ';

        $T->loadSysmaObjects();
        $TabObGeo = $T->sysmaObjectsArray;

        foreach ($TabObGeo as $Obj) {

            $TyO = new SysmaObjectType($Obj->sysma_object_type_id);
            $infos = $Obj->loadGeoInfos($TyO->geometry_type);

            $res['lineaire'] += $infos['perimetre'];
            if ($TyO->geometry_type == 'ST_Polygon')
                $res['surface'] += $infos['surface'];
        }
    }

    $res['fiches-sysma_action'] = substr($res['fiches-sysma_action'], 0, -2);
    return $res;
}

/*
 * renvoi une liste d'url pour une liste de fiche sysma_action
 * les url sont groupées par type de travaux
 */

function urlworksSheetReport2($app, $tab)
{

    $pdo = $app['pdo'];


    $i = "select sysma_object_type.sysma_object_type, sysma_object_type.sysma_object_type_id from " . SCHEMA . ".l_sysma_object_sysma_action," . SCHEMA . ".sysma_object," . SCHEMA . ".sysma_object_type where sysma_object_type.sysma_object_type_id = sysma_object.sysma_object_type_id and l_sysma_object_sysma_action.sysma_object_id = sysma_object.sysma_object_id and sysma_action_id in (" . implode(',', $tab) . ") group by sysma_object_type.sysma_object_type,sysma_object_type.sysma_object_type_id";

    $qry = $pdo->prepare($i);

    $qry->execute();

    if ($qry->rowCount() > 0) {
        while ($res = $qry->fetchObject()) {

            $list = null;
            // liste des sysma_action du bon type d'obje
            foreach ($tab as $t) {
                $T = new SysmaAction($t, $app);
                $TyT = new SysmaActionType($T->sysma_action_type_id, $app);
                if ($TyT->sysma_object_type_id == $res->sysma_object_type_id)
                    $list .= $t . ',';
            }

            $url .= '<a href="http://eptbsn.alwaysdata.net/sysma/sysma_actionSynthese.php?sysma_action=' . substr($list, 0, -1) . '" target="_blank">Fiche de synthèse des sysma_action ' . $res->sysma_object_type . '  <small>[' . implode(',', $tab) . ']</small></a>, ';
        }
        return substr($url, 0, -2);
    } else {
        return '<span style="color:#f00;">Pas de fiche sysma_action correspondante</span>';
    }
}

/*
 * renvoie une liste d'url de fiches segments
 */

function urlREHSheetReport($tab)
{

    $nbSeg = 0;
    foreach ($tab as $t) {

        $S = new SysmaObject($t);
        if ($S->sysma_object_type_id == 39) {
            $nbSeg++;
            $url .= '<a href="http://eptbsn.alwaysdata.net/sysma//segmentSynthese.php?id-segment=' . $t . '" target="_blank">Fiche de synthèse segment REH ' . $S->nom . ' <small>[' . $t . ']</small></a>, ';
        }
    }
    if ($nbSeg > 0)
        return substr($url, 0, -2);
    if ($nbSeg === 0)
        return '<span style="color:#f00;">Pas de fiche segment REH correspondante</span>';
}

function verifySingleObjectType($tab, $app)
{

    $premierPassage = true;
    $erreur = 0;

    foreach ($tab as $t) {

        $T = new SysmaAction($t, $app);
        $T->loadSysmaObjects();
        $TabObGeo = $T->sysmaObjectsArray;

        foreach ($TabObGeo as $Obj) {

            if ($premierPassage) {

                $old = $Obj->sysma_object_type_id;
                $premierPassage = false;
            }
            if ($old == $Obj->sysma_object_type_id) {
            } else {
                $erreur++;
            }
        }
    }

    if ($erreur > 0)
        return false;
    if ($erreur == 0)
        return true;
}

function dateDisplay($ladate)
{

    // complete
    if (strlen($ladate) == 10)
        return substr($ladate, 8, 2) . '-' . substr($ladate, 5, 2) . '-' . substr($ladate, 0, 4);

    // year-month
    if (strlen($ladate) == 15 or strlen($ladate) == 7)
        return substr($ladate, 5, 2) . '-' . substr($ladate, 0, 4);

    // complete with time
    if (strlen($ladate) == 19)
        return substr($ladate, 8, 2) . '-' . substr($ladate, 5, 2) . '-' . substr($ladate, 0, 4);
}

/*
 * retourne un id unique (séquence qui s'incrémente à chaque appel)
 */

function dataIndex($pdo)
{

    $qry = $pdo->prepare("select nextval('" . SCHEMA . ".increment_sysma_entity_seq'::regclass)");
    $qry->execute();
    $t = $qry->fetchObject();
    return $t->nextval;
}

function isADate($dat)
{

    /*
     * vérifie que dat est bien une date en découpant et vérifiant que chaque champ est numérique et entre les bornes min max
     */

    $tdat = null;
    $tdat = explode('-', $dat);

    $verifAnnee = false;
    $verfMois = false;
    $verifJour = false;

    // annee
    if (is_numeric($tdat[0]))
        $verifAnnee = true;
    if (is_numeric(@$tdat[1]) and @$tdat[1] >= 1 and @$tdat[1] <= 12)
        $verifMois = true;
    if (is_numeric(@$tdat[2]) and @$tdat[2] >= 1 and @$tdat[2] <= 31)
        $verifJour = true;

    if ($verifAnnee and $verifMois and $verifJour) {
        return true;
    } else {
        return false;
    }
}

function alias($texte)
{

    //var_dump($texte) ;


    if ($texte != "") {

        $texte = str_replace(array(' ', '&nbsp;', '&#039;'), '_', $texte);
        $texte = str_replace(array('é', 'è', 'ê', 'ë', '&eacute;', '&eagrve', '&ecirc;', '&euml;'), 'e', $texte);
        $texte = str_replace(array('ç', '&ccedil;'), 'c', $texte);
        $texte = str_replace(array('â', 'à', 'ä', '&eacute;', '&acirc;', '&auml;'), 'a', $texte);
        $texte = str_replace(array('î', 'ï', '&icirc;'), 'i', $texte);
        $texte = str_replace(array('ô', 'ö', '&ocirc;'), 'o', $texte);
        $texte = str_replace(array('"', '<', '>', '!;', '?', '.', '/', ',', '«', '»', '’', '&quot;', '(', ')'), '', $texte);
        $texte = str_replace(array(':', ';'), '', $texte);
        $texte = str_replace("'", "_", $texte);
        $texte = str_replace(array('-', '--', '---', '----', '-----'), '', $texte);
        $texte = strtolower($texte);
        return $texte;
    } else {
        return null;
    }
}

//http://stackoverflow.com/questions/4342531/generate-smooth-hex-gradient-table

function Gradient($HexFrom, $HexTo, $ColorSteps)
{
    $FromRGB['r'] = hexdec(substr($HexFrom, 0, 2));
    $FromRGB['g'] = hexdec(substr($HexFrom, 2, 2));
    $FromRGB['b'] = hexdec(substr($HexFrom, 4, 2));

    $ToRGB['r'] = hexdec(substr($HexTo, 0, 2));
    $ToRGB['g'] = hexdec(substr($HexTo, 2, 2));
    $ToRGB['b'] = hexdec(substr($HexTo, 4, 2));

    $StepRGB['r'] = ($FromRGB['r'] - $ToRGB['r']) / ($ColorSteps - 1);
    $StepRGB['g'] = ($FromRGB['g'] - $ToRGB['g']) / ($ColorSteps - 1);
    $StepRGB['b'] = ($FromRGB['b'] - $ToRGB['b']) / ($ColorSteps - 1);

    $GradientColors = array();

    for ($i = 0; $i <= $ColorSteps; $i++) {
        $RGB['r'] = floor($FromRGB['r'] - ($StepRGB['r'] * $i));
        $RGB['g'] = floor($FromRGB['g'] - ($StepRGB['g'] * $i));
        $RGB['b'] = floor($FromRGB['b'] - ($StepRGB['b'] * $i));

        $HexRGB['r'] = sprintf('%02x', ($RGB['r']));
        $HexRGB['g'] = sprintf('%02x', ($RGB['g']));
        $HexRGB['b'] = sprintf('%02x', ($RGB['b']));

        $GradientColors[] = implode(NULL, $HexRGB);
    }
    $GradientColors = array_filter($GradientColors, "len");
    return $GradientColors;
}

function hexToRGB($hex)
{
    list($r, $g, $b) = sscanf($hex, "#%02x%02x%02x");
    return ['r' => $r, 'g' => $g, 'b' => $b];
}

function len($val)
{
    return (strlen($val) == 6 ? true : false);
}

function uploadWebDav($filename, $filepath, $cible)
{

    // The user credentials I will use to login to the WebDav host
    $credentials = array(
        OCS_USER,
        OCS_PASS
    );
    // Prepare the file we are going to upload
    $filesize = filesize($filepath);
    $fh = fopen($filepath, 'r');

    // CURL et option
    $ch = curl_init(FILEWEBDAVPATH . '/' . $cible . $filename);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
    curl_setopt($ch, CURLOPT_USERPWD, implode(':', $credentials));
    curl_setopt($ch, CURLOPT_PUT, true);
    curl_setopt($ch, CURLOPT_INFILE, $fh);
    curl_setopt($ch, CURLOPT_INFILESIZE, $filesize);
    curl_exec($ch);
    fclose($fh);
    return true;
}

function createOCShare($filename, $filepath, $cible)
{

    $credentials = array(
        OCS_USER,
        OCS_PASS
    );
    $ch2 = curl_init('http://' . OCS_API_ROOT . '/shares');
    curl_setopt($ch2, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
    curl_setopt($ch2, CURLOPT_USERPWD, implode(':', $credentials));
    curl_setopt($ch2, CURLOPT_POST, true);
    curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch2, CURLOPT_POSTFIELDS, array('path' => $cible . $filename, 'shareType' => 3));
    $res = simplexml_load_string(curl_exec($ch2));
    return $res;
}

function deleteOCShare($id)
{
    $credentials = array(
        OCS_USER,
        OCS_PASS
    );
    $ch2 = curl_init('http://' . OCS_API_ROOT . '/shares/' . $id);
    curl_setopt($ch2, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
    curl_setopt($ch2, CURLOPT_USERPWD, implode(':', $credentials));
    curl_setopt($ch2, CURLOPT_CUSTOMREQUEST, 'DELETE');
    curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
    $res = simplexml_load_string(curl_exec($ch2));

    if ($res->meta->statuscode == 100) {
        return true;
    }
}

function upload($filename, $filepath, $cible)
{

    // upload du doc en webdav
    uploadWebDav($filename, $filepath, $cible);

    $res = createOCShare($filename, $filepath, $cible);

    // miniature du fichier si c'est un pdf
    if (extension($filename) == 'pdf') {

        creeMiniature($filepath, $res->data->id);
    }

    return $res;
}

/*
  function deleteDirectory($dir) {
  if (!file_exists($dir)) {
  return true;
  }

  if (!is_dir($dir)) {
  return unlink($dir);
  }

  foreach (scandir($dir) as $item) {
  if ($item == '.' || $item == '..') {
  continue;
  }

  if (!deleteDirectory($dir . DIRECTORY_SEPARATOR . $item)) {
  return false;
  }

  }

  return rmdir($dir);
  } */

function buildSQLFilter($object_type, $id_type, $app)
{

    $filter_sql = null;
    $tabFiltre = array();
    $pdo = $app['pdo'];

    if ($object_type === 'sysma_object') {
        $qry0 = $pdo->prepare('select * from sysma.l_filter_user l '
            . 'JOIN sysma.filter f ON f.filter_id = l.filter_id and l.sysma_object_type_id = :id_type and f.object_type = \'sysma_object\' '
            . 'where l.user_id = :id_util order by l_filter_user_id');
    } elseif ($object_type === 'sysma_action') {
        $qry0 = $pdo->prepare('select * from sysma.l_filter_user l '
            . 'JOIN sysma.filter f ON f.filter_id = l.filter_id and l.sysma_action_type_id = :id_type and f.object_type = \'sysma_action\' '
            . 'where l.user_id = :id_util order by l_filter_user_id');
    }

    $idu = $app['session']->get('user_id');
    $qry0->bindParam(':id_util', $idu, PDO::PARAM_INT);
    $qry0->bindParam(':id_type', $id_type, PDO::PARAM_INT);
    $qry0->execute();


    if ($qry0->rowCount() > 0) {
        $compt = 0;
        while ($f = $qry0->fetchObject()) {
            $compt++;
            if ($compt < $qry0->rowCount()) {
                $tabFiltre[] = $f->sql . ' ' . $f->condition;
            } else {
                $tabFiltre[] = $f->sql . ' ' . $f->condition;
                //  $tabFiltre[] = $f->sql;
            }
        }
        $filter_sql = ' and (' . implode(' ', $tabFiltre) . ')';
    }

    return $filter_sql;
}

/*
 * charge une liste de connexions
 * options = []
 *      'limit' : nb de connexion
 *      
 */

function lastModifiedSysmaObjectsList($app, $options = array())
{

    if (!isset($options['limit'])) {
        $options['limit'] = 10;
    }

    $pdo = $app['pdo'];
    $qry = $pdo->prepare('select o.sysma_object, o.sysma_object_id, tyo.sysma_object_type, u.user, o.modified_at from ' . SCHEMA . '.sysma_object o left join ' . SCHEMA . '.user u on o.modified_by = u.user_id left join ' . SCHEMA . '.sysma_object_type tyo on tyo.sysma_object_type_id = o.sysma_object_type_id where o.modified_at is not null order by o.modified_at DESC LIMIT :limit');
    $qry->bindParam(':limit', $options['limit'], PDO::PARAM_INT);
    $qry->execute();

    if ($qry->execute()) {
        return $qry->fetchAll();
    } else {
        return false;
    }
}

/*
 * charge une liste de connexions
 * options = []
 *      'limit' : nb de connexion
 *      
 */

function sql_filtersHistory($app, $options = array())
{

    if (!isset($options['limit'])) {
        $options['limit'] = 100;
    }

    $pdo = $app['pdo'];
    $qry = $pdo->prepare('select * from ' . SCHEMA . '.connection c left join ' . SCHEMA . '.user u on c.user_id = u.user_id order by c.connection_date DESC,c.connection_time DESC LIMIT :limit');
    $qry->bindParam(':limit', $options['limit'], PDO::PARAM_INT);
    if ($qry->execute()) {
        return $qry->fetchAll();
    } else {
        return false;
    }
}

/*
 * charge une liste de connexions
 * options = []
 *      'start_date' : nb de connexion
 *      
 */

function sql_filtersHistoryReport($app, $options = array())
{

    if (!isset($options['start_date'])) {
        $date = new DateTime();
        $date->modify('-10 days');
        $options['start_date'] = $date->format('Y-m-d');
    }

    //echo $options['jour_debut'];

    $pdo = $app['pdo'];
    $qry = $pdo->prepare('select u.firstname,u.lastname,u.user,count(*) as nb_connexions from ' . SCHEMA . '.connection c left join ' . SCHEMA . '.user u on c.user_id = u.user_id where c.connection_date >= :start_date GROUP BY u.user_id,u.firstname,u.lastname,u.user order by nb_connexions DESC');
    $qry->bindParam(':start_date', $options['start_date']);
    if ($qry->execute()) {
        return $qry->fetchAll();
    } else {
        // var_dump($pdo->errorInfo());
        return false;
    }
}

/*
 * table = array('table'=> schema.table, 'sysma_object_id' => champ, 'sysma_object' => champ) ;
 */

function objectsInTableList($app, $table)
{

    $tabR = array();
    $pdo = $app['pdo.export'];
    $qry = $pdo->prepare('select ' . $table['sysma_object_id'] . ' as sysma_object_id, ' . $table['sysma_object'] . ' as sysma_object from ' . $table['table'] . ' order by ' . $table['sysma_object']);
    $qry->execute();
    while ($res = $qry->fetchObject()) {

        $tabR[$table['table'] . '.' . $table['sysma_object_id'] . '.' . $table['sysma_object'] . '.' . $table['geom'] . '.' . $res->sysma_object_id] = $res->sysma_object . ' [' . $res->sysma_object_id . ']';
    }
    return $tabR;
}

/*
 * table = array('table'=> schema.table, 'sysma_object_id' => champ, 'sysma_object' => champ, 'id' => 123) ;
 */

function loadObject($app, $table)
{

    $pdo = $app['pdo.export'];
    //  echo 'select *, ' . $table['sysma_object_id'] . ' as sysma_object_id, ' . $table['sysma_object'] . ' as sysma_object, ' . $table['geom'] . ' as geom from ' . $table['table'] . ' where ' . $table['sysma_object_id'] . ' = \'' . $table['id'].'\'' ;
    $qry = $pdo->prepare('select *, ' . $table['sysma_object_id'] . ' as sysma_object_id, ' . $table['sysma_object'] . ' as sysma_object, ' . $table['geom'] . ' as geom from ' . $table['table'] . ' where ' . $table['sysma_object_id'] . ' = \'' . $table['id'] . '\'');
    $qry->execute();
    $res = $qry->fetchObject();
    return $res;
}

function loadSysmaSysmaObjectsInGeom($app, $geom, $options)
{

    $pdo = $app['pdo'];
    //echo 'select * from ' . SCHEMA . '.sysma_object where st_within(geom,\'' . $geom . '\'::geometry) and ' . $options['filter'] ;
    $qry = $pdo->prepare('select * from ' . SCHEMA . '.sysma_object where st_within(geom,\'' . $geom . '\'::geometry) and ' . $options['filter']);
    $qry->execute();
    $res = $qry->fetchAll();
    return $res;
}

/*
 * calcule une série d'indicateurs sur un ou plusieurs sous bassins
 */

function generateSubbasinReport($app, $data)
{

    $tab = array();
    $geoms = array();
    $ids = array();
    $dejaT = array();
    $tabR = array();

    // collecte des infos contenues dans data

    foreach ($data['sysma_object_id'] as $id) {
        $partsId = explode('.', $id);
        $tab[] = array('schema' => $partsId[0], 'table' => $partsId[1], 'sysma_object_id' => $partsId[2], 'sysma_object' => $partsId[3], 'geom' => $partsId[4], 'id' => $partsId[5]);
    }

    // création des éléments de la requête

    foreach ($tab as $layer) {

        if (@$dejaT[$layer['schema'] . '.' . $layer['table']] == null) {
            $geoms[] = $layer['table'] . '.' . $layer['geom'];
            $tables[] = $layer['schema'] . '.' . $layer['table'];
            $dejaT[$layer['schema'] . '.' . $layer['table']] = true;
        }
        $ids[] = $layer['table'] . '.' . $layer['sysma_object_id'] . ' = ' . $layer['id'];
    }

    $pdo = $app['pdo'];


    $req = 'select (ST_Dump(ST_union(' . implode(',', $geoms) . '))).geom::geometry(Polygon,2154) as geom_union  from ' . implode(',', $tables) . ' where ' . implode(' OR ', $ids) . ' ';
    // echo $req ; 
    $qry = $pdo->prepare($req);
    $qry->execute();

    // géometrie UNION 
    $geom = $qry->fetchObject();

    // segments

    $r1 = 'select sysma_object_id from ' . SCHEMA . '.sysma_object where ST_intersects(sysma_object.geom,\'' . $geom->geom_union . '\') and sysma_object_type_id = 39 and start_date < \'' . date('Y-m-d') . '\' and (end_date = \'0\' or end_date > \'' . date('Y-m-d') . '\')';
    // echo $r1;
    $q1 = $pdo->prepare($r1);
    $q1->execute();
    while ($seg = $q1->fetchObject()) {

        $Seg = new SysmaObject($seg->sysma_object_id, $app);
        // $Seg->loadActualInfosSysmaObject($app);
        $Seg->loadGeoInfos('ST_Linestring');
        //

        @$tabR['Nombre de segments concernés']++;
        @$tabR['Linéaire total des segments concernés'] += round($Seg->infosGeom['perimetre']);

        $tabR['Segments concernés'][] = '<a href="/carte/objet/' . $Seg->id . '" target="_blank">' . $Seg->nom . ' [' . $Seg->id . ']</a>';

        // linéaire du segment dans le sous bv

        $linDsPolygon = lengthInPolygon($app, $Seg->LoadGeom(), $geom->geom_union);


        //  echo $seg->sysma_object_id.' : '.$linDsPolygon.'<br>' ;
        @$tabR['Linéaire des segments dans la zone'] += $linDsPolygon;

        // altérations REH
        $paramArrayREH = array(203, 213, 187);
        // $paramArrayREH = array(203);

        $V = new SysmaObjectParameterValue(null);

        foreach ($paramArrayREH as $paramREH) {

            $V->buildFromParameters($Seg->id, $paramREH, date('Y-m-d'), $app);


            if (!isset($ftime[$V->parameter->parameter])) {
                $tabR[$V->parameter->parameter]['Non renseigné'] = 0;
                $tabR[$V->parameter->parameter]['Très bon'] = 0;
                $tabR[$V->parameter->parameter]['Bon'] = 0;
                $tabR[$V->parameter->parameter]['Moyen'] = 0;
                $tabR[$V->parameter->parameter]['Mauvais'] = 0;
                $tabR[$V->parameter->parameter]['Très mauvais'] = 0;
                $ftime[$V->parameter->parameter] = true;
            }

            // $tabR[$V->parameter->parameter][$V->value] += round($linDsPolygon);
            //  echo $paramREH.' - '.$V->value.' - '.$linDsPolygon.'<br>' ;

            if ($V->value == '')
                @$tabR[$V->parameter->parameter]['Non renseigné'] += round($linDsPolygon);
            if ($V->value == 'tres bon')
                @$tabR[$V->parameter->parameter]['Très bon'] += round($linDsPolygon);
            if ($V->value == 'bon')
                @$tabR[$V->parameter->parameter]['Bon'] += round($linDsPolygon);
            if ($V->value == 'moyen' or $V->value == 'moyenne')
                @$tabR[$V->parameter->parameter]['Moyen'] += round($linDsPolygon);
            if ($V->value == 'mauvais')
                @$tabR[$V->parameter->parameter]['Mauvais'] += round($linDsPolygon);
            if ($V->value == 'tres mauvais')
                @$tabR[$V->parameter->parameter]['Très mauvais'] += round($linDsPolygon);
        }

        // Autre parameters, incision du lit
        $paramArrayREH = array(193);

        $V = new SysmaObjectParameterValue(null);
        foreach ($paramArrayREH as $paramREH) {

            $V->buildFromParameters($Seg->id, $paramREH, date('Y-m-d'), $app);

            if (!empty($V->parameter->parameter) and !isset($ftime[$V->parameter->parameter])) {
                $tabR[$V->parameter->parameter]['Nulle'] = 0;
                $tabR[$V->parameter->parameter]['Faible'] = 0;
                $tabR[$V->parameter->parameter]['Moyenne'] = 0;
                $tabR[$V->parameter->parameter]['Forte'] = 0;
                $ftime[$V->parameter->parameter] = true;
            }


            // $tabR[$V->parameter->parameter][$V->value] += round($linDsPolygon);
            if ($V->value == '')
                @$tabR[$V->parameter->parameter]['Non renseigné'] += round($linDsPolygon);
            if ($V->value == 'nulle')
                @$tabR[$V->parameter->parameter]['Nulle'] += round($linDsPolygon);
            if ($V->value == 'faible')
                @$tabR[$V->parameter->parameter]['Faible'] += round($linDsPolygon);
            if ($V->value == 'moyenne')
                @$tabR[$V->parameter->parameter]['Moyenne'] += round($linDsPolygon);
            if ($V->value == 'forte')
                @$tabR[$V->parameter->parameter]['Forte'] += round($linDsPolygon);
        }
    }








    // lit artificialisés

    $r1 = 'select sum(st_length(geom)) as longueur from ' . SCHEMA . '.sysma_object where ST_Intersects(sysma_object.geom,\'' . $geom->geom_union . '\') and sysma_object_type_id = 43 and start_date < \'' . date('Y-m-d') . '\' and (end_date = \'0\' or end_date > \'' . date('Y-m-d') . '\')';
    $qry1 = $pdo->prepare($r1);
    $qry1->execute();
    $res1 = $qry1->fetchObject();
    @$tabR['Linéaire de lit artificialisé'] = $res1->longueur;


    // plans d'eau 
    // SIG
    $r2 = 'select value, count(*) as nb_pdo_connectes from ' . SCHEMA . '.sysma_object o '
        . ' inner join ' . SCHEMA . '.sysma_object_data d on d.sysma_object_id = o.sysma_object_id and d.parameter_id = 233 and  (d.start_date = \'0\' or d.start_date < \'' . date('Y-m-d') . '\') and  (d.end_date = \'0\' or d.end_date > \'' . date('Y-m-d') . '\') '
        . ' where ST_Intersects(o.geom,\'' . $geom->geom_union . '\') and o.sysma_object_type_id = 45 and o.start_date < \'' . date('Y-m-d') . '\' and (o.end_date = \'0\' or o.end_date > \'' . date('Y-m-d') . '\') group by value';

    $qry2 = $pdo->prepare($r2);
    $qry2->execute();
    while ($res2 = $qry2->fetchObject()) {
        $tabR['Nombre de plans d\'eau connectés = ' . $res2->value . ' (SIG)'] = $res2->nb_pdo_connectes;
    }

    // TERRAIN
    $r2 = 'select value, count(*) as nb_pdo_connectes from ' . SCHEMA . '.sysma_object o '
        . ' inner join ' . SCHEMA . '.sysma_object_data d on d.sysma_object_id = o.sysma_object_id and d.parameter_id = 234 and  (d.start_date = \'0\' or d.start_date < \'' . date('Y-m-d') . '\') and  (d.end_date = \'0\' or d.end_date > \'' . date('Y-m-d') . '\') '
        . ' where ST_Intersects(o.geom,\'' . $geom->geom_union . '\') and o.sysma_object_type_id = 45 and o.start_date < \'' . date('Y-m-d') . '\' and (o.end_date = \'0\' or o.end_date > \'' . date('Y-m-d') . '\') group by value';

    $qry2 = $pdo->prepare($r2);
    $qry2->execute();
    while ($res2 = $qry2->fetchObject()) {
        $tabR['Nombre de plans d\'eau ' . $res2->value . ' (Terrain)'] = $res2->nb_pdo_connectes;
    }




    // obstacles écoulements

    $r2 = 'select sysma_object_id from ' . SCHEMA . '.sysma_object o '
        . ' where ST_Intersects(o.geom,\'' . $geom->geom_union . '\') and o.sysma_object_type_id = 46 and o.start_date < \'' . date('Y-m-d') . '\' and (o.end_date = \'0\' or o.end_date > \'' . date('Y-m-d') . '\')';

    $qry2 = $pdo->prepare($r2);
    $qry2->execute();


    while ($obs = $qry2->fetchObject()) {

        $O = new SysmaObject($obs->sysma_object_id, $app);
        $O->loadActualInfosSysmaObject();


        $paramArrayREH = array(246);

        $V = new SysmaObjectParameterValue(null);
        foreach ($paramArrayREH as $paramREH) {

            $V->buildFromParameters($O->id, $paramREH, date('Y-m-d'), $app);
            @$tabR[$V->parameter->parameter][$V->value]++;
        }

        @$tabR['Nombre d\'obstacles à l\'écoulement']++;
    }






    // drains

    $r2 = 'select count(*) as compte from ' . SCHEMA . '.sysma_object o '
        . ' where ST_Intersects(o.geom,\'' . $geom->geom_union . '\') and o.sysma_object_type_id = 44 and o.start_date < \'' . date('Y-m-d') . '\' and (o.end_date = \'0\' or o.end_date > \'' . date('Y-m-d') . '\')';

    $qry2 = $pdo->prepare($r2);
    $qry2->execute();
    $res2 = $qry2->fetchObject();
    @$tabR['Nombre de sorties de drain'] = $res2->compte;



    // ab sauvages

    $r2 = 'select count(*) as compte from ' . SCHEMA . '.sysma_object o '
        . ' where ST_Intersects(o.geom,\'' . $geom->geom_union . '\') and o.sysma_object_type_id = 41 and o.start_date < \'' . date('Y-m-d') . '\' and (o.end_date = \'0\' or o.end_date > \'' . date('Y-m-d') . '\')';

    $qry2 = $pdo->prepare($r2);
    $qry2->execute();
    $res2 = $qry2->fetchObject();
    @$tabR['Nombre d\'abreuvoir sauvages'] = $res2->compte;


    // ab amenagées

    $r2 = 'select status,count(*) as compte from ' . SCHEMA . '.sysma_object o '
        . ' where ST_Intersects(o.geom,\'' . $geom->geom_union . '\') and o.sysma_object_type_id = 1 and o.start_date < \'' . date('Y-m-d') . '\' and (o.end_date = \'0\' or o.end_date > \'' . date('Y-m-d') . '\') group by status';

    $qry2 = $pdo->prepare($r2);
    $qry2->execute();
    while ($res2 = $qry2->fetchObject()) {
        @$tabR['Nombre d\'abreuvoirs aménagés STATUT ' . strtoupper($res2->status)] = $res2->compte;
    }


    return $tabR;
}

function lengthInPolygon($app, $ligne, $polygon)
{

    $pdo = $app['pdo'];
    $req = 'select ST_length(ST_Union(ST_Intersection(\'' . $ligne . '\',\'' . $polygon . '\'))) as lin';
    //echo $req ;
    $qry = $pdo->prepare($req);
    $qry->execute();
    $res = $qry->fetchObject();
    return round($res->lin);
}

function sysmaObjectsForUserList($app, $sysma_object_type_id, $search = null)
{


    $qry = '
        select 
        \'o\'||o.sysma_object_id as id, 
        o.sysma_object as name   
        from sysma.sysma_object as o       
        INNER JOIN sysma.user_privilege as d
        ON 
        (o.sysma_object_type_id = ANY(d.sysma_object_type_id_list) or d.sysma_object_type_id_list = \'{0}\') and 
        d.user_id = :id_util and
        (d.sysma_object_status_list = \'{tous}\' or o.status = ANY(d.sysma_object_status_list)) and 
        (d.sysma_object_organisation_id_list = \'{0}\' or o.organisation_id = ANY(d.sysma_object_organisation_id_list)) and 
        (\'lecteur\' = ANY(d.privilege_list) or (\'contributeur\' = ANY(d.privilege_list) and o.modified_by = d.user_id))               
        where  o.sysma_object_type_id = :sysma_object_type_id';

    if (isset($search) and $search !== '') {
        $qry .= ' and o.sysma_object ilike \'%' . $search . '%\' ';
    }

    $qry .= ' order by o.sysma_object';

    $sttmt = $app['pdo']->prepare($qry);



    $idu = $app['session']->get('user_id');
    $sttmt->bindParam(':id_util', $idu, PDO::PARAM_INT);
    $sttmt->bindParam(':sysma_object_type_id', $sysma_object_type_id, PDO::PARAM_INT);

    $sttmt->execute();
    return $sttmt->fetchAll(PDO::FETCH_ASSOC);
}

function getGeomTypeFromCoords($coords)
{


    $points = explode(',', $coords);
    if (count($points) == 1) {
        return 'ST_Point';
    } elseif ($points[0] === $points[count($points) - 1]) {
        return 'ST_Polygon';
    } else {
        return 'ST_Linestring';
    }
    return false;
}

function sysmaObjectsListInRelation($app, $sysma_relation_id, $parent_child)
{



    $qry = 'select string_agg(o.sysma_object||\' [\'||o.sysma_object_id||\']\',\',\') as sysma_objects_list from ' . SCHEMA . '.l_sysma_object_sysma_relation l
        inner join ' . SCHEMA . '.sysma_object o on o.sysma_object_id = l.sysma_object_id 
            where l.sysma_relation_id = :id and l.parent_child = :pc 
            group by sysma_relation_id';


    $sttmt = $app['pdo']->prepare($qry);

    $sttmt->bindParam(':id', $sysma_relation_id, PDO::PARAM_INT);
    $sttmt->bindParam(':pc', $parent_child, PDO::PARAM_STR);
    $sttmt->execute();
    $res = $sttmt->fetchObject();
    if (is_object($res))
        return $res->sysma_objects_list;
}

/*
 * child parent inversion
 */

function invertPC($pc)
{
    if ($pc == 'parent') {
        return 'child';
    } else {
        return 'parent';
    }
}

function rrmdir($dir)
{
    if (is_dir($dir)) {
        $objects = scandir($dir);
        foreach ($objects as $object) {
            if ($object != "." && $object != "..") {
                if (is_dir($dir . DIRECTORY_SEPARATOR . $object) && !is_link($dir . "/" . $object))
                    rrmdir($dir . DIRECTORY_SEPARATOR . $object);
                else
                    unlink($dir . DIRECTORY_SEPARATOR . $object);
            }
        }
        rmdir($dir);
    }
}

function loadBaseLayers($app, $is_connected = false)
{

    $sql = null;
    if ($is_connected === false) {
        $sql = ' AND access = \'public\' ';
    }
    return otherLayersList($app['pdo'], ['sql' => ' l.group = \'baseLayers\' ' . $sql]);
}

function loadOtherBaseLayers($app, $is_connected = false)
{

    $sql = null;
    if ($is_connected === false) {
        $sql = ' AND access = \'public\' ';
    }
    return otherLayersList($app['pdo'], ['sql' => ' l.group = \'otherBaseLayers\' ' . $sql]);
}

function loadOtherLayer($app, $pdo, $params, $layer_type, $layer_id)
{

    isset($params['zoom']) ? $params['zoom'] = intval($params['zoom']) : null;


    $qry0 = $pdo->prepare('select * from ' . SCHEMA . '.other_layer where other_layer_id = :other_layer_id and layer_type = :layer_type ');
    $qry0->bindParam(':other_layer_id', $layer_id, PDO::PARAM_STR);
    $qry0->bindParam(':layer_type', $layer_type, PDO::PARAM_STR);
    $qry0->execute();
    $layer = $qry0->fetchObject();
    $corr = json_decode($layer->mapping);




    if ($layer->access === 'public' or ($layer->access === 'restricted' and $app['session']->get('user_id') != ANONYMID)) {

        if ($layer_type == 'external_geojson_layer') {
            // In this case, we directly curl the geojson from the url located in the 'geolayer_id' field

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => $layer->geolayer_id,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_FOLLOWLOCATION=> true, // Suivre les redirections
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                //   CURLOPT_HTTPHEADER => array(
                //     "cache-control: no-cache"
                //   ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            return json_decode($response, true);
        } else {

            if (
                isset($params['center']) and isset($params['zoom']) and $params['center'] != 'undefined' and $params['zoom'] != 'undefined' and $params['center'] != 'null' and $params['zoom'] != 'null'
                and $layer->load_on_pan == 1
            ) {




                if (
                    $params['zoom'] < $layer->zoom_min
                    or $params['zoom'] > $layer->zoom_max
                ) {
                    return '{"type":"FeatureCollection","features":[]}';
                } else {

                    $center = explode(',', substr($params['center'], 7, -1));
                    $center[0] = trim($center[0]);
                    $center[1] = trim($center[1]);

                    $qry = '
                with
                area_download_geom as (
                select ST_buffer(
                ST_Transform(
                ST_SetSRID(ST_MakePoint(' . $center[1] . ', ' . $center[0] . '), 4326), 2154), ' . $GLOBALS['ZOOM_CONVERSION_ARRAY1'][$params['zoom']] . ') as geom
                )
                
                select concat(\'a' . $layer->other_layer_id . '_\',other.' . $corr->sysma_object_id . ') as id,
                other.' . $corr->sysma_object . ' as name, 
                \'' . $layer->geolayer_id . '\' as objecttype, 
                \'' . $layer_type . '\' as type, 
                \'' . $layer->style . '\' as style,
                \'' . $layer->geometry_type . '\' as type_geom, ';

                    /*
                    case
                    when GeometryType(other.' . $corr->geo . ')in (\'POLYGON\',\'MULTIPOLYGON\') then ST_AsGeoJSON(ST_ExteriorRing((ST_Dump(ST_Transform(ST_simplify(other.' . $corr->geo . ', ' . $GLOBALS['ZOOM_CONVERSION_ARRAY2'][$params['zoom']] . '),4326))).geom))
                    else ST_AsGeoJSON((ST_Dump(ST_Transform(ST_simplify(other.' . $corr->geo . ', ' . $GLOBALS['ZOOM_CONVERSION_ARRAY2'][$params['zoom']] . '),4326))).geom) end
                    */
                    if ($layer->geometry_type === 'ST_Polygon_to_Linestring') {
                        $qry .= ' ST_AsGeoJSON(ST_ExteriorRing((ST_Dump(ST_Transform(ST_simplify(ST_Force2D(other.' . $corr->geo . '),' . $GLOBALS['ZOOM_CONVERSION_ARRAY2'][$params['zoom']] . '),4326))).geom), maxdecimaldigits:=(' . ($params['zoom'] + 2.) / 4.0 . ')::int) ';
                    } else {
                        $qry .= ' ST_AsGeoJSON((ST_Dump(ST_Transform(ST_simplify(ST_Force2D(other.' . $corr->geo . '),' . $GLOBALS['ZOOM_CONVERSION_ARRAY2'][$params['zoom']] . '),4326))).geom,maxdecimaldigits:=(' . ($params['zoom'] + 2.) / 4.0 . ')::int)';
                    }
                    $qry .= '
                as geojson                 
                from ' . htmlentities(str_replace('___', '.', $layer->geolayer_id), ENT_QUOTES) . ' other
                LEFT JOIN area_download_geom as a on true                
                where other.' . $corr->geo . ' && a.geom ';
                }
            } else {

                $qry = '
                select concat(\'a' . $layer->other_layer_id . '_\',other.' . $corr->sysma_object_id . ') as id,
                other.' . $corr->sysma_object . ' as name,          
                \'' . $layer->geolayer_id . '\' as objecttype, 
                \'' . $layer_type . '\' as type, 
                \'' . $layer->style . '\' as style,
                \'' . $layer->geometry_type . '\' as type_geom, ';
                if (!empty($corr->description)) {
                    $qry .= ' other.' . $corr->description . ' as description,';
                }
                if ($layer->geometry_type === 'ST_Polygon_to_Linestring') {
                    $qry .= ' ST_AsGeoJSON(ST_ExteriorRing((ST_Dump(ST_Transform(ST_Force2D(other.' . $corr->geo . '),4326))).geom), maxdecimaldigits:=5) ';
                } else {
                    $qry .= ' ST_AsGeoJSON((ST_Dump(ST_Transform(ST_Force2D(other.' . $corr->geo . '),4326))).geom, maxdecimaldigits:=5)';
                }

                $qry .= ' as geojson from ' . htmlentities(str_replace('___', '.', $layer->geolayer_id), ENT_QUOTES) . ' other';
                $qry .= ' where 1=1 ';
            }

            $qry .= ' ' . str_replace('where', 'and', $layer->sql_filter);

            $qry .= ' order by st_area(other.' . $corr->geo . ') desc';



            $sttmt = $pdo->prepare($qry);
            $sttmt->execute();

            if ($sttmt->rowCount() == 0)
                return '{"type":"FeatureCollection","features":[]}';

            $params_res = $sttmt->fetchAll(PDO::FETCH_ASSOC);
            return json_decode(toGeoJSON($params_res));
        }
    } else {

        return $app->abort(403);
    }
}

//////////////////////////////////////////////////////////////////////////////////////
// base 62 tool funcs AND thesaurus item id generators func

//////////////////////////////////////////////////////////////////////////////////////
// private-ish helper funcs

/*************
 * Convert source (an array of integers) from sourceBase to targetBase
 * return an array of intergers compliant with the targetBase
 * 
 * source: http://codegolf.stackexchange.com/a/21672
 */

function baseConvert(array $source, int $sourceBase, int $targetBase)
{
    $result = [];
    while ($count = count($source)) {
        $quotient = [];
        $remainder = 0;
        for ($i = 0; $i !== $count; $i++) {
            $accumulator = $source[$i] + $remainder * $sourceBase;
            /* Same as PHP 7 intdiv($accumulator, $targetBase) */
            $digit = ($accumulator - ($accumulator % $targetBase)) / $targetBase;
            $remainder = $accumulator % $targetBase;
            if (count($quotient) || $digit) {
                $quotient[] = $digit;
            }
        }
        array_unshift($result, $remainder);
        $source = $quotient;
    }
    return $result;
}

const BASE_62_ALPHA_NUMERICAL_CHAR_LIST = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

/**
 * Undocumented function
 *
 * @param integer $_val
 * @return void
 */
function get_crc_in_base62_encoding(string $_data)
{
    $hash = hexdec(hash('crc32b', $_data)); // in decimal format
    $hash_b62_array = baseConvert([$hash], 256, 62);
    $result = '';
    foreach ($hash_b62_array as $sym) {
        $result .= substr(BASE_62_ALPHA_NUMERICAL_CHAR_LIST, $sym, 1);
    }
    return $result;
}

/**
 * generate short cryptographic compliant pseudo-random alpha-numerical (base 62) id string
 *
 * @param integer $_len
 * @return void
 */
function generate_base62_random_string(int $_len)
{
    $anrstr = "";
    for ($i = 0; $i < $_len; $i++) {
        $anrstr .= substr(BASE_62_ALPHA_NUMERICAL_CHAR_LIST, random_int(0, 61), 1);
    }
    return $anrstr;
}


function loadSysmaObjectTypeActionTypeAndParameters($app)
{

    $tab = [];
    $OTs = objectTypesList($app);
    foreach ($OTs as $key => $OT) {
        $SOT = new SysmaObjectType($OT['sysma_object_type_id'], $app);
        $SOT->loadParameters();

        foreach ($SOT->paramArray as $P) {
            $tab[$SOT->sysma_object_type]['sysma_object**' . $SOT->sysma_object_type_id . '**' . $P->parameter_id] =
                'Paramètre ' . $P->parameter;
        }

        $SOT->loadSysmaActionTypes();
        foreach ((array) $SOT->sysmaActionTypesArray as $SAT) {
            $SAT->loadParameters();
            foreach ($SAT->paramArray as $P) {
                $tab[$SOT->sysma_object_type][$SOT->sysma_object_type . ' / Action ' . $SAT->sysma_action_type]['sysma_action**' . $SAT->sysma_action_type_id . '**' . $P->parameter_id] =
                    'Paramètre ' . $P->parameter;
            }
        }
    }

    return $tab;
}


function loadUsersAndView($app, $view)
{


    $q = 'select u.user_id, u.user, uv.view from ' . SCHEMA . '.user u 
        left join ' . SCHEMA . '.user_views uv on u.user_id = uv.user_id';

    $qry = $app['pdo']->prepare($q);
    $qry->execute();
    while ($res = $qry->fetchObject()) {

        if ($res->view === $view) {
            $tabUsersWithTheView[] = $res->user_id;
        }

        $tabUser[$res->user_id] = $res->user;
    }

    return ['tabUsersWithTheView' => $tabUsersWithTheView, 'tabUsers' => $tabUser];
}



function sysmaExportLayerExists($sysma_object_type_alias, $app)
{

    $pdo = $app['pdo.export'];
    $q = "select exists ( SELECT FROM information_schema.tables WHERE table_schema LIKE '" . MOULSCHEMA . "' AND table_type LIKE 'BASE TABLE' AND  table_name = :table ) as table_exists";
    $qry = $pdo->prepare($q);
    $qry->bindParam(':table', $sysma_object_type_alias);
    $qry->execute();
    $res = $qry->fetchObject();
    return $res->table_exists;
}


/*
load the icon lists available at SVG_URLS
*/

function loadIcons($path, $search = null)
{
    $files = scanAllDir($path, $search);

    return $files;
}


function scanAllDir($dir, $search = null)
{
    $result = [];
    foreach (scandir($dir) as $filename) {
        if ($filename[0] === '.') continue;
        $filePath = $dir . '/' . $filename;
        if (is_dir($filePath)) {
            foreach (scanAllDir($filePath) as $childFilename) {
                if ($search !== null and $search !== '' and $search !== '*') {
                    if (strpos($childFilename, $search)) {
                        $result[] = $filename . '/' . $childFilename;
                    }
                } else {
                    $result[] = $filename . '/' . $childFilename;
                }
            }
        } else {
            $result[] = $filename;
        }
    }
    return $result;
}


// https://stackoverflow.com/questions/7489742/php-read-exif-data-and-adjust-orientation adapté

function image_fix_orientation($filename)
{

    $exif = exif_read_data($filename);


    if (!empty($exif['Orientation'])) {

        $image = imagecreatefromjpeg($filename);

        switch ($exif['Orientation']) {
            case 3:
                $image = imagerotate($image, 180, 0);
                break;

            case 6:
                $image = imagerotate($image, -90, 0);
                break;

            case 8:
                $image = imagerotate($image, 90, 0);
                break;
        }

        return imagejpeg($image, $filename);
    }
}



function dataTypeChoices($app)
{

    $tab = array();
    $q = "select * from " . SCHEMA . ".data_type_choice order by description asc";
    $qry = $app['pdo']->prepare($q);
    $qry->execute();
    while ($res = $qry->fetchObject()) {
        $tab[$res->data_type] = ucfirst($res->description);
    }
    return $tab;
}



function loadHistory($app, $depth = 100, $user_id = null)
{

    $tab = [];
    $cond = null;
    if ($user_id != null) {
        $cond = " and h.modified_by = :user_id ";
    }
    $q = "select h.*, u.user from " . SCHEMA . ".x99_t_track_historytables h
    left join " . SCHEMA . ".user u on u.user_id = h.modified_by
    where table_name not in ('user') $cond order by x99_t_track_historytables_id desc limit :depth";

    $qry = $app['pdo']->prepare($q);
    $qry->bindParam(':depth', $depth, PDO::PARAM_INT);
    if ($user_id != null) {
        $qry->bindParam(':user_id', $user_id, PDO::PARAM_INT);
    }
    $qry->execute();
    while ($res = $qry->fetchObject()) {

        $res->new_row_decode = json_decode($res->new_row, true);
        $res->old_row_decode = json_decode($res->old_row, true);

        $tab[] = $res;
    }

    return $tab;
}


function getSecteurGeom($app, $secteur)
{

    $tabOG = explode('.', $secteur);

    $q = 'select ST_AsText(g.' . $tabOG[4] . ') as geom from ' . $tabOG[0] . '.' . $tabOG[1] . ' g where g.' . $tabOG[2] . ' = :id';
    // echo $q ;
    $qry =  $app['pdo']->prepare($q);
    $qry->bindParam(':id', $tabOG[5]);
    $qry->execute();
    $res = $qry->fetchObject();
    return $res->geom;
}


// check if is called from iframe
// not used yet

function isCalledFromDifferentReferer($app, $request = null) {


    if ($app['session']->get('referer')!==null) {
        $referer = $app['session']->get('referer') ;
    } elseif ($request !== null) {
        $referer = $request->headers->get('referer') ;
    } else {
        $referer = null ;
    }


    if ($referer == null) {
        return false ;
    }
    elseif ($referer !== null and $referer == WEBURL) {
        return false ;
    } else {
        return true ;
    }

}