
<?php

// namespace App\ToolKit;


/******************************************************************************
 * Tool kit for processing geometries
 * 
 *****************************************************************************/ 
final class TkGeo
{

    /***************************************
     * convertGeomFromGeojsonToPostGis
     * 
     * convert geojson default lat,long format into the postGis geom format of Sysma DB
     * Caution: this fcuntion do not compute area and lentgh yet
     * 
     */
    static public function convertGeomFromGeojsonToPostGis($_geometry, $_app) {
        // Debug::log('------------ func convertGeomFromGeojsonToPostGis');
        $pdo = $_app['pdo'];

        $coords = $_geometry->coordinates;

        $coords = "";
        $coordsArray = [];
        switch ($_geometry->type) {
            case 'Point' :      //ST_Point
                // $sql = 'select ST_Transform(ST_SetSRID(ST_GeomFromText(\'POINT(' . implode(" ", $_geometry->coordinates) . ')\'),4326),2154)::geometry(point,2154) as geom';
                $sql = "select ST_Transform(ST_SetSRID(ST_GeomFromGeoJSON('" . json_encode($_geometry) . "'),4326),2154)::geometry(" . $_geometry->type . ",2154) as geom";
                break;
            case 'Polygon' :    //ST_Polygon
                // foreach($_geometry->coordinates as $c){
                //     array_push($coordsArray, implode(' ', $c));
                // }
                // $sql = 'select ST_Transform(ST_SetSRID(ST_MakePolygon(ST_MakeValid(ST_GeomFromText(\'LINESTRING(' . implode(",",$coordsArray) . ')\'))),4326),2154)::geometry(polygon,2154) as geom, ST_area(ST_Transform(ST_SetSRID(ST_MakePolygon(ST_MakeValid(ST_GeomFromText(\'LINESTRING(' . convertCoords($geom) . ')\'))),4326),2154)) as surface';
                // $sql = "select ST_Transform(ST_SetSRID(ST_GeomFromGeoJSON('".json_encode($_geometry)."'),4326),2154)::geometry(polygon,2154) as geom";
                $sql = "select ST_Transform(ST_SetSRID(ST_GeomFromGeoJSON('" . json_encode($_geometry) . "'),4326),2154)::geometry(" . $_geometry->type . ",2154) as geom";

                break;
            case 'LineString' :    //ST_Linestring
                // foreach($_geometry->coordinates as $c){
                //     array_push($coordsArray, implode(' ', $c));
                // }
                // $sql = 'select ST_Transform(ST_SetSRID(ST_MakeValid(ST_GeomFromText(\'LINESTRING(' . implode(",",$coordsArray) . ')\')),4326),2154)::geometry(linestring,2154) as geom, ST_Length(ST_Transform(ST_SetSRID(ST_MakeValid(ST_GeomFromText(\'LINESTRING(' . convertCoords($geom) . ')\')),4326),2154)::geometry(linestring,2154)) as lineaire';
                // $sql = "select ST_Transform(ST_SetSRID(ST_GeomFromGeoJSON('" . json_encode($_geometry) . "'),4326),2154)::geometry(linestring,2154) as geom";
                $sql = "select ST_Transform(ST_SetSRID(ST_GeomFromGeoJSON('" . json_encode($_geometry) . "'),4326),2154)::geometry(" . $_geometry->type . ",2154) as geom";
                break;
            default :
                $sql = null;
                break;
        }
        // Debug::log("sql: ".$sql);
        $qry = $pdo->prepare($sql);
        $qry->execute();
        return $qry->fetchObject()->geom;
    }
}