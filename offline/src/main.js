import { createApp } from 'vue';
import './style.css';
import OfflineRightContent from './OfflineRightContent.vue';
import OnlineOfflineButtonManager from './components/OnlineOfflineButtonManager.vue';

createApp(OnlineOfflineButtonManager).mount('#li-online-offline-vue-content');
createApp(OfflineRightContent).mount('#offline-right-vue-content');
