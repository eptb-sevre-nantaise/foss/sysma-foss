<?php

class NotFoundException extends Exception
{
}


// SILEX
$app = new Silex\Application();
$app['debug'] = DEBUG;


// TWIG
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__ . '/../views',
));


$app['twig'] = $app->share($app->extend('twig', function ($twig) use ($app) {
    $twig->addFilter(new Twig_SimpleFilter('hexToRGB', function ($hex) use ($app) {
        return hexToRGB($hex);
    }));
    return $twig;
}));
$app['twig'] = $app->share($app->extend('twig', function ($twig) use ($app) {
    $twig->addFilter(new Twig_SimpleFilter('hasAccess', function ($array) use ($app) {
        return hasAccess($app, $array[0], $array[1], $array[2], $array[3], $array[4], $array[5], $array[6], $array[7]);
    }));
    return $twig;
}));
$app['twig'] = $app->share($app->extend('twig', function ($twig) use ($app) {
    $twig->addFilter(new Twig_SimpleFilter('hasAccessSysmaActions', function ($array) use ($app) {
        return hasAccessSysmaActions($app, $array[0], $array[1], $array[2], $array[3]);
    }));
    return $twig;
}));
$app['twig'] = $app->share($app->extend('twig', function ($twig) use ($app) {
    $twig->addFilter(new Twig_SimpleFilter('hasCadastreAccess', function ($user_id) use ($app) {
        return hasCadastreAccess($app, $user_id);
    }));
    return $twig;
}));
$app['twig'] = $app->share($app->extend('twig', function ($twig) use ($app) {
    $twig->addFilter(new Twig_SimpleFilter('hasRpgAccess', function ($user_id) use ($app) {
        return hasRpgAccess($app, $user_id);
    }));
    return $twig;
}));
$app['twig'] = $app->share($app->extend('twig', function ($twig) use ($app) {
    $twig->addFilter(new Twig_SimpleFilter('invertPC', function ($pc) use ($app) {
        return invertPC($pc);
    }));
    return $twig;
}));

// pour render dans les vues twig
$app->register(new Silex\Provider\HttpFragmentServiceProvider());

// TRANSLATION
$app->register(new Silex\Provider\TranslationServiceProvider(), array(
    'locale_fallbacks' => array('en'),
));

// VALIDATOR
use Symfony\Component\Validator\Constraints as Assert;

$app->register(new Silex\Provider\ValidatorServiceProvider());

// FORM
use Silex\Provider\FormServiceProvider;

$app->register(new FormServiceProvider());

use Symfony\Component\Form\FormEvents as FormEvents;

// PG
// $app['pdo'] = new PDO('pgsql:dbname=' . DBNAME . ';host=' . DBHOST . '', DBUSER, DBPWD, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING, PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC));
$app['pdo'] = new SysmaPDO('pgsql:dbname=' . DBNAME . ';host=' . DBHOST . ';port=' . DBPORT, DBUSER, DBPWD, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING, PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC));
if (PHOTO_PWG_MODE == 'on') {
    $app['pdo.pwg'] = new PDO('mysql:dbname=' . DBPHNAME . ';host=' . DBPHHOST . ';port=' . DBPHPORT, DBPHUSER, DBPHPWD, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING, PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC));
}
$app['pdo.cad'] = new PDO('pgsql:dbname=' . CADDBNAME . ';host=' . CADDBHOST . ';port=' . CADDBPORT, CADDBUSER, CADDBPWD, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING, PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC));
$app['pdo.export'] = new PDO('pgsql:dbname=' . MOULDBNAME . ';host=' . MOULDBHOST . ';port=' . MOULDBPORT, MOULDBUSER, MOULDBPWD, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING, PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC));


// SESSION & COOKIES
$app->register(new Silex\Provider\SessionServiceProvider());
// session par defaut sur 8 heures
//session_set_cookie_params(3600 * 8, "/");

$app['session.storage.options'] = [
    'cookie_lifetime' => SESSIONTIME,
    'lifetime' => SESSIONTIME
];

use Symfony\Component\HttpFoundation\Cookie;

// URL MANAGER
$app->register(new Silex\Provider\UrlGeneratorServiceProvider());


$app['twig']->addGlobal('PROTOCOL', PROTOCOL);
$app['twig']->addGlobal('WEBURL', WEBURL);
$app['twig']->addGlobal('ORGANISATION', ORGANISATION);
$app['twig']->addGlobal('ORGANISATION_ACRONYM', ORGANISATION_ACRONYM);
$app['twig']->addGlobal('ORGANISATION_WEBSITE', ORGANISATION_WEBSITE);
$app['twig']->addGlobal('WELCOME_MESSAGE', WELCOME_MESSAGE);
$app['twig']->addGlobal('IGN_API_KEY', IGN_API_KEY);
$app['twig']->addGlobal('CADASTRE_MODE', CADASTRE_MODE);
@$app['twig']->addGlobal('RPG_MODE', RPG_MODE) ;
 

$app['twig']->addGlobal('PHOTO_PWG_MODE', PHOTO_PWG_MODE);
$app['twig']->addGlobal('PHOTO_UPLOAD_MODE', PHOTO_UPLOAD_MODE);
$app['twig']->addGlobal('ANONYMID', ANONYMID);
$app['twig']->addGlobal('UNIT_MEASURES', $GLOBALS['UNIT_MEASURES']);
$app['twig']->addGlobal('COST_MANAGEMENT', COST_MANAGEMENT);
$app['twig']->addGlobal('TVA', TVA);
$app['twig']->addGlobal('PROCESSES', $GLOBALS['PROCESSES']);
$app['twig']->addGlobal('ADDONS', $GLOBALS['ADDONS']);
$app['twig']->addGlobal('HELPPAGE_URL', HELPPAGE_URL);
$app['twig']->addGlobal('HT_TTC', HT_TTC);
$app['twig']->addGlobal('SYSMA_OFFLINE_MODE_ENABLED', SYSMA_OFFLINE_MODE_ENABLED);
$app['twig']->addGlobal('SYSMA_VERSION_NUMBER', SYSMA_VERSION_NUMBER);
$app['twig']->addGlobal('SYSMA_VERSION_DATE', SYSMA_VERSION_DATE);
if (defined(WEBSITE_ANALYTICS_CODE)){
$app['twig']->addGlobal('WEBSITE_ANALYTICS_CODE', WEBSITE_ANALYTICS_CODE);
}
if (defined('DEBUG_DEV_MAIL')) {
    $app['twig']->addGlobal('DEBUG_DEV_MAIL', DEBUG_DEV_MAIL);
}



use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

$app->after(function (Request $request, Response $response) {        
    if (defined('FRAME_ANCESTORS')) {
        $response->headers->set('Content-Security-Policy', 'frame-ancestors '.FRAME_ANCESTORS.'' );        
    } else {
        $response->headers->set('Content-Security-Policy', 'frame-ancestors \'none\'' );        
    }
});
