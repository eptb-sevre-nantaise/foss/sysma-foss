<?php

/**
 * Description of SysmaActionType 
 * 
 * @author srenou
 * 
 */
class SysmaActionType
{

    public function __construct($id, $app)
    {

        $this->app = $app;
        $this->pdo = $app['pdo'];
        if (is_numeric($id) and $id != null) {

            $qry = $this->pdo->prepare("select * from " . SCHEMA . ".sysma_action_type where sysma_action_type_id = :id");
            $qry->bindParam(':id', $id, PDO::PARAM_INT);
            $qry->execute();
            $t = $qry->fetchObject();

            if (empty($t)) { // object does not exists                 
                throw new NotFoundException('Sysma Action Type ' . $id . ' doesn\'t exist');
            }

            foreach ($t as $key => $value) {
                $this->$key = $value;
            }
            $this->description = html_entity_decode($t->description, ENT_QUOTES);

            $this->creation_date = dateDisplay($t->created_at);
            $this->modification_date = dateDisplay($t->modified_at);

            $this->style_decode = json_decode($this->style);
        }
    }

    public function loadParameters()
    {


        $this->paramArrayValue = $this->paramArray = array();
        $i = 0;

        $qry = $this->pdo->prepare("select * from " . SCHEMA . ".sysma_action_type_parameter where sysma_action_type_id = :id order by \"ordered_by\" ASC, \"parameter_id\" ASC");
        $qry->bindParam(':id', $this->sysma_action_type_id, PDO::PARAM_INT);
        $qry->execute();
        while ($p = $qry->fetchObject()) {

            $this->paramArrayValue[$i] = $p->parameter_id;
            $this->paramArray[$i] = new SysmaActionParameter($p->parameter_id, $this->app); // objets
            $i++;
        }
    }

    // TODO refactor? implementation similar in utils.php and in SysamAction.php?
    public function countSysmaActions($satId = null)
    {
        if ($satId == null) {
            $satId = $this->sysma_action_type_id;
        }
        $qry = $this->pdo->prepare("select count(sysma_action_id) as count from " . SCHEMA . ".sysma_action where sysma_action_type_id = :id");
        $qry->bindParam(':id', $satId, PDO::PARAM_INT);
        $qry->execute();
        $res = $qry->fetchObject();
        return $res->count;
    }

    public function getLinkedSysmaObjectType($satId = null)
    {
        if ($satId == null) {
            $satId = $this->sysma_action_type_id;
        }
        $qry = $this->pdo->prepare("select sysma_object_type_id as id from " . SCHEMA . ".sysma_action_type where sysma_action_type_id = :id");
        $qry->bindParam(':id', $satId, PDO::PARAM_INT);
        $qry->execute();
        $res = $qry->fetchObject();
        return $res->id;
    }


    public function getLinkedSysmaObjectTypeInfos($return_value = 'geometry_type')
    {
        $qry = $this->pdo->prepare("select $return_value as value from " . SCHEMA . ".sysma_object_type where sysma_object_type_id = :id");
        $qry->bindParam(':id', $this->sysma_object_type_id, PDO::PARAM_INT);
        $qry->execute();
        $res = $qry->fetchObject();
        return $res->value;
    }

    public function update($request)
    {

        $pp = prepUpdateQuery($request, 'sysma_action_type_id');

        $qry = $this->pdo->prepare("update " . SCHEMA . ".sysma_action_type set " . $pp['req'] . " where sysma_action_type_id = :sysma_action_type_id");

        foreach ($pp['tabD'] as $key => $val) {
            $qry->bindValue($key, $val);
        }

        return returnResultQuery($qry, $this->pdo, $pp['tabD']);
    }

    public function create($request)
    {
        $pp = prepInsertQuery($request);

        $qry = $this->pdo->prepare("insert into " . SCHEMA . ".sysma_action_type (" . $pp['reqFields'] . ") VALUES (" . $pp['reqParams'] . ") RETURNING sysma_action_type_id as id");
        // echo "insert into " . SCHEMA . ".sysma_action_type (" . $pp['reqFields'] . ") VALUES (" . $pp['reqParams'] . ") RETURNING sysma_action_type_id as id";

        foreach ($pp['tabD'] as $key => $val) {
            //echo $key . ' : ' . $val . '<br>';
            $qry->bindValue($key, $val);
        }

        return returnResultQuery($qry, $this->pdo, $pp['tabD']);
    }

    public function loadSysmaActions()
    {
        $qry = $this->pdo->prepare("select * from " . SCHEMA . ".sysma_action where sysma_action_type_id = :id");
        $qry->bindParam(':id', $this->sysma_action_type_id, PDO::PARAM_INT);
        $qry->execute();
        return $qry->fetchAll(PDO::FETCH_ASSOC);
    }


    public function delete()
    {

        Debug::startPgLog();

        if (!empty($this->sysma_action_type_id) and is_numeric($this->sysma_action_type_id)) {
            try {
                // BEGIN
                $this->pdo->beginNestedTransaction();

                // 2nd delete all this action type ref in l_sysma_action_type_user
                $qry = $this->pdo->prepare('DELETE FROM ' . SCHEMA . '.l_sysma_action_type_user where sysma_action_type_id = ' . $this->sysma_action_type_id);
                $res = returnResultQuery($qry, $this->pdo,  "Cannot delete l_sysma_action_type_user");

                // delete every actions of this type
                $sa_array = $this->loadSysmaActions();
                foreach ($sa_array as $sa_item) {
                    $sa = new SysmaAction($sa_item['sysma_action_id'], $this->app);
                    $res = $sa->delete();
                }


                $res = null;
                $this->loadParameters();
                // delete linked parameters
                foreach ($this->paramArray as $satp) {
                    // echo '<br>************************* right here ***************<br>';
                    $satp->delete();
                }

                // 2nd delete filters on action-type
                SysmaFilter::deleteFiltersLinkedToActionType($this->sysma_action_type_id, $this->app);

                // 3rd delete this sysma_action_type
                $qry = $this->pdo->prepare("DELETE FROM " . SCHEMA . ".sysma_action_type WHERE sysma_action_type_id=" . $this->sysma_action_type_id);
                $res = returnResultQuery($qry, $this->pdo, "Cannot delete sysma_action_type");
            } catch (Exception $e) {
                // ROLLBACK
                $this->pdo->rollbackNestedTransaction();
            }
        }
        // COMMIT
        $this->pdo->commitNestedTransaction();

        Debug::endPgLogAndPrintResults($this->app);
        return $res;
    }

    /************************************************************
     * Import func
     * 
     * 
     */
    public function importFromJson($_obj)
    {
        $rc = ''; // TODO error mgmt

        // rm pk id to let pg deal with it
        unset($_obj->sysma_action_type_id);

        // 1st add it then retreive its id after insert
        // make a local cpy of this parent obj
        $obj = clone $_obj;
        // rm inner sub objs
        unset($obj->actionTypeParams);

        // Finally, add modified by
        $obj->modified_by = $this->app['session']->get('user_id');
        // persist it
        $rc = json_decode($this->create($obj)); // TODO FIXM : we shouldn't have to decode here. cf. create() : it does the encoding 

        if ($rc->status != 'success') {
            return $rc;
        }
        $id = $rc->id;

        // 2nd now import/deal with inner objs

        if (property_exists($_obj, 'actionTypeParams')) {
            foreach ($_obj->actionTypeParams as $atp) {
                // For DEV WIP!!! rm this!!! v
                // $atp->parameter_alias = uniqid();
                // For DEV WIP!!! rm this!!! ^
                // $sap->sysma_object_type_id = $id;
                $atp->sysma_action_type_id = $id;

                $sap = new SysmaActionParameter(null, $this->app);
                $rc = $sap->importFromJson($atp);
            }
        }

        return $rc;
    }

    /************************************************************
     * export func
     * export the minimal requested members of "sysma action type"
     * 
     */
    public function getExport()
    {
        $sat = new StdClass;

        try {
            // Id shouln't be necessary IIRC but let's export it for now.
            $sat->sysma_action_type_id     = $this->sysma_action_type_id;
            $sat->sysma_action_type        = $this->sysma_action_type;
            // $sotp->action_type_id     = $this->sysma_action_type_id;
            // $sotp->action_type        = $this->sysma_action_type;
            $sat->description              = $this->description;
            $sat->sysma_object_type_id     = $this->sysma_object_type_id;
            // $sotp->object_type_id     = $this->sysma_object_type_id;

            $sat->modified_at              = $this->modified_at;
            $sat->created_at               = $this->created_at;
            $sat->style                    = $this->style;

            $sat->sysma_action_type_alias  = $this->sysma_action_type_alias;
            // $sotp->action_type_alias  = $this->sysma_action_type_alias;
            $sat->track_historytables_id   = $this->track_historytables_id;
            // $sotp->parameter_source         = $this->parameter_source;
            $sat->modified_by              = $this->modified_by;
            $sat->zoom_min                 = $this->zoom_min;
            $sat->zoom_max                 = $this->zoom_max;
            $sat->load_on_pan              = $this->load_on_pan;
            $sat->unit_cost                = $this->unit_cost;
            $sat->unit_measure             = $this->unit_measure;

            $sat->dig_justification                            = $this->dig_justification;
            $sat->dig_incidences_phase_travaux                 = $this->dig_incidences_phase_travaux;
            $sat->dig_incidences_fonctionnement                = $this->dig_incidences_fonctionnement;
            $sat->dig_prescriptions_et_mesures_accompagnement  = $this->dig_prescriptions_et_mesures_accompagnement;
            $sat->dig_reference_nomenclature                   = $this->dig_reference_nomenclature;

            // try to load paramArray if necessary
            if (empty($this->paramArray)) {
                $this->paramArray = null;
                $this->loadParameters();
            }

            $sat->actionTypeParams = [];
            foreach ($this->paramArray as $param) {
                $sat->actionTypeParams[] = $param->getExport();
            }
            // $sotp->actionTypeParams = $this->paramArray;

            // $sotp->choices = [];
            // foreach($this->choicesArray as $choice) {
            //     $sotp->choices[] = $choice;
            // }
            // $sotp->choices                  = $this->choicesArray;
        } catch (Exception $ex) {
            return 'TODO';
            // return '<span class="glyphicon glyphicon-remove text-danger" title="' . $ex->getMessage() . '"></span>' . $ex->getMessage();
        }

        return $sat;
    }
}
