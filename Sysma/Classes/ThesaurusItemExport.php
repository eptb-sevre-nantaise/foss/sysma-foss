<?php

/**
 * ThesaurusItemExport class goal is to deal with futur evolution of the thesaurus item format
 * 
 * @author alriviere
 */

class ThesaurusItemExport
{
    const LATEST_FORMAT_VERSION = '1.0';
    const TIID_RANDOM_PART_LENGTH = 8;


    public function __construct(SysmaObjectType $_sot, string $_format_version = ThesaurusItemExport::LATEST_FORMAT_VERSION)
    {

        switch ($_format_version) {
            case '1.0':
                $this->construct_v1_0($_sot, $_format_version);
                break;
            default:
                $msg = 'Error. Invalid ' . __CLASS__ . ' format.';
                throw new RuntimeException($msg);
                die($msg);
                break;
        }
    }


    // private funcs

    /*********************************************************************
     * construct_v1_0
     * Construct Thesaurus Item Export object for format version 1.00 
     ********************************************************************/
    private function construct_v1_0(SysmaObjectType $_sot, $_format_version)
    {
        // $tiex = new StdClass;
        $this->thesaurus_item = new StdClass;
        $this->metadata = new StdClass;

        ///////////////////////////////////////////////////////////
        // Thesaurus Item part ------------------------------------
        // $ot->sysma_object_type          = $this->type;
        $this->thesaurus_item->sysma_object_type          = $_sot->sysma_object_type;
        $this->thesaurus_item->description                = $_sot->description;
        $this->thesaurus_item->geometry_type              = $_sot->geometry_type;
        $this->thesaurus_item->created_at                 = $_sot->created_at;
        $this->thesaurus_item->modified_at                = $_sot->modified_at;
        $this->thesaurus_item->data_source                = $_sot->data_source;
        $this->thesaurus_item->definition_source          = $_sot->definition_source;
        $this->thesaurus_item->digitization_layer         = $_sot->digitization_layer; // voir a sr: est-ce que le sysma aura ces couches?
        $this->thesaurus_item->style                      = $_sot->style_decode;
        $this->thesaurus_item->sysma_object_type_alias    = $_sot->alias;
        $this->thesaurus_item->group_alias                = $_sot->group_alias; // voir a sr pour l'import
        // $ot->track_historytables          = $this->creation_date;
        // $ot->modified_by          ; // pas export ou export mais pas pris en compte ou val default a l'import?
        $this->thesaurus_item->zoom_min                   = $_sot->zoom_min;
        $this->thesaurus_item->zoom_max                   = $_sot->zoom_max;
        $this->thesaurus_item->load_on_pan                = $_sot->load_on_pan;
        $this->thesaurus_item->hidden                     = $_sot->hidden;

        // echo json_encode($tiex);
        // die();

        // object type params
        $this->thesaurus_item->objectTypeParams = [];
        if (!empty($_sot->paramArray)) {
            foreach ($_sot->paramArray as $otp) {
                $this->thesaurus_item->objectTypeParams[] = $otp->getExport();
            }
        }

        // actions
        $this->thesaurus_item->actionTypes = [];
        if (!empty($_sot->sysmaActionTypesArray)) {
            foreach ($_sot->sysmaActionTypesArray as $actionType) {
                $this->thesaurus_item->actionTypes[] = $actionType->getExport();
            }
        }
        // relations
        // TODO TBD
        // $ot->relationTypes = [];
        // if (!empty($this->sysmaRelationTypesArray) ) {
        //     if ( $filteredRelations == null ) {
        //         foreach ($this->sysmaRelationTypesArray as $relationType) {
        //             $rt = new StdClass;
        //             $rt = $relationType['RT']->getExport();
        //             // $rt->childObjectTypeId = $relationType['Child']->sysma_object_type_id;
        //             // $rt->parentObjectTypeId = $relationType['Parent']->sysma_object_type_id;
        //             $rt->parentObjectTypeId = $relationType['Parent']->sysma_object_type_alias;
        //             $rt->childObjectTypeId = $relationType['Child']->sysma_object_type_alias;

        //             $ot->relationTypes[] = $rt;
        //             // $ot->relationTypes[]  = $relationType['RT']->getExport();

        //             // $ot->relationTypes['RT']        = $relationType['RT']->getExport();
        //             // $ot->relationTypes['Parent']    = $relationType['Parent']->getExport();
        //             // $ot->relationTypes['Child']     = $relationType['Child']->getExport();
        //         }
        //     } else {
        //         echo __FUNCTION__ . ' -> TODO';
        //     }
        // }        




        $sot = clone $_sot;
        $md = json_decode($_sot->thesaurus_item_metadata);

        // echo '<br>/////// thesaurus_item_metadata: <br>';
        // echo json_encode($md);

        // $userId = $_sot->app['session']->get('user_id');
        $user = $_sot->app['session']->get('user');

        ///////////////////////////////////////////////////////////
        // Metadata part ------------------------------------------
        if (!empty($md->tiid)) {
            // Tiid is known (ie. thesaurus_item_metadata exists)
            // echo '<br>************************<br>';
            // echo 'Tiid is known (ie. thesaurus_item_metadata exists)';

            $this->metadata->tiid                   = $md->tiid;
            $this->metadata->parent_tiid            = $md->parent_tiid;
            $this->metadata->created_by_org         = $md->created_by_org;
            $this->metadata->created_by_user        = $md->created_by_user;
            $this->metadata->created_at             = $md->created_at;
            $this->metadata->exported_by_org        = ORGANISATION;
            $this->metadata->exported_by_user       = $user;
            $this->metadata->exported_at            = date('Y-m-d H:i:s');
            if (property_exists($md, 'tags')) {
                $this->metadata->tags                   = $md->tags;
            } else {
                $this->metadata->tags                   = '';
            }
            $this->metadata->ti_format_version      = $md->ti_format_version;
            $this->metadata->sysma_version          = $md->sysma_version;
            if (property_exists($md, 'official_reference_names')) {
                $this->metadata->official_reference_names = $md->official_reference_names;
            } else {
                $this->metadata->official_reference_names                   = '';
            }

            if (ThesaurusItemExport::verifyTiidAndDataIntrication($md->tiid, json_encode($this->thesaurus_item))) {
                    // Very highly probable that data have not changed.
                    // echo '<br>-----------<br>';
                    // echo 'Tiid match with Ti. There\'s is a high probability that data have not changed.';

                    // forward current data to export as it is
                ;
            } else {
                // Data have certainly change 
                // echo '<br>-----------<br>';
                // echo 'Ti\'s data certainly does have changed.';

                // create a new tiid
                $this->metadata->tiid = ThesaurusItemExport::generateThesaurusItemId(json_encode($this->thesaurus_item));
                $this->metadata->parent_tiid = $md->tiid; // new parent_tiid become current tiid
                $this->metadata->created_by_org = ORGANISATION;
                $this->metadata->created_by_user = $user;
                $this->metadata->created_at = date('Y-m-d H:i:s');
                $this->metadata->ti_format_version = $_format_version;
                $this->metadata->sysma_version = SYSMA_VERSION_NUMBER;

                // echo json_encode($tiex);
                // die();

                // UPDATE en DB : update tiid_parent (with current tiid) AND udpate tiid
                $qry = $_sot->pdo->prepare("UPDATE " . SCHEMA . ".sysma_object_type SET thesaurus_item_metadata = '" . json_encode($this->metadata, JSON_HEX_APOS | JSON_HEX_QUOT) . "' WHERE sysma_object_type_id = :sysma_object_type_id");
                $qry->bindParam(':sysma_object_type_id', $sot->sysma_object_type_id, PDO::PARAM_INT);
                $res = returnResultQuery($qry, $_sot->pdo, json_encode($this->metadata));

                $res = json_decode($res);
                // Important! We should not export file without being sure of updating DB first!
                if ($res->status == 'error') {
                    die(__CLASS__ . ' method: ' . __METHOD__ . ' Updating Thesaurus Item Metadata in DB not possible. Export canceled.'); // TODO better
                }

                //generate export file with updated data
            }
        } else {
            // Tiid is not known so this object-type is new and so its data
            // echo '<br>************************<br>';
            // echo 'Tiid is NOT known. New type-object';

            // create a new tiid
            $now = date('Y-m-d H:i:s');
            $org = ORGANISATION;

            $this->metadata->tiid = ThesaurusItemExport::generateThesaurusItemId(json_encode($this->thesaurus_item));
            $this->metadata->parent_tiid            = '';
            $this->metadata->created_by_org         = $org;
            $this->metadata->created_by_user        = $user;
            $this->metadata->created_at             = $now;
            $this->metadata->exported_by_org        = $org;
            $this->metadata->exported_by_user       = $user;
            $this->metadata->exported_at            = $now;
            $this->metadata->tags                   = '';
            // $this->metadata->tags                   = $md->tags;
            $this->metadata->ti_format_version      = $_format_version;
            $this->metadata->sysma_version          = SYSMA_VERSION_NUMBER;
            $this->metadata->official_reference_names = '';
            // $this->metadata->official_reference_names = $md->official_reference_names;


            // echo json_encode($tiex->metadata, JSON_HEX_APOS | JSON_HEX_QUOT );
            // die();

            // UPDATE en DB : update tiid_parent (with current tiid) AND udpate tiid
            $sql = "UPDATE " . SCHEMA . ".sysma_object_type SET thesaurus_item_metadata = '" . json_encode($this->metadata, JSON_HEX_APOS | JSON_HEX_QUOT) . "' WHERE sysma_object_type_id = :sysma_object_type_id";
            // $sql = "UPDATE " . SCHEMA . ".sysma_object_type SET thesaurus_item_metadata = $$" . '{"toto":"l\'tata"}' . "$$ WHERE sysma_object_type_id = :sysma_object_type_id";
            // echo '<br>************************<br>';
            // echo $sql;
            // echo '<br>************************<br>';
            // die();
            $qry = $_sot->pdo->prepare($sql);
            $qry->bindParam(':sysma_object_type_id', $sot->sysma_object_type_id, PDO::PARAM_INT);
            $res = returnResultQuery($qry, $_sot->pdo, json_encode($this->metadata));

            $res = json_decode($res);

            // Important! We should not export file without being sure of updating DB first!
            if ($res->status == 'error') {
                die(__CLASS__ . ' method: ' . __METHOD__ . ' Updating Thesaurus Item Metadata in DB not possible. Export canceled.'); // TODO better
            }
        }
    }



    /////////////////////////////////////////////////////////////////////////
    // public static funcs

    /**
     * Generate Thesaurus Item Id
     *
     * @param string $_data
     * @return void
     */
    public static function generateThesaurusItemId(string $_data)
    {
        $tiid = '';

        // 1st Tiid part : 8 char base62 random  
        $tiid .= generate_base62_random_string(ThesaurusItemExport::TIID_RANDOM_PART_LENGTH);

        // 2nd Tiid part : crc32 of the $_payload into base62 encoding
        $tiid .= get_crc_in_base62_encoding($_data);

        return $tiid;
    }

    /**
     * Verify if Tiid's CRC part match computed data CRC.
     *
     * @param string $_tiid
     * @param string $_data
     * @return boolean
     */
    public static function verifyTiidAndDataIntrication(string $_tiid, string $_data)
    {
        // get the crc part into _tiid
        $tiid_hash = substr($_tiid, ThesaurusItemExport::TIID_RANDOM_PART_LENGTH);

        // compute the crc of _data
        $data_hash = get_crc_in_base62_encoding($_data);

        // return the comparison
        return ($tiid_hash == $data_hash);
    }
}
