<?php

/**
 * Description of PiwigoPhoto 
 * 
 * @author srenou
 * 
 */
class PiwigoPhoto
{

    public function __construct($id, $app)
    {


        $this->pdo = $app['pdo.pwg'];
        if (is_numeric($id) and $id != null) {

            $qry = $this->pdo->prepare("select * from images where id = :id");
            $qry->bindParam(':id', $id, PDO::PARAM_INT);
            $qry->execute();
            $s = $qry->fetchObject();

            $this->id = $id;
            $this->file = utf8_encode($s->file);
            $this->path = $s->path;
            $this->author = $s->author;
            $this->comment = utf8_encode($s->comment);
            $this->center = 'LatLng(' . $s->latitude . ',' . $s->longitude . ')';
            $this->name = utf8_encode($s->name);
            $this->date_creation = $s->date_creation;

            $path_parts = pathinfo($this->path);
            $end_char_count = strlen($path_parts['extension']);

            $this->large = PWGURL . '/i.php?' . substr(str_replace(',', '-', $this->path), 1, (strlen($this->path) - $end_char_count - 2)) . '-la.' . $path_parts['extension'];
            $this->thumbnail = PWGURL . '/i.php?' . substr(str_replace(',', '-', $this->path), 1, (strlen($this->path) - $end_char_count - 2)) . '-me.' . $path_parts['extension'];
            $this->square = PWGURL . '/i.php?' . substr(str_replace(',', '-', $this->path), 1, (strlen($this->path) - $end_char_count - 2)) . '-sq.' . $path_parts['extension'];

            $this->thumbnailCode = '<a href="' . $this->large . '" target="_blank"><img src="' . $this->thumbnail . '" data-src="' . $this->thumbnail . '" /></a><br>' . $this->name . ' - ' . $this->date_creation;
            $this->squareCode = ' <a href="' . $this->large . '" target="_blank"><img src="' . $this->square . '" width="150px" data-src="' . $this->square . '" /></a><br>' . $this->name . '<br>' . dateDisplay($this->date_creation) . '<br>[' . $this->id . ']';
        }
    }
}
