<?php

/**
 * Description of SysmaObjectType
 * 
 *
 * @author srenou
 */



class SysmaObjectType
{

    public function __construct($id, $app)
    {

        $this->app = $app;
        $this->pdo = $app['pdo'];
        $this->paramArrayValue = null;

        if (is_numeric($id) and $id != null) {

            // WIP UUID @alr
            $qry = $this->pdo->prepare("select * from " . SCHEMA . ".sysma_object_type where sysma_object_type_id = :sysma_object_type_id");
            $qry->bindParam(':sysma_object_type_id', $id, PDO::PARAM_INT);
            // $qry = $this->pdo->prepare("select * from " . SCHEMA . ".sysma_object_type where sysma_object_type_uuid = :sysma_object_type_uid");
            // $qry->bindParam(':sysma_object_type_uid', $id, PDO::PARAM_INT);

            $qry->execute();
            $t = $qry->fetchObject();
            if (empty($t)) { // object does not exists               
                throw new NotFoundException('Sysma Object Type ' . $id . ' doesn\'t exist');
            }
            foreach ($t as $key => $value) {
                $this->$key = $value;
            }

            $this->type = ucfirst(html_entity_decode($t->sysma_object_type));
            $this->alias = $t->sysma_object_type_alias;
            $this->description = html_entity_decode($t->description, ENT_QUOTES);

            switch ($this->geometry_type) {
                case 'ST_Point':
                    $this->geometry_type_formatted = "Point";
                    break;
                case 'ST_Linestring':
                    $this->geometry_type_formatted = "Polyligne (linéaire)";
                    break;
                case 'ST_Polygon':
                    $this->geometry_type_formatted = "Polygone (Surface)";
                    break;
                default:
                    $this->geometry_type_formatted = 'Pas de géométrie';
            }

            $this->creation_date = dateDisplay($t->created_at);
            $this->modification_date = dateDisplay($t->modified_at);
            $this->style_decode = json_decode($this->style);


            switch ($this->digitization_layer) {
                case 'scan25':
                    $this->digitization_layerMisEnForme = 'Scan 25 IGN';
                    break;
                case 'orthophotos':
                    $this->digitization_layerMisEnForme = 'Photos aériennes IGN (Orthosphotos)';
                    break;
                case 'parcellaire':
                    $this->digitization_layerMisEnForme = 'Parcelles cadastrales';
                    break;
                default:
                    $this->digitization_layerMisEnForme = '';
            }
        } else {
            return false;
        }
    }

    public function loadParameters()
    {

        $this->paramArray = $this->paramArrayValue = array();

        $qry = $this->pdo->prepare("select * from " . SCHEMA . ".sysma_object_type_parameter where sysma_object_type_id = :sysma_object_type_id order by \"ordered_by\" ASC, \"parameter_id\" ASC");
        $qry->bindParam(':sysma_object_type_id', $this->sysma_object_type_id, PDO::PARAM_INT);
        $qry->execute();
        $i = 0;
        while ($p = $qry->fetchObject()) {
            $this->paramArrayValue[$i] = $p->parameter_id;
            $this->paramArray[$i] = new SysmaObjectParameter($p->parameter_id, $this->app); // objets
            $i++;
        }
    }

    public function loadSysmaActionSheets($in_organisation_id = null, $out_organisation_id = null)
    {

        if (isset($in_organisation_id)) {
            $this->actionSheetsInOrganisationArray = array();
        } elseif (isset($out_organisation_id)) {
            $this->actionSheetsOutOrganisationArray = array();
        } else {
            $this->actionSheetsArray = array();
        }

        $cond1 = $cond2 = null;
        if (isset($in_organisation_id))
            $cond1 = " and sysma_action.organisation_id = :in_organisation_id";
        if (isset($out_organisation_id))
            $cond2 = " and sysma_action.organisation_id != :hors_organisation_id";

        $qry = $this->pdo->prepare("select * from " . SCHEMA . ".sysma_action," . SCHEMA . ".sysma_action_type," . SCHEMA . ".organisation
                        where sysma_action_type.sysma_object_type_id = :sysma_object_type_id                        
                        and sysma_action_type.sysma_action_type_id = sysma_action.sysma_action_type_id 
                        and sysma_action.organisation_id = organisation.organisation_id
                        " . $cond1 . " " . $cond2 . " 
                        order by organisation.organisation_id ASC, sysma_action.start_date DESC");

        $qry->bindParam(':sysma_object_type_id', $this->sysma_object_type_id, PDO::PARAM_INT);
        if (isset($in_organisation_id))
            $qry->bindParam(':in_organisation_id', $in_organisation_id);
        if (isset($out_organisation_id))
            $qry->bindParam(':hors_organisation_id', $out_organisation_id);

        $qry->execute();

        while ($t1 = $qry->fetchObject()) {

            if (isset($in_organisation_id)) {
                array_push($this->actionSheetsInOrganisationArray, $t1);
            } elseif (isset($out_organisation_id)) {
                array_push($this->actionSheetsOutOrganisationArray, $t1);
            } else {
                array_push($this->actionSheetsArray, $t1);
            }
        }
    }

    public function loadSysmaActionTypes($withSysmaActionParameters = false)
    {

        $this->sysmaActionTypesArray = $this->sysmaActionTypesArrayVal = null;
        $qry = $this->pdo->prepare("select * from " . SCHEMA . ".sysma_action_type where sysma_object_type_id = :sysma_object_type_id order by sysma_action_type ASC");
        $qry->bindParam(':sysma_object_type_id', $this->sysma_object_type_id, PDO::PARAM_INT);
        $qry->execute();
        $i = 0;
        while ($p = $qry->fetchObject()) {

            $WT = new SysmaActionType($p->sysma_action_type_id, $this->app);
            if ($withSysmaActionParameters)
                $WT->loadParameters();
            $this->sysmaActionTypesArray[$i] = $WT;
            $this->sysmaActionTypesArrayVal[$i] = $p->sysma_action_type_id;
            $i++;
        }
    }

    public function loadSysmaRelationSheetsCount(){

        $qry = $this->pdo->prepare("
        select r.sysma_relation_id
        from " . SCHEMA . ".sysma_relation r," . SCHEMA . ".l_sysma_object_type_sysma_relation_type l
        where 
        (l.sysma_object_type_id_parent = :sysma_object_type_id
        or l.sysma_object_type_id_child = :sysma_object_type_id)
         and l.sysma_relation_type_id = r.sysma_relation_type_id
        ");
        $qry->bindParam(':sysma_object_type_id', $this->sysma_object_type_id, PDO::PARAM_INT);
        $qry->execute();
        return $qry->rowCount();


    }

    public function loadSysmaRelationSheets($in_organisation_id = null, $out_organisation_id = null)
    {


        if (isset($in_organisation_id)) {
            $this->relationSheetsInOrganisationArray = array();
        } elseif (isset($out_organisation_id)) {
            $this->relationSheetsOutOrganisationArray = array();
        } else {
            $this->relationSheetsArray = array();
        }

        $cond1 = $cond2 = null;
        if (isset($in_organisation_id))
            $cond1 = " and r.organisation_id = :in_organisation_id";
        if (isset($out_organisation_id))
            $cond2 = " and r.organisation_id != :hors_organisation_id";

        $qry = $this->pdo->prepare("
                        select r.sysma_relation_id,rt.sysma_relation_type_from_parent,rt.sysma_relation_type_from_child,r.start_date,r.end_date,
                        case when (l.sysma_object_type_id_parent = :sysma_object_type_id) then 'parent' else 'child' end as parent_child
                        from " . SCHEMA . ".sysma_relation r," . SCHEMA . ".l_sysma_object_type_sysma_relation_type l," . SCHEMA . ".sysma_relation_type rt," . SCHEMA . ".organisation o
                        where 
                        (l.sysma_object_type_id_parent = :sysma_object_type_id
                        or l.sysma_object_type_id_child = :sysma_object_type_id)
                         and l.sysma_relation_type_id = r.sysma_relation_type_id
                        and r.sysma_relation_type_id = rt.sysma_relation_type_id 
                        and r.organisation_id = o.organisation_id
                        " . $cond1 . " " . $cond2 . " 
                        order by o.organisation_id ASC, r.start_date DESC");
                      

        $qry->bindParam(':sysma_object_type_id', $this->sysma_object_type_id, PDO::PARAM_INT);
        if (isset($in_organisation_id))
            $qry->bindParam(':in_organisation_id', $in_organisation_id);
        if (isset($out_organisation_id))
            $qry->bindParam(':hors_organisation_id', $out_organisation_id);

        $qry->execute();

        while ($t1 = $qry->fetchObject()) {            

            $t1->objectsLinked[$t1->parent_child] = sysmaObjectsListInRelation($this->app, $t1->sysma_relation_id, invertPC($t1->parent_child));
            $t1->objectsLinked[invertPC($t1->parent_child)] = sysmaObjectsListInRelation($this->app, $t1->sysma_relation_id, $t1->parent_child);

            if (isset($in_organisation_id)) {
                array_push($this->relationSheetsInOrganisationArray, $t1);
            } elseif (isset($out_organisation_id)) {
                array_push($this->relationSheetsOutOrganisationArray, $t1);
            } else {
                array_push($this->relationSheetsArray, $t1);
            }
        }
    }

    public function loadSysmaRelationTypes($withSysmaRelationParameters = false)
    {

        $this->sysmaRelationTypesArray = $this->sysmaRelationTypesArrayVal = null;
        $qry = $this->pdo->prepare("select *,  case when (sysma_object_type_id_parent = :sysma_object_type_id) then 'parent' else 'child' end as parent_child from " . SCHEMA . ".l_sysma_object_type_sysma_relation_type where "
            . "(sysma_object_type_id_parent = :sysma_object_type_id or sysma_object_type_id_child = :sysma_object_type_id) order by display_order ASC");
        $qry->bindParam(':sysma_object_type_id', $this->sysma_object_type_id, PDO::PARAM_INT);
        $qry->execute();
        $i = 0;
        while ($p = $qry->fetchObject()) {

            $RT = new SysmaRelationType($p->sysma_relation_type_id, $this->app);
            if ($withSysmaRelationParameters)
                $RT->loadParameters();
            $this->sysmaRelationTypesArray[$i]['RT'] = $RT;
            $this->sysmaRelationTypesArray[$i]['infos'] = $p;
            $this->sysmaRelationTypesArray[$i]['Child'] = new SysmaObjectType($p->sysma_object_type_id_child, $this->app);
            $this->sysmaRelationTypesArray[$i]['Parent'] = new SysmaObjectType($p->sysma_object_type_id_parent, $this->app);
            $this->sysmaRelationTypesArrayVal[$i] = $p->sysma_relation_type_id;
            $i++;
        }
    }

    // TODO rename to getSysmaObjects as it is implemented. Or make it really load SysmaObjects into $this
    public function loadSysmaObjects()
    {

        $qry = $this->pdo->prepare("select * from " . SCHEMA . ".sysma_object where sysma_object_type_id = :id");
        $qry->bindParam(':id', $this->sysma_object_type_id, PDO::PARAM_INT);
        $qry->execute();
        return $qry->fetchAll(PDO::FETCH_ASSOC);
    }

    // TODO refactor? implementation similar in utils.php and in SysamAction.php?
    public function countSysmaObjects($sotId = null)
    {
        if ($sotId == null) {
            $sotId = $this->sysma_object_type_id;
        }
        $qry = $this->pdo->prepare("select count(sysma_object_id) as count from " . SCHEMA . ".sysma_object where sysma_object_type_id = :id");
        $qry->bindParam(':id', $sotId, PDO::PARAM_INT);
        $qry->execute();
        $res = $qry->fetchObject();
        return $res->count;
    }

    public function countLinkedSysmaActions($sotId = null)
    {
        if ($sotId == null) {
            $sotId = $this->sysma_object_type_id;
        }
        $qry = $this->pdo->prepare("select
            count(sysma_action.sysma_action_id) as count 
            FROM sysma.sysma_action_type
            JOIN sysma.sysma_action on (sysma_action_type.sysma_action_type_id = sysma_action.sysma_action_type_id )
            where sysma_action_type.sysma_object_type_id = :sysma_object_type_id");
        $qry->bindParam(':sysma_object_type_id', $sotId, PDO::PARAM_INT);
        $qry->execute();
        $res = $qry->fetchObject();
        return $res->count;
    }

    public function getLinkedSysmaActions($sotId = null)
    {
        if ($sotId == null) {
            $sotId = $this->sysma_object_type_id;
        }
        $qry = $this->pdo->prepare("select
            sysma_action.sysma_action_id
            FROM sysma.sysma_action_type
            JOIN sysma.sysma_action on (sysma_action_type.sysma_action_type_id = sysma_action.sysma_action_type_id )
            where sysma_action_type.sysma_object_type_id = :sysma_object_type_id");
        $qry->bindParam(':sysma_object_type_id', $sotId, PDO::PARAM_INT);
        $qry->execute();
        $res = $qry->fetchAll();
        return $res;
    }

    public function createRelation($data)
    {

        $qry = $this->pdo->prepare('insert into ' . SCHEMA . '.l_sysma_object_type_sysma_relation_type (sysma_object_type_id_parent,sysma_object_type_id_child,sysma_relation_type_id,display_order,modified_by) VALUES (:parent_id, :child_id, :relation_id, :display_order, :modified_by) RETURNING l_sysma_object_type_sysma_relation_type_id as id');
        $qry->bindParam(':parent_id', $this->sysma_object_type_id, PDO::PARAM_INT);
        $qry->bindParam(':child_id', $data['sysma_object_type_id_child'], PDO::PARAM_INT);
        $qry->bindParam(':relation_id', $data['sysma_relation_type_id'], PDO::PARAM_INT);
        $qry->bindParam(':display_order', $data['display_order'], PDO::PARAM_STR);
        $qry->bindParam(':modified_by', $data['modified_by'], PDO::PARAM_INT);
        if ($qry->execute()) {
            return $qry->fetchObject();
        } else {
            return false;
        }
    }

    public function updateRelation($idr, $data)
    {

        $qry = $this->pdo->prepare('update ' . SCHEMA . '.l_sysma_object_type_sysma_relation_type set '
            . 'sysma_object_type_id_child = :child_id,'
            . 'sysma_object_type_id_parent = :parent_id,'
            . 'sysma_relation_type_id = :relation_id,'
            . 'display_order = :display_order,'
            . 'modified_by = :modified_by '
            . 'where l_sysma_object_type_sysma_relation_type_id = :idr');

        $qry->bindParam(':child_id', $data['sysma_object_type_id_child'], PDO::PARAM_INT);
        $qry->bindParam(':parent_id', $data['sysma_object_type_id_parent'], PDO::PARAM_INT);
        $qry->bindParam(':relation_id', $data['sysma_relation_type_id'], PDO::PARAM_INT);
        $qry->bindParam(':display_order', $data['display_order'], PDO::PARAM_STR);
        $qry->bindParam(':modified_by', $data['modified_by'], PDO::PARAM_INT);
        $qry->bindParam(':idr', $idr, PDO::PARAM_INT);
        if ($qry->execute()) {
            return $qry->fetchObject();
        } else {
            return false;
        }
    }

    public function update($request)
    {

        $pp = prepUpdateQuery($request, 'sysma_object_type_id');

        $qry = $this->pdo->prepare("update " . SCHEMA . ".sysma_object_type set " . $pp['req'] . " where sysma_object_type_id = :sysma_object_type_id");

        foreach ($pp['tabD'] as $key => $val) {
            $qry->bindValue($key, $val);
        }

        return returnResultQuery($qry, $this->pdo, $pp['tabD']);
    }

    public function create($request)
    {

        $pp = prepInsertQuery($request);

        $qry = $this->pdo->prepare("insert into " . SCHEMA . ".sysma_object_type (" . $pp['reqFields'] . ") VALUES (" . $pp['reqParams'] . ") RETURNING sysma_object_type_id as id");

        foreach ($pp['tabD'] as $key => $val) {
            if (is_object($val)) {
                $val = json_encode($val);
            }
            $qry->bindValue($key, $val);
        }

        return returnResultQuery($qry, $this->pdo, $pp['tabD']);
    }

    /************************************************************
     * delete
     */
    public function delete($sotId = null)
    {
        Debug::startPgLog();

        if ($sotId == null) {
            $sotId = $this->sysma_object_type_id;
        }
        $res = null;

        if (!empty($sotId)) {
            // if ( $sotId == 'ALL' ) {
            //     $qry = $this->pdo->prepare("DELETE FROM " . SCHEMA . ".sysma_object_type");
            //     $res = returnResultQuery($qry, $this->pdo, null);
            // }
            if (is_numeric($sotId)) {
                try {
                    // BEGIN
                    // $this->pdo->beginTransaction();
                    $this->pdo->beginNestedTransaction();
                    // echo 'pdo beginTransaction<br>';

                    ///////////////////////////////////////////////////////////////////////////
                    // Phase A : related type actions
                    if (!empty($this->sysmaActionTypesArray)) {
                        foreach ($this->sysmaActionTypesArray as $sat) {
                            // $sat->loadParameters(); // load parameters to delete deep
                            $res = $sat->delete(); // TODO
                        }
                    }

                    ///////////////////////////////////////////////////////////////////////////
                    // Phase A : related type relations                        

                    ///////////////////////////////////////////////////////////////////////////
                    // Phase A.1 : delete every existing objects of that type
                    $so_array = $this->loadSysmaObjects();
                    foreach ($so_array as $so_item) {
                        $so = new SysmaObject($so_item['sysma_object_id'], $this->app);
                        $res = $so->delete();
                    }
                    // delete filters on object-type
                    SysmaFilter::deleteFiltersLinkedToObjectType($sotId, $this->app);


                    // Phase B.3 : delete sysma_object_type_parameter
                    foreach ($this->paramArray as $sotp) {
                        $res = $sotp->delete();
                    }

                    // Phase A.5 : delete sot in l_sysma_object_type_user
                    $qry = $this->pdo->prepare('DELETE FROM ' . SCHEMA . '.l_sysma_object_type_user where sysma_object_type_id = ' . $sotId);
                    $res = returnResultQuery($qry, $this->pdo,  "Cannot delete l_sysma_object_type_user");

                    // Phase B.4 - Finally delete this sysma_object_type
                    $qry = $this->pdo->prepare("DELETE FROM " . SCHEMA . ".sysma_object_type" . " WHERE sysma_object_type_id=" . $sotId);
                    $res = returnResultQuery($qry, $this->pdo, "Cannot delete sysma_object_type : " . $sotId);
                } catch (Exception $e) {
                    // if any SQL error : ROLLBACK
                    // $this->pdo->rollback();
                    $this->pdo->rollbackNestedTransaction();
                    // echo 'pdo ROLLBACK<br>';
                }
                // if everything's ok : COMMIT
                // $this->pdo->rollback(); // for dbg only
                // $this->pdo->commit();
                $this->pdo->commitNestedTransaction();
                // echo 'pdo COMMIT<br>';
            }
        }

        Debug::endPgLogAndPrintResults($this->app);
        return $res;
    }

    // public function delete($sotId=null) {
    //     if ($sotId == null) {
    //         $sotId = $this->sysma_object_type_id;
    //     }
    //     $res = null;

    //     if ( !empty($sotId) ) {
    //         // if ( $sotId == 'ALL' ) {
    //         //     $qry = $this->pdo->prepare("DELETE FROM " . SCHEMA . ".sysma_object_type");
    //         //     $res = returnResultQuery($qry, $this->pdo, null);
    //         // }
    //         if (is_numeric($sotId)) {
    //             try {
    //                 // BEGIN
    //                 // $this->pdo->beginTransaction();
    //                 $this->pdo->beginNestedTransaction();
    //                 // echo 'pdo beginTransaction<br>';

    //                 ///////////////////////////////////////////////////////////////////////////
    //                 // Phase A : delete existing objects and actions                        
    //                 // Phase A.1 : delete every existing objects of that type
    //                 $so_array = $this->loadSysmaObjects();
    //                 foreach($so_array as $so_item) {
    //                     $so = new SysmaObject($so_item['sysma_object_id'], $this->app);
    //                     $res = $so->delete();
    //                 }
    //                 // Phase A.2 : delete every existing linked actions 
    //                 $sa_array = $this->getLinkedSysmaActions();
    //                 foreach($sa_array as $sa_item) {
    //                     $satId = $sa_item['sysma_action_id'];
    //                     $sa = new SysmaAction($satId, $this->app);
    //                     $res = $sa->delete();

    //                     // Phase A.3 : delete sat in l_sysma_action_type_user
    //                     // TODO? TOCHECK : is it required? It seems not so far... but let's do it anyway since it should be delete in theory...
    //                     $qry = $this->pdo->prepare('delete from ' . SCHEMA . '.l_sysma_action_type_user where sysma_action_type_id = ' . $satId);
    //                     $res= returnResultQuery($qry, $this->pdo,  "Cannot delete l_sysma_action_type_user");
    //                 }
    //                 // delete filters on object-type
    //                 SysmaFilter::deleteFiltersLinkedToObjectType($sotId, $this->app);

    //                 // Phase A.4 : delete every existing linked relations? 
    //                 // TODO?
    //                 // Phase A.5 : delete sot in l_sysma_object_type_user
    //                 $qry = $this->pdo->prepare('delete from ' . SCHEMA . '.l_sysma_object_type_user where sysma_object_type_id = ' . $sotId);
    //                 $res= returnResultQuery($qry, $this->pdo,  "Cannot delete l_sysma_object_type_user");

    //                 ///////////////////////////////////////////////////////////////////////////
    //                 // Phase B : delete every related types up to this object type from the deepest to the highest lvl                  
    //                 // Phase B.1 : delete every related action types                        
    //                 // $this->loadSysmaActionTypes(true);
    //                 if(!empty($this->sysmaActionTypesArray)) {
    //                     foreach ($this->sysmaActionTypesArray as $sat) {
    //                         $sat->loadParameters(); // load parameters to delete deep
    //                         $res = $sat->delete(); // TODO
    //                     }
    //                 }
    //                 // Phase B.2 : delete every related relation types?
    //                 // TODO?                        

    //                 // Phase B.3 : delete sysma_object_type_parameter
    //                 foreach ($this->paramArray as $sotp) {
    //                     $res = $sotp->delete();
    //                 }

    //                 // Phase B.4 - Finally delete this sysma_object_type
    //                 $qry = $this->pdo->prepare("DELETE FROM " . SCHEMA . ".sysma_object_type" . " WHERE sysma_object_type_id=" . $sotId);
    //                 $res= returnResultQuery($qry, $this->pdo, "Cannot delete sysma_object_type : ".$sotId);

    //             } catch (Exception $e) {
    //                 // if any SQL error : ROLLBACK
    //                 // $this->pdo->rollback();
    //                 $this->pdo->rollbackNestedTransaction();
    //                 // echo 'pdo ROLLBACK<br>';
    //             }
    //             // if everything's ok : COMMIT
    //             // $this->pdo->rollback(); // for dbg only
    //             // $this->pdo->commit();
    //             $this->pdo->commitNestedTransaction();
    //             // echo 'pdo COMMIT<br>';
    //         }
    //     }
    //     return $res;
    // }
    /************************************************************
     * Import func
     * 
     * 
     */
    public function importFromJson($_thesaurusItemExport)
    {
        $rc = ''; // TODO error mgmt

        // echo json_encode($_obj);
        // die();

        // get a ref to thesaurus item
        $_ti = $_thesaurusItemExport->thesaurus_item;
        // and get a local cpy of the innner object type part to rebuild it
        $sot = clone $_thesaurusItemExport->thesaurus_item;
        // insert metadata if any
        if (property_exists($_thesaurusItemExport, 'metadata')) {
            $sot->thesaurus_item_metadata =  $_thesaurusItemExport->metadata;
        }

        // rm PK id if any to let pg deal with it
        unset($sot->sysma_object_type_id);

        // 1st add it then retreive its id after insert
        // rm inner sub objs
        unset($sot->objectTypeParams);
        unset($sot->actionTypes);
        unset($sot->relationTypes);

        // Finally, add modified by
        $sot->modified_by = $this->app['session']->get('user_id');
        // persist it

        // echo '<br>********************************************************************************<br>';
        // echo json_encode($ti);
        // echo '<br><br>';
        // die();
        try {
            // BEGIN
            // $this->pdo->beginTransaction();
            $this->pdo->beginNestedTransaction();

            $rc = json_decode($this->create($sot)); // TODO FIXM : we shouldn't have to decode here. cf. create() : it does the encoding 

            if ($rc->status != 'success') {
                return $rc;
            }
            $id = $rc->id; // get newly created id of this sot


            // now import/deal with other inner objects.
            // 2nd import param types
            if (property_exists($_ti, 'objectTypeParams')) {
                foreach ($_ti->objectTypeParams as $otp) {
                    // For DEV WIP!!! rm this!!! v
                    // $otp->sysma_object_type_id = uniqid();
                    // For DEV WIP!!! rm this!!! ^
                    $otp->sysma_object_type_id = $id;

                    $sop = new SysmaObjectParameter(null, $this->app);
                    $res = $sop->importFromJson($otp);
                }
            }

            // 3rd import action types
            if (property_exists($_ti, 'actionTypes')) {
                foreach ($_ti->actionTypes as $at) {
                    // For DEV WIP!!! rm this!!! v
                    // $at->sysma_action_type_alias = uniqid();
                    // For DEV WIP!!! rm this!!! ^
                    $at->sysma_object_type_id = $id;

                    $sat = new SysmaActionType(null, $this->app);
                    $res = $sat->importFromJson($at);
                }
            }

            // 4th import relation types
            // TODO TBD
            // if ( property_exists($_ti, 'relationTypes')  ) {
            //     foreach($_ti->relationTypes as $rt) {
            //         // For DEV WIP!!! rm this!!! v
            //         $rt->sysma_relation_type_alias = uniqid();
            //         // For DEV WIP!!! rm this!!! ^
            //         $rt->sysma__type_id = $id;

            //         $srt = new SysmaRelationType(null, $this->app);
            //         $rc = $srt->importFromJson($rt);
            //     }
            // }
            // return $_obj;
            // var_dump($_obj);
            // die;
        } catch (Exception $e) {
            // if error : ROLLBACK
            // $this->pdo->rollback();
            $this->pdo->rollbackNestedTransaction();
            // echo 'pdo ROLLBACK<br>';
        }
        // if everything's ok : COMMIT
        // $this->pdo->rollback(); // for dbg only
        // $this->pdo->commit();
        $this->pdo->commitNestedTransaction();

        return $rc;
    }



    /************************************************************
     * Retreive id from its TIID
     * (TODO: This func use "alias" atm. Should use UUID later.)
     * 
     */
    public static function retreiveId($_tiid, $_app)
    {
        $rc = null;
        $qry = $_app['pdo']->prepare("SELECT  sysma_object_type_id AS id FROM " . SCHEMA . ".sysma_object_type WHERE thesaurus_item_metadata->>'tiid' = :tiid");
        $qry->bindParam(':tiid', $_tiid, PDO::PARAM_INT);
        if ($qry->execute()) {
            $res = $qry->fetchObject();
            if (!empty($res->id)) {
                $rc = $res->id;
            }
        }
        return $rc;
    }

    /************************************************************
     * Export func
     * 
     * 
     */
    public function getExportFiltered($filteredParams = null, $filteredActions = null, $filteredRelations = null)
    {
        $tiex = new ThesaurusItemExport($this, '1.0'); // thesaurus item export
        // echo json_encode($tiex);
        // die();
        return $tiex;
    }



   
}
