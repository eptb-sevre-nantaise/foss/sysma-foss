<?php


class Organisation
{

    public function __construct($app, $id)
    {

        $this->app = $app;
        $this->pdo = $app['pdo'];
        if (is_numeric($id) and isset($id)) {

            $q = 'select * from ' . SCHEMA . '.organisation where organisation_id = :id';

            $qry = $this->pdo->prepare($q);
            $qry->bindParam(':id', $id, PDO::PARAM_INT);

            $qry->execute();
            if ($qry->rowCount() > 0) {
                foreach ($qry->fetchObject() as $key => $val) {
                    $this->$key = $val;
                }
            } else {
                return null;
            }
        } else {
            return null;
        }
    }


    public function countSysmaObjects()
    {

        $q = 'select sysma_object_id from ' . SCHEMA . '.sysma_object where organisation_id = :id';
        $qry = $this->pdo->prepare($q);
        $qry->bindParam(':id', $this->organisation_id);
        $qry->execute();
        return $qry->rowCount();
    }


    public function countSysmaActions()
    {

        $q = 'select sysma_action_id from ' . SCHEMA . '.sysma_action where organisation_id = :id';
        $qry = $this->pdo->prepare($q);
        $qry->bindParam(':id', $this->organisation_id);
        $qry->execute();
        return $qry->rowCount();
    }


    public function update($request)
    {


        $pp = prepUpdateQuery($request, 'organisation_id');

        $qry = $this->pdo->prepare("update " . SCHEMA . ".organisation set " . $pp['req'] . " where organisation_id = :organisation_id");
        foreach ($pp['tabD'] as $key => $val) {
            $qry->bindValue($key, $val);
        }
        return returnResultQuery($qry, $this->pdo, $pp['tabD']);
    }


    public function create($request)
    {

        $pp = prepInsertQuery($request);
        $qry = $this->pdo->prepare("insert into " . SCHEMA . ".organisation (" . $pp['reqFields'] . ") VALUES (" . $pp['reqParams'] . ") RETURNING organisation_id");
        foreach ($pp['tabD'] as $key => $val) {
            $qry->bindValue($key, $val);
        }
        return returnResultQuery($qry, $this->pdo, $pp['tabD']);
    }

    public function delete()
    {
        $qry = $this->pdo->prepare("delete from " . SCHEMA . ".organisation where organisation_id = :id");
        $qry->bindParam(':id', $this->organisation_id, PDO::PARAM_INT);
        return $qry->execute();
    }
}
