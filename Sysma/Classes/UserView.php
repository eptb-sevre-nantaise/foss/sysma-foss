<?php

/**
 * Description of UserView
 *
 * @author srenou
 */
class UserView
{

    public function __construct($id, $app)
    {

        $this->pdo = $app['pdo'];
        if ($id != null and is_numeric($id)) {

            $vars = buildSimpleEntity('user_views', 'user_view_id', $id, $app);

            foreach ($vars as $key => $value) {
                $this->$key = $value;
            }
            return true;
        } else {
            return false;
        }
    }



    public function insert($request)
    {


        $pp = prepInsertQuery($request);


        $qry = $this->pdo->prepare("insert into " . SCHEMA . ".user_views (" . $pp['reqFields'] . ") VALUES (" . $pp['reqParams'] . ") returning user_view_id as id");

        foreach ($pp['tabD'] as $key => $val) {
            $qry->bindValue($key, $val);
        }

        return returnResultQuery($qry, $this->pdo, $pp['tabD']);
    }

    public function update($style)
    {

        $pp = prepUpdateQuery($request, 'user_view_id');
        $qry = $this->pdo->prepare("update " . SCHEMA . ".user_views set " . $pp['req'] . " where user_view_id = :user_view_id");
        foreach ($pp['tabD'] as $key => $val) {
            $qry->bindValue($key, $val);
        }
        return returnResultQuery($qry, $this->pdo, $pp['tabD']);
    }

    public function delete()
    {

        $qry = $this->pdo->prepare('delete from  ' . SCHEMA . '.user_views where user_view_id = :id');
        $qry->bindParam('id', $this->user_view_id, PDO::PARAM_INT);
        return $qry->execute();
    }

    /*
     * applique l'analyse à une value
     * $options min : value min pour un gradient, max value max
     */

    public function apply($value, $options = null)
    {
    }
}
