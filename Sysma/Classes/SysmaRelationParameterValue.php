<?php

/**
 * Description of SysmaRelationParameterValue
 *  
 *
 * @author srenou
 * 
 */
class SysmaRelationParameterValue extends ParameterValue
{

    protected $type = 'sysma_relation';
    protected $typeId = 'sysma_relation_id';
    protected $typeTableDonnees = 'sysma_relation_data';
    protected $typeTableChoixparameters = 'sysma_relation_type_parameter_choice';

    public function __construct($id, $app = null)
    {

        $this->pdo = $app['pdo'];
        if ($id != null and is_numeric($id)) {

            $qry = $this->pdo->prepare("select *,d.created_at as created_at_value "
                . " from " . SCHEMA . ".sysma_relation_data  d "
                . "LEFT JOIN " . SCHEMA . ".user u on u.user_id = d.modified_by "
                . "FULL JOIN " . SCHEMA . ".sysma_relation_type_parameter t on d.parameter_id = t.parameter_id "
                . "where d.data_index = :id");
            $qry->bindParam(':id', $id, PDO::PARAM_INT);
            $qry->execute();
            $compt = 0;

            while ($v = $qry->fetchObject()) {

                $this->sysma_relation_id = $v->sysma_relation_id;
                $this->parameter_id = $v->parameter_id;
                $this->parameter = new SysmaRelationParameter($this->parameter_id, $app);
                $this->creation_date = $v->created_at_value;
                $this->iduser = $v->modified_by;
                $this->data_index = $id;
                $this->firstname = $v->firstname;
                $this->lastname = $v->lastname;
                $this->descriptionparameter = null;

                if ($this->parameter->dataType == 'multipleTextChoiceType') {

                    $this->value[$compt] = $v->value;
                    $compt++;
                } else {

                    $this->value = $v->value;
                }
            }
        }
    }

    /*
     * construction de la value via une série de parametres
     * pour un sysma_relation, un parametre
     * une seule value possible pour une sysma_relation et un paramètre donnés
     *
     */

    public function buildFromParameters($sysma_relation_id, $parameter_id, $app)
    {



        if (is_numeric($sysma_relation_id) and $sysma_relation_id != null and is_numeric($parameter_id) and $parameter_id != null) {

            $this->pdo = $app['pdo'];
            $this->descriptionparameter = null;
            $qry = $this->pdo->prepare("select * from " . SCHEMA . ".sysma_relation_data LEFT JOIN " . SCHEMA . ".user u on u.user_id = sysma_relation_data.modified_by where sysma_relation_id = :sysma_relation_id and parameter_id = :parameter_id");
            $qry->bindParam(':sysma_relation_id', $sysma_relation_id, PDO::PARAM_INT);
            $qry->bindParam(':parameter_id', $parameter_id, PDO::PARAM_INT);
            $qry->execute();

            // no value
            if ($qry->rowCount() == 0) {

                $this->sysma_relation_id = $sysma_relation_id;
                $this->parameter_id = $parameter_id;
                $this->parameter = new SysmaRelationParameter($this->parameter_id, $app);
                $this->value = null;
                $this->creation_date = null;
                $this->data_index = null;
            }
            // one value
            elseif ($qry->rowCount() == 1) {


                $v = $qry->fetchObject();

                $this->sysma_relation_id = $v->sysma_relation_id;
                $this->parameter_id = $v->parameter_id;
                $this->parameter = new SysmaRelationParameter($this->parameter_id, $app);
                $this->value = $v->value;
                $this->creation_date = $v->created_at;
                $this->iduser = $v->modified_by;
                $this->data_index = $v->data_index;
                $this->firstname = $v->firstname;
                $this->lastname = $v->lastname;
            }
            // many values
            elseif ($qry->rowCount() > 1) {

                $compt = 0;

                while ($v = $qry->fetchObject()) {

                    if ($compt > 0) {
                        $this->sysma_relation_id = $v->sysma_relation_id;
                        $this->parameter_id = $v->parameter_id;
                        $this->parameter = new SysmaRelationParameter($this->parameter_id, $app);

                        $this->creation_date = $v->created_at;
                        $this->iduser = $v->modified_by;
                        $this->data_index = $v->data_index;
                        $this->firstname = $v->firstname;
                        $this->lastname = $v->lastname;
                    }
                    $this->value[$compt] = $v->value;
                    $compt++;
                }
            }
        }
    }

    public function multipleUpdate($request)
    {


        $this->loadChoices($this->parameter->dataType);
        $res = $this->delete();

        $new_index = dataIndex($this->pdo);

        foreach ($this->choicesArray as $choice) {

            $inputName = 'choice' . $choice['choice_id'];
            if ($request->get($inputName) == 1) {

                $r2['parameter_id'] = $request->get('parameter_id');
                $r2['sysma_relation_id'] = $request->get('sysma_relation_id');
                $r2['created_at'] = $request->get('created_at');
                $r2['data_index'] = $new_index;
                $r2['value'] = $choice['choice'];
                $r2['user_id'] = $request->get('user_id');
                $r2['modified_by'] = $request->get('modified_by');

                $res = $this->create($r2);
            }
        }

        return $res; // last insert
    }

    public function delete()
    {

        $qry = $this->pdo->prepare('delete from ' . SCHEMA . '.sysma_relation_data where '
            . 'data_index = :data_index and '
            . 'sysma_relation_id = :sysma_relation_id and '
            . 'parameter_id = :parameter_id');
        $qry->bindParam(':data_index', $this->data_index);
        $qry->bindParam(':sysma_relation_id', $this->sysma_relation_id);
        $qry->bindParam(':parameter_id', $this->parameter_id);

        return returnResultQuery($qry, $this->pdo, null);
    }
}
