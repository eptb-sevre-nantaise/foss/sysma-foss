<?php


class Contract
{

    public function __construct($id, $app)
    {

        $this->pdo = $app['pdo'];
        if (is_numeric($id) and $id != null) {

            $ol = buildSimpleEntity('contract', 'contract_id', $id, $app);
            foreach ($ol as $key => $val) {
                $this->$key = $val;
            }
        }
    }

    public function updateContractFromActions($new_contract)
    {

        $q = 'select * from ' . SCHEMA . '.sysma_action where :contract = ANY(contracts)';
        $qry = $this->pdo->prepare($q);
        $qry->bindValue(':contract', $this->contract);
        $qry->execute();
        while ($res = $qry->fetchObject()) {

            $qry2 = $this->pdo->prepare('update ' . SCHEMA . '.sysma_action set contracts = array_replace(contracts, :old_contract, :new_contract) where sysma_action_id = :sysma_action_id');
            $qry2->bindParam(':old_contract', $this->contract, PDO::PARAM_STR);
            $qry2->bindParam(':new_contract', $new_contract, PDO::PARAM_STR);
            $qry2->bindParam(':sysma_action_id', $res->sysma_action_id, PDO::PARAM_INT);
            $qry2->execute();
        }
    }

    public function updateContractFromFilters($new_contract)
    {

        $q = 'select * from ' . SCHEMA . '.filter where sql ilike :c or filter ilike :c';
        $qry = $this->pdo->prepare($q);
        $qry->bindValue(':c', "%$this->contract%");
        $qry->execute();
        while ($res = $qry->fetchObject()) {

            $v2_sql = $v2_filter = null ;
            $v2_sql = str_replace($this->contract, $new_contract, $res->sql) ;
            $v2_filter = str_replace($this->contract, $new_contract, $res->filter) ;

            $qry2 = $this->pdo->prepare('update ' . SCHEMA . '.filter set sql = :v2_sql, filter = :v2_filter where filter_id = :filter_id');
            $qry2->bindParam(':v2_sql', $v2_sql, PDO::PARAM_STR);
            $qry2->bindParam(':v2_filter', $v2_filter, PDO::PARAM_STR);
            $qry2->bindParam(':filter_id', $res->filter_id, PDO::PARAM_INT);
            $qry2->execute();
        }
    }


    public function update($request)
    {

        $this->updateContractFromActions($request['contract']);
        $this->updateContractFromFilters($request['contract']);
        $pp = prepUpdateQuery($request, 'contract_id');

        $qry = $this->pdo->prepare("update " . SCHEMA . ".contract set " . $pp['req'] . " where contract_id = :contract_id");
        foreach ($pp['tabD'] as $key => $val) {
            $qry->bindValue($key, $val);
        }
        return returnResultQuery($qry, $this->pdo, $pp['tabD']);
    }

    public function create($request)
    {
        $pp = prepInsertQuery($request);
        $qry = $this->pdo->prepare("insert into " . SCHEMA . ".contract (" . $pp['reqFields'] . ") VALUES (" . $pp['reqParams'] . ") RETURNING contract_id");
        foreach ($pp['tabD'] as $key => $val) {
            $qry->bindValue($key, $val);
        }
        return returnResultQuery($qry, $this->pdo, $pp['tabD']);
    }

    public function deleteContractFromActions()
    {

        $q = 'select * from ' . SCHEMA . '.sysma_action where :contract = ANY(contracts)';
        $qry = $this->pdo->prepare($q);
        $qry->bindValue(':contract', $this->contract);
        $qry->execute();
        while ($res = $qry->fetchObject()) {

            $qry2 = $this->pdo->prepare('update ' . SCHEMA . '.sysma_action set contracts = array_remove(contracts, :contract) where sysma_action_id = :sysma_action_id');
            $qry2->bindParam(':contract', $this->contract, PDO::PARAM_STR);
            $qry2->bindParam(':sysma_action_id', $res->sysma_action_id, PDO::PARAM_INT);
            return $qry2->execute();
        }
    }

    public function delete()
    {
        $this->deleteContractFromActions();
        $qry = $this->pdo->prepare("delete from " . SCHEMA . ".contract where contract_id = :id");
        $qry->bindParam(':id', $this->contract_id, PDO::PARAM_INT);
        return $qry->execute();
    }
}
