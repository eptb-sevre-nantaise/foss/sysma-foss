<?php


class SysmaPDO extends PDO
{

    protected $transactionCounter = 0;
    public $logAllQueries = false;

    public function __construct( string $dsn, ?string $username = null, ?string $password = null, ?array $options = null) {
        $options[\PDO::ATTR_STATEMENT_CLASS] = array('SysmaPDOStatement', array($this));
        $options[\PDO::ATTR_PERSISTENT] = FALSE;
        parent::__construct( $dsn, $username, $password, $options);
        if (DEBUG_LOG_ALL_SQL_QUERIES) {
            $this->logAllQueries = true;
        }
    }

    public function getTransactionCounter() {
        return $this->transactionCounter;
    }

    // public function beginTransaction()
    public function beginNestedTransaction() {
        if (!$this->transactionCounter++) {
            return parent::beginTransaction();
        }
        // $this->exec('SAVEPOINT trans'.$this->transactionCounter);
        return $this->transactionCounter >= 0;
    }

    // public function commit()
    public function commitNestedTransaction() {
        if (!--$this->transactionCounter) {
            return parent::commit();
        }
        return $this->transactionCounter >= 0;
    }

    // public function rollback()
    public function rollbackNestedTransaction() {
        if (--$this->transactionCounter) {
            // $this->exec('ROLLBACK TO trans'.$this->transactionCounter + 1);
            return true;
        }
        return parent::rollback();
    }
}

