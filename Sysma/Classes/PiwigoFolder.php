<?php

/**
 * Description of PiwigoFolder 
 * 
 * @author srenou
 * 
 */
class PiwigoFolder
{

    public $tabPhotos;

    public function __construct($id, $app)
    {


        if (is_numeric($id) and $id != null) {

            $this->id = $id;
            $this->pdo = $app['pdo.pwg'];

            $i = 0;
            $qry = $this->pdo->prepare("select images.id as id, categories.name as categorie from images,image_category,categories where categories.id = image_category.category_id and images.id = image_category.image_id and category_id = :id order by images.date_creation asc");
            $qry->bindParam(':id', $id, PDO::PARAM_INT);
            $qry->execute();

            if ($qry->rowCount() > 0) {

                while ($ph = $qry->fetchObject()) {

                    $P = new PiwigoPhoto($ph->id, $app);
                    $this->tabPhotos[$i] = $P;
                    $i++;
                    $this->categorie = $ph->categorie;
                }

                $this->titleCode = $this->categorie . '<br><span class="glyphicon glyphicon-folder-open"></span>&nbsp; <a target="_blank" href="' . PWGURL . '/index.php?/category/' . $this->id . '"> Ouvrir le dossier</a><br>';
            }
        }
    }
}
