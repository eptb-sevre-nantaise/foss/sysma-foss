<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Date;

/**
 * Description of Importer 
 * 
 * Importer is the Class to import PG data to Sysma DB.
 * 
 * 
 * 
 * @author srenou
 * 
 */



class Importer
{

    public $tabParamObjet = [
        'sys___sysma_object' => '[SYSMA] Importer comme nom d\'objet Sysma',
        'sys___sysma_object_id' => '[SYSMA] Importer comme identifiant d\'objet Sysma',
        'sys___to_delete' => '[SYSMA] Importer comme colonne de suppression'
    ];

    public $tabParamAction = [/*
        'sysact___sysma_object_temp_id' => '[SYSMA ACTION] Utiliser comme identifiant temporaire d\'objet (même identifiant pour des objets identiques ayant plusieurs actions)',
        'sysact___sysma_action_id' => '[SYSMA ACTION] Importer comme identifiant d\'action Sysma',
        'sysact___sysma_action_temp_id' => '[SYSMA ACTION] Utiliser comme identifiant temporaire d\'action (même identifiant pour des actions identiques sur plusieurs objets)',
        'sysact___sysma_action_type_alias' => '[SYSMA ACTION] Importer comme alias de type d\'action',
        'sysact___sysma_action' => '[SYSMA ACTION] importer comme nom d\'action',
        'sysact___sysma_action_start_date' => '[SYSMA ACTION] importer comme date de début de l\'action',
        'sysact___sysma_action_end_date' => '[SYSMA ACTION] importer comme date de fin de l\'action',
        'sysact___sysma_program_year' => '[SYSMA ACTION] importer comme année d\'engagement de l\'action',
        'sysact___unit_cost_estimated_user' => '[SYSMA ACTION] importer comme coût unitaire prévisionnel',
        'sysact___unit_measure_estimated_user' => '[SYSMA ACTION] importer comme unité de coût',
        'sysact___total_cost_estimated_user' => '[SYSMA ACTION] importer comme coût total prévisionnel',
        'sysact___total_cost_final_user' => '[SYSMA ACTION] importer comme coût total final',
        'sysact___sysma_action_status' => "[SYSMA ACTION] importer comme statut de l'action",
        'sysact___sysma_action_structure_id' => '[SYSMA ACTION] importer comme identifiant de la structure maître d\'ouvrage de l\'action',
        'sysact___sysma_action_contracts' => '[SYSMA ACTION] importer comme liste des contrats de l\'action (séparateur [] )'*/];

    public $report = null ;


    public $noChangeCount = 0;
    public $alreadyImportedCount = 0;
    public $deletedObjectsCount = 0;
    public $createdObjectsCount = 0;
    public $addedValueCount = 0;
    public $updatedValueCount = 0;
    public $modifiedValueCount = 0;

    public $batchCount = 100 ;

    public function __construct($app, $user_schema, $data)
    {
        $this->app = $app;        
        $this->schema = $user_schema;

        //var_dump($data);

        foreach ($data as $key => $val) {
            $this->formVars[$key] = $val;
        }

        // var_dump($this->formVars);
    }


    //////////////////////////////////////////////////////////////////////////////////
    // STEP 1 LAYER TO IMPORT AND METADATA
    //////////////////////////////////////////////////////////////////////////////////

    private function buildImportTableChoiceForm()
    {

        $userLayers = userLayersList($this->schema, $this->app['pdo.export']);
        if (!$userLayers) {
            $message = '<p>Problème d\'accès aux couches du schéma ' . $this->schema . '</p><p>Vérifiez qu\'une couche est présente dans le schéma ' . $this->schema . ' et que les droits d\'accès de l\'utilisateur PostgreSQL à ce schéma sont bien ouverts.</p>';
            $this->report = array('status' => 'error', 'errorsCount' => 1, 'report' => $message);
            return $this->report;
        }

        $builder = $this->app['form.factory']->createBuilder('form', ['step' => 2, 'import_date' => new DateTime('now'), 'schema' => $this->schema])
            ->add('layer_import', 'choice', array('label' => 'Couche à importer', 'choices' => $userLayers, 'multiple' => false, 'constraints' => new Assert\NotBlank()))
            //   ->add('action_import', 'checkbox', array('label' => 'Importer également des fiches actions'))
            ->add('organisation', 'choice', array('choices' => organisationWithUserAccessList($this->app), 'empty_data' => strval($this->app['session']->get('organisation_id')), 'label' => 'Organisation ayant réalisé le relevé', 'constraints' => new Assert\NotBlank()))
            ->add('import_date', 'date', array('label' => 'Date du relevé (ou de l\'import)', 'empty_data' => date('Y-m-d'), 'widget' => 'single_text', 'constraints' => new Assert\NotBlank()))
            //->add('batch_import', 'checkbox', array( 'label' => 'Import en lots (pour les couches lourdes)', 'constraints' => new Assert\NotBlank()))
            ->add('step', 'hidden')
            ->add('schema', 'hidden');



        $form = $builder->getForm();
        return  ['form_view' => $form->createView(), 'message' => null];
    }

    private function addLastVars($form)
    {


        // get vars form the last step in this form
        foreach ($this->formVars as $key => $val) {
            if (!in_array($key, ['app', 'report'])) {
                $form->add($key, 'hidden');
            }
        }

        return  $form;
    }


    //////////////////////////////////////////////////////////////////////////////////
    // STEP 2 TARGET  SYSMA OBJECT TYPE OR NEW THESAURUS
    //////////////////////////////////////////////////////////////////////////////////

    private function buildSysmaObjectTypeChoiceForm()
    {


        // object type list
        $OTList = objectTypesOfGeomTypeList(geoLayerType($this->schema, $this->formVars['layer_import'], $this->app['pdo']), 'gestionnaire',  $this->app);
        $preselectedOT = preSelect($OTList, $this->formVars['layer_import']);
        //var_dump($preselectedOT) ;

        $builder = $this->app['form.factory']->createBuilder('form', $this->formVars + ['sysma_object_type' => $preselectedOT]);
        $form = $builder->getForm();
        $form = $this->addLastVars($form);

        $form->add('sysma_object_type', 'choice', array(
            'label' => 'Type d\'objet Sysma',
            'choices' =>  $OTList + ['new' => 'Créer un nouveau dictionnaire Sysma à partir de la couche SIG'],
            'constraints' => new Assert\NotBlank()
        ));

        $form->add('geometry_type', 'hidden', array('data' => geoLayerType($this->schema, $this->formVars['layer_import'], $this->app['pdo'])));





        return  ['form_view' => $form->createView(), 'message' => null];
    }



    //////////////////////////////////////////////////////////////////////////////////
    // STEP 3 COLUMNS MAPPING FOR EXISTING SYSMA OBJECT TYPE
    //////////////////////////////////////////////////////////////////////////////////


    private function buildMappingColumnsForm()
    {


        $this->loadLayerColumnsList();

        // pour chaque colonne, recherche de valeurs par défaut ;
        $defaultValues = $this->checkDefaultMapping();


        $builder = $this->app['form.factory']->createBuilder('form', array_merge($this->formVars, $defaultValues));
        $form = $builder->getForm();

        $form = $this->addLastVars($form);


        foreach ($this->cols as $col) {

            if ($col['type'] != null) {

                $form->add('import_col_' . $col['name'], 'choice', array('data' => $defaultValues['import_col_' . $col['name']], 'choices' => array_merge(array('0' => 'Ne pas importer'), $this->tabParamObjet, $this->tabParamAction, parameterFromTypeList($col['type'], $this->formVars['sysma_object_type'], 'contributeur', $this->app)), 'label' => 'Importer la colonne ' . strtoupper($col['name']) . ' ? (' . $col['type'] . ')', 'constraints' => new Assert\NotBlank()));
            }
        }

        return ['form_view' =>  $form->createView(), 'message' => null];
    }



    //////////////////////////////////////////////////////////////////////////////////
    // STEP 4 IMPORT INTO THE SYSMA DB
    //////////////////////////////////////////////////////////////////////////////////


    public function import()
    {

        $errorCount = 0;
/*
        $_SESSION['batch_import_offset'] = null ;
        if ($this->formVars['batch_import']==1){
            if ($_SESSION['batch_import_offset']==null) {
                $_SESSION['batch_import_offset'] = 0 ;
            } else {
                $_SESSION['batch_import_offset'] =  $_SESSION['batch_import_offset'] + $this->batchCount ;
            }
        }
      */

        // colonne de la couche à importer
        $this->loadLayerColumnsList();


        // correspondance nom de l'input et nom de la colonne de la couche
        $this->prepareParameters();
        $P = $this->SysToCol;
        //$P2 = $this->ColToInput;


        // vérification que les infos pour l'import des actions sont OK
        /*
        if (isset($this->formVars['action_import'])) {
            $resAction = $this->checkActionParameters();
            if ($resAction['status'] == "error") {
                return json_encode($resAction);
            }
        }
        */

        $Or = new Organisation($this->app,  $this->formVars['organisation']);

        // 01. loading the GIS table

    $layerLines = $this->loadLayerToImport(/*['offset' => $_SESSION['batch_import_offset']]*/);

        // for each line
        foreach ($layerLines as $key => $line) {

            // 02. which operation ?
            if (isset($line[@$P['sys___sysma_object_id']])) {
                $this->report .= 'Objet sysma ' . $line[@$P['sys___sysma_object_id']];

                $to_delete = 0;
                if (@$line[@$P['sys___to_delete']] != null) {
                    $to_delete = 1;
                }
                $todo = verifieObjetPreExistantSysma($line[$P['sys___sysma_object_id']], $to_delete, $this->formVars, $line['mygeom'], $this->formVars['sysma_object_type'], $line, $this->app);
            } else {

                $todo = verifieGeomPreExistantSysma($line['mygeom'], $this->formVars['sysma_object_type'], $this->app);
            }

            //var_dump($todo);


            if ($todo['res'] === 'no_change') {
                $this->noChangeCount++ ;
                $this->report .= '<p class="">Objet ' . $this->formVars['sysma_object_type'] . ' : pas de changement</p>';
            } elseif ($todo['res'] === 'already_imported') {
                $this->alreadyImportedCount++ ;
                $this->report .= '<p class="">' . $todo['count'] . ' objet(s) Sysma de type ' . $this->formVars['sysma_object_type'] . ' et de même géométrie que l\'objet GID ' . @$line['gid'] . ' existe(nt) déjà : pas de changement. Objet(s) existant(s) : ' . $todo['ids'] . '</p>';
            }

            // objet à supprimer (= update de la date de fin)
            elseif ($todo['res'] === 'to_delete') {

                $res = json_decode($this->deleteObject($line[@$P['sys___sysma_object_id']]));

                //var_dump($res);

                if ($res->status === 'success') {
                    $this->deletedObjectsCount++ ;
                    $this->report .= '<p class="text-success">Objet ' . $line[$P['sys___sysma_object_id']]  . ' archivé : date de fin => ' .  $this->formVars['import_date'] . '</p>';
                } else {
                    $this->report .= '<p class="text-danger">Objet ' . $line[$P['sys___sysma_object_id']]  . ' : echec de la mise à jour de la date de fin => ' .  $this->formVars['import_date'] . '</p>';
                    $errorCount++;
                }
            }

            // nouvel objet
            elseif ($todo['res'] === 'new') {

                $res = json_decode($this->createObject($line, $Or->organisation));


                if ($res->status === 'success') {
                    $this->createdObjectsCount++ ;
                    $this->report .= '<p class="text-success"><span class="badge badge-success">Objet ' . $res->id . '</span> créé</p>';

                    // paramètres de l'objet
                    $tabRes2 = $this->createParametersValues($res->id, $line);

                    foreach ($tabRes2 as $id_param => $res2) {

                        $res2 = json_decode($res2);

                        if ($res->status === 'success') {
                           // $this->report .= '<p class="text-success">Objet ' . $res->id . '';
                           // $this->report .= ' - Paramètre ' . $id_param . ' - ';
                            // $this->report .= 'Valeur ajouté : ' . $s->$key . '</p>';
                        } else {

                          // $this->report .= '<p class="text-danger">Objet ' . $id . '';
                          //  $this->report .= ' - Paramètre ' . substr($p, 3) . ' - ';
                            // $this->report .= 'Erreur lors l\'ajout de la value : ' . $s->key . '</p>';
                            $errorCount++;
                        }
                    }
                }
            } elseif ($todo['res'] == 'to_update') {

                foreach ($todo['tabUpdates'] as $tab) {

                    $this->report  .= '<p class="">Objet ' . $line[$P['sys___sysma_object_id']] . '';
                    $this->report  .= ' - Paramètre ' . $tab['parameter_id'] . ' - ';

                    $V = new SysmaObjectParameterValue(null, $this->app);
                    $V->buildFromParameters($tab['sysma_object_id'], $tab['parameter_id'], null, $this->app);

                    // new Value
                    if (!isset($V->data_index)) {


                        $index = dataIndex($this->app['pdo']);

                        $tabV = array();
                        $tabV = [
                            'value' => $tab['value'],
                            'start_date' => $this->formVars['import_date'],
                            'end_date' => '0',
                            'parameter_id' => $tab['parameter_id'],
                            'sysma_object_id' => $tab['sysma_object_id'],
                            'data_index' => $index,
                            'created_at' => date('Y-m-d H:i:s'),
                            'modified_by' => $this->app['session']->get('user_id'),
                            'modified_at' => date('Y-m-d H:i:s'),
                            'user_id' => $this->app['session']->get('user_id')
                        ];

                        if ($V->create($tabV)) {
                            $this->addedValueCount++ ;
                            $this->report .= '<span class="text-success">Valeur ajoutée : ' . $V->value . ' => ' . $tab['value'] . '</span>';
                        } else {
                            $this->report  .= '<span class="text-danger">Echec de l\{ajout de valeur : ' . $V->value . ' => ' . $tab['value'] . '</span>';
                            $errorCount++;
                        }


                        // update or modify value
                    } else {

                        $index = $V->data_index;
                        $Param = new SysmaObjectParameter($V->parameter_id, $this->app);


                        $tabV = array();
                        $tabV = [
                            'value' => $tab['value'],
                            'start_date' => $this->formVars['import_date'],
                            'end_date' => '0',
                            'parameter_id' => $tab['parameter_id'],
                            'sysma_object_id' => $tab['sysma_object_id'],
                            'data_index' => $index,
                            'created_at' => date('Y-m-d H:i:s'),
                            'modified_by' => $this->app['session']->get('user_id'),
                            'modified_at' => date('Y-m-d H:i:s'),
                            'user_id' => $this->app['session']->get('user_id')
                        ];

                        if ($Param->time_tracking == 1) {
                            $this->updatedValueCount++ ;
                            $result = $V->update($tabV);
                            $action = 'Mise à jour';
                        } else {
                            $this->modifiedValueCount++ ;
                            $result = $V->modify($tabV);
                            $action = 'Correction';
                        }


                        if ($result) {
                            $this->report .= '<span class="text-success">' . $action . ' : ' . $V->value . ' => ' . $tab['value'] . '</span>';
                        } else {
                            $this->report  .= '<span class="text-danger">Echec de ' . $action . ' : ' . $V->value . ' => ' . $tab['value'] . '</span>';
                            $errorCount++;
                        }
                    }



                    $this->report  .= '</p>';
                }
            }
        }



        if ($errorCount > 0) {
            $status = 'error';
            $message = 'Erreur lors de l\'import';
        } else {
            $status = 'success';
            $message = '<p class="badge badge-success">Import OK !</p><p>Rapport d \'import : </p>';
        }

        $this->report = "
        Objet inchangés :  $this->noChangeCount <br>
        Objets déjà importés (même geométrie et type d'objet qu'un objet existant) = $this->alreadyImportedCount <br>
        Objets archivés : $this->deletedObjectsCount <br>
        Objets créés : $this->createdObjectsCount <br>
        Valeurs de paramètres ajoutés : $this->addedValueCount <br>
        Valeurs de paramètres mises à jour : $this->updatedValueCount <br>
        Valeurs de paramètres corrigées : $this->modifiedValueCount<br>
        <hr></hr>
        <h3>Rapport détaillé</h3>
        ".$this->report ;

/*
        if ($this->formVars['batch_import']==1){

            JS to load next import batch command

        }
*/
        return json_encode(['status' => $status, 'errorCount' => $errorCount, 'report' => $this->report, 'message' => $message]);
    }



    ////////////////////////////////////////////////////////////////
    // NEW THESAURUS 
    ////////////////////////////////////////////////////////////////


    ////////////////////////////////////////////////////////////////
    // STEP 3 NEW THESAURUS CREATION FORM
    ////////////////////////////////////////////////////////////////


    private function buildSysmaObjectTypeCreationForm()
    {

        $m =  $this->importPrecheck($this->schema, $this->formVars['layer_import']);


        $builder = $this->app['form.factory']->createBuilder('form', $this->formVars);
        $form = $builder->getForm();

        $form = $this->addLastVars($form);


        foreach ($this->precheckResults as $col => $info) {

            if ($info['col_type_estimate_best'] == 'character varying' or $info['col_type_estimate_best'] == 'json' or $info['col_type_estimate_best'] == 'jsonb') {

                $ddef_data = null;
                if ($info['count_distinct_values'] <= 10) $ddef_data = 'textChoiceType';

                $form->add('import_type_' . alias($col), 'choice', array('choices' => ['textType' => 'Champ texte court', 'longTextType' => 'Champ texte long (plusieurs lignes)', 'textChoiceType' => 'Liste de choix (les choix seront créés automatiquement)', 'noImport' => 'Ne pas importer'], 'data' => $ddef_data,  'label' => 'Préciser le type de valeurs de la colonne ' . strtoupper($col) . ' (' . $info['count_distinct_values'] . ' valeurs distinctes)', 'constraints' => new Assert\NotBlank()));
            } elseif ($info['col_type_estimate_best'] == 'integer' or $info['col_type_estimate_best'] == 'bigint') {

                $form->add('import_type_' . alias($col), 'choice', array('choices' => ['integerType' => 'Numérique entier', 'textType' => 'Champt texte court (par défaut)','noImport' => 'Ne pas importer'], 'label' => 'Colonne ' . strtoupper($col), 'constraints' => new Assert\NotBlank()));
            } elseif ($info['col_type_estimate_best'] == 'double precision' or $info['col_type_estimate_best'] == 'NUMERIC') {

                $form->add('import_type_' . alias($col), 'choice', array('choices' => ['floatType' => 'Numérique avec décimale', 'textType' => 'Champt texte court (par défaut)','noImport' => 'Ne pas importer'], 'label' => 'Colonne ' . strtoupper($col), 'constraints' => new Assert\NotBlank()));
            } elseif ($info['col_type_estimate_best'] == 'geometry') {
                //$form->add('import_type_' . alias($col), 'text', array('empty_data' => 'geometry','default' => 'Colonne géométrie', 'constraints' => new Assert\NotBlank()));
            } elseif ($info['col_type_estimate_best'] == 'boolean') {

                $form->add('import_type_' . alias($col), 'choice', array('choices' => ['booleanType' => 'Vrai ou faux (booléen)', 'textType' => 'Champt texte court (par défaut)','noImport' => 'Ne pas importer'], 'label' => 'Colonne ' . strtoupper($col), 'constraints' => new Assert\NotBlank()));
            } else {
                $form->add('import_type_' . alias($col), 'choice', array('choices' => ['textType' => 'Champt texte court (par défaut)', 'noImport' => 'Ne pas importer'], 'label' => 'Colonne ' . strtoupper($col), 'constraints' => new Assert\NotBlank()));
            }
        }





        // prepare form
        $geoLayerType = geoLayerType($this->schema, $this->formVars['layer_import'], $this->app['pdo']);


        $form->add('confirm_creation', 'choice', array('choices' => ['yes' => 'Oui', 'no' => 'Non'], 'label' => 'Confirmez-vous la création d\'un nouveau type d\'objet Sysma ? (' . $geoLayerType . ')', 'constraints' => new Assert\NotBlank()));


        if (strpos(strtolower($geoLayerType),'multi')!== false) {
            $m['message'] = '<p class="text-danger text-center">Attention, l\'import des géométries multiples sera partiel (seul la première entité géographique d\'un multi objet sera importé)</p>'.$m['message'] ;
        }

        return ['form_view' =>  $form->createView(), 'message' => $m['message']];
    }



    ///////////////////////////////////////////////////////////////////////
    // STEP 4 SYSMA NEW THESAURUS FROM GIS TABLE AUTOMATIC CREATION
    ///////////////////////////////////////////////////////////////////////

    public function createNewSysmaObjectTypeFromGISTable()
    {


     

        $this->report = null;

        // 1. create the sysma object TYPE
        $data['sysma_object_type'] = 'Import_' . $this->formVars['layer_import'] . '_' . date('YmdHis');
        $data['sysma_object_type_alias'] = alias($this->formVars['layer_import']). '_' . date('YmdHis');

        $data['created_at'] = $this->formVars['import_date'];
        $data['modified_by'] = $this->app['session']->get('user_id');
        $data['style'] = '{"color":"#ff0000","fillColor":"#f3bcbc","weight":"2.5","opacity":"1","fillOpacity":"0.2","dashArray":"0","label":"","labelColor":"#000000","labelFillColor":"#ccf6cc","labelSize":"10","labelOpacity":"1","labelFillOpacity":"1","labelFillRadius":"5"}';
        $data['digitization_layer'] = 'aucun';
        $Or = new Organisation($this->app,  $this->formVars['organisation']);
        $data['data_source'] = $Or->organisation;

        // 1.1 geometry type

        $typegeom = $this->formVars['geometry_type'];
        $typegeom === 'ST_LineString' ? $typegeom = 'ST_Linestring' : null;
        $typegeom === 'ST_MultiLineString' ? $typegeom = 'ST_Linestring' : null;
        $typegeom === 'ST_MultiPolygon' ? $typegeom = 'ST_Polygon' : null;
        $data['geometry_type'] = $typegeom;

        //     var_dump($data);

        // die() ;


        $OT = new SysmaObjectType(null, $this->app);

        $res = json_decode($OT->create($data));
        //return $res->id ;
        if ($res->status !== 'success') return 'Erreur lor de la création du type d\'objet';


        $OT = new SysmaObjectType($res->id, $this->app);

        $this->report .= '<p><span class="glyphicon glyphicon-ok" aria-hidden="true"></span>  Nouveau type d\'objet <b>' . $OT->sysma_object_type . '</b> [' . $OT->sysma_object_type_id . '] créé</p>';



        // 2. parameters
        $this->loadLayerColumnsList();



        foreach ($this->cols as $col) {

            if ($this->formVars['import_type_' . alias($col['name'])] !== 'noImport') {
                $P = new SysmaObjectParameter(null, $this->app);
                $data2['sysma_object_type_id'] = $res->id;
                $data2['parameter'] = $col['name'];

                $data2['created_at'] = $this->formVars['import_date'];
                $data2['parameter_alias'] = alias($col['name']);

                $data2['modified_by'] = $this->app['session']->get('user_id');
                $data2['data_source'] = $Or->organisation;


                $data2['data_type'] = $this->formVars['import_type_' . alias($col['name'])];

                $res2 = json_decode($P->create($data2));
                if ($res2->status !== 'success') die('Erreurs lors de la création du parametre ' . $col['name']);




                $P = new SysmaObjectParameter($res2->id, $this->app);

                $this->report .= '<p><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Nouveau paramètre <b>' . $P->parameter . '</b> [' . $P->parameter_id . '] créé</p>';

                // OK pour la création des paramètres
                if ($this->formVars['import_type_' . alias($col['name'])] == 'textChoiceType') {

                    $vals = array();
                    // création des choix 
                    $vals = loadDistinctColumnValues($this->schema, $this->formVars['layer_import'], $col['name'], $this->app['pdo']);
                    $count = 0;

                    foreach ((array) $vals as $val) {

                        $res3 = json_decode($P->createChoice(['choice' => $val, 'description' => null, 'display_order' => $count, 'parameter_id' => $P->parameter_id, 'modified_by' => $this->app['session']->get('user_id'), 'value_type' => 'choice']));
                        $count++;

                        if ($res3->status != 'success') die('Erreurs lors de la création du choix ' . $col['name'] . ' ' . $val);

                        $this->report .= '<p><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Nouveau choix <b>' . $val . '</b> créé</p>';
                    }
                }
            }
        }

        // message

        $msg = '<p><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Le  nouveau type d\'objet <a href="' . $this->app['url_generator']->generate('sysma-object-type', ['id' => $OT->sysma_object_type_id]) . '">Import_' . $this->formVars['layer_import'] . '_' . date('YmdHis') . ' [' . $OT->sysma_object_type_id . ']</a> a bien été ajouté au dictionnaire</p>';

        $now = (new \DateTime())->format('Y-m-d');
        $msg .= '<p>Vous pouvez maintenant <a href="' . $this->app['url_generator']->generate('import-gis') . '?step=3&layer_import=' . $this->formVars['layer_import'] . '&sysma_object_type=' . $OT->sysma_object_type_id . '&schema=' . $this->schema . '&organisation=' . $this->formVars['organisation'] . '&import_date=' . $now . '&geometry_type=' . $this->formVars['geometry_type'] . '"><b>lancer l\'import des données</b></a></p>';





        return json_encode(array('status' => 'success', 'errorCount' => 0, 'report' => $this->report, 'message' => $msg));
    }







    /////////////////////////////////////////////
    // UTILS
    /////////////////////////////////////////////

    // columns of the table to import 
    private function loadLayerColumnsList()
    {
        $this->cols = layerColumnsList($this->schema, $this->formVars['layer_import'], $this->app['pdo']);
    }

    private function debug()
    {
        $this->app = null;
        // var_dump($this);
    }


    public function buildForm()
    {

        if (!isset($this->formVars['step']) or $this->formVars['step'] == null) {
            //echo 'step 0';
        } else {
            // next step for forms
            $step =  $this->formVars['step'];
            $this->formVars['step']++;

            if ($step == '1') {
                //echo 'step 1';
                return $this->buildImportTableChoiceForm();
            } else if ($step == '2') {
                //echo 'step 2';
                return $this->buildSysmaObjectTypeChoiceForm();
            } else if ($step == '3') {
                // echo 'step 3';
                if ($this->formVars['sysma_object_type'] == 'new') {
                    return $this->buildSysmaObjectTypeCreationForm();
                } else {
                    return $this->buildMappingColumnsForm();
                }
            }
        }
    }


    /*
    * Infos about the data to import
    */


    private function importPrecheck($schema, $table)
    {

        $q = '
        with cols as (
            SELECT * from xx_99_utils.f_table_columns_list (:schema, :table)
            )
            SELECT tbl, col,  xx_99_utils.f_column_scan_analyse_content (
               (\'{"schemaname" : "' . $schema . '"
             , "tablename" : "' . $table . '"
             , "column_name" : "\'|| col ||\'"}\'::text) :: json
             )
             from cols';

        //echo $q;



        $qry = $this->app['pdo']->prepare($q);
        $qry->bindParam(':schema', $schema);
        $qry->bindParam(':table', $table);
        if ($qry->execute()) {

            $m = '<h2>Analyse du contenu de la table</h2>';

            $m .= '<table class="table"><tr><td>Test</td><tr></table>';

            $m .= '<table class="table table-condensed table-striped table-bordered"><thead><tr><th>Colonne</th><th>Typage proposé</th><th>Nombre de valeurs distinctes</th><th>Liste des valeurs distinctes</th></tr></thead><tbody>';

            while ($row = $qry->fetchObject()) {



                $json = json_decode($row->f_column_scan_analyse_content);


                if ($json->return_analyse_detail->col_type_estimate_best == 'character varying') {
                    $t[$row->col] = ['col_type_estimate_best' => $json->return_analyse_detail->col_type_estimate_best, 'count_distinct_values' => $json->return_analyse_detail->analyse_result->count_distinct_values, 'list_values_and_count' => $json->return_analyse_detail->analyse_result->list_values_and_count];
                } else {
                    $t[$row->col] = ['col_type_estimate_best' => $json->return_analyse_detail->col_type_estimate_best];
                }


                $m .= '<tr><td>' . $row->col . '</td>';
                $m .= '<td>' . $json->return_analyse_detail->col_type_estimate_best . '</td>';
                if ($json->return_analyse_detail->col_type_estimate_best == 'character varying') {
                    $m .= '<td>' . $json->return_analyse_detail->analyse_result->count_distinct_values . '</td>';
                    //     $m .= '<td><input type="checkbox" value="1" name="form_is_choice_type_' . alias($row->col) . '" /> Créer un champ de type "liste de choix" à partir des valeurs de la table ? </td>';
                    $m .= '<td title="' . $json->return_analyse_detail->analyse_result->list_values_and_count . '">' . substr($json->return_analyse_detail->analyse_result->list_values_and_count, 0, 50) . '...</td>';
                } else {
                    $m .= '<td></td><td></td>';
                }
                $m .= '</tr>';
            }

            $m . '</tbody></table>';

            $this->precheckResults = $t;
        } else {
            return 'erreur sql';
        }


        return ['message' => $m];
    }




    private function deleteObject($id)
    {

        // on update la date de fin de l'objet
        $O = new SysmaObject($id, $this->app);
        $tab = array();
        $tab = array_merge($tab, array('sysma_object_id' => $id));
        $tab = array_merge($tab, array('end_date' => $this->formVars['import_date']));
        $res = $O->update($tab);
        return $res;
    }


    private function createParametersValues($sysma_object_id, $line)
    {


        $tabRes = [];


        // insertion des données
        foreach ($this->ColToInput as $key => $p) {

            //  echo $p ;

            if (substr($this->formVars[$p], 0, 6) == 'idp___' and $line[$key] !== null and $line[$key] !== '') {


                $tabV = array();
                $tabV = array_merge($tabV, array('value' => $line[$key]));
                $tabV = array_merge($tabV, array('start_date' => $this->formVars['import_date']));
                $tabV = array_merge($tabV, array('end_date' => '0'));
                $tabV = array_merge($tabV, array('parameter_id' => substr($this->formVars[$p], 6)));
                $tabV = array_merge($tabV, array('sysma_object_id' => $sysma_object_id));
                $tabV = array_merge($tabV, array('data_index' => dataIndex($this->app['pdo'])));
                $tabV = array_merge($tabV, array('modified_by' => $this->app['session']->get('user_id')));
                $tabV = array_merge($tabV, array('user_id' => $this->app['session']->get('user_id')));
                $tab['value'] = explode(' [] ', $tabV['value']);


                $V = new SysmaObjectParameterValue(null, $this->app);

                if (count($tab['value']) == 1) {
                    $res2 = json_decode($V->create($tabV));
                    if ($res2->status !== 'success') {                        
                        $this->report .= '<p class="text-danger">Valeur ' . $key.' : '.$tabV['value'] . ' Erreur</p>';
                    } else {
                        $this->addedValueCount++ ;
                        $this->report .= '<p class="text-success">Valeur <i>' . $key.' : '.$tabV['value'] . '</i> créée</p>';
                    }
                } elseif (count($tab['value']) > 1) {
                    $index = dataIndex($this->app['pdo']);
                    foreach ($tab['value'] as $value) {
                        $this->addedValueCount++ ;
                        $tabToInsert = $tabV;
                        $tabToInsert['data_index'] = $index;
                        $tabToInsert['value'] = $value;
                        $res2 = json_decode($V->create($tabToInsert));
                        if ($res2->status !== 'success') {                            
                            $this->report .= '<p class="text-danger">Valeur ' . $tabV['value'] . ' Erreur</p>';
                        } else {
                            $this->addedValueCount++;
                            $this->report .= '<p class="text-success">Valeur <i>' . $key .' : '.$tabV['value'] . '</i> créée</p>';
                        }
                    }
                }

                $tabRes[substr($p, 6)] = json_encode($res2);
            }
        }

        return $tabRes;
    }


    private function createObject($line, $organisation)
    {


        // création de l'objet
        $id = dataIndex($this->app['pdo']);
        $O = new SysmaObject(null, $this->app);
        // tableau des paramètres de créations de l'objet
        $tab = array();
        if (isset($line[@$this->SysToCol['sys___sysma_object']])) {
            $tab = array_merge($tab, array('sysma_object' => $line[$this->SysToCol['sys___sysma_object']]));
        }
        $tab = array_merge($tab, array('sysma_object_type_id' => $this->formVars['sysma_object_type']));
        $tab = array_merge($tab, array('geom' => $line['mygeom']));
        $tab = array_merge($tab, array('start_date' => $this->formVars['import_date']));
        $tab = array_merge($tab, array('end_date' => '0'));
        $tab = array_merge($tab, array('status' => 'existant'));
        $tab = array_merge($tab, array('organisation_id' => $this->formVars['organisation']));
        $tab = array_merge($tab, array('modified_by' => $this->app['session']->get('user_id')));
        $tab = array_merge($tab, array('created_by' => $this->app['session']->get('user_id')));
        $tab = array_merge($tab, array('sysma_object_id' => $id));

        return $O->create($tab);
    }


    private function loadLayerToImport($options = null)
    {

        $cond = null ;
        /*
        if (isset($options['offset'])) {
            $cond = ' offset '.$options['offset'].' limit '.($options['offset']+$this->batch_count) ;
        }
*/
        $qry = $this->app['pdo']->prepare('select *,(ST_Dump(ST_Force2D(ST_CollectionExtract(geom,:typeg)))).geom as mygeom from ' . $this->schema . '."' . $this->formVars['layer_import'].'" '.$cond);
        $typeg = typeGeoIntegerPG(geoLayerType($this->schema, $this->formVars['layer_import'], $this->app['pdo']));
        $qry->bindParam(':typeg', $typeg, PDO::PARAM_INT);
        $qry->execute();
        return $qry->fetchAll();
    }

    private function prepareParameters()
    {

        // column name > input name
        $this->ColToInput = array();
        // sys name > column name
        $this->SysToCol = array();

        // tab paramètres cibles
        // nom de la colonne de la table à importer -> post value récupérée

        foreach ($this->cols as $col) {
            if ($col['type'] != null) {
                $colName = 'import_col_' . $col['name'];
                $this->ColToInput[$col['name']] = $colName;

                foreach ($this->tabParamObjet as $key => $p) {
                    if ($this->formVars[$colName] == $key) {
                        $this->SysToCol[$key] = $col['name'];
                    }
                }
                foreach ($this->tabParamAction as $key => $p) {
                    if ($this->formVars[$colName] == $key) {
                        $this->SysToCol[$key] = $col['name'];
                    }
                }
            }
        }
    }


    private function checkDefaultMapping()
    {

        $defaultValues = null;
        foreach ($this->cols as $col) {

            if ($col['type'] != null) {

                // preselection si la colonne est du même nom que l'alias sysma (col_sig)

                $id_param_preselec = null;
                if ($param = parameterFromAlias('sysma_object', $this->formVars['sysma_object_type'], $col['name'], $this->app)) {
                    $id_param_preselec = 'idp___' . $param->parameter_id;
                } elseif (in_array($col['name'], ['id_objetge', 'sysma_object_id', 'id_objetgeo',  'sysma_obje'])) {
                    $id_param_preselec = 'sys_sysma_object_id';
                } elseif (in_array($col['name'], ['sysma_object', 'objetgeo', 'objet'])) {
                    $id_param_preselec = 'name';
                } elseif ($col['name'] === 'suppr') {
                    $id_param_preselec = 'sys_suppr';
                } elseif ($col['name'] === 'delete') {
                    $id_param_preselec = 'sys_suppr';
                }

                foreach ($this->tabParamObjet as $key => $p) {
                    if ($col['name'] == substr($key, 6)) {
                        $id_param_preselec = $key;
                    }
                }

                if (isset($this->formVars['action_import'])) {
                    foreach ($this->tabParamAction as $key => $p) {
                        if ($col['name'] == substr($key, 9)) {
                            $id_param_preselec = $key;
                        }
                    }
                }

                $defaultValues['import_col_' . $col['name']] = $id_param_preselec;
            }
        }

        return $defaultValues;
    }





    private function checkActionParameters()
    {



        $paramPrepActionIsOk = 0;

        // preparation des colonnes utiles

        //$this->debug();


        // pour chaque paramètre action necessaire
        foreach ($this->tabParamAction as $key_param_action => $desc_param_action) {
            // pour chaque colonne et sa correspondance
            foreach ($this->formVars as $input_name => $input_value) {

                if ($input_value == $key_param_action) {
                    $paramPrepActionIsOk++;
                }
            }
        }



        // tout est OK pour traiter la fiche action
        // = autant d'infos que nécessaires
        if ($paramPrepActionIsOk < count($this->tabParamAction)) {
            return array('status' => 'error', 'errorCount' => 1, 'report' => 'Champs manquants pour l\'import des actions', 'message' => 'Champs manquants pour l\'import des actions');
        } else {
            return array('status' => 'success', 'errorCount' => 0, 'report' => '', 'message' => '');
        }
    }
}
