<?php

/**
 * Description of Filter
 * 
 * Filters allow user to customize the layers display 
 * 
 * @author srenou
 * 
 */
class SysmaFilter
{

    public function __construct($id, $app)
    {

        $this->pdo = $app['pdo'];
        if ($id != null) {

            $vars = buildSimpleEntity('filter', 'filter_id', $id, $app);

            foreach ($vars as $key => $value) {
                $this->$key = $value;
            }
        }
    }

    public function activate($id_util, $sysma_object_type_id, $sysma_action_type_id)
    {

        $this->desactivate($id_util, $sysma_object_type_id, $sysma_action_type_id);


        $qry = $this->pdo->prepare('insert into ' . SCHEMA . '.l_filter_user (filter_id,user_id, sysma_object_type_id, sysma_action_type_id) VALUES (:filter_id,:id_util, :idto, :idtt)');
        $qry->bindParam(':filter_id', $this->filter_id, PDO::PARAM_INT);
        $qry->bindParam(':id_util', $id_util, PDO::PARAM_INT);
        $qry->bindParam(':idto', $sysma_object_type_id, PDO::PARAM_INT);
        $qry->bindParam(':idtt', $sysma_action_type_id, PDO::PARAM_INT);
        return $qry->execute();
    }

    public function activateWithCondition($id_util, $sysma_object_type_id, $sysma_action_type_id, $cond)
    {

        //$this->desactivateOtherFilters($id_util, $app);
        $this->desactivate($id_util, $sysma_object_type_id, $sysma_action_type_id);


        $qry = $this->pdo->prepare('insert into ' . SCHEMA . '.l_filter_user (filter_id,user_id, sysma_object_type_id, sysma_action_type_id, condition) VALUES (:filter_id,:id_util, :idto, :idtt, :cond)');
        $qry->bindParam(':filter_id', $this->filter_id, PDO::PARAM_INT);
        $qry->bindParam(':id_util', $id_util, PDO::PARAM_INT);
        $qry->bindParam(':idto', $sysma_object_type_id, PDO::PARAM_INT);
        $qry->bindParam(':idtt', $sysma_action_type_id, PDO::PARAM_INT);
        $qry->bindParam(':cond', $cond, PDO::PARAM_STR);
        return $qry->execute();
    }

    public function desactivate($id_util, $sysma_object_type_id, $sysma_action_type_id)
    {
        $qry = $this->pdo->prepare('delete from ' . SCHEMA . '.l_filter_user where filter_id = :filter_id and user_id = :id_util and sysma_object_type_id = :idto and sysma_action_type_id = :idtt');
        $qry->bindParam(':filter_id', $this->filter_id, PDO::PARAM_INT);
        $qry->bindParam(':id_util', $id_util, PDO::PARAM_INT);
        $qry->bindParam(':idto', $sysma_object_type_id, PDO::PARAM_INT);
        $qry->bindParam(':idtt', $sysma_action_type_id, PDO::PARAM_INT);
        return $qry->execute();
    }

    public static function deleteFiltersLinkedToObjectType($sysma_object_type_id, $app)
    {
        $qry = $app['pdo']->prepare('DELETE FROM ' . SCHEMA . '.l_filter_user where sysma_object_type_id = :idto');
        $qry->bindParam(':idto', $sysma_object_type_id, PDO::PARAM_INT);
        return $qry->execute();
    }


    public function desactivateForAllUsers()
    {
        $qry = $this->pdo->prepare('delete from ' . SCHEMA . '.l_filter_user where filter_id = :filter_id');
        $qry->bindParam(':filter_id', $this->filter_id, PDO::PARAM_INT);
        return $qry->execute();
    }

    public static function deleteFiltersLinkedToActionType($sysma_action_type_id, $app)
    {
        $qry = $app['pdo']->prepare('DELETE FROM ' . SCHEMA . '.l_filter_user where sysma_action_type_id = :idtt');
        $qry->bindParam(':idtt', $sysma_action_type_id, PDO::PARAM_INT);
        return $qry->execute();
    }

    public function desactivateOtherFilters($id_util, $sysma_object_type_id, $sysma_action_type_id)
    {
        $qry = $this->pdo->prepare('delete from ' . SCHEMA . '.l_filteruser where filter_id != :filter_id and user_id = :id_util and sysma_object_type_id = :idto and sysma_action_type_id = :idtt');
        $qry->bindParam(':filter_id', $this->filter_id, PDO::PARAM_INT);
        $qry->bindParam(':id_util', $id_util, PDO::PARAM_INT);
        $qry->bindParam(':idto', $sysma_object_type_id, PDO::PARAM_INT);
        $qry->bindParam(':idtt', $sysma_action_type_id, PDO::PARAM_INT);
        return $qry->execute();
    }

    public function insert($request)
    {

        $pp = prepInsertQuery($request);
        $qry = $this->pdo->prepare("insert into " . SCHEMA . ".filter (" . $pp['reqFields'] . ") VALUES (" . $pp['reqParams'] . ") returning filter_id as id");

        foreach ($pp['tabD'] as $key => $val) {
            $qry->bindValue($key, $val);
        }

        return returnResultQuery($qry, $this->pdo, $pp['tabD']);
    }

    public function update($request)
    {

        $pp = prepUpdateQuery($request, 'filter_id');


        $qry = $this->pdo->prepare("update " . SCHEMA . ".filter set " . $pp['req'] . " where filter_id = :filter_id");

        foreach ($pp['tabD'] as $key => $val) {
            $qry->bindValue($key, $val);
        }

        return returnResultQuery($qry, $this->pdo, $pp['tabD']);
    }

    public function updateCondition($cond, $sysma_object_type_id, $sysma_action_type_id, $id_util)
    {


        $qry = $this->pdo->prepare("update " . SCHEMA . ".l_filter_user set condition = :cond where filter_id = :filter_id and sysma_object_type_id = :idto and sysma_action_type_id = :idtt and user_id = :id_util");
        $qry->bindParam(':filter_id', $this->filter_id, PDO::PARAM_INT);
        $qry->bindParam(':id_util', $id_util, PDO::PARAM_INT);
        $qry->bindParam(':idto', $sysma_object_type_id, PDO::PARAM_INT);
        $qry->bindParam(':idtt', $sysma_action_type_id, PDO::PARAM_INT);
        $qry->bindParam(':cond', $cond, PDO::PARAM_STR);
        return $qry->execute();
    }



    public function delete()
    {

        if ($this->desactivateForAllUsers()) {
            $qry = $this->pdo->prepare("delete from " . SCHEMA . ".filter where filter_id = :id");
            $qry->bindParam(':id', $this->filter_id, PDO::PARAM_INT);
            return $qry->execute();
        } else {
            return false;
        }
    }
}
