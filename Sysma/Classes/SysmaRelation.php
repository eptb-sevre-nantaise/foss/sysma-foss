<?php

/**
 * Description of SysmaRelation
 * 
 * relation between objects
 *
 * @author srenou
 * 
 */
class SysmaRelation
{

    public function __construct($id, $app)
    {

        $this->app = $app;
        $this->pdo = $app['pdo'];

        if (is_numeric($id) and $id != null) {

            $qry = $this->pdo->prepare('select *, array_to_json(r.contracts) as tab_contracts '
                . ' from ' . SCHEMA . '.sysma_relation r '
                . ' inner join ' . SCHEMA . '.sysma_relation_type rt on rt.sysma_relation_type_id = r.sysma_relation_type_id '
                . ' inner join ' . SCHEMA . '.organisation o on o.organisation_id = r.organisation_id '
                . ' where sysma_relation_id = :id');

            $qry->bindParam(':id', $id, PDO::PARAM_INT);
            $qry->execute();

            if ($qry->rowCount() > 0) {

                $ob = $qry->fetchObject();
                $this->id = $id;

                foreach ($ob as $key => $val) {
                    $this->$key = $val;
                }

                $this->Status = new Status('sysma_relation', $this->status, $this->app);
                $this->tabC = json_decode($ob->tab_contracts);
                $this->loadContracts();


                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private function loadContracts()
    {

        $compt = 0;

        $this->contract = array();
        if ($this->tabC != null) {
            foreach ($this->tabC as $c) {
                $this->contract[$c] = true;
            }
        }
        $this->contracts_labels = null;
        $this->tabcontracts = array();
        foreach (contracts($this->app) as $ctr) {
            if (array_key_exists($ctr['contract'], $this->contract)) {
                $this->tabcontractsVal[$ctr['contract']] = true;
                $this->contracts_labels .= '<span class="contract" >' . str_replace(' ', '&nbsp;', $ctr['contract']) . '</span> ';
            } else {
                $this->tabcontractsVal[$ctr['contract']] = false;
            }
            $this->tabcontracts[$compt]['id'] = $ctr['contract_id'];
            $this->tabcontracts[$compt]['contract'] = $ctr['contract'];
            $compt++;
        }
    }

    public function create($request)
    {

        $pp = prepInsertQuery($request);
        $qry = $this->pdo->prepare("insert into " . SCHEMA . ".sysma_relation (" . $pp['reqFields'] . ") VALUES (" . $pp['reqParams'] . ") RETURNING sysma_relation_id as id");
        foreach ($pp['tabD'] as $key => $val) {
            $qry->bindValue($key, $val);
        }
        return returnResultQuery($qry, $this->pdo, $pp['tabD']);
    }

    public function update($request)
    {


        $pp = prepUpdateQuery($request, 'sysma_relation_id');
        $qry = $this->pdo->prepare("update " . SCHEMA . ".sysma_relation set " . $pp['req'] . " where sysma_relation_id = :sysma_relation_id");
        foreach ($pp['tabD'] as $key => $val) {
            $qry->bindValue($key, $val);
        }
        return returnResultQuery($qry, $this->pdo, $pp['tabD']);
    }

    public function loadSysmaRelationTypeSysmaObjectTypeInfos($sysma_object_type_id)
    {

        $this->RLT_OT_Infos = null;
        $qry = $this->pdo->prepare('select '
            . ' *, case when (sysma_object_type_id_parent = :ido) then \'parent\' else \'child\' end as parent_child'
            . ' from ' . SCHEMA . '.l_sysma_object_type_sysma_relation_type r '
            . ' where sysma_relation_type_id = :id'
            . ' and (sysma_object_type_id_parent = :ido or sysma_object_type_id_child = :ido)');

        $qry->bindParam(':id', $this->sysma_relation_type_id, PDO::PARAM_INT);
        $qry->bindParam(':ido', $sysma_object_type_id, PDO::PARAM_INT);
        $qry->execute();
        $this->RLT_OT_Infos = $qry->fetchAll(PDO::FETCH_ASSOC);
        //var_dump($this->RLT_OT_Infos) ;
    }

    public function loadActualInfosSysmaRelation()
    {

        $Typ = new SysmaRelationType($this->sysma_relation_type_id, $this->app);
        $Typ->loadParameters($this->app);
        $this->data = array();

        // for each parameter
        for ($i = 0; $i < count($Typ->paramArrayValue); $i++) {

            $P = new SysmaRelationParameter($Typ->paramArrayValue[$i], $this->app);
            $Val = new SysmaRelationParameterValue(null, $this->app);
            $Val->buildFromParameters($this->sysma_relation_id, $P->parameter_id, $this->app);
            array_push($this->data, $Val);
        }
    }

    public function loadActualInfosSysmaRelationWithIndex()
    {

        $Typ = new SysmaRelationType($this->sysma_object_type_id, $this->app);
        $Typ->loadParameters($this->app);
        $this->data = array();

        // for each parameter
        for ($i = 0; $i < count($Typ->paramArrayValue); $i++) {

            $P = new SysmaRelationParameter($Typ->paramArrayValue[$i], $this->app);
            $Val = new SysmaRelationParameterValue();
            $Val->buildFromParameters($this->id, $P->parameter_id, '', $this->app);
            $this->dataIndex[$P->parameter_alias] = $Val;
        }
    }

    public function loadSysmaObjectsLinked($parent_child)
    {



        $qry = $this->pdo->prepare('select * from ' . SCHEMA . '.l_sysma_object_sysma_relation r '
            . ' inner join ' . SCHEMA . '.sysma_object o on r.sysma_object_id = o.sysma_object_id '
            . ' inner join ' . SCHEMA . '.sysma_object_type t on t.sysma_object_type_id = o.sysma_object_type_id '
            . ' where r.sysma_relation_id = :id and r.parent_child = :pc'
            . ' ');

        $qry->bindParam(':id', $this->sysma_relation_id, PDO::PARAM_INT);
        $qry->bindParam(':pc', $parent_child, PDO::PARAM_STR);
        $qry->execute();
        return $qry->fetchAll(PDO::FETCH_ASSOC);
    }

    public function addSysmaRelationLink($sysma_object_id, $parent_child, $modified_by)
    {



        $this->unlinkSysmaRelation($sysma_object_id, $parent_child);

        $qry = $this->pdo->prepare("insert into " . SCHEMA . ".l_sysma_object_sysma_relation (sysma_object_id, parent_child,sysma_relation_id,modified_by) VALUES (:sysma_object_id,:parent_child,:sysma_relation_id,:modified_by)");
        $qry->bindParam(':sysma_object_id', $sysma_object_id, PDO::PARAM_INT);
        $qry->bindParam(':parent_child', $parent_child, PDO::PARAM_STR);
        $qry->bindParam(':sysma_relation_id', $this->sysma_relation_id, PDO::PARAM_INT);
        $qry->bindParam(':modified_by', $modified_by, PDO::PARAM_INT);
        return $qry->execute();
    }

    /*
     * delete the link between the object and a worksheet
     * @param integer $sysma_action_id    
     * @return boolean
     */

    public function unlinkSysmaRelation($sysma_object_id, $parent_child)
    {
        $qry = $this->pdo->prepare("delete from " . SCHEMA . ".l_sysma_object_sysma_relation where sysma_object_id = :sysma_object_id and  parent_child = :parent_child and sysma_relation_id = :sysma_relation_id");
        $qry->bindParam(':sysma_object_id', $sysma_object_id, PDO::PARAM_INT);
        $qry->bindParam(':parent_child', $parent_child, PDO::PARAM_STR);
        $qry->bindParam(':sysma_relation_id', $this->sysma_relation_id, PDO::PARAM_INT);
        return $qry->execute();
    }

    public function addContract($contract)
    {


        $qry = $this->pdo->prepare('update ' . SCHEMA . '.sysma_relation set contracts = array_append(contracts, :contract) where sysma_relation_id = :sysma_relation_id');
        $qry->bindParam(':contract', $contract, PDO::PARAM_STR);
        $qry->bindParam('sysma_relation_id', $this->sysma_relation_id, PDO::PARAM_INT);
        return $qry->execute();
    }

    public function deleteContracts()
    {


        $qry = $this->pdo->prepare('update ' . SCHEMA . '.sysma_relation set contracts = null where sysma_relation_id = :sysma_relation_id');
        $qry->bindParam('sysma_relation_id', $this->sysma_relation_id, PDO::PARAM_INT);
        return $qry->execute();
    }

    public function deleteData()
    {


        $qry = $this->pdo->prepare("delete from " . SCHEMA . ".sysma_relation_data where sysma_relation_id = :sysma_relation_id");
        $qry->bindParam(':sysma_relation_id', $this->sysma_relation_id, PDO::PARAM_INT);
        return $qry->execute();
    }

    public function delete()
    {

        $this->deleteData();

        $qry = $this->pdo->prepare("delete from " . SCHEMA . ".sysma_relation where sysma_relation_id = :sysma_relation_id");
        $qry->bindParam(':sysma_relation_id', $this->sysma_relation_id, PDO::PARAM_INT);
        return $qry->execute();
    }
}
