<?php


class OtherLayer
{

    public function __construct($id, $app)
    {

        $this->pdo = $app['pdo'];
        if (is_numeric($id) and $id != null) {

            $ol = buildSimpleEntity('other_layer', 'other_layer_id', $id, $app);
            foreach ($ol as $key => $val) {
                $this->$key = $val;
            }
        }
    }


    public function update($request)
    {
        $pp = prepUpdateQuery($request, 'other_layer_id');

        $qry = $this->pdo->prepare("update " . SCHEMA . ".other_layer set " . $pp['req'] . " where other_layer_id = :other_layer_id");
        foreach ($pp['tabD'] as $key => $val) {
            $qry->bindValue($key, $val);
        }
        return returnResultQuery($qry, $this->pdo, $pp['tabD']);
    }


    public function create($request)
    {
        $pp = prepInsertQuery($request);
        $qry = $this->pdo->prepare("insert into " . SCHEMA . ".other_layer (" . $pp['reqFields'] . ") VALUES (" . $pp['reqParams'] . ") RETURNING other_layer_id");
        foreach ($pp['tabD'] as $key => $val) {
            $qry->bindValue($key, $val);
        }
        return returnResultQuery($qry, $this->pdo, $pp['tabD']);
    }

    public function delete()
    {
        $qry = $this->pdo->prepare("delete from " . SCHEMA . ".other_layer where other_layer_id = :id");
        $qry->bindParam(':id', $this->other_layer_id, PDO::PARAM_INT);
        return $qry->execute();
    }
}
