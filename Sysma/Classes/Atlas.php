<?php

/**
 * Description of Atlas
 *  * 
 * WORK IN PROGRESS
 * 
 * @author srenou 
 */
class Atlas
{

    public function __construct($table, $app)
    {

        $this->app = $app;
        if ($table != null) {
            $this->table = $table;
        }
    }

    public function loadPlanches()
    {

        $q = '
select gid,atlas_name, atlas_date, id_publi_post, planche_num, planche_nombre_total, sysma_datas,geom_planche_enveloppe, geom_planche_objet 
from sysma_atlas.' . $this->table . '  order by planche_num asc';


        $qry = $this->app['pdo']->prepare($q);

        $qry->execute();
        $res = $qry->fetchAll();


        foreach ($res as $id => $planche) {
            foreach ($planche as $key => $val) {
                $this->tabPlanches[$planche['planche_num']][$key] = $val;
            }
        }
    }
}
