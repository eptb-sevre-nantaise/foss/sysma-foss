<?php

/**
 * Description of SysmaObjectParameter
 * 
 * @author srenou
 * 
 */



class SysmaObjectParameter
{

    public function __construct($id, $app)
    {

        $this->app = $app;
        $this->pdo = $app['pdo'];
        if (is_numeric($id) and $id != null) {

            $qry = $this->pdo->prepare("select * from " . SCHEMA . ".sysma_object_type_parameter where parameter_id = :id");
            $qry->bindParam(':id', $id, PDO::PARAM_INT);
            $qry->execute();
            $p = $qry->fetchObject();
            if (empty($p)) { // object does not exists                 
                throw new NotFoundException('Sysma Object Parameter ' . $id . ' doesn\'t exist');
            }

            foreach ($p as $key => $value) {
                $this->$key = $value;
            }

            $this->parameter = ucfirst($p->parameter);
            $this->description = html_entity_decode($p->description, ENT_QUOTES);
            $this->dataType = str_replace(' ', '', $p->data_type);

            $this->creation_date = dateDisplay($p->created_at);
            $this->modification_date = dateDisplay($p->modified_at);

            $this->choicesArray = array();
            if ($this->dataType == 'textChoiceType' or $this->dataType == 'multipleTextChoiceType' or $this->dataType == 'sysmaObjectLinkType')
                $this->choicesArray = $this->loadChoices();
            if ($this->dataType == 'sysmaObjectLinkType')
                count($this->choicesArray) > 0 ? $this->sysmaObjectLinked = $this->choicesArray[0] : $this->sysmaObjectLinked = ['nom' => null, 'choice_id' => null];

            return true; // ? there is no return in constructor
        } else {
            return false; // ? there is no return in constructor
        }
    }

    public function constructByAlias($alias, $sysma_object_type_id, $app) 
    {

        $qry = $this->pdo->prepare("select parameter_id from " . SCHEMA . ".sysma_object_type_parameter where parameter_alias = :alias and sysma_object_type_id = :sotid");
        $qry->bindParam(':alias', $alias, PDO::PARAM_STR);
        $qry->bindParam(':sotid', $sysma_object_type_id, PDO::PARAM_INT);
        $qry->execute();
        if ($p = $qry->fetchObject()) {
            $this->__construct($p->parameter_id, $app);
            return true ;
        } else {
            return false ;
        }

    }


    public function update($request)
    {


        $pp = prepUpdateQuery($request, 'parameter_id');
        $qry = $this->pdo->prepare("update " . SCHEMA . ".sysma_object_type_parameter set " . $pp['req'] . " where parameter_id = :parameter_id");
        foreach ($pp['tabD'] as $key => $val) {
            $qry->bindValue($key, $val);
        }
        return returnResultQuery($qry, $this->pdo, $pp['tabD']);
    }

    public function create($request)
    {

        $pp = prepInsertQuery($request);
        $qry = $this->pdo->prepare("insert into " . SCHEMA . ".sysma_object_type_parameter (" . $pp['reqFields'] . ") VALUES (" . $pp['reqParams'] . ") RETURNING parameter_id as id");

        foreach ($pp['tabD'] as $key => $val) {
            if (is_object($val)) {
                // echo 'is_object: '. var_dump($val);
                // echo '<br>convert to json!<br>';
                $val = json_encode($val);
            }
            $qry->bindValue($key, $val);
        }
        return returnResultQuery($qry, $this->pdo, $pp['tabD']);
    }


    public function countSysmaObjectParameters($sopId = null)
    {
        if ($sopId == null) {
            $sopId = $this->parameter_id;
        }
        $qry = $this->pdo->prepare("select count(sysma_object_id) as count from " . SCHEMA . ".sysma_object_data where parameter_id = :id");
        $qry->bindParam(':id', $sopId, PDO::PARAM_INT);
        $qry->execute();
        $res = $qry->fetchObject();
        return $res->count;
    }


    public function delete()
    {
        Debug::startPgLog();

        try {
            // BEGIN
            // echo "<br>";
            // echo $this->pdo->getTransactionCounter();
            // echo "<br>";
            $this->pdo->beginNestedTransaction();
            // $this->pdo->beginTransaction();
            // echo "<br>";
            // echo "ici";
            // die();

            // 1st delete related param values
            $qry = $this->pdo->prepare("DELETE FROM " . SCHEMA . ".sysma_object_data WHERE parameter_id= :id");
            $qry->bindParam(':id', $this->parameter_id, PDO::PARAM_INT);
            $res = returnResultQuery($qry, $this->pdo,  "Cannot delete sysma_object_data");

            // 2nd delete linked parameter_choices
            $qry = $this->pdo->prepare("DELETE FROM " . SCHEMA . ".sysma_object_type_parameter_choice" . " WHERE parameter_id=" . $this->parameter_id);
            $res = returnResultQuery($qry, $this->pdo,  "Cannot delete sysma_object_type_parameter_choice");

            // 3rd delete this parameter
            $qry = $this->pdo->prepare("DELETE FROM " . SCHEMA . ".sysma_object_type_parameter" . " WHERE parameter_id=" . $this->parameter_id);
            $res = returnResultQuery($qry, $this->pdo, "Cannot delete sysma_object_type_parameter");
        } catch (Exception $e) {
            // if any SQL error : ROLLBACK
            $this->pdo->rollbackNestedTransaction();
            // $this->pdo->rollback();
            // echo 'pdo ROLLBACK<br>';
        }
        // if everything's ok : COMMIT
        $this->pdo->commitNestedTransaction();
        // $this->pdo->rollback();
        // $this->pdo->commit();

        Debug::endPgLogAndPrintResults($this->app);
        return $res;
    }

    public function createChoice($request)
    {

        $pp = prepInsertQuery($request);
        $qry = $this->pdo->prepare("insert into " . SCHEMA . ".sysma_object_type_parameter_choice (" . $pp['reqFields'] . ") VALUES (" . $pp['reqParams'] . ") RETURNING choice_id as id");

      
        foreach ($pp['tabD'] as $key => $val) {

            $qry->bindValue($key, $val);
        }
        return returnResultQuery($qry, $this->pdo, $pp['tabD']);
    }

    public function updateChoice($request)
    {

        $pp = prepUpdateQuery($request, 'choice_id');
        $qry = $this->pdo->prepare("update " . SCHEMA . ".sysma_object_type_parameter_choice set " . $pp['req'] . " where choice_id = :choice_id");

        foreach ($pp['tabD'] as $key => $val) {
            echo $key . ' : ' . $val . '<br>';
            $qry->bindValue($key, $val);
        }

        return returnResultQuery($qry, $this->pdo, $pp['tabD']);
    }

    public function deleteChoice($idc)
    {

        $qry = $this->pdo->prepare("DELETE FROM " . SCHEMA . ".sysma_object_type_parameter_choice where choice_id = :choice_id");
        $qry->bindParam(':choice_id', $idc, PDO::PARAM_INT);
        return $qry->execute();
    }

    public function loadChoices()
    {

        $tab = array();
        $qry = $this->pdo->prepare("select * from " . SCHEMA . ".sysma_object_type_parameter_choice where parameter_id = :id order by \"display_order\" ASC");
        $qry->bindParam(':id', $this->parameter_id, PDO::PARAM_INT);
        $qry->execute();
        $compt = 0;
        $deja = array();
        while ($c = $qry->fetchObject()) {

            if (isset($deja[$c->display_order])) {
                $c->display_order = $c->display_order + 999 + $compt;
            }
            $tab[$c->display_order]['choice_id'] = $c->choice_id;
            $tab[$c->display_order]['nom'] = $c->choice;
            $tab[$c->display_order]['description'] = $c->description;
            $tab[$c->display_order]['display_order'] = $c->display_order;
            $tab[$c->display_order]['value_type'] = $c->value_type;
            $deja[$c->display_order] = true;
            $compt++;
        }
        $this->choicesArray = $tab;
        return $tab;
    }


    /************************************************************
     * import func
     * 
     * 
     */

    public function importFromJson($_obj)
    {
        $rc = ''; // TODO error mgmt


        // 1st add it then retreive its id after insert
        // make a local cpy of this parent obj
        $obj = clone $_obj;
        // rm inner sub objs
        unset($obj->choices);
        // rm pk id to let pg deal with it
        unset($obj->parameter_id);
        // Finally, add modified by
        $obj->modified_by = $this->app['session']->get('user_id');
        // persit it
        $rc = json_decode($this->create($obj)); // TODO FIXM : we shouldn't have to decode here. cf. create() : it does the encoding 

        if ($rc->status != 'success') {
            return $rc;
        }
        $id = $rc->id;


        // 2nd now import/deal with inner objs
        if (property_exists($_obj, 'choices')) {
            foreach ($_obj->choices as $otpc) {

                // For DEV WIP!!! rm this!!! v
                // $ot2i->sysma_object_type_alias = uniqid();
                // For DEV WIP!!! rm this!!! ^

                unset($otpc->choice_id);
                $otpc->parameter_id = $id;

                // Finally, add modified by
                $otpc->modified_by = $this->app['session']->get('user_id');
                $rc = $this->createChoice($otpc);
            }
        }
        return $rc;
    }

    /************************************************************
     * export func
     * @info : export the minimal requested members of "sysma object type parameter"
     * 
     */

    public function getExport()
    {
        $sotp = new StdClass;

        // Id shouln't be necessary IIRC but let's export it for now.
        $sotp->parameter_id             = $this->parameter_id;

        $sotp->sysma_object_type_id     = $this->sysma_object_type_id;
        $sotp->parameter                = $this->parameter;
        $sotp->description              = $this->description;
        $sotp->data_type                = $this->data_type;
        $sotp->ordered_by               = $this->ordered_by;
        $sotp->created_at               = $this->created_at;
        $sotp->modified_at              = $this->modified_at;
        $sotp->parameter_source         = $this->parameter_source;
        $sotp->data_source              = $this->data_source;
        $sotp->unit                     = $this->unit;
        $sotp->required                 = $this->required;
        $sotp->parameter_alias          = $this->parameter_alias;
        $sotp->time_tracking            = $this->time_tracking;
        // $pt->track_historytables_id = $this->track_historytables_id;

        // $pt->modified_by            = $this->modified_by;
        $sotp->required_field_parameter = $this->required_field_parameter;
        $sotp->hidden_in_form           = $this->hidden_in_form;

        if (!empty($this->choicesArray)) {
            $sotp->choices = [];
            $idx = 0;
            foreach ($this->choicesArray as $choice) {
                $sotp->choices[$idx] = new stdClass();
                $sotp->choices[$idx]->choice_id = $choice['choice_id'];
                $sotp->choices[$idx]->choice = $choice['nom'];
                $sotp->choices[$idx]->description = $choice['description'];
                $sotp->choices[$idx]->display_order = $choice['display_order'];
                $sotp->choices[$idx]->value_type = $choice['value_type'];
                $idx++;
            }
        }
        return $sotp;
    }
}
