<?php

/**
 * Description of SysmaObject
 * 
 * Geom (or no geom since v3) objects
 *
 * @author srenou
 */
class SysmaObject
{

    // a 1st step towards symfony :p
    // members decalaration used only by tab_mode_terrain's stuf ATM
    public $sysmaObjectId;
    public $parameterId; // Why/how parameterId would be a member of SysmaObject???
    public $startDate;
    public $endDate;
    public $value;
    public $createdAt;
    public $userId;// TODO rename -> createdBy
    public $dataIndex;
    public $sysmaObjectDataId;
    public $modifiedBy;
    public $modifiedAt;
    public $trackHistorytablesId;

    public function __construct($id, $app)
    {

        $this->app = $app;
        $this->pdo = $app['pdo'];

        if (is_numeric($id) and $id != null) {

            $qry = $this->pdo->prepare("select 
                sysma_object_id,
                sysma_object.start_date,
                end_date,
                sysma_object.organisation_id,
                sysma_object.created_by, 
                sysma_object.modified_by,
                status,
                ST_AsText(ST_Centroid(ST_Transform(geom,4326))) As centroid,
                sysma_object.sysma_object_type_id,
                sysma_object_type.sysma_object_type,
                sysma_object.created_at,
                sysma_object.modified_at,
                sysma_object,
                geometry_type,
                organisation.organisation,
                organisation.acronym
                from " . SCHEMA . ".sysma_object," . SCHEMA . ".sysma_object_type, " . SCHEMA . ".organisation  
                where 
                sysma_object.sysma_object_id = :id 
                and sysma_object.sysma_object_type_id = sysma_object_type.sysma_object_type_id 
                and sysma_object.organisation_id = organisation.organisation_id");

            $qry->bindParam(':id', $id, PDO::PARAM_INT);
            $qry->execute();

            if ($qry->rowCount() > 0) {

                $ob = $qry->fetchObject();
                $this->id = $id;


                foreach ($ob as $key => $val) {
                    $this->$key = $val;
                }

                // redondant for now but for later/futur use
                $this->sysmaObjectId = $ob->sysma_object_id;
                // $this->parameterId = ; // Why parameterId is a member of SysmaObject???
                $this->startDate = $ob->start_date;
                $this->endDate = $ob->end_date;
                // $this->value = $ob->; // What is that?
                $this->createdAt = $ob->created_at;
                $this->userId = $ob->created_by; // TODO rename -> createdBy
                // $this->dataIndex = $ob->id;
                // $this->sysmaObjectDataId = $ob->id;
                $this->modifiedBy = $ob->modified_by;
                $this->modifiedAt = $ob->modified_at;
            
                // centroid
               
                if ($ob->centroid !== null and $ob->geometry_type != 'ST_Point' and $ob->geometry_type != 'no_geom') {
                    $tabC = explode(' ', $ob->centroid);                 
                    $this->centroid = 'LatLng(' . substr($tabC[1], 0, -1) . ',' . substr($tabC[0], 6, -1) . ')';
                }

                $this->Status = new Status('sysma_object', $this->status, $this->app);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function updateGeom($geom)
    {

        $qry = $this->pdo->prepare('update ' . SCHEMA . '.sysma_object set geom = :geom  where sysma_object_id = :sysma_object_id');
        $qry->bindParam(':geom', $geom);
        $qry->bindParam(':sysma_object_id', $this->id);
        return returnResultQuery($qry, $this->pdo, null);
    }

    public function create($request)
    {

        $pp = prepInsertQuery($request);
        $qry = $this->pdo->prepare("insert into " . SCHEMA . ".sysma_object (" . $pp['reqFields'] . ") VALUES (" . $pp['reqParams'] . ") RETURNING sysma_object_id AS id");
        foreach ($pp['tabD'] as $key => $val) {
            $qry->bindValue($key, $val);
        }
        return returnResultQuery($qry, $this->pdo, $pp['tabD']);
    }

    public function update($request)
    {


        $pp = prepUpdateQuery($request, 'sysma_object_id');
        $qry = $this->pdo->prepare("update " . SCHEMA . ".sysma_object set " . $pp['req'] . " where sysma_object_id = :sysma_object_id");
        foreach ($pp['tabD'] as $key => $val) {
            $qry->bindValue($key, $val);
        }
        return returnResultQuery($qry, $this->pdo, $pp['tabD']);
    }

    public function loadActualInfosSysmaObject()
    {

        $Typ = new SysmaObjectType($this->sysma_object_type_id, $this->app);
        $Typ->loadParameters($this->app);
        $this->data = array();

        // for each parameter
        for ($i = 0; $i < count($Typ->paramArrayValue); $i++) {

            $P = new SysmaObjectParameter($Typ->paramArrayValue[$i], $this->app);
            $Val = new SysmaObjectParameterValue();
            $Val->buildFromParameters($this->id, $P->parameter_id, '', $this->app);
            array_push($this->data, $Val);
        }
    }

    public function loadActualInfosSysmaObjectWithIndex()
    {

        $Typ = new SysmaObjectType($this->sysma_object_type_id, $this->app);
        $Typ->loadParameters($this->app);
        $this->data = array();

        // for each parameter
        for ($i = 0; $i < count($Typ->paramArrayValue); $i++) {

            $P = new SysmaObjectParameter($Typ->paramArrayValue[$i], $this->app);
            $Val = new SysmaObjectParameterValue();
            $Val->buildFromParameters($this->id, $P->parameter_id, '', $this->app);
            $this->dataIndex[$P->parameter_alias] = $Val;
        }
    }


    
    public function loadActualInfosSysmaObjectWithIndexId()
    {

        $Typ = new SysmaObjectType($this->sysma_object_type_id, $this->app);
        $Typ->loadParameters($this->app);
        $this->data = array();

        // for each parameter
        for ($i = 0; $i < count($Typ->paramArrayValue); $i++) {

            $P = new SysmaObjectParameter($Typ->paramArrayValue[$i], $this->app);
            $Val = new SysmaObjectParameterValue();
            $Val->buildFromParameters($this->id, $P->parameter_id, '', $this->app);
            $this->dataIndex[$P->parameter_id] = $Val;
        }
    }



    /*
     * load complementary infos for the object
     */

    public function loadSysmaObjectInfos()
    {

        $qry = $this->pdo->prepare("select * from " . SCHEMA . ".sysma_object o "
            . "LEFT JOIN " . SCHEMA . ".user u ON o.modified_by=u.user_id "
            . "LEFT JOIN " . SCHEMA . ".organisation s ON o.organisation_id=s.organisation_id "
            . "LEFT JOIN " . SCHEMA . ".sysma_object_type tyo ON o.sysma_object_type_id=tyo.sysma_object_type_id "
            . "where o.sysma_object_id = :id");

        $qry->bindParam(':id', $this->id, PDO::PARAM_INT);
        if ($qry->execute()) {
            $this->infos = $qry->fetchObject();
            return true;
        } else {
            return false;
        }
    }

    public function LoadGeom()
    {

        $qry = $this->pdo->prepare("select geom from " . SCHEMA . ".sysma_object where sysma_object_id =  :id");
        $qry->bindParam(':id', $this->id, PDO::PARAM_INT);
        $qry->execute();
        $res = $qry->fetchObject();
        $this->geomRaw = $res->geom;
        return $res->geom;
    }

    /*
     * load geographic informations
     * calculate the area length according to the geo type
     */

    public function loadGeoInfos($geometry_type)
    {

        $result = $this->infosGeom = $result['surface'] = $result['perimetre'] = null;

        if ($geometry_type == 'no_geom')
            return null;


        elseif ($geometry_type == 'ST_Linestring') {

            $qry = $this->pdo->prepare("select ST_Length(ST_transform(geom,2154)) as perimetre, ST_AsText(ST_Centroid(ST_Transform(geom,4326))) as centre, ST_AsText(ST_Centroid(ST_Transform(geom,2154))) as \"centreL93\" , ST_AsText(ST_Centroid(ST_Transform(geom,27572))) as \"centreL2e\" , ST_AsText(ST_Centroid(ST_Transform(geom,3947))) as \"centreCC47\"from " . SCHEMA . ".sysma_object where sysma_object_id =  :id");
            $qry->bindParam(':id', $this->id, PDO::PARAM_INT);
            $qry->execute();
            $res = $qry->fetchObject();
            $result['perimetre'] = $res->perimetre;
        } elseif ($geometry_type == 'ST_Polygon') {

            $qry = $this->pdo->prepare("select ST_Perimeter(ST_transform(geom,2154)) as perimetre, ST_Area(ST_transform(geom,2154)) as surface, ST_AsText(ST_Centroid(ST_Transform(geom,4326))) as centre, ST_AsText(ST_Centroid(ST_Transform(geom,2154))) as \"centreL93\"  , ST_AsText(ST_Centroid(ST_Transform(geom,27572))) as \"centreL2e\" , ST_AsText(ST_Centroid(ST_Transform(geom,3947))) as \"centreCC47\" from " . SCHEMA . ".sysma_object where sysma_object_id =  :id");
            $qry->bindParam(':id', $this->id, PDO::PARAM_INT);
            $qry->execute();
            $res = $qry->fetchObject();
            $result['perimetre'] = $res->perimetre;
            $result['surface'] = $res->surface;
        } elseif ($geometry_type == 'ST_Point') {

            $qry = $this->pdo->prepare("select ST_AsText(ST_Centroid(ST_Transform(geom,4326))) as centre, ST_AsText(ST_Centroid(ST_Transform(geom,2154))) as \"centreL93\"  , ST_AsText(ST_Centroid(ST_Transform(geom,27572))) as \"centreL2e\" , ST_AsText(ST_Centroid(ST_Transform(geom,3947))) as \"centreCC47\" from " . SCHEMA . ".sysma_object where sysma_object_id =  :id");
            $qry->bindParam(':id', $this->id, PDO::PARAM_INT);
            $qry->execute();
            $res = $qry->fetchObject();
        }

        $tabC = explode(' ', $res->centre);
        $centre = 'LatLng(' . substr($tabC[1], 0, -1) . ',' . substr($tabC[0], 6, -1) . ')';
        $result['centre'] = $centre;
        $result['centreWGS84'] = $res->centre;
        $result['centreL93'] = $res->centreL93;
        $result['centreL2e'] = $res->centreL2e;
        $result['centreCC47'] = $res->centreCC47;


        $this->infosGeom = $result;
    }

    /*
     * load object main photo => order = 1
     * @return boolean
     */

    public function loadMainPhoto()
    {

        $this->PP = null;
        $qry = $this->pdo->prepare("select photo_object_id from " . SCHEMA . ".photo_object where sysma_object_id = :id and \"display_order\" = 1 and (photo_type='photo' or photo_type='photo_upload') order by \"display_order\" ASC OFFSET 0");
        $qry->bindParam(':id', $this->id, PDO::PARAM_INT);
        $qry->execute();
        if ($qry->rowCount() > 0) {
            $res = $qry->fetchObject();
            $PO = new PhotoObject($res->photo_object_id, $this->app);
            $this->PP = $PO->Photo;
            return true;
        } else {
            return false;
        }
    }

    public function loadSysmaActions()
    {

        $actionsArray = array();

        $qry = $this->pdo->prepare("select sysma_action.sysma_action_id,l_sysma_object_sysma_action.sysma_object_id,sysma_action.start_date from " . SCHEMA . ".l_sysma_object_sysma_action," . SCHEMA . ".sysma_action where l_sysma_object_sysma_action.sysma_object_id = :id and sysma_action.sysma_action_id=l_sysma_object_sysma_action.sysma_action_id order by sysma_action.start_date DESC ");

        $qry->bindParam(':id', $this->id, PDO::PARAM_INT);
        $qry->execute();

        $i = 0;

        // TODO sort the works for the actual organisation

        while ($t = $qry->fetchObject()) {

            $actionsArray[$i] = new SysmaAction($t->sysma_action_id, $this->app);
            $i++;
        }

        $this->actionsArray = $actionsArray;
    }

    public function loadSysmaRelations()
    {

        $relationsArray = array();

        $qry = $this->pdo->prepare("select "
            . " r.sysma_relation_id,l.sysma_object_id,r.start_date, r.end_date,l.parent_child as parent_child "
            . " from " . SCHEMA . ".l_sysma_object_sysma_relation l"
            . " inner join " . SCHEMA . ".sysma_relation r on r.sysma_relation_id=l.sysma_relation_id "
            . " where l.sysma_object_id = :id"
            . " order by r.start_date DESC ");

        $qry->bindParam(':id', $this->id, PDO::PARAM_INT);
        $qry->execute();

        $i = 0;

        // TODO sort the works for the actual organisation

        while ($t = $qry->fetchObject()) {

            $RL = new SysmaRelation($t->sysma_relation_id, $this->app);
            $RL->loadActualInfosSysmaRelation();
            $relationsArray[$t->sysma_relation_id]['RL'] = $RL;
            $relationsArray[$t->sysma_relation_id]['infos'] = $t;
            $relationsArray[$t->sysma_relation_id]['objects_linked'] = $RL->loadSysmaObjectsLinked(invertPC($t->parent_child), $this->sysma_object_id);
            $i++;
        }

        $this->relationsArray = $relationsArray;
    }

    public function loadSysmaRelation($sysma_relation_id)
    {

        $relationsArray = array();

        $qry = $this->pdo->prepare("select "
            . " r.sysma_relation_id,l.sysma_object_id,r.start_date, r.end_date, l.parent_child "
            . " from " . SCHEMA . ".l_sysma_object_sysma_relation l"
            . " inner join " . SCHEMA . ".sysma_relation r on r.sysma_relation_id=l.sysma_relation_id "
            . " where l.sysma_object_id = :id and r.sysma_relation_id = :idr"
            . " order by r.start_date DESC ");

        $qry->bindParam(':id', $this->id, PDO::PARAM_INT);
        $qry->bindParam(':idr', $sysma_relation_id, PDO::PARAM_INT);
        $qry->execute();

        $i = 0;

        // TODO sort the works for the actual organisation

        while ($t = $qry->fetchObject()) {

            $RL = new SysmaRelation($t->sysma_relation_id, $this->app);
            $RL->loadActualInfosSysmaRelation();
            $relationsArray[$t->sysma_relation_id]['RL'] = $RL;
            $relationsArray[$t->sysma_relation_id]['infos'] = $t;
            $relationsArray[$t->sysma_relation_id]['objects_linked'] = $RL->loadSysmaObjectsLinked(invertPC($t->parent_child));

            $i++;
        }

        $this->relationsArray = $relationsArray;
    }

    /*
     * load photo linked to the object
     * @param string $beforeAfter before or after an action (work) on the object
     * @return void  
     */

    public function loadPhotoObjects($beforeAfter = null)
    {


        $this->tabOP = array();
        $this->beforePhotosArray = array();
        $this->afterPhotosArray = array();

        if ($beforeAfter == null)
            $qry = $this->pdo->prepare("select * from " . SCHEMA . ".photo_object where sysma_object_id = :id order by \"display_order\" ASC, before_after DESC ");
        if ($beforeAfter != null) {
            $qry = $this->pdo->prepare("select * from " . SCHEMA . ".photo_object where sysma_object_id = :id and before_after = :before_after order by \"display_order\" ASC ");
            $qry->bindParam(':before_after', $beforeAfter, PDO::PARAM_STR);
        }

        $qry->bindParam(':id', $this->id, PDO::PARAM_INT);
        $qry->execute();

        while ($t = $qry->fetchObject()) {
            if ($beforeAfter == null) {
                array_push($this->tabOP, new PhotoObject($t->photo_object_id, $this->app));
            }
            if ($beforeAfter === 'before') {
                array_push($this->beforePhotosArray, new PhotoObject($t->photo_object_id, $this->app));
            }
            if ($beforeAfter === 'after') {
                array_push($this->afterPhotosArray, new PhotoObject($t->photo_object_id, $this->app));
            }
        }
    }

    /*
     * add a link between the object and a worksheet
     * @param integer $sysma_action_id
     * @param integer $modified_by
     * @return boolean
     */

    public function linkSysmaActionSheet($sysma_action_id, $modified_by)
    {

        if ($sysma_action_id != null and is_numeric($sysma_action_id)) {

            $this->unlinkSysmaActionSheet($sysma_action_id);

            $qry = $this->pdo->prepare("insert into " . SCHEMA . ".l_sysma_object_sysma_action (sysma_object_id,sysma_action_id,modified_by) VALUES (:sysma_object_id,:sysma_action_id,:modified_by)");
            $qry->bindParam(':sysma_object_id', $this->id, PDO::PARAM_INT);
            $qry->bindParam(':sysma_action_id', $sysma_action_id, PDO::PARAM_INT);
            $qry->bindParam(':modified_by', $modified_by, PDO::PARAM_INT);
            return $qry->execute();
        }
    }

    /*
     * delete the link between the object and a worksheet
     * @param integer $sysma_action_id     * 
     * @return boolean
     */

    public function unlinkSysmaActionSheet($sysma_action_id)
    {

        if ($sysma_action_id != null and is_numeric($sysma_action_id)) {

            $qry = $this->pdo->prepare("DELETE FROM " . SCHEMA . ".l_sysma_object_sysma_action where sysma_object_id = :sysma_object_id and sysma_action_id = :sysma_action_id");
            $qry->bindParam(':sysma_object_id', $this->id, PDO::PARAM_INT);
            $qry->bindParam(':sysma_action_id', $sysma_action_id, PDO::PARAM_INT);
            return $qry->execute();
        }
    }

    public function deleteData()
    {


        $qry = $this->pdo->prepare("DELETE FROM " . SCHEMA . ".sysma_object_data where sysma_object_id = :sysma_object_id");
        $qry->bindParam(':sysma_object_id', $this->id, PDO::PARAM_INT);
        return $qry->execute();
    }

    public function deletePhotos()
    {


        $qry = $this->pdo->prepare("DELETE FROM " . SCHEMA . ".photo_object where sysma_object_id = :sysma_object_id");
        $qry->bindParam(':sysma_object_id', $this->id, PDO::PARAM_INT);
        return $qry->execute();
    }

    public function delete()
    {

        $this->deleteData();
        $this->deletePhotos();

        $this->loadSysmaActions();
        foreach ($this->actionsArray as $T) {
            $this->unlinkSysmaActionSheet($T->id);
        }
        $qry = $this->pdo->prepare("DELETE FROM " . SCHEMA . ".sysma_object where sysma_object_id = :sysma_object_id");
        $qry->bindParam(':sysma_object_id', $this->id, PDO::PARAM_INT);
        return $qry->execute();
    }

}
