<?php

/**
 * Description of SysmaRelationType
 *  
 *
 * @author srenou
 * 
 */
class SysmaRelationTypeSysmaObjectTypeLink
{

    public function __construct($sysma_relationtype_id, $parent_child, $sysma_object_id, $app)
    {

        $this->pdo = $app['pdo'];

        if ($parent_child == 'child') {
            $object_id_search = 'sysma_object_id_parent';
            $object_id = 'sysma_object_id_child';
        } else {
            $object_id_search = 'sysma_object_id_child';
            $object_id = 'sysma_object_id_parent';
        }

        $qry = $this->pdo->prepare('select * from ' . SCHEMA . '.l_sysma_object_type_sysma_relation_type r '
            . ' where sysma_relation_type_id = :id'
            . ' and r.' . $object_id . ' = :ido');

        $qry->bindParam(':id', $sysma_relationtype_id, PDO::PARAM_INT);
        $qry->bindParam(':ido', $sysma_object_id, PDO::PARAM_INT);
        $qry->execute();
        foreach ($qry->fetchObject() as $key => $val) {
            $this->$key = $val;
        }
        $this->parent_child = $parent_child;
    }
}
