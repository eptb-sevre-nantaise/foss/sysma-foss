<?php

/**
 * Description of ThematicAnalysis
 *
 * @author srenou
 */
class ThematicAnalysis
{

    public function __construct($id, $app)
    {

        $this->pdo = $app['pdo'];

        if ($id != null) {

            $vars = buildSimpleEntity('theme_analysis', 'theme_analysis_id', $id, $app);

            foreach ($vars as $key => $value) {
                $this->$key = $value;
            }

            if ($this->object_type === 'sysma_object') {
                $this->Typ = new SysmaObjectType($this->type_id, $app);
                $this->P = new SysmaObjectParameter($this->parameter_id, $app);
            }
            if ($this->object_type === 'sysma_action') {
                $this->Typ = new SysmaActionType($this->type_id, $app);
                $this->P = new SysmaActionParameter($this->parameter_id, $app);
            }

            $this->styles = get_object_vars(json_decode($this->style));
            return true;
        } else {
            return false;
        }
    }

    public function activate($id_util)
    {

        $this->desactivateOthersTA($id_util);
        $this->desactivate($id_util);


        $qry = $this->pdo->prepare('insert into ' . SCHEMA . '.l_theme_analysis_user (theme_analysis_id,user_id) VALUES (:theme_analysis_id,:id_util)');
        $qry->bindParam(':theme_analysis_id', $this->theme_analysis_id, PDO::PARAM_INT);
        $qry->bindParam(':id_util', $id_util, PDO::PARAM_INT);
        return $qry->execute();
    }

    public function desactivate($id_util)
    {

        $qry = $this->pdo->prepare('delete from ' . SCHEMA . '.l_theme_analysis_user where theme_analysis_id = :theme_analysis_id and user_id = :id_util');
        $qry->bindParam(':theme_analysis_id', $this->theme_analysis_id, PDO::PARAM_INT);
        $qry->bindParam(':id_util', $id_util, PDO::PARAM_INT);
        return $qry->execute();
    }


    public function desactivateForAllUsers()
    {

        $qry = $this->pdo->prepare('delete from ' . SCHEMA . '.l_theme_analysis_user where theme_analysis_id = :theme_analysis_id');
        $qry->bindParam(':theme_analysis_id', $this->theme_analysis_id, PDO::PARAM_INT);
        return $qry->execute();
    }

    public function desactivateOthersTA($id_util)
    {


        $qry = $this->pdo->prepare('select theme_analysis_id from ' . SCHEMA . '.theme_analysis where object_type = :type and type_id = :type_id and theme_analysis_id != :theme_analysis_id');
        $qry->bindParam(':theme_analysis_id', $this->theme_analysis_id, PDO::PARAM_INT);
        $qry->bindParam(':type_id', $this->type_id, PDO::PARAM_INT);
        $qry->bindParam(':type', $this->object_type, PDO::PARAM_STR);
        $qry->execute();
        while ($at = $qry->fetchObject()) {

            $qry2 = $this->pdo->prepare('delete from ' . SCHEMA . '.l_theme_analysis_user where theme_analysis_id = :theme_analysis_id and user_id = :id_util');
            $qry2->bindParam(':theme_analysis_id', $at->theme_analysis_id, PDO::PARAM_INT);
            $qry2->bindParam(':id_util', $id_util, PDO::PARAM_INT);
            $qry2->execute();
        }

        return true;
    }

    public function insert($request)
    {


        $pp = prepInsertQuery($request);


        $qry = $this->pdo->prepare("insert into " . SCHEMA . ".theme_analysis (" . $pp['reqFields'] . ") VALUES (" . $pp['reqParams'] . ") returning theme_analysis_id as id");

        foreach ($pp['tabD'] as $key => $val) {
            $qry->bindValue($key, $val);
        }

        return returnResultQuery($qry, $this->pdo, $pp['tabD']);
    }

    /*
     * applique l'analyse à une value
     * $options min : value min pour un gradient, max value max
     */

    public function apply($value, $options = null)
    {

        if ($this->theme_analysis_type === 'classes') {

            $style = null;
            $defaut = null;

            foreach ($this->styles as $key => $s) {

                if ($key != 'defaut' and floatval($s->min) < floatval($value) and floatval($s->max) >= floatval($value)) {
                    $style = $s->style;
                } else {
                    $defaut = $s->style;
                }
            }

            if ($style === null)
                $style = $defaut;
            return json_encode($style);
        } elseif ($this->theme_analysis_type === 'values') {

            $style = null;
            $defaut = null;

            foreach ($this->styles as $key => $s) {


                if ($key != 'defaut' and $value == $key) {
                    $style = $s->style;
                } else {
                    $defaut = $s->style;
                }
            }

            if ($style === null)
                $style = $defaut;

            if (isset($style->display) and $style->display == 'none') {                
                return null;
            }

            return json_encode($style);
        } elseif ($this->theme_analysis_type === 'gradient') {

            $style = null;
            $defaut = $this->styles['defaut']->style;


            foreach ($this->classes as $key => $s) {


                if (floatval($s['min']) <= floatval($value) and floatval($s['max']) >= floatval($value)) {

                    $style = array("fillColor" => $s['color'], "color" => $s['color'], "fillOpacity" => 0.8, "opacity" => 1, 'radius' => 5, "weight" => "5");
                }
            }

            if ($style === null)
                $style = $defaut;


            return json_encode($style);
        } else {
            return null;
        }
    }

    public function calculateTAClasses($min, $max)
    {


        $nbcl = $this->styles['nb_classes'];
        $coulmin = $this->styles['min']->style->color;
        $coulmax = $this->styles['max']->style->color;
        /*
          $coulmin = substr($coulmin, 4, -1);
          $coulmin = explode(',', $coulmin);
          $coulmax = substr($coulmax, 4, -1);
          $coulmax = explode(',', $coulmax);
         */
        $couleurs = Gradient(substr($coulmin, 1), substr($coulmax, 1), $nbcl);


        $classes = array();
        for ($i = 0; $i < $nbcl; $i++) {

            //$classes[$i]['couleur'] = 'rgb(' . ($coulmin[0] + (($coulmax[0] - $coulmin[0]) / $nbcl * ($i))) . ',' . ($coulmin[1] + (($coulmax[1] - $coulmin[1]) / $nbcl * ($i))) . ',' . ($coulmin[2] + (($coulmax[2] - $coulmin[2]) / $nbcl * ($i))) . ')';
            $classes[$i]['color'] = '#' . $couleurs[$i];
            $classes[$i]['min'] = $min + (($max - $min) / $nbcl * ($i));
            $classes[$i]['max'] = $min + (($max - $min) / $nbcl * ($i + 1));
        }

        $this->classes = $classes;
        //var_dump($classes) ;
        return $this->classes;
    }



    public function update($request)
    {


        $pp = prepUpdateQuery($request, 'theme_analysis_id');

        $qry = $this->pdo->prepare("update " . SCHEMA . ".theme_analysis set " . $pp['req'] . " where theme_analysis_id = :theme_analysis_id");
        foreach ($pp['tabD'] as $key => $val) {
            $qry->bindValue($key, $val);
        }
        return returnResultQuery($qry, $this->pdo, $pp['tabD']);
    }



    public function create($request)
    {

        $pp = prepInsertQuery($request);
        $qry = $this->pdo->prepare("insert into " . SCHEMA . ".theme_analysis (" . $pp['reqFields'] . ") VALUES (" . $pp['reqParams'] . ") RETURNING theme_analysis_id as id");

        foreach ($pp['tabD'] as $key => $val) {
            $qry->bindValue($key, $val);
        }
        return returnResultQuery($qry, $this->pdo, $pp['tabD']);
    }

    public function delete()
    {
        if ($this->desactivateForAllUsers()) {
            $qry = $this->pdo->prepare("delete from " . SCHEMA . ".theme_analysis where theme_analysis_id = :id");
            $qry->bindParam(':id', $this->theme_analysis_id, PDO::PARAM_INT);
            return $qry->execute();
        } else {
            return false;
        }
    }
}
