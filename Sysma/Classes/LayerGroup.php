<?php


class LayerGroup
{

    public function __construct($id, $app)
    {

        $this->pdo = $app['pdo'];
        if (is_numeric($id) and $id != null) {

            $ol = buildSimpleEntity('layer_group', 'layer_group_id', $id, $app);
            foreach ($ol as $key => $val) {
                $this->$key = $val;
            }
        }
    }

    public function updateLayerGroup($new_group)
    {

        $q = 'select * from ' . SCHEMA . '.sysma_object_type where group_alias = :group';
        $qry = $this->pdo->prepare($q);
        $qry->bindValue(':group', $this->group_alias);
        $qry->execute();
        while ($res = $qry->fetchObject()) {

            $qry2 = $this->pdo->prepare('update ' . SCHEMA . '.sysma_object_type set group_alias = :new_group where group_alias = :old_group');
            $qry2->bindParam(':old_group', $this->group_alias, PDO::PARAM_STR);
            $qry2->bindParam(':new_group', $new_group, PDO::PARAM_STR);
            $qry2->execute();
        }
    }

    public function update($request)
    {

        $this->updateLayerGroup($request['group_alias']);
        $pp = prepUpdateQuery($request, 'layer_group_id');

        $qry = $this->pdo->prepare("update " . SCHEMA . ".layer_group set " . $pp['req'] . " where layer_group_id = :layer_group_id");
        foreach ($pp['tabD'] as $key => $val) {
            $qry->bindValue($key, $val);
        }
        return returnResultQuery($qry, $this->pdo, $pp['tabD']);
    }

    public function create($request)
    {
        $pp = prepInsertQuery($request);
        $qry = $this->pdo->prepare("insert into " . SCHEMA . ".layer_group (" . $pp['reqFields'] . ") VALUES (" . $pp['reqParams'] . ") RETURNING layer_group_id");
        foreach ($pp['tabD'] as $key => $val) {
            $qry->bindValue($key, $val);
        }
        return returnResultQuery($qry, $this->pdo, $pp['tabD']);
    }



    public function delete()
    {
        $this->updateLayerGroup(null);
        $qry = $this->pdo->prepare("delete from " . SCHEMA . ".layer_group where layer_group_id = :id");
        $qry->bindParam(':id', $this->layer_group_id, PDO::PARAM_INT);
        return $qry->execute();
    }
}
