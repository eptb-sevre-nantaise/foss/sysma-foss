<?php

/**
 * Description of Style 
 * 
 * @author srenou
 * 
 */
class Style
{

    public function __construct($object_type, $type_id, $user_id, $app)
    {

        $this->pdo = $app['pdo'];

        $this->object_type = $object_type;
        $this->type_id = $type_id;
        $this->user_id = $user_id;


        if ($object_type === 'sysma_object') {

            $qry = $this->pdo->prepare(
                ' select l.l_style_user_id as id, l.object_type, l.type_id, l.user_id, xx_99_utils.json_merge(tyo.style::json,l.style::json) as style from ' . SCHEMA . '.sysma_object_type tyo inner join ' . SCHEMA . '.l_style_user l on l.user_id = :idu and object_type = \'sysma_object\' and l.type_id = tyo.sysma_object_type_id where tyo.sysma_object_type_id = :idt'
            );

            $qry->bindParam(':idu', $user_id, PDO::PARAM_INT);
            $qry->bindParam(':idt', $type_id, PDO::PARAM_INT);
        } elseif ($object_type === 'sysma_action') {

            $qry = $this->pdo->prepare('select l.l_style_user_id as id, l.object_type, l.type_id, l.user_id, xx_99_utils.json_merge(tyt.style::json,l.style::json) as style from ' . SCHEMA . '.sysma_action_type tyt inner join ' . SCHEMA . '.l_style_user l on l.user_id = :idu and object_type = \'sysma_action\' and l.type_id = tyt.sysma_action_type_id where tyt.sysma_action_type_id = :idt');

            $qry->bindParam(':idu', $user_id, PDO::PARAM_INT);
            $qry->bindParam(':idt', $type_id, PDO::PARAM_INT);
        }

        $qry->execute();

        $this->style_decode = null;
        if ($qry->rowCount() > 0) {
            $vars = $qry->fetch();

            foreach ($vars as $key => $value) {
                $this->$key = $value;
            }
            $this->style_decode = json_decode($this->style);
        }
    }

    public function update($style)
    {

        $this->delete();
        $qry = $this->pdo->prepare('insert into ' . SCHEMA . '.l_style_user (style,user_id,object_type,type_id) VALUES (:style,:idu,:tyo,:idt)');
        $qry->bindParam('idu', $this->user_id, PDO::PARAM_INT);
        $qry->bindParam('tyo', $this->object_type, PDO::PARAM_STR);
        $qry->bindParam('idt', $this->type_id, PDO::PARAM_INT);
        $qry->bindParam('style', $style, PDO::PARAM_STR);
        return $qry->execute();
    }

    public function delete()
    {

        $qry = $this->pdo->prepare('delete from  ' . SCHEMA . '.l_style_user where l_style_user_id = :id');
        $qry->bindParam('id', $this->id, PDO::PARAM_INT);
        return $qry->execute();
    }
}
