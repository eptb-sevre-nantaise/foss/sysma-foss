<?php

/**
 * Description of SysmaRelationType
 *  
 *
 * @author srenou
 * 
 */
class SysmaRelationType
{

    public function __construct($id, $app)
    {

        $this->app = $app;
        $this->pdo = $app['pdo'];
        $this->paramArrayValue = null;

        if (is_numeric($id) and $id != null) {

            $qry = $this->pdo->prepare("select * from " . SCHEMA . ".sysma_relation_type where sysma_relation_type_id = :sysma_relation_type_id");
            $qry->bindParam(':sysma_relation_type_id', $id, PDO::PARAM_INT);
            $qry->execute();
            $t = $qry->fetchObject();
            foreach ($t as $key => $value) {
                $this->$key = $value;
            }
        }
    }

    public function isAUniqRelation($parent_child)
    {

        if (($parent_child == 'parent' and in_array($this->relation_type, ['1..1', '*..1'])) or ($parent_child == 'child' and in_array($this->relation_type, ['1..1', '1..*']))) {
            return true;
        } else {
            return false;
        }
    }

    public function loadParameters()
    {

        $this->paramArray = $this->paramArrayValue = array();

        $qry = $this->pdo->prepare("select * from " . SCHEMA . ".sysma_relation_type_parameter where sysma_relation_type_id = :sysma_relation_type_id order by \"ordered_by\" ASC, \"parameter_id\" ASC");
        $qry->bindParam(':sysma_relation_type_id', $this->sysma_relation_type_id, PDO::PARAM_INT);
        $qry->execute();
        $i = 0;
        while ($p = $qry->fetchObject()) {
            $this->paramArrayValue[$i] = $p->parameter_id;
            $this->paramArray[$i] = new SysmaRelationParameter($p->parameter_id, $this->app); // objets
            $i++;
        }
    }

    // TODO not a class function...

    public function loadSysmaObjects($sysma_object_type_id)
    {

        $sysmaObjectsArray = array();
        $qry = $this->pdo->prepare("select sysma_object_id from " . SCHEMA . ".sysma_object where sysma_object_type_id = :id");
        $qry->bindParam(':id', $sysma_object_type_id, PDO::PARAM_INT);
        $qry->execute();
        while ($o = $qry->fetchObject()) {
            $sysmaObjectsArray[] = new SysmaObject($o->sysma_object_id, $this->app);
        }
        return $sysmaObjectsArray;
    }

    public function loadSysmaRelations()
    {

        $qry = $this->pdo->prepare("select * from " . SCHEMA . ".sysma_relation where sysma_relation_type_id = :id");
        $qry->bindParam(':id', $this->sysma_relation_type_id, PDO::PARAM_INT);
        $qry->execute();
        return $qry->fetchAll(PDO::FETCH_ASSOC);
    }

    public function countSysmaRelations()
    {


        $qry = $this->pdo->prepare("select count(sysma_relation_id) as count from " . SCHEMA . ".sysma_relation where sysma_relation_type_id = :id");
        $qry->bindParam(':id', $this->sysma_relation_type_id, PDO::PARAM_INT);
        $qry->execute();
        $res = $qry->fetchObject();
        return $res->count;
    }

    public function update($request)
    {

        $pp = prepUpdateQuery($request, 'sysma_relation_type_id');

        $qry = $this->pdo->prepare("update " . SCHEMA . ".sysma_relation_type set " . $pp['req'] . " where sysma_relation_type_id = :sysma_relation_type_id");

        foreach ($pp['tabD'] as $key => $val) {
            $qry->bindValue($key, $val);
        }

        return returnResultQuery($qry, $this->pdo, $pp['tabD']);
    }

    public function create($request)
    {

        $pp = prepInsertQuery($request);

        $qry = $this->pdo->prepare("insert into " . SCHEMA . ".sysma_relation_type (" . $pp['reqFields'] . ") VALUES (" . $pp['reqParams'] . ") RETURNING sysma_relation_type_id as id");

        foreach ($pp['tabD'] as $key => $val) {
            $qry->bindValue($key, $val);
        }

        return returnResultQuery($qry, $this->pdo, $pp['tabD']);
    }


    /************************************************************
     * Import func
     * 
     * 
     */
    public function importFromJson($_obj)
    {
        $rc = ''; // TODO error mgmt

        // rm pk id to let pg deal with it
        unset($_obj->sysma_relation_type_id);

        // 1st add it then retreive its id after insert
        // make a local cpy of this parent obj
        $obj = clone $_obj;
        // rm inner sub objs
        unset($obj->relationTypeParams);

        // Finally, add modified by
        $obj->modified_by = $this->app['session']->get('user_id');
        // persist it
        $rc = json_decode($this->create($obj)); // TODO FIXM : we shouldn't have to decode here. cf. create() : it does the encoding 
        // var_dump($obj);
        // var_dump('*******************************************************************\n');
        // var_dump($rc);
        // die;
        if ($rc->status != 'success') {
            return $rc;
        }
        $id = $rc->id;
        // var_dump($id);
        // die;

        // 2nd now import/deal with inner objs
        // TODO TBD
        // if ( property_exists($_obj, 'relationTypeParams') ) {
        //     foreach($_obj->relationTypeParams as $rtp) {
        //         $srp = new SysmaRelationParameter(null, $this->app);
        //         // For DEV WIP!!! rm this!!! v
        //         $rtp->parameter_alias = uniqid();
        //         // For DEV WIP!!! rm this!!! ^
        //         // $sap->sysma_object_type_id = $id;
        //         $rtp->sysma_action_type_id = $id;
        //         $rc = $srp->importFromJson($rtp);
        //     }
        // }

        return $rc;
    }

    /************************************************************
     * export func
     * export the minimal requested members of "sysma relation type"
     * 
     */
    public function getExport()
    {
        $srt = new StdClass;

        try {
            // Id shouln't be necessary IIRC but let's export it for now.
            $srt->sysma_relation_type_id            = $this->sysma_relation_type_id;
            $srt->sysma_relation_type_from_parent   = $this->sysma_relation_type_from_parent;
            $srt->sysma_relation_type_from_child    = $this->sysma_relation_type_from_child;

            $srt->sysma_relation_type_alias         = $this->sysma_relation_type_alias;
            $srt->description                       = $this->description;
            $srt->relation_type                     = $this->relation_type;

            $srt->created_at                        = $this->created_at;
            $srt->modified_at                       = $this->modified_at;
            $srt->data_source                       = $this->data_source;
            $srt->definition_source                 = $this->definition_source;

            // $srt->track_historytables_id            = $this->track_historytables_id;
            $srt->modified_by                       = $this->modified_by;
            $srt->hidden                            = $this->hidden;
            $srt->sysma_relation_type               = $this->sysma_relation_type;

            // try to load paramArray if necessary
            if (empty($this->paramArray)) {
                $this->paramArray = null;
                $this->loadParameters();
            }

            $srt->relationTypeParams = [];
            foreach ($this->paramArray as $param) {
                $srt->relationTypeParams[] = $param->getExport();
            }
            // $sotp->actionTypeParams = $this->paramArray;

            // $sotp->choices = [];
            // foreach($this->choicesArray as $choice) {
            //     $sotp->choices[] = $choice;
            // }
            // $sotp->choices                  = $this->choicesArray;
        } catch (Exception $ex) {
            return 'TODO';
            // return '<span class="glyphicon glyphicon-remove text-danger" title="' . $ex->getMessage() . '"></span>' . $ex->getMessage();
        }

        return $srt;
    }
}
