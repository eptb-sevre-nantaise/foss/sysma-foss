<?php

/**
 * Description of SysmaRelationParameter
 *  
 *
 * @author srenou
 * 
 */
class SysmaRelationParameter
{

    public function __construct($id, $app)
    {

        $this->pdo = $app['pdo'];
        if (is_numeric($id) and $id != null) {

            $qry = $this->pdo->prepare("select * from " . SCHEMA . ".sysma_relation_type_parameter where parameter_id = :id");
            $qry->bindParam(':id', $id, PDO::PARAM_INT);
            $qry->execute();
            $p = $qry->fetchObject();

            foreach ($p as $key => $value) {
                $this->$key = $value;
            }

            $this->parameter = ucfirst($p->parameter);
            $this->description = html_entity_decode($p->description, ENT_QUOTES);
            $this->dataType = str_replace(' ', '', $p->data_type);

            $this->creation_date = dateDisplay($p->created_at);
            $this->modification_date = dateDisplay($p->modified_at);

            if ($this->dataType == 'textChoiceType' or $this->dataType == 'multipleTextChoiceType' or $this->dataType == 'sysmaObjectLinkType')
                $this->choicesArray = $this->loadChoices();
            if ($this->dataType == 'sysmaObjectLinkType')
                count($this->choicesArray) > 0 ? $this->sysmaObjectLinked = $this->choicesArray[0] : $this->sysmaObjectLinked = ['nom' => null, 'choice_id' => null];

            return true;
        } else {
            return false;
        }
    }

    public function update($request)
    {


        $pp = prepUpdateQuery($request, 'parameter_id');
        $qry = $this->pdo->prepare("update " . SCHEMA . ".sysma_relation_type_parameter set " . $pp['req'] . " where parameter_id = :parameter_id");
        foreach ($pp['tabD'] as $key => $val) {
            $qry->bindValue($key, $val);
        }
        return returnResultQuery($qry, $this->pdo, $pp['tabD']);
    }

    public function create($request)
    {

        $pp = prepInsertQuery($request);
        $qry = $this->pdo->prepare("insert into " . SCHEMA . ".sysma_relation_type_parameter (" . $pp['reqFields'] . ") VALUES (" . $pp['reqParams'] . ") RETURNING parameter_id as id");

        foreach ($pp['tabD'] as $key => $val) {
            $qry->bindValue($key, $val);
        }
        return returnResultQuery($qry, $this->pdo, $pp['tabD']);
    }

    public function createChoice($request)
    {

        $pp = prepInsertQuery($request);
        $qry = $this->pdo->prepare("insert into " . SCHEMA . ".sysma_relation_type_parameter_choice (" . $pp['reqFields'] . ") VALUES (" . $pp['reqParams'] . ") RETURNING choice_id as id");

        foreach ($pp['tabD'] as $key => $val) {

            $qry->bindValue($key, $val);
        }
        return returnResultQuery($qry, $this->pdo, $pp['tabD']);
    }

    public function updateChoice($request)
    {


        $pp = prepUpdateQuery($request, 'choice_id');
        $qry = $this->pdo->prepare("update " . SCHEMA . ".sysma_relation_type_parameter_choice set " . $pp['req'] . " where choice_id = :choice_id");
        foreach ($pp['tabD'] as $key => $val) {
            $qry->bindValue($key, $val);
        }
        return returnResultQuery($qry, $this->pdo, $pp['tabD']);
    }

    public function deleteChoice($idc)
    {

        $qry = $this->pdo->prepare("delete from " . SCHEMA . ".sysma_relation_type_parameter_choice where choice_id = :choice_id");
        $qry->bindParam(':choice_id', $idc, PDO::PARAM_INT);
        return $qry->execute();
    }

    public function loadChoices()
    {

        $tab = null;
        $qry = $this->pdo->prepare("select * from " . SCHEMA . ".sysma_relation_type_parameter_choice where parameter_id = :id order by \"display_order\" ASC");
        $qry->bindParam(':id', $this->parameter_id, PDO::PARAM_INT);
        $qry->execute();
        $compt = 0;
        $deja = array();
        while ($c = $qry->fetchObject()) {

            if (isset($deja[$c->display_order])) {
                $c->display_order = $c->display_order + 999;
            }
            $tab[$c->display_order]['choice_id'] = $c->choice_id;
            $tab[$c->display_order]['nom'] = $c->choice;
            $tab[$c->display_order]['description'] = $c->description;
            $tab[$c->display_order]['display_order'] = $c->display_order;
            $tab[$c->display_order]['value_type'] = $c->value_type;
            $deja[$c->display_order] = true;
            $compt++;
        }
        $this->choicesArray = $tab;
        return $tab;
    }

    /************************************************************
     * export func
     * export the minimal requested members of "sysma relation type parameter"
     * 
     */
    public function getExport()
    {
        $srtp = new StdClass;

        // Id shouln't be necessary IIRC but let's export it for now.
        $srtp->parameter_id             = $this->parameter_id;
        $srtp->sysma_relation_type_id   = $this->sysma_relation_type_id;

        $srtp->parameter                = $this->parameter;
        $srtp->description              = $this->description;

        $srtp->data_type                = $this->data_type;
        $srtp->ordered_by               = $this->ordered_by;
        $srtp->created_at               = $this->created_at;
        $srtp->modified_at              = $this->modified_at;
        $srtp->parameter_source         = $this->parameter_source;
        $srtp->data_source              = $this->data_source;
        $srtp->unit                     = $this->unit;
        $srtp->required                 = $this->required;
        $srtp->parameter_alias          = $this->parameter_alias;
        // $satp->time_tracking            = $this->time_tracking;
        $srtp->track_historytables_id   = $this->track_historytables_id;

        $srtp->modified_by              = $this->modified_by;
        $srtp->required_field_parameter = $this->required_field_parameter;
        $srtp->hidden_in_form           = $this->hidden_in_form;

        // $sotp->choices = [];
        // foreach($this->choicesArray as $choice) {
        //     $sotp->choices[] = $choice;
        // }
        if (!empty($this->choicesArray)) {
            $srtp->choices = [];
            $idx = 0;
            foreach ($this->choicesArray as $choice) {
                $srtp->choices[$idx] = new stdClass();
                $srtp->choices[$idx]->choice_id = $choice['choice_id'];
                $srtp->choices[$idx]->choice = $choice['nom'];
                $srtp->choices[$idx]->description = $choice['description'];
                $srtp->choices[$idx]->display_order = $choice['display_order'];
                $srtp->choices[$idx]->value_type = $choice['value_type'];
                $idx++;
            }
        }
        return $srtp;
    }
}
