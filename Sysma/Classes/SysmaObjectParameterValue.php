<?php

/**
 * Description of SysmaObjectParameterValue
 * 
 * @author srenou
 */
class SysmaObjectParameterValue extends ParameterValue
{

    protected $type = 'sysma_object';
    protected $typeId = 'sysma_object_id';
    protected $typeTableDonnees = 'sysma_object_data';
    protected $typeTableChoixparameters = 'sysma_object_type_parameter_choice';

    public function __construct($id = null, $app = null)
    {

        $this->pdo = $app['pdo'];
        if ($id != null and is_numeric($id)) {

            $qry = $this->pdo->prepare(""
                . "select *,d.start_date as start_date "
                . "from " . SCHEMA . ".sysma_object_data d "
                . "LEFT JOIN " . SCHEMA . ".user u on d.modified_by = u.user_id "
                . "FULL JOIN " . SCHEMA . ".sysma_object_type_parameter t on d.parameter_id = t.parameter_id "
                . "where "
                . "d.data_index = :id");
            $qry->bindParam(':id', $id, PDO::PARAM_INT);
            $qry->execute();

            $compt = 0;

            while ($v = $qry->fetchObject()) {

                if ($compt === 0) {

                    $this->idsysma_object = $v->sysma_object_id;
                    $this->parameter_id = $v->parameter_id;
                    $this->parameter = $v->parameter;
                    $this->unit = $v->unit;
                    $this->start_date = $v->start_date;
                    $this->end_date = $v->end_date;
                    $this->creation_date = $v->created_at;
                    $this->iduser = $v->modified_by;
                    $this->data_index = $v->data_index;
                    $this->parameter = new SysmaObjectParameter($this->parameter_id, $app);
                    $this->firstname = $v->firstname;
                    $this->lastname = $v->lastname;
                }

                // cas d'un choice mutliple, plusieurs réponses possibles
                if ($v->data_type == 'multipleTextChoiceType') {

                    $this->value[$compt] = $v->value;
                    $compt++;
                }
                // value unique
                else {
                    $this->value = $v->value;
                }

                $this->loadParameterDescription($app);
            }


            return true;
        }
    }

    /*
     * function buildFromParameters  
     * 
     * 
     * @param int $idsysma_object
     * @param int $parameter_id
     * @param string $dateValidite
     * @param mixed $app silex app
     *
     *
     */

    public function buildFromParameters($idsysma_object, $parameter_id, $dateValidite = null, $app)
    {


        $this->pdo = $app['pdo'];
        $P = new SysmaObjectParameter(intval($parameter_id), $app);

        $this->dateValidite = $dateValidite;


        if ($idsysma_object != null and $parameter_id != null) {

            // Last data

            if (is_numeric($idsysma_object) and $idsysma_object != null and is_numeric($parameter_id) and $parameter_id != null and $dateValidite == null) {

                // selection de la value la plus récente pour l'objet et parameter dont la date de fin est nulle (value encore valable)
                // Rq le classement par odre de date début est normalement inutile, étant donné que pour un objet et un paramtre donné, il de doit
                // y avoir qu'une seule value dont la date de fin est nulle

                $inst = "select * from " . SCHEMA . ".sysma_object_data LEFT JOIN " . SCHEMA . ".user u on sysma_object_data.modified_by = u.user_id where sysma_object_id = :sysma_object_id and parameter_id = :parameter_id and (end_date is NULL or end_date = '0') order by start_date DESC";
            }

            // data for a date

            if (is_numeric($idsysma_object) and $idsysma_object != null and is_numeric($parameter_id) and $parameter_id != null and isADate($dateValidite)) {

                // select the value that has begun just before the $dateValidite date or that has ended just after (or has not ended yet)               

                $inst = "
				select * from " . SCHEMA . ".sysma_object_data 
				where sysma_object_id = :sysma_object_id
				and parameter_id = :parameter_id 
				and 
				((start_date <= :date_validite and end_date  >= :date_validite and end_date  != '0') 
				or (start_date <= :date_validite and end_date IS NULL ) 
                                or (start_date <= :date_validite and end_date = '0' ))     
				order by start_date DESC";
            }


            $qry = $this->pdo->prepare($inst);
            $qry->bindParam(':sysma_object_id', $idsysma_object, PDO::PARAM_INT);
            $qry->bindParam(':parameter_id', $parameter_id, PDO::PARAM_INT);
            if (isADate($dateValidite))
                $qry->bindParam(':date_validite', $dateValidite, PDO::PARAM_STR);

            $qry->execute();

            // no data
            if ($qry->rowCount() == 0) {

                $this->idsysma_object = $idsysma_object;
                $this->parameter_id = $parameter_id;
                $this->value = null;
                $this->start_date = 0;
                $this->end_date = 0;
                $this->parameter = $P;
            } else {

                // one value
                if ($P->dataType != 'multipleTextChoiceType') {

                    // traitement de la value
                    $v = $qry->fetchObject();
                    $this->idsysma_object = $v->sysma_object_id;
                    $this->parameter_id = $v->parameter_id;
                    $this->start_date = $v->start_date;
                    $this->end_date = $v->end_date;
                    $this->value = $v->value;
                    $this->creation_date = $v->start_date;
                    $this->iduser = $v->modified_by;
                    $this->firstname = @$v->firstname;
                    $this->lastname = @$v->lastname;
                    $this->data_index = $v->data_index;
                    $this->parameter = $P;
                    $this->loadParameterDescription($app);
                }

                // multiple values
                if ($P->dataType == 'multipleTextChoiceType') {

                    $compt = 0;
                    while ($v = $qry->fetchObject()) {

                        if ($compt === 0) {
                            $this->data_index = $v->data_index;
                            $this->idsysma_object = $v->sysma_object_id;
                            $this->parameter_id = $v->parameter_id;
                            $this->start_date = $v->start_date;
                            $this->end_date = $v->end_date;
                            $this->creation_date = $v->start_date;
                            $this->iduser = $v->modified_by;
                            $this->firstname = $v->firstname;
                            $this->lastname = $v->lastname;
                            $this->parameter = $P;
                        }
                        $this->value[$compt] = $v->value;

                        $compt++;
                    }
                }
            }
        }
    }

    /*
     * @return boolean
     */

    public function hasOldData()
    {


        $qry = $this->pdo->prepare("select * from " . SCHEMA . ".sysma_object_data where sysma_object_id = :sysma_object_id and parameter_id = :parameter_id and end_date != '0' and end_date is not null and end_date != ''");
        $qry->bindParam(':sysma_object_id', $this->idsysma_object, PDO::PARAM_INT);
        $qry->bindParam(':parameter_id', $this->parameter_id, PDO::PARAM_INT);
        $qry->execute();
        if ($qry->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function delete()
    {

        $qry = $this->pdo->prepare('DELETE FROM ' . SCHEMA . '.sysma_object_data where '
            . 'data_index = :data_index and '
            . 'sysma_object_id = :sysma_object_id and '
            . 'parameter_id = :parameter_id and '
            . 'start_date = :start_date and '
            . 'end_date = :end_date');
        $qry->bindParam(':data_index', $this->data_index);
        $qry->bindParam(':sysma_object_id', $this->idsysma_object);
        $qry->bindParam(':parameter_id', $this->parameter_id);
        $qry->bindParam(':start_date', $this->start_date);
        $qry->bindParam(':end_date', $this->end_date);


        return returnResultQuery($qry, $this->pdo, null);
    }

    public function multipleModifyByValues(SysmaObjectParameterValue $_sopv, Array $_values) {
        // Debug::log(__FUNCTION__);
        // clear previous records
        $res = $this->delete();
      

        $newIndex = dataIndex($this->pdo);

        // insert the new ones
        foreach ($_values as $value) {
            // $r['parameter_id'] = $_sopv->parameterId;
            // $r['sysma_object_id'] = $_sopv->sysmaObjectId;
            // $r['start_date'] = $_sopv->startDate;
            // $r['end_date'] = $_sopv->endDate;
            // $r['modified_at'] = $_sopv->modifiedAt;
            // $r['data_index'] = $newIndex;
            // $r['value'] = $value;
            // $r['user_id'] = $_sopv->userId;
            // $r['modified_by'] = $_sopv->modifiedBy;
            $r['parameter_id'] = $_sopv->parameter_id;
            $r['sysma_object_id'] = $_sopv->idsysma_object;
            $r['start_date'] = $_sopv->start_date;
            $r['end_date'] = $_sopv->end_date;
            $r['modified_at'] = $_sopv->modified_at;
            $r['data_index'] = $newIndex;
            $r['value'] = $value;
            $r['user_id'] = $_sopv->iduser;
            $r['modified_by'] = $_sopv->modified_by;

            //var_dump($r) ;
            $res = $this->create($r);
        }

       

        return $res; // derniere insertion
    }

    public function multipleModify($request)
    {

        $this->loadChoices($this->parameter->dataType);

        $res = $this->delete();
        $nouvelIndex = dataIndex($this->pdo);

        foreach ($this->choicesArray as $choice) {

            $inputName = 'choice' . $choice['choice_id'];

            if ($request->get($inputName) == 1) {
                // $rc['data'] = __FUNCTION__.' !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! '.$request->get('parameter_id');
                // $rc['message'] = $request;
                // return json_encode($rc);

                $r2['parameter_id'] = $request->get('parameter_id');
                $r2['sysma_object_id'] = $request->get('sysma_object_id');
                $r2['start_date'] = $request->get('start_date');
                $r2['end_date'] = $request->get('end_date');
                $r2['modified_at'] = $request->get('modified_at');
                $r2['data_index'] = $nouvelIndex;
                $r2['value'] = $choice['choice'];
                $r2['user_id'] = $request->get('user_id');
                $r2['modified_by'] = $request->get('modified_by');

                $res = $this->create($r2);
            }
        }

        return $res; // derniere insertion
    }

    /*
     * sysma update function means : new value which starts from the precised date, and backup of the old value, with an end date = precised date   
     * 
     *     
     * @return string
     */

    public function update($request)
    {


        foreach ($request as $key => $val) {
            $r[$key] = $val;
        }

        // update the end date of the actual value
        $end_dateUpdate = array('data_index' => $r['data_index'], 'end_date' => $r['start_date']);
        $this->modify($end_dateUpdate);

        // insert the new value
        $newValueInsert = array(
            'sysma_object_id' => $r['sysma_object_id'],
            'parameter_id' => $r['parameter_id'],
            'start_date' => $r['start_date'],
            'end_date' => $r['end_date'],
            'modified_at' => $r['modified_at'],
            'user_id' => $r['user_id'],
            'modified_by' => $r['modified_by'],
            'data_index' => dataIndex($this->pdo),
            'value' => $r['value']
        );

        return $this->create($newValueInsert);
    }



    public function multipleUpdateByValues(SysmaObjectParameterValue $_sopv, array $_values)
    {

        // update the end date of the actual values
        $end_dateUpdate = array('data_index' => $this->data_index, 'end_date' => $_sopv->start_date);
        $this->modify($end_dateUpdate);

        $rctab['status'] = 'success';
        $rc = json_encode($rctab);
        $nouvelIndex = dataIndex($this->pdo);

        // insert the new values
        foreach ($_values as $value) {
            $r['parameter_id'] = $_sopv->parameter_id;
            $r['sysma_object_id'] = $_sopv->idsysma_object;
            $r['start_date'] = $_sopv->start_date;
            $r['end_date'] = $_sopv->end_date;
            $r['modified_at'] = $_sopv->modified_at;
            $r['data_index'] = $nouvelIndex;
            $r['value'] = $value;
            $r['user_id'] = $_sopv->iduser;
            $r['modified_by'] = $_sopv->modified_by;
            $rc = $this->create($r);
        }

        return $rc;
    }


    /*
     * update function for a multiple value parameter
     */

    public function multipleValuesUpdate($request)
    {

        $end_dateUpdate = array('data_index' => $this->data_index, 'end_date' => $request->get('start_date'));
        $this->modify($end_dateUpdate);

        $this->loadChoices($this->parameter->dataType);
        $nouvelIndex = dataIndex($this->pdo);

        foreach ($this->choicesArray as $choice) {

            $inputName = 'choice' . $choice['choice_id'];
            if ($request->get($inputName) == 1) {

                $r2['parameter_id'] = $request->get('parameter_id');
                $r2['sysma_object_id'] = $request->get('sysma_object_id');
                $r2['start_date'] = $request->get('start_date');
                $r2['end_date'] = $request->get('end_date');
                $r2['modified_at'] = $request->get('modified_at');
                $r2['data_index'] = $nouvelIndex;
                $r2['value'] = $choice['choice'];
                $r2['user_id'] = $request->get('user_id');
                $r2['modified_by'] = $request->get('modified_by');


                $res = $this->create($r2);
            }
        }
        return $res;
    }
}
