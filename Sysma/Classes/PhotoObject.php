<?php

/**
 * Description of PhotoObject
 * 
 * A sysma Photo object can be a piwigo photo , piwigo folder (https://fr.piwigo.org/) or an uploaded photo
 * 
 * @author srenou
 * 
 */
class PhotoObject
{

    public function __construct($index, $app)
    {


        if (is_numeric($index) and $index != null) {

            $pdo = $app['pdo'];
            $qry = $pdo->prepare("select * from " . SCHEMA . ".photo_object where photo_object_id = :index");
            $qry->bindParam(':index', $index, PDO::PARAM_INT);
            $qry->execute();

            if ($qry->rowCount() == 1) {

                $res = $qry->fetchObject();
                $this->index = $index;

                foreach ($res as $key => $val) {
                    $this->$key = $val;
                }


                if ($res->photo_type == 'photo') {
                    $P = new PiwigoPhoto($res->photo_id, $app);
                    $this->Photo = $P;
                } elseif ($res->photo_type == 'folder') {
                    $D = new PiwigoFolder($res->photo_id, $app);
                    $this->PiwigoFolder = $D;
                } elseif ($res->photo_type == 'photo_upload') {
                    $this->Photo = (object) null;

                    $this->Photo->large = '/' . PHOTO_UPLOAD_PATH . '/' . $res->photo_file . '.jpg';
                    $this->Photo->thumbnail = '/' . PHOTO_UPLOAD_PATH . '/' . $res->photo_file . '_thumbnail.jpg';
                    $this->Photo->square = '/' . PHOTO_UPLOAD_PATH . '/' . $res->photo_file . '_square.jpg';
                    $this->Photo->thumbnailCode = '<a href="' . $this->Photo->large . '" target="_blank"><img src="' . $this->Photo->thumbnail . '" width="200px" data-src="' . $this->Photo->thumbnail . '" /></a><br>' . $this->photo_title . ' - ' . $this->photo_date;
                    $this->Photo->squareCode = ' <a href="' . $this->Photo->large . '" target="_blank"><img src="' . $this->Photo->square . '" width="150px" data-src="' . $this->Photo->square . '" /></a><br>' . $this->photo_title . '<br>' . dateDisplay($this->photo_date) . '<br>[' . $this->photo_object_id . ']';
                    if (file_exists(ROOT_URI . '/public/' . PHOTO_UPLOAD_PATH . '/' . $res->photo_file . '.jpg')) {
                        $exif = exif_read_data(ROOT_URI . '/public/' . PHOTO_UPLOAD_PATH . '/' . $res->photo_file . '.jpg');
                        $this->Photo->exif_orientation = @$exif['Orientation'];
                    }
                }
            }
        }
    }

    public function create($app, $request)
    {


        $pp = prepInsertQuery($request);
        $pdo = $app['pdo'];
        $qry = $pdo->prepare("insert into " . SCHEMA . ".photo_object (" . $pp['reqFields'] . ") VALUES (" . $pp['reqParams'] . ")");

        foreach ($pp['tabD'] as $key => $val) {
            $qry->bindValue($key, $val);
        }
        return returnResultQuery($qry, $pdo, $pp['tabD']);
    }

    public function update($app, $request)
    {

        //var_dump($request);

        $pp = prepUpdateQuery($request, 'photo_object_id');

        $pdo = $app['pdo'];
        $qry = $pdo->prepare("update " . SCHEMA . ".photo_object set " . $pp['req'] . " where photo_object_id = :photo_object_id");

        foreach ($pp['tabD'] as $key => $val) {
            $qry->bindValue($key, $val);
        }

        return returnResultQuery($qry, $pdo, $pp['tabD']);
    }

    public function delete($app)
    {


        $pdo = $app['pdo'];
        $qry = $pdo->prepare("DELETE FROM " . SCHEMA . ".photo_object where photo_object_id = :photo_object_id");
        $qry->bindParam(':photo_object_id', $this->photo_object_id, PDO::PARAM_INT);
        if ($qry->execute()) {
            return true;
        } else {
            return false;
        }
    }
}
