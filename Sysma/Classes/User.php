<?php

/**
 * Description of User
 *  
 *
 * @author srenou
 * 
 */
class User
{

    public $user;
    public $id;
    public $organisation_id;
    public $registration_date;

    public function __construct($id, $app)
    {


        $this->app = $app;
        $this->pdo = $this->app['pdo'];

        if ($id !== null and is_numeric($id)) {
            $qry = $this->pdo->prepare("select * from " . SCHEMA . ".user where user_id = :id");
            $qry->bindParam(':id', $id, PDO::PARAM_INT);
            if ($qry->execute() and $qry->rowCount() > 0) {
                $u = $qry->fetchObject();

                foreach ($u as $key => $val) {
                    $this->$key = $val;
                }
                $this->id = $this->user_id;

                $this->snap_options = json_decode($u->snap_options);
                $this->loadTA();
                $this->loadFilters();
                $this->loadGroupInfo();
                $this->loadUserViews();

                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function resetUserActualView($app)
    {

        $app['session']->set('userObjectTypeSelection', null) ;
        $app['session']->set('userSysmaActionTypeSelection', null) ;
        $app['session']->set('userOtherLayersSelection', null) ;
        $this->deleteOtherBaseLayers();
        $this->removeAllOtherLayersFromUserSelection();
        $this->deleteSelectedLayers();
        $this->deleteUserFilters();
        $this->deleteUserTAs();
        $this->removeAllObjectTypeLayersFromUserSelection();
        $this->removeAllSysmaActionTypeLayersFromUserSelection();
        return true;
    }

    public function loadUserViewAsActualView($UV)
    {

        $options = json_decode($UV->view_json);

        updateDefaultCoords($this->app, $this->id, $options->center, $options->zoom);
        updateBaseLayer($this->app, $this->id, $options->baseLayer);

        $this->deleteOtherBaseLayers();
        foreach ($options->refLayers as $o) {
            addOtherBaseLayer($this->app, $this->id, $o->layer);
        }

        $this->removeAllOtherLayersFromUserSelection();
        foreach ($options->othersLayers as $o) {
            if ($o->is_displayed_by_user == true) {
                $this->addOtherLayerToUserSelection($o->type, $o->id, ['layerstyle' => $o->style, 'parentid' => null, 'leaflet_id' => null, 'layer' => null, 'options' => null]);
            }
        }


        $this->deleteSelectedLayers();
        $this->deleteUserFilters();
        $this->deleteUserTAs();


        // objects

        $this->removeAllObjectTypeLayersFromUserSelection();
        foreach ($options->OTlayers as $o) {
            if ($o->style->is_user_style == 1) {
                $S = new Style('sysma_object', $o->sysma_object_type_id, $this->id, $this->app);
                $S->update(json_encode(get_object_vars($o->style)));
            }
            if ($o->is_diplayed_by_user == true) {
                $this->addObjectTypeLayerToUserSelection($o->sysma_object_type_id, null);
            }
        }
        if (!empty($options->filtersConditions->sysma_object)) {
            foreach ($options->filtersConditions->sysma_object as $idto => $cond) {
                foreach ($cond as $idf => $condition) {
                    $F = new SysmaFilter($idf, $this->app);
                    $F->activateWithCondition($this->id, $idto, 0, $condition->condition);
                }
            }
        }


        // actions

        $this->removeAllSysmaActionTypeLayersFromUserSelection();
        foreach ($options->WTlayers as $o) {
            if ($o->style->is_user_style == 1) {
                $S = new Style('sysma_action', $o->sysma_action_type_id, $this->id, $this->app);
                $S->update(json_encode(get_object_vars($o->style)));
            }
            if ($o->is_diplayed_by_user == true) {
                $this->addSysmaActionTypeLayerToUserSelection($o->sysma_action_type_id, null);
            }
        }

        if (!empty($options->filtersConditions->sysma_action)) {
            foreach ($options->filtersConditions->sysma_action as $idtt => $cond) {
                foreach ($cond as $idf => $condition) {
                    $F = new SysmaFilter($idf, $this->app);
                    $F->activateWithCondition($this->id, 0, $idtt, $condition->condition);
                }
            }
        }


        if (!empty($options->TAs)) {
            foreach ($options->TAs as $type => $TA) {
                foreach ($TA as $idto => $idta) {
                    $TA = new ThematicAnalysis($idta, $this->app);
                    $TA->activate($this->id);
                }
            }
        }

        // print

        $this->app['session']->remove('print') ;

        $sessionPrintInfos = new stdClass();
        if (isset($options->print)) {
            $sessionPrintInfos = $options->print;
        }
        $sessionPrintInfos->title = $UV->view;

        $this->app['session']->set('print', $sessionPrintInfos);


        return true;
    }

    public function loadUserViews()
    {

        $this->tabUserViews = null;

        $qry = $this->pdo->prepare("select * from " . SCHEMA . ".user_views l where user_id = :id order by view asc");
        $qry->bindParam(':id', $this->id, PDO::PARAM_INT);
        $qry->execute();
        while ($u = $qry->fetchObject()) {
            $this->tabUserViews[$u->user_view_id] = $u->view;
        }
    }

    public function loadGroupInfo()
    {

        $this->tabGroupInfo = null;

        $qry = $this->pdo->prepare("select * from " . SCHEMA . ".l_group_user l where user_id = :id");
        $qry->bindParam(':id', $this->id, PDO::PARAM_INT);
        $qry->execute();
        while ($u = $qry->fetchObject()) {
            $this->tabGroupInfo[$u->group_name] = $u->l_group_user_action;
        }
    }

    public function loadTA()
    {

        $this->tabAT = null;

        $qry = $this->pdo->prepare("select at.theme_analysis_id as theme_analysis_id, at.object_type as object_type, at.type_id as id_type from " . SCHEMA . ".l_theme_analysis_user l LEFT JOIN " . SCHEMA . ".theme_analysis at ON at.theme_analysis_id = l.theme_analysis_id where l.user_id = :id");
        $qry->bindParam(':id', $this->id, PDO::PARAM_INT);
        $qry->execute();
        while ($u = $qry->fetchObject()) {
            $this->tabAT[$u->object_type][$u->id_type] = $u->theme_analysis_id;
        }
    }

    public function loadFilters()
    {

        $this->tabFiltres = $this->tabFiltresCond = null;

        $qry = $this->pdo->prepare("select f.filter_id as filter_id, f.object_type as object_type, l.sysma_object_type_id as sysma_object_type_id, l.sysma_action_type_id as sysma_action_type_id, l.condition as condition from " . SCHEMA . ".l_filter_user l LEFT JOIN " . SCHEMA . ".filter f ON f.filter_id = l.filter_id where l.user_id = :id order by l_filter_user_id asc");
        $qry->bindParam(':id', $this->id, PDO::PARAM_INT);
        $qry->execute();
        while ($u = $qry->fetchObject()) {
            if ($u->object_type === 'sysma_object') {
                $this->tabFiltres[$u->object_type][$u->sysma_object_type_id][] = $u->filter_id;
                $this->tabFiltresCond[$u->object_type][$u->sysma_object_type_id][$u->filter_id]['condition'] = $u->condition;
            }
            if ($u->object_type === 'sysma_action') {
                $this->tabFiltres[$u->object_type][$u->sysma_action_type_id][] = $u->filter_id;
                $this->tabFiltresCond[$u->object_type][$u->sysma_action_type_id][$u->filter_id]['condition'] = $u->condition;
            }
        }
    }

    public function layersRef()
    {

        $qry = $this->pdo->prepare("select layer as layer from " . SCHEMA . ".l_ref_layer_user where user_id = :id");
        $qry->bindParam(':id', $this->id, PDO::PARAM_INT);
        $qry->execute();
        return $qry->fetchAll();
    }

    public function setMapWidth($w)
    {

        $this->map_width = intval($w);
    }

    public function isAdmin()
    {


        $qry = $this->pdo->prepare("select * from " . SCHEMA . ".user where user_id = :id and 'administrateur' = ANY(privileges)");
        $qry->bindParam(':id', $this->id, PDO::PARAM_INT);
        $qry->execute();
        if ($qry->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function updateLastConnexion()
    {
        $qry = $this->pdo->prepare("update " . SCHEMA . ".user set last_access = '" . date('Y-m-d H:i:s') . "' where user_id = :id");
        $qry->bindParam(':id', $this->id, PDO::PARAM_INT);
        $qry->execute();
        return true;
    }

    public function updatePasswordHash($_newPassword)
    {
        $qry = $this->pdo->prepare("update " . SCHEMA . ".user set password = '" . $_newPassword . "' where user_id = :id");
        $qry->bindParam(':id', $this->id, PDO::PARAM_INT);
        $qry->execute();
        return true;
    }

    public function tracksql_filter($appli)
    {


        $qry = $this->pdo->prepare("insert into " . SCHEMA . ".connection (user_id,\"user\", app, connection_date, connection_time) VALUES (:id,:util,:app,'" . date('Y-m-d') . "','" . date('H:i:s') . "')");
        $qry->bindParam(':id', $this->id, PDO::PARAM_INT);
        $qry->bindParam(':util', $this->user, PDO::PARAM_STR);
        $qry->bindParam(':app', $app, PDO::PARAM_STR);
        return $qry->execute();
    }

    public function insert($request)
    {


        $pp = prepInsertQuery($request);


        $qry = $this->pdo->prepare("insert into " . SCHEMA . ".user (" . $pp['reqFields'] . ") VALUES (" . $pp['reqParams'] . ") RETURNING user_id as id");

        foreach ($pp['tabD'] as $key => $val) {
            $qry->bindValue($key, $val);
        }

        return returnResultQuery($qry, $this->pdo, $pp['tabD']);
    }

    public function update($request)
    {

        $pp = prepUpdateQuery($request, 'user_id');

        $qry = $this->pdo->prepare("update " . SCHEMA . ".user set " . $pp['req'] . " where user_id = :user_id");


        foreach ($pp['tabD'] as $key => $val) {
            $qry->bindValue($key, $val);
        }
        return returnResultQuery($qry, $this->pdo, $pp['tabD']);
    }

    public function loadRights()
    {


        $qry = $this->pdo->prepare("select * from " . SCHEMA . ".user_privilege where  user_id = :user_id");
        $qry->bindParam('user_id', $this->id);
        $qry->execute();
        return $qry->fetchAll();
    }

    public function insertRights($request)
    {

        $pp = prepInsertQuery($request);


        $qry = $this->pdo->prepare("insert into " . SCHEMA . ".user_privilege (" . $pp['reqFields'] . ") VALUES (" . $pp['reqParams'] . ")");

        foreach ($pp['tabD'] as $key => $val) {
            $qry->bindValue($key, $val);
        }

        return returnResultQuery($qry, $this->pdo, $pp['tabD']);
    }

    public function updateRights($request)
    {

        $pp = prepUpdateQuery($request, 'user_privilege_id');


        $qry = $this->pdo->prepare("update " . SCHEMA . ".user_privilege set " . $pp['req'] . " where user_privilege_id = :user_privilege_id");


        foreach ($pp['tabD'] as $key => $val) {
            $qry->bindValue($key, $val);
        }

        return returnResultQuery($qry, $this->pdo, $pp['tabD']);
    }

    public function deleteRights($user_privilege_id)
    {



        $qry = $this->pdo->prepare("delete from " . SCHEMA . ".user_privilege where user_privilege_id = :user_privilege_id and user_id = :id");
        $qry->bindParam(':user_privilege_id', $user_privilege_id, PDO::PARAM_INT);
        $qry->bindParam(':id', $this->id, PDO::PARAM_INT);
        $qry->execute();
        return true;
    }

    public function deleteOtherBaseLayers()
    {

        $qry = $this->pdo->prepare('delete from ' . SCHEMA . '.l_ref_layer_user where user_id = :id');
        $qry->bindParam(':id', $this->id, PDO::PARAM_INT);
        return $qry->execute();
    }

    /*
     * suppression des layers sélectionnés par l'user lorsqu'on change les privilege_list par exemple
     */

    public function deleteSelectedLayers()
    {


        $qry = $this->pdo->prepare('delete from ' . SCHEMA . '.l_sysma_object_type_user where user_id = :id');
        $qry->bindParam(':id', $this->id, PDO::PARAM_INT);
        $qry->execute();
        $qry = $this->pdo->prepare('delete from ' . SCHEMA . '.l_sysma_action_type_user where user_id = :id');
        $qry->bindParam(':id', $this->id, PDO::PARAM_INT);
        $qry->execute();
        return true;
    }

    public function deleteUserFilters()
    {

        $qry = $this->pdo->prepare('delete from ' . SCHEMA . '.l_filter_user where user_id = :id');
        $qry->bindParam(':id', $this->id, PDO::PARAM_INT);
        return $qry->execute();
    }

    public function deleteUserTAs()
    {

        $qry = $this->pdo->prepare('delete from ' . SCHEMA . '.l_theme_analysis_user where user_id = :id');
        $qry->bindParam(':id', $this->id, PDO::PARAM_INT);
        return $qry->execute();
    }

    public function addInfosToUserSession($index, $layerId, $value)
    {

        $sessionVal = $this->app['session']->get($index);
        $sessionVal[$layerId] = $value;
        $this->app['session']->set($index, $sessionVal);
    }

    public function removeInfosFromUserSession($index, $layerId)
    {

        $sessionVal = $this->app['session']->get($index);
        unset($sessionVal[$layerId]);
        $this->app['session']->set($index, $sessionVal);
    }

    public function addObjectTypeLayerToUserSelection($layerId, $options)
    {

        if (countSysmaObjects($this->app, $layerId) == 0)
            return false;

        $this->addInfosToUserSession('userObjectTypeSelection', $layerId, ['selected' => true, 'layerstyle' => $options['layerstyle'], 'parentid' => $options['parentid'], 'layer' => $options['layer'], 'leaflet_id' => $options['leaflet_id'], 'options' => $options['options']]);
        $qry = $this->pdo->prepare('insert into ' . SCHEMA . '.l_sysma_object_type_user (sysma_object_type_id, user_id) select :id,:userid where not exists (select * from ' . SCHEMA . '.l_sysma_object_type_user where sysma_object_type_id = :id and user_id = :userid)');
        $qry->bindParam(':id', $layerId);
        $qry->bindParam('userid', $this->id, PDO::PARAM_INT);
        return $qry->execute();
    }

    public function removeObjectTypeLayerFromUserSelection($layerId)
    {

        $this->removeInfosFromUserSession('userObjectTypeSelection', $layerId);

        $qry = $this->pdo->prepare('delete from ' . SCHEMA . '.l_sysma_object_type_user where sysma_object_type_id = :id and user_id = :userid');
        $qry->bindParam(':id', $layerId);
        $qry->bindParam('userid', $this->id, PDO::PARAM_INT);
        return $qry->execute();
    }

    public function removeAllObjectTypeLayersFromUserSelection()
    {

        $qry = $this->pdo->prepare('delete from ' . SCHEMA . '.l_sysma_object_type_user where user_id = :userid');
        $qry->bindParam('userid', $this->id, PDO::PARAM_INT);
        return $qry->execute();
    }

    public function addSysmaActionTypeLayerToUserSelection($layerId, $options)
    {


        if (countSysmaActions($this->app, $layerId) == 0)
            return false;

        $this->addInfosToUserSession('userSysmaActionTypeSelection', $layerId, ['selected' => true, 'layerstyle' => $options['layerstyle'], 'parentid' => $options['parentid'], 'layer' => $options['layer'], 'options' => $options['options']]);
        $qry = $this->pdo->prepare('insert into ' . SCHEMA . '.l_sysma_action_type_user (sysma_action_type_id, user_id) select :id,:userid where not exists (select * from ' . SCHEMA . '.l_sysma_action_type_user where sysma_action_type_id = :id and user_id = :userid)');
        $qry->bindParam(':id', $layerId);
        $qry->bindParam('userid', $this->id, PDO::PARAM_INT);
        return $qry->execute();
    }

    public function removeSysmaActionTypeLayerFromUserSelection($layerId)
    {

        $this->removeInfosFromUserSession('userSysmaActionTypeSelection', $layerId);

        $qry = $this->pdo->prepare('delete from ' . SCHEMA . '.l_sysma_action_type_user where sysma_action_type_id = :id and user_id = :userid');
        $qry->bindParam(':id', $layerId);
        $qry->bindParam(':userid', $this->id, PDO::PARAM_INT);
        return $qry->execute();
    }

    public function removeAllSysmaActionTypeLayersFromUserSelection()
    {

        $qry = $this->pdo->prepare('delete from ' . SCHEMA . '.l_sysma_action_type_user where user_id = :userid');
        $qry->bindParam(':userid', $this->id, PDO::PARAM_INT);
        return $qry->execute();
    }

    public function removeGroupFromUserSelection($group)
    {

        $qry = $this->pdo->prepare('delete from ' . SCHEMA . '.l_group_user where group_name = :group and user_id = :userid');
        $qry->bindParam(':group', $group);
        $qry->bindParam(':userid', $this->id, PDO::PARAM_INT);
        return $qry->execute();
    }

    public function updateGroupInfoForUser($group, $action)
    {

        if ($this->removeGroupFromUserSelection($group)) {
            $qry = $this->pdo->prepare('insert into ' . SCHEMA . '.l_group_user (group_name,user_id,l_group_user_action) VALUES (:group,:userid,:action)');
            $qry->bindParam(':group', $group);
            $qry->bindParam(':userid', $this->id, PDO::PARAM_INT);
            $qry->bindParam(':action', $action);
            return $qry->execute();
        } else {
            return false;
        }
    }

    public function addOtherLayerToUserSelection($type, $layer, $options)
    {

        $this->addInfosToUserSession('userOtherLayersSelection', $layer, ['selected' => true, 'layerstyle' => $options['layerstyle'], 'parentid' => $options['parentid'], 'leaflet_id' => $options['leaflet_id'], 'layer' => $options['layer'], 'options' => $options['options']]);

        $qry = $this->pdo->prepare('insert into ' . SCHEMA . '.l_other_layer_user (layer_type, layer, user_id) select :type::text,:layer::text,:userid::integer where not exists (select * from ' . SCHEMA . '.l_other_layer_user where layer_type = :type::text and layer = :layer::text and user_id = :userid::integer)');

        $qry->bindParam(':type', $type);
        $qry->bindParam(':layer', $layer);
        $qry->bindParam(':userid', $this->id, PDO::PARAM_INT);
        return $qry->execute();
    }

    public function removeOtherLayerFromUserSelection($type, $layer)
    {

        $this->removeInfosFromUserSession('userOtherLayersSelection', $layer);

        $qry = $this->pdo->prepare('delete from ' . SCHEMA . '.l_other_layer_user where layer_type = :type and layer = :layer and user_id = :userid');
        $qry->bindParam(':type', $type);
        $qry->bindParam(':layer', $layer);
        $qry->bindParam('userid', $this->id, PDO::PARAM_INT);
        return $qry->execute();
    }

    public function removeAllOtherLayersFromUserSelection()
    {

        $qry = $this->pdo->prepare('delete from ' . SCHEMA . '.l_other_layer_user where user_id = :userid');
        $qry->bindParam('userid', $this->id, PDO::PARAM_INT);
        return $qry->execute();
    }

    public function loadLayerManager()
    {

        $objectTypesLayers = $this->loadObjectTypesLayers();

        $workTypesLayers = [];
        foreach ($objectTypesLayers as $objectTypeLayer) {
            $workTypesLayers[$objectTypeLayer['sysma_object_type_id']] = $this->loadSysmaActionTypesLayers($objectTypeLayer['sysma_object_type_id']);
        }
        $otherLayers = $this->loadOtherLayers(['sql' => 'layer_type in (\'piwigo_layer\', \'simple_pg_layer\', \'external_geojson_layer\')']);
        return ['objectTypeLayers' => $objectTypesLayers, 'workTypeLayers' => $workTypesLayers, 'otherLayers' => $otherLayers];
    }

    /*
     * returns all object types layers with user selection and personalized styles
     */

    public function loadObjectTypesLayers()
    {


        $qry = $this->pdo->prepare(
            'select tyo.sysma_object_type_id, tyo.sysma_object_type, '
                . 'xx_99_utils.json_merge(xx_99_utils.json_merge(tyo.style,s.style::text::json), case when s.style::text is not null then \'{"is_user_style":1}\'::text::json else \'{"is_user_style":0}\'::text::json end) as style, '
                . 'tyo.geometry_type, tyo.group_alias, tyo.load_on_pan, g.group_name as group, '
                . 'lu.user_id, '
                . 'case when lu.user_id = :userid then true else false end as is_diplayed_by_user '
                . 'from ' . SCHEMA . '.sysma_object_type as tyo '
                . 'LEFT JOIN ' . SCHEMA . '.l_sysma_object_type_user as lu ON (tyo.sysma_object_type_id = lu.sysma_object_type_id and lu.user_id = :userid) '
                . 'LEFT JOIN ' . SCHEMA . '.l_style_user s on (s.user_id = :userid and s.object_type=\'sysma_object\' and s.type_id = tyo.sysma_object_type_id) '
                . 'LEFT JOIN ' . SCHEMA . '.layer_group g on g.group_alias = tyo.group_alias '
                . ''
                . 'group by tyo.sysma_object_type_id, tyo.sysma_object_type, tyo.geometry_type, g.group_display_order, g.group_name, lu.user_id, s.style::text '
                . 'order by g.group_display_order, tyo.sysma_object_type asc'
        );


        $qry->bindParam('userid', $this->id, PDO::PARAM_INT);
        $qry->execute();
        $tab = array();
        while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
            $row['style'] = json_decode($row['style']);
            $tab[] = $row;
        }
        return $tab;
    }

    /*
     * returns all work types layers with user selection and personalized styles
     */

    public function loadSysmaActionTypesLayers($typeObjectId = null)
    {

        $sql_options = null;
        if ($typeObjectId != null)
            $sql_options = 'and t.sysma_object_type_id = :id';

        $qry = $this->pdo->prepare(
            'select t.sysma_action_type_id, t.sysma_action_type, t.load_on_pan, '
                . 'xx_99_utils.json_merge(xx_99_utils.json_merge(t.style,s.style), '
                . 'case when s.style::text is not null then \'{"is_user_style":1}\'::text::json else \'{"is_user_style":0}\'::text::json end) as style, '
                . 'lu.user_id,'
                . 'case when lu.user_id = :userid then true else false end as is_diplayed_by_user '
                . 'from ' . SCHEMA . '.sysma_action_type as t '
                . 'LEFT JOIN ' . SCHEMA . '.l_sysma_action_type_user as lu ON (t.sysma_action_type_id = lu.sysma_action_type_id and lu.user_id = :userid) '
                . 'LEFT JOIN ' . SCHEMA . '.l_style_user s on (s.user_id = :userid and s.object_type=\'sysma_action\' and s.type_id = t.sysma_action_type_id) '
                . 'where 1 = 1 ' . $sql_options . ' order by t.sysma_action_type'
        );

        $qry->bindParam(':userid', $this->id, PDO::PARAM_INT);
        if ($typeObjectId != null)
            $qry->bindParam(':id', $typeObjectId, PDO::PARAM_INT);
        $qry->execute();
        $tab = array();
        while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
            $row['style'] = json_decode($row['style']);
            $tab[] = $row;
        }
        //die() ;
        return $tab;
    }

    public function otherLayerIsSelectedForUser($type, $layer)
    {


        $qry = $this->pdo->prepare('select * from ' . SCHEMA . '.l_other_layer_user where user_id = :userid and layer_type = :type and layer = :layer ');
        $qry->bindParam(':userid', $this->id, PDO::PARAM_INT);
        $qry->bindParam(':type', $type);
        $qry->bindParam(':layer', $layer);
        $qry->execute();
        return $qry->rowCount() > 0;
    }

    public function loadOtherLayers($options = null)
    {

        $otherLayers = array();

        foreach (otherLayersList($this->pdo, $options) as $otherLayer) {
            if ($otherLayer['access'] === 'public' or ($otherLayer['access'] === 'restricted' and $this->app['session']->get('user_id') != ANONYMID)) {
                $otherLayers[$otherLayer['layer_id']] = array("id" => $otherLayer['other_layer_id'], "group" => $otherLayer['group'], "type" => $otherLayer['layer_type'], "geolayer_id" => $otherLayer['geolayer_id'], "nom" => $otherLayer['layer_id'], "style" => get_object_vars((object) json_decode($otherLayer['style'])), "geometry_type" => $otherLayer['geometry_type'], "user_id" => $this->otherLayerIsSelectedForUser($otherLayer['layer_type'], $otherLayer['other_layer_id']) ? $this->id : 0, "is_displayed_by_user" => $this->otherLayerIsSelectedForUser($otherLayer['layer_type'], $otherLayer['other_layer_id']));
            }
        }
        return $otherLayers;
    }

    public function loadUserStats()
    {


        $qry = $this->pdo->prepare('select sysma_object_id  from ' . SCHEMA . '.sysma_object where created_by = :userid');
        $qry->bindParam(':userid', $this->id, PDO::PARAM_INT);
        $qry->execute();
        $this->stats['geoobject']['created_by_user'] = $qry->rowCount();

        $qry = $this->pdo->prepare('select sysma_object_id  from ' . SCHEMA . '.sysma_object where modified_by = :userid');
        $qry->bindParam(':userid', $this->id, PDO::PARAM_INT);
        $qry->execute();
        $this->stats['geoobject']['modificated_by_user'] = $qry->rowCount();

        $qry = $this->pdo->prepare('select sysma_action_id from ' . SCHEMA . '.sysma_action where created_by = :userid');
        $qry->bindParam(':userid', $this->id, PDO::PARAM_INT);
        $qry->execute();
        $this->stats['action']['created_by_user'] = $qry->rowCount();

        $qry = $this->pdo->prepare('select sysma_action_id  from ' . SCHEMA . '.sysma_action where modified_by = :userid');
        $qry->bindParam(':userid', $this->id, PDO::PARAM_INT);
        $qry->execute();
        $this->stats['action']['modificated_by_user'] = $qry->rowCount();


        $qry = $this->pdo->prepare('select data_index from ' . SCHEMA . '.sysma_object_data where user_id = :userid');
        $qry->bindParam(':userid', $this->id, PDO::PARAM_INT);
        $qry->execute();
        $this->stats['geoobject_data']['created_by_user'] = $qry->rowCount();


        $qry = $this->pdo->prepare('select data_index  from ' . SCHEMA . '.sysma_action_data where user_id = :userid');
        $qry->bindParam(':userid', $this->id, PDO::PARAM_INT);
        $qry->execute();
        $this->stats['work_data']['created_by_user'] = $qry->rowCount();

        $qry = $this->pdo->prepare('select * from ' . SCHEMA . '.connection where user_id = :userid');
        $qry->bindParam(':userid', $this->id, PDO::PARAM_INT);
        $qry->execute();
        $this->stats['connections']['total'] = $qry->rowCount();
    }

    public function loadActualUserView()
    {

        $options = [
            'center' => $this->default_center,
            'zoom' => $this->default_zoom,
            'baseLayer' => $this->base_layer,
            'refLayers' => $this->layersRef(),
            'othersLayers' => $this->loadOtherLayers(),
            'GroupInfo' => $this->tabGroupInfo,
            'OTlayers' => $this->loadObjectTypesLayers(),
            'WTlayers' => $this->loadSysmaActionTypesLayers(),
            'filters' => $this->tabFiltres,
            'filtersConditions' => $this->tabFiltresCond,
            'TAs' => $this->tabAT
        ];

        return $options;
    }

    public static function removeUsersView($view, $app)
    {

        $q = 'delete from ' . SCHEMA . '.user_views where view = :v';
        $qry = $app['pdo']->prepare($q);
        $qry->bindParam(':v', $view, PDO::PARAM_STR);
        return $qry->execute();
    }
}
