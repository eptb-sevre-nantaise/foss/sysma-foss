<?php

/**
 * Description of SysmaAction 
 * 
 * @author srenou
 * 
 */
class SysmaAction
{

    public function __construct($id = null, $app)
    {

        $this->app = $app;
        $this->pdo = $app['pdo'];

        if (is_numeric($id) and $id != null) {


            $qry = $this->pdo->prepare(""
                . "select *, sysma_action.modified_by as modified_by_sysma_action, sysma_action.modified_at as modified_at_sysma_action,  array_to_json(contracts) as tab_contracts "
                . "from " . SCHEMA . ".sysma_action "
                . "left join " . SCHEMA . ".sysma_action_type on sysma_action.sysma_action_type_id=sysma_action_type.sysma_action_type_id "
                . "left join " . SCHEMA . ".organisation on sysma_action.organisation_id=organisation.organisation_id "
                . "where "
                . "sysma_action_id = :sysma_action_id ");

            $qry->bindParam(':sysma_action_id', $id, PDO::PARAM_INT);
            $qry->execute();
            $ob = $qry->fetchObject();

            foreach ($ob as $key => $val) {
                $this->$key = $val;
            }

            $this->id = $id;
            $this->Status = new Status('sysma_action', $this->status, $app);
            // contracts
            $this->tabC = json_decode($ob->tab_contracts);
            $this->loadContracts();
        }
    }

    private function loadContracts()
    {

        $compt = 0;

        $this->contract = array();
        if ($this->tabC != null) {
            foreach ($this->tabC as $c) {
                $this->contract[$c] = true;
            }
        }
        $this->contracts_labels = null;
        $this->tabcontracts = array();
        foreach (contracts($this->app) as $ctr) {
            if (array_key_exists($ctr['contract'], $this->contract)) {
                $this->tabcontractsVal[$ctr['contract']] = true;
                $this->contracts_labels .= '<span class="contract" >' . str_replace(' ', '&nbsp;', $ctr['contract']) . '</span> ';
            } else {
                $this->tabcontractsVal[$ctr['contract']] = false;
            }
            $this->tabcontracts[$compt]['id'] = $ctr['contract_id'];
            $this->tabcontracts[$compt]['contract'] = $ctr['contract'];
            $compt++;
        }
    }

    public function update($request)
    {


        $pp = prepUpdateQuery($request, 'sysma_action_id');


        $qry = $this->pdo->prepare("update " . SCHEMA . ".sysma_action set " . $pp['req'] . " where sysma_action_id = :sysma_action_id");

        foreach ($pp['tabD'] as $key => $val) {
            $qry->bindValue($key, $val);
        }

        return returnResultQuery($qry, $this->pdo, $pp['tabD']);
    }

    public function create($request)
    {

        $pp = prepInsertQuery($request, array('sysma_object_id' => true));


        $qry = $this->pdo->prepare("insert into " . SCHEMA . ".sysma_action (" . $pp['reqFields'] . ") VALUES (" . $pp['reqParams'] . ") RETURNING sysma_action_id as id");

        foreach ($pp['tabD'] as $key => $val) {
            $qry->bindValue($key, $val);
        }

        return returnResultQuery($qry, $this->pdo, $pp['tabD']);
    }

    public function addContract($contract)
    {


        $qry = $this->pdo->prepare('update ' . SCHEMA . '.sysma_action set contracts = array_append(contracts, :contract) where sysma_action_id = :sysma_action_id');
        $qry->bindParam(':contract', $contract, PDO::PARAM_STR);
        $qry->bindParam('sysma_action_id', $this->id, PDO::PARAM_INT);
        return $qry->execute();
    }

    public function deleteContracts()
    {


        $qry = $this->pdo->prepare('update ' . SCHEMA . '.sysma_action set contracts = null where sysma_action_id = :sysma_action_id');
        $qry->bindParam('sysma_action_id', $this->id, PDO::PARAM_INT);
        return $qry->execute();
    }

    /*
     * supprime les données de l'objet
     * @return boolean
     */

    public function deleteData()
    {


        $qry = $this->pdo->prepare("DELETE FROM " . SCHEMA . ".sysma_action_data where sysma_action_id = :sysma_action_id");
        $qry->bindParam(':sysma_action_id', $this->id, PDO::PARAM_INT);
        if ($qry->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function delete()
    {

        $this->deleteData();

        $qry = $this->pdo->prepare("DELETE FROM " . SCHEMA . ".sysma_action where sysma_action_id = :sysma_action_id");
        $qry->bindParam(':sysma_action_id', $this->id, PDO::PARAM_INT);
        return $qry->execute();
    }

    // implementation quite similar in utils.php and in SysmaObjectType.php?
    public function countSysmaObjects()
    {
        $qry = $this->pdo->prepare('select count(*) as compte from ' . SCHEMA . '.l_sysma_object_sysma_action where sysma_action_id = :sysma_action_id');
        $qry->bindParam(':sysma_action_id', $this->id);
        $qry->execute();
        $res = $qry->fetchObject();
        return $res->compte;
    }

    
    /*
     * load complementary infos for the action
     */

     public function loadSysmaActionInfos()
     {
 
         $qry = $this->pdo->prepare("select * from " . SCHEMA . ".sysma_action o "
             . "LEFT JOIN " . SCHEMA . ".user u ON o.modified_by=u.user_id "
            // . "LEFT JOIN " . SCHEMA . ".organisation s ON o.organisation_id=s.organisation_id "
           //  . "LEFT JOIN " . SCHEMA . ".sysma_action_type tyo ON o.sysma_action_type_id=tyo.sysma_action_type_id "
             . "where o.sysma_action_id = :id");
 
         $qry->bindParam(':id', $this->id, PDO::PARAM_INT);
         if ($qry->execute()) {
             $this->infos = $qry->fetchObject();
             return true;
         } else {
             return false;
         }
     }

     


    public function loadSysmaObjects()
    {

        $i = 0; // compteur
        $this->sysmaObjectsArray = $this->sysmaObjectsArrayValue = array();

        $qry = $this->pdo->prepare("select * from " . SCHEMA . ".l_sysma_object_sysma_action where sysma_action_id = :sysma_action_id");
        $qry->bindParam(':sysma_action_id', $this->id);
        $qry->execute();

        while ($ob = $qry->fetchObject()) {

            // pour chaque objet geo            
            $Ob = new SysmaObject($ob->sysma_object_id, $this->app);

            // enregistrement
            $this->sysmaObjectsArray[$i] = $Ob; // todo on stocke des objets, pour les sysma_action on stocke juste les id...
            $this->sysmaObjectsArrayValue[$ob->sysma_object_id] = 1;

            $i++;
        }
    }

    /*
     * permet de charger une partie des objets geo (utile pour croisement avec EPCI, recherche travaux
     */

    public function setArraySysmaObjects($O)
    {

        $this->sysmaObjectsArray[] = $O;
        $this->sysmaObjectsArrayValue[$O->id] = 1;
    }

    public function loadActualInfosSysmaAction()
    {

        $this->data = array();
        // chargement des paramètres de ce type de travaux
        $Typ = new SysmaActionType($this->sysma_action_type_id, $this->app);
        $Typ->loadParameters();


        // parcours des parametres du type de travaux
        for ($i = 0; $i < count($Typ->paramArrayValue); $i++) {

            $P = new SysmaActionParameter($Typ->paramArrayValue[$i], $this->app);

            // construction de la value, initialisation puis chargement de la value actuelle
            $Val = new SysmaActionParameterValue(null, $this->app);
            $Val->buildFromParameters($this->id, $P->parameter_id, $this->app);
            array_push($this->data, $Val);
        }
    }
}
