<?php

/**
 * Description of ParameterValue
 * 
 * Parent class for all object, action and relation values
 * 
 * @author srenou
 * 
 */
class ParameterValue
{

    protected $type;
    protected $typeId;
    protected $typeTableDonnees;
    protected $typeTableparameters;
    protected $typeTableChoixparameters;

    public function loadParameterDescription($app)
    {

        $this->pdo = $app['pdo'];
        if ($this->parameter->dataType == 'textChoiceType') {


            $this->descriptionparameter = null;
            $qry = $this->pdo->prepare("select description from " . SCHEMA . "." . $this->typeTableChoixparameters . " where parameter_id = :parameter_id and choice = :value");
            $qry->bindParam(':parameter_id', $this->parameter_id, PDO::PARAM_INT);
            $qry->bindParam(':value', $this->value, PDO::PARAM_STR);
            $qry->execute();
            if ($qry->rowCount() === 1) {
                $res = $qry->fetchObject();
                $this->descriptionparameter = $res->description;
            }
            return true;
        }
    }

    public function loadChoices($dataType, $app = null, $search = null)
    {

        if ($dataType == 'multipleTextChoiceType' or $dataType == 'textChoiceType' or $dataType == 'sysmaObjectLinkType') {


            $qry = $this->pdo->prepare("select * from " . SCHEMA . "." . $this->typeTableChoixparameters . " where parameter_id = :parameter_id order by \"display_order\" asc");
            $qry->bindParam(':parameter_id', $this->parameter_id, PDO::PARAM_INT);

            $qry->execute();
            $this->choicesArray = $qry->fetchAll();
        }


        if ($dataType == 'sysmaObjectLinkType' and count($this->choicesArray) > 0) {

            $this->linkedObjectsList = sysmaObjectsForUserList($app, $this->choicesArray[0]['choice'], $search);
        }
        return true;
    }

    public function create($request)
    {

        if (!is_array($request) and $request->get('value') === '') {
            return json_encode(['result' => 'error', 'message' => 'empty value !']);
        }

        $pp = prepInsertQuery($request);

        $qry = $this->pdo->prepare("insert into " . SCHEMA . ".$this->typeTableDonnees (" . $pp['reqFields'] . ") VALUES (" . $pp['reqParams'] . ")");

        // echo "insert into " . SCHEMA . ".$this->typeTableDonnees (" . $pp['reqFields'] . ") VALUES (" . $pp['reqParams'] . ")";

        foreach ($pp['tabD'] as $key => $val) {
            $qry->bindValue($key, $val);
            //  echo $key . ' : ' . $val . '<br>';
        }

        return returnResultQuery($qry, $this->pdo, $pp['tabD']);
    }

    public function modify($request)
    {

        if (!is_array($request) and $request->get('value') === '') {
            return json_encode(['result' => 'error', 'message' => 'empty value !']);
        }

        $pp = prepUpdateQuery($request, 'data_index');


        $qry = $this->pdo->prepare("update " . SCHEMA . ".$this->typeTableDonnees set " . $pp['req'] . " where data_index = :data_index");

        // echo "update " . SCHEMA . ".$this->typeTableDonnees set " . $pp['req'] . " where data_index = :data_index";

        foreach ($pp['tabD'] as $key => $val) {
            $qry->bindValue($key, $val);
            //  echo $key . ' : ' . $val . '<br>';
        }

        return returnResultQuery($qry, $this->pdo, $pp['tabD']);
    }
}
