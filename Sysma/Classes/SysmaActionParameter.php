<?php

/**
 * Description of SysmaActionParameter 
 * 
 * @author srenou
 * 
 */
class SysmaActionParameter
{

    public function __construct($id, $app)
    {

        $this->app = $app;
        $this->pdo = $app['pdo'];

        if (is_numeric($id) and $id != null) {

            $qry = $this->pdo->prepare("select * from " . SCHEMA . ".sysma_action_type_parameter where parameter_id = :id");
            $qry->bindParam(':id', $id, PDO::PARAM_INT);
            $qry->execute();
            $p = $qry->fetchObject();

            if (empty($p)) { // object does not exists 
                throw new NotFoundException('Sysma Action Parameter ' . $id . ' doesn\'t exist');
            }

            foreach ($p as $key => $val) {
                $this->$key = $val;
            }

            $this->parameter = ucfirst($p->parameter);
            $this->dataType = str_replace(' ', '', $p->data_type);
            $this->description = html_entity_decode($p->description, ENT_QUOTES);
            $this->description = html_entity_decode($p->description, ENT_QUOTES);
            $this->sourceparameter = html_entity_decode($p->parameter_source, ENT_QUOTES);
            $this->data_source = html_entity_decode($p->data_source, ENT_QUOTES);

            $this->creation_date = dateDisplay($p->created_at);
            $this->modification_date = dateDisplay($p->modified_at);



            if ($this->required == 1)
                $this->classeOblig = ' requiredParameter'; // style css pour les champs requireds

            $this->choicesArray = array();
            if ($this->dataType == 'textChoiceType' or $this->dataType == 'multipleTextChoiceType' or $this->dataType == 'sysmaObjectLinkType')
                $this->choicesArray = $this->loadChoices();

            if ($this->dataType == 'sysmaObjectLinkType')
                count($this->choicesArray) > 0 ? $this->sysmaObjectLinked = $this->choicesArray[0] : $this->sysmaObjectLinked = ['nom' => null, 'choice_id' => null, 'description' => null];

            

            return true;
        } else {
            return false;
        }
    }

    private function loadChoices()
    {

        $tab = null;
        $qry = $this->pdo->prepare("select * from " . SCHEMA . ".sysma_action_type_parameter_choice where parameter_id = :id order by \"display_order\" ASC");
        $qry->bindParam(':id', $this->parameter_id, PDO::PARAM_INT);
        $qry->execute();
        $compt = 0;
        $deja = array();
        $tab = array() ;
        while ($c = $qry->fetchObject()) {

            if (isset($deja[$c->display_order])) {
                $c->display_order = $c->display_order + 999;
            }
            $tab[$c->display_order]['choice_id'] = $c->choice_id;
            $tab[$c->display_order]['nom'] = $c->choice;
            $tab[$c->display_order]['description'] = $c->description;
            $tab[$c->display_order]['display_order'] = $c->display_order;
            $deja[$c->display_order] = true;
            $compt++;
        }
        $this->nbChoix = $compt++;
        return $tab;
    }

    public function update($request)
    {

        $pp = prepUpdateQuery($request, 'parameter_id');

        $qry = $this->pdo->prepare("update " . SCHEMA . ".sysma_action_type_parameter set " . $pp['req'] . " where parameter_id = :parameter_id");


        foreach ($pp['tabD'] as $key => $val) {
            $qry->bindValue($key, $val);
            echo $key . ' : ' . $val . '<br>';
        }

        return returnResultQuery($qry, $this->pdo, $pp['tabD']);
    }

    public function create($request)
    {

        $pp = prepInsertQuery($request);

        $qry = $this->pdo->prepare("insert into " . SCHEMA . ".sysma_action_type_parameter (" . $pp['reqFields'] . ") VALUES (" . $pp['reqParams'] . ") RETURNING parameter_id as id");

        foreach ($pp['tabD'] as $key => $val) {
            $qry->bindValue($key, $val);
        }

        return returnResultQuery($qry, $this->pdo, $pp['tabD']);
    }

    public function countSysmaActionParameters($sapId = null)
    {
        if ($sapId == null) {
            $sapId = $this->parameter_id;
        }
        $qry = $this->pdo->prepare("select count(sysma_action_id) as count from " . SCHEMA . ".sysma_action_data where parameter_id = :id");
        $qry->bindParam(':id', $sapId, PDO::PARAM_INT);
        $qry->execute();
        $res = $qry->fetchObject();
        return $res->count;
    }

    public function delete()
    {
        Debug::startPgLog();

        try {
            // BEGIN
            $this->pdo->beginNestedTransaction();

            // 1st delete related param values
            $qry = $this->pdo->prepare("DELETE FROM " . SCHEMA . ".sysma_action_data WHERE parameter_id= :id");
            $qry->bindParam(':id', $this->parameter_id, PDO::PARAM_INT);
            $res = returnResultQuery($qry, $this->pdo,  "Cannot delete sysma_action_data");

            // 2nd delete linked parameter_choices
            $qry = $this->pdo->prepare("DELETE FROM " . SCHEMA . ".sysma_action_type_parameter_choice" . " WHERE parameter_id=" . $this->parameter_id);
            $res = returnResultQuery($qry, $this->pdo,  "Cannot delete sysma_action_type_parameter_choice");

            // 3rd delete this parameter
            $qry = $this->pdo->prepare("DELETE FROM " . SCHEMA . ".sysma_action_type_parameter" . " WHERE parameter_id=" . $this->parameter_id);
            $res = returnResultQuery($qry, $this->pdo, "Cannot delete sysma_action_type_parameter");
        } catch (Exception $e) {
            // if any SQL error : ROLLBACK
            $this->pdo->rollbackNestedTransaction();
            // $this->pdo->rollback();
            // echo 'pdo ROLLBACK<br>';
        }
        // if everything's ok : COMMIT
        $this->pdo->commitNestedTransaction();
        Debug::endPgLogAndPrintResults($this->app);

        return $res;
    }

    public function createChoice($request)
    {

        $pp = prepInsertQuery($request);

        $qry = $this->pdo->prepare("insert into " . SCHEMA . ".sysma_action_type_parameter_choice (" . $pp['reqFields'] . ") VALUES (" . $pp['reqParams'] . ") RETURNING choice_id as id");

        foreach ($pp['tabD'] as $key => $val) {
            $qry->bindValue($key, $val);
        }

        return returnResultQuery($qry, $this->pdo, $pp['tabD']);
    }

    public function updateChoice($request)
    {


        $pp = prepUpdateQuery($request, 'choice_id');
        $qry = $this->pdo->prepare("update " . SCHEMA . ".sysma_action_type_parameter_choice set " . $pp['req'] . " where choice_id = :choice_id");
        foreach ($pp['tabD'] as $key => $val) {
            $qry->bindValue($key, $val);
        }

        return returnResultQuery($qry, $this->pdo, $pp['tabD']);
    }

    public function deleteChoice($idc)
    {

        $qry = $this->pdo->prepare("DELETE FROM " . SCHEMA . ".sysma_action_type_parameter_choice where choice_id = :choice_id");
        $qry->bindParam(':choice_id', $idc, PDO::PARAM_INT);
        return $qry->execute();
    }

    /************************************************************
     * import func
     * 
     * 
     */
    public function importFromJson($_obj)
    {
        $rc = ''; // TODO error mgmt


        // 1st add it then retreive its id after insert
        // make a local cpy of this parent obj
        $obj = clone $_obj;
        // rm inner sub objs
        unset($obj->choices);
        // rm pk id to let pg deal with it
        unset($obj->parameter_id);
        // Finally, add modified by
        $obj->modified_by = $this->app['session']->get('user_id');
        // persist it
        // var_dump($obj);
        $rc = json_decode($this->create($obj)); // TODO FIXM : we shouldn't have to decode here. cf. create() : it does the encoding 

        if ($rc->status != 'success') {
            return $rc;
        }
        $id = $rc->id;

        // 2nd now import/deal with inner objs
        if (property_exists($_obj, 'choices')) {
            // importObject_Parameters($_ot2i->objectTypeParams, $_app);
            foreach ($_obj->choices as $atpc) {
                unset($atpc->choice_id);
                $atpc->parameter_id = $id;

                // Finally, add modified by
                $atpc->modified_by = $this->app['session']->get('user_id');
                $rc = $this->createChoice($atpc);
            }
        }
        return $rc;
    }

    /************************************************************
     * export func
     * export the minimal requested members of "sysma action type"
     * 
     */
    public function getExport()
    {
        $satp = new StdClass;

        // Id shouln't be necessary IIRC but let's export it for now.
        $satp->parameter_id           = $this->parameter_id;

        $satp->parameter              = $this->parameter;
        $satp->description                    = $this->description;

        $satp->data_type                = $this->data_type;
        $satp->ordered_by               = $this->ordered_by;
        $satp->created_at               = $this->created_at;
        $satp->modified_at              = $this->modified_at;
        $satp->sysma_action_type_id     = $this->sysma_action_type_id;
        $satp->parameter_source         = $this->parameter_source;
        $satp->data_source              = $this->data_source;
        $satp->unit                     = $this->unit;
        $satp->required                 = $this->required;
        $satp->parameter_alias          = $this->parameter_alias;
        $satp->track_historytables_id   = $this->track_historytables_id;
        // $satp->time_tracking            = $this->time_tracking;

        $satp->modified_by            = $this->modified_by;
        // $satp->required_field_parameter = $this->required_field_parameter;
        // $satp->hidden_in_form           = $this->hidden_in_form;

        if (!empty($this->choicesArray)) {
            $satp->choices = [];
            $idx = 0;
            foreach ($this->choicesArray as $choice) {
                $satp->choices[$idx] = new stdClass();
                $satp->choices[$idx]->choice_id = $choice['choice_id'];
                $satp->choices[$idx]->choice = $choice['nom'];
                $satp->choices[$idx]->description = $choice['description'];
                $satp->choices[$idx]->display_order = $choice['display_order'];
                // $satp->choices[$idx]->value_type = $choice['value_type'];
                $idx++;
            }
        }
        return $satp;
    }
}
