<?php

/**
 * Description of Status 
 * 
 * @author srenou
 * 
 */
class Status
{

    public function __construct($ot, $st, $app)
    {

        $this->pdo = $app['pdo'];
        $qry = $this->pdo->prepare("select * from " . SCHEMA . ".status where status_value =:status and object_type = :ot");
        $qry->bindParam(':status', $st, PDO::PARAM_STR);
        $qry->bindParam(':ot', $ot, PDO::PARAM_STR);
        $qry->execute();
        if ($qry->rowCount() > 0) {
            foreach ($qry->fetchObject() as $key => $val) {
                $this->$key = $val;
            }
        } else {
            return null;
        }
    }
}
