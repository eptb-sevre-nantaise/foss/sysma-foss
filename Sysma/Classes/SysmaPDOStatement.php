<?php



class SysmaPDOStatement extends \PDOStatement{
    private $bound_variables=array();
    protected $pdo;

    protected function __construct($pdo) {
        $this->pdo = $pdo;
        $this->pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, true);
    }

    private function getBindedVariables(){
        $vars=array();
        foreach($this->bound_variables as $key=>$val){
            $vars[$key] = $val->value;
            if($vars[$key]===NULL)
                continue;
            switch($val->type){
                case \PDO::PARAM_STR: $type = 'string'; break;
                case \PDO::PARAM_BOOL: $type = 'boolean'; break;
                case \PDO::PARAM_INT: $type = 'integer'; break;
                case \PDO::PARAM_NULL: $type = 'null'; break;
                default: $type = FALSE;
            }
            if($type !== FALSE)
                settype($vars[$key], $type);
        }
        if(is_numeric(key($vars)))
            ksort($vars);
        return $vars;
    }
    
    private function rebuildSqlQuery(){
        $queryString = $this->queryString;
    
        $vars=$this->getBindedVariables();
        $params_are_numeric=is_numeric(key($vars));
    
        foreach($vars as $key=>&$var){
            switch(gettype($var)){
                // case 'string': $var = "'{$var}'" ; break;
                case 'string': $var = '$$'."{$var}". '$$' .  " /*" . gettype($var)."*/ "; break;
                case 'integer': $var = "{$var}"; break;
                case 'boolean': $var = $var ? 'TRUE' : 'FALSE'; break;
                case 'NULL': $var = 'NULL';
                default:
                    $var = "{$var}" .  " /*" . gettype($var) . "*/ ";
                    break;
                
            }
        }
        if($params_are_numeric){
            $queryString = preg_replace_callback( '/\?/', function($match) use( &$vars) { return array_shift($vars); }, $queryString);
        }else{
            $queryString = strtr($queryString, $vars);
        }
        return $queryString;
    }
    private function pdo_debugStrParams()
    {
        ob_start();
        $this->debugDumpParams();
        $r = ob_get_contents();
        ob_end_clean();
        return $r;
    }
    private function pdo_debugStrParamsExtractSentQuery($debugStrParams) {
        $rtn = __FILE__.' '.__FUNCTION__.' no match :( ';
        // Utilisation d'une expression régulière pour extraire la requête SQL et la requête envoyée
        $pattern = '/SQL: \[\d+\] (.+?)\nSent SQL: \[\d+\] (.+?)\n/s';
        if (preg_match($pattern, $debugStrParams, $matches)) {
            $query = $matches[1]; // La requête SQL
            $sentQuery = $matches[2]; // La requête SQL envoyée
            $rtn = $sentQuery;
        }
        return $rtn;
    }
    public function execute($bound_input_params = NULL)    {
        if ($this->pdo->logAllQueries) {
            // Debug::logSql($this->queryString);
            Debug::logSql($this->rebuildSqlQuery());
        }
        $res =  parent::execute($bound_input_params);
        if (!$res) {
            //var_dump($this->errorInfo());
            // Debug::logSqlError($this->rebuildSqlQuery(), $this->errorInfo()[2]);
            $msg = "";
            $msg .= "/*-- SQL V1 [rebuildSqlQuery()] */ \n" . $this->rebuildSqlQuery();
            $msg .= "\n\n";
            $msg .= "/*-- SQL V2 [debugDumpParams()] */ \n" . $this->pdo_debugStrParamsExtractSentQuery($this->pdo_debugStrParams()) ;
            Debug::logSqlError( $msg, $this->errorInfo()[2]);
            // Debug::logSqlError($this->rebuildSqlQuery(), '');
            //var_dump($res);
        }
        return $res;

        // $errors = parent::errorInfo();
        // if ($errors[0] != '00000'):
        //     throw new Exception ($errors[2]);
        // endif;
    }

    public function bindValue($parameter, $value, $data_type=\PDO::PARAM_STR){
        $this->bound_variables[$parameter] = (object) array('type'=>$data_type, 'value'=>$value);
        return parent::bindValue($parameter, $value, $data_type);
    }

    public function bindParam($parameter, &$variable, $data_type=\PDO::PARAM_STR, $length=NULL , $driver_options=NULL){
        $this->bound_variables[$parameter] = (object) array('type'=>$data_type, 'value'=>&$variable);
        return parent::bindParam($parameter, $variable, $data_type, $length, $driver_options);
    }


}