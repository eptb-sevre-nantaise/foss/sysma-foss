<?php


require_once __DIR__ . '/../../Sysma/Controllers/UserController.php';
require_once __DIR__ . '/../../Sysma/Controllers/MapController.php';
require_once __DIR__ . '/../../Sysma/Controllers/HomeController.php';
require_once __DIR__ . '/../../Sysma/Controllers/LayerManagerController.php';
require_once __DIR__ . '/../../Sysma/Controllers/LayerController.php';
require_once __DIR__ . '/../../Sysma/Controllers/SysmaObjectController.php';
require_once __DIR__ . '/../../Sysma/Controllers/SysmaActionController.php';
require_once __DIR__ . '/../../Sysma/Controllers/SysmaRelationController.php';
require_once __DIR__ . '/../../Sysma/Controllers/SysmaObjectParameterValueController.php';
require_once __DIR__ . '/../../Sysma/Controllers/SysmaActionParameterValueController.php';
require_once __DIR__ . '/../../Sysma/Controllers/SysmaRelationParameterValueController.php';
require_once __DIR__ . '/../../Sysma/Controllers/CadastreController.php';
require_once __DIR__ . '/../../Sysma/Controllers/RpgController.php';
require_once __DIR__ . '/../../Sysma/Controllers/FormattingController.php';
require_once __DIR__ . '/../../Sysma/Controllers/FilterController.php';
require_once __DIR__ . '/../../Sysma/Controllers/ReportController.php';
require_once __DIR__ . '/../../Sysma/Controllers/ImportExportController.php';
require_once __DIR__ . '/../../Sysma/Controllers/ThesaurusController.php';
require_once __DIR__ . '/../../Sysma/Controllers/HelpController.php';
require_once __DIR__ . '/../../Sysma/Controllers/AdminController.php';
require_once __DIR__ . '/../../Sysma/Controllers/GeoJsonController.php';
require_once __DIR__ . '/../../Sysma/Controllers/PhotoController.php';
require_once __DIR__ . '/../../Sysma/Controllers/AboutController.php';
require_once __DIR__ . '/../../Sysma/Controllers/ProcessController.php';
require_once __DIR__ . '/../../Sysma/Controllers/AddOnController.php';
require_once __DIR__ . '/../../Sysma/Controllers/AtlasController.php';
require_once __DIR__ . '/../../Sysma/Controllers/ErrorController.php';
//require_once __DIR__ . '/../../Sysma/Controllers/TempController.php';
