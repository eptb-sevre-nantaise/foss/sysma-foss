<?php

use Symfony\Component\HttpFoundation\Request;

$app->match('/layermanager', function (Request $request) use ($app) {
    $U = new User($app['session']->get('user_id'), $app);
    $layers = $U->loadLayerManager();

    return $app['twig']->render('layer-manager/layermanager.twig', ['U' => $U, 'messageaccueil' => $request->get('messageaccueil'), 'layers' => $layers['objectTypeLayers'], 'tt' => $layers['workTypeLayers'], 'others' => $layers['otherLayers']]);
})->bind('layermanager');


$app->match('/layermanager-print', function (Request $request) use ($app) {
    $U = new User($app['session']->get('user_id'), $app);
    $layers = $U->loadLayerManager();

    return $app['twig']->render('layer-manager/layermanagerPrint.twig', ['U' => $U, 'layers' => $layers['objectTypeLayers'], 'tt' => $layers['workTypeLayers'], 'others' => $layers['otherLayers']]);
})->bind('layermanager-print');
