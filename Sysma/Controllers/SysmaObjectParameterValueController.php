<?php

use Symfony\Component\HttpFoundation\Request;

$app->match('/value-parameter-object', function (Request $request) use ($app) {

    if ($request->request->get('data_index') != null) {
        $V = new SysmaObjectParameterValue($request->request->get('data_index'), $app);
    } else {
        $V = new SysmaObjectParameterValue(null);
        $V->buildFromParameters($request->request->get('sysma_object_id'), $request->request->get('parameter_id'), null, $app);
    }

    $O = new SysmaObject($V->idsysma_object, $app);
    if (
        hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'lecteur')
        or (hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'contributeur') and $O->modified_by == $app['session']->get('user_id'))
    ) {

        return $app['twig']->render('sysma-object-parameter-value/sysmaObjectParameterValueDisplay.twig', ['O' => new SysmaObject($request->request->get('sysma_object_id'), $app), 'dataType' => $V->parameter->dataType, 'id' => $request->request->get('sysma_object_id'), 'parameter_id' => $request->request->get('parameter_id'), 'V' => $V]);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cet objet';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('sysma-object-parameter-value');

/* MODIFICATION */

$app->match('/value-parameter-object/modification', function (Request $request) use ($app) {

    $V = new SysmaObjectParameterValue($request->request->get('data_index'), $app);
    $O = new SysmaObject($V->idsysma_object, $app);
    if (
        hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'gestionnaire')
        or (hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'contributeur') and  $O->modified_by == $app['session']->get('user_id'))
    ) {

        $V->loadChoices($request->request->get('data_type'), $app);

        return $app['twig']->render('sysma-object-parameter-value/sysmaObjectParameterValueModificationForm.twig', ['O' => new SysmaObject($request->request->get('sysma_object_id'), $app), 'dataType' => $request->request->get('data_type'), 'id' => $request->request->get('sysma_object_id'), 'parameter_id' => $request->request->get('parameter_id'), 'V' => $V]);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cet objet';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('sysma-object-parameter-value-modification');


/* filtered objects list for sysmaObjectLinkType parameters */

$app->post('/value-parameter-object/modification/sysma-object-link-type/object-list', function (Request $request) use ($app) {

    $V = new SysmaObjectParameterValue(null);
    $V->buildFromParameters($request->request->get('sysma_object_id'), $request->request->get('parameter_id'), null, $app);
    $O = new SysmaObject($request->request->get('sysma_object_id'), $app);
    if (
        hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'gestionnaire')
        or (hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'contributeur') and  $O->modified_by == $app['session']->get('user_id'))
    ) {


        $V->loadChoices($request->request->get('data_type'), $app, $request->request->get('search'));

        return $app['twig']->render('sysma-object-parameter-value/sysmaObjectParameterObjectsListValueForm.twig', ['O' => new SysmaObject($request->request->get('sysma_object_id'), $app), 'dataType' => $request->request->get('data_type'), 'id' => $request->request->get('sysma_object_id'), 'parameter_id' => $request->request->get('parameter_id'), 'V' => $V]);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cet objet';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('sysma-object-link-type-parameter-value-modification');




$app->match('/value-parameter-object/modification/commit', function (Request $request) use ($app) {

    $request->request->set('modified_by', $app['session']->get('user_id'));
    $request->request->set('user_id', $app['session']->get('user_id'));
    $request->request->set('modified_at', date('Y-m-d H:i:s'));
    $r = $request->request;
    $V = new SysmaObjectParameterValue($r->get('data_index'), $app);

    $O = new SysmaObject($V->idsysma_object, $app);
    if (
        hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'gestionnaire')
        or (hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'contributeur') and $O->modified_by == $app['session']->get('user_id'))
    ) {

        return $V->modify($r);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cet objet';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('sysma-object-parameter-value-modification-commit');



$app->match('/value-parameter-multiple-object/modification/commit', function (Request $request) use ($app) {

    $request->request->set('modified_by', $app['session']->get('user_id'));
    $request->request->set('modified_at', date("Y-m-d H:i:s"));
    $request->request->set('user_id', $app['session']->get('user_id'));
    $r = $request->request;
    $V = new SysmaObjectParameterValue($r->get('data_index'), $app);

    //var_dump($V);
    $O = new SysmaObject($V->idsysma_object, $app);
    if (
        hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'gestionnaire')
        or (hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'contributeur') and $O->modified_by == $app['session']->get('user_id'))
    ) {

        return $V->multipleModify($r);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cet objet';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('object-parameter-multiple-value-modification-commit');


/* UPDATE */

$app->match('/value-parameter-object/update', function (Request $request) use ($app) {


    $V = new SysmaObjectParameterValue($request->request->get('data_index'), $app);
    $O = new SysmaObject($V->idsysma_object, $app);
    if (
        hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'gestionnaire')
        or (hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'contributeur') and $O->modified_by == $app['session']->get('user_id'))
    ) {

        $V->loadChoices($request->request->get('data_type'), $app);

        return $app['twig']->render('sysma-object-parameter-value/sysmaObjectParameterValueUpdateForm.twig', ['dataType' => $request->request->get('data_type'), 'id' => $request->request->get('sysma_object_id'), 'parameter_id' => $request->request->get('parameter_id'), 'V' => $V]);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cet objet';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('sysma-object-parameter-value-update');




$app->match('/value-parameter-object/update/commit', function (Request $request) use ($app) {

    $request->request->set('modified_by', $app['session']->get('user_id'));
    $request->request->set('user_id', $app['session']->get('user_id'));
    $request->request->set('created_at', date('Y-m-d H:i:s'));
    $request->request->set('modified_at', date('Y-m-d H:i:s'));
    $r = $request->request;


    $V = new SysmaObjectParameterValue($r->get('data_index'), $app);
    $O = new SysmaObject($V->idsysma_object, $app);
    if (
        hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'gestionnaire')
        or (hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'contributeur') and $O->modified_by == $app['session']->get('user_id'))
    ) {


        return $V->update($r);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cet objet';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('sysma-object-parameter-value-update-commit');



$app->match('/value-parameter-multiple-object/update/commit', function (Request $request) use ($app) {

    $request->request->set('modified_by', $app['session']->get('user_id'));
    $request->request->set('user_id', $app['session']->get('user_id'));
    $request->request->set('created_at', date('Y-m-d H:i:s'));
    $request->request->set('modified_at', date('Y-m-d H:i:s'));
    $r = $request->request;


    $V = new SysmaObjectParameterValue($r->get('data_index'), $app);

    $O = new SysmaObject($V->idsysma_object, $app);
    if (
        hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'gestionnaire')
        or (hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'contributeur') and $O->modified_by == $app['session']->get('user_id'))
    ) {


        return $V->multipleValuesUpdate($r);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cet objet';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('object-parameter-multiple-value-update-commit');




/* CREATION */

$app->match('/value-parameter-object/create', function (Request $request) use ($app) {

    $V = new SysmaObjectParameterValue(null);
    $V->buildFromParameters($request->request->get('sysma_object_id'), $request->request->get('parameter_id'), null, $app);
    $O = new SysmaObject($V->idsysma_object, $app);
    if (
        hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'gestionnaire')
        or (hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'contributeur') and $O->modified_by == $app['session']->get('user_id'))
    ) {

        $V->loadChoices($request->request->get('data_type'), $app);

        return $app['twig']->render('sysma-object-parameter-value/sysmaObjectParameterValueFillForm.twig', ['dataType' => $request->request->get('data_type'), 'id' => $request->request->get('sysma_object_id'), 'parameter_id' => $request->request->get('parameter_id'), 'indexDonnee' => dataIndex($app['pdo']), 'V' => $V]);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cet objet';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('sysma-object-parameter-value-create');


$app->match('/value-parameter-object/create/commit', function (Request $request) use ($app) {

    $request->request->set('modified_by', $app['session']->get('user_id'));
    $request->request->set('user_id', $app['session']->get('user_id'));
    $request->request->set('created_at', date('Y-m-d H:i:s'));
    $r = $request->request;
    $V = new SysmaObjectParameterValue(null);
    $V->buildFromParameters($request->request->get('sysma_object_id'), $request->request->get('parameter_id'), null, $app);

    $O = new SysmaObject($V->idsysma_object, $app);
    if (
        hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'gestionnaire', $app)
        or (hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'contributeur', $app) and $O->modified_by == $app['session']->get('user_id'))
    ) {
        return $V->create($r);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cet objet';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('sysma-object-parameter-value-create-commit');


$app->match('/value-parameter-multiple-object/create/commit', function (Request $request) use ($app) {

    $request->request->set('modified_by', $app['session']->get('user_id'));
    $request->request->set('user_id', $app['session']->get('user_id'));
    $request->request->set('modified_at', date('Y-m-d H:i:s'));
    $r = $request->request;
    $V = new SysmaObjectParameterValue(null, $app);
    $V->buildFromParameters($request->request->get('sysma_object_id'), $request->request->get('parameter_id'), null, $app);

    $O = new SysmaObject($V->idsysma_object, $app);

    if (
        hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'gestionnaire', $app)
        or (hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'contributeur', $app) and $O->modified_by == $app['session']->get('user_id'))
    ) {

        $V->loadChoices($V->parameter->dataType);

        $res = null;

        foreach ($V->choicesArray as $choice) {

            $inputName = 'choice' . $choice['choice_id'];


            if ($r->get($inputName) == 1) {

                $r2['parameter_id'] = $r->get('parameter_id');
                $r2['sysma_object_id'] = $r->get('sysma_object_id');
                $r2['start_date'] = $r->get('start_date');
                $r2['end_date'] = $r->get('end_date');
                $r2['start_date'] = $r->get('start_date');
                $r2['data_index'] = $r->get('data_index');
                $r2['value'] = $choice['choice'];
                $r2['user_id'] = $r->get('user_id');
                $r2['modified_by'] = $r->get('user_id');

                $res = $V->create($r2);
            }
        }
        return $res; // derniere insertion
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cet objet';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('object-parameter-multiple-value-create-commit');



$app->match('/value-parameter-object/history', function (Request $request) use ($app) {

    $O = new SysmaObject($request->request->get('sysma_object_id'), $app);
    if (
        hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'lecteur', $app)
        or (hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'contributeur', $app) and $O->modified_by == $app['session']->get('user_id'))
    ) {

        return $app['twig']->render('sysma-object-parameter-value/sysmaObjectParameterValueHistoryForm.twig', ['O' => $O, 'tabV' => history($request->request->get('sysma_object_id'), $request->request->get('parameter_id'), $app)]);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cet objet';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('sysma-object-parameter-value-history');




$app->match('/value-parameter-object/delete', function (Request $request) use ($app) {

    $V = new SysmaObjectParameterValue($request->request->get('data_index'), $app);

    $O = new SysmaObject($V->idsysma_object, $app);
    if (
        hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'gestionnaire')
        or (hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'contributeur') and $O->modified_by == $app['session']->get('user_id'))
    ) {

        $V->delete();
        $V2 = new SysmaObjectParameterValue(null);
        $V2->buildFromParameters($V->idsysma_object, $V->parameter_id, null, $app);
        return $app['twig']->render('sysma-object-parameter-value/sysmaObjectParameterValueDisplay.twig', ['O' => new SysmaObject($request->request->get('sysma_object_id'), $app), 'dataType' => $V2->parameter->dataType, 'id' => $request->request->get('sysma_object_id'), 'parameter_id' => $request->request->get('parameter_id'), 'V' => $V2]);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cet objet';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('sysma-object-parameter-value-delete');
