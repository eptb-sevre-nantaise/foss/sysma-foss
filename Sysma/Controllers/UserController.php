<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\Cookie;

$app->match('/login', function (Request $request) use ($app) {

    $form = $app['form.factory']->createBuilder('form')
        ->add('login', 'text', array('label' => 'Utilisateur', 'constraints' => new Assert\NotBlank()))
        ->add('password', 'password', array('label' => 'Mot de passe', 'constraints' => new Assert\NotBlank()))
        ->add('rememberme', 'checkbox', array('label' => 'Se souvenir de moi'))
        ->getForm();
    $form->handleRequest($request);

    if ($accessId = control($request->request->get('login'), $request->request->get('password'), $app)) {

        $U = new User($accessId, $app);
        $U->updateLastConnexion();
        $U->tracksql_filter('sysma_foss_v2_login');
        $app['session']->set('user_id', $accessId);
        $app['session']->set('user', $U->user);
        $app['session']->set('is_known', true);
        $app['session']->set('map_width', $U->map_width);
        $app['session']->set('default_center', $U->default_center);
        $app['session']->set('default_zoom', $U->default_zoom);
        $app['session']->set('is_admin', $U->isAdmin());
        $app['session']->set('has_cadastre_access', hasCadastreAccess($app, $accessId));
        $app['session']->set('has_rpg_access', hasRpgAccess($app, $accessId));
        $app['session']->set('has_gis_export_privilege', hasGISExportPrivilege($app, $accessId));
        $app['session']->set('has_pg_export_privilege', hasPGExportPrivilege($app, $accessId));
        $app['session']->set('organisation_id', $U->organisation_id);
        $app['session']->set('snap_options', $U->snap_options);
        $app['session']->set('load-on-pan', 'true');

        // création du cookie

        if ($request->request->get('rememberme') === 'true') {
            $C = new Cookie('uid', md5($U->user), time() + COOKIEEXPIRE, '/', BASEURL, false, true);
            $C2 = new Cookie('sid', hashSysmaCookie($U->password), time() + COOKIEEXPIRE, '/', BASEURL, true, false);

            $response = new Response('<p class="text-center">Connexion en cours</p><script>window.location.replace("' . WEBURL . '");</script>', 200, array());
            $response->headers->setCookie($C);
            $response->headers->setCookie($C2);
            return $response;
        } else {
            return '<p class="text-center">Connexion en cours</p><script>window.location.replace("' . WEBURL . '");</script>';
        }
    } else {
        return $app['twig']->render('user/login.twig', ['form' => $form->createView()]);
    }
})->bind('login');


$app->match('/logout', function () use ($app) {

    $response = new Response('', 302, array("LOCATION" => WEBURL));
    $response->headers->clearCookie('uid', '/', BASEURL, false, true);
    $response->headers->clearCookie('sid', '/', BASEURL, false, true);
    $response->headers->clearCookie('PHPSESSID');
    return $response;
})->bind('logout');




$app->match('/user', function (Request $request) use ($app) {


    if ($app['session']->get('user_id') != ANONYMID and !empty($app['session']->get('user_id'))) {


        $U = new User($app['session']->get('user_id'), $app);

        $form = $app['form.factory']->createBuilder('form', ['firstname' => $U->firstname, 'lastname' => $U->lastname, 'map_width' => $U->map_width, 'snap_active' => $U->snap_options->active, 'snap_layer' => $U->snap_options->layer_id, 'snap_distance' => $U->snap_options->distance])
            ->add('firstname', 'text', array('label' => 'Prénom', 'constraints' => new Assert\NotBlank()))
            ->add('lastname', 'text', array('label' => 'Nom', 'constraints' => new Assert\NotBlank()))
            ->add('map_width', 'number', array('label' => 'Largeur de la carte (%) (max : 90)', 'constraints' => new Assert\NotBlank()))
            ->add('snap_active', 'checkbox', array('label' => 'Activer l\'accrochage des layers'))
            ->add('snap_layer', 'choice', array('label' => 'Couche d\'accrochage', 'choices' => sysmaObjectTypesList($app)))
            ->add('snap_distance', 'number', array('label' => 'Distance d\'accrochage', 'constraints' => new Assert\NotBlank()))
            ->getForm();
        $form->handleRequest($request);

        if ($form->isValid()) {

            $data = $form->getData();
            $data['snap_options'] = ['active' => $data['snap_active'], 'layer_id' => $data['snap_layer'], 'distance' => $data['snap_distance']];
            $data['snap_options'] = json_encode($data['snap_options']);
            unset($data['snap_active']);
            unset($data['snap_layer']);
            unset($data['snap_distance']);
            $data['user_id'] = $U->user_id;
            if ($data['map_width'] > 90) {
                $data['map_width'] = 90;
            }

            $U = new User($app['session']->get('user_id'), $app);
            $U->update($data);

            return '<p class="text-center">Rechargement de la carte</p><script>window.location.replace("' . WEBURL . '");</script>';
        }


        $objectTypesLayers = $U->loadObjectTypesLayers();


        return $app['twig']->render('user/userAccount.twig', ['form' => $form->createView(), 'U' => $U, 'groups' => groups($app), 'objectTypesLayers' => $objectTypesLayers]);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('user-account');


$app->match('/user-view/create', function (Request $request) use ($app) {

    if ($app['session']->get('user_id') != ANONYMID and !empty($app['session']->get('user_id'))) {
        $U = new User($app['session']->get('user_id'), $app);

        $view = $request->request->get('view');    

        $actualUserView = $U->loadActualUserView();

        // print elements

        $printTab = ['comment', 'legend_width', 'legend_size'];
        foreach ($printTab as $p) {
            if ($request->request->get('print_' . $p)) {
                $actualUserView['print'][$p] = $request->request->get('print_' . $p);
            }
        }


        $data = [
            'view' => $view,
            'view_json' => json_encode($actualUserView),
            'user_id' => $app['session']->get('user_id'),
            'modified_by' => $app['session']->get('user_id'),
        ];

        $UV = new UserView(null, $app);
        $res = json_decode($UV->insert($data));
        if ($res->status == 'success') {

            $U->loadUserViewAsActualView(new UserView($res->id, $app));
            $app['session']->getFlashBag()->add('message', array('text' => 'Vue sauvegardée', 'status' => 'success'));
            return '<script type="text/javascript"> location.reload()</script>';
        } else {
            $app['session']->getFlashBag()->add('message', array('text' => 'Echec', 'status' => 'danger'));
            return '<script type="text/javascript"> location.reload()</script>';
        }
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('user-view-create');



$app->match('/user-view/delete', function (Request $request) use ($app) {

    if ($app['session']->get('user_id') != ANONYMID and !empty($app['session']->get('user_id'))) {

        $user_view_id = $request->request->get('user_view_id');
        $UV = new UserView($user_view_id, $app);
        $res = $UV->delete();
        if ($res === true) {
            $app['session']->getFlashBag()->add('message', array('text' => 'Vue supprimée', 'status' => 'success'));
        } else {
            $app['session']->getFlashBag()->add('message', array('text' => 'Echec', 'status' => 'danger'));
        }
        return '<script type="text/javascript"> location.reload("' . $app['url_generator']->generate('map') . '")</script>';
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('user-view-delete');





$app->match('/user-view/load', function (Request $request) use ($app) {

    if ($app['session']->get('user_id') != ANONYMID and !empty($app['session']->get('user_id'))) {

        $user_view_id = $request->request->get('user_view_id');
        $UV = new UserView($user_view_id, $app);

        $U = new User($app['session']->get('user_id'), $app);
        $res = $U->loadUserViewAsActualView($UV);


        if ($res === true) {
            $app['session']->getFlashBag()->add('message', array('text' => 'Vue chargée', 'status' => 'success'));
        } else {
            $app['session']->getFlashBag()->add('message', array('text' => 'Echec', 'status' => 'danger'));
        }
        return '<script type="text/javascript"> location.reload("' . $app['url_generator']->generate('map') . '")</script>';
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('user-view-load');



$app->match('/user-view/reset', function (Request $request) use ($app) {

    if ($app['session']->get('user_id') != ANONYMID and !empty($app['session']->get('user_id'))) {


        $U = new User($app['session']->get('user_id'), $app);
        $res = $U->resetUserActualView($app);


        if ($res === true) {
            $app['session']->getFlashBag()->add('message', array('text' => 'Vue désactivée', 'status' => 'success'));
        } else {
            $app['session']->getFlashBag()->add('message', array('text' => 'Echec', 'status' => 'danger'));
        }
        return '<script type="text/javascript"> location.reload("' . $app['url_generator']->generate('map') . '")</script>';
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('user-view-reset');
