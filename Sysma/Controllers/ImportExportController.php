<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints as Assert;



$app->match('/import-sig', function (Request $request) use ($app) {


    if (hasAccess($app, $app['session']->get('user_id'), null, null, null, null, null, null, 'gestionnaire')) {


        $data = $request->request->all();
        $data = @$data['form'] ;

        // get parameters (shortcut after thesaurus creation)
        $data2 = $request->query->all();
       
        if (isset($data2['step'])) {
            foreach($data2 as $key => $val) {
                $data[$key] = $val ;
            }
        }

        $U = new User($app['session']->get('user_id'), $app);

        if (!isset($data['step'])) {
            $data['step'] = 1;
        }


        $Importer = new Importer($app, $U->user_schema, $data);

      
      
        if ($data['step']=='4') {            

            if ($Importer->formVars['sysma_object_type'] == 'new' and $Importer->formVars['confirm_creation'] == 'yes') {
                
                $res = $Importer->createNewSysmaObjectTypeFromGISTable() ;   
                return $app['twig']->render('import-export/GISImport2.twig', ['U' => $U, 'step' => $data['step'], 'res' => json_decode($res)]);   

            } else {                 
               
                // Data import
                $res = $Importer->import() ;
                return $app['twig']->render('import-export/GISImport2.twig', ['U' => $U, 'step' => $data['step'], 'res' => json_decode($res)]);

            }


        } else {

            
            $form = $Importer->buildForm();

        //echo  $data['step'] ;

        return $app['twig']->render('import-export/GISImport2.twig', ['form' => $form['form_view'], 'form_message' => $form['message'], 'U' => $U, 'step' => $data['step']]);

        }
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }


})->bind('import-gis');


$app->match('/import-sig-old', function (Request $request) use ($app) {


    if (hasAccess($app, $app['session']->get('user_id'), null, null, null, null, null, null, 'gestionnaire')) {

        $rapport = null;
        $U = new User($app['session']->get('user_id'), $app);
        $schema = $U->user_schema;


        // ETAPE 1 : Couche à importer

        $userLayers = userLayersList($schema, $app['pdo.export']);
        if (!$userLayers) {

            $message = '<p>Problème d\'accès aux couches du schéma ' . $U->user_schema . '</p><p>Vérifiez premièrement que vous avez bien des tables dans ce schéma.</p><p>Si le problème persiste, vérifiez les droits d\'accès de l\'utilisateur PG à ce schéma.</p>';
            return $app['twig']->render('/error/error.twig', ['erreur' => ['message' => $message]]);
        }

        $builder = $app['form.factory']->createBuilder('form')
            ->add('layer_import', 'choice', array('label' => 'Couche à importer', 'choices' => $userLayers, 'multiple' => false, 'constraints' => new Assert\NotBlank()));

        $builder->addEventListener('form.pre_bind', function (\Symfony\Component\Form\FormEvent $event) use ($app, $schema) {

            $data = $event->getData();
            $form = $event->getForm();

            // ETAPE 2 : type objet SYSMA
            if (isset($data['layer_import'])) {

                $form->add('sysma_object_type', 'choice', array(
                    'label' => 'Type d\'objet Sysma',
                    'choices' => objectTypesOfGeomTypeList(geoLayerType($schema, $data['layer_import'], $app['pdo']), 'contributeur', $app),
                    'constraints' => new Assert\NotBlank()
                ));

                // ETAPE 3 : colonnes
                if (isset($data['sysma_object_type'])) {

                    $cols = layerColumnsList($schema, $data['layer_import'], $app['pdo']);

                    // pour chaque colonne, sous formulaire

                    foreach ($cols as $col) {

                        if ($col['type'] != null) {

                            // preselection si la colonne est du même nom que l'alias sysma (col_sig)

                            $id_param_preselec = null;
                            if ($param = parameterFromAlias('sysma_object', $data['sysma_object_type'], $col['name'], $app)) {
                                $id_param_preselec = 'idp' . $param->parameter_id;
                            } elseif ($col['name'] === 'id_objetge') {
                                $id_param_preselec = 'sys_sysma_object_id';
                            } elseif ($col['name'] === 'id_objetgeo') {
                                $id_param_preselec = 'sys_sysma_object_id';
                            } elseif ($col['name'] === 'id_objetgeo') {
                                $id_param_preselec = 'sys_sysma_object_id';
                            } elseif ($col['name'] === 'sysma_obje') {
                                $id_param_preselec = 'sys_sysma_object_id';
                            } elseif ($col['name'] === 'sysma_object') {
                                $id_param_preselec = 'name';
                            } elseif ($col['name'] === 'sysma_ob_2') {
                                $id_param_preselec = 'name';
                            } elseif ($col['name'] === 'suppr') {
                                $id_param_preselec = 'sys_suppr';
                            } elseif ($col['name'] === 'delete') {
                                $id_param_preselec = 'sys_suppr';
                            }

                            $form->add('import_col_' . $col['name'], 'choice', array('choices' => array_merge(array('0' => 'Ne pas importer', 'name' => 'Importer comme nom d\'objet Sysma', 'sys_sysma_object_id' => 'Importer comme identifiant d\'objet Sysma', 'sys_suppr' => 'Importer comme colonne de suppression'), parameterFromTypeList($col['type'], $data['sysma_object_type'], 'contributeur', $app)), 'empty_data' => $id_param_preselec, 'label' => 'Importer la colonne ' . $col['name'] . ' ? (' . $col['type'] . ')', 'constraints' => new Assert\NotBlank()));
                        }
                    }

                    // ETAPE 4 : organisation et date
                    if (isset($data['import_col_' . $cols[0]['name']])) {



                        $form->add('organisation', 'choice', array('choices' => organisationWithUserAccessList($app), 'empty_data' => strval($app['session']->get('organisation_id')), 'label' => 'organisation ayant réalisé le relevé', 'constraints' => new Assert\NotBlank()));
                        $form->add('date_releve', 'date', array('label' => 'Date du relevé (ou de l\'import)', 'empty_data' => date('Y-m-d'), 'widget' => 'single_text', 'constraints' => new Assert\NotBlank()));

                        // ETAPE 5 : confirmation
                        if (isset($data['organisation'])) {

                            $form->add('confirmation', 'choice', array('label' => 'Confirmez-vous l\'import des données ?', 'choices' => array('0' => 'Non', '1' => 'Oui')));

                            // ETAPE 6 : import
                            if (isset($data['confirmation']) and $data['confirmation'] === '1') {

                                $tabP = array();
                                // tab paramètres cibles
                                foreach ($cols as $col) {
                                    if ($col['type'] != null) {
                                        $tabP = array_merge($tabP, array($col['name'] => $data['import_col_' . $col['name']]));
                                    }
                                }

                                $res = importShapeSysma2($schema, $data['layer_import'], $data['sysma_object_type'], $tabP, $data['organisation'], $data['date_releve'], null, null, $app);

                                // TODO
                                var_dump($res);
                                //die();


                                if ($res['status'] == 'succes') {
                                    $app['session']->getFlashBag()->add('message', array('status' => 'success', 'text' => 'Import réussi', 'rapport' => $res['rapport'], 'res_rapport' => $res['res_rapport']));
                                } else {
                                    $app['session']->getFlashBag()->add('message', array('status' => 'danger', 'text' => 'Echec de l\'import', 'rapport' => $res['rapport'], 'res_rapport' => $res['res_rapport']));
                                }
                            }
                        }
                    }
                }
            }
        });

        $form = $builder->getForm();
        $form->handleRequest($request);

        if ($form->isValid()) {
            return $app['twig']->render('import-export/GISImport.twig', ['form' => $form->createView(), 'U' => $U]);
        }
        return $app['twig']->render('import-export/GISImport.twig', ['form' => $form->createView(), 'U' => $U]);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('import-gis-old');



///////////////////////////////////////////////////////////////////////////////
// export thesaurus controller
$app->match('/dictionnaire/export', function (Request $request) use ($app) {

    if ($app['session']->get('is_admin')) {
        $form = $app['form.factory']->createBuilder('form')
            ->add('sysma_object_type_id_list', 'choice', array('label' => 'Types d\'objet', 'choices' => ['all' => 'Tous'] + objectTypesFilteredList($app), 'multiple' => true, 'constraints' => new Assert\NotBlank()))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {

            $otIds = $form->getData()['sysma_object_type_id_list'];

            if ($otIds[0] == 'all') {
                $otIds = array_keys(objectTypesFilteredList($app));
            }

            $jsonExports = [];
            foreach ($otIds as $otId) {

                $ot = new SysmaObjectType($otId, $app);
                $ot->loadParameters();
                $ot->loadSysmaActionTypes();
                $ot->loadSysmaRelationTypes();

                $export = $ot->getExportFiltered();

                // TODO add metadata: SYSMA_DICTIONARY_VERSION, etc.
                // $export->version = $SYSMA_DICTIONARY_VERSION;
                // echo '<br>-------------------<br>';
                // echo json_encode($export);
                // echo '<br>-------------------<br>';
                // die();
                $jsonExports[] = $export;
            }

            $tarFilepath = '/var/tmp/sysma_tmp_' . bin2hex(random_bytes(16)) . '.tar';

            $tar = new PharData($tarFilepath);

            // loop
            foreach ($jsonExports as $jsonExport) {
                // create a tmp file to store json object type json-
                $tmpFile = tmpfile();
                $tmpFilePath = stream_get_meta_data($tmpFile)['uri'];
                fwrite($tmpFile, json_encode($jsonExport));

                // add it in the tarball
                // echo json_encode($jsonExport);
                // echo '<br>************************<br>';
                // $tar->addFile($tmpFilePath, $jsonExport->sysma_object_type_alias.'.json');
                $tar->addFile($tmpFilePath, $jsonExport->thesaurus_item->sysma_object_type_alias
                    . '_' . $jsonExport->metadata->tiid . '.json');
                // echo '<br>************************<br>';
                // die();
                fclose($tmpFile); // this should remove the tmp file
            }

            $tar->compress(Phar::GZ);

            $tar_gzip_content = file_get_contents($tarFilepath . '.gz');
            unlink($tarFilepath . '.gz');
            unlink($tarFilepath);


            $response = new Response();
            if ($tar_gzip_content != false) {
                $response->setContent($tar_gzip_content);
            } else {
                $response->setContent(__FUNCTION__ . " | ERROR | Dictionary download failed.");
            }
            $response->headers->set('Content-Type', 'application/octet-stream');
            $response->headers->set('Content-Disposition', 'attachment; filename="Sysma_Dictionary_Export_' . date('Ymd_His') . '.tar.gz"');
            ob_clean(); // Mandatory with binary ! This fix this wtf bug that add a line-break (0x0A) before the binary file content... Oo
            return $response;
        } else {
            return $app['twig']->render('import-export/thesaurusExport.twig', ['form' => $form->createView(), 'tabO' => null, 'to' => objectTypesList($app), 'rs' => relationTypesList($app)]);
        }
    } else {

        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('thesaurus-export');





///////////////////////////////////////////////////////////////////////////////
// import thesaurus controller
$app->match('/dictionnaire/import', function (Request $request) use ($app) {

    if ($app['session']->get('is_admin')) {
        $form = $app['form.factory']->createBuilder('form')
            ->add('files', 'file', array('label' => 'Sélectionner le(s) fichiers(s) de dictionnaire à importer dans Sysma. Les types de fichiers dictionnaires acceptés ont l\'extension .json ou .tar.gz.', 'multiple' => "multiple"))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {

            $idx = 0;
            $objectTypesToImport = [];
            foreach ($_FILES['form']['name']['files'] as $fileName) {
                try {
                    $tmpFilePath = $_FILES['form']['tmp_name']['files'][$idx];
                    $mimeType = $_FILES['form']['type']['files'][$idx];
                    // DO NOT TRUST $_FILES['upfile']['mime'] VALUE !!
                    // Check MIME Type by yourself.
                    $finfo = new finfo(FILEINFO_MIME_TYPE);
                    if (false === $ext = array_search(
                        $finfo->file($tmpFilePath),
                        array(
                            '.tar.gz' => 'application/x-gzip',
                            '.json' => 'application/json', // not compatible with php <7.4 probably (cf. https://bugs.php.net/bug.php?id=78075)
                            '.json' => 'text/plain', // workaround for .json file with php 7.2
                        ),
                        true
                    )) {
                        throw new RuntimeException('Invalid file format.');
                    }

                    if (!move_uploaded_file($tmpFilePath, $tmpFilePath . $ext)) {
                        throw new RuntimeException('Failed to move uploaded file.');
                    }

                    if ($ext == '.tar.gz') {
                        $archive = new PharData($tmpFilePath . $ext);
                        foreach ($archive as $file) {
                            $contents = file_get_contents($file);
                            $objectTypesToImport[] = json_decode($contents);
                        }
                    } elseif ($ext == '.json') {
                        $contents = file_get_contents($tmpFilePath . $ext);
                        $objectTypesToImport[] = json_decode($contents);
                    }

                    $idx++;
                } catch (RuntimeException $e) {
                    // echo $e->getMessage();
                    return $app['twig']->render('error/error.twig', ['erreur' => $e->getMessage()]);
                }
            }

            // return content to test/debug
            $rc = "";
            if (count($objectTypesToImport) > 1) {
                $rc = "[";
                foreach ($objectTypesToImport as $ot) {
                    // $rc .= json_encode($objectTypesToImport[0]);
                    $rc .= json_encode($ot);
                    $rc .= ",";
                }

                // $rc .= "blabla"; 
                $rc = substr($rc, 0, -1);
                // substr_replace($rc ,"", -1);
                $rc .= "]";
            } else {
                $rc = json_encode($objectTypesToImport[0]);
            }

            ////////////////////////////////////////////////////////////////////////////////////////
            // At this point every objects to import are in objectTypesToImport[]
            ////////////////////////////////////////////////////////////////////////////////////////

            // 1st check if one element to import already exists (check pks are unique or not)
            $tabMsg = [];
            // echo '<br>********************************************************************************<br>';
            // echo json_encode($objectTypesToImport);
            // echo '<br><br>';

            foreach ($objectTypesToImport as $key => $ot2i) {
                if (ThesaurusItemExport::verifyTiidAndDataIntrication($ot2i->metadata->tiid, json_encode($ot2i->thesaurus_item))) {
                    // echo '<br>********************************************************************************<br>';
                    // echo 'tiid ftw';
                    // echo '<br><br>';
                    $id = SysmaObjectType::retreiveId($ot2i->metadata->tiid, $app);
                    if ($id > 0) {
                        $tabMsg[] = array('AlreadyExist', $id, htmlspecialchars_decode($ot2i->thesaurus_item->sysma_object_type, ENT_QUOTES));
                        unset($objectTypesToImport[$key]);
                    }
                } else {
                    // echo '<br>********************************************************************************<br>';
                    // echo 'wrong tiid';
                    // echo '<br><br>';
                    $tabMsg[] = array('TiidTiMissMatch', '', htmlspecialchars_decode($ot2i->thesaurus_item->sysma_object_type, ENT_QUOTES));
                    unset($objectTypesToImport[$key]);
                }
            }

            // Debug::startPgLog();
            // 2nd let's create/insert objects in DB now
            foreach ($objectTypesToImport as $ot2i) {
                try {
                    // pdo BEGIN
                    // $app['pdo']->beginTransaction();
                    $app['pdo']->beginNestedTransaction();
                    $sot = new SysmaObjectType(null, $app);
                    // For DEV WIP!!! rm this!!! v
                    // $ot2i->sysma_object_type_alias = uniqid();
                    // For DEV WIP!!! rm this!!! ^
                    $rc = $sot->importFromJson($ot2i);
                } catch (Exception $e) {
                    // if error : pdo ROLLBACK
                    // $app['pdo']->rollback();
                    $app['pdo']->rollbackNestedTransaction();
                    // echo Debug::endPgLogAndGetResults($app);
                    // Debug::endPgLogAndPrintResults($app);
                    // die();
                }
                // pdo COMMIT
                // $app['pdo']->rollback();
                // $app['pdo']->commit();
                $app['pdo']->commitNestedTransaction();

                $tabMsg[] = array('Ok', $rc->id, htmlspecialchars_decode($ot2i->thesaurus_item->sysma_object_type));
            }

            // return content to test/debug
            // $response = new Response();
            // $response->headers->set('Content-Type', 'application/json');
            // $response->setContent(json_encode($rc));
            return $app['twig']->render('import-export/thesaurusImport.twig', ['form' => $form->createView(), 'tabMsg' => $tabMsg, 'to' => objectTypesList($app), 'rs' => relationTypesList($app)]);
        } else {
            return $app['twig']->render('import-export/thesaurusImport.twig', ['form' => $form->createView(), 'tabMsg' => null, 'to' => objectTypesList($app), 'rs' => relationTypesList($app)]);
        }
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('thesaurus-import');






// TODO move this in a shared defines.php containing all app consts (=/= conf)
define('SUCCESS', 'SUCCESS');
define('FAILURE', 'FAILURE');
define('ACCESS_NOT_GRANTED', 'ACCESS_NOT_GRANTED');

/********************************************************
 * Import a whole sysma layer in a geojson format
 * 
 */

///////////////////////////////////////////////////
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// TODOs:
// - who hasAccess? : TBD
// - per parameter creation/modification without necessarly updating the whole object
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
///////////////////////////////////////////////////
$app->post('/import/layers/geojson', function (Request $request) use ($app) {

    $currentUserId = $app['session']->get('user_id');
    $userIsManager = hasAccess($app, $currentUserId, null, null, null, null, null, null, 'gestionnaire');
    $userIsCommiter = hasAccess($app, $currentUserId, null, null, null, null, null, null, 'contributeur');
    $responseHtml = "";
    $responseStatus = ACCESS_NOT_GRANTED;
    $output = new stdClass();

    // First check wether we are logged in as a manager or a commiter
    if ($userIsManager or $userIsCommiter) {

        Debug::clearLogFile();


        $geojsons_str = file_get_contents('php://input');
        // $geojsons_str = $test;

        /////////////////////////////////////////
        // saveTextToFile($geojsonsBackupFilePath, $geojsons_str);
        // TODO backup en base. dump file? email?
        // $geojsonsBackupFilePath = date('Y-m-d H:i:s');
        // $email = "alriviere@sevre-nantaise.com";
        // $headers = ['From' => $email, 'Reply-To' => $email, 'Content-type' => 'text/html; charset=iso-8859-1'];
        // $rc = mail($email, "Sysma - issue - data backup", "Hello there", $headers);
        // Debug::log("mail result: " . json_encode($rc));

        $geojsons = json_decode($geojsons_str);
        $currentDate = date('Y-m-d H:i:s');
        $backup_import_data_id =null;



        // 1) backup import metadata 
        $qry = $app['pdo']->prepare("INSERT INTO " . SCHEMA . ".backup_import_metadata 
            (created_by, backup_import_metadata_comment, backup_import_metadata_log) 
            VALUES (:created_by, :backup_import_metadata_comment, :backup_import_metadata_log)
            RETURNING backup_import_metadata_id AS id;");
        $qry->bindValue("created_by", $currentUserId);
        $qry->bindValue("backup_import_metadata_comment", "");
        $qry->bindValue("backup_import_metadata_log", "");
        $res = returnResultQuery($qry, $app['pdo'], "Import backup attempt.");

        $res = json_decode($res);
        if ($res->status == 'success') {
            $backup_import_metadata_id = $res->id;
            // 2) backup import data
            $qry = $app['pdo']->prepare("INSERT INTO " . SCHEMA . ".backup_import_data 
                (backup_import_metadata_id, backup_import_data_origin, backup_import_data_type, backup_import_data, created_by, backup_import_data_comment, backup_import_data_log) 
                VALUES (:backup_import_metadata_id, :backup_import_data_origin, :backup_import_data_type, :backup_import_data, :created_by, :backup_import_data_comment, :backup_import_data_log)
                RETURNING backup_import_data_id AS id;");
            $qry->bindValue("backup_import_metadata_id", $backup_import_metadata_id);
            $qry->bindValue("backup_import_data_origin", "layers-import-geojson");
            $qry->bindValue("backup_import_data_type", "save-all");
            $qry->bindValue("backup_import_data", $geojsons_str);
            $qry->bindValue("created_by", $currentUserId);
            $qry->bindValue("backup_import_data_comment", "");
            $qry->bindValue("backup_import_data_log", "");
            $res = returnResultQuery($qry, $app['pdo'], "Import backup attempt.");

            $res = json_decode($res);

            $backup_import_data_id = $res->id;
        }

        function pg_track_backup_import_errors ($app, $backup_import_data_id, $errorMsgToAppend){
            if ($backup_import_data_id != null) {
                $qry = $app['pdo']->prepare("UPDATE ". SCHEMA . ".backup_import_data 
                SET backup_import_data_log = (coalesce(backup_import_data_log,'') || :errorMsgToAppend )
                WHERE backup_import_data_id = :backup_import_data_id
                ;");
                $qry->bindValue("errorMsgToAppend", $errorMsgToAppend);
                $qry->bindValue("backup_import_data_id", $backup_import_data_id);
                $res = returnResultQuery($qry, $app['pdo'], "Update offline backup log.");

            }
        }
        




        // Debug::log(json_encode($geojsons));
        // Debug::log('--------------------');

        // pre-checks
        if (array_key_exists('features', $geojsons[0]) /* TODO more*/) {
                // Debug::log('acceptable geojson format');
            ;
        } else {
            Debug::log('ERROR!!!!!!!!!!!!!!!!!!!!!!!!');
            Debug::log('Wrong geojson format');
            pg_track_backup_import_errors($app,$backup_import_data_id, FAILURE .' : Wrong geojson format');
            return $app->json(FAILURE);
        }
        $responseStatus = SUCCESS;

        foreach ($geojsons as $gjson) {
            // Debug::log('-------------------- geojson');
            // Debug::log(json_encode($gjson));          
            // foreach ($gjson['features'] as $f) {
            // $dbgcounter = 0;
            foreach ($gjson->features as $f) {
                // TOCHECK !!! filter access !!!!
                // boucle obejts 
                $sotId = substr($f->properties->layer_id, 17); // rm the bordel before teh sotId ... TODO rm this 'o' where it comes from
                $userIsManagerOnCurrentObject = hasAccess($app, $app['session']->get('user_id'), $f->properties->organisation_id, $sotId, $f->properties->status, null, null, null, 'gestionnaire');
                $userIsCommiterOnCurrentObject = hasAccess($app, $app['session']->get('user_id'), $f->properties->organisation_id, $sotId, $f->properties->status, null, null, null, 'contributeur');
                if ($userIsManagerOnCurrentObject or $userIsCommiterOnCurrentObject) {

                    // a chaque create d'object
                    // rien 
                    // a chaque modif/update d'object
                    // if( hasAccess($app, $app['session']->get('user_id'), $f->properties->organisation_id, $sotId, $f->properties->status, null, null, null, 'contributeur') and $app['session']->get('user_id') == $f->properties->modified_by)

                    $app['pdo']->beginNestedTransaction();
                    // $dbgcounter++;  
                    // if ($dbgcounter == 2) {
                    //     break;
                    //   }
                    // Debug::log('-------------------- feature');
                    // Debug::log(json_encode($f));          
                    // Debug::log('-------------------- properties');
                    // Debug::log(json_encode($f->properties));          
                    // if (array_key_exists('offline_status', $f->properties)) {
                    if (property_exists($f->properties, 'offline_status')) {
                        $soId = substr($f->properties->id, 1); // rm the 'o' ... TODO rm this 'o' where it comes from

                        // Debug::log('soId: ' . $soId);

                        // WIP for TEST DEBUG. rm this later
                        // $currentUserId = "102520";
                        Debug::log('...........................');
                        Debug::log('currentUserId: ' . $currentUserId);
                        Debug::log('sotId: ' . $sotId);
                        Debug::log('soId: ' . $soId);
                        Debug::log('object->offline_status: ' . $f->properties->offline_status);
                        Debug::log('userIsManagerOnCurrentObject: ' . $userIsManagerOnCurrentObject);
                        Debug::log('userIsCommiterOnCurrentObject: ' . $userIsCommiterOnCurrentObject);
                        Debug::log('...........................');
                        //Debug::log(json_encode($f, JSON_PRETTY_PRINT));


                        $newSoData = [];
                        $newSoData['start_date'] = $f->properties->startDate;
                        $newSoData['end_date'] = $f->properties->endDate;
                        $newSoData['organisation_id'] = $f->properties->organisation_id;
                        $newSoData['status'] = $f->properties->status;
                        $newSoData['sysma_object']  = "";
                        if (isset($f->properties->name)){
                            $newSoData['sysma_object'] = $f->properties->name;
                        }
                        


                        /////////////////////////////////////////////////////////////////////////////////
                        if ($f->properties->offline_status == 'object_created') {
                            // create new object created offline
                            // 1st) creation d'une ligne dans sysma_object
                            $so = new SysmaObject(null, $app);
                            // $soId = $so->create($request) {
                            // $newSoData = [];
                            // $newSoData['sysma_object_id'] = $soId;
                            // $newSoData['start_date'] = $f->properties->startDate;
                            // $newSoData['end_date'] = $f->properties->endDate;
                            // $newSoData['organisation_id'] = $f->properties->organisation_id;
                            $newSoData['created_by'] = $currentUserId; // requested?
                            // $newSoData['status'] = $f->properties->status; // requested?
                            $newSoData['sysma_object_type_id'] = $sotId; // requested?
                            $newSoData['created_at'] = $currentDate; // requested?
                            // $newSoData['sysma_object'] = $f->properties->name;
                            Debug::log('avant convertGeomFromGeojsonToPostGis: ' . json_encode($f->geometry));
                            $newSoData['geom'] = TkGeo::convertGeomFromGeojsonToPostGis($f->geometry, $app);
                            Debug::log('apres convertGeomFromGeojsonToPostGis: ' . json_encode($newSoData['geom']));
                            $newSoData['modified_by'] = $currentUserId;
                            $newSoData['modified_at'] = $currentDate;
                            Debug::logJson($newSoData);
                            // $res = $so->create($newSoData);
                            // $soId = json_decode($res->id;
                            $res = $so->create($newSoData);
                            if (json_decode($res)->status != 'success') {
                                $app['pdo']->rollbackNestedTransaction();
                                Debug::log('ERROR!!!!!!!!!!!!!!!!!!!!!!!!');
                                Debug::logJson(json_decode($res));
                                $responseStatus = FAILURE;
                                $responseHtml .= "<br><span>Objet <b>" . @$f->properties->name . " (" . $soId . ") </b> -> Problème de création en base de données</span><br>";
                                continue;
                                // return $app->json($responseStatus);
                            }

                            $soId = json_decode($res)->id;
                            Debug::log('___create object with new $soId: ' . $soId);
                            // Debug::log(json_encode($soId));


                            /////////////////////////////////////////////////////////////////////////////////
                        } else if (($f->properties->offline_status == 'object_modified') and ($userIsManagerOnCurrentObject or ($userIsCommiterOnCurrentObject and ($currentUserId == $f->properties->modifiedBy)))) {
                            // modify known object which has an EXISTING SOID in DB already
                            // Debug::log('---soId: ' . $soId . ' modified');
                            Debug::log('___modify object with $soId: ' . $soId);

                            // updateObjectProps
                            $so = new SysmaObject($soId, $app);
                            $newSoData['sysma_object_id'] = $soId;
                            // $newSoData['sysma_object_type_id'] = ; // requested?
                            // $newSoData['geom'] = TkGeo::convertGeomFromGeojsonToPostGis($f->geometry, $app);
                            $newSoData['modified_by'] = $currentUserId;
                            $newSoData['modified_at'] = $currentDate;
                            // $newSoData['sysma_object_id'] = $soId;

                            $res = $so->update($newSoData);
                            if (json_decode($res)->status != 'success') {
                                $app['pdo']->rollbackNestedTransaction();
                                Debug::log('ERROR!!!!!!!!!!!!!!!!!!!!!!!!');
                                Debug::log(json_encode(json_decode($res), JSON_PRETTY_PRINT));
                                $responseStatus = FAILURE;
                                $responseHtml .= "<br><span>Objet <b>" . @$f->properties->name . "</b> (" . $soId . ") -> Problème de modification en base de données</span><br>";
                                continue;
                                // return $app->json($responseStatus);
                            }

                            $geom = TkGeo::convertGeomFromGeojsonToPostGis($f->geometry, $app);
                            // Debug::log('geom:');
                            // Debug::log(json_encode($geom));

                            $res = $so->updateGeom($geom);
                            if (json_decode($res)->status != 'success') {
                                $app['pdo']->rollbackNestedTransaction();
                                Debug::log('ERROR!!!!!!!!!!!!!!!!!!!!!!!!');
                                Debug::logJson(json_decode($res));
                                $responseStatus = FAILURE;
                                $responseHtml .= "<br><span>Objet <b>" . @$f->properties->name . "</b> (" . $soId . ") -> Problème de modification en base de données de sa geometrie</span><br>";
                                continue;
                                // return $app->json($responseStatus);
                            }
                            // } else {
                            //     Debug::log("WARNING: user not allowed to modifiy/create this object" . $f->properties->name . "</b> (" . $soId . ")");
                            //     Debug::log('object->offline_status: ' . $f->properties->offline_status);
                            //     Debug::log('currentUserId: ' . $currentUserId);
                            //     Debug::log('f->properties->modifiedBy: ' . $f->properties->modifiedBy);
                            //     Debug::log('userIsManagerOnCurrentObject: ' . $userIsManagerOnCurrentObject);
                            //     Debug::log('userIsCommiterOnCurrentObject: ' . $userIsCommiterOnCurrentObject);
                            //     Debug::log('soId: ' . $soId);
                            //     $responseHtml .= "<br><span>Objet <b>" . $f->properties->name . "</b> (" . $soId . ") -> Vous n'avez pas les droits de modifier ou créer cet objet</span><br>";
                        }


                        if (property_exists($f->properties, 'parameters')) {
                            foreach ($f->properties->parameters as $p) {

                                /////////////////////////////////////////////////////////////////////////////////
                                // update or create parameters
                                if (property_exists($p, 'offline_status')) {
                                    Debug::log('param->offline_status: ' . $p->offline_status);
                                    // if (($p->offline_status == 'created') or ($p->offline_status == 'modified')) {
                                    // Debug::log('-------------------- p');
                                    // Debug::log(json_encode($p, JSON_PRETTY_PRINT));
                                    // Debug::log($p->id);

                                    // $sopv = new SysmaObjectParameterValue($p->id, $app);
                                    $sopv = new SysmaObjectParameterValue();
                                    $sopv->buildFromParameters($soId, $p->id, null, $app);

                                    $newSopvData = []; // we clean this each time just to make sure
                                    if (property_exists($p, 'startDate')) {
                                        $newSopvData['start_date'] = $p->startDate;
                                    } else { // if no specific startDate at the parameter level we set it with the object startDate value
                                        $newSopvData['start_date'] = $newSoData['start_date'];
                                    }
                                    if (property_exists($p, 'endDate')) {
                                        $newSopvData['end_date'] = $p->endDate;
                                    } else { // if no specific endDate at the parameter level we set it with the object endDate value
                                        $newSopvData['end_date'] = $newSoData['end_date'];
                                    }
                                    $newSopvData['modified_by'] = $currentUserId;
                                    $newSopvData['modified_at'] = $currentDate;

                                    $newSopvData['value'] = $p->value;
                                    // if (s_array($p->value) && ($p->dataType == "multipleTextChoiceType") ) {
                                    //         $rc = $V->multipleModifySysmaObjectsByValues($o, $values);
                                    //     }

                                    // } else {
                                    //     $newSopvData['value'] = $p->value;
                                    // }
                                    // dataType: "multipleTextChoiceType"
                                    Debug::log("newSopvData['value']");
                                    Debug::log($newSopvData['value']);
                                    // Debug::log(json_encode(json_decode($newSopvData['value']), JSON_PRETTY_PRINT));

                                    // Debug::log('-------------------- sopv');
                                    // Debug::log(json_encode($sopv), JSON_PRETTY_PRINT);

                                        /////////////////////////////////////////////////////////////////////////////////
                                        // param_created
                                        // if there is no data_index it means there is no SysmaObjectParamValue in DB
                                        // then if offline mode ask for "modified" it should be processed as "created"
                                        if ( 
                                            (empty($sopv->data_index) and (($p->offline_status == 'created') or ($p->offline_status == 'modified') or ($p->offline_status == 'added')))
                                            and $p->value != "" and !empty($p->value)
                                         ) { // doucble check
                                            $newSopvData['sysma_object_id'] = $soId;
                                            $newSopvData['parameter_id'] = $p->id;
                                            $newSopvData['user_id'] = $currentUserId; // AKA created_by
                                            $newSopvData['created_at'] = $currentDate;
                                            $newSopvData['data_index'] = dataIndex($app['pdo']);
                        
                                            // Debug::log('-------------------- newSopvData');
                                            // Debug::log(json_encode($newSopvData));
                                            Debug::log("___create parameter value with new data_index:" . $newSopvData['data_index'] . ' and soid: '. $soId);

                                        if (is_array($p->value) && ($p->dataType == "multipleTextChoiceType")) {
                                            $sopv->iduser = $currentUserId; // AKA createdBy
                                            $sopv->modified_at = $currentDate;
                                            $sopv->modified_by = $currentUserId;
                                            $res = $sopv->multipleModifyByValues($sopv, $p->value);
                                        } else {
                                            $res = $sopv->create($newSopvData);
                                        }

                                        if (json_decode($res)->status != 'success') {
                                            $app['pdo']->rollbackNestedTransaction();
                                            Debug::log('ERROR!!!!!!!!!!!!!!!!!!!!!!!!');
                                            Debug::logJson(json_decode($res));
                                            $responseStatus = FAILURE;
                                            $responseHtml .= "<br><span>Objet <b>" . @$f->properties->name . "</b> (" . $soId . ") - Paramètre <b>[" . $p->id . "]</b> -> Problème de création en base de données</span><br>";
                                            break;
                                            // return $app->json($responseStatus);
                                        }
                                        /////////////////////////////////////////////////////////////////////////////////
                                        // param_modified 
                                    } else if (!empty($sopv->data_index) and (($p->offline_status == 'modified') or ($p->offline_status == 'added'))) { // double check
                                        // if ( !empty($sopv->data_index ) ) {
                                        // Debug::log(json_encode($newSopvData));
                                        $newSopvData['data_index'] = $sopv->data_index;
                                        if ($p->offline_status == 'modified') {
                                            Debug::log("___modify parameter value with data_index:" . $newSopvData['data_index']);
                                            if (is_array($p->value) && ($p->dataType == "multipleTextChoiceType")) {
                                                $sopv->modified_at = $newSopvData['modified_at'];
                                                $sopv->modified_by = $currentUserId;
                                                $res = $sopv->multipleModifyByValues($sopv, $p->value);
                                            } else {
                                                $res = $sopv->modify($newSopvData);
                                            }
                                        } else if ($p->offline_status == 'added') {
                                            Debug::log("___add (AKA update) parameter value with previous data_index:" . $newSopvData['data_index']);
                                            $newSopvData['sysma_object_id'] = $soId;
                                            $newSopvData['parameter_id'] = $p->id;
                                            $newSopvData['user_id'] = $currentUserId; // AKA created_by
                                            $newSopvData['created_at'] = $currentDate;
                                            // $newSopvData['data_index'] = dataIndex($app['pdo']);

                                            if (is_array($p->value) && ($p->dataType == "multipleTextChoiceType")) {
                                                $sopv->modified_at = $currentDate;
                                                $res = $sopv->multipleUpdateByValues($sopv, $p->value);
                                            } else {
                                                $res = $sopv->update($newSopvData);
                                            }
                                        }

                                        if (json_decode($res)->status != 'success') {
                                            $app['pdo']->rollbackNestedTransaction();
                                            Debug::log('ERROR!!!!!!!!!!!!!!!!!!!!!!!!');
                                            Debug::logJson(json_decode($res));
                                            $responseStatus = FAILURE;
                                            $responseHtml .= "<br><span>Objet <b>" . @$f->properties->name . "</b> (" . $soId . ") - - Paramètre <b>[" . $p->id . "]</b> (" . $newSopvData['data_index'] . ") -> Problème de modification en base de données</span><br>";
                                            break;
                                        }
                                    }
                                    // Debug::log('-------------------- res');
                                    // Debug::log(json_encode($res));
                                } // end check if offline_status exists in param
                            } // end loop params
                        } // end check if parameters exists

                    } // end check if offline_status exists in object
                    $app['pdo']->commitNestedTransaction();
                } else { // end check per sysmaobjects/features first access rights
                    Debug::log("WARNING: user not allowed to modifiy/create this object" . @$f->properties->name . "</b> (" . $soId . ")");
                    $responseHtml .= "<br><span>Objet <b>" . @$f->properties->name . "</b> (" . $soId . ") -> Vous n'avez pas les droits de modifier ou créer cet objet</span><br>";
                }
            } // end sysmaobjects/features loop
        } // end layers loop
        pg_track_backup_import_errors($app,$backup_import_data_id, $responseStatus . $responseHtml);

        pg_track_backup_import_errors($app,$backup_import_data_id, $responseStatus . $responseHtml);

    } else {
        $output->status = ACCESS_NOT_GRANTED;
        return $app->json($output);
    }
    $output->status = $responseStatus;
    $output->responseHtml = $responseHtml;
    
    return $app->json($output);
})->bind('import-layers-geojson');



/******************************************************
 * Get/export a whole or not Sysma Layer geojson with content:
 *  - all or not geometries associated with the sysma_object_type_id provided at the finest resolution (ie. no simplification of the geometries with regard to the zoom factor)
 *  - all or not sysma data associated with the sysma_object_type_id provided
 * Get/export a whole or not Sysma Layer geojson with content:
 *  - all or not geometries associated with the sysma_object_type_id provided at the finest resolution (ie. no simplification of the geometries with regard to the zoom factor)
 *  - all or not sysma data associated with the sysma_object_type_id provided
 */
$app->get('/export/layer/geojson/{sysma_object_type_id}', function (Request $request, $sysma_object_type_id) use ($app) {

    $currentUserId = $app['session']->get('user_id');
    $userIsManager = hasAccess($app, $currentUserId, null, $sysma_object_type_id, null, null, null, null, 'gestionnaire');
    $userIsCommiter = hasAccess($app, $currentUserId, null, $sysma_object_type_id, null, null, null, null, 'contributeur');

    // First check wether we are logged in as a manager or a commiter
    if ($userIsManager or $userIsCommiter) {
        // print_r('/geojson/sysmalayer/{sysma_object_type_id}');
        // die;
        $pdo = $app['pdo'];
        $r = $request->query->all();
        

        $qry = null;

        $lop = objectTypeLayerLoadOnPan($app, $sysma_object_type_id);
        // print_r($lop);
        // echo('<br>');

        if ($lop['geometry_type'] == 'no_geom') {
            return $app->json(array());
        }
        
        $qryDefaultWhereClause= ' where o.sysma_object_type_id = :sysma_object_type_id '; 

        $qryAreaDownloadAddWith ='';
        $qryAreaDownloadAddJoin = '';
        $qryAreaDownloadAddWhereClause = '';
        

        if (isset($r['center']) and isset($r['zoom']) and $r['center'] != 'undefined' and $r['zoom'] != 'undefined' ) {
            $r['zoom'] = intval($r['zoom']);
            $center = explode(',', substr($r['center'], 7, -1));
            $center[0] = trim($center[0]);
            $center[1] = trim($center[1]);

            $qryAreaDownloadAddWith= 'with
                area_download_geom as (
                select ST_buffer(
                ST_Transform(
                ST_SetSRID(ST_MakePoint(' . $center[1] . ', ' . $center[0] . '), 4326), 2154), ' . $GLOBALS['ZOOM_CONVERSION_ARRAY1'][$r['zoom']] . ') as geom
                ) ';
            $qryAreaDownloadAddJoin = ' LEFT JOIN area_download_geom as a on true ';
            $qryAreaDownloadAddWhereClause =  ' and o.geom && a.geom ';
        }
        $qry .= $qryAreaDownloadAddWith;
        $qry .= '
            select 
            \'o\'||o.sysma_object_id as id, 
            o.sysma_object as name,
            \'objet\'::text as type,      
            \'layer_object_type\'||o.sysma_object_type_id as layer_id,
            t.geometry_type as type_geom,
            t.sysma_object_type as object_type,
            ST_AsGeoJSON((ST_Dump(ST_Transform(ST_SnapToGrid(o.geom,1),4326))).geom) as geojson, 
            case when o.end_date = \'0\' then xx_99_utils.json_merge(t.style,s.style) else \'{"color":"#999999","opacity":0.6,"weight":2,"fillColor":"#999999","fillOpacity":0.7}\' end as style
            from ' . SCHEMA . '.sysma_object as o 
            LEFT JOIN ' . SCHEMA . '.sysma_object_type as t ON o.sysma_object_type_id = t.sysma_object_type_id 
            INNER JOIN ' . SCHEMA . '.user_privilege as d
            ON 
            (o.sysma_object_type_id = ANY(d.sysma_object_type_id_list) or d.sysma_object_type_id_list = \'{0}\') and 
            d.user_id = :id_util and
            (d.sysma_object_status_list = \'{tous}\' or o.status = ANY(d.sysma_object_status_list)) and 
            (d.sysma_object_organisation_id_list = \'{0}\' or o.organisation_id = ANY(d.sysma_object_organisation_id_list)) and 
            (\'lecteur\' = ANY(d.privilege_list) or (\'contributeur\' = ANY(d.privilege_list) and o.modified_by = d.user_id))         
            LEFT JOIN ' . SCHEMA . '.l_style_user as s ON s.user_id = d.user_id and s.object_type = \'sysma_object\' and s.type_id = t.sysma_object_type_id
            ';
        $qry .= $qryAreaDownloadAddJoin;
        $qry .= $qryDefaultWhereClause;
        $qry .= $qryAreaDownloadAddWhereClause;

        $qry .= buildSQLFilter('sysma_object', $sysma_object_type_id, $app);
        $qry .= ' order by st_area(o.geom) desc';
        //echo $qry ;
        $sttmt = $pdo->prepare($qry);



        $idu = $app['session']->get('user_id');
        $sttmt->bindParam(':id_util', $idu, PDO::PARAM_INT);
        $sttmt->bindParam(':sysma_object_type_id', $sysma_object_type_id, PDO::PARAM_INT);

        $sttmt->execute();

        if ($sttmt->rowCount() == 0)
            return '{"type":"FeatureCollection","features":[]}';

        $features = [];
        while ($o = $sttmt->fetchObject()) {
            // print_r(json_encode($o));
            // echo "<br><br>";
            // print_r(substr($o->id,1));
            // $original = $o;
            $soid = substr($o->id, 1);
            // $soid = $o->id;
            unset($o->id);

            $so = new SysmaObject($soid, $app);
            $so->loadActualInfosSysmaObject();


            // feature
            $f = new stdClass();
            $f->type = "Feature";

            $f->geometry = json_decode($o->geojson);
            unset($o->geojson);
            $f->style = json_decode($o->style);
            unset($o->style);
            $f->properties = $o;

            // props fine tunning
            $f->properties->id = 'o' . $so->sysmaObjectId;
            // $f->properties->id = 'o'.$o->id;
            $f->properties->startDate = $so->startDate;
            $f->properties->endDate = $so->endDate;
            $f->properties->value = $so->value;
            $f->properties->createdAt = $so->createdAt;
            $f->properties->createdBy = $so->userId;
            $f->properties->modifiedAt = $so->modifiedAt;
            $f->properties->modifiedBy = $so->modifiedBy;

            // $f->properties->sysmaObjectType = $so->sysma_object_type;
            // $f->properties->organisation = $so->organisation; // <- rm this later since it shouldn't be necessary anymore
            $f->properties->organisation_id = $so->organisation_id;
            $f->properties->status = $so->status;

            // params data
            $params = [];
            foreach ($so->data as $d) {
                // echo json_encode($d);
                $p = new stdClass();
                $p->id = $d->parameter_id;
                $p->dataType = $d->parameter->data_type;
                $p->createdAt = $d->parameter->created_at;
                $p->modifiedAt = $d->parameter->modified_at;
                //  specific multipleTextChoiceType output if necessary
                // if (($p->dataType=="multipleTextChoiceType") && property_exists($d->parameter, "choicesArray") ) {
                //     $p->choicesArray = $d->parameter->choicesArray;
                //     Debug::log( $p->id . " -> p->choicesArray: " );
                //     Debug::log( json_encode($p->choicesArray, JSON_PRETTY_PRINT) );
                // }
                // Debug::logJson($d->value);
                $p->value = $d->value;
                array_push($params, $p);
            }
            $f->properties->parameters = $params;

            array_push($features, $f);
        }

        // feature collection
        $fc = new stdClass();
        $fc->type = "FeatureCollection";
        $fc->features = $features;

        return $app->json($fc);
    } else {
        $output = new stdClass();
        $output->status = ACCESS_NOT_GRANTED;
        return $app->json($output);
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // TBD for later
    // TA ?

    //     $qry2 = $pdo->prepare('select * from ' . SCHEMA . '.l_theme_analysis_user l '
    //             . 'JOIN ' . SCHEMA . '.theme_analysis at ON at.theme_analysis_id = l.theme_analysis_id and at.type_id = :sysma_object_type_id and object_type = \'sysma_object\' '
    //             . 'where l.user_id = :id_util');


    //     $qry2->bindParam(':id_util', $idu, PDO::PARAM_INT);
    //     $qry2->bindParam(':sysma_object_type_id', $sysma_object_type_id, PDO::PARAM_INT);
    //     $qry2->execute();

    //     if ($qry2->rowCount() > 0) {
    //         print_r('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
    //         $rs = array();
    // // TA
    //         $at = $qry2->fetchObject();

    //         $AT = new ThematicAnalysis($at->theme_analysis_id, $app);

    //         // AT de type classes
    //         $options = array();
    //         if ($AT->theme_analysis_type === 'gradient') {

    //             $minmax = findMinMax('sysma_object', $AT->parameter_id, $app);

    //             $options['min'] = $minmax->min;
    //             $options['max'] = $minmax->max;
    //             $options['classes'] = $AT->calculateTAClasses($options['min'], $options['max']);
    //         }


    //         while ($ob = $sttmt->fetchObject()) {
    //             print_r($ob);
    //             $V = new SysmaObjectParameterValue(null, $app);
    //             $V->buildFromParameters(substr($ob->id, 1), $AT->parameter_id, null, $app);
    //             if ($V->parameter->dataType == 'multipleTextChoiceType' and $V->value != null and count($V->value > 0)) {
    //                 $ob->value = implode(',', $V->value);
    //                 $ob->style = $AT->apply($ob->value, $options);
    //             } else {
    //                 $ob->value = $V->value;
    //                 $ob->style = $AT->apply($V->value, $options);
    //             }
    //             array_push($rs, get_object_vars($ob));
    //         }
    //     } else {
    //         $rs = $sttmt->fetchAll(PDO::FETCH_ASSOC);
    //     }


    //     return $app->json(json_decode(toGeoJSON($rs)));
})->bind('export-layer-geojson');
