<?php

use Symfony\Component\HttpFoundation\Request;

$app->match('/photo', function (Request $request) use ($app) {

    $P = new PiwigoPhoto($request->request->get('id'), $app);
    return $app['twig']->render('photo/photoPage.twig', ['P' => $P, 'mapWidth' => $app['session']->get('mapWidth')]);
})->bind('pwg-photo');


$app->match('/photo-object/update', function (Request $request) use ($app) {

    $OP = new PhotoObject($request->request->get('photo_object_id'), $app);

    $O = new SysmaObject($OP->sysma_object_id, $app);
    if (
        hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'gestionnaire', $app)
        or (hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'contributeur', $app) and $O->modified_by == $app['session']->get('user_id'))
    ) {

        return $app['twig']->render('photo/photoModificationForm.twig', ['OP' => $OP]);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cet objet';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('photo-object-update');


$app->match('/photo-object/update/commit', function (Request $request) use ($app) {

    $r = $request->request;
    $OP = new PhotoObject($r->get('photo_object_id'), $app);
    $O = new SysmaObject($OP->sysma_object_id, $app);
    if (
        hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'gestionnaire', $app)
        or (hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'contributeur', $app) and $O->modified_by == $app['session']->get('user_id'))
    ) {

        $form_data = $r->all();
        $form_data['modified_by'] = $app['session']->get('user_id');
        $dt = new DateTime('now');
        $form_data['modified_at'] = $dt->format('Y-m-d H:i:s');
        if (empty($form_data['photo_date']))
            $form_data['photo_date'] = null;

        return $OP->update($app, $form_data);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cet objet';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('photo-object-update-commit');



$app->match('/photo-object/create', function (Request $request) use ($app) {

    $O = new SysmaObject($request->request->get('sysma_object_id'), $app);
    if (
        hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'gestionnaire', $app)
        or (hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'contributeur', $app) and $O->modified_by == $app['session']->get('user_id'))
    ) {


        return $app['twig']->render('photo/photoCreationForm.twig', ['sysma_object_id' => $request->request->get('sysma_object_id'), 'photo_object_id' => dataIndex($app['pdo'])]);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cet objet';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('photo-object-create');



$app->post('/photo-object/create/commit', function (Request $request) use ($app) {

    $r = $request->request;
    $O = new SysmaObject($r->get('sysma_object_id'), $app);
    if (
        hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'gestionnaire', $app)
        or (hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'contributeur', $app) and $O->modified_by == $app['session']->get('user_id'))
    ) {



        $data = $request->request->all();

        if ($data['photo_type'] == 'photo_upload') {

            // $file = $request->files->all();

            if ($data['photo_file'] != null) {

                $dir = ROOT_URI . 'public/' . PHOTO_UPLOAD_PATH;
                $photo_code = uniqid();
                $photoName = $photo_code . '.jpg';
                $photoNameMini = $photo_code . '_mini.jpg';


              

                $image = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data['photo_file']));

                // /* Process image with GD library */
                $verifyimg = getimagesizefromstring($image);
        
                // /* Make sure the MIME type is an image */
                $pattern = "#^(image/)[^\s\n<]+$#i";
        
                if (!preg_match($pattern, $verifyimg['mime'])) {
        
                    $app['session']->getFlashBag()->add('message', array('status' => 'danger', 'text' => 'Le fichier n\'est pas une image'));
                    return false ;
                }



                file_put_contents($dir . '/' . $photoName, $image);
                image_fix_orientation($dir . '/' . $photoName);

                resize_image($dir . '/' . $photoName, 'thumbnail', 500, 350);
                resize_image($dir . '/' . $photoName, 'square', 151, 150, true);

                $form_data = $r->all();

                $form_data['photo_file'] = $photo_code;
                $form_data['photo_id'] = null;
                $form_data['modified_by'] = $app['session']->get('user_id');
                if ($form_data['photo_date'] === "")
                    $form_data['photo_date'] = null;
                $P = new PhotoObject(null, $app);

                //var_dump($form_data) ;
                $res =  json_decode($P->create($app, $form_data));
                if ($res->status=='success') {
                    $app['session']->getFlashBag()->add('message', array('status' => 'success', 'text' => 'Photo ajoutée'));
                    return true ;
                } else {
                    $app['session']->getFlashBag()->add('message', array('status' => 'danger', 'text' => 'Erreur lors de l\'ajout de la photo'));
                    return false ;
                }
            }
        } elseif ($data['photo_type'] == 'photo' or $data['photo_type'] == 'folder') {
            $P = new PhotoObject(null, $app);
            $r->set('photo_date', null);
            return $P->create($app, $r);
        }
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cet objet';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('photo-object-create-commit');





// SUPPRIMER
$app->match('/photo-object/delete', function (Request $request) use ($app) {

    $OP = new PhotoObject($request->request->get('photo_object_id'), $app);
    $O = new SysmaObject($OP->sysma_object_id, $app);
    if (
        hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'gestionnaire', $app)
        or (hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'contributeur', $app) and $O->modified_by == $app['session']->get('user_id'))
    ) {

        return $OP->delete($app);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cet objet';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('photo-object-delete');
