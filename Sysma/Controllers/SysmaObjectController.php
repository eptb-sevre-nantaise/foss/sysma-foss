<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

$app->post('/objet', function (Request $request) use ($app) {

    if ($app['session']->get('user') === null) {
        loadUserFromSession($app, $request);
    }

    $O = new SysmaObject($request->request->get('id'), $app);



    if (
        hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'lecteur')
        or (hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'contributeur') and $app['session']->get('user_id') == $O->modified_by)
    ) {

        $O = new SysmaObject($request->request->get('id'), $app);
        $O->loadSysmaObjectInfos();
        $O->loadMainPhoto();

        $O->loadActualInfosSysmaObject();
        $O->loadSysmaActions();
        $O->loadSysmaRelations();
        $O->loadPhotoObjects();

        $TO = new SysmaObjectType($O->sysma_object_type_id, $app);
        $TO->loadSysmaActionTypes();
        $TO->loadSysmaRelationTypes();
        $TO->loadSysmaActionSheets($app['session']->get('organisation_id'), null);
        $TO->loadSysmaActionSheets(null, $app['session']->get('organisation_id'));

        // TODO : current hack : no relation association form when > 200 relations for this object type
        if ($TO->loadSysmaRelationSheetsCount() > 200) {
        } else {
            $TO->loadSysmaRelationSheets($app['session']->get('organisation_id'), null);
            $TO->loadSysmaRelationSheets(null, $app['session']->get('organisation_id'));
        }
        if ($request->request->get('message') != null) {
            $app['session']->getFlashBag()->add('message', array('status' => 'success', 'text' => $request->request->get('message')));
        }

        return $app['twig']->render('sysma-object/sysmaObjectPage.twig', ['O' => $O, 'TO' => $TO]);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à l\'objet ' . $request->request->get('id');
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('object');


$app->post('/objets', function (Request $request) use ($app) {

    $TO = new SysmaObjectType($request->request->get('sysma_object_type_id'), $app);

    if (hasAccess($app, $app['session']->get('user_id'), null, $TO->sysma_object_type_id, null, null, null, null, 'lecteur')) {

        $objects = $TO->loadSysmaObjects();
        foreach ($objects as $O) {

            if (
                hasAccess($app, $app['session']->get('user_id'), $O['organisation_id'], $O['sysma_object_type_id'], $O['status'], null, null, null, 'lecteur')
                or (hasAccess($app, $app['session']->get('user_id'), $O['organisation_id'], $O['sysma_object_type_id'], $O['status'], null, null, null, 'contributeur') and $app['session']->get('user_id') == $O['modified_by'])
            ) {
                $objectsArray[] = $O;
            }
        }
        return $app['twig']->render('sysma-object/sysmaObjectsPage.twig', ['objects' => $objectsArray]);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder au type d\'objet ' . $request->request->get('sysma_object_type_id');
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('objects');



$app->post('/objet/tab-form', function (Request $request) use ($app) {

    $O = new SysmaObject($request->request->get('id'), $app);

    if (
        hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'gestionnaire')
        or (hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'contributeur') and $app['session']->get('user_id') == $O->modified_by)
    ) {

        $O = new SysmaObject($request->request->get('id'), $app);

        $O->loadActualInfosSysmaObject();
        foreach ($O->data as $Val) {
            // var_dump($Val) ;
            $Val->loadChoices($Val->parameter->dataType, $app);
            // var_dump($Val) ;die() ;
        }


        return $app['twig']->render('sysma-object/sysmaObjectTabForm.twig', ['O' => $O, 'mode' => $request->request->get('mode')]);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à l\'objet ' . $request->request->get('id');
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('object-tab-form');



$app->get('/objets/tab-form', function (Request $request) use ($app) {

    $sysma_object_ids_list = $request->query->get('sysma_object_ids_list');
    $sysma_object_type_id = $request->query->get('sysma_object_type_id');


    // TODO SECURITE
    if (
        hasAccess($app, $app['session']->get('user_id'), null, $sysma_object_type_id, null, null, null, null, 'gestionnaire')
    ) {

        $tabO = null;


        foreach (explode(',', $sysma_object_ids_list) as $id) {

            $O = new SysmaObject($id, $app);

            if (
                hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'gestionnaire')
                or (hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'contributeur') and $app['session']->get('user_id') == $O->modified_by)
            ) {

                $O->loadActualInfosSysmaObjectWithIndexId();
                foreach ($O->dataIndex as $Val) {
                    // var_dump($Val) ;
                    $Val->loadChoices($Val->parameter->dataType, $app);
                    // var_dump($Val) ;die() ;
                }

                $tabO[] = $O;
            }
        }



        return $app['twig']->render('sysma-object/sysmaObjectsTabForm.twig', ['tabO' => $tabO, 'sysma_object_ids_list' => $sysma_object_ids_list, 'mode' => $request->query->get('mode')]);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à l\'objet ' . $request->query->get('id');
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('objects-tab-form');




$app->post('/objet/tab-form/commit', function (Request $request) use ($app) {

    $r = $request->request->all();




    if ($request->request->get('sysma_object_id') !== null) {
        $sysma_object_ids_list = $request->request->get('sysma_object_id');
    } else {
        $sysma_object_ids_list = $request->request->get('sysma_object_ids_list');
    }

    $sysmaObjects = explode(',', $sysma_object_ids_list);

    foreach ($sysmaObjects as $id) {

        $O = new SysmaObject($id, $app);



        if (
            hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'gestionnaire')
            or (hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'contributeur') and $app['session']->get('user_id') == $O->modified_by)
        ) {

            $O->loadActualInfosSysmaObject();
            foreach ($O->data as $V) {


                // the parameter must be treated
                if (isset($r['parameter' . $V->parameter->parameter_id]) and $r['parameter' . $V->parameter->parameter_id] == '1') {



                    if (
                        // non empty value 
                        (isset($r['valueO' . $O->id . 'P' . $V->parameter->parameter_id])
                            and $r['valueO' . $O->id . 'P' . $V->parameter->parameter_id] !== ''
                        )
                        and (
                            // no old value for this parameterr
                            empty($V->data_index)
                            // or there's an old value, but with different infos
                            or ($r['valueO' . $O->id . 'P' . $V->parameter->parameter_id] !== $V->value
                                or $r['start_date'] != $V->start_date
                            )
                        )
                    ) {



                        // $rc['status'] = 'error';
                        // $rc['data'] = $V;
                        // $rc['message'] = 'DBG '.__CLASS__.'->'.__FUNCTION__;
                        // // return json_encode(['result' => 'error']);
                        // return json_encode($rc);

                        // update sysma
                        /* suivi dans le temps et màj */
                        if ($V->parameter->time_tracking == 1 and isset($r['toggleO' . $O->id . 'P' . $V->parameter->parameter_id])) {
                            //$start_date = $r['start_date'];
                            // nouvelle donnée suivie dans le temps
                            if (empty($V->data_index)) {
                                $index = dataIndex($app['pdo']);
                            }

                            // there's an old data, 
                            else {
                                $index = $V->data_index;
                            }

                            $res = $V->update([
                                'value' => $r['valueO' . $O->id . 'P' . $V->parameter->parameter_id],
                                'start_date' => $r['start_date'],
                                'end_date' => $r['end_date'],
                                'parameter_id' => $V->parameter->parameter_id,
                                'sysma_object_id' => $O->id,
                                'data_index' => $index,
                                'user_id' => $app['session']->get('user_id'),
                                'modified_by' => $app['session']->get('user_id'),
                                'modified_at' => date('Y-m-d H:i:s')
                            ]);
                        }

                        // modification sysma
                        // suivi dans le temps mais en mode correction
                        /* correction */ elseif ($V->parameter->time_tracking == 1 and !isset($r['toggleO' . $O->id . 'P' . $V->parameter->parameter_id])) {
                            //$start_date = $r['start_date'];

                            if (empty($V->data_index)) {
                                $res = $V->create([
                                    'value' => $r['valueO' . $O->id . 'P' . $V->parameter->parameter_id],
                                    'start_date' => $r['start_date'],
                                    'end_date' => '0',
                                    'parameter_id' => $V->parameter->parameter_id,
                                    'sysma_object_id' => $O->id,
                                    'user_id' => $app['session']->get('user_id'),
                                    'modified_by' => $app['session']->get('user_id'),
                                    'modified_at' => date('Y-m-d H:i:s'),
                                    'data_index' => dataIndex($app['pdo'])
                                ]);
                            }

                            // there's an old data, 
                            else {
                                $res = $V->modify([
                                    'value' => $r['valueO' . $O->id . 'P' . $V->parameter->parameter_id],
                                    'start_date' => $r['start_date'],
                                    'end_date' => $r['end_date'],
                                    'modified_by' => $app['session']->get('user_id'),
                                    'modified_at' => date('Y-m-d H:i:s'),
                                    'data_index' => $V->data_index
                                ]);
                            }
                        }

                        /* nouvelle value */ elseif ($V->parameter->time_tracking != 1 and $V->value != $r['valueO' . $O->id . 'P' . $V->parameter->parameter_id]) {

                            if (empty($V->data_index)) {
                                $res = $V->create([
                                    'value' => $r['valueO' . $O->id . 'P' . $V->parameter->parameter_id],
                                    'start_date' => $r['start_date'],
                                    'end_date' => '0',
                                    'parameter_id' => $V->parameter->parameter_id,
                                    'sysma_object_id' => $O->id,
                                    'user_id' => $app['session']->get('user_id'),
                                    'modified_by' => $app['session']->get('user_id'),
                                    'modified_at' => date('Y-m-d H:i:s'),
                                    'data_index' => dataIndex($app['pdo'])
                                ]);
                            }

                            // there's an old data, 
                            else {
                                $res = $V->modify([
                                    'value' => $r['valueO' . $O->id . 'P' . $V->parameter->parameter_id],
                                    'start_date' => $r['start_date'],
                                    'end_date' => $r['end_date'],
                                    'modified_by' => $app['session']->get('user_id'),
                                    'modified_at' => date('Y-m-d H:i:s'),
                                    'data_index' => $V->data_index
                                ]);
                            }
                        } else {
                        }
                    } elseif (!empty($V->parameter->choicesArray) and $V->parameter->dataType == 'multipleTextChoiceType') { //
                        /**********************************************************************
                         * Alr : Quick&D way to deal with multipleTextChoiceType 
                         * TODO: refactor (factorise++) with above code 
                         **********************************************************************/
                        $sopv = new SysmaObjectParameterValue(null, $app); // create a dummy SysmaObject to get std structure
                        $sopv->parameter_id = $V->parameter->parameter_id;
                        $sopv->idsysma_object = $O->id;
                        $sopv->start_date = $r['start_date'];
                        $sopv->end_date = $r['end_date'];
                        $sopv->modified_at = date('Y-m-d H:i:s');
                        $sopv->iduser = $app['session']->get('user_id');
                        $sopv->modified_by = $sopv->iduser;


                        $values = array();
                        foreach ($V->parameter->choicesArray as $choice) {
                            $choiceId = $choice['choice_id'];
                            if (!empty($r['choiceC' . $choiceId . 'O' . $V->idsysma_object . 'P' . $V->parameter_id])) {
                                $values[] = $choice['nom'];
                            }
                        }

                        /* create new or correction */
                        if (($V->parameter->time_tracking != 1) or ($V->parameter->time_tracking == 1 and !isset($r['toggleO' . $O->id . 'P' . $V->parameter->parameter_id]))) {
                            // $sopv->dataIndex = dataIndex($app['pdo']);
                            //echo 'correction' ;                       
                            $rc = $V->multipleModifyByValues($sopv, $values);
                        }
                        /* suivi dans le temps et màj */ elseif ($V->parameter->time_tracking == 1 and isset($r['toggleO' . $O->id . 'P' . $V->parameter->parameter_id])) {
                            // echo 'suivi dans le temps' ;
                            //var_dump($V->data_index) ;

                            if (empty($V->data_index)) {
                                $rc = $V->multipleModifyByValues($sopv, $values);
                            } else {
                                // there's an old data, 
                                $rc = $V->multipleUpdateByValues($sopv, $values);
                            }
                        }
                    }
                }
            }
        } else {
            $erreur['message'] = 'Vous ne pouvez pas accéder à l\'objet ' . $request->request->get('id');
            return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
        }
    }

    $app['session']->set('start_date_value', $r['start_date']);
    return json_encode(['result' => 'success']);
})->bind('object-tab-form-commit');





$app->match('/objet/create', function (Request $request) use ($app) {

    if (hasAccess($app, $app['session']->get('user_id'), null, null, null, null, null, null, 'gestionnaire') or hasAccess($app, $app['session']->get('user_id'), null, null, null, null, null, null, 'contributeur')) {


        $surface = $lineaire = null;
        $pdo = $app['pdo'];

        $coords = convertCoords($request->request->get('geom'));
        if ($request->request->get('geometry_type') == 'no_geom') {
            $geomType = 'no_geom';
            $app['session']->set('sysma_object_type_id', $request->request->get('sysma_object_type_id'));
        } else {
            $geomType = getGeomTypeFromCoords($coords);
        }

        // TODO: use TkGeo::convertGeojsonObjectFormatToPostGisGeomFormat()?
        // attention cette fonction ne gere pas le calcul de d'air (surface) ni du linéraire.
        if ($geomType === 'ST_Polygon') {
            $qry = $pdo->prepare('select ST_Transform(ST_SetSRID(ST_MakePolygon(ST_MakeValid(ST_GeomFromText(\'LINESTRING(' . convertCoords($request->request->get('geom')) . ')\'))),4326),2154)::geometry(polygon,2154) as geom, ST_area(ST_Transform(ST_SetSRID(ST_MakePolygon(ST_MakeValid(ST_GeomFromText(\'LINESTRING(' . convertCoords($request->request->get('geom')) . ')\'))),4326),2154)) as surface');
            $qry->execute();
            $res = $qry->fetchObject();
            $surface = $res->surface;
        } elseif ($geomType === 'ST_Point') {
            $qry = $pdo->prepare('select ST_Transform(ST_SetSRID(ST_GeomFromText(\'POINT(' . convertCoords($request->request->get('geom')) . ')\'),4326),2154)::geometry(point,2154) as geom');
            $qry->execute();
            $res = $qry->fetchObject();
        } elseif ($geomType === 'ST_Linestring') {
            $qry = $pdo->prepare('select ST_Transform(ST_SetSRID(ST_MakeValid(ST_GeomFromText(\'LINESTRING(' . convertCoords($request->request->get('geom')) . ')\')),4326),2154)::geometry(linestring,2154) as geom, ST_Length(ST_Transform(ST_SetSRID(ST_MakeValid(ST_GeomFromText(\'LINESTRING(' . convertCoords($request->request->get('geom')) . ')\')),4326),2154)::geometry(linestring,2154)) as lineaire');
            $qry->execute();
            $res = $qry->fetchObject();
            $lineaire = $res->lineaire;
        } elseif ($geomType === 'no_geom') {
            $res = new stdClass();
            $res->geom = $lineaire = null;
        }


        return $app['twig']->render('sysma-object/sysmaObjectCreationForm.twig', ['index' => dataIndex($app['pdo']), 'geom' => $res->geom, 'surface' => $surface, 'lineaire' => $lineaire, 'tabTO' => tabTO($geomType, $app), 'tabStatuts' => statusList('sysma_object', $app), 'taborganisation' => organisationList($app)]);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas créer un objet';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('object-create');


$app->match('/objet/update-geom', function (Request $request) use ($app) {

    $r = $request->request;
    $O = new SysmaObject(substr($r->get('id_layer'), 1), $app);

    if (
        hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'gestionnaire') or (hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'contributeur') and $O->modified_by == $app['session']->get('user_id'))
    ) {

        // TODO: use TkGeo::convertGeojsonObjectFormatToPostGisGeomFormat()?
        // attention cette fonction ne gere pas le calcul de d'air (surface) ni du linéraire.
        $pdo = $app['pdo'];
        if ($request->request->get('geometry_type') === 'ST_Polygon') {
            $geom = 'select ST_Transform(ST_SetSRID(ST_MakePolygon(ST_MakeValid(ST_GeomFromText(\'LINESTRING(' . convertCoords($request->request->get('geom')) . ')\'))),4326),2154)::geometry(polygon,2154) as geom, ST_area(ST_Transform(ST_SetSRID(ST_MakePolygon(ST_MakeValid(ST_GeomFromText(\'LINESTRING(' . convertCoords($request->request->get('geom')) . ')\'))),4326),2154)) as surface';
        } elseif ($request->request->get('geometry_type') === 'ST_Point') {
            $geom = 'select ST_Transform(ST_SetSRID(ST_GeomFromText(\'POINT(' . convertCoords($request->request->get('geom')) . ')\'),4326),2154)::geometry(point,2154) as geom';
        } else if ($request->request->get('geometry_type') === 'ST_Linestring') {
            $geom = 'select ST_Transform(ST_SetSRID(ST_MakeValid(ST_GeomFromText(\'LINESTRING(' . convertCoords($request->request->get('geom')) . ')\')),4326),2154)::geometry(linestring,2154) as geom, ST_Length(ST_Transform(ST_SetSRID(ST_MakeValid(ST_GeomFromText(\'LINESTRING(' . convertCoords($request->request->get('geom')) . ')\')),4326),2154)::geometry(linestring,2154)) as lineaire';
        } else {
            die();
        }

        $qry = $pdo->prepare($geom);
        $qry->execute();
        $res = $qry->fetchObject();

        $app['session']->getFlashBag()->add('message', array('text' => 'Tracé corrigé', 'status' => 'success'));

        return $O->updateGeom($res->geom);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à l\'objet ' . $r->get('sysma_object_id');
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('object-update-geom');



$app->match('/set-idtypeobjet-session', function (Request $request) use ($app) {

    $app['session']->set('sysma_object_type_id', $request->request->get('sysma_object_type_id'));
    return ' ';
})->bind('set-idtypeobjet-session');




// OBJET GET
// pour print (entre autre)

$app->get('/objet/{id}', function ($id, Request $request) use ($app) {

    $O = new SysmaObject($id, $app);


    if (
        hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'lecteur')
        or (hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'contributeur') and $app['session']->get('user_id') == $O->modified_by)
    ) {


        if ($O = new SysmaObject($id, $app)) {

            $O->loadSysmaObjectInfos();
            $O->loadMainPhoto();

            $O->loadActualInfosSysmaObject();
            $O->loadSysmaActions();
            foreach ($O->actionsArray as $T) {
                $T->loadActualInfosSysmaAction();
            }
            $O->loadPhotoObjects();
            $communes = null;
            if ($O->geometry_type != 'no_geom') {
                $geom = $O->loadGeom();
                $communes = findIntersectingObjects(REF_PG_SCHEMA_TABLE_CITIES, $geom, array('tampon' => 20, 'id' => REF_PG_SCHEMA_TABLE_CITIES_GID, 'object' => REF_PG_SCHEMA_TABLE_CITIES_NAME, 'geom' => REF_PG_SCHEMA_TABLE_CITIES_GEOM), $app);
            }

            $TO = new SysmaObjectType($O->sysma_object_type_id, $app);
            $TO->loadSysmaActionTypes();
            $TO->loadSysmaActionSheets($app['session']->get('organisation_id'), null);
            $TO->loadSysmaActionSheets(null, $app['session']->get('organisation_id'));

            $O->loadGeoInfos($TO->geometry_type);

            return $app['twig']->render('report/sysmaObjectSheetPrint.twig', ['O' => $O, 'TO' => $TO, 'tabO' => array($O), 'tabC' => $communes, 'GET' => $request->query, 'BASE_LAYERS' => loadBaseLayers($app, ($app['session']->get('user_id') != ANONYMID)), 'OTHER_BASE_LAYERS' => loadOtherBaseLayers($app, ($app['session']->get('user_id') != ANONYMID))]);
        } else {
            $erreur['message'] = 'L\'objet ' . $id . ' n\'existe pas';
            return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
        }
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à l\'objet ' . $id;
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->assert('id', '\d+')->bind('object-form');




$app->match('/object/{id}/geo-infos', function ($id) use ($app) {

    $O = new SysmaObject($id, $app);

    if (
        hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'lecteur')
        or (hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'contributeur') and $app['session']->get('user_id') == $O->modified_by)
    ) {


        $TyO = new SysmaObjectType($O->sysma_object_type_id, $app);
        $O->loadGeoInfos($TyO->geometry_type);
        $geom = $O->loadGeom();
        $communes = findIntersectingObjects(REF_PG_SCHEMA_TABLE_CITIES, $geom, array('tampon' => 20, 'id' => REF_PG_SCHEMA_TABLE_CITIES_GID, 'object' => REF_PG_SCHEMA_TABLE_CITIES_NAME, 'geom' => REF_PG_SCHEMA_TABLE_CITIES_GEOM), $app);

        return $app['twig']->render('sysma-object/sysmaObjectGeoInfosSheet.twig', ['O' => $O, 'TyO' => $TyO, 'tabC' => $communes]);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à l\'objet ' . $id;
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('object-geo-infos');






$app->match('/objet/{id}/infos-cadastre/{buffer}', function ($id, $buffer) use ($app) {



    $O = new SysmaObject($id, $app);

    if ($app['session']->get('has_cadastre_access')) {

        $TyO = new SysmaObjectType($O->sysma_object_type_id, $app);
        $O->loadGeoInfos($TyO->geometry_type);
        $geom = $O->loadGeom();

        $parcelles = findCadastreObjectInfos($app, $geom, $buffer);

        $cadastreVersionAnnee = findCadastreVersionAnnee($app);


        return $app['twig']->render('sysma-object/sysmaObjectCadastreSheet.twig', ['O' => $O, 'parcelles' => $parcelles
                , 'buffer' => $buffer, 'cadastreVersionAnnee' => $cadastreVersionAnnee
        ]);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à l\'objet ' . $id;
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('objet-infos-cadastre');





$app->match('/objet/{id}/infos-cadastre-export-shape/{buffer}', function ($id, $buffer) use ($app) {


    $O = new SysmaObject($id, $app);

    if ($app['session']->get('has_cadastre_access')) {

        $TyO = new SysmaObjectType($O->sysma_object_type_id, $app);
        $O->loadGeoInfos($TyO->geometry_type);
        $geom = $O->loadGeom();

        $unic = uniqid();
        $folder = dirname(__DIR__) . '/../export_shp/' . $unic . '/';
        $zipFileInfos = createCadastreInfosObjectShape($app, $geom, $folder, $buffer);


        if ($zipFileInfos['zipFile'] != false) {

            $response = new BinaryFileResponse($zipFileInfos['zipFile']);
            $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);
            $app['folders_to_delete'] = $zipFileInfos['foldersToDelete'];
            ob_clean(); // Mandatory with BinaryFileResponse! This fix this wtf bug that add a line-break (0x0A) before the binary file content... Oo
            return $response;
        } else {
            $erreur['message'] = 'Erreur lors de la création de la layer';
            return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
        }
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à l\'objet ' . $id;
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('objet-infos-cadastre-export-shape');

// delete files after export and zip
$app->finish(function (Request $request, Response $response) use ($app) {
    if (isset($app["folders_to_delete"])) {

        foreach ($app["folders_to_delete"] as $f) {
            rrmdir($f);
        }
    }
});



$app->match('/objet/{id}/infos-cadastre-geo/{buffer}', function ($id, $buffer) use ($app) {

    if ($app['session']->get('has_cadastre_access')) {
        return '<script>
        
        if (myCadastreLayer !== undefined) { map.removeLayer(myCadastreLayer); }

        var myCadastreLayer = new L.GeoJSON.AJAX("/geojson/cadastre/' . $id . '/' . $buffer . '", 
            { 
                id: "' . $id . '098",
                name: "' . $id . '098",
                style:{ "color" :"#ff9933", "opacity" :"1", "weight" :"2", "fillOpacity" :"0.2",  }, 
                onEachFeature: popUp,    
                
            }
            
        );
        groupCad.addLayer(myCadastreLayer) ;
        </script>';
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à l\'objet ' . $id;
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('objet-infos-cadastre-geo');


$app->match('/objet/{id}/infos-cadastre-csv/{buffer}', function ($id, $buffer) use ($app) {

    $O = new SysmaObject($id, $app);

    if ($app['session']->get('has_cadastre_access')) {

        $TyO = new SysmaObjectType($O->sysma_object_type_id, $app);
        $O->loadGeoInfos($TyO->geometry_type);
        $geom = $O->loadGeom();

        $parcelles = findCadastreObjectInfos($app, $geom, $buffer);
        $content = $app['twig']->render('sysma-object/sysmaObjectCadastreCSVSheet.twig', ['O' => $O, 'parcelles' => $parcelles, 'buffer' => $buffer]);

        $response = new Response();
        $response->setContent($content);
        $response->headers->set('Content-Type', 'text/csv');
        $response->headers->set('Content-disposition', 'filename=sysma_infoscadastre_objet' . $id . '_tamp' . $buffer . '.csv');
        ob_clean(); // Mandatory ! This fix this wtf bug that add a line-break (0x0A) at the begining of the response... Oo
        return $response;
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à l\'objet ' . $id;
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('objet-infos-cadastre-csv');


$app->match('/objet/{id}/infos-cadastre-publipostage-csv/{buffer}', function ($id, $buffer) use ($app) {

    $O = new SysmaObject($id, $app);

    if ($app['session']->get('has_cadastre_access')) {

        $TyO = new SysmaObjectType($O->sysma_object_type_id, $app);
        $O->loadGeoInfos($TyO->geometry_type);
        $geom = $O->loadGeom();

        $parcelles = findMailingCadastreInfoObject($app, $geom, $buffer, 1);
        $content = $app['twig']->render('sysma-object/sysmaObjectCadastreCSVMailingSheet.twig', ['O' => $O, 'parcelles' => $parcelles, 'buffer' => $buffer]);

        $response = new Response();
        $response->setContent($content);
        $response->headers->set('Content-Type', 'text/csv');
        $response->headers->set('Content-disposition', 'filename=sysma_infoscadastre_publipost_objet' . $id . '_tamp' . $buffer . '.csv');
        ob_clean(); // Mandatory ! This fix this wtf bug that add a line-break (0x0A) at the begining of the response... Oo
        return $response;
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à l\'objet ' . $id;
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('objet-infos-cadastre-publipostage-csv');






// CORRIGER
$app->match('/objet/corriger', function (Request $request) use ($app) {



    $O = new SysmaObject($request->request->get('id'), $app);
    if (
        hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'gestionnaire') or (hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'contributeur') and $O->modified_by == $app['session']->get('user_id'))
    ) {
        return $app['twig']->render('sysma-object/sysmaObjectModificationForm.twig', ['O' => $O, 'tabStatuts' => statusList('sysma_object', $app), 'taborganisation' => organisationList($app)]);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à l\'objet ' . $request->request->get('id');
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('object-modify');




// CORRIGER
$app->match('/objets/corriger', function (Request $request) use ($app) {


    $sysma_object_ids_list = $request->query->get('sysma_object_ids_list');
    $sysma_object_type_id = $request->query->get('sysma_object_type_id');


    // TODO SECURITE
    if (
        hasAccess($app, $app['session']->get('user_id'), null, $sysma_object_type_id, null, null, null, null, 'gestionnaire')
    ) {

        $tabO = null;


        foreach (explode(',', $sysma_object_ids_list) as $id) {

            $O = new SysmaObject($id, $app);

            if (
                hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'gestionnaire')
                or (hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'contributeur') and $app['session']->get('user_id') == $O->modified_by)
            ) {

                $O->loadActualInfosSysmaObjectWithIndexId();
                foreach ($O->dataIndex as $Val) {
                    // var_dump($Val) ;
                    $Val->loadChoices($Val->parameter->dataType, $app);
                    // var_dump($Val) ;die() ;
                }

                $tabO[] = $O;
            }
        }


        return $app['twig']->render('sysma-object/sysmaObjectsModificationForm.twig', ['tabO' => $tabO, 'sysma_object_ids_list' => $sysma_object_ids_list, 'tabStatuts' => statusList('sysma_object', $app), 'taborganisation' => organisationList($app)]);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à l\'objet ' . $request->request->get('id');
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('objects-modify');



$app->match('/objet/corriger/commit', function (Request $request) use ($app) {

    $request->request->set('modified_by', $app['session']->get('user_id'));
    $request->request->set('modified_at', date('Y-m-d H:i:s'));

    $r = $request->request;


    if ($request->request->get('sysma_object_id') !== null) {
        $sysma_object_ids_list = $r->get('sysma_object_id');
    } else {
        $sysma_object_ids_list = $r->get('sysma_object_ids_list');
    }

    $sysmaObjects = explode(',', $sysma_object_ids_list);

     // data preparation
     $tabD = ['parameterObjectName' => 'PObjectName', 'parameterStatus' => 'PStatus', 'parameterStartDate' => 'PStartDate', 'parameterEndDate' => 'PEndDate', 'parameterOrganisation' => 'POrganisation'];
     $tabParamNameInDB = ['parameterObjectName' => 'sysma_object', 'parameterStatus' => 'status', 'parameterStartDate' => 'start_date', 'parameterEndDate' => 'end_date', 'parameterOrganisation' => 'organisation_id'];
   

    foreach ($sysmaObjects as $id) {

        $data = array();
        $O = new SysmaObject($id, $app);

        if (
            hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'gestionnaire') or (hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'contributeur') and $O->modified_by == $app['session']->get('user_id'))
        ) {
          
            foreach ($tabD as $key => $d) {
                if ($r->get($key) !== null and $r->get($key) == '1') {
                    $data[$tabParamNameInDB[$key]] = $r->get('valueO' . $O->id . $d);
                }  
            }
            $data['sysma_object_id'] = $O->id;
            $data['modified_by'] = $app['session']->get('user_id');
           
           // var_dump($data) ;
            $res[] = $O->update($data);
        } else {
            $erreur['message'] = 'Vous ne pouvez pas accéder à l\'objet ' . $r->get('sysma_object_id');
            return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
        }
    }

   // die() ;
    return $res[0];

})->bind('object-modify-commit');


$app->match('/objet/creer/commit', function (Request $request) use ($app) {

    $request->request->set('modified_by', $app['session']->get('user_id'));
    $request->request->set('created_by', $app['session']->get('user_id'));

    $r = $request->request;

    if (
        hasAccess($app, $app['session']->get('user_id'), $r->get('organisation_id'), $r->get('sysma_object_type_id'), $r->get('status'), null, null, null, 'gestionnaire') or (hasAccess($app, $app['session']->get('user_id'), $r->get('organisation_id'), $r->get('sysma_object_type_id'), $r->get('status'), null, null, null, 'contributeur'))
    ) {
        $O = new SysmaObject(null, $app);

        if ($r->get('geom') == '')
            $r->set('geom', null);
        return $O->create($r);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas créer un objet de ce type';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('object-create-commit');





// SUPPRIMER
$app->match('/objet/supprimer', function (Request $request) use ($app) {

    $O = new SysmaObject($request->request->get('sysma_object_id'), $app);
    if (
        hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'gestionnaire') or (hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'contributeur') and $O->modified_by == $app['session']->get('user_id') and $app['session']->get('user_id') != ANONYMID)
    ) {
        if ($O->delete()) {
            $app['session']->getFlashBag()->add('message', array('status' => 'success', 'text' => 'Objet supprimé'));
        } else {
            $app['session']->getFlashBag()->add('message', array('status' => 'error', 'text' => 'Erreur lors de la suppression'));
        }
        return $app->redirect($app['url_generator']->generate('map'));
    } else {
        $erreur['message'] = 'Vous ne pouvez pas supprimer cet objet ';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('object-delete');


// ASSOCIER TRAVAUX
$app->match('/objet/associer-sysma_action', function (Request $request) use ($app) {

    $O = new SysmaObject($request->request->get('sysma_object_id'), $app);
    if (
        hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'gestionnaire') or (hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'contributeur') and $O->modified_by == $app['session']->get('user_id'))
    ) {
        $O->linkSysmaActionSheet($request->request->get('sysma_action_id'), $app['session']->get('user_id'));
        return true;
    } else {
        return false;
    }
})->bind('object-link-work');




// DISSOCIER TRAVAUX
$app->match('/objet/dissocier-sysma_action', function (Request $request) use ($app) {


    $O = new SysmaObject($request->request->get('sysma_object_id'), $app);
    if (
        hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'gestionnaire') or (hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'contributeur') and $O->modified_by == $app['session']->get('user_id'))
    ) {
        if ($O->unlinkSysmaActionSheet($request->request->get('sysma_action_id')))
            return 'Fiche dissociée';
    } else {
        return 'Vous ne pouvez pas dissocier cette fiche sysma_action';
    }
})->bind('object-unlink-work');
