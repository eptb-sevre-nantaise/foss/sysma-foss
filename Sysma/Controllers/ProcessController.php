<?php

use Symfony\Component\HttpFoundation\Request;

$app->get('/processes', function (Request $request) use ($app) {


    if (hasAccess($app, $app['session']->get('user_id'), null, null, null, null, null, null, 'gestionnaire')) {

        foreach ($GLOBALS['PROCESSES'] as $key => $P) {
            if (hasAccess($app, $app['session']->get('user_id'), null, $key, null, null, null, null, 'gestionnaire')) {
                $TyO = new SysmaObjectType($key, $app);
                $tabTyO[$key] = $TyO;
            }
        }

        //TODO pouvoir ne pas filter par secteur
        //$tabS = array_merge(['false' => 'Ne pas limiter à un secteur'], objectsInTableList($app, array('table' => REF_PG_SCHEMA_TABLE_MASSEEAU, 'sysma_object_id' => REF_PG_SCHEMA_TABLE_MASSEEAU_GID, 'sysma_object' => REF_PG_SCHEMA_TABLE_MASSEEAU_NAME, 'geom' => REF_PG_SCHEMA_TABLE_MASSEEAU_GEOM)));
        $tabS = objectsInTableList($app, array('table' => REF_PG_SCHEMA_TABLE_MASSEEAU, 'sysma_object_id' => REF_PG_SCHEMA_TABLE_MASSEEAU_GID, 'sysma_object' => REF_PG_SCHEMA_TABLE_MASSEEAU_NAME, 'geom' => REF_PG_SCHEMA_TABLE_MASSEEAU_GEOM));

        return $app['twig']->render('processes/processesPage.twig', ['tabTyO' => $tabTyO, 'tabP' => $GLOBALS['PROCESSES'], 'tabS' => $tabS]);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('processes');


$app->post('/process/load-geoobjects', function (Request $request) use ($app) {


    $r = $request->request;
    $secteur = explode('.', $r->get('secteur'));
    $id_secteur = $secteur[5];
    $process = explode('|', $r->get('process'));
    $id_process = $process[1];
    $sysma_object_type_id = $process[0];



    if (hasAccess($app, $app['session']->get('user_id'), null, $sysma_object_type_id, null, null, null, null, 'gestionnaire')) {
        $Secteur = loadObject($app, ['table' => REF_PG_SCHEMA_TABLE_MASSEEAU, 'geom' => REF_PG_SCHEMA_TABLE_MASSEEAU_GEOM, 'sysma_object' => REF_PG_SCHEMA_TABLE_MASSEEAU_NAME, 'sysma_object_id' => REF_PG_SCHEMA_TABLE_MASSEEAU_GID, 'id' => $id_secteur]);


        $tabO = loadSysmaSysmaObjectsInGeom($app, $Secteur->geom, ['filter' => ' sysma_object_type_id = ' . $sysma_object_type_id]);

        if (count($tabO) == 0) {
            return 'Pas d\'objet de ce type sur ce secteur';
        }

        $tabIdObjects = array();

        foreach ($tabO as $O) {
            if (hasAccess($app, $app['session']->get('user_id'), $O['organisation_id'], $sysma_object_type_id, $O['status'], null, null, null, 'gestionnaire')) {
                $tabIdObjects[] = $O['sysma_object_id'];
            }
        }



        return $app['twig']->render('processes/processForm.twig', ['Process' => $GLOBALS['PROCESSES'][$sysma_object_type_id][$id_process], 'id_process' => $id_process, 'sysma_object_type_id' => $sysma_object_type_id, 'sysma_object_id' => implode('|', $tabIdObjects)]);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('process-load-geoobjects');



$app->post('/processes/{sysma_object_type_id}', function (Request $request, $sysma_object_type_id) use ($app) {

    if (hasAccess($app, $app['session']->get('user_id'), null, $sysma_object_type_id, null, null, null, null, 'gestionnaire')) {
        if (count($GLOBALS['PROCESSES'][$sysma_object_type_id]) > 0) {

            return $app['twig']->render('processes/sysmaObjectProcessesPage.twig', ['sysma_object_type_id' => $sysma_object_type_id, 'sysma_object_id' => $request->request->get('sysma_object_id'), 'Processes' => $GLOBALS['PROCESSES'][$sysma_object_type_id]]);
        } else {
            return 'No processes defined for this type of object';
        }
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('processes-sysma-object-type');


$app->post('/process/{id}/form', function (Request $request, $id) use ($app) {

    $r = $request->request;
    $sysma_object_type_id = $r->get('sysma_object_type_id');

    if (hasAccess($app, $app['session']->get('user_id'), null, $sysma_object_type_id, null, null, null, null, 'gestionnaire')) {

        $TyO = new SysmaObjectType($sysma_object_type_id, $app);
        $sysma_object_id = $r->get('sysma_object_id');
        if (isset($GLOBALS['PROCESSES'][$sysma_object_type_id][$id])) {
            return $app['twig']->render('processes/processForm.twig', ['Process' => $GLOBALS['PROCESSES'][$sysma_object_type_id][$id], 'sysma_object_id' => $sysma_object_id, 'TyO' => $TyO]);
        }
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('process-form');


$app->post('/process/{id}', function (Request $request, $id) use ($app) {


    $r = $request->request;

    //var_dump($r->get('update_function')) ;

    $sysma_object_type_id = $r->get('sysma_object_type_id');
    $sysma_object_id = $r->get('sysma_object_id');
    $tabO = explode('|', $sysma_object_id);
    $calculation_date = $r->get('calculation_date');


    if ($r->get('update_function') == null) {
        return 'Précisez l\'action';
    }


    if (hasAccess($app, $app['session']->get('user_id'), null, $sysma_object_type_id, null, null, null, null, 'gestionnaire')) {


        // if the process is declared
        if (isset($GLOBALS['PROCESSES'][$sysma_object_type_id][$id])) {


            $textResult = null;

            $process = $GLOBALS['PROCESSES'][$sysma_object_type_id][$id];
            include(ROOT_URI . 'Processes' . $process['path']);

            foreach ($tabO as $sysma_object_id) {

                $O = new SysmaObject($sysma_object_id, $app);

                if (!hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'gestionnaire')) {
                    die('Vous ne pouvez pas accéder à cette page');
                }


                $textResult .= '<p><a href="/carte/objet/' . $sysma_object_id . '" target="_blank">Objet ' . $sysma_object_id . '</a></p>';
                // mandatory arguments of the process
                $data['app'] = $app;
                $data['sysma_object_id'] = $sysma_object_id;
                $data['calculation_date'] = $calculation_date;

                // loading the arguments of the process
                foreach ($process['args'] as $keyarg => $arg) {
                    if ($r->get('' . $keyarg) !== null) {
                        $data[$keyarg] = $r->get('' . $keyarg);
                    }
                }


                // add status_exec
                if ($tabRes = call_user_func_array($process['id'], $data)) {
                    $result['status_exec'] = 'success';
                } else {
                    $result['status_exec'] = 'failure';
                }

                $result['res'] = $tabRes;



                foreach ($tabRes as $res_key => $res) {


                    if ($res === null) {
                        $textResult .= '<span class="text-danger">L\'indice ' . $res_key . ' n\'a pas pu être calculé.</span><br>';
                    } elseif (!is_array($res)) {
                        $textResult .= '' . $res_key . ' : ' . $res . '<br>';
                    }


                    // this result is linked with a sysma parameter
                    if ($res !== null and isset($process['return_args'][$res_key]['parameter_id']) and !in_array($process['return_args'][$res_key]['parameter_id'], ['geoobject_geom', 'ids_sysma_object_list', 'new_object'])) {



                        $V = new SysmaObjectParameterValue();
                        $P = new SysmaObjectParameter($process['return_args'][$res_key]['parameter_id'], $app);
                        $V->buildFromParameters($sysma_object_id, $P->parameter_id, $calculation_date, $app);

                        // modification asked with an old value existing
                        if (!empty($V->data_index) and $r->get('update_function') != 'display') {


                            if ($r->get('update_function') == 'modify') {
                                $result0 = $V->modify(['value' => $res, 'data_index' => $V->data_index, 'start_date' => $calculation_date, 'end_date' => 0, 'user_id' => $app['session']->get('user_id'), 'modified_by' => $app['session']->get('user_id'), 'modified_at' => date('Y-m-d H:i:s')]);
                            }
                            if ($r->get('update_function') == 'update') {
                                $result0 = $V->update(['sysma_object_id' => $sysma_object_id, 'parameter_id' => $P->parameter_id, 'start_date' => $calculation_date, 'end_date' => 0, 'value' => $res, 'data_index' => $V->data_index, 'user_id' => $app['session']->get('user_id'), 'modified_by' => $app['session']->get('user_id'), 'created_at' => date('Y-m-d H:i:s'), 'modified_at' => date('Y-m-d H:i:s')]);
                            }

                            $result0 = get_object_vars(json_decode($result0));

                            if ($result0['status'] == 'success') {
                                $textResult .= '<p><span class="btn btn-success btn-xs"><span class="glyphicon glyphicon-ok"></span></span> ';
                                $textResult .= 'Correction du la value du paramètre ' . $P->parameter . ' : ' . $V->value . ' -> ' . $res . ' ';
                            } else {
                                die('Erreur');
                            }
                            $textResult .= '</p>';


                            // modification asked with no old value
                        } elseif (empty($V->data_index) and $r->get('update_function') != 'display') {

                            $result0 = $V->create(['sysma_object_id' => $sysma_object_id, 'parameter_id' => $P->parameter_id, 'start_date' => $calculation_date, 'end_date' => 0, 'value' => $res, 'data_index' => dataIndex($app['pdo']), 'modified_by' => $app['session']->get('user_id'), 'user_id' => $app['session']->get('user_id'), 'modified_at' => date('Y-m-d H:i:s')]);
                            $result0 = get_object_vars(json_decode($result0));


                            if ($result0['status'] == 'success') {
                                $textResult .= '<p><span class="btn btn-success btn-xs"><span class="glyphicon glyphicon-ok"></span></span> ';
                                $textResult .= 'Création du la value du paramètre ' . $P->parameter . ' : ' . $res . '</p> ';
                            } else {
                                die('Erreur');
                            }
                            $textResult .= '</p>';
                        } else {
                        }
                    }

                    // this result is a geom object
                    elseif ($res !== null and isset($process['return_args'][$res_key]['parameter_id']) and $process['return_args'][$res_key]['parameter_id'] == 'geoobject_geom') {



                        if ($r->get('update_function') !== 'display') {
                            $O = new SysmaObject($sysma_object_id, $app);
                            $result0 = $O->updateGeom($res);
                            $result0 = get_object_vars(json_decode($result0));

                            if ($result0['status'] == 'success') {
                                $textResult .= '<p><span class="btn btn-success btn-xs"><span class="glyphicon glyphicon-ok"></span></span> ';
                                $textResult .= 'Géométrie de l\'objet mise à jour';
                                $textResult .= '<script type="text/javascript">window.location = "/carte/objet/' . $sysma_object_id . '";</script>';
                            } else {
                                die('Erreur');
                            }
                        } else {
                            $textResult .= 'Nouvelle géométrie calculée';
                        }
                    }


                    // this result is a geom object
                    elseif ($res !== null and isset($process['return_args'][$res_key]['parameter_id']) and $process['return_args'][$res_key]['parameter_id'] == 'new_object') {


                        if ($r->get('update_function') !== 'display') {



                            $O = new SysmaObject(null, $app);
                            $result0 = $O->create($res);

                            $result0 = get_object_vars(json_decode($result0));

                            if ($result0['status'] == 'success') {
                                $textResult .= '<p><span class="btn btn-success btn-xs"><span class="glyphicon glyphicon-ok"></span></span> ';
                                $textResult .= 'Nouvel objet créé : ';
                                $textResult .= '<a href="/carte/objet/' . $result0['id'] . '">' . $res['sysma_object'] . ' <small>[' . $result0['id'] . ']</a>';
                            } else {
                                die('Erreur');
                            }
                        } else {
                            $textResult .= 'Nouvel objet calculé';
                        }
                    }


                    // this result is a list of geoobjects we have to link with the main object
                    elseif ($res !== null and isset($process['return_args'][$res_key]['parameter_id']) and $process['return_args'][$res_key]['parameter_id'] == 'ids_sysma_object_list') {


                        $P = new SysmaObjectParameter($r->get('linked_parameter_id_to_update'), $app);


                        if ($r->get('update_function') !== 'display') {

                            foreach ($res as $s) {

                                //    var_dump($s);
                                //  $ToBeLinkedO = new SysmaObject($s['sysma_object_id'], $app);
                                $MainO = new SysmaObject($sysma_object_id, $app);
                                // value to be updated
                                $V = new SysmaObjectParameterValue();
                                $V->buildFromParameters($s['sysma_object_id'], $r->get('linked_parameter_id_to_update'), $calculation_date, $app);

                                $value = $MainO->sysma_object . ' [o' . $sysma_object_id . ']';

                                if (!empty($V->data_index)) {

                                    // var_dump($MainO->nom . ' [o' . $sysma_object_id . ']');

                                    $result0 = $V->modify(['value' => $value, 'data_index' => $V->data_index, 'start_date' => $calculation_date, 'end_date' => 0, 'user_id' => $app['session']->get('user_id'), 'modified_by' => $app['session']->get('user_id'), 'modified_at' => date('Y-m-d H:i:s')]);
                                    $result0 = get_object_vars(json_decode($result0));

                                    if ($result0['status'] == 'success') {
                                        $textResult .= '<p><span class="btn btn-success btn-xs"><span class="glyphicon glyphicon-ok"></span></span> ';
                                        $textResult .= 'Correction du la value du paramètre ' . $P->parameter . ' : ' . $V->value . ' -> ' . $value . ' ';
                                    } else {
                                        die('Erreur');
                                    }
                                } elseif (empty($V->data_index)) {

                                    // var_dump($MainO->nom . ' [o' . $sysma_object_id . ']');

                                    $result0 = $V->create(['sysma_object_id' => $s['sysma_object_id'], 'parameter_id' => $r->get('linked_parameter_id_to_update'), 'start_date' => $calculation_date, 'end_date' => 0, 'value' => $value, 'data_index' => dataIndex($app['pdo']), 'user_id' => $app['session']->get('user_id'), 'modified_by' => $app['session']->get('user_id'), 'created_at' => date('Y-m-d H:i:s'), 'modified_at' => date('Y-m-d H:i:s')]);
                                    $result0 = get_object_vars(json_decode($result0));


                                    if ($result0['status'] == 'success') {
                                        $textResult .= '<p><span class="btn btn-success btn-xs"><span class="glyphicon glyphicon-ok"></span></span> ';
                                        $textResult .= 'Création du la value du paramètre ' . $P->parameter . ' : ' . $value . '</p> ';
                                    } else {
                                        die('Erreur');
                                    }
                                }
                            }
                        } else {
                            $textResult .= count($res) . ' objets sélectionnés';
                        }
                    } else {
                        //echo 'nothing to do' ;
                    }
                }
            }
            return '<h2>Résultats </h2> ' . $textResult;
        } else {
            return 'Erreur Process';
        }
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('process');
