<?php

use Symfony\Component\HttpFoundation\Request;

$app->post('/action', function (Request $request) use ($app) {


    $T = new SysmaAction($request->request->get('sysma_action_id'), $app);
    $TT = new SysmaActionType($T->sysma_action_type_id, $app);

    if (
        hasAccess($app, $app['session']->get('user_id'), null, $TT->sysma_object_type_id, null, $T->sysma_action_type_id, $T->status, $T->organisation_id, 'lecteur')
        or (hasAccess($app, $app['session']->get('user_id'), null, $TT->sysma_object_type_id, null, $T->sysma_action_type_id, $T->status, $T->organisation_id, 'contributeur')
            and $T->modified_by_sysma_action == $app['session']->get('user_id'))
    ) {

        $T->loadActualInfosSysmaAction();
        $nbObjets = $T->countSysmaObjects();

        return $app['twig']->render('sysma-action/sysmaActionPage.twig', ['T' => $T, 'TT' => $TT, 'O' => new SysmaObject($request->request->get('sysma_object_id'), $app), 'sysma_object_id' => $request->request->get('sysma_object_id'), 'nbObjets' => $nbObjets]);
    } else {
        $erreur['message'] = 'Vous ne pouvez accéder à cette fiche sysma_action';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('action');



$app->post('/action/liste-objets', function (Request $request) use ($app) {

    $T = new SysmaAction($request->request->get('sysma_action_id'), $app);
    $TT = new SysmaActionType($T->sysma_action_type_id, $app);

    if (
        hasAccess($app, $app['session']->get('user_id'), null, $TT->sysma_object_type_id, null, $T->sysma_action_type_id, $T->status, $T->organisation_id, 'lecteur')
        or (hasAccess($app, $app['session']->get('user_id'), null, $TT->sysma_object_type_id, null, $T->sysma_action_type_id, $T->status, $T->organisation_id, 'contributeur')
            and $T->modified_by_sysma_action == $app['session']->get('user_id'))
    ) {
        $T->loadSysmaObjects();

        return $app['twig']->render('sysma-action/sysmaActionSysmaObjectsList.twig', ['T' => $T, 'sysma_object_id' => $request->request->get('sysma_object_id')]);
    } else {
        $erreur['message'] = 'Vous ne pouvez accéder à cette fiche sysma_action';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('action-objects-list');




$app->post('/action/ligne', function (Request $request) use ($app) {

    $T = new SysmaAction($request->request->get('sysma_action_id'), $app);
    $TT = new SysmaActionType($T->sysma_action_type_id, $app);


    if (
        hasAccess($app, $app['session']->get('user_id'), null, $TT->sysma_object_type_id, null, $T->sysma_action_type_id, $T->status, $T->organisation_id, 'lecteur')
        or (hasAccess($app, $app['session']->get('user_id'), null, $TT->sysma_object_type_id, null, $T->sysma_action_type_id, $T->status, $T->organisation_id, 'contributeur')
            and $T->modified_by_sysma_action == $app['session']->get('user_id'))
    ) {
        return $app['twig']->render('sysma-action/sysmaActionRow.twig', ['T' => $T, 'sysma_object_id' => $request->request->get('sysma_object_id')]);
    } else {
        $erreur['message'] = 'Vous ne pouvez accéder à cette fiche sysma_action';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('action-line');



$app->post('/action/update', function (Request $request) use ($app) {

    $T = new SysmaAction($request->request->get('sysma_action_id'), $app);
    $TT = new SysmaActionType($T->sysma_action_type_id, $app);


    if (
        hasAccess($app, $app['session']->get('user_id'), null, $TT->sysma_object_type_id, null, $T->sysma_action_type_id, $T->status, $T->organisation_id, 'gestionnaire') or (hasAccess($app, $app['session']->get('user_id'), null, $TT->sysma_object_type_id, null, $T->sysma_action_type_id, $T->status, $T->organisation_id, 'contributeur') and $T->modified_by_sysma_action == $app['session']->get('user_id'))
    ) {

        $T->loadSysmaObjects();
        $TO = new SysmaObjectType($TT->sysma_object_type_id, $app);

        $res['count'] = $res['surface'] = $res['perimetre'] = 0;
        foreach ($T->sysmaObjectsArray as $O) {

            $O->loadGeoInfos($TO->geometry_type);
            $res['count']++;
            !(empty($O->infosGeom['perimetre'])) ? $res['perimetre'] += $O->infosGeom['perimetre'] : null;
            !(empty($O->infosGeom['surface'])) ? $res['surface'] += $O->infosGeom['surface'] : null;
        }



        return $app['twig']->render('sysma-action/sysmaActionModificationForm.twig', ['T' => $T, 'linkedObjectsInfos' => $res, 'sysma_object_id' => $request->request->get('sysma_object_id'), 'tabStatuts' => statusList('sysma_action', $app), 'taborganisation' => organisationList($app)]);
    } else {
        $erreur['message'] = 'Vous ne pouvez accéder à cette fiche sysma_action';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('action-update');




$app->match('/action/update/commit', function (Request $request) use ($app) {

    $request->request->set('modified_by', $app['session']->get('user_id'));
    $request->request->set('modified_at', date('Y-m-d H:i:s'));
    $r = $request->request;
    $T = new SysmaAction($r->get('sysma_action_id'), $app);
    $TT = new SysmaActionType($T->sysma_action_type_id, $app);
    if (
        hasAccess($app, $app['session']->get('user_id'), null, $TT->sysma_object_type_id, null, $T->sysma_action_type_id, $T->status, $T->organisation_id, 'gestionnaire') or (hasAccess($app, $app['session']->get('user_id'), null, $TT->sysma_object_type_id, null, $T->sysma_action_type_id, $T->status, $T->organisation_id, 'contributeur') and $T->modified_by_sysma_action == $app['session']->get('user_id'))
    ) {

        $T->deleteContracts();

        foreach (contracts($app) as $c) {

            if ($r->get('contract' . $c['contract_id']) != null) {
                $T->addContract($c['contract']);
                $r->remove('contract' . $c['contract_id']);
            }
        }

        if ($r->get('cost_management') == null) {
            $r->set('unit_cost_thesaurus', null);
            $r->set('unit_cost_estimated_user', null);
            $r->set('unit_measure_estimated_user', null);
            $r->set('total_cost_estimated_user', null);
            $r->set('total_cost_final_user', null);
        }

        foreach ($r->all() as $key => $d) {
            if ($r->get($key) == '') {
                $r->set($key, null);
            }
        }

        $r->remove('cost_management');

        return $T->update($r);
    } else {
        $erreur['message'] = 'Vous ne pouvez accéder à cette fiche sysma_action';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('action-update-commit');





$app->match('/action/create', function (Request $request) use ($app) {

    $TO = new SysmaObjectType($request->request->get('sysma_object_type_id'), $app);
    $O = new SysmaObject($request->request->get('sysma_object_id'), $app);
    $O->loadGeoInfos($O->geometry_type);
    if (
        hasAccessSysmaActions($app, $app['session']->get('user_id'), $TO->sysma_object_type_id, $O->organisation_id, 'gestionnaire') or (hasAccessSysmaActions($app, $app['session']->get('user_id'), $TO->sysma_object_type_id, $O->organisation_id, 'contributeur'))
    ) {
        $TO->loadSysmaActionTypes();
        return $app['twig']->render('sysma-action/sysmaActionCreationForm.twig', ['index' => dataIndex($app['pdo']), 'sysma_object_id' => $request->request->get('sysma_object_id'), 'TO' => $TO, 'O' => $O, 'tabStatuts' => statusList('sysma_action', $app), 'taborganisation' => organisationList($app), 'tabcontracts' => contracts($app)]);
    } else {
        $erreur['message'] = 'Vous ne pouvez accéder à cette fiche sysma_action';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('action-create');


$app->match('/action/create/commit', function (Request $request) use ($app) {

    $request->request->set('modified_by', $app['session']->get('user_id'));
    $request->request->set('created_by', $app['session']->get('user_id'));


    $r = $request->request;
    $r2 = $r->all();

    $TT = new SysmaActionType($r->get('sysma_action_type_id'), $app);

    if (
        hasAccess($app, $app['session']->get('user_id'), null, $TT->sysma_object_type_id, null, $r->get('sysma_action_type_id'), $r->get('status'), $r->get('organisation_id'), 'gestionnaire') or hasAccess($app, $app['session']->get('user_id'), null, $TT->sysma_object_type_id, null, $r->get('sysma_action_type_id'), $r->get('status'), $r->get('organisation_id'), 'contributeur')
    ) {
        $T = new SysmaAction(null, $app);
        // suppression des champs contracts
        foreach (contracts($app) as $c) {
            if ($r->get('contract' . $c['contract_id']) != null) {
                $r->remove('contract' . $c['contract_id']);
            }
        }

        if ($r->get('cost_management') != 1) {
            $r->remove('unit_cost_thesaurus');
            $r->remove('unit_cost_estimated_user');
            $r->remove('unit_measure_estimated_user');
            $r->remove('total_cost_estimated_user');
            $r->remove('total_cost_final_user');
        }

        foreach ($r->all() as $key => $d) {
            if ($r->get($key) == '') {
                $r->set($key, null);
            }
        }

        $r->remove('cost_management');

        $res = $T->create($r);

        $res_decoded = json_decode($res);
        if ($res_decoded->status == 'error') {
            return $res;
        } else {

            // nouvelle fiche sysma_action, update des contracts
            $T2 = new SysmaAction($r->get('sysma_action_id'), $app);
            foreach (contracts($app) as $c) {

                if (key_exists('contract' . $c['contract_id'], $r2)) {
                    $T2->addContract($c['contract']);
                }
            }

            $O = new SysmaObject($r->get('sysma_object_id'), $app);
            $O->linkSysmaActionSheet($r->get('sysma_action_id'), $app['session']->get('user_id'));
            return $res;
        }
    } else {
        $erreur['message'] = 'Vous ne pouvez accéder à cette fiche sysma_action';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('action-create-commit');



$app->post('/action/delete', function (Request $request) use ($app) {

    $T = new SysmaAction($request->request->get('sysma_action_id'), $app);
    $TT = new SysmaActionType($T->sysma_action_type_id, $app);
    if (
        hasAccess($app, $app['session']->get('user_id'), null, $TT->sysma_object_type_id, null, $T->sysma_action_type_id, $T->status, $T->organisation_id, 'gestionnaire')
        or (hasAccess($app, $app['session']->get('user_id'), null, $TT->sysma_object_type_id, null, $T->sysma_action_type_id, $T->status, $T->organisation_id, 'contributeur')
            and $T->modified_by_sysma_action == $app['session']->get('user_id')
            and $app['session']->get('user_id') != ANONYMID)
    ) {


        $errors = 0;
        $T->loadSysmaObjects();
        foreach ($T->sysmaObjectsArray as $O) {

            if ($O->unlinkSysmaActionSheet($request->request->get('sysma_action_id'))) {
            } else {
                $errors++;
            }
        }

        if ($errors == 0) {
            if ($T->delete()) {
                return json_encode(['status' => 'success']);
            } else {
                return json_encode(['status' => 'error']);
            }
        } else {
            return json_encode(['status' => 'error']);
        }
    } else {
        $erreur['message'] = 'Vous ne pouvez pas supprimer cette fiche sysma_action';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('action-delete');
