<?php

/*
 * Work in progress
 */


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

$app->match('/atlas/{table}', function (Request $request, $table) use ($app) {

    $Atlas = new Atlas($table, $app);
    $Atlas->loadPlanches();
    $html = null;
    $compt = 0;
    $lastIdTroncon = null;



    // pre bouclage pour constituer la liste des planches - travaux - sous-planches
    // pour chaque planche
    foreach ($Atlas->tabPlanches as $planche_num => $data) {

        $sysma_datas = json_decode($data['sysma_datas']);

        if (is_object($sysma_datas) and count($sysma_datas->sysma_action_type) > 0) {

            // pour chaque types de travaux 

            $t = null;
            foreach ($sysma_datas->sysma_action_type as $sysma_action) {

                // pour chaque fiche travaux
                $comptW = 0;

                $tabGOs = array();
                $tabWs = array();

                foreach ($sysma_action->sysma_action as $travail) {

                    //   echo $travail->sysma_action_id;
                    //$T = new SysmaAction($travail->sysma_action_id, $app);
                    // pour chaque objet de la fiche travaux


                    foreach ($travail->sysma_object as $o) {
                        if ($o->is_totaly_covered == true) {

                            $O = new SysmaObject($o->sysma_object_id, $app);
                            $O->loadActualInfosSysmaObjectWithIndex();


                            // ordered_by par id troncon dans le cas specifique des seq t2bv
                            if ($sysma_action->sysma_object_type_id == 158507) {

                                $idTroncon = $O->dataIndex['id_tronc']->value;
                                $idTroncon = (substr($idTroncon, strpos($idTroncon, '[o') + 2, strlen($idTroncon) - strpos($idTroncon, ']') - 2));
                                $regroup = alias($idTroncon);

                                $uniq_id = $planche_num . '-' . $sysma_action->sysma_object_type_id . '-' . $sysma_action->sysma_action_type_id . '-' . $regroup;

                                $tabRegroup[$uniq_id]['planche_num'] = $planche_num;
                                $tabRegroup[$uniq_id]['sysma_object_type_id'] = $sysma_action->sysma_object_type_id;
                                $tabRegroup[$uniq_id]['sysma_action_type_id'] = $sysma_action->sysma_action_type_id;
                                $tabRegroup[$uniq_id]['GOs'][] = $o->sysma_object_id;
                                $tabRegroup[$uniq_id]['Ws'][] = $travail->sysma_action_id;
                            } else {
                                $uniq_id = $planche_num . '-' . $sysma_action->sysma_object_type_id . '-' . $sysma_action->sysma_action_type_id . '-0';
                                $tabRegroup[$uniq_id]['planche_num'] = $planche_num;
                                $tabRegroup[$uniq_id]['sysma_object_type_id'] = $sysma_action->sysma_object_type_id;
                                $tabRegroup[$uniq_id]['sysma_action_type_id'] = $sysma_action->sysma_action_type_id;
                                $tabRegroup[$uniq_id]['GOs'][] = $o->sysma_object_id;
                                $tabRegroup[$uniq_id]['Ws'][] = $travail->sysma_action_id;
                            }
                        }
                    }
                }
            }
        }
    }



    foreach ($tabRegroup as $regroup => $tab) {



        $t['planche_num'] = $tabRegroup[$regroup]['planche_num'];
        $t['sysma_object_type_id'] = $tabRegroup[$regroup]['sysma_object_type_id'];
        $t['sysma_action_type_id'] = $tabRegroup[$regroup]['sysma_action_type_id'];
        $t['Ws'] = $tabRegroup[$regroup]['Ws'];
        $t['GOs'] = $tabRegroup[$regroup]['GOs'];
        $t['regroup'] = $regroup;
        $tabPlanches[] = $t;
    }



    //var_dump($tabPlanches);


    $html = null;
    $res_html = null;

    foreach ($tabPlanches as $p) {

        $tabT = array();
        $tabGO = array();
        $tabGOIds = array();
        $tabTroncons = array();
        $planchenum = $p['planche_num'];


        $TT = new SysmaActionType($p['sysma_action_type_id'], $app);
        $TyO = new SysmaObjectType($p['sysma_object_type_id'], $app);
        foreach ($p['Ws'] as $idt) {
            $T = new SysmaAction($idt, $app);
            $tabT[] = $T;
        }
        foreach ($p['GOs'] as $id) {


            $O = new SysmaObject($id, $app);
            $O->loadActualInfosSysmaObjectWithIndex();
            $O->loadSysmaObjectInfos();
            $O->loadGeoInfos($TyO->geometry_type);
            $O->loadMainPhoto();
            $tabGO[] = $O;
            $tabGOIds[] = $id;

            // cas spécifique des séquences de têtes de bv

            if ($TyO->alias == 't2bv_seq') {
                // tronçon
                $idTroncon = $O->dataIndex['id_tronc']->value;
                $idTroncon = (substr($idTroncon, strpos($idTroncon, '[o') + 2, strlen($idTroncon) - strpos($idTroncon, ']') - 2));

                $Troncon = new SysmaObject($idTroncon, $app);
                $Troncon->loadActualInfosSysmaObjectWithIndex();
                $tabTroncons[$id] = $Troncon;
            }
        }


        if (count($tabGOIds) > 0) {


            $geom = loadMultiGeom($app, $tabGOIds);
            $com = findIntersectingObjects(REF_PG_SCHEMA_TABLE_CITIES, $geom['geom'], array('tampon' => 20), $app);
            $me = findIntersectingObjects(REF_PG_SCHEMA_TABLE_MASSEEAU, $geom['geom'], array('tampon' => 20, 'sysma_object_id' => REF_PG_SCHEMA_TABLE_MASSEEAU_GID, 'sysma_object' => REF_PG_SCHEMA_TABLE_MASSEEAU_NAME), $app);

            //    var_dump($me);


            if (file_exists(__DIR__ . '/../../views/atlas/atlasWT-' . $TyO->alias . '-' . $TT->alias . '.twig')) {
                $html .= $app['twig']->render('atlas/atlasWT-' . $TyO->alias . '-' . $TT->alias . '.twig', ['Planche' => ['planche_num' => $planchenum, 'regroup' => $p['regroup']], 'tabGO' => $tabGO, 'tabTroncons' => $tabTroncons, 'tabT' => $tabT, 'tabC' => $com, 'tabME' => $me, 'TT' => $TT, 'TyO' => $TyO, 'geom' => $geom, 'maps' => $request->query->get('maps')]);
            } else {
                $html .= $app['twig']->render('atlas/atlasWT-default.twig', ['Planche' => ['planche_num' => $planchenum, 'regroup' => $p['regroup']], 'tabGO' => $tabGO, 'tabTroncons' => $tabTroncons, 'tabT' => $tabT, 'tabC' => $com, 'tabME' => $me, 'TT' => $TT, 'TyO' => $TyO, 'geom' => $geom, 'maps' => $request->query->get('maps')]);
            }
        }

        // echo $html;
    }
    /*

      // pour chaque planche
      foreach ($Atlas->tabPlanches as $planche_num => $data) {

      //  echo 'Planche ' . $planche_num . '<br>';

      $sysma_datas = json_decode($data['sysma_datas']);
      //  var_dump($sysma_datas) ;
      // pour le type de travaux
      if (is_object($sysma_datas) and count($sysma_datas->sysma_action_type) > 0) {
      foreach ($sysma_datas->sysma_action_type as $sysma_action) {


      //  $html = null;
      //  $res_html = null;
      $tabT = array();
      $tabGO = array();
      $tabGOIds = array();
      $tabTroncons = array();

      $TT = new SysmaActionType($sysma_action->sysma_action_type_id, $app);
      $TyO = new SysmaObjectType($TT->sysma_object_type_id, $app);
      //  var_dump($travaux);
      foreach ($sysma_action->sysma_action as $travail) {

      //  var_dump($travail->sysma_action_id) ;

      $T = new SysmaAction($travail->sysma_action_id, $app);
      if ($T->dateDebut == null)
      die('ID TRAVAUX ' . $travail->sysma_action_id . ' n existe pas ');

      $tabT[] = $T;

      foreach ($travail->sysma_object as $o) {

      if ($o->is_totaly_covered == true) {
      $O = new SysmaObject($o->sysma_object_id, $app);

      $O->loadActualInfosSysmaObjectWithIndex();
      $O->loadSysmaObjectInfos();
      $O->loadGeoInfos($TyO->geometry_type);
      $O->loadMainPhoto();
      $tabGO[] = $O;
      $tabGOIds[] = $o->sysma_object_id;

      // cas spécifique des séquences de têtes de bv

      if ($TyO->alias == 't2bv_seq') {
      // tronçon
      $idTroncon = $O->dataIndex['id_tronc']->value;
      $idTroncon = (substr($idTroncon, strpos($idTroncon, '[o') + 2, strlen($idTroncon) - strpos($idTroncon, ']') - 2));

      $Troncon = new SysmaObject($idTroncon, $app);
      $Troncon->loadActualInfosSysmaObjectWithIndex();
      $tabTroncons[$o->sysma_object_id] = $Troncon;
      }
      }
      }
      }


      if (count($tabGOIds) > 0) {


      $geom = loadMultiGeom($app, $tabGOIds);
      $com = findIntersectingObjects(REF_PG_SCHEMA_TABLE_CITIES, $geom['geom'], array('tampon' => 20), $app);
      $me = findIntersectingObjects(REF_PG_SCHEMA_TABLE_MASSEEAU, $geom['geom'], array('tampon' => 20, 'sysma_object_id' => REF_PG_SCHEMA_TABLE_MASSEEAU_GID, 'sysma_object' => REF_PG_SCHEMA_TABLE_MASSEEAU_NAME), $app);

      //    var_dump($me);


      if (file_exists(__DIR__ . '/../../views/atlas/atlasWT-' . $TyO->alias . '-' . $TT->alias . '.twig')) {
      $html .= $app['twig']->render('atlas/atlasWT-' . $TyO->alias . '-' . $TT->alias . '.twig', ['Planche' => ['planche_num' => $planche_num], 'tabGO' => $tabGO, 'tabTroncons' => $tabTroncons, 'tabT' => $tabT, 'tabC' => $com, 'tabME' => $me, 'TT' => $TT, 'TyO' => $TyO, 'geom' => $geom, 'maps' => $request->query->get('maps')]);
      } else {
      $html .= $app['twig']->render('atlas/atlasWT-default.twig', ['Planche' => ['planche_num' => $planche_num], 'tabGO' => $tabGO, 'tabTroncons' => $tabTroncons, 'tabT' => $tabT, 'tabC' => $com, 'tabME' => $me, 'TT' => $TT, 'TyO' => $TyO, 'geom' => $geom, 'maps' => $request->query->get('maps')]);
      }


      // echo $html;
      } else {

      }

      }
      }
      }
     */
    // get the layout content and merge it with the addon content (usefull for multi object calls)
    $layout = $app['twig']->render('atlas/atlasLayout.twig');
    $res_html = str_replace('<!-- block body -->', $html, $layout);

    // return $res_html;

    //  $file = ROOT_URI . "public/upload/atlas_".$table.".html";
    $file = "/var/www/sysma/sysma_prod/sysma_prod/public/upload/atlas_" . $table . ".html";

    file_put_contents($file, $res_html);
    return 'OK';
})->bind('atlas');
