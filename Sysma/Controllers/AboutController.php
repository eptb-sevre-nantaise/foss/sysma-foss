<?php

$app->match('/about', function () use ($app) {
    return $app['twig']->render('about/about.twig');
})->bind('about');
