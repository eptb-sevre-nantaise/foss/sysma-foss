<?php

use Symfony\Component\HttpFoundation\Request;

$app->get('/extensions', function (Request $request) use ($app) {


    if (hasAccess($app, $app['session']->get('user_id'), null, null, null, null, null, null, 'gestionnaire')) {

        foreach ($GLOBALS['ADDONS'] as $key => $P) {

            $TyO = new SysmaObjectType($key, $app);
            $tabTyO[$key] = $TyO;
        }

        $tabS = array_merge(['false' => 'Ne pas limiter à un secteur'], objectsInTableList($app, array('table' => REF_PG_SCHEMA_TABLE_MASSEEAU, 'sysma_object_id' => REF_PG_SCHEMA_TABLE_MASSEEAU_GID, 'sysma_object' => REF_PG_SCHEMA_TABLE_MASSEEAU_NAME, 'geom' => REF_PG_SCHEMA_TABLE_MASSEEAU_GEOM)));

        return $app['twig']->render('addons/addonsPage.twig', ['tabTyO' => $tabTyO, 'tabAO' => $GLOBALS['ADDONS'], 'tabS' => $tabS]);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('addons');


$app->post('/extension/load-geoobjects', function (Request $request) use ($app) {


    $r = $request->request;
    $secteur = explode('.', $r->get('secteur'));
    $id_secteur = $secteur[5];
    $addon = explode('|', $r->get('addon'));
    $id_addon = $addon[1];
    $sysma_object_type_id = $addon[0];
    // var_dump($sysma_object_type_id) ;


    if (hasAccess($app, $app['session']->get('user_id'), null, $sysma_object_type_id, null, null, null, null, 'gestionnaire')) {

        $Secteur = loadObject($app, ['table' => REF_PG_SCHEMA_TABLE_MASSEEAU, 'geom' => REF_PG_SCHEMA_TABLE_MASSEEAU_GEOM, 'sysma_object' => REF_PG_SCHEMA_TABLE_MASSEEAU_NAME, 'sysma_object_id' => REF_PG_SCHEMA_TABLE_MASSEEAU_GID, 'id' => $id_secteur]);


        $tabO = loadSysmaSysmaObjectsInGeom($app, $Secteur->geom, ['filter' => ' sysma_object_type_id = ' . $sysma_object_type_id . ' order by sysma_object asc']);

        if (count($tabO) == 0) {
            return 'Pas d\'objet de ce type sur ce secteur';
        }

        $tabIdObjects = array();

        foreach ($tabO as $O) {
            $tabIdObjects[] = $O['sysma_object_id'];
        }


        $AO = $GLOBALS['ADDONS'][$sysma_object_type_id][$id_addon];

        return '<a class="btn btn-primary" target="_blank" href="/extension/' . $AO['url'] . '/' . implode('|', $tabIdObjects) . '"><span class="' . $AO['icon'] . '"></span> ' . $AO['title'] . '</a>';
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('addon-load-geoobjects');



$app->get('/extension/{id}/{sysma_object_id}', function (Request $request, $id, $sysma_object_id) use ($app) {


    $tabO = explode('|', $sysma_object_id);
    $html = null;

    foreach ((array) $tabO as $sysma_object_id) {
        $O = new SysmaObject($sysma_object_id, $app);


        // if the report is declared
        if (isset($GLOBALS['ADDONS'][$O->sysma_object_type_id][$id])) {

            $r = $GLOBALS['ADDONS'][$O->sysma_object_type_id][$id];
            $TyO = new SysmaObjectType($O->sysma_object_type_id, $app);
            $O->loadActualInfosSysmaObjectWithIndex();
            $O->loadSysmaObjectInfos();
            $geom = $O->LoadGeom();
            $O->loadGeoInfos($TyO->geometry_type);
            $O->loadPhotoObjects();
            $O->loadSysmaActions();
            foreach ($O->actionsArray as $A) {
                $A->loadActualInfosSysmaAction();
            }

            $com = findIntersectingObjects(REF_PG_SCHEMA_TABLE_CITIES, $geom, array('tampon' => 20, 'id' => REF_PG_SCHEMA_TABLE_CITIES_GID, 'object' => REF_PG_SCHEMA_TABLE_CITIES_NAME), $app);
            $me = findIntersectingObjects(REF_PG_SCHEMA_TABLE_MASSEEAU, $geom, array('tampon' => 20, 'id' => REF_PG_SCHEMA_TABLE_MASSEEAU_GID, 'object' => REF_PG_SCHEMA_TABLE_MASSEEAU_NAME), $app);

            $data['app'] = $app;
            $data['sysma_object_id'] = $sysma_object_id;
            $data['calculation_date'] = date('Y-m-d');
            $data['no_comment'] = true;
            $tab = array();

            foreach ($r['processes'] as $key => $process) {
                include(ROOT_URI . 'Processes' . $process['path']);
                if ($tabRes = call_user_func_array($process['id'], $data)) {
                    $result['status_exec'] = 'success';
                } else {
                    $result['status_exec'] = 'failure';
                }

                $result['res'] = $tabRes;
                $tab[$key] = $result;
            }


            // load the raw content of the addon
            $html .= $app['twig']->render($r['twig_file_content'], ['O' => $O, 'tabC' => $com, 'tabME' => $me, 'tabProcesses' => $tab, 'maps' => $request->query->get('maps'), 'BASE_LAYERS' => loadBaseLayers($app, ($app['session']->get('user_id') != ANONYMID)), 'OTHER_BASE_LAYERS' => loadOtherBaseLayers($app, ($app['session']->get('user_id') != ANONYMID))]);
        } else {
            return 'erreur';
        }
    }

    // get the layout content and merge it with the addon content (usefull for multi object calls)
    $layout = $app['twig']->render('addons/addonsLayout.twig');
    $res_html = str_replace('<!-- block body -->', $html, $layout);
    return $res_html;
})->bind('addon');
