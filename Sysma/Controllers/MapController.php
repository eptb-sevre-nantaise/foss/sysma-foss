<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

$app->get('/carte', function (Request $request) use ($app) {   
    return $app['twig']->render('map/map.twig', ['U' => loadUserFromSession($app, $request), 'mapWidth' => $app['session']->get('map_width')]) ;
})->bind('map');

$app->get('/carte/print', function (Request $request) use ($app) {
    if ($app['session']->get('user_id') != ANONYMID) {
        return $app['twig']->render('map/mapPrint.twig', ['U' => loadUserFromSession($app, $request), 'mapWidth' => $app['session']->get('map_width')]);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('map-print');

$app->match('/set-map-width', function (Request $request) use ($app) {

    $app['session']->set('map_width', $request->request->get('width'));
    return ' ';
})->bind('set-map-width');


$app->post('/update-map-settings', function (Request $request) use ($app) {

    if ($app['session']->get('user_id') != ANONYMID) {

        //echo '<script>console.log("LOP '.$app['session']->get('load-on-pan').'");</script>' ;

        updateDefaultCoords($app, $app['session']->get('user_id'), str_replace(' ', '', $request->request->get('centre')), $request->request->get('zoom'));
        if ($app['session']->get('load-on-pan') == 'true' or $app['session']->get('load-on-pan') == null) {
            return reloadGeoLayers($app, str_replace(' ', '', $request->request->get('centre')), intval($request->request->get('zoom')));
        } else {
            return '';
        }
    } else {
        return '';
    }
})->bind('update-map-settings');


$app->post('/disable-loadonpan', function (Request $request) use ($app) {

    if ($app['session']->get('user_id') != ANONYMID) {
        $app['session']->set('load-on-pan', 'false');
    }
    return '';
})->bind('disable-loadonpan');

$app->post('/enable-loadonpan', function (Request $request) use ($app) {

    if ($app['session']->get('user_id') != ANONYMID) {
        $app['session']->set('load-on-pan', 'true');
    }
    return '';
})->bind('enable-loadonpan');

$app->post('/update-baselayer', function (Request $request) use ($app) {
    if ($app['session']->get('user_id') != ANONYMID) {
        return updateBaseLayer($app, $app['session']->get('user_id'), $request->request->get('baselayer'));
    }
})->bind('update-baselayer');




$app->post('/update-other-baselayer', function (Request $request) use ($app) {

    if ($app['session']->get('user_id') != ANONYMID) {

        if ($request->request->get('action') === 'add') {
            return addOtherBaseLayer($app, $app['session']->get('user_id'), $request->request->get('otherbaselayer'));
        }
        if ($request->request->get('action') === 'remove') {
            return removeOtherBaseLayer($app, $app['session']->get('user_id'), $request->request->get('otherbaselayer'));
        }
    }
})->bind('update-other-baselayer');



$app->match('/coords', function (Request $request) use ($app) {

    $coords = substr($request->request->get('mouse'), 7, -1);
    $tabC = explode(',', $coords);
    $qry = $app['pdo']->prepare('select ST_AsText(ST_Transform(ST_setSRID(ST_Point(' . $tabC[1] . ',' . $tabC[0] . '),4326),2154)) as geom_2154, ST_AsText(ST_Transform(ST_setSRID(ST_Point(' . $tabC[1] . ',' . $tabC[0] . '),4326),27572)) as geom_27572, ST_AsText(ST_Transform(ST_setSRID(ST_Point(' . $tabC[1] . ',' . $tabC[0] . '),4326),3947)) as geom_3947 ');
    $qry->execute();
    $res = $qry->fetchObject();
    return $app['twig']->render('map/infoCoord.twig', ['coord_4326' => $tabC, 'coord_2154' => explode(' ', substr($res->geom_2154, 6, -1)), 'coord_27572' => explode(' ', substr($res->geom_27572, 6, -1)), 'coord_3947' => explode(' ', substr($res->geom_3947, 6, -1)),]);
})->bind('coords');












$app->get('/carte/photo/{photo}', function (Request $request, $photo) use ($app) {

    if ($app['session']->get('user_id') != ANONYMID and PHOTO_PWG_MODE === 'on') {

        $P = new PiwigoPhoto($photo, $app);

        if (isset($P->id)) {

            $U = loadUserFromSession($app, $request);
            return $app['twig']->render('map/map.twig', ['U' => $U, 'mapWidth' => $app['session']->get('map_width'), 'photo' => $photo]);
        } else {
            $app['session']->getFlashBag()->add('message', array('text' => 'La photo ' . $photo . ' n\'existe pas', 'status' => 'danger'));
            return $app->redirect($app['url_generator']->generate('map'));
        }
    } else {
        $erreur['message'] = 'erreur';
        return $app['twig']->render('geojson/error.twig', ['erreur' => $erreur]);
    }
})->bind('photo-carte');






$app->get('/carte/objet/{sysma_object_id}', function (Request $request, $sysma_object_id) use ($app) {

    if ($app['session']->get('user') === null) {
        loadUserFromSession($app, $request);
    }

    $O = new SysmaObject($sysma_object_id, $app);

    if (isset($O->id)) {


        if (hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'lecteur') or (hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'contributeur') and $app['session']->get('user_id') == $O->modified_by)) {

            $U = loadUserFromSession($app, $request);
            return $app['twig']->render('map/map.twig', ['U' => $U, 'mapWidth' => $app['session']->get('map_width'), 'sysma_object_id' => $sysma_object_id]);
        } else {
            $erreur['message'] = 'Vous ne pouvez pas accéder à l\'objet ' . $sysma_object_id;
            return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
        }
    } else {
        $app['session']->getFlashBag()->add('message', array('text' => 'L\'objet ' . $sysma_object_id . ' n\'existe pas', 'status' => 'danger'));
        return $app->redirect($app['url_generator']->generate('map'));
    }
})->bind('objet-carte');



$app->get('/carte/objet/{sysma_object_id}/sysma_action/{sysma_action_id}', function ($sysma_object_id, $sysma_action_id) use ($app) {

    $O = new SysmaObject($sysma_object_id, $app);
    $T = new SysmaAction($sysma_action_id, $app);

    if (hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, $T->sysma_action_type_id, $T->status, $T->organisation_id, 'lecteur') or (hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, $T->sysma_action_type_id, $T->status, $T->organisation_id, 'contributeur') and $app['session']->get('user_id') == $T->modified_by_sysma_action)) {


        if (isset($O->id)) {

            $U = new User($app['session']->get('user_id'), $app);
            $O->loadGeoInfos($O->geometry_type);
            $centre = $O->infosGeom['centre'];

            isset($app['session']->get('U')->base_layer) ? $baselayer = $app['session']->get('U')->base_layer : $baselayer = BASELAYER;

            return $app['twig']->render('map/map.twig', ['U' => $U, 'mapWidth' => $U->map_width, 'centre' => $centre, 'zoom' => 17, 'baselayer' => $baselayer, 'otherlayers' => $app['session']->get('U')->layersRef($app), 'sysma_object_id' => $sysma_object_id, 'sysma_action_id' => $sysma_action_id]);
        } else {
            $app['session']->getFlashBag()->add('message', array('text' => 'L\'objet ' . $sysma_object_id . ' n\'existe pas', 'status' => 'danger'));
            return $app['twig']->render('map/map.twig', ['centre' => CENTERDEFAULT, 'zoom' => ZOOMDEFAULT, 'baselayer' => BASELAYER]);
        }
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à l\'objet ' . $sysma_object_id;
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
});




$app->get('/js/map/sysma.js', function (Request $request) use ($app) {

    $U = new User($app['session']->get('user_id'), $app);


    isset($U->default_center) ? $centre = $U->default_center : $centre = CENTERDEFAULT;
    isset($U->default_zoom) ? $zoom = $U->default_zoom : $zoom = ZOOMDEFAULT;
    isset($U->base_layer) ? $baselayer = $U->base_layer : $baselayer = BASELAYER;
    $otherlayers = $U->layersRef();

    !empty($request->query->get('sysma_object_id')) ? $sysma_object_id = $request->query->get('sysma_object_id') : $sysma_object_id = null;
    !empty($request->query->get('q')) ? $q = $request->query->get('q') : $q = null;
    !empty($request->query->get('photo')) ? $photo = $request->query->get('photo') : $photo = null;
    !empty($request->query->get('print')) ? $print = $request->query->get('print') : $print = null;


    if ($sysma_object_id != null) {

        $O = new SysmaObject($sysma_object_id, $app);
        $infoGeom = $O->loadGeoInfos($O->geometry_type, $app);
        $centre = $O->infosGeom['centre'];
        $zoom = 17;
    }

    if ($photo != null) {

        $P = new PiwigoPhoto($photo, $app);
        $centre = $P->center;
        $zoom = 17;
    }


    $response = new Response();
    $response->headers->set('Content-Type', 'text/javascript');
    $response->setContent($app['twig']->render('map/mapSysma.js.twig', ['U' => $U, 'sysma_object_id' => $sysma_object_id, 'photo' => $photo, 'print' => $print, 'q' => $q, 'mapWidth' => @$U->map_width, 'centre' => $centre, 'snapDistance' => @$U->snap_options->distance, 'zoom' => $zoom, 'baselayer' => $baselayer, 'otherlayers' => $otherlayers, 'BASE_LAYERS' => loadBaseLayers($app, ($app['session']->get('user_id') != ANONYMID)), 'OTHER_BASE_LAYERS' => loadOtherBaseLayers($app, ($app['session']->get('user_id') != ANONYMID))]));
    return $response;
});


$app->get('/js/map/utils.js', function (Request $request) use ($app) {

    $response = new Response();
    $response->headers->set('Content-Type', 'text/javascript');
    $response->setContent($app['twig']->render('map/mapFunctions.js.twig', ['mapWidth' => $request->query->get('map-width'), 'centre' => $request->query->get('centre')]));
    return $response;
});
