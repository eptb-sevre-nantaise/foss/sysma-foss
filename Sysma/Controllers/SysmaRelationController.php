<?php

use Symfony\Component\HttpFoundation\Request;

$app->post('/relation', function (Request $request) use ($app) {

    $r = $request->request;
    $O = new SysmaObject($request->request->get('sysma_object_id'), $app);
    $O->loadSysmaRelations();


    if (
        hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'gestionnaire') or (hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'contributeur') and $O->modified_by == $app['session']->get('user_id'))
    ) {

        return $app['twig']->render('sysma-relation/sysmaRelationPage.twig', ['O' => $O, 'sysma_object_id' => $request->request->get('sysma_object_id'), 'sysma_relation_id' => $r->get('sysma_relation_id')]);
    } else {
        $erreur['message'] = 'Vous ne pouvez accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('relation');


// CREER
$app->match('/relation/create', function (Request $request) use ($app) {

    $TO = new SysmaObjectType($request->request->get('sysma_object_type_id'), $app);
    $O = new SysmaObject($request->request->get('sysma_object_id'), $app);

    if (
        hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'gestionnaire') or (hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'contributeur') and $O->modified_by == $app['session']->get('user_id'))
    ) {

        $TO->loadSysmaRelationTypes();
        return $app['twig']->render('sysma-relation/sysmaRelationCreationForm.twig', ['index' => dataIndex($app['pdo']), 'sysma_object_id' => $request->request->get('sysma_object_id'), 'TO' => $TO, 'O' => $O, 'tabO' => objectsList($app, $TO->sysma_object_type_id), 'tabStatuts' => statusList('sysma_relation', $app), 'taborganisation' => organisationList($app), 'tabcontracts' => contracts($app)]);
    } else {
        $erreur['message'] = 'Vous ne pouvez accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('relation-modify-create');


$app->match('/relation/update', function (Request $request) use ($app) {

    $r = $request->request;

    $RL = new SysmaRelation($r->get('sysma_relation_id'), $app);
    $O = new SysmaObject($r->get('sysma_object_id'), $app);

    if (
        hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'gestionnaire') or (hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'contributeur') and $O->modified_by == $app['session']->get('user_id'))
    ) {


        return $app['twig']->render('sysma-relation/sysmaRelationModificationForm.twig', ['RL' => $RL, 'sysma_object_id' => $request->request->get('sysma_object_id'), 'tabStatuts' => statusList('sysma_relation', $app), 'taborganisation' => organisationList($app), 'tabcontracts' => contracts($app)]);
    } else {
        $erreur['message'] = 'Vous ne pouvez accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('relation-modify-update');


$app->match('/relation/line', function (Request $request) use ($app) {



    count($request->request->all()) > 0 ? $r = $request->request : $r = $request->query;


    $O = new SysmaObject($r->get('sysma_object_id'), $app);
    $O->loadSysmaRelation($r->get('sysma_relation_id'));


    if (
        hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'gestionnaire') or (hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'contributeur') and $O->modified_by == $app['session']->get('user_id'))
    ) {

        return $app['twig']->render('sysma-relation/sysmaRelationRow.twig', ['R' => $O->relationsArray[$r->get('sysma_relation_id')], 'sysma_object_id' => $r->get('sysma_object_id')]);
    } else {
        $erreur['message'] = 'Vous ne pouvez accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('relation-line');



$app->match('/relation/create/commit', function (Request $request) use ($app) {

    $request->request->set('modified_by', $app['session']->get('user_id'));
    $request->request->set('created_by', $app['session']->get('user_id'));

    $r = $request->request;
    $rl = explode('|', $r->get('sysma_relation_type_id'));


    $RLT = new SysmaRelationType($rl[0], $app);
    $TO = new SysmaObjectType($request->request->get('sysma_object_type_id'), $app);
    $O = new SysmaObject($request->request->get('sysma_object_id'), $app);

    if (
        hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'gestionnaire') or (hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'contributeur') and $O->modified_by == $app['session']->get('user_id'))
    ) {

        $R = new SysmaRelation(null, $app);
        $r2 = clone ($r);
        // suppression des champs contracts
        foreach (contracts($app) as $c) {
            if ($r2->get('contract' . $c['contract_id']) != null) {
                $r2->remove('contract' . $c['contract_id']);
            }
        }


        $data['sysma_relation_type_id'] = $rl[0];
        $data['sysma_relation'] = $r->get('sysma_relation');
        $data['sysma_relation_id'] = $r->get('sysma_relation_id');
        $data['start_date'] = $r->get('start_date');
        $data['end_date'] = $r->get('end_date');
        $data['status'] = $r->get('status');
        $data['organisation_id'] = $r->get('organisation_id');
        $data['created_by'] = $app['session']->get('user_id');
        $data['modified_by'] = $app['session']->get('user_id');

        $res = $R->create($data);
        $res_decoded = json_decode($res);
        if ($res_decoded->status == 'error') {
            return $res;
        } else {

            // new relation, contracts insert
            $T2 = new SysmaRelation($r->get('sysma_relation_id'), $app);
            foreach (contracts($app) as $c) {
                if (key_exists('contract' . $c['contract_id'], $r->all())) {
                    $T2->addContract($c['contract']);
                }
            }

            $rl[2] == 'parent' ? $parent_child = 'child' : $parent_child = 'parent';

            // add "parent" (left object in the form relation)
            $T2->addSysmaRelationLink($r->get('sysma_object_id'), $rl[2], $app['session']->get('user_id'));

            // uniq relation, only one linked object
            if ($RLT->isAUniqRelation($rl[2])) {

                $T2->addSysmaRelationLink($r->get('sysma_object_id_link'), $parent_child, $app['session']->get('user_id'));
            } else {

                // many parents or children
                $Objects = $RLT->loadSysmaObjects($rl[3]);
                foreach ($Objects as $O) {
                    if ($r->get('sysma_object_id_link_' . $O->sysma_object_id) == 1) {
                        $T2->addSysmaRelationLink($O->sysma_object_id, $parent_child, $app['session']->get('user_id'));
                    }
                }
            }
            return $res;
        }
    } else {
        $erreur['message'] = 'Vous ne pouvez accéder à cette fiche sysma_action';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('relation-modify-create-commit');






$app->match('/relation/update/commit', function (Request $request) use ($app) {

    $request->request->set('modified_by', $app['session']->get('user_id'));
    $request->request->set('created_by', $app['session']->get('user_id'));

    $r = $request->request;


    $RL = new SysmaRelation($r->get('sysma_relation_id'), $app);
    $O = new SysmaObject($request->request->get('sysma_object_id'), $app);

    if (
        hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'gestionnaire') or (hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'contributeur') and $O->modified_by == $app['session']->get('user_id'))
    ) {

        $data['sysma_relation'] = $r->get('sysma_relation');
        $data['sysma_relation_id'] = $r->get('sysma_relation_id');
        $data['start_date'] = $r->get('start_date');
        $data['end_date'] = $r->get('end_date');
        $data['status'] = $r->get('status');
        $data['organisation_id'] = $r->get('organisation_id');
        $data['modified_by'] = $app['session']->get('user_id');

        $res = $RL->update($data);
        $res_decoded = json_decode($res);
        if ($res_decoded->status == 'error') {
            return $res;
        } else {

            // new relation, contracts insert

            $RL->deleteContracts();
            foreach (contracts($app) as $c) {
                if (key_exists('contract' . $c['contract_id'], $r->all())) {
                    $RL->addContract($c['contract']);
                }
            }
            return $res;
        }
    } else {
        $erreur['message'] = 'Vous ne pouvez accéder à cette fiche sysma_action';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('relation-modify-update-commit');

$app->match('/relation/sysma-objects-list', function (Request $request) use ($app) {


    $r = $request->request->all();
    if ($r['sysma_relation_type_id'] !== '') {
        $rl = explode('|', $r['sysma_relation_type_id']);


        $RLT = new SysmaRelationType($rl[0], $app);
        $Objects = $RLT->loadSysmaObjects($rl[3]);

        if (count($Objects) == 0)
            return 'Aucun objet a associer, commencez par créer un objet de type ' . $rl[3];

        $is_a_uniq_relation = $RLT->isAUniqRelation($rl[2]);


        $res = null;

        if ($is_a_uniq_relation) {
            $res = '<select class="form-control" name="sysma_object_id_link">';
        }

        foreach ($Objects as $O) {

            if (
                hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'gestionnaire') or (hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'contributeur') and $O->modified_by == $app['session']->get('user_id'))
            ) {


                if ($is_a_uniq_relation) {
                    $res .= '<option value="' . $O->sysma_object_id . '"> ' . $O->sysma_object . ' [' . $O->sysma_object_id . '] </option>';
                } else {
                    $res .= '<input type="checkbox" name="sysma_object_id_link_' . $O->sysma_object_id . '" value="1"> ' . $O->sysma_object . ' [' . $O->sysma_object_id . '] <br>';
                }
            }
        }

        if ($is_a_uniq_relation) {
            $res .= '</select>';
        }

        return $res;
    } else {
        return ' Chosissez un type de relation';
    }
})->bind('relation-sysma-objects-list');


$app->match('/relation/object-link', function (Request $request) use ($app) {

    $r = $request->request;
    $O = new SysmaObject($r->get('sysma_object_id'), $app);


    if (
        hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'gestionnaire') or (hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'contributeur') and $O->modified_by == $app['session']->get('user_id'))
    ) {

        $rl = explode('|', $r->get('sysma_relation_id'));
        $RL = new SysmaRelation($rl[0], $app);

        return $RL->addSysmaRelationLink($r->get('sysma_object_id'), $rl[1], $app['session']->get('user_id'));
    } else {
        $erreur['message'] = 'Vous ne pouvez accéder à cette fiche';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('object-link-relation');



$app->match('/relation/object-unlink', function (Request $request) use ($app) {

    $r = $request->request;
    $O = new SysmaObject($r->get('sysma_object_id'), $app);


    if (
        hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'gestionnaire') or (hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'contributeur') and $O->modified_by == $app['session']->get('user_id'))
    ) {


        $RL = new SysmaRelation($r->get('sysma_relation_id'), $app);
        if ($RL->unlinkSysmaRelation($r->get('sysma_object_id'), invertPC($r->get('parent_child')))) {
            return 'Objet dissocié';
        }
    } else {
        $erreur['message'] = 'Vous ne pouvez dissocier cet objet';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('object-unlink-relation');



// SUPPRIMER
$app->post('/relation/delete', function (Request $request) use ($app) {

    $RL = new SysmaRelation($request->request->get('sysma_relation_id'), $app);
    $O = new SysmaObject($request->request->get('sysma_object_id'), $app);
    if (
        hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'gestionnaire')
        or (hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'contributeur')
            and $T->modified_by == $app['session']->get('user_id')
            and $app['session']->get('user_id') != ANONYMID)
    ) {


        $errors = 0;
        $tabO = $RL->loadSysmaObjectsLinked('parent');
        foreach ($tabO as $Obj) {
            if ($RL->unlinkSysmaRelation($Obj['sysma_object_id'], 'parent')) {
            } else {
                $errors++;
            }
        }
        $tabO = $RL->loadSysmaObjectsLinked('child');
        foreach ($tabO as $Obj) {
            if ($RL->unlinkSysmaRelation($Obj['sysma_object_id'], 'child')) {
            } else {
                $errors++;
            }
        }


        if ($RL->delete($app)) {
        } else {
            $errors++;
        }

        if ($errors == 0) {
            if ($RL->delete()) {
                return json_encode(['status' => 'success']);
            } else {
                return json_encode(['status' => 'error']);
            }
        } else {
            return json_encode(['status' => 'error']);
        }
    } else {
        $erreur['message'] = 'Vous ne pouvez pas supprimer cette fiche relation';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('relation-delete');
