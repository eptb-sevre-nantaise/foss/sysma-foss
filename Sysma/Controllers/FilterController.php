<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

$app->match('/filter/{filter_id}/type/{object_type}/{id_type}/{dernier}', function ($filter_id, $object_type, $id_type, $dernier) use ($app) {

    return $app['twig']->render('filter/filterApplicationForm.twig', ['F' => new SysmaFilter($filter_id, $app), 'object_type' => $object_type, 'id_type' => $id_type, 'dernier' => $dernier, 'userFilters' => userFilters($app, $app['session']->get('user_id'))]);
})->bind('filter');



$app->post('/filter/condition/update', function (Request $request) use ($app) {

    $F = new SysmaFilter($request->request->get('filter_id'), $app);

    if ($request->request->get('object_type') == 'sysma_object') {
        $sysma_object_type_id = $request->request->get('id_type');
        $sysma_action_type_id = 0;
        $suff = '001';
    } elseif ($request->request->get('object_type') == 'sysma_action') {
        $sysma_action_type_id = $request->request->get('id_type');
        $sysma_object_type_id = 0;
        $suff = '002';
    }

    if ($F->updateCondition($request->request->get('condition'), $sysma_object_type_id, $sysma_action_type_id, $app['session']->get('user_id'))) {
        return ('');
    } else {
        return ('<small>KO</small><script>');
    }
})->bind('filter-condition-update');


$app->post('/filter/link', function (Request $request) use ($app) {

    if ($request->request->get('filter_id') !== null) {

        $F = new SysmaFilter($request->request->get('filter_id'), $app);

        if ($request->request->get('object_type') == 'sysma_object') {
            $sysma_object_type_id = $request->request->get('id_type');
            $sysma_action_type_id = 0;
        } elseif ($request->request->get('object_type') == 'sysma_action') {
            $sysma_action_type_id = $request->request->get('id_type');
            $sysma_object_type_id = 0;
        }
        if ($F->activate($app['session']->get('user_id'), $sysma_object_type_id, $sysma_action_type_id)) {

            return $app['twig']->render('filter/filterActiveFiltersSheet.twig', ['listeFiltres' => filtersList($request->request->get('object_type'), $request->request->get('id_type'), $app['session']->get('user_id'), $app), 'object_type' => $request->request->get('object_type'), 'id_type' => $request->request->get('id_type'), 'userFilters' => userFilters($app, $app['session']->get('user_id'))]);
        } else {
            return ('<small>KO</small>');
        }
    }
})->bind('filter-link');


/*
 * permet de lier les filter actifs sur le type de travaux en cours à tous les autres types de travaux du même type d'objet
 */

$app->post('/filter/link/all-sysma-action-types', function (Request $request) use ($app) {

    if ($request->request->get('object_type') == 'sysma_action') {

        $TT = new SysmaActionType($request->request->get('id_type'), $app);
        $TO = new SysmaObjectType($TT->sysma_object_type_id, $app);
        $TO->loadSysmaActionTypes();

        $U = new User($app['session']->get('user_id'), $app);


        // charge les filter pour le type de travaux
        if (isset($U->tabFiltres['sysma_action'][$request->request->get('id_type')])) {

            // pour chaque filter de la layer actuel
            foreach ($U->tabFiltres['sysma_action'][$request->request->get('id_type')] as $idf) {

                // pour chaque type de travaux du type d'objet actuel
                foreach ($TO->sysmaActionTypesArrayVal as $idtt) {
                    if ($idtt != $request->request->get('id_type')) {
                        $F = new SysmaFilter($idf, $app);
                        $F->activateWithCondition($app['session']->get('user_id'), 0, $idtt, $U->tabFiltresCond['sysma_action'][$request->request->get('id_type')][$idf]['condition']);
                    }
                }
            }
        }

        // pas de filter sur la layer actuelle, donc on supprimer les filter sur les autres layers travaux
        else {

            foreach ($TO->sysmaActionTypesArrayVal as $idtt) {
                if ($idtt != $request->request->get('id_type')) {
                    foreach ($U->tabFiltres['sysma_action'][$idtt] as $idf) {

                        $F = new SysmaFilter($idf, $app);
                        $F->desactivate($app['session']->get('user_id'), 0, $idtt);
                    }
                }
            }
        }

        // reloadSessionUser($app);
        return '<p>Filtres appliqués aux autrex layers sysma_action de ' . $TO->type . '</p>';
    } else {
        return ('<small>KO</small>');
    }
})->bind('filter-link-all-works');



$app->post('/filter/unlink', function (Request $request) use ($app) {

    $F = new SysmaFilter($request->request->get('filter_id'), $app);
    if ($request->request->get('object_type') == 'sysma_object') {
        $sysma_object_type_id = $request->request->get('id_type');
        $sysma_action_type_id = 0;
        $suff = '001';
    } elseif ($request->request->get('object_type') == 'sysma_action') {
        $sysma_action_type_id = $request->request->get('id_type');
        $sysma_object_type_id = 0;
        $suff = '002';
    }

    if ($F->desactivate($app['session']->get('user_id'), $sysma_object_type_id, $sysma_action_type_id)) {

        // reloadSessionUser($app);

        return $app['twig']->render('filter/filterActiveFiltersSheet.twig', ['listeFiltres' => filtersList($request->request->get('object_type'), $request->request->get('id_type'), $app['session']->get('user_id'), $app), 'object_type' => $request->request->get('object_type'), 'id_type' => $request->request->get('id_type'), 'userFilters' => userFilters($app, $app['session']->get('user_id'))]);
    } else {
        return ('<small>KO</small>');
    }
})->bind('filter-unlink');



$app->match('/filter-active/type/{object_type}/{id_type}', function ($object_type, $id_type) use ($app) {

    return $app['twig']->render('filter/filterActiveFiltersSheet.twig', ['listeFiltres' => filtersList($object_type, $id_type, $app['session']->get('user_id'), $app), 'object_type' => $object_type, 'id_type' => $id_type, 'userFilters' => userFilters($app, $app['session']->get('user_id'))]);
})->bind('active-filters');


$app->match('/filter/type/{object_type}/{id_type}', function ($object_type, $id_type) use ($app) {

    return $app['twig']->render('filter/filtersListForm.twig', ['listeFiltres' => filtersList($object_type, $id_type, $app['session']->get('user_id'), $app), 'object_type' => $object_type, 'id_type' => $id_type, 'userFilters' => userFilters($app, $app['session']->get('user_id'))]);
})->bind('filters');





$app->match('filter/{filter_id}/update', function ($filter_id, Request $request) use ($app) {



    if ($filter_id === 'nouveau') {
        $data = array();
    } else {
        $F = new SysmaFilter($filter_id, $app);
        $data = $F;
    }

    if ($app['session']->get('is_admin')) {

        $form = $app['form.factory']->createBuilder('form', $data)
            ->add('filter', 'text', array('label' => 'Intitulé du filter', 'constraints' => new Assert\NotBlank()))
            ->add('sql', 'text', array('label' => 'Code SQL (champs disponibles)', 'constraints' => new Assert\NotBlank()))
            ->add('object_type', 'choice', array('label' => 'filter applicable sur :', 'choices' => array('sysma_object' => 'Sur les objets', 'sysma_action' => 'Sur les sysma_action'), 'constraints' => new Assert\NotBlank()))
            ->getForm();

        return $app['twig']->render('filter/filterModificationForm.twig', ['F' => null, 'form' => $form->createView(), 'filter_id' => $filter_id, 'vars' => $request->request->all()]);
    } else {
        return $app->abort();
    }
})->bind('filter-modify');


$app->match('filter/{filter_id}/modifier/commit', function ($filter_id, Request $request) use ($app) {

    if ($app['session']->get('is_admin')) {

        $data = $request->request->all();
        $data = $data['form'];
        unset($data['_token']);


        $F = new SysmaFilter($filter_id, $app);
        if ($filter_id === 'nouveau') {
            $id = $F->insert($data);
        } else {
            $id = $F->update($data);
        }
        return $id;
    } else {
        return $app->abort();
    }
})->bind('filter-modify-commit');
