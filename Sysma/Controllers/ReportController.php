<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;





$app->match('/selection-geo', function (Request $request) use ($app) {



    $U = new User($app['session']->get('user_id'), $app);
    $layers = $U->loadLayerManager();


    $sysmaObjectTypeIds = array();
    $sysmaObjectTypeIdFilters = array();
    $selectionGeom = null ;
    foreach ($layers['objectTypeLayers'] as $key => $l) {
        if ($l['user_id'] != null) {
            $sysmaObjectTypeIds[] = $l['sysma_object_type_id'];
            $sysmaObjectTypeIdFilters[$l['sysma_object_type_id']] = buildSQLFilter('sysma_object', $l['sysma_object_type_id'], $app);
        }
    }


    $objects = null;

    if ($request->request->get('secteur') !== null) {

        $objects = findSysmaObjects(null, ['sysma_object_type_ids' => $sysmaObjectTypeIds, 'sysma_object_type_id_filters' => $sysmaObjectTypeIdFilters, 'geom_from_secteur' => getSecteurGeom($app, $request->request->get('secteur'))],  $app);
        $app['session']->set('selection_secteur', $request->request->get('secteur'));
    } elseif ($app['session']->get('selection_secteur') !== null and $request->request->get('geom') == null) {
        $objects = findSysmaObjects(null, ['sysma_object_type_ids' => $sysmaObjectTypeIds, 'sysma_object_type_id_filters' => $sysmaObjectTypeIdFilters, 'geom_from_secteur' => getSecteurGeom($app, $app['session']->get('selection_secteur'))],  $app);
    } else {


        // SESSION

        if ($request->request->get('geom') !== null) {
            $app['session']->set('user_selection_sysma_objects',  null);
            $app['session']->set('selection_geo', $request->request->get('geom'));
            $selectionGeom = $request->request->get('geom');
        } elseif ($app['session']->get('selection_geo') !== null) {
            $selectionGeom =  $app['session']->get('selection_geo');
        }

        if ($selectionGeom !== null) {
            $objects = findSysmaObjects(convertCoords($selectionGeom), ['sysma_object_type_ids' => $sysmaObjectTypeIds, 'sysma_object_type_id_filters' => $sysmaObjectTypeIdFilters],  $app);
        }
    }


    $secteurs = array_merge(array('' => ''), listeEPCI($app), listeME($app), objectsInTableList($app, array('table' => REF_PG_SCHEMA_TABLE_SECTEUR, 'sysma_object_id' => REF_PG_SCHEMA_TABLE_SECTEUR_GID, 'sysma_object' => REF_PG_SCHEMA_TABLE_SECTEUR_NAME, 'geom' => REF_PG_SCHEMA_TABLE_SECTEUR_GEOM)));


    $res = $app['twig']->render('report/selectionResults.twig', ['res' => array('sysma_objects' => $objects, 'sysma_actions' => null), 'secteurs' => $secteurs]);
    return $res;
})->bind('selection-geo');





$app->post('/selection-action', function (Request $request) use ($app) {




    $r = $request->request->all();


    if ($r['action'] == 'fill_objects_parameters') {
        return $app->redirect($app['url_generator']->generate('objects-tab-form', ['sysma_object_ids_list' => $r['sysma_object_ids_list'], 'sysma_object_type_id' => $r['sysma_object_type_id']]));
    } else  if ($r['action'] == 'modify_objects') {
        return $app->redirect($app['url_generator']->generate('objects-modify', ['sysma_object_ids_list' => $r['sysma_object_ids_list'], 'sysma_object_type_id' => $r['sysma_object_type_id']]));
    } else {
        return 'Choisir une action';
    }
})->bind('selection-action');



$app->post('/selection-update', function (Request $request) use ($app) {



    $r = $request->request->all();


    if ($r['type'] == 'delete_all') {
        $app['session']->set('user_selection_sysma_objects',  null);
        $app['session']->set('selection_geo', null);
        $app['session']->set('selection_secteur', null);
    }

    //  var_dump( $app['session']->get('user_selection_sysma_objects')) ;

    if ($r['type'] == 'sysma_objects') {

        $sOb = $app['session']->get('user_selection_sysma_objects');
        //   var_dump($sOb) ;
        $tabOb = explode(',', $r['sysma_objects_list']);
        $sOb[$r['sysma_object_type_id']] = null;
        foreach ($tabOb as $o) {
            $sOb[$r['sysma_object_type_id']][$o] =  true;
        }
        //  var_dump($sOb) ;

        $app['session']->set('user_selection_sysma_objects',  $sOb);
    }

    // var_dump( $app['session']->get('user_selection_sysma_objects')) ;

    return '';
})->bind('selection-update');



$app->match('/selection', function (Request $request) use ($app) {

    $selectedFeatures = explode(',', $request->request->get('selectedFeatures'));
    $objects = null;

    foreach ($selectedFeatures as $f) {


        $fType = substr($f, 0, 1);
        $fId = substr($f, 1, strlen($f) - 1);

        if ($fType == 'o') {
            $O = new SysmaObject($fId, $app);
            $objects[] = $O;
        }
        /*
        if ($fType=='a'){
            $A = new SysmaAction($fType,$app);
            $actions[$O->sysma_object_type_id][] = $O;
        }
*/
    }

    $secteurs = array_merge(array('' => ''), listeEPCI($app), listeME($app), objectsInTableList($app, array('table' => REF_PG_SCHEMA_TABLE_SECTEUR, 'sysma_object_id' => REF_PG_SCHEMA_TABLE_SECTEUR_GID, 'sysma_object' => REF_PG_SCHEMA_TABLE_SECTEUR_NAME, 'geom' => REF_PG_SCHEMA_TABLE_SECTEUR_GEOM)));

    $res = $app['twig']->render('report/selectionResults.twig', ['res' => array('sysma_objects' => $objects, 'sysma_actions' => null, 'secteurs' => $secteurs)]);
    return $res;
})->bind('selection');



$app->match('/recherche', function (Request $request) use ($app) {

    $request->query->get('q') === null ? $q = $request->request->get('q') : $q = $request->query->get('q');


    $input = '
              <form class="form-inline" action="#">              
              <div class="form-group">
              <input type="text" class="form-control" placeholder="Rechercher" name="q" id="q" value="' . $q . '" />              
              </div>              
              <div class="form-group">
              <button class="btn btn-default" onclick=\'
              window.history.pushState(null, null, "' . $app['url_generator']->generate('map') . '?q=" + $("#q").val());
              sendData("q=" + $("#q").val(), "' . $app['url_generator']->generate('search') . '", "objet", event); \'>
              <span class="glyphicon glyphicon-search" ></span> </a></div>
              </div>';
    $res = null;


    if ($q != '' and $q != null and strlen($q) > 2) {
        if (is_numeric($q)) {

            $sysma_objects = findElementById($q, 'sysma_object', 'sysma_object_id', 'SysmaObject', $app);
            $sysma_actions = findElementById($q, 'sysma_action', 'sysma_action_id', 'SysmaAction', $app);
            //var_dump($Objets) ;
        }

        $sysma_objects = findElementByName($q, 'sysma_object', 'sysma_object', 'sysma_object_id', 'SysmaObject', $app);
        $sysma_actions = findElementByName($q, 'sysma_action', 'sysma_action', 'sysma_action_id', 'SysmaAction', $app);

        $res = $app['twig']->render('report/searchResults.twig', ['q' => $q, 'res' => array('sysma_objects' => $sysma_objects, 'sysma_actions' => $sysma_actions)]);
    } else {
    }

    return $input . $res;
})->bind('search');




$app->match('/bilan/objets', function (Request $request) use ($app) {


    $data = null;
    if (hasAccess($app, $app['session']->get('user_id'), null, null, null, null, null, null, 'lecteur')) {


        $form = $app['form.factory']->createBuilder('form')
            ->add('sysma_object_type_id_list', 'choice', array('label' => 'Types d\'objet', 'choices' => ['all' => 'Tous'] + objectTypesFilteredList($app), 'multiple' => true, 'constraints' => new Assert\NotBlank()))
            ->add('sysma_object_organisation_id_list', 'choice', array('label' => 'Structure', 'choices' => ['all' => 'Toutes'] + organisationWithUserAccessList($app), 'multiple' => true, 'constraints' => new Assert\NotBlank()))
            ->add('sysma_object_status_list', 'choice', array('label' => 'Statuts', 'choices' => ['all' => 'Tous'] + objectStatusFilteredList($app), 'multiple' => true, 'constraints' => new Assert\NotBlank()))
            ->add('donnees', 'checkbox', array('label' => 'Afficher les données des objets'))
            ->add('communes', 'checkbox', array('label' => 'Afficher la liste des communes par objet'))
            ->add('epci', 'checkbox', array('label' => 'Liste des EPCI par objet'))
            //    ->add('bv_ct', 'checkbox', array('label' => 'Liste des BV CT par objet'))
            ->add('bv_me', 'checkbox', array('label' => 'Liste des Masses d\'eau par objet'))
            ->add('infogeo', 'checkbox', array('label' => 'Linéaires et surfaces'))
            ->add('photos', 'checkbox', array('label' => 'Afficher les photos'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {

            $data = $form->getData();

            $tabO = findObjects($data['sysma_object_type_id_list'], $data['sysma_object_organisation_id_list'], $data['sysma_object_status_list'], $app);


            $tabC = array();
            if ($data['communes'] == 'true') {

                foreach ($tabO as $O) {
                    $tabC[$O->id] = findIntersectingCities($O->geomRaw, array('tampon' => 0.1), $app);
                }
            }

            if ($data['donnees'] == 'true') {

                foreach ($tabO as $O) {
                    $O->loadActualInfosSysmaObject();
                }
            }

            if ($data['photos'] == 'true') {
                foreach ($tabO as $O) {
                    $O->loadPhotoObjects();
                }
            }

            $tabBVCT = array(); /*
              if ($data['bv_ct'] == 'true') {
              foreach ($tabO as $O) {
              $tabBVCT[$O->id] = findIntersectingObjects(REF_PG_SCHEMA_TABLE_WATERSHED, $O->geomRaw, array('sysma_object_id' => REF_PG_SCHEMA_TABLE_WATERSHED_GID, 'sysma_object' => REF_PG_SCHEMA_TABLE_WATERSHED_NAME, 'tampon' => 0.01, 'seulement' => array(13, 14, 15, 16)), $app);
              }
              }
             */

            $tabEPCI = array();
            if ($data['epci'] == 'true') {
                foreach ($tabO as $O) {
                    $tabEPCI[$O->id] = findIntersectingObjects(REF_PG_SCHEMA_TABLE_INTERCOMMUNAL, $O->loadGeom(), array('tampon' => 0.01, 'id' => REF_PG_SCHEMA_TABLE_INTERCOMMUNAL_GID, 'object' => REF_PG_SCHEMA_TABLE_INTERCOMMUNAL_NAME, 'geom' => REF_PG_SCHEMA_TABLE_INTERCOMMUNAL_GEOM), $app);
                }
            }


            $tabME = array();
            if ($data['bv_me'] == 'true') {
                foreach ($tabO as $O) {
                    $tabME[$O->id] = findIntersectingObjects(REF_PG_SCHEMA_TABLE_MASSEEAU, $O->geomRaw, array('id' => REF_PG_SCHEMA_TABLE_MASSEEAU_GID, 'object' => REF_PG_SCHEMA_TABLE_MASSEEAU_NAME, 'tampon' => 0.01), $app);
                }
            }


            $tabLin = $tabSurf = null;
            if ($data['infogeo'] == 'true') {

                foreach ($tabO as $O) {
                    if (isset($O->infosGeom['perimetre']))
                        $tabLin[$O->id] = $O->infosGeom['perimetre'];
                    if (isset($O->infosGeom['surface']))
                        $tabSurf[$O->id] = $O->infosGeom['surface'];
                }
            }


            if ($request->query->get('csv') !== null) {


                $response = new Response();
                $response->setContent($app['twig']->render('report/sysmaObjectsReportCsv.twig', ['tabO' => $tabO, 'tabC' => $tabC, 'tabEPCI' => $tabEPCI, 'tabME' => $tabME, 'tabBVCT' => $tabBVCT, 'infogeo' => $data['infogeo']]));
                $response->headers->set('Content-Type', 'text/csv');
                $response->headers->set('Content-Disposition', 'attachment; filename="Export_Sysma_' . date('Ymd_His') . '.csv"');
                ob_clean(); // Mandatory ! This fix this wtf bug that add a line-break (0x0A) at the begining of the response... Oo
                return $response;
            } else {
                return $app['twig']->render('report/sysmaObjectsReport.twig', ['form' => $form->createView(), 'tabO' => $tabO, 'tabC' => $tabC, 'tabEPCI' => $tabEPCI, 'tabME' => $tabME, 'tabBVCT' => $tabBVCT, 'infogeo' => $data['infogeo']]);
            }
        }

        if ($data['csv'] == 'true') {
            return $app['twig']->render('report/sysmaObjectsReportCsv.twig', ['form' => $form->createView(), 'tabO' => null]);
        } else {
            return $app['twig']->render('report/sysmaObjectsReport.twig', ['form' => $form->createView(), 'tabO' => null]);
        }
    } else {

        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('report-objects');


/*
 * 
 * TODO
 * A revoir, trop lié à piwigo et owncloud


  $app->match('/bilan/objets/photos', function (Request $request) use ($app) {


  if (hasAccess($app, $app['session']->get('user_id'), null, null, null, null, null, null, 'lecteur')) {


  ini_set('max_execution_time', 3000); //300 seconds = 5 minutes

  $data0 = $request->request->all();
  $data = $data0['form'];

  if (isset($data['sysma_object_type_id_list']) and isset($data['sysma_object_organisation_id_list']) and isset($data['sysma_object_status_list'])) {

  //var_dump($data) ;

  $tabO = findObjects($data['sysma_object_type_id_list'], $data['sysma_object_organisation_id_list'], $data['sysma_object_status_list'], $app);


  foreach ($tabO as $O) {
  $O->loadPhotoObjects();
  }


  // constitution de l'archive photo

  $res = '<html><head> <meta charset="utf-8"><title>Sysma, export photos</title><style>body { font-family:arial,sans-serif ; }</style></head><body><h2>EPTB Sèvre Nantaise</h2><h1>Export photo Sysma</h1><p>Droits : EPTB Sèvre Nantaise, sauf mentions contraires<p>';
  $now = date('Ymd_His');
  $dir = ROOT_URI . '/export_photo/'.$now;
  mkdir($dir, 0755);
  $filename = 'Export_photos_Sysma_' . $now . '.zip';

  $zip = new ZipArchive;

  if ($zip->open($dir . '/' . $filename, ZipArchive::CREATE) === TRUE) {

  foreach ($tabO as $O) {

  foreach ($O->tabOP as $OP) {

  if (isset($OP->Photo)) {


  if ($zip->addFile('/var/www/photos/www' . substr($OP->Photo->path, 1), $OP->Photo->file)) {

  $res .= '' . $OP->Photo->file;
  $OP->Photo->author != null ? $res .= ' - Auteur : ' . utf8_encode($OP->Photo->author) : null;
  $res .= ' - Id phototheque EPTBSN : ' . $OP->Photo->id;
  $res .= '<br />';
  } else {
  die('Erreur lors de la récupération des fichiers photos');
  }
  }
  }
  }

  $res .= '</body></html>';
  $zip->addFromString('infos_photos.html', $res);
  $zip->close();
  }




  if (uploadWebDav($filename, $dir . '/' . $filename, FILEDIR)) {
  $share = createOCShare($filename, $dir . '/' . $filename, FILEDIR);
  unlink($dir . '/' . $filename);
  rmdir($dir);

  return $app->redirect($share->data->url);
  }
  } else {
  $erreur['message'] = 'Paramètres non fournis';
  return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
  }
  } else {

  $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
  return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
  }
  })->bind('report-objects-photos');




 */



$app->match('/bilan/actions', function (Request $request) use ($app) {

    $data = null;
    if (hasAccess($app, $app['session']->get('user_id'), null, null, null, null, null, null, 'lecteur')) {


        $form = $app['form.factory']->createBuilder('form')
            ->add('sysma_action_type_id_list', 'choice', array('label' => 'Types de travaux', 'choices' => ['all' => 'Tous'] + listeTypesTravauxFiltres($app), 'multiple' => true, 'constraints' => new Assert\NotBlank()))
            ->add('sysma_object_organisation_id_list', 'choice', array('label' => 'Structure', 'choices' => ['all' => 'Toutes'] + organisationWithUserAccessToSysmaActionList($app), 'multiple' => true, 'constraints' => new Assert\NotBlank()))
            ->add('sysma_action_status_list', 'choice', array('label' => 'Statuts', 'choices' => ['all' => 'Tous'] + listeStatutsTravauxFiltres($app), 'multiple' => true, 'constraints' => new Assert\NotBlank()))
            ->add('sysma_action_year', 'integer', array('label' => 'Année des travaux'))
            ->add('program_year', 'integer', array('label' => 'Année d\'engagement dans le budget'))
            ->add('contract', 'choice', array('label' => 'Contrat', 'choices' => array_merge(array('' => ''), contractsNom($app))))
            ->add('object', 'choice', array('label' => 'Limiter la recherche à ', 'choices' => array_merge(array('' => ''), listeEPCI($app), listeME($app), objectsInTableList($app, array('table' => REF_PG_SCHEMA_TABLE_SECTEUR, 'sysma_object_id' => REF_PG_SCHEMA_TABLE_SECTEUR_GID, 'sysma_object' => REF_PG_SCHEMA_TABLE_SECTEUR_NAME, 'geom' => REF_PG_SCHEMA_TABLE_SECTEUR_GEOM)))))
            ->add('donnees', 'checkbox', array('label' => 'Données des travaux'))
            ->add('donnees_objects', 'checkbox', array('label' => 'Données des objets (s\'affichera uniquement si un seul type d\'objet dans le résultat du bilan)'))
            ->add('communes', 'checkbox', array('label' => 'Liste des communes par objet'))
            ->add('epci', 'checkbox', array('label' => 'Liste des EPCI par objet'))
            ->add('bv_me', 'checkbox', array('label' => 'Liste des Masses d\'eau par objet'))
            ->add('infogeo', 'checkbox', array('label' => 'Linéaires et surfaces'))
            ->add('regroup', 'checkbox', array('label' => 'Regrouper les résultats par fiche travaux'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {

            $data = $form->getData();
            // var_dump($data) ;
            // 

            if (!isset($data['object']) or $data['object'] == '') {
                $tabT = findSysmaActions2($data['sysma_action_type_id_list'], $data['sysma_object_organisation_id_list'], $data['sysma_action_status_list'], $data['sysma_action_year'], $data['program_year'], $data['contract'], $app);
            } else {
                $tabT = findSysmaActions3($data['sysma_action_type_id_list'], $data['sysma_object_organisation_id_list'], $data['sysma_action_status_list'], $data['sysma_action_year'], $data['program_year'], $data['contract'], $data['object'], $app);
            }

            //  var_dump(count($tabT)) ;

            $tabC = array();
            if ($data['communes'] == 'true') {
                foreach ($tabT as $T) {
                    // $T->loadSysmaObjects($app);
                    foreach ($T->sysmaObjectsArray as $O) {
                        $tabC[$O->id] = findIntersectingObjects(REF_PG_SCHEMA_TABLE_CITIES, $O->loadGeom(), array('tampon' => 0.01, 'id' => REF_PG_SCHEMA_TABLE_CITIES_GID, 'object' => REF_PG_SCHEMA_TABLE_CITIES_NAME), $app);
                    }
                }
            }


            $tabEPCI = array();
            if ($data['epci'] == 'true') {
                foreach ($tabT as $T) {
                    // $T->loadSysmaObjects($app);
                    foreach ($T->sysmaObjectsArray as $O) {
                        $tabEPCI[$O->id] = findIntersectingObjects(REF_PG_SCHEMA_TABLE_INTERCOMMUNAL, $O->loadGeom(), array('tampon' => 0.01, 'id' => REF_PG_SCHEMA_TABLE_INTERCOMMUNAL_GID, 'object' => REF_PG_SCHEMA_TABLE_INTERCOMMUNAL_NAME, 'geom' => REF_PG_SCHEMA_TABLE_INTERCOMMUNAL_GEOM), $app);
                    }
                }
            }


            $tabME = array();
            if ($data['bv_me'] == 'true') {
                foreach ($tabT as $T) {
                    // $T->loadSysmaObjects($app);
                    foreach ($T->sysmaObjectsArray as $O) {
                        $tabME[$O->id] = findIntersectingObjects(REF_PG_SCHEMA_TABLE_MASSEEAU, $O->loadGeom(), array('id' => REF_PG_SCHEMA_TABLE_MASSEEAU_GID, 'object' => REF_PG_SCHEMA_TABLE_MASSEEAU_NAME, 'tampon' => 0.01), $app);
                    }
                }
            }

            if ($data['donnees'] == 'true') {

                foreach ($tabT as $T) {
                    $T->loadActualInfosSysmaAction();
                }
            }
            // Objet datas 
            // !! Experimental !!
            // TODO :  gérer les données pour différents type d'objets
            // S'assurer que l'affichage soit correcte même si le premier objet n'a pas tous les paramètres de rensignés
            // car c'est ce premier objet [0] qui sera utilisé dans le twig pour afficher les titre de colonnes
            
            if ($data['donnees_objects'] == 'true') {
                // vérifie le enombre de type objet, car l'affichage ne sera corecte que sur un seul type d'objet
                $ObjectTypeIds = array();
                foreach ($tabT as $T) {
                    foreach ($T->sysmaObjectsArray as $O) {
                        // $ObjectTypeIds[] = $O->id;
                        $ObjectTypeIds[] = $O->sysma_object_type_id;                  
                    }
                }
                // Compter le nombre d'occurrences de chaque ID
                $ObjectTypeIdsOccurrences = array_count_values($ObjectTypeIds);
                // Compter le nombre d'ID différents
                $ObjectTypeIdsCountDifferents = count($ObjectTypeIdsOccurrences);              
                ## Il ne faut qu'un seul type d'objet
                if ($ObjectTypeIdsCountDifferents === 1) {
                    foreach ($tabT as $T) {
                        foreach ($T->sysmaObjectsArray as $O) {
                            $O->loadActualInfosSysmaObject();
                        }
                    } 
                } else {
                        // Atuellement , l'affichage ds données objet n'est pas possible sur plusieurs types d'objets
                        $data['donnees_objects'] = 'false';
                        $app['session']->getFlashBag()->add('message', array('status' => 'warning', 'text' => 'Impossible d\'afficher les données des objets car le résultat retourne plusieurs types d\'objets'));
                    }

            }
            
           
            
            $tabLin = $tabSurf = null;
            if ($data['infogeo'] == 'true') {

                foreach ($tabT as $T) {
                    //$T->loadSysmaObjects($app);
                    foreach ($T->sysmaObjectsArray as $O) {
                        $O->loadGeoInfos($O->geometry_type);
                        if (isset($O->infosGeom['perimetre']))
                            $tabLin[$O->id] = $O->infosGeom['perimetre'];
                        if (isset($O->infosGeom['surface']))
                            $tabSurf[$O->id] = $O->infosGeom['surface'];
                    }
                }
            }


            // costs
            $tabCost['cost_estimated_user'] = null;
            $tabCost['cost_final_user'] = null;
            $tabCost['total_cost_estimated_user'] = 0;
            $tabCost['total_cost_final_user'] = 0;
            foreach ($tabT as $T) {
                $tabCost['cost_estimated_user'][$T->id] = @$tabCost['cost_estimated_user'][$T->id] + $T->total_cost_estimated_user;
                $tabCost['cost_final_user'][$T->id] = @$tabCost['cost_final_user'][$T->id] + $T->total_cost_final_user;
            }
            foreach ($tabCost['cost_estimated_user'] as $c) {
                $tabCost['total_cost_estimated_user'] += $c;
            }
            foreach ($tabCost['cost_final_user'] as $c) {
                $tabCost['total_cost_final_user'] += $c;
            }

            if ($request->query->get('csv') !== null) {

                $response = new Response();
                $response->setContent($app['twig']->render('report/sysmaActionReportCsv.twig', ['form' => $form->createView(), 'tabT' => $tabT, 'tabC' => $tabC, 'tabEPCI' => $tabEPCI, 'tabME' => $tabME, 'tabLin' => $tabLin, 'tabSurf' => $tabSurf, 'regroup' => $data['regroup'], 'communes' => $data['communes'], 'infogeo' => $data['infogeo']]));
                $response->headers->set('Content-Type', 'text/csv');
                $response->headers->set('Content-Disposition', 'attachment; filename="Export_Sysma_' . date('Ymd_His') . '.csv"');
                ob_clean(); // Mandatory ! This fix this wtf bug that add a line-break (0x0A) at the begining of the response... Oo
                return $response;
            } else {


                return $app['twig']->render('report/sysmaActionReport.twig', ['form' => $form->createView(), 'tabT' => $tabT, 'tabC' => $tabC, 'tabEPCI' => $tabEPCI, 'tabME' => $tabME, 'tabLin' => $tabLin, 'tabSurf' => $tabSurf, 'regroup' => $data['regroup'], 'communes' => $data['communes'], 'infogeo' => $data['infogeo'], 'tabCost' => $tabCost]);
            }
        }

        return $app['twig']->render('report/sysmaActionReport.twig', ['form' => $form->createView(), 'tabT' => null]);
    } else {

        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('report-works');






$app->match('/bilan/export-pg', function (Request $request) use ($app) {

    if ($app['session']->get('is_admin') or $app['session']->get('has_gis_export_privilege')) {

        $form = $app['form.factory']->createBuilder('form')
            ->add('sysma_object_type_id_list', 'choice', array('label' => 'Types d\'objet', 'choices' => objectTypesFilteredList($app), 'multiple' => true, 'constraints' => new Assert\NotBlank()))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {

            $data = $form->getData();
            $resTO = array();
            $resTT = array();

            if ($request->query->get('old') !== null) {
                // previous original func
                foreach ($data['sysma_object_type_id_list'] as $id) {
                    if (hasAccess($app, $app['session']->get('user_id'), null, $id, null, null, null, null, 'lecteur')) {
                        $tabTO[] = $id;
                    }
                }
                $res = generateSysmaLayers($tabTO, $app);
            } else {
                foreach ($data['sysma_object_type_id_list'] as $id) {
                    if (hasAccess($app, $app['session']->get('user_id'), null, $id, null, null, null, null, 'lecteur')) {
                        if (empty($r = generateSysmaLayerFast($id, $app))) {
                            break;
                        }
                        $resTO[] = $r['resTO'];
                        foreach ($r['resTT'] as $rTT) {
                            $resTT[] = $rTT;
                        }
                    }
                }
                $res = array('resTO' => $resTO, 'resTT' => $resTT);
            }


            if ($res) {
                /*
                  if (isset($data['export_shape']) and $data['export_shape'] == 'true') {



                  if ($data['id_secteur'] != 'false') {
                  $tabSecteur = explode('.', $data['id_secteur']);

                  $Secteur = loadObject($app, array('table' => $tabSecteur[0] . '.' . $tabSecteur[1], 'sysma_object_id' => $tabSecteur[2], 'sysma_object' => $tabSecteur[3], 'geom' => $tabSecteur[4], 'id' => $tabSecteur[5]));
                  $secteur_geom = $Secteur->geom;
                  } else {
                  $secteur_geom = null;
                  }

                  $unic = uniqid();
                  $folder = dirname(__DIR__) . '/../export_shp/' . $unic . '/';


                  $zipFileInfos = createSysmaLayerShapes($app, $res, $data['export_sysma_action'], $Secteur->geom, $folder, $data['buffer']);

                  if ($zipFileInfos['zipFile'] != false) {

                  // zip file may be corrupted if php errors are displayed
                  $response = new BinaryFileResponse($zipFileInfos['zipFile']);
                  $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);
                  $app['folders_to_delete'] = $zipFileInfos['foldersToDelete'];
                  return $response;
                  } else {
                  $erreur['message'] = 'Erreur lors de la création de la layer';
                  return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
                  }
                  }
                 */
                return $app['twig']->render('report/PGLayersReport.twig', ['form' => $form->createView(), 'tabO' => $data, 'res' => $res]);
            }
        }

        return $app['twig']->render('report/PGLayersReport.twig', ['form' => $form->createView(), 'tabO' => null, 'res' => array('resTO' => null, 'resTT' => null)]);
    } else {

        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('report-pg-layers');




$app->match('/bilan/couches-sig', function (Request $request) use ($app) {

    if ($app['session']->get('has_gis_export_privilege')) {

        $default['export_pg'] = true;
        $form = $app['form.factory']->createBuilder('form', $default)
            ->add('sysma_object_type_id_list', 'choice', array('label' => 'Types d\'objet', 'choices' => objectTypesFilteredList($app), 'multiple' => true, 'constraints' => new Assert\NotBlank()))
            ->add('export_gis', 'hidden', array('label' => 'Exporter les layers générées au format shape', 'data' => 'true', 'constraints' => new Assert\NotBlank()))
            ->add('export_pg', 'checkbox', array('label' => 'Forcer la mise à jour des données (export PG)'))
            ->add('export_sysma_action', 'choice', array('label' => 'Exporter également les couches des actions ?', 'choices' => array('true' => 'Oui', 'false' => 'Non'), 'data' => 'false', 'constraints' => new Assert\NotBlank()))
            ->add('id_secteur', 'choice', array('label' => 'Limiter le création des exports à un secteur ?', 'choices' => array_merge(array_merge(array('false' => 'Ne pas limiter à un secteur'), listeEPCI($app), listeME($app), objectsInTableList($app, array('table' => REF_PG_SCHEMA_TABLE_SECTEUR, 'sysma_object_id' => REF_PG_SCHEMA_TABLE_SECTEUR_GID, 'sysma_object' => REF_PG_SCHEMA_TABLE_SECTEUR_NAME, 'geom' => REF_PG_SCHEMA_TABLE_SECTEUR_GEOM)))), 'multiple' => false, 'data' => 'false', 'constraints' => new Assert\NotBlank()))
            ->add('buffer', 'integer', array('label' => 'Ajouter un tampon autour du secteur (m) ?', 'data' => 0, 'constraints' => new Assert\NotBlank()))
            ->add('gis_format', 'choice', array('label' => 'Format d\'export', 'choices' => ['gpkg' => 'Géopackage', 'shp' => 'Shape']))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {

            $data = $form->getData();


            // reverification des privilege_list
            $tabTO = array();

            $tabSysmaObjectType = [];
            $tabSysmsaActionType = [];
            foreach ((array) $data['sysma_object_type_id_list'] as $toid) {
                if (hasAccess($app, $app['session']->get('user_id'), null, $toid, null, null, null, null, 'lecteur')) {
                    $tabTO[] = $toid;

                    $TO = new SysmaObjectType($toid, $app);

                    // previous old version
                    if ($request->query->get('old') !== null) {
                        if (!sysmaExportLayerExists($TO->sysma_object_type_alias, $app) or ($data['export_pg'] == true)) {
                            generateSysmaLayers($tabTO, $app);
                        }
                    } else {

                        // new fast version
                        // generate pg export tables (TO+TA+?) if explicitly ask for it or if they don't exists yet 
                        if (($data['export_pg'] == true) or (!sysmaExportLayerExists($TO->sysma_object_type_alias, $app))) {
                            if (generateSysmaLayerFast($toid, $app)) {
                            } else {
                                die();
                            }
                        }
                    }


                    $tabSysmaObjectType[] = ['sysma_object_type_alias' => $TO->sysma_object_type_alias, 'sysma_object_type_id' => $toid];

                    // get TAs' info
                    $TO->loadSysmaActionTypes();
                    foreach ((array) $TO->sysmaActionTypesArray as $TA) {
                        $tabSysmsaActionType[] = ['sysma_action_type_alias' => $TA->sysma_action_type_alias, 'sysma_action_type_id' => $TA->sysma_action_type_id, 'sysma_object_type_id' => $toid];
                    }
                }
            }
            $res = ['resTO' => $tabSysmaObjectType, 'resTT' => $tabSysmsaActionType];


            // if ($data['export_pg'] != true) {
            //     $tabSysmaObjectType = [];
            //     $tabSysmsaActionType = [];

            //     foreach ((array) $tabTO as $to) {

            //         $TO = new SysmaObjectType($to, $app);
            //         $tabSysmaObjectType[] = ['sysma_object_type_alias' => $TO->sysma_object_type_alias, 'sysma_object_type_id' => $to];

            //         $TO->loadSysmaActionTypes();
            //         foreach ((array) $TO->sysmaActionTypesArray as $TA) {

            //             $tabSysmsaActionType[] = ['sysma_action_type_alias' => $TA->sysma_action_type_alias, 'sysma_action_type_id' => $TA->sysma_action_type_id, 'sysma_object_type_id' => $to];
            //         }
            //     }

            //     $res = ['resTO' => $tabSysmaObjectType, 'resTT' => $tabSysmsaActionType];
            // }

            // var_dump($res);

            if (count($res['resTO']) == 0 or !isset($res['resTO'])) {

                $app['session']->getFlashBag()->add('message', array('status' => 'success', 'text' => 'Il n\' y aucun objet à exporter'));
                return $app['twig']->render('report/GISLayersReport.twig', ['form' => $form->createView(), 'tabTO' => null, 'res' => array('resTO' => null, 'resTT' => null)]);
            }


            if ($data['export_gis'] == 'true') {

                if ($data['id_secteur'] != 'false') {
                    $tabSecteur = explode('.', $data['id_secteur']);

                    $Secteur = loadObject($app, array('table' => $tabSecteur[0] . '.' . $tabSecteur[1], 'sysma_object_id' => $tabSecteur[2], 'sysma_object' => $tabSecteur[3], 'geom' => $tabSecteur[4], 'id' => $tabSecteur[5]));
                    $secteur_geom = $Secteur->geom;
                } else {
                    $secteur_geom = null;
                }

                $unic = uniqid();
                $folder = dirname(__DIR__) . '/../export_shp/' . $unic . '/';
                $zipFileInfos = createSysmaGISLayers($app, $res, $data['export_sysma_action'], $secteur_geom, $folder, $data['buffer'], $data['gis_format']);

                if ($zipFileInfos['zipFile'] != false) {

                    // zip file may be corrupted if php errors are displayed
                    $response = new BinaryFileResponse($zipFileInfos['zipFile']);
                    $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);
                    $app['folders_to_delete'] = $zipFileInfos['foldersToDelete'];
                    ob_clean(); // Mandatory with BinaryFileResponse! This fix this wtf bug that add a line-break (0x0A) before the binary file content... Oo
                    return $response;
                } else {
                    $erreur['message'] = 'Erreur lors de la création de la couche SIG';
                    return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
                }
            }

            return $app['twig']->render('report/GISLayersReport.twig', ['form' => $form->createView(), 'tabO' => $data, 'res' => $res]);
        }

        return $app['twig']->render('report/GISLayersReport.twig', ['form' => $form->createView(), 'tabO' => null, 'res' => array('resTO' => null, 'resTT' => null)]);
    } else {

        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('report-gis-layers');




/*
 * création d'un bilan REH et objets required_field_parameter à l'échelle d'un polygon (microbv)
 */



$app->match('/bilan/sous-bv', function (Request $request) use ($app) {

    // les user avec privilege_list de lecture sur les segments REH seulement
    if (hasAccess($app, $app['session']->get('user_id'), null, 39, null, null, null, null, 'lecteur')) {



        $builder = $app['form.factory']->createBuilder('form')->add('sysma_object_id', 'choice', array('label' => 'Sous-bassin(s)', 'choices' => objectsInTableList($app, array('table' => '__selection_secteur.t_secteurs', 'sysma_object_id' => 'gid', 'sysma_object' => sysma_object, 'geom' => 'geom')), 'multiple' => true, 'constraints' => new Assert\NotBlank()));


        $form = $builder->getForm();


        $form->handleRequest($request);

        if ($form->isValid()) {

            $data = $form->getData();


            $res = generateSubbasinReport($app, $data);

            return $app['twig']->render('report/subbasinReport.twig', ['form' => $form->createView(), 'tabR' => $res]);
        }

        return $app['twig']->render('report/subbasinReport.twig', ['form' => $form->createView()]);
    } else {

        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('report-areas');





$app->match('/actions/{ids}/fiche-synthese', function ($ids, Request $request) use ($app) {



    if (!verifySingleObjectType(explode(',', $ids), $app)) {

        $erreur['message'] = 'Ces fiches sysma_action concernent plusieurs types d\'objets';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    } else {




        $pdo = $app['pdo'];
        $qry = $pdo->prepare("select * from " . SCHEMA . ".sysma_action t where t.sysma_action_id in (" . $ids . ")");
        $qry->execute();

        if ($qry->rowCount() === 0) {

            $erreur['message'] = 'Aucune fiche sysma_action ne correspond aux identifiants.';
            return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
        } else {

            // privilege_list, basés sur le premier sysma_action
            $t0 = $qry->fetch();
            $T0 = new SysmaAction($t0['sysma_action_id'], $app);
            $TT0 = new SysmaActionType($T0->sysma_action_type_id, $app);

            if (hasAccess($app, $app['session']->get('user_id'), null, $TT0->sysma_object_type_id, null, $T0->sysma_action_type_id, null, null, 'lecteur')) {

                $c = 0;
                $d = 0;
                $tab = $typeT = $tabObjets = $tabDonneesObjets = $listeOG = $listeIdOG = $PhAvant = $PhApres = array();


                $qry->execute();
                while ($t = $qry->fetchObject()) {

                    $T = new SysmaAction($t->sysma_action_id, $app);
                    $T->loadActualInfosSysmaAction();
                    $T->loadSysmaObjects();
                    $T->loadSysmaActionInfos();

                    if (count($T->sysmaObjectsArray) === 0) {

                        $erreur['message'] = 'Aucun objet lié à cette fiche sysma_action.';
                        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
                    }


                    // liste des objets geo pour pouvoir obtenir une géographie fusionnée et croiser avec les communes
                    foreach ($T->sysmaObjectsArray as $O) {
                        $O->loadSysmaObjectInfos();
                        $O->loadActualInfosSysmaObject();
                        $O->loadGeoInfos($O->infos->geometry_type);
                        $O->loadPhotoObjects(null);

                        $listeIdOG[] = $O->id;
                        $listeOG[] = $O;
                    }

                    $tab[] = $T;
                }



                // communes intersectant les objets géo concernés

                $qry2 = $pdo->prepare('Select ST_Union(o.geom) as geo from ' . SCHEMA . '.sysma_object o where sysma_object_id in (' . implode(',', $listeIdOG) . ')');
                $qry2->execute();
                $res = $qry2->fetchObject();
                $com = findIntersectingObjects(REF_PG_SCHEMA_TABLE_CITIES, $res->geo, array('tampon' => 20, 'object' => REF_PG_SCHEMA_TABLE_CITIES_NAME, 'id' => REF_PG_SCHEMA_TABLE_CITIES_GID), $app);

                return $app['twig']->render('report/sysmaActionSheetReport.twig', ['tabT' => $tab, 'tabO' => $listeOG, 'tabCommunes' => $com, 'GET' => $request->query, 'BASE_LAYERS' => loadBaseLayers($app, ($app['session']->get('user_id') != ANONYMID)), 'OTHER_BASE_LAYERS' => loadOtherBaseLayers($app, ($app['session']->get('user_id') != ANONYMID))]);
            } else {

                $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
                return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
            }
        }
    }
})->bind('report-work');
