<?php

use Symfony\Component\HttpFoundation\Request;

$app->match('/infos-cadastre', function (Request $request) use ($app) {


    if ($app['session']->get('has_cadastre_access')) {

        // TODO make a function
        $coords = substr($request->request->get('mouse'), 7, -1);
        $tabC = explode(',', $coords);
        $pdo = $app['pdo'];
        $i = 'select ST_Transform(ST_setSRID(ST_Point(' . $tabC[1] . ',' . $tabC[0] . '),4326),2154) as  point';
        $qry = $pdo->prepare($i);
        $qry->execute();
        $res = $qry->fetchObject();
        $buffer = 1;
        $parcelles = findCadastreObjectInfos($app, $res->point, $buffer);
        $cadastreVersionAnnee = findCadastreVersionAnnee($app);


        return $app['twig']->render('cadastre/cadastreInfos.twig', ['parcelles' => $parcelles, 'x' => trim($tabC[1]), 'y' => trim($tabC[0])
            , 'buffer' => $buffer
            , 'cadastreVersionAnnee' => $cadastreVersionAnnee]);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('/error/error.twig', ['erreur' => $erreur]);
    }
})->bind('infos-cadastre');




$app->match('/infos-cadastre-geo/{x}/{y}/{buffer}', function ($x, $y, $buffer) use ($app) {

    if ($app['session']->get('has_cadastre_access')) {
        return '<script>
        
        if (myCadastreLayer !== undefined) { map.removeLayer(myCadastreLayer); }

        var myCadastreLayer = new L.GeoJSON.AJAX("/geojson/infos-cadastre/' . $x . '/' . $y . '/' . $buffer . '", 
            { 
                id: "098",
                name: "098",
                style:{ "color" :"#ff9933", "opacity" :"1", "weight" :"2", "fillOpacity" :"0.2",  }, 
                onEachFeature: popUp,   
                
            }
            
        );
        groupCad.addLayer(myCadastreLayer) ;
        </script>';
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('erreur.twig', ['erreur' => $erreur]);
    }
})->bind('infos-cadastre-geo');
