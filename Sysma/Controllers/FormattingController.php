<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

$app->match('/style/{object_type}/{id_type}', function (Request $request, $object_type, $id_type) use ($app) {

    if ($object_type == 'sysma_object') {
        $Type = new SysmaObjectType($id_type, $app);
    } elseif ($object_type == 'sysma_action') {
        $Type = new SysmaActionType($id_type, $app);
    }

    return $app['twig']->render('formatting/thematicAnalysisStyleForm.twig', ['fromThesaurus' => $request->request->get('from-thesaurus'), 'S' => new Style($object_type, $id_type, $app['session']->get('user_id'), $app), 'object_type' => $object_type, 'id_type' => $id_type, 'Type' => $Type]);
})->bind('style');


$app->match('/style/{object_type}/{id_type}/update', function (Request $request, $object_type, $id_type) use ($app) {


    //var_dump($request->request->get('color')) ;


    if ($object_type == 'sysma_object') {
        $suff = '001';
    } elseif ($object_type == 'sysma_action') {
        $suff = '002';
    }

    $object_type == 'sysma_object' ? $ot = 'objecttype' : $ot = 'actiontype';

    $S = new Style($object_type, $id_type, $app['session']->get('user_id'), $app);

    $r = $request->request->all();


    if ($S->update(json_encode($r))) {
        // refreshLayer(/*sendData("","' . $app['url_generator']->generate('layermanager') . '","infos",null);
        return "<p>Style modifié</p><script>refreshLayer('$ot',$id_type, null, null, {'center' : L." . $app['session']->get('default_center') . ", 'zoom' : " . $app['session']->get('default_zoom') . " });</script>";
    } else {
        return 'KO';
    }
})->bind('style-update');


$app->match('/style/{object_type}/{id_type}/delete', function ($object_type, $id_type) use ($app) {


    if ($object_type == 'sysma_object') {
        $suff = '001';
    } elseif ($object_type == 'sysma_action') {
        $suff = '002';
    }

    $S = new Style($object_type, $id_type, $app['session']->get('user_id'), $app);

    if ($S->delete()) {
        return '<p>Style par défaut appliqué</p><script>/*map._layers[' . $id_type . $suff . '].refresh();*/sendData("","' . $app['url_generator']->generate('layermanager') . '","infos",null);</script>';
    } else {
        return 'KO';
    }
})->bind('style-delete');


//////////////
// THEMATIC ANALYSIS
//////////////


$app->match('/formatting/type/{object_type}/{id_type}', function ($object_type, $id_type) use ($app) {

    return $app['twig']->render('formatting/thematicAnalysisSheet.twig', ['listeAT' => thematicAnalysisList($object_type, $id_type, $app['session']->get('user_id'), $app), 'object_type' => $object_type, 'id_type' => $id_type, 'tabAT' => userThematicAnalysis($app, $app['session']->get('user_id'))]);
})->bind('formatting');


$app->match('/formatting/{theme_analysis_id}', function (Request $request, $theme_analysis_id) use ($app) {

    if ($theme_analysis_id != 0) {
        $AT = new ThematicAnalysis($theme_analysis_id, $app);


        return $app['twig']->render('formatting/thematicAnalysisDetailsSheet.twig', ['AT' => $AT, 'hideActivateButton' => $request->query->get('hideActivateButton'), 'tabAT' => userThematicAnalysis($app, $app['session']->get('user_id'))]);
    } else {
        return 'Choisissez une analyse dans la liste ou bien créez en une.';
    }
})->bind('formatting-details')->assert('theme_analysis_id', '\d+');



$app->match('/formatting/{theme_analysis_id}/activate', function ($theme_analysis_id) use ($app) {

    $AT = new ThematicAnalysis($theme_analysis_id, $app);

    if ($AT->activate($app['session']->get('user_id'))) {
        // reloadSessionUser($app);

        $type_layer = explode('_',$AT->object_type)[1].'type' ;
    

        return '<p><span class="glyphicon glyphicon-ok"></span> Analyse ' . $theme_analysis_id . ' associée à la layer</p><script>refreshLayer(\''.$type_layer.'\', ' . $AT->type_id . ');</script>';
    } else {
        return 'Erreur';
    }
})->bind('formatting-activate');


$app->match('/formatting/{theme_analysis_id}/desactivate', function ($theme_analysis_id) use ($app) {

    $AT = new ThematicAnalysis($theme_analysis_id, $app);
    $AT->desactivate($app['session']->get('user_id'));
    $type_layer = explode('_',$AT->object_type)[1].'type' ;
    // reloadSessionUser($app);
    return '<p><span class="glyphicon glyphicon-ok"></span> Analyse désactivée</p><script>refreshLayer(\''.$type_layer.'\', ' . $AT->type_id . ');</script>';
})->bind('formatting-desactivate');


$app->match('/formatting/type/{object_type}/{id_type}/{theme_analysis_id}', function ($object_type, $id_type, $theme_analysis_id, Request $request) use ($app) {

    if ($theme_analysis_id === 'nouveau') {
        $data = array();
    } else {
        $AT = new ThematicAnalysis($theme_analysis_id, $app);
        $data = $AT;
    }


    $form = $app['form.factory']->createBuilder('form', $data)
        ->add('at', 'text', array('label' => 'Titre', 'constraints' => new Assert\NotBlank()))
        ->add('parameter_id', 'choice', array('choices' => parametersList($object_type, $id_type, $app)))
        ->add('object_type', 'hidden', array('data' => $object_type))
        ->add('type', 'choice', array('choices' => array('gradient' => 'Gradient', 'values' => 'Valeurs', 'classes' => 'Classes'), 'constraints' => new Assert\NotBlank()))
        ->add('id_type', 'hidden', array('data' => $id_type))
        ->add('styles', 'textarea', array('value' => get_object_vars($data->styles), 'constraints' => new Assert\NotBlank()))
        ->getForm();
    $form->handleRequest($request);


    if ($form->isValid()) {

        $data = $form->getData();

        $AT = new ThematicAnalysis(null, $app);
        $id = $AT->insert($data);
    }

    return $app['twig']->render('formatting/thematicAnalysisModificationForm.twig', ['AT' => null, 'form' => $form->createView(), 'object_type' => $object_type, 'id_type' => $id_type]);
})->bind('formatting-create');


$app->match('/formatting/type/{object_type}/{id_type}/create/commit', function ($object_type, $id_type, Request $request) use ($app) {

    $AT = new ThematicAnalysis(null, $app);
    $id = $AT->insert($request->request->all());
    $id = json_decode($id);

    return 'theme_analysis_id=' . $id->{'id'};
})->bind('formatting-create-commit');



$app->match('/icon/search', function (Request $request) use ($app) {

    $icons = [];
    $search = $request->request->get('search');
    if (!defined('SVG_PATHS')) {
        define('SVG_PATHS', '/icon');
    }
    $paths = explode(',', SVG_PATHS);
    foreach ($paths as $path) {

        if (substr($path, 0, 1) === '/') {
            $completePath = __DIR__ . '/../../public' . $path;
        } else if (filter_var($path, FILTER_VALIDATE_URL)) {
            $completePath = $path;
        } else {
            return 'Wrong SVG Path ! ' . $path;
        }

        $icons[$path] = loadIcons($completePath, $search);
    }

    return $app['twig']->render('formatting/iconSearchList.twig', ['icons' => $icons, 'search' => $search]);
})->bind('icon-search');
