<?php

$app->match('/help', function () use ($app) {

    return $app['twig']->render('help/help.twig');
})->bind('help');
