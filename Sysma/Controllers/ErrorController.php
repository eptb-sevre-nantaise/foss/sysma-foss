<?php

use Symfony\Component\HttpFoundation\Request;

$app->post('/error', function (Request $request) use ($app) {

    $r = $request->request->all();
    if (DEBUG) {
        $msg = $r['message'] . ' <br>Détails : ' . $r['data'];
    } else {
        $msg = $r['message'];
    }

    return '<div id="errorDivMessage">'
        . '<div class="text-danger">'
        . '<p class="text-right">'
        . '<span class="glyphicon glyphicon-remove" onclick="$(\'#errorDiv\').html(\'\');"></span></p>'
        . '<p> Erreur : ' . $msg . ' </p>'
        . '</div>';
})->bind('error');
