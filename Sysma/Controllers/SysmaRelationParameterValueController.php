<?php

use Symfony\Component\HttpFoundation\Request;

$app->match('/value-parameter-sysma-relation', function (Request $request) use ($app) {

    if ($request->request->get('data_index') != null) {
        $V = new SysmaRelationParameterValue($request->request->get('data_index'), $app);
    } else {
        $V = new SysmaRelationParameterValue(null, $app);
        $V->buildFromParameters($request->request->get('sysma_relation_id'), $request->request->get('parameter_id'), $app);
    }

    $T = new SysmaRelation($V->sysma_relation_id, $app);
    $TT = new SysmaRelationType($T->sysma_relation_type_id, $app);
    $O = new SysmaObject($request->request->get('sysma_object_id'), $app);

    if (
        hasAccess($app, $app['session']->get('user_id'), null, $request->request->get('sysma_object_type_id'), null, $T->sysma_relation_type_id, $T->status, $T->organisation_id, 'lecteur')
        or (hasAccess($app, $app['session']->get('user_id'), null, $request->request->get('sysma_object_type_id'), null, $T->sysma_relation_type_id, $T->status, $T->organisation_id, 'contributeur') and $T->modified_by == $app['session']->get('user_id'))
    ) {


        return $app['twig']->render('sysma-relation-parameter-value/sysmaRelationParameterValueDisplay.twig', ['dataType' => $request->request->get('data_type'), 'id' => $request->request->get('sysma_relation_id'), 'parameter_id' => $request->request->get('parameter_id'), 'V' => $V, 'TT' => $TT, 'T' => $T, 'O' => $O, 'sysma_object_id' => $O->sysma_object_id, 'sysma_object_type_id' => $O->sysma_object_type_id]);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cet objet';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('sysma-relation-parameter-value');




$app->match('/value-parameter-sysma-relation/update', function (Request $request) use ($app) {

    $V = new SysmaRelationParameterValue($request->request->get('data_index'), $app);

    $T = new SysmaRelation($V->sysma_relation_id, $app);
    $TT = new SysmaRelationType($T->sysma_relation_type_id, $app);
    if (
        hasAccess($app, $app['session']->get('user_id'), null, $request->request->get('sysma_object_type_id'), null, $T->sysma_relation_type_id, $T->status, $T->organisation_id, 'gestionnaire')
        or (hasAccess($app, $app['session']->get('user_id'), null, $request->request->get('sysma_object_type_id'), null, $T->sysma_relation_type_id, $T->status, $T->organisation_id, 'contributeur') and $T->modified_by == $app['session']->get('user_id'))
    ) {

        $V->loadChoices($request->request->get('data_type'));

        return $app['twig']->render('sysma-relation-parameter-value/sysmaRelationParameterValueModificationForm.twig', ['dataType' => $request->request->get('data_type'), 'sysma_object_id' => $request->request->get('sysma_object_id'), 'sysma_object_type_id' => $request->request->get('sysma_object_type_id'), 'id' => $request->request->get('sysma_relation_id'), 'parameter_id' => $request->request->get('parameter_id'), 'V' => $V]);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cet objet';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('sysma-relation-parameter-value-update');





$app->match('/value-parameter-sysma-relation/update/commit', function (Request $request) use ($app) {


    $request->request->set('modified_by', $app['session']->get('user_id'));
    $request->request->set('user_id', $app['session']->get('user_id'));
    $request->request->set('modified_at', date('Y-m-d H:i:s'));
    $r = $request->request;
    $V = new SysmaRelationParameterValue($r->get('data_index'), $app);

    $T = new SysmaRelation($V->sysma_relation_id, $app);
    $TT = new SysmaRelationType($T->sysma_relation_type_id, $app);
    if (
        hasAccess($app, $app['session']->get('user_id'), null, $request->request->get('sysma_object_type_id'), null, $T->sysma_relation_type_id, $T->status, $T->organisation_id, 'gestionnaire')
        or (hasAccess($app, $app['session']->get('user_id'), null, $request->request->get('sysma_object_type_id'), null, $T->sysma_relation_type_id, $T->status, $T->organisation_id, 'contributeur') and $T->modified_by == $app['session']->get('user_id'))
    ) {

        $r->remove('sysma_object_type_id');
        return $V->modify($r);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cet objet';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('sysma-relation-parameter-value-update-commit');



$app->match('/value-parameter-multiple-sysma-relation/update/commit', function (Request $request) use ($app) {

    $request->request->set('modified_by', $app['session']->get('user_id'));
    $request->request->set('user_id', $app['session']->get('user_id'));
    $request->request->set('created_at', date('Y-m-d H:i:s'));

    $r = $request->request;
    $V = new SysmaRelationParameterValue($r->get('data_index'), $app);

    $T = new SysmaRelation($V->sysma_relation_id, $app);
    $TT = new SysmaRelationType($T->sysma_relation_type_id, $app);
    if (
        hasAccess($app, $app['session']->get('user_id'), null, $request->request->get('sysma_object_type_id'), null, $T->sysma_relation_type_id, $T->status, $T->organisation_id, 'gestionnaire')
        or (hasAccess($app, $app['session']->get('user_id'), null, $request->request->get('sysma_object_type_id'), null, $T->sysma_relation_type_id, $T->status, $T->organisation_id, 'contributeur') and $T->modified_by == $app['session']->get('user_id'))
    ) {

        $r->remove('sysma_object_type_id');
        return $V->multipleUpdate($r);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cet objet';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('sysma-relation-parameter-multiple-value-update-commit');







$app->match('/value-parameter-sysma-relation/create', function (Request $request) use ($app) {



    $V = new SysmaRelationParameterValue(null, $app);
    $V->buildFromParameters($request->request->get('sysma_relation_id'), $request->request->get('parameter_id'), $app);
    //$V->buildFromParameters(100317, 100446, $app);

    $T = new SysmaRelation($V->sysma_relation_id, $app);
    $TT = new SysmaRelationType($T->sysma_relation_type_id, $app);
    if (
        hasAccess($app, $app['session']->get('user_id'), null, $request->request->get('sysma_object_type_id'), null, $T->sysma_relation_type_id, $T->status, $T->organisation_id, 'gestionnaire')
        or (hasAccess($app, $app['session']->get('user_id'), null, $request->request->get('sysma_object_type_id'), null, $T->sysma_relation_type_id, $T->status, $T->organisation_id, 'contributeur') and $T->modified_by == $app['session']->get('user_id'))
    ) {

        $V->loadChoices($request->request->get('data_type'));
        $V->loadChoices('multipleTextChoiceType');

        return $app['twig']->render('sysma-relation-parameter-value/sysmaRelationParameterValueFillForm.twig', ['dataType' => $request->request->get('data_type'), 'sysma_object_id' => $request->request->get('sysma_object_id'), 'sysma_object_type_id' => $request->request->get('sysma_object_type_id'), 'id' => $request->request->get('sysma_relation_id'), 'parameter_id' => $request->request->get('parameter_id'), 'V' => $V, 'indexDonnee' => dataIndex($app['pdo'])]);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cet objet';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('sysma-relation-parameter-value-create');



$app->match('/value-parameter-sysma-relation/create/commit', function (Request $request) use ($app) {

    $request->request->set('modified_by', $app['session']->get('user_id'));
    $request->request->set('created_at', date('Y-m-d H:i:s'));
    $request->request->set('user_id', $app['session']->get('user_id'));
    $r = $request->request;
    $V = new SysmaRelationParameterValue(null);
    $V->buildFromParameters($request->request->get('sysma_relation_id'), $request->request->get('parameter_id'), $app);
    $T = new SysmaRelation($V->sysma_relation_id, $app);
    $TT = new SysmaRelationType($T->sysma_relation_type_id, $app);
    if (
        hasAccess($app, $app['session']->get('user_id'), null, $request->request->get('sysma_object_type_id'), null, $T->sysma_relation_type_id, $T->status, $T->organisation_id, 'gestionnaire')
        or (hasAccess($app, $app['session']->get('user_id'), null, $request->request->get('sysma_object_type_id'), null, $T->sysma_relation_type_id, $T->status, $T->organisation_id, 'contributeur') and $T->modified_by == $app['session']->get('user_id'))
    ) {

        $r->remove('sysma_object_type_id');

        return $V->create($r);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cet objet';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('sysma-relation-parameter-value-create-commit');


$app->match('/value-parameter-multiple-sysma-relation/create/commit', function (Request $request) use ($app) {

    $request->request->set('modified_by', $app['session']->get('user_id'));
    $request->request->set('created_at', date('Y-m-d H:i:s'));
    $request->request->set('user_id', $app['session']->get('user_id'));
    $r = $request->request;
    $V = new SysmaRelationParameterValue(null, $app);
    $V->buildFromParameters($request->request->get('sysma_relation_id'), $request->request->get('parameter_id'), $app);

    $T = new SysmaRelation($V->sysma_relation_id, $app);
    $TT = new SysmaRelationType($T->sysma_relation_type_id, $app);
    if (
        hasAccess($app, $app['session']->get('user_id'), null, $request->request->get('sysma_object_type_id'), null, $T->sysma_relation_type_id, $T->status, $T->organisation_id, 'gestionnaire')
        or (hasAccess($app, $app['session']->get('user_id'), null, $TT->sysma_object_type_id, null, $T->sysma_relation_type_id, $T->status, $T->organisation_id, 'contributeur') and $T->modified_by == $app['session']->get('user_id'))
    ) {

        $V->loadChoices($V->parameter->dataType);
        $res = null;

        foreach ($V->choicesArray as $choice) {

            $inputName = 'choice' . $choice['choice_id'];
            if ($r->get($inputName) == 1) {

                $r2['user_id'] = $r->get('user_id');
                $r2['parameter_id'] = $r->get('parameter_id');
                $r2['sysma_relation_id'] = $r->get('sysma_relation_id');
                $r2['created_at'] = $r->get('created_at');
                $r2['data_index'] = $r->get('data_index');
                $r2['value'] = $choice['choice'];
                $r2['user_id'] = $r->get('user_id');
                $r2['modified_by'] = $r->get('modified_by');

                $res = $V->create($r2);
            }
        }

        return $res; // derniere insertion
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cet objet';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('sysma-relation-parameter-multiple-value-create-commit');
