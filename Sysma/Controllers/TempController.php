<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Validator\Constraints as Assert;

//////////////////////////////////////////////////////
// MODELES DE CODES POUR IMPORT ET MANIP DE DONNEES // 
//////////////////////////////////////////////////////


/* import de données tabulaires, station déjà importées via impotSIG sysma */

$app->get('/import_donnees_onde', function (Request $request) use ($app) {

    die();

    $q = 'select * from _agent_srenou.t_onde_dgallard_pour_import onde    
    inner join sysma.sysma_object o on o.sysma_object = onde.cours_eau||\' - \'||onde.nom';

    $qry = $app['pdo']->prepare($q);
    $qry->execute();
    while ($res = $qry->fetchObject()) {

        $data['modified_by'] = 1;
        $data['created_at'] = date('Y-m-d H:i:s');
        $data['modified_at'] = date('Y-m-d H:i:s');
        $data['sysma_object_id'] = $res->sysma_object_id;
        $data['parameter_id'] = 1017440;
        $data['value'] = $res->lib_onde;
        $data['data_index'] = dataIndex($app['pdo']);
        $data['start_date'] = substr($res->date, 6, 4) . '-' . substr($res->date, 3, 2) . '-' . substr($res->date, 0, 2);



        $V = new SysmaObjectParameterValue(null);
        $V->buildFromParameters($res->sysma_object_id, 1017440, null, $app);

        $V->create($data);

        // die('test');    

    }
});


/* suite import données tabulaires, gestion des dates de fin */

$app->get('/import_donnees_onde2', function (Request $request) use ($app) {

    die();

    $q = 'select * from sysma.sysma_object_data where parameter_id = 1017440 and modified_by = 1 and  track_historytables_id > 729218 and end_date = \'0\' order by sysma_object_id asc, start_date asc';
    $qry = $app['pdo']->prepare($q);
    $qry->execute();
    while ($res = $qry->fetchObject()) {

        echo 'ici';

        $q2 = 'select sysma_object_data_id,sysma_object_id,value,start_date from sysma.sysma_object_data where
         parameter_id = 1017440 
         and modified_by = 1 
         and  track_historytables_id > 729218 
         and end_date = \'0\' 
         and sysma_object_id = :id   
         and start_date > :dt      
         order by start_date asc LIMIT 1';


        $qry2 = $app['pdo']->prepare($q2);
        $qry2->bindParam(':id', $res->sysma_object_id);
        $qry2->bindParam(':dt', $res->start_date);
        $qry2->execute();

        if ($qry2->rowCount() > 0) {

            $res2 = $qry2->fetchObject();

            echo 'next - > SOBDATA ' . $res->sysma_object_data_id . ' - SOB ' . $res2->sysma_object_id . ' - ' . $res2->value . '<br>';

            $q3 = 'update sysma.sysma_object_data set end_date = :dt2 where sysma_object_data_id = :id2';
            $qry3 = $app['pdo']->prepare($q3);
            $qry3->bindParam(':dt2', $res2->start_date);
            $qry3->bindParam(':id2', $res->sysma_object_data_id);

            $qry3->execute();

            //   die() ;

        }
    }
});


$app->get('/compl_dico_dig', function (Request $request) use ($app) {

    die();

    //158530,158614
    //$tab = [315285,315286,315287,315288,158641,415828,415837,415838,415839,415841,415842,158646,158647,324203,324204,324205,158685,158686,315290,315291,315292,315293,315294,315295,315302,315305,315304] ;
    // $tab = [315303,315304] ;
    //$tab = [158614,158530] ;

    // infos pour sysma_action DIG
    //$tab = [315285,315286,315287,315288,158641,415828,415837,415838,415839,415841,415842,158646,158647,324203,324204,324205,158685,158686,315290,315291,315292,315293,315294,315295,315302,315303,315305,315304] ;

    $tab = [5, 3, 4, 6, 8, 9, 11, 7, 10];

    foreach ($tab as $t) {
        echo $t . '<br>';
        // $P = new SysmaObjectParameter($t,$app) ;
        $P = new SysmaActionParameter($t, $app);
        $p = get_object_vars($P);
        $tabC = $p['tabChoix'];
        var_dump($p);

        $p2['sysma_action_type_id'] = 971523;
        $p2['parameter_id'] = dataIndex($app['pdo']);
        $p2['parameter'] = $p['parameter'];
        $p2['description'] = $p['description'];
        $p2['data_type'] = $p['dataType'];
        $p2['ordered_by'] = '1000' . $p['ordered_by'];
        $p2['start_date'] = date('Y-m-d');
        $p2['modified_at'] = date('Y-m-d');
        $p2['parameter_source'] = $p['sourceparameter'];
        $p2['source_donnees'] = $p['data_source'];
        $p2['unit'] = $p['unit'];
        $p2['parameter_alias'] = $p['colSIG'];
        $p2['required'] = $p['required'];
        $p2['modified_by'] = 1;

        $newP = new SysmaActionParameter(null, $app);
        $newId = $newP->create($p2);

        $idtab = json_decode($newId, true);
        $id = $idtab['id'];

        $thatNewP = new SysmaActionParameter($id, $app);

        foreach ($tabC as $cle => $C) {

            $c2['choice_id'] = dataIndex($app['pdo']);
            $c2['choice'] = $C['nom'];
            $c2['description'] = $C['description'];
            $c2['parameter_id'] = $id;
            $c2['order'] = $cle;
            $c2['modified_by'] = 1;
            $c2['value_type'] = 'choice';
            $thatNewP->createChoice($c2);
        }

        // die() ;

    }
});

$app->get('/split_worksheets', function (Request $request) use ($app) {

    die();

    $qry = 'select 
t.sysma_action_id,
count(o.sysma_object_id),
string_agg(o.sysma_object_id::text,\',\') as liste_sysma_object_id,
count(distinct(me.*)) as compte_me,
string_agg(distinct(me.nom_me_court),\',\') as noms_me
from sysma.sysma_action t 
inner join sysma.l_sysma_object_sysma_action l on l.sysma_action_id = t.sysma_action_id 
inner join sysma.sysma_object o on o.sysma_object_id = l.sysma_object_id
inner join m010_etat_ecologique.t_me_eptbsn_terrprio me on st_within(o.geom,me.geom) and me.retenu_analyse_terrprio is true
where t.contracts = \'{CT EAU (2021-2026)}\'
group by t.sysma_action_id
having count(distinct(me.*)) > 1';

    $sttmt = $app['pdo.export']->query($qry);
    while ($FT = $sttmt->fetchObject()) {

        echo $FT->sysma_action_id . '<br>';
        $T = new SysmaAction($FT->sysma_action_id, $app);

        $tabO = explode(',', $FT->liste_sysma_object_id);

        foreach ($tabO as $sysma_object_id) {

            echo ' -- ' . $sysma_object_id . '<br>';
            $O = new SysmaObject($sysma_object_id, $app);

            $T2 = new SysmaAction(null, $app);


            $data['sysma_action_type_id'] = $T->sysma_action_type_id;
            $data['start_date'] = $T->dateDebut;
            $data['end_date'] = $T->end_date;
            $data['created_by'] = $T->idCreateur;
            $data['status'] = $T->status;
            $data['modified_by'] = $T->modified_by;
            $data['created_at'] = $T->creation_dateTravaux;
            $data['modified_at'] = $T->modification_dateTravaux;
            $data['program_year'] = $T->anneeProg;
            $data['organisation_id'] = $T->organisation_id;
            $data['unit_cost_thesaurus'] = $T->unit_cost_thesaurus;
            $data['unit_cost_estimated_user'] = $T->unit_cost_estimated_user;
            $data['unit_measure_estimated_user'] = $T->unit_measure_estimated_user;
            $data['total_cost_estimated_user'] = $T->total_cost_estimated_user;

            var_dump($data);

            $res = $T2->create($data);
            $res = json_decode($res);
            if ($res->status == 'success') {
                $O->linkSysmaActionSheet($res->id, 1);
                $O->unlinkSysmaActionSheet($FT->sysma_action_id);
            } else {
                die('erreur creation fiche travayx');
            }
        }

        $T->delete();
    }
})->bind('');


$app->get('/import_resilience_salmo', function (Request $request) use ($app) {

    die();

    $q = 'select * from "_agent_srenou".ind_resilience_ruisseau_salmo';
    $st = $app['pdo.export']->query($q);
    $st->execute();
    while ($ir = $st->fetchObject()) {

        echo $ir->id_sequences . ' - ';

        $q2 = 'select * from sysma.sysma_object_data where value = \'' . $ir->id_sequences . '\' and parameter_id = 158508';
        $st2 = $app['pdo.export']->query($q2);
        $st2->execute();
        $seq = $st2->fetchObject();
        echo $seq->sysma_object_id . '<br>';
        $V = new SysmaObjectParameterValue(null, $app);
        $data['sysma_object_id'] = $seq->sysma_object_id;
        $data['parameter_id'] = 158695;
        $data['value'] = $ir->indiceresilience;
        $data['start_date'] = '2019-10-08';
        $data['end_date'] = '0';
        $data['user_id'] = 1;
        $data['modified_by'] = 1;
        $data['data_index'] = dataIndex($app['pdo']);

        $res = $V->create($data);
        $res = json_decode($res);
        if ($res->status != 'success')
            die();
    }
})->bind('');





$app->get('/import_profil_ds_seq_salmo', function (Request $request) use ($app) {


    die();

    $q = 'select s.id, s."Id_Sequences" as ids,p."PT_Lpb" as lpb, p."PT_Lbase" as lb,p."PT_Htotal" as h, '
        . 'p."Surface_Vu" as surf, p."Perimetre_Vu" as perim, p."Rayon_hydraulique_Vu" as rh, p."Debit_Vu" as debit, p."Puissance_Vu" as puis, '
        . 'st_buffer(st_centroid(s.geom),1) as centre from _agent_srenou.t_hc_salmo_seq_t2bv_20191007_tt s '
        . 'left join _agent_srenou.t_hc_salmo_profils_20191008 p on s.id = p.id';
    $st = $app['pdo.export']->query($q);
    $st->execute();

    while ($s = $st->fetchObject()) {

        echo $s->ids . " - lpb" . $s->lpb . ' - lb' . $s->lb . ' - h' . $s->h . '  - surf' . $s->surf . ' - perim' . $s->perim . ' - rh' . $s->rh . ' - debit' . $s->debit . ' - puis' . $s->puis;

        // recherche de la sequence sysma


        $q2 = "select o.sysma_object_id from sysma.sysma_object o where st_within(st_centroid(o.geom), '" . $s->centre . "') and o.sysma_object_type_id = 158507";
        // echo $q2 ;
        $st2 = $app['pdo.export']->query($q2);
        $st2->execute();
        $s2 = $st2->fetchObject();


        echo $s2->sysma_object_id . '<br>';


        $V = new SysmaObjectParameterValue(null, $app);
        $data['sysma_object_id'] = $s2->sysma_object_id;
        $data['parameter_id'] = 324200;
        $data['value'] = $s->lpb;
        $data['start_date'] = '2019-10-09';
        $data['end_date'] = '0';
        $data['user_id'] = 1;
        $data['modified_by'] = 1;
        $data['data_index'] = dataIndex($app['pdo']);

        $res = $V->create($data);
        /*
        $res = json_decode($res);
        if ($res->status != 'success')
         die();*/



        $V = new SysmaObjectParameterValue(null, $app);

        $data['parameter_id'] = 324201;
        $data['value'] = $s->lb;
        $data['data_index'] = dataIndex($app['pdo']);

        $res = $V->create($data);
        /*
        $res = json_decode($res);
        if ($res->status != 'success')
         //   die();*/


        $V = new SysmaObjectParameterValue(null, $app);

        $data['parameter_id'] = 324202;
        $data['value'] = $s->h;
        $data['data_index'] = dataIndex($app['pdo']);

        $res = $V->create($data);/*
        $res = json_decode($res);
        if ($res->status != 'success')
          //  die();*/



        $V = new SysmaObjectParameterValue(null, $app);

        $data['parameter_id'] = 324203;
        $data['value'] = $s->surf;
        $data['data_index'] = dataIndex($app['pdo']);

        $res = $V->create($data);/*
        $res = json_decode($res);
        if ($res->status != 'success')
          //  die();*/



        $V = new SysmaObjectParameterValue(null, $app);

        $data['parameter_id'] = 324204;
        $data['value'] = $s->perim;
        $data['data_index'] = dataIndex($app['pdo']);

        $res = $V->create($data);/*
        $res = json_decode($res);
        if ($res->status != 'success')
          //  die();*/


        $V = new SysmaObjectParameterValue(null, $app);

        $data['parameter_id'] = 324205;
        $data['value'] = $s->rh;
        $data['data_index'] = dataIndex($app['pdo']);

        $res = $V->create($data);/*
        $res = json_decode($res);
        if ($res->status != 'success')
          //  die();*/



        $V = new SysmaObjectParameterValue(null, $app);

        $data['parameter_id'] = 158685;
        $data['value'] = $s->debit;
        $data['data_index'] = dataIndex($app['pdo']);

        $res = $V->create($data);/*
        $res = json_decode($res);
        if ($res->status != 'success')
         //   die();*/




        $V = new SysmaObjectParameterValue(null, $app);

        $data['parameter_id'] = 158686;
        $data['value'] = $s->puis;
        $data['data_index'] = dataIndex($app['pdo']);

        $res = $V->create($data);/*
        $res = json_decode($res);
        if ($res->status != 'success')
          //  die();*/



        // die();


        /*
          echo $ir->id_sequences . ' - ';

          $q2 = 'select * from sysma.sysma_object_data where value = \'' . $ir->id_sequences . '\' and parameter_id = 158508';
          $st2 = $app['pdo.export']->query($q2);
          $st2->execute();
          $seq = $st2->fetchObject();
          echo $seq->sysma_object_id . '<br>';
          $V = new SysmaObjectParameterValue(null, $app);
          $data['sysma_object_id'] = $seq->sysma_object_id;
          $data['parameter_id'] = 158695;
          $data['value'] = $ir->indiceresilience;
          $data['start_date'] = '2019-10-08';
          $data['end_date'] = '0';
          $data['user_id'] = 1;
          $data['modified_by'] = 1;
          $data['data_index'] = dataIndex($app['pdo']);

          $res = $V->create($data);
          $res = json_decode($res);
          if ($res->status != 'success')
          die(); */
    }
})->bind('');




$app->get('/fiche_sysma_action_plandeau', function (Request $request) use ($app) {

    die();

    $q = 'with bv as 
(
select geom from m010_etat_ecologique.t_me_eptbsn_terrprio me 
where me.nom_me_court in (\'La Grande Maine amont\')

)/*, 
seq as 
(
select st_union(st_buffer(geom,100)) as geom from sysma_layers.t2bv_seq
),
reh as 
(
select st_union(st_buffer(geom,100)) as geom from sysma_layers.segment_reh o
where ((o.modified_by >= 100000000 and o.modified_by != ANONYMID) or o.modified_by =3056) and o.end_date = \'0\'
)*/

select o.sysma_object_id, o.geom from 
sysma_layers.plan_d_eau o, bv /*,reh, seq */ where 1=1
and o.end_date = \'0\'
and o.sysma_object_id != 18403
and ((conn_terra is not null and conn_terra = \'sur cours d eau\') or ((conn_terra is null or conn_terra = \'doute\') and conn_sig is true))
and (st_within(o.geom,bv.geom))/* and ( st_within(o.geom,seq.geom) or st_within(o.geom, reh.geom ) )*/';

    $st = $app['pdo.export']->query($q);
    $st->execute();
    while ($o = $st->fetchObject()) {

        echo $o->sysma_object_id . ' - ';
        $O = new SysmaObject($o->sysma_object_id, $app);
        /*
          $O->loadSysmaActions();

          foreach ($O->actionsArray as $T) {
          echo $T->sysma_action . ' - ' . $T->status . ' - ' . $T->contracts_labels . ' - ' . $T->modified_by . '<br>';

          if ($T->status == 'programme') {
          // $O->unlinkSysmaActionSheet($T->id);
          }
          }
          echo '<br>';
         */

        $O->linkSysmaActionSheet(907793, 1);
    }
})->bind('');



$app->get('/fiche_sysma_action_drains', function (Request $request) use ($app) {


    die();

    $q = '
            select * from sysma.sysma_object where sysma_object_id in (
        391835,380470,375712,484425,484728,484837,485267,485728,485089,500292,434003,499944,611495,498485,499562,498045,498308
)
             ';

    $st = $app['pdo.export']->query($q);
    $st->execute();
    while ($o = $st->fetchObject()) {

        $O = new SysmaObject($o->sysma_object_id, $app);
        echo $O->id . '<br>';
        $O->linkSysmaActionSheet(907789, 1);
    }

    /*
      $O->loadSysmaActions();

      foreach ($O->actionsArray as $T) {
      echo $T->sysma_action . ' - ' . $T->status . ' - ' . $T->contracts_labels . ' - ' . $T->modified_by . '<br>';

      if ($T->status == 'programme') {
      // $O->unlinkSysmaActionSheet($T->id);
      }
      }
      echo '<br>';
     */
})->bind('');

$app->get('/fiche_sysma_action_obst_suppr', function (Request $request) use ($app) {

    die();

    $tab = [626218, 619311, 619270, 619267, 619266, 619263, 618471, 618131, 616366, 616122, 616037, 418456, 403840, 402244, 385953, 364796, 355934, 345837, 345748, 328451];
    foreach ($tab as $t) {

        $T = new SysmaAction($t, $app);
        $T->loadSysmaObjects();
        foreach ($T->sysmaObjectsArray as $O) {
            $errors = 0;

            if ($O->unlinkSysmaActionSheet($t)) {
            } else {
                $errors++;
            }

            if ($errors == 0) {
                if ($T->delete()) {
                    //   return json_encode(['status' => 'success']);
                } else {
                    //    return json_encode(['status' => 'error']);
                }
            } else {
                // return json_encode(['status' => 'error']);
            }
        }
    }
})->bind('');




$app->get('/fiche_sysma_action_petite_contitnuite', function (Request $request) use ($app) {

    die();
    $q = '
        with bv as 
(
select * from sysma_layers.zone_selection where sysma_object_id = 908623 
)
select o.sysma_object_id,o.typeouvrag from sysma_layers.obstacle_ecoulement o, bv where 
st_within(o.geom, bv.geom)
and o.sysma_object_id not in (25432,19694 ,20124 ,25416) ';

    $st = $app['pdo.export']->query($q);
    $st->execute();
    while ($o = $st->fetchObject()) {

        if ($o->typeouvrag != 'digue et bonde d etang') {

            $O = new SysmaObject($o->sysma_object_id, $app);
            $O->loadSysmaActions();
            if (count($O->actionsArray) == 0) {
                echo $o->sysma_object_id . ' ' . $o->typeouvrag . ' - LIEN<br> ';
                $O->linkSysmaActionSheet(908637, 1);
            } else {
                echo $o->sysma_object_id . ' avec sysma_action <br> ';
                foreach ($O->actionsArray as $T) {
                    echo $T->typeTravaux . '<br>';
                    if (
                        $T->typeTravaux != 'Installation d&#039;une rampe'
                        and $T->typeTravaux != 'Remplacement par buse type PEHD'
                        and $T->typeTravaux != 'Effacement'
                        and $T->typeTravaux != 'Effacement partiel'
                        and $T->typeTravaux != 'Remplacement par pont cadre'
                    ) {
                        echo $o->sysma_object_id . ' ' . $o->typeouvrag . ' LIEN<br> ';
                        echo $T->typeTravaux . ' - ' . $o->typeouvrag . ' - ' . $T->status . ' - ' . $T->contracts_labels . '<br>';
                        $O->linkSysmaActionSheet(908637, 1);
                    }
                }
            }
        }

        /*
          $O->loadSysmaActions();

          foreach ($O->actionsArray as $T) {
          echo $T->sysma_action . ' - ' . $T->status . ' - ' . $T->contracts_labels . ' - ' . $T->modified_by . '<br>';

          if ($T->status == 'programme') {
          // $O->unlinkSysmaActionSheet($T->id);
          }
          }
          echo '<br>';
         */

        // $O->linkSysmaActionSheet(908567, 1);
    }
})->bind('');


$app->get('/import_sysma_action_salmo', function (Request $request) use ($app) {

    die();

    $q = ' select * from ext_hydroconcept_01.t_sysma_action_all t where etatlieux != \'Plan d&#039;eau\' and sysma_object_id != 296312  ';

    $st = $app['pdo.export']->query($q);
    $st->execute();
    while ($t = $st->fetchObject()) {

        $W = new SysmaAction(null, $app);

        $index = dataIndex($app['pdo']);

        $data['sysma_action_type_id'] = $t->sysma_action_type_id;
        $data['sysma_action'] = $t->sysma_action;
        $data['start_date'] = '2021-01';
        $data['end_date'] = '2023-12';
        $data['organisation_id'] = 1;
        $data['created_by'] = 1;
        $data['program_year'] = '2021';
        $data['status'] = 'programme';
        $data['created_at'] = date('Y-m-d H:i:s');
        $data['modified_by'] = 1;
        $data['total_cost_estimated_user'] = $t->cout;
        $data['unit_measure_estimated_user'] = 'object';
        $data['sysma_action_id'] = $index;


        var_dump($data);


        $res = json_decode($W->create($data));

        var_dump($res);

        $W2 = new SysmaAction($index, $app);
        $W2->addContract('CT EAU (2021-2026)');


        $O = new SysmaObject($t->sysma_object_id, $app);
        $O->linkSysmaActionSheet($index, 1);
    }
})->bind('');

/*
 * permet de créer dans sysma un lien vers les photos de piwigo contenus dans un dossier et ayant le même nom que le nom d'objet (ici pour les séquences de t2bv)
 */

$app->get('/import_photos_hydroconcept', function (Request $request) use ($app) {

    die();

    $q = "select * from images where path like  './galleries/dossiers_photos/srenou/2019_10_14_ruisseaux_salmonicoles_required_field_parameter_tete_bassin_hydroconcept%'";
    $qry = $app['pdo.pwg']->prepare($q);
    $qry->execute();
    while ($res = $qry->fetchObject()) {

        echo $res->name . ' - ';

        $q2 = "select sysma_object_id from sysma.sysma_object where sysma_object= '" . $res->name . "' and sysma_object_type_id = 158507 ";
        $qry2 = $app['pdo']->prepare($q2);
        $qry2->execute();
        $res2 = $qry2->fetchAll();
        if (count($res2) > 1) {
            var_dump($res2);
            die('2 objet du même nom');
        } else {

            $objet = $res2[0];
            echo $objet['sysma_object_id'] . ' OK<br>';
            $data = ['sysma_object_id' => $objet['sysma_object_id'], 'photo_id' => $res->id, 'type' => 'photo', 'order' => 1, 'index' => dataIndex($app['pdo'])];
            $P = new PhotoObject(null, $app);
            $P->create($app, $data);
        }
    }
})->bind('');




/*
 * lien avec les photos étude PE
 * ex de chemin ./galleries/dossiers_photos/athomas/2022_06_01_Etude_Plans_eau_Salmo_Blanc_ECOLIMNEAU/11979/Vue_etang_aval_vers_amont.jpg
 */

$app->get('/import_photos_ecolimneau', function (Request $request) use ($app) {

    die();
    $q = "select * from images where path like  './galleries/dossiers_photos/athomas/2022_06_01_Etude_Plans_eau_Salmo_Blanc_ECOLIMNEAU%' order by id ASC";
    $qry = $app['pdo.pwg']->prepare($q);
    $qry->execute();
    while ($res = $qry->fetchObject()) {

        echo $res->name . ' - ';

        // get sysma_object_id

        $thePath = explode('/', $res->path);
        $sysma_object_id = intval($thePath[5]);

        echo $sysma_object_id . ' - ';

        // objet sysma existe ?

        //   var_dump(intval($sysma_object_id)) ;


        $q2 = "select sysma_object_id from sysma.sysma_object where sysma_object_id= '" . $sysma_object_id . "' and sysma_object_type_id = 45 ";
        $qry2 = $app['pdo']->prepare($q2);
        $qry2->execute();
        $res2 = $qry2->fetchAll();
        if (count($res2) == 0) {
            var_dump($res2);
            die('aucun objet avec ID ' . $sysma_object_id);
        } else {



            $theOrder = 2;

            $data = ['sysma_object_id' => $sysma_object_id, 'photo_id' => $res->id, 'photo_type' => 'photo', 'display_order' => $theOrder, 'photo_object_id' => dataIndex($app['pdo'])];
            $P = new PhotoObject(null, $app);
            $P->create($app, $data);
            echo 'ok';
            //echo 'A insérer';
        }

        echo '<br>';
    }
})->bind('');


/* remontée de photo "Vue" en ordre 1 */

$app->get('/import_photos_ecolimneau_tri', function (Request $request) use ($app) {

    die();

    $q = "select * from sysma.l_sysma_object_sysma_action where sysma_action_id in (982230,982232)";
    $qry = $app['pdo']->prepare($q);
    $qry->execute();
    while ($res = $qry->fetchObject()) {

        $q2 = 'select * from sysma.photo_object where sysma_object_id = ' . $res->sysma_object_id . ' and photo_type = \'photo\' ';
        $qry2 = $app['pdo']->prepare($q2);
        $qry2->execute();
        while ($res2 = $qry2->fetchObject()) {


            $q3 = "select * from images where id = " . $res2->photo_id;
            $qry3 = $app['pdo.pwg']->prepare($q3);
            $qry3->execute();
            $res3 = $qry3->fetchObject();
            echo $res3->name;

            var_dump(strpos($res3->name, 'Vue'));
            if (strpos($res3->name, 'Vue') === 0) {
                echo ' <u>avec Vue</u>';

                $q4 = 'update sysma.photo_object set display_order = 1 where photo_object_id = ' . $res2->photo_object_id;
                $qry4 = $app['pdo']->prepare($q4);
                $qry4->execute();
            } else {
            }

            echo '<br>';
        }
    }
})->bind('');


/* passage de paramètres en fiches travaux */

$app->get('/import_photos_ecolimneau_fichetravaux', function (Request $request) use ($app) {


    die();

    $q = "select sysma_object_id from sysma.l_sysma_object_sysma_action where sysma_action_id in (982230,982232)";
    $qry = $app['pdo']->prepare($q);
    $qry->execute();
    while ($res = $qry->fetchObject()) {


        echo 'Objet ' . $res->sysma_object_id . ' - ';

        $O = new SysmaObject($res->sysma_object_id, $app);
        $O->loadActualInfosSysmaObjectWithIndex();

        if ($O->dataIndex['insp_terr']->value == '1') {

            // création action visite terrain

            $A = new SysmaAction(null, $app);
            $data = [
                'sysma_action_type_id' => 982966,
                'sysma_action' => 'Inspection terrain Ecolimneau 2022',
                'start_date' => '2022-04',
                'end_date' => '2022-04',
                'organisation_id' => 1,
                'created_by' => 999999998,
                'program_year' => 2022,
                'status' => 'termine',
                'contracts' => '{CT EAU (2021-2023)}'
            ];

            $resCreated = json_decode($A->create($data));

            $O->linkSysmaActionSheet($resCreated->id, 999999998);
        }


        if ($O->dataIndex['rep_questi']->value == '1') {

            // questionnaire

            $A = new SysmaAction(null, $app);
            $data = [
                'sysma_action_type_id' => 982759,
                'sysma_action' => 'Questionnaire Ecolimneau 2022',
                'start_date' => '2022-04',
                'end_date' => '2022-04',
                'organisation_id' => 1,
                'created_by' => 999999998,
                'program_year' => 2022,
                'status' => 'termine',
                'contracts' => '{CT EAU (2021-2023)}'
            ];

            $resCreated = json_decode($A->create($data));

            $O->linkSysmaActionSheet($resCreated->id, 999999998);


            $V = new SysmaActionParameterValue(null, $app);

            $data = [
                'sysma_action_id' => $resCreated->id,
                'parameter_id' => 982957,
                'value' => $O->dataIndex['trav_prevu']->value,
                'user_id' => 999999998,
                'data_index' => dataIndex($app['pdo'])
            ];

            $V->create($data);


            $V = new SysmaActionParameterValue(null, $app);

            $data = [
                'sysma_action_id' => $resCreated->id,
                'parameter_id' => 982961,
                'value' => $O->dataIndex['ok_amenag']->value,
                'user_id' => 999999998,
                'data_index' => dataIndex($app['pdo'])
            ];

            $V->create($data);



            $V = new SysmaActionParameterValue(null, $app);

            $data = [
                'sysma_action_id' => $resCreated->id,
                'parameter_id' => 982965,
                'value' => $O->dataIndex['acc_res_et']->value,
                'user_id' => 999999998,
                'data_index' => dataIndex($app['pdo'])
            ];

            $V->create($data);
        }
    }
})->bind('');



/* PROG CT 24- 26 EPTB SN */

/* ajout du contrat CT 24-26 aux fiches actions du PA 21-26 restant à faire */
/* principe : sur Blanc, salmo, GM, (trezon ?) on ne garde que les FA sur les zones non terminés (hors zones de sélection = CT21-23_travaux_termines )
   on update ces FA avec <CT24-26>
   */



/* travaux CT21-26 programme hors zone des travaux terminés */


$app->get('/travail-ct-2426', function (Request $request) use ($app) {

    die();


    // sélection de la prog dans une masse d'eau et hors seteur terminés
    $q = "

with zone_termine as
(
select 
st_union(st_makevalid(o.geom)) as geom
from  sysma.sysma_object o  
inner join sysma.sysma_object_data d on d.sysma_object_id = o.sysma_object_id
where 
d.parameter_id = 120706 
and d.value = 'CT21-23_travaux_termines'
and sysma_object_type_id = 115998
)


select 

a.sysma_action_id,
o.sysma_object_id

from zone_termine, sysma.sysma_object o 

inner join sysma.l_sysma_object_sysma_action l on l.sysma_object_id = o.sysma_object_id
inner join sysma.sysma_action a on a.sysma_action_id = l.sysma_action_id 
inner join sysma.sysma_action_type aty on aty.sysma_action_type_id = a.sysma_action_type_id
inner join \"__selection_secteur\".t_secteurs me on st_within(o.geom,me.geom)


where 

'PA 2021-2026' = ANY(a.contracts) 
and a.status = 'programme'
and not st_within(o.geom, zone_termine.geom)
and a.sysma_action_type_id not in (82 /* pas etude plan d'eau */, 83 /* travaux à définir sur plans d'eau*/ )
and me.categorie IN ('HORS_PRIO','ME_PRIO')
and me.objetgeo = 'ME_PRIO Le Blanc'

";




    // sélection de la prog dans une masse d'eau dans des secteurs précis
    $q = "
/*
with zone_prog as
(
select 
st_union(st_makevalid(o.geom)) as geom
from  sysma.sysma_object o  
inner join sysma.sysma_object_data d on d.sysma_object_id = o.sysma_object_id
where 
d.parameter_id = 120706 
and d.value in ('CT24-26')
and sysma_object_type_id = 115998
)
*/

select 

a.sysma_action_id,
o.sysma_object_id
/*,

a.*, 
aty.sysma_action_type, 
me.objetgeo as me,
o.geom 
*/
from /*zone_prog, */ sysma.sysma_object o 

inner join sysma.l_sysma_object_sysma_action l on l.sysma_object_id = o.sysma_object_id
inner join sysma.sysma_action a on a.sysma_action_id = l.sysma_action_id 
inner join sysma.sysma_action_type aty on aty.sysma_action_type_id = a.sysma_action_type_id
inner join \"__selection_secteur\".t_secteurs me on st_within(o.geom,me.geom)


where 

'PA 2021-2026' = ANY(a.contracts) 
and a.status = 'programme'
--and st_within(o.geom, zone_prog.geom)
and a.sysma_action_type_id not in (82 /* pas etude plan d'eau */, 83 /* travaux à définir sur plans d'eau*/ )
-- and a.sysma_action_type_id in (94)
--and me.categorie IN ('HORS_PRIO','ME_PRIO')
and me.objetgeo in ('T5', 'T3')

";


    /*

    $qry = $app['pdo']->prepare($q);
    $qry->execute();
    while ($res = $qry->fetchObject()) {
*//*
        $A = new SysmaAction($res->sysma_action_id, $app);
        $O = new SysmaObject($res->sysma_object_id, $app) ;
        echo $O->sysma_object_id.' ('.$O->sysma_object_type.') - ';
        echo $A->sysma_action_id.' ('.$A->sysma_action_type.') -';
        echo $A->contracts ;
        echo '<br>' ;
*/
    //supprimer contrat ajouté par erreur
    //  $A->delete() ;




    // création d'une nouvelle FA
    /*
        $NFA = new SysmaAction(null, $app);
        $data['sysma_action_type_id'] = $A->sysma_action_type_id;
        $data['sysma_action'] = $A->sysma_action;
        $data['start_date'] = '2024-01-01' ;
        $data['end_date'] = '2026-01-01' ;
        $data['program_year'] = 2024 ;        
        $data['unit_cost_estimated_user'] = $A->unit_cost_estimated_user ;
        $data['unit_measure_estimated_user'] = $A->unit_measure_estimated_user ;
        if (in_array($A->sysma_action_type_id, [80,        27,        907757,        979205,        30,        907766,        35,        36,        31])) {
        $data['total_cost_estimated_user'] = $A->total_cost_estimated_user ;        
        $data['total_cost_final_user'] = $A->total_cost_final_user ;
        } else {
            $data['total_cost_estimated_user'] = 0 ;
            $data['total_cost_final_user'] = 0 ;
        }
        $data['status'] = $A->status ;
        $data['organisation_id'] = $A->organisation_id ;
        $data['contracts'] = '{CT EAU (2024-2026)}' ;
        $data['created_by'] = 1;
        $myFA = json_decode($NFA->create($data)) ;

        $O->linkSysmaActionSheet($myFA->id,1);
*/


    // die();
    /*
    }*/
})->bind('');



// suppression de FA 24-26 sur certaines zones de sélection
/*
$app->get('/travail-ct-2426-2', function (Request $request) use ($app) {


    $q = "select a.* from sysma.sysma_object o 
    inner join sysma.sysma_object z on st_within(o.geom, z.geom)
    inner join sysma.l_sysma_object_sysma_action l on l.sysma_object_id = o.sysma_object_id
    inner join sysma.sysma_action a on l.sysma_action_id = a.sysma_action_id
    where 
    z.sysma_object_id = 1017931
    and 'CT EAU (2024-2026)' = ANY(a.contracts)    
    ";

    $qry = $app['pdo']->prepare($q);
    $qry->execute();
    while ($res = $qry->fetchObject()) {

        $A = new SysmaAction($res->sysma_action_id,$app) ;

        echo $A->sysma_action_id.' - '.$A->sysma_action.' - '.$A->contracts.' '.$A->program_year.' - '.$A->total_cost_estimated_user.'<br>' ;

       $A->delete() ;

    }        


})->bind('');*/



// creation de FA 24-26 sur certaines zones de sélection

$app->get('/travail-ct-2426-3', function (Request $request) use ($app) {

    die();

    $q = "select a.*, o.sysma_object_id from sysma.sysma_object o 
    inner join sysma.sysma_object z on st_within(o.geom, z.geom)
    inner join sysma.l_sysma_object_sysma_action l on l.sysma_object_id = o.sysma_object_id
    inner join sysma.sysma_action a on l.sysma_action_id = a.sysma_action_id
    where 
    z.sysma_object_id = 1017934
    and 'PA 2021-2026' = ANY(a.contracts)    
    ";

    $qry = $app['pdo']->prepare($q);
    $qry->execute();
    while ($res = $qry->fetchObject()) {

        $A = new SysmaAction($res->sysma_action_id, $app);
        $O = new SysmaObject($res->sysma_object_id, $app);


        $NFA = new SysmaAction(null, $app);
        $data['sysma_action_type_id'] = $A->sysma_action_type_id;
        $data['sysma_action'] = $A->sysma_action;
        $data['start_date'] = '2024-01-01';
        $data['end_date'] = '2026-01-01';
        $data['program_year'] = 2024;
        $data['unit_cost_estimated_user'] = $A->unit_cost_estimated_user;
        $data['unit_measure_estimated_user'] = $A->unit_measure_estimated_user;

        $data['total_cost_estimated_user'] = $A->total_cost_estimated_user;
        $data['total_cost_final_user'] = $A->total_cost_final_user;

        // ne garder que les chiffrages morpho 
        /*
        if (in_array($A->sysma_action_type_id, [80,        27,        907757,        979205,        30,        907766,        35,        36,        31])) {
        $data['total_cost_estimated_user'] = $A->total_cost_estimated_user ;        
        $data['total_cost_final_user'] = $A->total_cost_final_user ;
        } else {
            $data['total_cost_estimated_user'] = 0 ;
            $data['total_cost_final_user'] = 0 ;
        }
        */
        $data['status'] = $A->status;
        $data['organisation_id'] = $A->organisation_id;
        $data['contracts'] = '{CT EAU (2024-2026)}';
        $data['created_by'] = 1;


        var_dump($data);

        $myFA = json_decode($NFA->create($data));

        $O->linkSysmaActionSheet($myFA->id, 1);


        echo $O->sysma_object_id . ' - ' . $A->sysma_action_id . ' - ' . $A->sysma_action . ' - ' . $A->contracts . ' ' . $A->program_year . ' - ' . $A->total_cost_estimated_user . '<br>';
        //die() ;


    }
})->bind('');



// création des séquences de t2bv

$app->get('/travail-smib-t2bv', function (Request $request) use ($app) {


    die();
    $q = 'select * from sysma_temp_import."_hardy_litmineur_berge_annexeshydrau"  where methode = \'Tête de bassin versant\' order by id asc';




    $qry = $app['pdo']->prepare($q);
    $qry->execute();
    while ($r = $qry->fetchObject()) {

        // création de l'objet

        $data['sysma_object'] = $r->id_segment;
        $data['sysma_object_type_id'] = 158507;
        $data['start_date'] = '2024-04-03';
        $data['end_date'] = '0';
        $data['organisation_id'] = 3;
        $data['created_by'] = 2;
        $data['modified_by'] = 2;
        $data['status'] = 'existant';
        $data['created_at'] = date('Y-m-d- H:i:s');
        $data['modified_at'] = date('Y-m-d- H:i:s');
        $data['geom'] = $r->geom_simple;

        //  var_dump($data) ;
        //die() ;   

        $O = new SysmaObject(null, $app);
        $newObj = json_decode($O->create($data));
        echo 'nouvel objet créé ' . $newObj->id . '<br>';

        // update de l'id
        $q2 = "update sysma_temp_import.\"_hardy_litmineur_berge_annexeshydrau\" set sysma_object_id = :id where id = :id0";
        $qry2 = $app['pdo']->prepare($q2);
        $qry2->bindParam(':id', $newObj->id);
        $qry2->bindParam(':id0', $r->id);
        if ($qry2->execute()) {
            echo 'update id OK<br>';
        } else {
            die('erreur update ID');
        }
        //die() ;


    }
})->bind('');





// renseignement des seq t2bv

$app->get('/travail-smib-t2bv-2', function (Request $request) use ($app) {

    die() ;
    $q = 'select * from sysma_temp_import."_hardy_litmineur_berge_annexeshydrau"  where methode = \'Tête de bassin versant\' and sysma_object_id is not null order by id asc';




    $qry = $app['pdo']->prepare($q);
    $qry->execute();
    while ($r = $qry->fetchObject()) {

        echo '<h1>ID' . $r->id . ' - ' . $r->sysma_object_id . '</h1><br>';

        $O = new SysmaObject($r->sysma_object_id, $app);
        var_dump($O->sysmaObjectId);
        if ($O->sysmaObjectId !== null) {

            // correspondance paramètre sysma colonne hardy
            /*
        $t['id_segment'] = 'id_seq' ;
        $t['id_tronc'] = 'h_id_tronc' ;
        $t['type_ce'] = 'type_res' ;
        $t['position_t'] = 'talwegpos' ;
        $t['forme_vall'] = 'talwegform' ;
        $t['niveau_enc'] = 'nivencaiss' ;
        $t['type_ecoul'] = 'ecoul_niv' ;
        $t['facies_eco'] = 'facies_eco' ;
        $t['largeur_pb'] = 'larg_p_b' ;
        $t['largeur_lm'] = 'larg_base' ;
        $t['cl_evo_mor'] = 'classe_cem' ;
        $t['larg_banq'] = 'larg_banqu' ;
        $t['profil_tra'] = 'homogprofi' ;
        $t['h_tot_m'] = 'haut_tot' ;
        $t['potappsol'] = 'pot_ap_sol' ;
        $t['emprise_d'] = 'empr_dispo' ;
        $t['sinuosite'] = 'sinuo_type' ;
        $t['granul_dom'] = 'granul_dom' ;
        $t['granul_acc'] = 'granul_acc' ;
        $t['colmatage'] = 'colmatage' ;
        $t['pres_hab'] = 'qtite_hab' ;
        $t['rug_elmnat'] = 'h_ruglm' ;
        $t['rug_elmveg'] = 'h_ruglmv' ;
        $t['recalib'] = 'h_recal' ;
        $t['rectif'] = 'h_rectif' ; 
       // $t['deplac_lit'] = 'h_depl_lit' ;
        $t['curage'] = 'h_curage' ;
        $t['b_materiau'] = 'berg_mat' ;
        $t['b_erodabil'] = 'berg_eroda' ;
        $t['b_erosion'] = 'berg_erosi' ;
        $t['n_erosion'] = 'h_nat_eros' ;
        $t['b_protect'] = 'berg_protec' ;
        $t['n_protect'] = 'h_prot_ber' ;
        $t['b_clotur'] = 'h_presclot' ;
        $t['b_pietinem'] = 'brrg_pieti' ;
        $t['b_pietinem'] = 'brrd_pieti' ;
        $t['rg_hber_pb'] = 'h_htb_rg' ;
        $t['rg_clitmaj'] = 'h_conlmjrg' ;
        $t['rg_con_rip'] = 'h_contryrg' ;
        $t['rg_epa_rip'] = 'h_epryrg' ;
        $t['rg_nat_veg'] = 'h_natryrg' ;
        $t['rg_especes'] = 'h_espryrg' ;
        $t['rg_talus'] = 'h_talus_rg' ;
        $t['occsol_rg0'] = 'brrg_ods5' ;
        $t['occsol_rg5'] = 'brrg_ods10' ;
        $t['rg_resp_znt'] = 'brrg_znt' ;
        $t['rd_hber_pb'] = 'h_htbergrd' ;
        $t['rd_clitmaj'] = 'h_conlmjrd' ;
        $t['rd_con_rip'] = 'h_contryrd' ;
        $t['rd_epa_rip'] = 'h_epryrd' ;
        $t['rd_nat_veg'] = 'h_natryrd' ;
        $t['rd_especes'] = 'h_espryrd' ;
        $t['rd_talus'] = 'h_talus_rd' ;
        $t['occsol_rd0'] = 'brrd_ods5' ;
        $t['occsol_rd5'] = 'brrd_ods10' ;
        $t['rd_resp_znt'] = 'brrd_znt' ;
        $t['com_lm'] = 'h_infolit' ;
        $t['com_beripi'] = 'h_infoberg' ;
        $t['lgr_m'] = 'h_lg_m';
        $t['ind1_rug'] = 'indice_rug' ;
        $t['ind2_ratio'] = 'h_ratiofor' ;
        $t['ind4_pui_s'] = 'puis_spe' ;
        $t['ind_arti'] = 'iartif_not' ;
        $t['ind6_0_5'] = 'h_iprebr05' ;
        $t['ind6_5_10'] = 'h_iprbr510' ;
        $t['ind_res'] = 'i_resil' ;
        $t['type_arti'] = 'h_typartif' ;
        $t['nom_me'] = 'h_masseau' ;
        $t['preco_lm'] = 'h_preco_lm' ;
        $t['preco_br'] = 'h_preco_be' ;
        $t['preco_rha'] = 'h_preco_ri' ;
*/

            $t['rg_resp_znt'] = 'brrg_znt';
            $t['rd_resp_znt'] = 'brrd_znt';



            foreach ($t as $col => $alias) {

                if ($r->$col !== null) {

                    $P = new SysmaObjectParameter(null, $app);

                    if (!$P->constructByAlias($alias, 158507, $app)) {
                        die('no parameter');
                    }


                    $V = new SysmaObjectParameterValue(null, $app);

                    $data['sysma_object_id'] = $r->sysma_object_id;
                    $data['parameter_id'] = $P->parameter_id;
                    $data['start_date'] = '2023-04-03';
                    $data['end_date'] = 0;
                    $data['value'] = $r->$col;
                    $data['user_id'] = 2;
                    $data['modified_by'] = 2;
                    $data['created_at'] = date('Y-m-d- H:i:s');
                    $data['modified_at'] = date('Y-m-d- H:i:s');
                    $data['data_index'] = dataIndex($app['pdo']);

                    $res = json_decode($V->create($data));
                    var_dump($res);
                    //  die() ;

                }
            }

            // die() ;

        }
    }
})->bind('');





// création des segments REH

$app->get('/travail-smib-reh-creation', function (Request $request) use ($app) {


    die() ;
    $q = 'select * from sysma_temp_import."_hardy_litmineur_berge_annexeshydrau"  where methode = \'REH\' order by id asc';

    // $q = 'select * from sysma_temp_import."_hardy_litmineur_berge_annexeshydrau"  where sysma_object_id = 10568798' ;
    //  10568798



    $qry = $app['pdo']->prepare($q);
    $qry->execute();
    while ($r = $qry->fetchObject()) {

        // création de l'objet

        $data['sysma_object'] = $r->id_segment;
        $data['sysma_object_type_id'] = 10568681;
        $data['start_date'] = '2024-04-03';
        $data['end_date'] = '0';
        $data['organisation_id'] = 3;
        $data['created_by'] = 2;
        $data['modified_by'] = 2;
        $data['status'] = 'existant';
        $data['created_at'] = date('Y-m-d- H:i:s');
        $data['modified_at'] = date('Y-m-d- H:i:s');
        $data['geom'] = $r->geom_simple;

        //  var_dump($data) ;
        //die() ;   

        $O = new SysmaObject(null, $app);
        $newObj = json_decode($O->create($data));
        echo 'nouvel objet créé ' . $newObj->id . '<br>';

        // update de l'id
        $q2 = "update sysma_temp_import.\"_hardy_litmineur_berge_annexeshydrau\" set sysma_object_id = :id where id = :id0";
        $qry2 = $app['pdo']->prepare($q2);
        $qry2->bindParam(':id', $newObj->id);
        $qry2->bindParam(':id0', $r->id);
        if ($qry2->execute()) {
            echo 'update id OK<br>';
        } else {
            die('erreur update ID');
        }
        //die() ;


    }
})->bind('');







// renseignement des seq t2bv

$app->get('/travail-smib-reh-data', function (Request $request) use ($app) {

    die() ;
    $q = 'select * from sysma_temp_import."_hardy_litmineur_berge_annexeshydrau"  where methode = \'Tête de bassin versant\' and sysma_object_id is not null and act_an_lm is not null order by id asc';

    // $q = 'select * from sysma_temp_import."_hardy_litmineur_berge_annexeshydrau"  where methode = \'REH\' and sysma_object_id = 10569306' ;



    $qry = $app['pdo']->prepare($q);
    $qry->execute();
    while ($r = $qry->fetchObject()) {

        echo '<h1>ID' . $r->id . ' - ' . $r->sysma_object_id . '</h1><br>';


        $O = new SysmaObject($r->sysma_object_id, $app);
        var_dump($O->sysmaObjectId);
        if ($O->sysmaObjectId !== null) {

            // correspondance paramètre sysma colonne hardy

            $t['act_an_lm'] = 'act_an_lm';
            /*
        $t['id_segment'] = 'id_seg' ;
        $t['id_tronc'] = 'id_tronc' ;
        $t['type_ce'] = 'type_res' ;
        $t['position_t'] = 'talwegpos' ;
        $t['forme_vall'] = 'talwegform' ;
        $t['niveau_enc'] = 'nivencaiss' ;
        $t['type_ecoul'] = 'ecoul_niv' ;
        $t['facies_eco'] = 'facies_eco' ;
        $t['largeur_pb'] = 'larg_p_b' ;
        $t['largeur_lm'] = 'larg_base' ;
        $t['cl_evo_mor'] = 'classe_cem' ;
        $t['larg_banq'] = 'larg_banqu' ;
        $t['profil_tra'] = 'homogprofi' ;
        $t['h_tot_m'] = 'haut_tot' ;
        $t['potappsol'] = 'pot_ap_sol' ;
        $t['emprise_d'] = 'empr_dispo' ;
        $t['sinuosite'] = 'sinuo_type' ;
        $t['granul_dom'] = 'granul_dom' ;
        $t['granul_acc'] = 'granul_acc' ;
        $t['colmatage'] = 'colmatage' ;
        $t['pres_hab'] = 'qtite_hab' ;
        $t['rug_elmnat'] = 'h_ruglm' ;
        $t['rug_elmveg'] = 'h_ruglmv' ;
        $t['recalib'] = 'h_recal' ;
        $t['rectif'] = 'h_rectif' ; 
       // $t['deplac_lit'] = 'h_depl_lit' ;
        $t['curage'] = 'h_curage' ;
        $t['b_materiau'] = 'berg_mat' ;
        $t['b_erodabil'] = 'berg_eroda' ;
        $t['b_erosion'] = 'berg_erosi' ;
        $t['n_erosion'] = 'h_nat_eros' ;
        $t['b_protect'] = 'berg_protec' ;
        $t['n_protect'] = 'h_prot_ber' ;
        $t['b_clotur'] = 'h_presclot' ;
        $t['b_pietinem'] = 'h_piet' ;
       // $t['b_pietinem'] = 'brrd_pieti' ;
        $t['rg_hber_pb'] = 'h_htb_rg' ;
        $t['rg_clitmaj'] = 'h_conlmjrg' ;
        $t['rg_con_rip'] = 'h_contryrg' ;
        $t['rg_epa_rip'] = 'h_epryrg' ;
        $t['rg_nat_veg'] = 'h_natryrg' ;
        $t['rg_especes'] = 'h_espryrg' ;
        $t['rg_talus'] = 'h_talus_rg' ;
        $t['occsol_rg0'] = 'brrg_ods5' ;
        $t['occsol_rg5'] = 'brrg_ods10' ;
        $t['rg_resp_znt'] = 'brrg_znt' ;
        $t['rd_hber_pb'] = 'h_htbergrd' ;
        $t['rd_clitmaj'] = 'h_conlmjrd' ;
        $t['rd_con_rip'] = 'h_contryrd' ;
        $t['rd_epa_rip'] = 'h_epryrd' ;
        $t['rd_nat_veg'] = 'h_natryrd' ;
        $t['rd_especes'] = 'h_espryrd' ;
        $t['rd_talus'] = 'h_talus_rd' ;
        $t['occsol_rd0'] = 'brrd_ods5' ;
        $t['occsol_rd5'] = 'brrd_ods10' ;
        $t['rd_resp_znt'] = 'brrd_znt' ;
        $t['com_lm'] = 'h_infolit' ;
        $t['com_beripi'] = 'h_infoberg' ;
        $t['lgr_m'] = 'h_lg_m';



        $t['reh_litmin'] = 'altc_lit';
        $t['reh_annexe'] = 'altc_annexe';
        $t['reh_berges'] = 'altc_berg';
        $t['reh_cholo'] = 'altc_cholo';
        $t['reh_camph'] = 'altc_camph';
        $t['reh_lignea'] = 'altc_lignd';
        $t['reh_debit'] = 'altc_debi';





       
        $t['nom_me'] = 'h_masseau' ;
        $t['preco_lm'] = 'h_preco_lm' ;
        $t['preco_br'] = 'h_preco_be' ;
        $t['preco_rha'] = 'h_preco_ri' ;
*/


            foreach ($t as $col => $alias) {

                if ($r->$col !== null) {

                    $P = new SysmaObjectParameter(null, $app);

                    if (!$P->constructByAlias($alias, 10568681, $app)) {
                        echo $alias;
                        die('no parameter');
                    }


                    $V = new SysmaObjectParameterValue(null, $app);

                    $data['sysma_object_id'] = $r->sysma_object_id;
                    $data['parameter_id'] = $P->parameter_id;
                    $data['start_date'] = '2023-04-03';
                    $data['end_date'] = 0;
                    $data['value'] = $r->$col;
                    $data['user_id'] = 2;
                    $data['modified_by'] = 2;
                    $data['created_at'] = date('Y-m-d- H:i:s');
                    $data['modified_at'] = date('Y-m-d- H:i:s');
                    $data['data_index'] = dataIndex($app['pdo']);

                    $res = json_decode($V->create($data));
                    var_dump($res);
                    die();
                }
            }
        }

        //  die() ;


    }
})->bind('');





$app->get('/travail-smib-creation-actions', function (Request $request) use ($app) {

    die();

    $r = $request->query->all();
    $id = $r['id'];




    $t[$id] = ['type_action' => 10568789, 'annee_action' => 10704348, 'info_action' => 10704350];

    $q = 'select d.value, o.sysma_object_type_id, d.sysma_object_id from sysma.sysma_object_data d inner join sysma.sysma_object o on d.sysma_object_id = o.sysma_object_id  where d.parameter_id = :id';
    $qry = $app['pdo']->prepare($q);
    $qry->bindParam(':id', $t[$id]['type_action']);
    $qry->execute();
    while ($res = $qry->fetchObject()) {

        echo $res->value . ' - ';

        $V = new SysmaObjectParameterValue(null, $app);
        $V->buildFromParameters($res->sysma_object_id, $t[$id]['annee_action'], null, $app);
        if ($V->value == "0" or $V->value == '') {
            $annee_prog = 2024;
            $start_date = '2024-01-01';
            $end_date = '2029-12-31';
        } else {
            $annee_prog = $V->value + 2023;
            $start_date = $annee_prog . '-01-01';
            $end_date = $annee_prog . '-12-31';
        }


        echo ' - ';
        // action sysma

        $q2 = 'select * from sysma.sysma_action_type where sysma_action_type = :at and sysma_object_type_id = :ot';
        $qry2 = $app['pdo']->prepare($q2);
        $qry2->bindParam(':at', $res->value);
        $qry2->bindParam(':ot', $res->sysma_object_type_id);
        $qry2->execute();
        $res2 = $qry2->fetchObject();

        echo $res2->sysma_action_type . '<br>';

        $A = new SysmaAction(null, $app);

        $data = [
            'sysma_action' => $res2->sysma_action_type,
            'sysma_action_type_id' => $res2->sysma_action_type_id,
            'start_date' => $start_date,
            'end_date' =>  $end_date,
            'organisation_id' => 3,
            'created_by' => 2,
            'program_year' => $annee_prog,
            'status' => 'programme',
            'contracts' => '{Prog Evre-Thau-St Denis 2024-2029}'
        ];


        $resA = json_decode($A->create($data));

        var_dump($resA);

        $O = new SysmaObject($res->sysma_object_id, $app);

        $O->linkSysmaActionSheet($resA->id, 2);

        echo $res->sysma_object_id;


        // die() ;

    }
})->bind('');



$app->get('/travail-smib-creation-actions-reh-lm', function (Request $request) use ($app) {


    die() ;

    $q = 'select * from sysma_temp_import._hardy_litmineur_berge_annexeshydrau where methode = \'REH\' and action_lm is not null';
    $qry = $app['pdo']->prepare($q);
    $qry->execute();
    while ($res = $qry->fetchObject()) {


        if ($res->act_an_lm == "0" or $res->act_an_lm == '') {
            $annee_prog = 2024;
            $start_date = '2024-01-01';
            $end_date = '2029-12-31';
        } else {
            $annee_prog = $res->act_an_lm + 2023;
            $start_date = $annee_prog . '-01-01';
            $end_date = $annee_prog . '-12-31';
        }


        echo $res->id_segment;
        echo ' - ';
        echo $res->sysma_object_id;
        echo ' - ';
        echo $res->action_lm;

        $q2 = 'select * from sysma.sysma_action_type where sysma_action_type = :at and sysma_object_type_id = 10568681';
        $qry2 = $app['pdo']->prepare($q2);
        $qry2->bindParam(':at', $res->action_lm);
        $qry2->execute();
        $res2 = $qry2->fetchObject();

        echo $res2->sysma_action_type . ' - ';

        $A = new SysmaAction(null, $app);

        $data = [
            'sysma_action' => $res2->sysma_action_type,
            'sysma_action_type_id' => $res2->sysma_action_type_id,
            'start_date' => $start_date,
            'end_date' =>  $end_date,
            'organisation_id' => 3,
            'created_by' => 2,
            'program_year' => $annee_prog,
            'status' => 'programme',
            'contracts' => '{Prog Evre-Thau-St Denis 2024-2029}'
        ];


        $resA = json_decode($A->create($data));

        var_dump($resA);

        $O = new SysmaObject($res->sysma_object_id, $app);

        $O->linkSysmaActionSheet($resA->id, 2);



        //die() ;

    }
})->bind('');





$app->get('/travail-smib-creation-actions-reh-berge', function (Request $request) use ($app) {


    die() ;

    $q = 'select * from sysma_temp_import._hardy_litmineur_berge_annexeshydrau where methode = \'REH\' and action_ber is not null';
    $qry = $app['pdo']->prepare($q);
    $qry->execute();
    while ($res = $qry->fetchObject()) {


        if ($res->act_an_lm == "0" or $res->act_an_lm == '') {
            $annee_prog = 2024;
            $start_date = '2024-01-01';
            $end_date = '2029-12-31';
        } else {
            $annee_prog = $res->act_an_lm + 2023;
            $start_date = $annee_prog . '-01-01';
            $end_date = $annee_prog . '-12-31';
        }


        echo $res->id_segment;
        echo ' - ';
        echo $res->sysma_object_id;
        echo ' - ';
        echo $res->action_ber;

        $q2 = 'select * from sysma.sysma_action_type where sysma_action_type = :at and sysma_object_type_id = 10568681';
        $qry2 = $app['pdo']->prepare($q2);
        $qry2->bindParam(':at', $res->action_ber);
        $qry2->execute();
        $res2 = $qry2->fetchObject();

        echo $res2->sysma_action_type . ' - ';

        $A = new SysmaAction(null, $app);

        $data = [
            'sysma_action' => $res2->sysma_action_type,
            'sysma_action_type_id' => $res2->sysma_action_type_id,
            'start_date' => $start_date,
            'end_date' =>  $end_date,
            'organisation_id' => 3,
            'created_by' => 2,
            'program_year' => $annee_prog,
            'status' => 'programme',
            'contracts' => '{Prog Evre-Thau-St Denis 2024-2029}'
        ];


        $resA = json_decode($A->create($data));

        var_dump($resA);

        $O = new SysmaObject($res->sysma_object_id, $app);

        $O->linkSysmaActionSheet($resA->id, 2);



        //die() ;

    }
})->bind('');





$app->get('/travail-smib-creation-actions-reh-rip', function (Request $request) use ($app) {

    die() ;


    $q = 'select * from sysma_temp_import._hardy_litmineur_berge_annexeshydrau where methode = \'REH\' and action_rip is not null';
    $qry = $app['pdo']->prepare($q);
    $qry->execute();
    while ($res = $qry->fetchObject()) {


        if ($res->act_an_lm == "0" or $res->act_an_lm == '') {
            $annee_prog = 2024;
            $start_date = '2024-01-01';
            $end_date = '2029-12-31';
        } else {
            $annee_prog = $res->act_an_lm + 2023;
            $start_date = $annee_prog . '-01-01';
            $end_date = $annee_prog . '-12-31';
        }


        echo $res->id_segment;
        echo ' - ';
        echo $res->sysma_object_id;
        echo ' - ';
        echo $res->action_rip;

        $q2 = 'select * from sysma.sysma_action_type where sysma_action_type = :at and sysma_object_type_id = 10568681';
        $qry2 = $app['pdo']->prepare($q2);
        $qry2->bindParam(':at', $res->action_rip);
        $qry2->execute();
        $res2 = $qry2->fetchObject();

        echo $res2->sysma_action_type . ' - ';

        $A = new SysmaAction(null, $app);

        $data = [
            'sysma_action' => $res2->sysma_action_type,
            'sysma_action_type_id' => $res2->sysma_action_type_id,
            'start_date' => $start_date,
            'end_date' =>  $end_date,
            'organisation_id' => 3,
            'created_by' => 2,
            'program_year' => $annee_prog,
            'status' => 'programme',
            'contracts' => '{Prog Evre-Thau-St Denis 2024-2029}'
        ];


        $resA = json_decode($A->create($data));

        var_dump($resA);

        $O = new SysmaObject($res->sysma_object_id, $app);

        $O->linkSysmaActionSheet($resA->id, 2);



        //die() ;

    }
})->bind('');




// T2BV


$app->get('/travail-smib-creation-actions-t2bv-lm', function (Request $request) use ($app) {

    die() ;


    $q = 'select * from sysma_temp_import._hardy_litmineur_berge_annexeshydrau where methode = \'Tête de bassin versant\' and action_lm is not null';
    $qry = $app['pdo']->prepare($q);
    $qry->execute();
    while ($res = $qry->fetchObject()) {


        if ($res->act_an_lm == "0" or $res->act_an_lm == '') {
            $annee_prog = 2024;
            $start_date = '2024-01-01';
            $end_date = '2029-12-31';
        } else {
            $annee_prog = $res->act_an_lm + 2023;
            $start_date = $annee_prog . '-01-01';
            $end_date = $annee_prog . '-12-31';
        }


        echo $res->id_segment;
        echo ' - ';
        echo $res->sysma_object_id;
        echo ' - ';
        echo $res->action_lm;

        $q2 = 'select * from sysma.sysma_action_type where sysma_action_type = :at and sysma_object_type_id = 158507';
        $qry2 = $app['pdo']->prepare($q2);
        $qry2->bindParam(':at', $res->action_lm);
        $qry2->execute();
        $res2 = $qry2->fetchObject();

        echo $res2->sysma_action_type . ' - ';

        $A = new SysmaAction(null, $app);

        $data = [
            'sysma_action' => $res2->sysma_action_type,
            'sysma_action_type_id' => $res2->sysma_action_type_id,
            'start_date' => $start_date,
            'end_date' =>  $end_date,
            'organisation_id' => 3,
            'created_by' => 2,
            'program_year' => $annee_prog,
            'status' => 'programme',
            'contracts' => '{Prog Evre-Thau-St Denis 2024-2029}'
        ];


        $resA = json_decode($A->create($data));

        var_dump($resA);

        $O = new SysmaObject($res->sysma_object_id, $app);

        $O->linkSysmaActionSheet($resA->id, 2);



        //die() ;

    }
})->bind('');





$app->get('/travail-smib-creation-actions-t2bv-berge', function (Request $request) use ($app) {


    die();

    $q = 'select * from sysma_temp_import._hardy_litmineur_berge_annexeshydrau where methode = \'Tête de bassin versant\' and action_ber is not null';
    $qry = $app['pdo']->prepare($q);
    $qry->execute();
    while ($res = $qry->fetchObject()) {


        if ($res->act_an_lm == "0" or $res->act_an_lm == '') {
            $annee_prog = 2024;
            $start_date = '2024-01-01';
            $end_date = '2029-12-31';
        } else {
            $annee_prog = $res->act_an_lm + 2023;
            $start_date = $annee_prog . '-01-01';
            $end_date = $annee_prog . '-12-31';
        }


        echo $res->id_segment;
        echo ' - ';
        echo $res->sysma_object_id;
        echo ' - ';
        echo $res->action_ber;

        $q2 = 'select * from sysma.sysma_action_type where sysma_action_type = :at and sysma_object_type_id = 158507';
        $qry2 = $app['pdo']->prepare($q2);
        $qry2->bindParam(':at', $res->action_ber);
        $qry2->execute();
        $res2 = $qry2->fetchObject();

        echo $res2->sysma_action_type . ' - ';

        $A = new SysmaAction(null, $app);

        $data = [
            'sysma_action' => $res2->sysma_action_type,
            'sysma_action_type_id' => $res2->sysma_action_type_id,
            'start_date' => $start_date,
            'end_date' =>  $end_date,
            'organisation_id' => 3,
            'created_by' => 2,
            'program_year' => $annee_prog,
            'status' => 'programme',
            'contracts' => '{Prog Evre-Thau-St Denis 2024-2029}'
        ];


        $resA = json_decode($A->create($data));

        var_dump($resA);

        $O = new SysmaObject($res->sysma_object_id, $app);

        $O->linkSysmaActionSheet($resA->id, 2);



        //die() ;

    }
})->bind('');





$app->get('/travail-smib-creation-actions-t2bv-rip', function (Request $request) use ($app) {

    die();


    $q = 'select * from sysma_temp_import._hardy_litmineur_berge_annexeshydrau where methode = \'Tête de bassin versant\' and action_rip is not null';
    $qry = $app['pdo']->prepare($q);
    $qry->execute();
    while ($res = $qry->fetchObject()) {


        if ($res->act_an_lm == "0" or $res->act_an_lm == '') {
            $annee_prog = 2024;
            $start_date = '2024-01-01';
            $end_date = '2029-12-31';
        } else {
            $annee_prog = $res->act_an_lm + 2023;
            $start_date = $annee_prog . '-01-01';
            $end_date = $annee_prog . '-12-31';
        }


        echo $res->id_segment;
        echo ' - ';
        echo $res->sysma_object_id;
        echo ' - ';
        echo $res->action_rip;

        $q2 = 'select * from sysma.sysma_action_type where sysma_action_type = :at and sysma_object_type_id = 158507';
        $qry2 = $app['pdo']->prepare($q2);
        $qry2->bindParam(':at', $res->action_rip);
        $qry2->execute();
        $res2 = $qry2->fetchObject();

        echo $res2->sysma_action_type . ' - ';

        $A = new SysmaAction(null, $app);

        $data = [
            'sysma_action' => $res2->sysma_action_type,
            'sysma_action_type_id' => $res2->sysma_action_type_id,
            'start_date' => $start_date,
            'end_date' =>  $end_date,
            'organisation_id' => 3,
            'created_by' => 2,
            'program_year' => $annee_prog,
            'status' => 'programme',
            'contracts' => '{Prog Evre-Thau-St Denis 2024-2029}'
        ];


        $resA = json_decode($A->create($data));

        var_dump($resA);

        $O = new SysmaObject($res->sysma_object_id, $app);

        $O->linkSysmaActionSheet($resA->id, 2);



        //die() ;

    }
})->bind('');
