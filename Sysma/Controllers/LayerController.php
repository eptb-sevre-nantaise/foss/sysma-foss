<?php

use Symfony\Component\HttpFoundation\Request;

$app->post('/updatelayerstate', function(Request $request) use($app) {


    if ($app['session']->get('user_id') != ANONYMID && $app['session']->get('user_id') != null) { 

        $r = $request->request->all();


        $U = new User($app['session']->get('user_id'), $app);

        try {

            if ($r['type'] === 'objecttype') {

                if ($r['action'] === 'add') {
                    return $U->addObjectTypeLayerToUserSelection($r['id'], ['layerstyle' => $r['layerstyle'], 'parentid' => $r['parentid'], 'options' => $r['options'], 'leaflet_id' => $r['leaflet_id'], 'layer' => $r['layer']]);
                }
                if ($r['action'] === 'delete') {
                    return $U->removeObjectTypeLayerFromUserSelection($r['id'], ['layerstyle' => $r['layerstyle'], 'parentid' => $r['parentid'], 'options' => $r['options'], 'leaflet_id' => $r['leaflet_id'], 'layer' => $r['layer']]);
                }
            } elseif ($r['type'] === 'actiontype') {
                if ($r['action'] === 'add') {
                    return $U->addSysmaActionTypeLayerToUserSelection($r['id'], ['layerstyle' => $r['layerstyle'], 'parentid' => $r['parentid'], 'options' => $r['options'], 'leaflet_id' => $r['leaflet_id'], 'layer' => $r['layer']]);
                }
                if ($r['action'] === 'delete') {
                    return $U->removeSysmaActionTypeLayerFromUserSelection($r['id'], ['layerstyle' => $r['layerstyle'], 'parentid' => $r['parentid'], 'options' => $r['options'], 'leaflet_id' => $r['leaflet_id'], 'layer' => $r['layer']]);
                }
            } elseif ($r['type'] === 'group') {
                if ($U->updateGroupInfoForUser($r['id'], $r['action'])) {
                    return "<script>sendData(null, '/layermanager', 'infos', event)</script>";
                }
            } else {
                if ($r['action'] === 'add') {
                    return $U->addOtherLayerToUserSelection($r['type'], $r['id'], ['layerstyle' => $r['layerstyle'], 'parentid' => $r['parentid'], 'options' => $r['options'], 'leaflet_id' => $r['leaflet_id'], 'layer' => $r['layer']]);
                }
                if ($r['action'] === 'delete') {
                    return $U->removeOtherLayerFromUserSelection($r['type'], $r['id'], ['layerstyle' => $r['layerstyle'], 'parentid' => $r['parentid'], 'options' => $r['options'], 'leaflet_id' => $r['leaflet_id'], 'layer' => $r['layer']]);
                }
            }
        } catch (Exception $ex) {
            return '<span class="glyphicon glyphicon-remove text-danger" title="' . $ex->getMessage() . '"></span>' . $ex->getMessage();
        }
    } else {
        return '';
    }
})->bind('updatelayerstate');



// /offline-init-data
// return a json containing all necessary data for init offline mode  
$app->match('/offline-init-data', function(Request $request) use($app) {
    $currentUserId = $app['session']->get('user_id');
    $output = new stdClass;
    if ( $currentUserId != ANONYMID) {
        
        $U = new User($currentUserId, $app);
        // layers ids and names
        $layers = $U->loadObjectTypesLayers();
        $layerids = [];
        foreach ($layers as $layer) {
            // TOTO better: seules le couches dont le user est gestionnaire sont retournées pour l'instant
            $userIsManagerOnCurrentObjectType = hasAccess($app, $currentUserId, null, $layer['sysma_object_type_id'], null, null, null, null, 'gestionnaire');
            // $userIsCommiterOnCurrentObjectType = hasAccess($app, $currentUserId, null, $layer['sysma_object_type_id'], null, null, null, null, 'contributeur');
            
            // if ($userIsManagerOnCurrentObjectType or $userIsCommiterOnCurrentObjectType) {
            if ($userIsManagerOnCurrentObjectType) {
                    $layerids[$layer['sysma_object_type_id']] = $layer['sysma_object_type'];
            }

        }
        $output->layerIds = $layerids;

        // this Sysma org label
        $output->SysmaOrgLabel = ORGANISATION_ACRONYM;

        // orgs labels list
        $output->orgLabels = organisationList($app);

        // status labels list
        $output->statusLabels = statusList('sysma_object', $app);

    } else {
        $output->status = ACCESS_NOT_GRANTED;
    }
    return $app->json($output);
})->bind('offline-init-data');



// generic layer controller
// TODO: routes for each layertype?/layerid/geoobjectid
$app->post('/generic-other-layer', function (Request $request) use ($app) {

    $r = $request->request->all();

    $url = $r['urlToEmbed'];
    unset($r['urlToEmbed']);

    foreach ($r as $key => $item) {
        $url .= '&' . $key . '=' . $item;
    }
    // echo $url;

    return $app['twig']->render('layer-manager/genericOtherLayer.twig', ['url' => $url]);
})->bind('generic-other-layer');