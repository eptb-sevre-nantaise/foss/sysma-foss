<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;


$app->match('/admin', function (Request $request) use ($app) {

    if ($app['session']->get('is_admin')) {

        return $app['twig']->render('admin/adminHome.twig');
    } else {

        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('admin-home');

$app->match('/admin/users', function (Request $request) use ($app) {

    if ($app['session']->get('user') === null) {
        loadUserFromSession($app, $request);
    }

    if ($app['session']->get('is_admin')) {

        return $app['twig']->render('admin/adminUsersSheet.twig', ['tabU' => usersList($app)]);
    } else {

        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('users');



$app->match('/admin/history', function(Request $request) use($app) {   
    
    if ($app['session']->get('is_admin')) {
        $request->query->get('limit') != null ? $limit = $request->query->get('limit') : $limit = 100;
        return $app['twig']->render('admin/adminHistory.twig', ['History' => loadHistory($app, $limit), 'limit' => $limit]);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('history');




$app->match('/admin/user/{id}/infos', function (Request $request, $id) use ($app) {

    if ($app['session']->get('is_admin')) {

        $U = new User($id, $app);
        $U->loadUserStats();
        $request->query->get('limit') != null ? $limit = $request->query->get('limit') : $limit = 50;
        $History = loadHistory($app, $limit, $id) ;
        return $app['twig']->render('admin/adminUserInfoSheet.twig', ['U' => $U, 'History' => $History, 'limit' => $limit]);

    } else {

        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('user-infos');



$app->match('/admin/user/{id}', function ($id, Request $request) use ($app) {


    if ($app['session']->get('is_admin')) {

        $U = buildSimpleEntity('user', 'user_id', $id, $app);

        $U['active'] = ($U['active'] === '1');
        $U['privilege_admin'] = (strpos($U['privileges'], 'administrateur') !== false);
        $U['privilege_cadastre'] = (strpos($U['privileges'], 'cadastre') !== false);
        $U['privilege_rpg'] = (strpos($U['privileges'], 'rpg') !== false);
        $U['privilege_gis_export'] = (strpos($U['privileges'], 'gis_export') !== false);
        $U['privilege_pg_export'] = (strpos($U['privileges'], 'pg_export') !== false);

        $form = $app['form.factory']->createBuilder('form', $U)
            ->add('firstname', 'text', array('label' => 'Prénom', 'constraints' => new Assert\NotBlank()))
            ->add('lastname', 'text', array('label' => 'Nom', 'constraints' => new Assert\NotBlank()))
            ->add('user', 'text', array('label' => 'Nom d\'utlisateur, uniquement des minuscules, sans espace ni accent', 'constraints' => new Assert\NotBlank()))
            ->add('organisation_id', 'choice', array('choices' => organisationListForForm($app), 'constraints' => new Assert\NotBlank()))
            ->add('password', 'password', array('data' => null))
            ->add('default_center', 'text', array('label' => 'Centre par défaut de la carte, sous la forme LatLng(46.924754, -0.943285)'))
            ->add('default_zoom', 'integer', array('label' => 'Zoom par défaut de la carte, entre 1 et 20'))
            ->add('map_width', 'integer', array('label' => 'Largeur de la carte en %'))
            ->add('user_schema', 'text', array('label' => 'Schema PostgreSQL où sont stockées les tables de l\'user (pour l\'import des layers dans Sysma'))
            ->add('privilege_admin', 'checkbox', array('label' => 'Est administrateur de Sysma'))
            ->add('privilege_cadastre', 'checkbox', array('label' => 'A accès aux informations nominatives du cadastre'))
            ->add('privilege_rpg', 'checkbox', array('label' => 'A accès aux informations du Référentiel Parcellaire Graphique (RPG) agricole'))
            ->add('privilege_gis_export', 'checkbox', array('label' => 'Peut exporter des fichiers Shape (selon son niveau de droit d\'accès aux couches)'))
            ->add('privilege_pg_export', 'checkbox', array('label' => 'Peut générer les couches Posgtre (Schéma : sysma_export_layers)'))
            ->add('active', 'checkbox', ['label' => 'Compte activé (l\'utilsateur peut se connecter)'])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {

            $data = $form->getData();


            unset($data['track_historytables_id']);
            $data['last_access'] = null;
            $data['modified_by'] = $app['session']->get('user_id');
            $data['user_id'] = $id;

            $privileges = array();
            if ($data['privilege_admin'] === true) {
                $privileges[] = 'administrateur';
            }
            if ($data['privilege_cadastre'] === true) {
                $privileges[] = 'cadastre';
            }
            if ($data['privilege_rpg'] === true) {
                $privileges[] = 'rpg';
            }
            if ($data['privilege_gis_export'] === true) {
                $privileges[] = 'gis_export';
            }
            if ($data['privilege_pg_export'] === true) {
                $privileges[] = 'pg_export';
            }

            unset($data['privilege_admin']);
            unset($data['privilege_cadastre']);
            unset($data['privilege_rpg']);
            unset($data['privilege_gis_export']);
            unset($data['privilege_pg_export']);

            $data['privileges'] = '{' . implode(',', $privileges) . '}';



            if ($data['password'] == null) {
                unset($data['password']);
            } else {
                $U = new User($id, $app);
                $data['password'] = hashSysma($data['password']);
            }

            $data['active'] = intval($data['active'] === true);


            $U = new User($id, $app);
            $res = $U->update($data);


            if ($U->id == $app['session']->get('user_id')) {
                return $app->redirect($app['url_generator']->generate('logout'));
            } else {
                return $app->redirect($app['url_generator']->generate('users'));
            }
        }

        return $app['twig']->render('admin/adminUserForm.twig', ['U' => $U, 'form' => $form->createView()]);
    } else {

        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('user')->assert('id', '\d+');



$app->match('/admin/user/new', function (Request $request) use ($app) {


    if ($app['session']->get('is_admin')) {


        $form = $app['form.factory']->createBuilder('form', ['user_id' => dataIndex($app['pdo'])])
            ->add('user_id', 'hidden', array('label' => 'Id user (automatique)', 'constraints' => new Assert\NotBlank()))
            ->add('user', 'text', array('label' => 'Nom d\'utlisateur, uniquement des minuscules, sans espace ni accent', 'constraints' => new Assert\NotBlank()))
            ->add('firstname', 'text', array('label' => 'Prénom', 'constraints' => new Assert\NotBlank()))
            ->add('lastname', 'text', array('label' => 'Nom', 'constraints' => new Assert\NotBlank()))
            ->add('organisation_id', 'choice', array('choices' => organisationListForForm($app), 'constraints' => new Assert\NotBlank()))
            ->add('password', 'password', array('data' => null, 'constraints' => new Assert\Length(['min' => 8])))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {

            $data = $form->getData();
            $data['registration_date'] = date('Y-m-d H:i:s');
            $data['password'] = hashSysma($data['password']);
            $data['modified_by'] = $app['session']->get('user_id');

            $U = new User(null, $app);
            $res = json_decode($U->insert($data));

            return $app->redirect($app['url_generator']->generate('user', ['id' => $res->id]));
        }

        return $app['twig']->render('admin/adminUserForm.twig', ['U' => null, 'form' => $form->createView()]);
    } else {

        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('user-new');




$app->match('/admin/user/{id}/privilege_list', function ($id) use ($app) {


    if ($app['session']->get('is_admin')) {

        $U = new User($id, $app);
        $privilege_list = $U->loadRights();

        return $app['twig']->render('admin/adminUserRightsSheet.twig', ['U' => $U, 'tabD' => $privilege_list]);
    } else {

        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('user-rights');




$app->match('/admin/user/{id}/droit/{idd}/modifier', function ($id, $idd, Request $request) use ($app) {

    $r = $request->request->all();

    if ($app['session']->get('is_admin')) {

        $U = new User($id, $app);
        if ($idd == 'new') {
            $D = null;
        } else {
            $SE = buildSimpleEntity('user_privilege', 'user_privilege_id', $idd, $app);

            foreach ((array) $SE as $key => $val) {
                $D[$key] = explode(',', substr($SE[$key], 1, -1));
            }
        }


        $builder = $app['form.factory']->createBuilder('form', $D);

        $builder->add('privilege_list', 'choice', array('label' => 'Niveaux de droits', 'choices' => $GLOBALS['PRIVILEGES_LEVELS'], 'multiple' => true, 'constraints' => new Assert\NotBlank()));
        $builder->add('sysma_object_type_id_list', 'choice', array('label' => 'Type d\'objets', 'choices' => ['0' => 'Tous', 'NULL' => 'Aucun'] + sysmaObjectTypesListForForm($app), 'multiple' => true, 'constraints' => new Assert\NotBlank()));
        $builder->add('sysma_object_status_list', 'choice', array('label' => 'Status des objets', 'choices' => ['tous' => 'Tous', 'NULL' => 'Aucun'] + statusListForForm($app, 'sysma_object'), 'multiple' => true, 'constraints' => new Assert\NotBlank()));
        $builder->add('sysma_object_organisation_id_list', 'choice', array('label' => 'Structures des objets', 'choices' => ['0' => 'Toutes', 'NULL' => 'Aucune'] + organisationListForForm($app), 'multiple' => true, 'constraints' => new Assert\NotBlank()));
        $builder->add('sysma_action_type_id_list', 'choice', array('label' => 'Type de travaux', 'choices' => ['0' => 'Tous', 'NULL' => 'Aucun'] + sysmaActionTypesListForForm($app), 'multiple' => true, 'constraints' => new Assert\NotBlank()));
        $builder->add('sysma_action_status_list', 'choice', array('label' => 'Status des travaux', 'choices' => ['tous' => 'Tous', 'NULL' => 'Aucun'] + statusListForForm($app, 'sysma_action'), 'multiple' => true, 'constraints' => new Assert\NotBlank()));
        $builder->add('sysma_action_organisation_id_list', 'choice', array('label' => 'Structures des travaux', 'choices' => ['0' => 'Toutes', 'NULL' => 'Aucune'] + organisationListForForm($app), 'multiple' => true, 'constraints' => new Assert\NotBlank()));

        $form = $builder->getForm();
        $form->handleRequest($request);

        if ($form->isValid()) {

            $data = $form->getData();
            $data['modified_by'] = $app['session']->get('user_id');
            $data['privilege_list'] = '{' . implode($data['privilege_list'], ',') . '}';
            $data['sysma_object_type_id_list'] = '{' . implode($data['sysma_object_type_id_list'], ',') . '}';
            $data['sysma_object_status_list'] = '{' . implode($data['sysma_object_status_list'], ',') . '}';
            $data['sysma_object_organisation_id_list'] = '{' . implode($data['sysma_object_organisation_id_list'], ',') . '}';
            $data['sysma_action_type_id_list'] = '{' . implode($data['sysma_action_type_id_list'], ',') . '}';
            $data['sysma_action_status_list'] = '{' . implode($data['sysma_action_status_list'], ',') . '}';
            $data['sysma_action_organisation_id_list'] = '{' . implode($data['sysma_action_organisation_id_list'], ',') . '}';
            $data['user_id'] = $id;


            $U = new User($id, $app);
            if ($idd == 'new') {
                $res = json_decode($U->insertRights($data));
            } else {
                $data['user_privilege_id'] = $idd;
                unset($data['track_historytables_id']);
                $res = json_decode($U->updateRights($data));
                $U->deleteSelectedLayers();
            }
            if (!isset($res->message)) {
                $mes = 'Droit enregistré';
            } else {
                $mes = $res->message;
            }


            $app['session']->getFlashBag()->add('message', array('status' => $res->status, 'text' => $mes));


            return $app->redirect($app['url_generator']->generate('user-rights', ['id' => $id]));
        }



        return $app['twig']->render('admin/adminUserRightForm.twig', ['U' => $U, 'form' => $form->createView()]);
    } else {

        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('user-right');


$app->match('/admin/user/{id}/droit/{idd}/supprimer', function ($id, $idd) use ($app) {


    if ($app['session']->get('is_admin')) {

        $U = new User($id, $app);
        $U->deleteRights($idd);
        return $app->redirect($app['url_generator']->generate('user-rights', ['id' => $id]));
    } else {

        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('user-right-delete');


$app->match('/admin/connexions', function (Request $request) use ($app) {


    if ($app['session']->get('is_admin')) {

        return $app['twig']->render('admin/adminConnections.twig', ['tabC' => sql_filtersHistory($app, array('limit' => $request->query->get('limit'))), 'bilan' => sql_filtersHistoryReport($app, array('start_date' => $request->query->get('start_date'))), 'start_date' => $request->query->get('start_date'), 'tabObjets' => lastModifiedSysmaObjectsList($app, ['limit' => 100])]);
    } else {

        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('connections');



/* VIEWS */

$app->get('/admin/configuration/vues', function (Request $request) use ($app) {

    if ($app['session']->get('is_admin')) {

        return $app['twig']->render('admin/adminViewsSheet.twig', ['U' => new User($app['session']->get('user_id'), $app)]);
    } else {

        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('admin-views');



$app->match('/admin/configuration/vues/form', function (Request $request) use ($app) {

    if ($app['session']->get('is_admin')) {

        $v = $request->query->get('v');
        $V = new UserView($v, $app);
        $UV = loadUsersAndView($app, $V->view);


        $form = $app['form.factory']->createBuilder('form', ['view_id' => $V->view, 'users' => $UV['tabUsersWithTheView']])
            ->add('users', 'choice', array('label' => 'Appliquer la vue aux utilisateurs suivants : (attention, la vue sera supprimée pour les utilisateurs non sélectionnés)', 'multiple' => true, 'choices' => $UV['tabUsers'], 'constraints' => new Assert\NotBlank()))
            ->add('view_id', 'hidden')
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {

            $data = $form->getData();

            User::removeUsersView($data['view_id'], $app);

            foreach ($data['users'] as $user_id) {

                $U = new User($user_id, $app);

                $newUV = new UserView(null, $app);
                $newUV->insert(['user_id' => $U->user_id, 'view' => $V->view, 'view_json' => $V->view_json, 'modified_by' => $app['session']->get('user_id')]);
            }

            $app['session']->getFlashBag()->add('message', array('status' => 'success', 'text' => 'Vue ajoutée aux utilisateurs'));


            return $app->redirect($app['url_generator']->generate('admin-views', []));
        } else {

            return $app['twig']->render('admin/adminConfForm.twig', ['U' => null, 'form' => $form->createView()]);
        }
    } else {

        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('admin-view-form');




/* LAYERS GROUPS */


$app->get('/admin/configuration/groupes', function (Request $request) use ($app) {

    if ($app['session']->get('is_admin')) {

        return $app['twig']->render('admin/adminLayerGroupsSheet.twig', ['U' => null, 'Gs' =>  groups($app)]);
    } else {

        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('admin-layer-groups');






$app->match('/admin/configuration/groupe/form', function (Request $request) use ($app) {

    if ($app['session']->get('is_admin')) {

        $g = $request->query->get('g');
        if ($g == 'new') {
            $gData = ['layer_group_id' => 'new'];
        } else {
            $G = new LayerGroup($g, $app);
            $gData = get_object_vars($G);
            unset($gData['pdo']);
        }

        $form = $app['form.factory']->createBuilder('form', $gData)
            ->add('group_name', 'text', array('label' => 'Nom du groupe', 'constraints' => new Assert\NotBlank()))
            ->add('group_alias', 'text', array('label' => 'Alias du groupe (sans espace, accent ou caractère spécial)', 'constraints' => new Assert\NotBlank()))
            ->add('group_display_order', 'text', array('label' => 'Ordre d\'affichage du contrat'))
            ->add('layer_group_id', 'hidden')
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {

            $data = $form->getData();


            if ($data['layer_group_id'] == 'new') {
                $G = new LayerGroup(null, $app);
                unset($data['layer_group_id']);

                if ($G->create($data)) {
                    $app['session']->getFlashBag()->add('message', array('status' => 'success', 'text' => 'Groupe créé'));
                } else {
                    $app['session']->getFlashBag()->add('message', array('status' => 'danger', 'text' => 'Erreur lors de la création du groupe'));
                }
            } else {
                $G = new LayerGroup($data['layer_group_id'], $app);
                $G->update($data);
                $app['session']->getFlashBag()->add('message', array('status' => 'success', 'text' => 'Groupe mis à jour'));
            }

            return $app->redirect($app['url_generator']->generate('admin-layer-groups', []));
        } else {

            return $app['twig']->render('admin/adminConfForm.twig', ['U' => null, 'form' => $form->createView()]);
        }
    } else {

        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('admin-layer-group-form');



$app->match('/admin/configuration/groupe/delete', function (Request $request) use ($app) {

    if ($app['session']->get('is_admin')) {


        $G = new LayerGroup($request->query->get('g'), $app);

        if ($G->delete()) {
            $app['session']->getFlashBag()->add('message', array('status' => 'success', 'text' => 'Groupe supprimé'));
        } else {
            $app['session']->getFlashBag()->add('message', array('status' => 'success', 'text' => 'Erreur lors de la suppresion du groupe'));
        }


        return $app->redirect($app['url_generator']->generate('admin-layer-groups', []));
    } else {

        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('admin-layer-group-delete');



/* CONTRACTS */


$app->get('/admin/configuration/contrats', function (Request $request) use ($app) {

    if ($app['session']->get('is_admin')) {

        return $app['twig']->render('admin/adminContractsSheet.twig', ['U' => null, 'Cs' =>  contracts($app)]);
    } else {

        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('admin-contracts');





$app->match('/admin/configuration/contrat/form', function (Request $request) use ($app) {

    if ($app['session']->get('is_admin')) {

        $c = $request->query->get('c');
        if ($c == 'new') {
            $cData = ['contract_id' => 'new'];
        } else {
            $C = new Contract($c, $app);
            $cData = get_object_vars($C);
            unset($cData['pdo']);
        }

        $form = $app['form.factory']->createBuilder('form', $cData)
            ->add('contract', 'text', array('label' => 'Nom du contrat', 'constraints' => new Assert\NotBlank()))
            ->add('display_order', 'text', array('label' => 'Ordre d\'affichage du contrat'))
            ->add('contract_id', 'hidden')
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {

            $data = $form->getData();


            if ($data['contract_id'] == 'new') {
                $C = new Contract(null, $app);
                unset($data['contract_id']);

                $res = json_decode($C->create($data));

                if ($res->status == 'success') {
                    $app['session']->getFlashBag()->add('message', array('status' => 'success', 'text' => 'Contrat créé'));
                } else {
                    $app['session']->getFlashBag()->add('message', array('status' => 'danger', 'text' => 'Erreur lors de la création du contrat'));
                }
            } else {
                $F = new Contract($data['contract_id'], $app);
                $res = json_decode($F->update($data));
                if($res->status=='success'){
                    $app['session']->getFlashBag()->add('message', array('status' => 'success', 'text' => 'Contrat mis à jour'));
                } else {
                    $app['session']->getFlashBag()->add('message', array('status' => 'danger', 'text' => 'Echec lors de la mise à jour'));
                }
            }

            return $app->redirect($app['url_generator']->generate('admin-contracts', []));
        } else {

            return $app['twig']->render('admin/adminConfForm.twig', ['U' => null, 'form' => $form->createView()]);
        }
    } else {

        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('admin-contract-form');



$app->match('/admin/configuration/contrat/delete', function (Request $request) use ($app) {

    if ($app['session']->get('is_admin')) {


        $C = new Contract($request->query->get('c'), $app);

        if ($C->delete()) {
            $app['session']->getFlashBag()->add('message', array('status' => 'success', 'text' => 'Contrat supprimé'));
        } else {
            $app['session']->getFlashBag()->add('message', array('status' => 'success', 'text' => 'Erreur lors de la suppresion du contrat'));
        }


        return $app->redirect($app['url_generator']->generate('admin-contracts', []));
    } else {

        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('admin-contract-delete');




/* OTHERS LAYERS */


$app->get('/admin/configuration/couches', function (Request $request) use ($app) {

    if ($app['session']->get('is_admin')) {

        $tabOL = otherLayersList($app['pdo'], null);
        foreach ($tabOL as $OL) {
            $OL['style_decode'] = json_decode($OL['style']);
            $OLs[] = $OL;
        }

        return $app['twig']->render('admin/adminOLsSheet.twig', ['U' => null, 'OLs' =>  $OLs]);
    } else {

        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('admin-other-layers');



$app->match('/admin/configuration/couches/form', function (Request $request) use ($app) {

    if ($app['session']->get('is_admin')) {

        $ol = $request->query->get('ol');
        if ($ol == 'new') {
            $olData = ['other_layer_id' => 'new', 'style' => '{}', 'mapping' => '{}'];
        } else {
            $olData = buildSimpleEntity('other_layer', 'other_layer_id', $ol, $app);
        }

        $form = $app['form.factory']->createBuilder('form', $olData)
            ->add('layer_id', 'text', array('label' => 'Nom de la couche', 'constraints' => new Assert\NotBlank()))
            ->add('layer_alias', 'text', array('label' => 'Alias de la couche (sans espace ni accent) - A renseigner obligatoirement pour les tile_layer et tile_layers_wms'))
            ->add('layer_type', 'choice', array('label' => 'Type de couche', 'choices' => ['simple_pg_layer' => 'simple_pg_layer', 'tile_layer' => 'tile_layer',  'tile_layer_wms' => 'tile_layer_wms', 'piwigo_layer' => 'piwigo_layer (photohèque)', 'external_geojson_layer' => 'external_geojson_layer'], 'constraints' => new Assert\NotBlank()))
            ->add('geometry_type', 'choice', array('label' => 'Type de géométrie - A préciser uniquement pour les simple_pg_layer', 'choices' => ['' => '', 'ST_Point' => 'ST_Point', 'ST_Linestring' => 'ST_Linestring', 'ST_Polygon' => 'ST_Polygon', 'ST_Polygon_to_Linestring' => 'ST_Polygon_to_Linestring']))
            ->add('geolayer_id', 'text', array('label' => 'URL ou chemin de la couche', 'constraints' => new Assert\NotBlank()))
            ->add('style', 'textarea', array('label' => 'Style, format json - A préciser uniquement pour les simple_pg_layer'))
            ->add('mapping', 'textarea', array('label' => 'Correspondance des champs - A préciser uniquement pour les simple_pg_layer'))
            ->add('sql_filter', 'text', array('label' => 'Filtre SQL'))
            ->add('zoom_min', 'integer', array('constraints' => new Assert\NotBlank()))
            ->add('zoom_max', 'integer', array('constraints' => new Assert\NotBlank()))
            ->add('load_on_pan', 'choice', array('label' => 'Rechargement lors du déplacement de la carte', 'choices' => [0 => 'Non', 1 => 'Oui'],  'constraints' => new Assert\NotBlank()))
            //->add('group', 'choice', array('choices' => , 'label' => 'Groupe'))
            ->add('group', 'choice', array('label' => 'Groupe', 'choices' => array_merge(sysmaObjectTypeGroupsNameList($app), ['baseLayers' => 'baseLayers', 'otherBaseLayers' => 'otherBaseLayers']), 'constraints' => new Assert\NotBlank()))
            ->add('other_layer_display_order', 'text', array('label' => 'Ordre d\'affichage'))
            ->add('options', 'text', array())
            ->add('access', 'choice', array('label' => 'Accès', 'choices' => ['public' => 'public', 'restricted' => 'restricted'], 'constraints' => new Assert\NotBlank()))
            ->add('other_layer_id', 'hidden')
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {

            $data = $form->getData();


            if (!json_decode($data['style'])) {
                $app['session']->getFlashBag()->add('message', array('status' => 'danger', 'text' => 'Format du style non conforme'));
                return $app['twig']->render('admin/adminConfForm.twig', ['U' => null, 'form' => $form->createView()]);
            }
            if (!json_decode($data['mapping'])) {
                $app['session']->getFlashBag()->add('message', array('status' => 'danger', 'text' => 'Format de la correspondance de champ non conforme'));
                return $app['twig']->render('admin/adminConfForm.twig', ['U' => null, 'form' => $form->createView()]);
            }

            if ($data['other_layer_id'] == 'new') {
                $OL = new OtherLayer(null, $app);
                unset($data['other_layer_id']);

                $res = json_decode($OL->create($data)) ;
                if ($res->status == 'success') {
                    $app['session']->getFlashBag()->add('message', array('status' => 'success', 'text' => 'Couche créée'));
                } else {
                    $app['session']->getFlashBag()->add('message', array('status' => 'danger', 'text' => 'Erreur lors de la création de la couche'));
                }
            } else {
                $F = new OtherLayer($data['other_layer_id'], $app);
                $res = json_decode($F->update($data));
                if ($res->status == 'success') {
                    $app['session']->getFlashBag()->add('message', array('status' => 'success', 'text' => 'Couche mise à jour'));
                } else {
                    $app['session']->getFlashBag()->add('message', array('status' => 'danger', 'text' => 'Erreur lors de la mise à jour de la couche'));
                }
            }

            return $app->redirect($app['url_generator']->generate('admin-other-layers', []));
        } else {

            return $app['twig']->render('admin/adminConfForm.twig', ['U' => null, 'form' => $form->createView()]);
        }
    } else {

        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('admin-other-layer-form');





$app->match('/admin/configuration/couche/delete', function (Request $request) use ($app) {

    if ($app['session']->get('is_admin')) {

        $OL = new OtherLayer($request->query->get('ol'), $app);

        if ($OL->delete()) {
            $app['session']->getFlashBag()->add('message', array('status' => 'success', 'text' => 'Couche supprimée'));
        } else {
            $app['session']->getFlashBag()->add('message', array('status' => 'success', 'text' => 'Erreur lors de la suppresion de la couche'));
        }

        return $app->redirect($app['url_generator']->generate('admin-other-layers', []));
    } else {

        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('admin-other-layer-delete');




/* FILTERS */

$app->get('/admin/configuration/filtres', function (Request $request) use ($app) {

    if ($app['session']->get('is_admin')) {

        return $app['twig']->render('admin/adminFiltersSheet.twig', ['U' => null, 'Filters' => filterObjectList($app)]);
    } else {

        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('admin-filters');





$app->match('/admin/configuration/filtre/form', function (Request $request) use ($app) {

    if ($app['session']->get('is_admin')) {

        $f = $request->query->get('f');
        if ($f == 'new') {
            $fData = ['filter_id' => 'new'];
        } else {
            $F = new SysmaFilter($f, $app);
            $fData = get_object_vars($F);
            unset($fData['pdo']);
        }

        $form = $app['form.factory']->createBuilder('form', $fData)
            ->add('filter', 'text', array('label' => 'Nom du filtre', 'constraints' => new Assert\NotBlank()))
            ->add('object_type', 'choice', array('label' => 'Filtre applicable aux objets ou actions', 'choices' => ['sysma_object' => 'sysma_object', 'sysma_action' => 'sysma_action'], 'constraints' => new Assert\NotBlank()))
            ->add('sql', 'text', array('constraints' => new Assert\NotBlank()))
            ->add('filter_id', 'hidden')
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {

            $data = $form->getData();

            // sanitize SQL, (PDO already does the job !)
            $data['sql'] = str_replace(';', '', $data['sql']);

            if ($data['filter_id'] == 'new') {
                $F = new SysmaFilter(null, $app);
                unset($data['filter_id']);

                if ($F->insert($data)) {
                    $app['session']->getFlashBag()->add('message', array('status' => 'success', 'text' => 'Filtre créé'));
                } else {
                    $app['session']->getFlashBag()->add('message', array('status' => 'danger', 'text' => 'Erreur lors de la création du filtre'));
                }
            } else {
                $F = new SysmaFilter($data['filter_id'], $app);
                $F->update($data);
                $app['session']->getFlashBag()->add('message', array('status' => 'success', 'text' => 'Filtre mis à jour'));
            }

            return $app->redirect($app['url_generator']->generate('admin-filters', []));
        } else {

            return $app['twig']->render('admin/adminConfForm.twig', ['U' => null, 'form' => $form->createView()]);
        }
    } else {

        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('admin-filter-form');



$app->match('/admin/configuration/filtre/delete', function (Request $request) use ($app) {

    if ($app['session']->get('is_admin')) {


        $F = new SysmaFilter($request->query->get('f'), $app);

        if ($F->delete()) {
            $app['session']->getFlashBag()->add('message', array('status' => 'success', 'text' => 'Filtre supprimé'));
        } else {
            $app['session']->getFlashBag()->add('message', array('status' => 'success', 'text' => 'Erreur lors de la suppresion du filtre'));
        }


        return $app->redirect($app['url_generator']->generate('admin-filters', []));
    } else {

        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('admin-filter-delete');


/* THEME ANALYSIS */

$app->get('/admin/configuration/ta', function (Request $request) use ($app) {

    if ($app['session']->get('is_admin')) {
        return $app['twig']->render('admin/adminTAsSheet.twig', ['U' => null, 'TAs' => thematicAnalysisObjectList($app)]);
    } else {

        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('admin-ta');


$app->match('/admin/configuration/ta/form', function (Request $request) use ($app) {

    if ($app['session']->get('is_admin')) {

        $ta = $request->query->get('ta');
        if ($ta == 'new') {
            $taData = ['theme_analysis_id' => 'new'];
        } else {
            $TA = new ThematicAnalysis($ta, $app);

            $taData = [
                'theme_analysis_id' => $TA->theme_analysis_id,
                'theme_analysis' => $TA->theme_analysis,
                'object_action_type_parameter_id' => $TA->object_type . '**' . $TA->type_id . '**' . $TA->parameter_id,
                'theme_analysis_type' => $TA->theme_analysis_type,
                'style' => $TA->style
            ];
        }

        $form = $app['form.factory']->createBuilder('form', $taData)
            ->add('theme_analysis', 'text', array('label' => 'Nom de l\'analyse', 'constraints' => new Assert\NotBlank()))
            ->add('object_action_type_parameter_id', 'choice', array('label' => 'Paramètre analysé', 'choices' => loadSysmaObjectTypeActionTypeAndParameters($app), 'constraints' => new Assert\NotBlank()))
            ->add('theme_analysis_type', 'choice', array('choices' => ['classes' => 'classes', 'values' => 'values', 'gradient' => 'gradient'], 'constraints' => new Assert\NotBlank()))
            ->add('style', 'textarea', array('constraints' => new Assert\NotBlank()))
            ->add('theme_analysis_id', 'hidden')
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {

            $data = $form->getData();
            $d = explode('**', $data['object_action_type_parameter_id']);
            $data['object_type'] = $d[0];
            $data['type_id'] = $d[1];
            $data['parameter_id'] = $d[2];
            unset($data['object_action_type_parameter_id']);

            if (json_decode($data['style'])) {

                if ($data['theme_analysis_id'] == 'new') {
                    $TA = new ThematicAnalysis(null, $app);
                    unset($data['theme_analysis_id']);

                    if ($TA->create($data)) {
                        $app['session']->getFlashBag()->add('message', array('status' => 'success', 'text' => 'Analyse créée'));
                    } else {
                        $app['session']->getFlashBag()->add('message', array('status' => 'danger', 'text' => 'Erreur lors de la création de l\'analyse'));
                    }
                } else {
                    $TA = new ThematicAnalysis($data['theme_analysis_id'], $app);
                    $TA->update($data);
                    $app['session']->getFlashBag()->add('message', array('status' => 'success', 'text' => 'Analyse mise à jour'));
                }

                return $app->redirect($app['url_generator']->generate('admin-ta', []));
            } else {
                $app['session']->getFlashBag()->add('message', array('status' => 'danger', 'text' => 'Le style est mal formaté'));
            }
        }

        return $app['twig']->render('admin/adminTAForm.twig', ['U' => null, 'form' => $form->createView()]);
    } else {

        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('admin-ta-form');




$app->match('/admin/configuration/ta/delete', function (Request $request) use ($app) {

    if ($app['session']->get('is_admin')) {

        $TA = new ThematicAnalysis($request->query->get('ta'), $app);

        if ($TA->delete()) {
            $app['session']->getFlashBag()->add('message', array('status' => 'success', 'text' => 'Analyse supprimée'));
        } else {
            $app['session']->getFlashBag()->add('message', array('status' => 'success', 'text' => 'Erreur lors de la suppresion de l\'analyse'));
        }

        return $app->redirect($app['url_generator']->generate('admin-ta', []));
    } else {

        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('admin-ta-delete');



/* ORGANISATION */

$app->get('/admin/configuration/organisations', function (Request $request) use ($app) {

    if ($app['session']->get('is_admin')) {

        return $app['twig']->render('admin/adminOrganisationsSheet.twig', ['U' => null, 'organisations' => organisationObjectList($app)]);
    } else {

        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('admin-organisations');


$app->match('/admin/configuration/organisation/form', function (Request $request) use ($app) {

    if ($app['session']->get('is_admin')) {

        $o = $request->query->get('o');
        if ($o == 'new') {
            $o = ['organisation_id' => 'new'];
        } else {
            $o = get_object_vars(new Organisation($app, $o));
            unset($o['app']);
            unset($o['pdo']);
        }

        $form = $app['form.factory']->createBuilder('form', $o)
            ->add('organisation', 'text', array('label' => 'Nom complet de l\'organisation'))
            ->add('acronym', 'text', array('label' => 'Acronyme'))
            ->add('organisation_id', 'hidden', array())
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {

            $data = $form->getData();

            if ($data['organisation_id'] == 'new') {
                $O = new Organisation($app, null);
                $data['organisation_id'] = maxOrganisationId($app);
                $O->create($data);
                $app['session']->getFlashBag()->add('message', array('status' => 'success', 'text' => 'Organisation créée'));
            } else {
                $O = new Organisation($app, $data['organisation_id']);
                $O->update($data);
                $app['session']->getFlashBag()->add('message', array('status' => 'success', 'text' => 'Organisation mise à jour'));
            }

            return $app->redirect($app['url_generator']->generate('admin-organisations', []));
        }

        return $app['twig']->render('admin/adminConfForm.twig', ['U' => null, 'form' => $form->createView()]);
    } else {

        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('admin-organisation-form');





$app->match('/admin/configuration/organisation/delete', function (Request $request) use ($app) {

    if ($app['session']->get('is_admin')) {


        $O = new Organisation($app, $request->query->get('o'));
        if ($O->countSysmaActions() == 0 and $O->countSysmaObjects() == 0) {
            if ($O->delete()) {
                $app['session']->getFlashBag()->add('message', array('status' => 'success', 'text' => 'Organisation supprimée'));
            } else {
                $app['session']->getFlashBag()->add('message', array('status' => 'success', 'text' => 'Cette organisation ne peut être supprimée car elle est liée à ' . $O->countSysmaObjects() . ' objets et ' . $O->countSysmaActions() . ' actions'));
            }
        }

        return $app->redirect($app['url_generator']->generate('admin-organisations', []));
    } else {

        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('admin-organisation-delete');


/* CONFIGURATION */


$app->get('/admin/configuration/application', function (Request $request) use ($app) {

    if ($app['session']->get('is_admin')) {

        return $app['twig']->render('admin/adminConfSheet.twig', ['U' => null, 'params' => loadAppParameters($app)]);
    } else {

        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('admin-app-conf');



$app->match('/admin/configuration/application/form', function (Request $request) use ($app) {

    if ($app['session']->get('is_admin')) {

        $p = loadAppParameter($app, $request->query->get('p'));

        $form = $app['form.factory']->createBuilder('form', [$p['alias'] => $p['value']]);

        $paramType = array('int' => 'integer', 'text' => 'text', 'json' => 'text', 'numeric' => 'number');

        if ($p['value_choices'] == null) {
            $form->add($p['alias'], $paramType[$p['data_type']], array('label' => $p['parameter'], 'constraints' => new Assert\NotBlank()));
        } else {

            $choices = json_decode($p['value_choices'], true);
            $form->add($p['alias'], 'choice', array('choices' => $choices, 'label' => $p['parameter'], 'constraints' => new Assert\NotBlank()));
        }

     

        $theForm = $form->getForm();
        $theForm->handleRequest($request);

        if ($theForm->isValid()) {

            $data = $theForm->getData();

            if ($p['data_type'] == 'json') {
                if (json_decode($data[$p['alias']]) == null) {
                    $app['session']->getFlashBag()->add('message', array('status' => 'danger', 'text' => 'Le json n\'est pas valide'));
                    return $app['twig']->render('admin/adminConfForm.twig', ['U' => null, 'form' => $theForm->createView()]);
                }
            }            

            if (updateAppParameter($app, $p['alias'], $data[$p['alias']])) {
                $app['session']->getFlashBag()->add('message', array('status' => 'success', 'text' => 'Paramètre mis à jour'));
                return $app->redirect($app['url_generator']->generate('admin-app-conf', []));
            } else {
                $app['session']->getFlashBag()->add('message', array('status' => 'danger', 'text' => 'Erreur lors de la mise à jour du paramètre'));
                return $app->redirect($app['url_generator']->generate('admin-app-conf', []));
            }
        }

        return $app['twig']->render('admin/adminConfForm.twig', ['U' => null, 'form' => $theForm->createView()]);
    } else {

        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('admin-app-conf-form');
