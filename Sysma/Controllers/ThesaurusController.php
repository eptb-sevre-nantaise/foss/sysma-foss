<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

$app->get('/dictionnaire', function (Request $request) use ($app) {

    $U = loadUserFromSession($app, $request);

    if ($app['session']->get('user_id') != null and $app['session']->get('user_id') != ANONYMID) {

        return $app['twig']->render('thesaurus/thesaurus.twig', ['to' => objectTypesList($app), 'rs' => relationTypesList($app)]);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('thesaurus');


$app->get('/dictionnaire/sysma-type-id/{type}', function (Request $request, $type) use ($app) {

    $U = loadUserFromSession($app, $request);


    if ($app['session']->get('user_id') != null and $app['session']->get('user_id') != ANONYMID) {

        $redir = explode('|', $type);
        return $app->redirect($app['url_generator']->generate($redir[0], ['id' => $redir[1]]));
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('thesaurus-type');


$app->get('/dictionnaire/print', function (Request $request) use ($app) {

    if ($app['session']->get('user_id') != null and $app['session']->get('user_id') != ANONYMID) {

        if ($request->get('sysma_object_type_id') != null) {


            $tt = explode(',', $request->get('sysma_object_type_id'));
            foreach ($tt as $t) {

                $tempTO['sysma_object_type_id'] = intval($t);
                $tabTO[] = $tempTO;
            }
        } else {

            $tabTO = objectTypesList($app, ['filter' => ' and hidden = 0']);
        }


        foreach ($tabTO as $TO) {


            if (
                (hasAccess($app, $app['session']->get('user_id'), null, $TO['sysma_object_type_id'], null, null, null, null, 'lecteur')
                    or hasAccess($app, $app['session']->get('user_id'), null, $TO['sysma_object_type_id'], null, null, null, null, 'contributeur'))
                and !in_array($TO['sysma_object_type_id'], explode('|', $request->query->get('exclus')))
            ) {


                $TyO = new SysmaObjectType($TO['sysma_object_type_id'], $app);

                $TyO->loadParameters();
                $TyO->loadSysmaActionTypes();

                foreach ((array) $TyO->sysmaActionTypesArray as $TT) {


                    if (hasAccess($app, $app['session']->get('user_id'), null, $TyO->sysma_object_type_id, null, $TT->sysma_action_type_id, null, null, 'lecteur') or hasAccess($app, $app['session']->get('user_id'), null, $TyO->sysma_object_type_id, null, $TT->sysma_action_type_id, null, null, 'contributeur')) {
                        $TT->loadParameters();
                    }
                }

                $tabObjTO[] = $TyO;
            }
        }

        return $app['twig']->render('thesaurus/thesaurusPrint.twig', ['to' => $tabObjTO, 'simple' => true]);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('thesaurus-print');



$app->get('/dictionnaire/sysma-object-type/{id}', function ($id) use ($app) {

    if (hasAccess($app, $app['session']->get('user_id'), null, $id, null, null, null, null, 'lecteur') or hasAccess($app, $app['session']->get('user_id'), null, $id, null, null, null, null, 'contributeur')) {
        $TyO = new SysmaObjectType($id, $app);
        $TyO->loadParameters();
        $TyO->loadSysmaActionTypes();
        $TyO->loadSysmaRelationTypes();
        return $app['twig']->render('thesaurus/thesaurusSysmaObjectType.twig', ['TyO' => $TyO, 'to' => objectTypesList($app), 'rs' => relationTypesList($app)]);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('sysma-object-type')->assert('id', '\d+');



$app->get('/dictionnaire/sysma-object-type/{id}/json', function ($id) use ($app) {

    if (hasAccess($app, $app['session']->get('user_id'), null, $id, null, null, null, null, 'lecteur') or hasAccess($app, $app['session']->get('user_id'), null, $id, null, null, null, null, 'contributeur')) {
        $TyO = new SysmaObjectType($id, $app);
        $TyO->loadParameters();
        $TyO->loadSysmaActionTypes(true);
        unset($TyO->app);
        unset($TyO->pdo);

        return $app->json($TyO);
    } else {
        $rc = new stdClass;
        $rc->status = ACCESS_NOT_GRANTED;
        return $app->json($rc);
    }
})->bind('sysma-object-type-json')->assert('id', '\d+');



$app->match('/dictionnaire/sysma-object-type/{id}/update', function ($id, Request $request) use ($app) {

    if ($app['session']->get('is_admin')) {

        if ($id == 'new') {
            $TyO['sysma_object_type_id'] = 'new';
            $TyO['load_on_pan'] = 0;
            $TyO['hidden'] = 0;
            $TyO['zoom_min'] = 1;
            $TyO['zoom_max'] = 20;
            $TyO['style'] = '{ "color":"red", "opacity":"0.95", "weight":"1","fillOpacity" : "0.20", "fillColor":"red" }';
        } else {
            $TyO = buildSimpleEntity('sysma_object_type', 'sysma_object_type_id', $id, $app);
        }
        //  var_dump($TyO['load_on_pan']);

        $form = $app['form.factory']->createBuilder('form', $TyO)
            ->add('sysma_object_type', 'text', array('label' => 'Type d\'objet (nom complet)', 'constraints' => new Assert\NotBlank()))
            ->add('sysma_object_type_alias', 'text', array('label' => 'Alias du type d\'objet (sans espace, accent ou caractères spéciaux)', 'constraints' => new Assert\NotBlank()))
            ->add('description', 'textarea')
            ->add('definition_source', 'textarea', array('label' => 'Source de la définition'))
            ->add('data_source', 'textarea', array('label' => 'Sources des données', 'constraints' => new Assert\NotBlank()))
            ->add('digitization_layer', 'choice', array('constraints' => new Assert\NotBlank(), 'label' => 'Support privilégié de numérisation', 'choices' => array('aucun' => 'Aucun', 'orthophotos' => 'Photos aériennes', 'scan25' => 'Carte IGN SCAN 25', 'parcellaire' => 'Parcellaire'), 'expanded' => true))
            ->add('geometry_type', 'choice', array('constraints' => new Assert\NotBlank(), 'label' => 'Type de géométrie', 'choices' => array('ST_Point' => 'Point', 'ST_Linestring' => 'Ligne', 'ST_Polygon' => 'Polygone', 'no_geom' => 'Objet non géographique'), 'expanded' => true,))
            ->add('sysma_object_type_id', 'hidden', array('constraints' => new Assert\NotBlank()))
            ->add('style', 'text', array('constraints' => new Assert\NotBlank()))
            ->add('group_alias', 'choice', array('choices' => sysmaObjectTypeGroupsList($app), 'label' => 'Groupe'))
            ->add('load_on_pan', 'choice', array('constraints' => new Assert\NotBlank(), 'choices' => ['1' => 'Oui', '0' => 'Non'], 'label' => 'Rechargement de la couche lors du zoom'))
            ->add('zoom_min', 'integer', array('constraints' => new Assert\NotBlank(), 'label' => 'Niveau de zoom minimum à partir duquel afficher la couche (1 minimum)'))
            ->add('zoom_max', 'integer', array('constraints' => new Assert\NotBlank(), 'label' => 'Niveau de zoom maximum à partir duquel afficher la couche (20 maximum)'))
            ->add('hidden', 'choice', array('constraints' => new Assert\NotBlank(), 'choices' => array(0 => 'Non', 1 => 'Oui'), 'label' => 'Masquer dans l\'export du dictionnaire'))
            ->getForm();
        $form->handleRequest($request);

        if ($form->isValid()) {

            $data = $form->getData();

            if ($id == 'new') {
                $data['sysma_object_type_id'] = dataIndex($app['pdo']);
                $data['modified_by'] = $app['session']->get('user_id');
                $choice = sysmaObjectTypeGroupsList($app);
                $TyO = new SysmaObjectType(null, $app);
                $res = $TyO->create($data);
                $tabres = json_decode($res);
                $newid = $tabres->id;
            } else {
                $data['modified_at'] = date('Y-m-d H:i:s');
                $data['modified_by'] = $app['session']->get('user_id');
                unset($data['thesaurus_item_metadata']);

                $TyO = new SysmaObjectType($data['sysma_object_type_id'], $app);
                $TyO->update($data);
                $newid = $data['sysma_object_type_id'];
            }


            return $app->redirect('/dictionnaire/sysma-object-type/' . $newid);
        }

        return $app['twig']->render('thesaurus/thesaurusSysmaObjectTypeModificationForm.twig', ['TyO' => $TyO, 'form' => $form->createView(), 'to' => objectTypesList($app), 'rs' => relationTypesList($app)]);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('thesaurus-sysma-object-type-update');

// popup de confirmation DELETE TO
// Tiny controller to ask confirmation before deleting an object type
$app->match('/dictionnaire/sysma-object-type/{id}/confirm-delete', function ($id, Request $request) use ($app) {
    if ($app['session']->get('is_admin')) {

        $sot = new SysmaObjectType(null, $app); // dummy sot object
        $nbSysmaObjects = $sot->countSysmaObjects($id);
        $nbSysmaActions = $sot->countLinkedSysmaActions($id);

        if (($nbSysmaObjects > 0) || ($nbSysmaActions > 0)) {
            // confirm lvl 2
            $msg = "Attention! Il existe " . $nbSysmaObjects . " objets et " . $nbSysmaActions . " actions liés à ce type d'objet et des type d'action. Etes-vous sur de vouloir supprimer tous ces objets, ces actions, ces types d'action avec ce type d'objet? Cette suppression est définitive.";
        } else {
            // confirm lvl 1
            $msg = "Etes vous sur de vouloir supprimer ce type d'objet?";
        }

        $url = $app['url_generator']->generate('thesaurus-sysma-object-type-delete', ['id' => $id]);
        $actionOnClick = "sendData(null, '" . $url . "', 'modal-content', event, null);";
        return $app['twig']->render('modal-popups/confirm.twig',  [
            'message' => $msg, 'action_on_click_if_confirmed' => $actionOnClick
        ]);
    } else {
        $erreur['message'] = "Vous n'êtes pas authorisé à supprimer cet objet";
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('thesaurus-sysma-object-type-confirm-delete');


// DELETE TO
$app->match('/dictionnaire/sysma-object-type/{id}/delete', function ($id, Request $request) use ($app) {

    if ($app['session']->get('is_admin')) {

        $sot = new SysmaObjectType($id, $app);
        $sot->loadParameters();
        $sot->loadSysmaActionTypes(true);
        $res = $sot->delete(); // delete attempt
        $rc = json_decode($res, true);

        if ($rc['status'] == 'success') {
            // cleanup session infos
            $u = new User($app['session']->get('user_id'), $app);
            $u->removeInfosFromUserSession('userObjectTypeSelection', $id);
            // TODO? TOCHECK : rm sat session infos also?
            if (!empty($sot->sysmaActionTypesArrayVal)) {
                foreach ($sot->sysmaActionTypesArrayVal as $satId) {
                    $u->removeInfosFromUserSession('userSysmaActionTypeSelection', $satId);
                }
            }

            $actionOnClick = 'location.replace("' . $app['url_generator']->generate('thesaurus') . '")';
            return $app['twig']->render(
                'modal-popups/info.twig',
                ['message' => 'Le type objet ' . $sot->sysma_object_type . ' a été supprimé', 'action_on_click' => $actionOnClick]
            );
        } elseif ($rc['status'] == 'error') {
            return $app['twig']->render(
                'modal-popups/warning.twig',
                ['message' => 'Echec de la suppression. Erreur : ' . $res, 'action_on_click' => ';']
            );
        } else {
            return $app['twig']->render(
                'modal-popups/warning.twig',
                [
                    'message' => var_dump($res) . '<br>' . var_dump($rc) . '<br>' . 'Echec de la suppression.', 'action_on_click' => ';'
                ]
            );
        }
        // we shouldn't fall in here ever
        return $app->redirect('/dictionnaire');
    } else {
        $erreur['message'] = "Vous n'êtes pas authorisé à supprimer cet objet";
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('thesaurus-sysma-object-type-delete');

// popup de confirmation DELETE TO PARAMETER
// Tiny controller to ask confirmation before deleting an object parameter
$app->match('/dictionnaire/sysma-object-type/{id}/parameter/{idp}/confirm-delete', function ($id, $idp, Request $request) use ($app) {
    if ($app['session']->get('is_admin')) {

        $sot = new SysmaObjectType(null, $app); // dummy sot object
        $nbSysmaObjects = $sot->countSysmaObjects($id);
        // $nbSysmaObjects = "???";

        $sop = new SysmaObjectParameter(null, $app); // dummy sop object
        $nbParams = $sop->countSysmaObjectParameters($idp);

        if (($nbSysmaObjects > 0) || ($nbParams > 0)) {
            // confirm lvl 2
            $msg = "Attention! Il existe " . $nbSysmaObjects . " objets et " . $nbParams . " données de paramètre liés à ce paramètre. Etes-vous sur de vouloir supprimer tous ces données de paramètre avec ce paramètre? Cette suppression est définitive.";
        } else {
            // confirm lvl 1
            $msg = "Aucune données lié à ce paramètre. Etes vous sur de vouloir supprimer ce paramètre?";
        }

        $url = $app['url_generator']->generate('thesaurus-sysma-object-type-parameter-delete', ['id' => $id, 'idp' => $idp]);
        $actionOnClick = "sendData(null, '" . $url . "', 'modal-content', event, null);";
        return $app['twig']->render('modal-popups/confirm.twig',  [
            'message' => $msg, 'action_on_click_if_confirmed' => $actionOnClick
        ]);
    } else {
        $erreur['message'] = "Vous n'êtes pas authorisé à supprimer ce paramètre";
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('thesaurus-sysma-object-type-parameter-confirm-delete');

// DELETE TO PARAMETER
$app->match('/dictionnaire/sysma-object-type/{id}/parameter{idp}/delete', function ($id, $idp, Request $request) use ($app) {

    if ($app['session']->get('is_admin')) {

        $sop = new SysmaObjectParameter($idp, $app);
        $res = $sop->delete(); // delete attempt
        $rc = json_decode($res, true);

        if ($rc['status'] == 'success') {

            $actionOnClick = 'location.replace("' . $app['url_generator']->generate('sysma-object-type', ['id' => $id]) . '")';
            return $app['twig']->render(
                'modal-popups/info.twig',
                ['message' => 'Le paramètre ' . $sop->parameter . ' a été supprimé', 'action_on_click' => $actionOnClick]
            );
        } elseif ($rc['status'] == 'error') {
            return $app['twig']->render(
                'modal-popups/warning.twig',
                ['message' => 'Echec de la suppression. Erreur : ' . $res, 'action_on_click' => ';']
            );
        } else {
            return $app['twig']->render(
                'modal-popups/warning.twig',
                [
                    'message' => var_dump($res) . '<br>' . var_dump($rc) . '<br>' . 'Echec de la suppression.', 'action_on_click' => ';'
                ]
            );
        }
        // we shouldn't fall in here ever
        return $app->redirect('/dictionnaire');
    } else {
        $erreur['message'] = "Vous n'êtes pas authorisé à supprimer cet objet";
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('thesaurus-sysma-object-type-parameter-delete');


$app->match('/dictionnaire/sysma-object-type/{id}/parameter/{idp}/update', function ($id, $idp, Request $request) use ($app) {

    if ($app['session']->get('is_admin')) {

        if ($idp == 'new') {
            $P['sysma_object_type_id'] = $id;
            $P['parameter_id'] = 'new';
            $P['data_source'] = ORGANISATION_ACRONYM;
        } else {
            $P = buildSimpleEntity('sysma_object_type_parameter', 'parameter_id', $idp, $app);
        }

        $form = $app['form.factory']->createBuilder('form', $P)
            ->add('parameter', 'text', array('label' => 'Paramètre', 'constraints' => new Assert\NotBlank()))
            ->add('parameter_alias', 'text', array('constraints' => new Assert\NotBlank(), 'label' => 'Alias SIG - nom de la colonne dans les exports SIG', 'attr' => ['maxlength' => 10]))
            ->add('description', 'textarea')
            ->add('parameter_source', 'textarea', array('label' => 'Source de la définition'))
            ->add('data_source', 'textarea', array('label' => 'Source des données', 'constraints' => new Assert\NotBlank()))
            ->add('data_type', 'choice', array('label' => 'Type de données', 'constraints' => new Assert\NotBlank(), 'choices' => dataTypeChoices($app), 'expanded' => true,))
            ->add('ordered_by', 'text', array('label' => 'Ordre d\'affichage (possibilité de nommer un groupe de paramètre : 001**Nom du groupe)'))
            ->add('unit', 'text', array('label' => 'Unité'))
            ->add('required_field_parameter', 'choice', array('choices' => array('non' => 'Non', 'oui a renseigner' => 'Oui, à renseigner', 'oui a renseigner obligatoire' => 'Oui, à renseigner obligatoirement', 'oui en lecture' => 'Oui, à afficher (non modifiable)'), 'label' => 'Paramètre à utiliser sur le terrain'))
            ->add('required', 'choice', array('label' => 'Paramètre à renseigner obligatoirement',  'choices' => array('0' => 'Non', '1' => 'Oui')))
            ->add('time_tracking', 'choice', array('label' => 'Suivi dans le temps', 'choices' => array('0' => 'Non', '1' => 'Oui')))
            ->add('hidden_in_form', 'choice', array('label' => 'Paramètre caché', 'choices' => array('non' => 'Non', 'oui' => 'Oui')))
            ->add('sysma_object_type_id', 'hidden', array('constraints' => new Assert\NotBlank()))
            ->add('parameter_id', 'hidden', array('constraints' => new Assert\NotBlank()))
            ->getForm();
        $form->handleRequest($request);

        if ($form->isValid()) {

            $data = $form->getData();


            if ($idp == 'new') {
                $data['parameter_id'] = dataIndex($app['pdo']);
                $data['modified_at'] = $data['created_at'] = date('Y-m-d H:i:s');
                $data['modified_by'] = $app['session']->get('user_id');
                $P = new SysmaObjectParameter(null, $app);;
                $res = json_decode($P->create($data));
                                
            } else {
                unset($data['created_at']) ;
                $data['modified_at'] = date('H-m-d H:i:s');
                $data['modified_by'] = $app['session']->get('user_id');
                $P = new SysmaObjectParameter($idp, $app);
                $res = json_decode($P->update($data));
                $res = json_decode($P->update($data));
            }


            if ($res->status == 'success') {        
              
                return $app->redirect('/dictionnaire/sysma-object-type/' . $id);
            } else {
                $app['session']->getFlashBag()->add('message', array('text' => 'Problème lors de la mise à jour du paramètre', 'status' => 'danger'));
            }

            

           
        }
        return $app['twig']->render('thesaurus/thesaurusSysmaObjectTypeParameterModificationForm.twig', ['P' => $P, 'form' => $form->createView(), 'to' => objectTypesList($app), 'rs' => relationTypesList($app)]);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('thesaurus-object-type-parameter-modify');



// LIEN OBJET, DEFINIR TYPE OBJET
$app->match('/dictionnaire/sysma-object-type/{ido}/parameter/{id}/objet-link/update', function ($ido, $id, Request $request) use ($app) {

    $TO = new SysmaObjectParameter($id, $app);

    if ($app['session']->get('is_admin')) {
        $form = $app['form.factory']->createBuilder('form')
            ->add('parameter_id', 'hidden', array('data' => $id, 'constraints' => new Assert\NotBlank()))
            ->add('value_type', 'hidden', array('data' => 'sysmaObjectLinkType', 'constraints' => new Assert\NotBlank()))
            ->add('choice_id', 'hidden', array('data' => $TO->sysmaObjectLinked['choice_id']))
            ->add('display_order', 'hidden', array('data' => 0, 'constraints' => new Assert\NotBlank()))
            ->add('choice', 'choice', array('choices' => objectTypesListForSelect($app), 'data' => $TO->sysmaObjectLinked['nom'] . '|' . $TO->sysmaObjectLinked['description'], 'constraints' => new Assert\NotBlank()))
            ->getForm();
        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();
            $data['modified_by'] = $app['session']->get('user_id');
            $P = new SysmaObjectParameter($id, $app);
            if ($TO->sysmaObjectLinked['nom'] == null) {
                unset($data['choice_id']);
                $P->createChoice($data);
            } else {
                $c = explode('|', $data['choice']);
                $data['choice'] = $c[0];
                $data['description'] = $c[1];
                $P->updateChoice($data);
            }
            return $app->redirect('/dictionnaire/sysma-object-type/' . $ido);
        }
        return $app['twig']->render('thesaurus/thesaurusSysmaObjectTypeParameterChoiceModificationForm.twig', ['C' => null, 'idp' => $id, 'ido' => $ido, 'form' => $form->createView(), 'to' => objectTypesList($app), 'rs' => relationTypesList($app)]);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('thesaurus-object-type-parameter-object-type-link-update');



// CHOIX

$app->match('/dictionnaire/sysma-object-type/{ido}/parameter/{id}/choice/{idc}/update', function ($ido, $id, $idc, Request $request) use ($app) {

    if ($app['session']->get('is_admin')) {

        if ($idc == 'new') {
            $C = null;
        } else {
            $C = buildSimpleEntity('sysma_object_type_parameter_choice', 'choice_id', $idc, $app);
        }

        $form = $app['form.factory']->createBuilder('form', $C)
            ->add('parameter_id', 'hidden', array('data' => $id, 'constraints' => new Assert\NotBlank()))
            ->add('choice_id', 'hidden', array('data' => $idc, 'constraints' => new Assert\NotBlank()))
            ->add('choice', 'text', array('label' => 'Choix', 'constraints' => new Assert\NotBlank()))
            ->add('description', 'textarea')
            ->add('display_order', 'integer', array('label' => 'Ordre d\'affichage', 'constraints' => new Assert\NotBlank()))
            ->getForm();
        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();
            if ($idc == 'new') {
                unset($data['choice_id']);
                $data['modified_by'] = $app['session']->get('user_id');
                $P = new SysmaObjectParameter($id, $app);
                $P->createChoice($data);
            } else {
                $data['modified_by'] = $app['session']->get('user_id');
                $P = new SysmaObjectParameter($id, $app);
                $P->updateChoice($data);
            }

            return $app->redirect('/dictionnaire/sysma-object-type/' . $ido);
        }

        return $app['twig']->render('thesaurus/thesaurusSysmaObjectTypeParameterChoiceModificationForm.twig', ['C' => $C, 'idp' => $id, 'ido' => $ido, 'form' => $form->createView(), 'to' => objectTypesList($app), 'rs' => relationTypesList($app)]);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('thesaurus-object-type-parameter-choice-update');


// supprimer TO PARAMETER CHOICE
$app->match('/dictionnaire/sysma-object-type/{ido}/parameter/{id}/choice/{idc}/delete', function ($ido, $id, $idc) use ($app) {
    if ($app['session']->get('is_admin')) {
        $P = new SysmaObjectParameter($id, $app);
        $P->deleteChoice($idc);
        return $app->redirect('/dictionnaire/sysma-object-type/' . $ido);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('thesaurus-object-type-parameter-choice-delete');


/********************************************************************************************/
// TYPE ACTION
$app->get('/dictionnaire/sysma-action-type/{id}', function ($id) use ($app) {

    $TT = new SysmaActionType($id, $app);
    $TT->loadParameters();
    $TyO = new SysmaObjectType($TT->sysma_object_type_id, $app);
    if (hasAccess($app, $app['session']->get('user_id'), null, $TT->sysma_object_type_id, null, $id, null, null, 'lecteur') or hasAccess($app, $app['session']->get('user_id'), null, $TT->sysma_object_type_id, null, $id, null, null, 'contributeur')) {
        return $app['twig']->render('thesaurus/thesaurusSysmaActionType.twig', ['TyO' => $TyO, 'TT' => $TT, 'to' => objectTypesList($app), 'rs' => relationTypesList($app)]);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('thesaurus-action-type')->assert('id', '\d+');


// popup de confirmation DELETE TA
// Tiny controller to ask confirmation before deleting an action type
$app->match('/dictionnaire/sysma-action-type/{id}/confirm-delete', function ($id, Request $request) use ($app) {
    if ($app['session']->get('is_admin')) {

        $sat = new SysmaActionType(null, $app); // dummy sot object
        $nbSysmaActions = $sat->countSysmaActions($id);
        // $nbSysmaActions = $sot->countLinkedSysmaActions($id);

        // if ( ($nbSysmaObjects>0) || ($nbSysmaActions>0) ) {
        if ($nbSysmaActions > 0) {
            // confirm lvl 2
            $msg = "Attention! Il existe " . $nbSysmaActions . " actions liés à ce type d'action. Etes-vous sur de vouloir supprimer toutes ces actions avec ce type d'action? Cette suppression est définitive.";
        } else {
            // confirm lvl 1
            $msg = "Etes vous sur de vouloir supprimer ce type d'action?";
        }

        $url = $app['url_generator']->generate('thesaurus-sysma-action-type-delete', ['id' => $id]);
        $actionOnClick = "sendData(null, '" . $url . "', 'modal-content', event, null);";
        return $app['twig']->render('modal-popups/confirm.twig',  [
            'message' => $msg, 'action_on_click_if_confirmed' => $actionOnClick
        ]);
    } else {
        $erreur['message'] = "Vous n'êtes pas authorisé à supprimer cet objet";
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('thesaurus-sysma-action-type-confirm-delete');

// DELETE TA
$app->match('/dictionnaire/sysma-action-type/{id}/delete', function ($id, Request $request) use ($app) {

    if ($app['session']->get('is_admin')) {

        $sat = new SysmaActionType($id, $app);
        $parentSotId = $sat->getLinkedSysmaObjectType();
        $sat->loadParameters();
        // $sot->loadSysmaActionTypes(true);
        $res = $sat->delete(); // delete attempt
        $rc = json_decode($res, true);

        if ($rc['status'] == 'success') {
            // cleanup session infos
            $u = new User($app['session']->get('user_id'), $app);
            $u->removeInfosFromUserSession('userSysmaActionTypeSelection', $id);

            $actionOnClick = 'location.replace("' . $app['url_generator']->generate('sysma-object-type', ['id' => $parentSotId]) . '")';
            return $app['twig']->render(
                'modal-popups/info.twig',
                ['message' => 'Le type action ' . $sat->sysma_action_type . ' a été supprimé', 'action_on_click' => $actionOnClick]
            );
        } elseif ($rc['status'] == 'error') {
            return $app['twig']->render(
                'modal-popups/warning.twig',
                ['message' => 'Echec de la suppression. Erreur : ' . $res, 'action_on_click' => ';']
            );
        } else {
            return $app['twig']->render(
                'modal-popups/warning.twig',
                [
                    'message' => var_dump($res) . '<br>' . var_dump($rc) . '<br>' . 'Echec de la suppression.', 'action_on_click' => ';'
                ]
            );
        }
        // we shouldn't fall in here ever
        return $app->redirect('/dictionnaire');
    } else {
        $erreur['message'] = "Vous n'êtes pas authorisé à supprimer cet objet";
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('thesaurus-sysma-action-type-delete');

// popup de confirmation DELETE TA PARAMETER
// Tiny controller to ask confirmation before deleting an action parameter
$app->match('/dictionnaire/sysma-action-type/{id}/parameter/{idp}/confirm-delete', function ($id, $idp, Request $request) use ($app) {
    if ($app['session']->get('is_admin')) {

        $sat = new SysmaActionType(null, $app); // dummy sat object
        $nbSysmaActions = $sat->countSysmaActions($id);

        $sap = new SysmaActionParameter(null, $app); // dummy sap object
        $nbParams = $sap->countSysmaActionParameters($idp);

        if (($nbSysmaActions > 0) || ($nbParams > 0)) {
            // confirm lvl 2
            $msg = "Attention! Il existe " . $nbSysmaActions . " actions et " . $nbParams . " données de paramètre liés à ce paramètre. Etes-vous sur de vouloir supprimer tous ces données de paramètre avec ce paramètre? Cette suppression est définitive.";
        } else {
            // confirm lvl 1
            $msg = "Aucune données lié à ce paramètre. Etes vous sur de vouloir supprimer ce paramètre?";
        }

        $url = $app['url_generator']->generate('thesaurus-sysma-action-type-parameter-delete', ['id' => $id, 'idp' => $idp]);
        $actionOnClick = "sendData(null, '" . $url . "', 'modal-content', event, null);";
        return $app['twig']->render('modal-popups/confirm.twig',  [
            'message' => $msg, 'action_on_click_if_confirmed' => $actionOnClick
        ]);
    } else {
        $erreur['message'] = "Vous n'êtes pas authorisé à supprimer ce paramètre";
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('thesaurus-sysma-action-type-parameter-confirm-delete');

// DELETE TA PARAMETER
$app->match('/dictionnaire/sysma-action-type/{id}/parameter{idp}/delete', function ($id, $idp, Request $request) use ($app) {

    if ($app['session']->get('is_admin')) {

        $sap = new SysmaActionParameter($idp, $app);
        $res = $sap->delete(); // delete attempt
        $rc = json_decode($res, true);

        if ($rc['status'] == 'success') {

            $actionOnClick = 'location.replace("' . $app['url_generator']->generate('thesaurus-action-type', ['id' => $id]) . '")';
            return $app['twig']->render(
                'modal-popups/info.twig',
                ['message' => 'Le paramètre ' . $sap->parameter . ' a été supprimé', 'action_on_click' => $actionOnClick]
            );
        } elseif ($rc['status'] == 'error') {
            return $app['twig']->render(
                'modal-popups/warning.twig',
                ['message' => 'Echec de la suppression. Erreur : ' . $res, 'action_on_click' => ';']
            );
        } else {
            return $app['twig']->render(
                'modal-popups/warning.twig',
                [
                    'message' => var_dump($res) . '<br>' . var_dump($rc) . '<br>' . 'Echec de la suppression.', 'action_on_click' => ';'
                ]
            );
        }
        // we shouldn't fall in here ever
        return $app->redirect('/dictionnaire');
    } else {
        $erreur['message'] = "Vous n'êtes pas authorisé à supprimer cet objet";
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('thesaurus-sysma-action-type-parameter-delete');



$app->match('/dictionnaire/sysma-object-type/{ido}/sysma-action-type/{id}/update', function ($id, $ido, Request $request) use ($app) {

    if ($app['session']->get('is_admin')) {

        if ($id == 'new') {
            $TT['sysma_object_type_id'] = $ido;
            $TT['sysma_action_type_id'] = 'new';
            $TT['style'] = '{ "color":"red", "opacity":"0.95", "weight":"1","fillOpacity" : "0.20", "fillColor":"white" }';
            $TT['load_on_pan'] = 0;
            $TT['zoom_min'] = 1;
            $TT['zoom_max'] = 20;
            $TT['unit_cost'] = 0;
        } else {
            $TT = buildSimpleEntity('sysma_action_type', 'sysma_action_type_id', $id, $app);
        }



        $form = $app['form.factory']->createBuilder('form', $TT)
            ->add('sysma_action_type', 'text', array('label' => 'Type d\'action', 'constraints' => new Assert\NotBlank()))
            ->add('sysma_action_type_alias', 'text', array('label' => 'Alias du type d\'action (sans espace, accent ni caractères spéciaux)', 'constraints' => new Assert\NotBlank()))
            ->add('description', 'textarea')
            ->add('dig_justification', 'textarea', array('required' => false))
            ->add('dig_incidences_phase_travaux', 'textarea', array('required' => false))
            ->add('dig_incidences_fonctionnement', 'textarea', array('required' => false))
            ->add('dig_prescriptions_et_mesures_accompagnement', 'textarea', array('required' => false))
            ->add('dig_reference_nomenclature', 'textarea', array('required' => false))
            ->add('unit_cost', 'number', array('label' => 'Coût unitaire indicatif en € ' . HT_TTC . ' (laisser à 0 pour ne pas gérer le coût)'))
            ->add('unit_measure', 'choice', array('label' => 'Unité', 'choices' => $GLOBALS['UNIT_MEASURES']))
            ->add('sysma_object_type_id', 'hidden', array('constraints' => new Assert\NotBlank()))
            ->add('sysma_action_type_id', 'hidden', array('constraints' => new Assert\NotBlank()))
            ->add('style', 'text', array('constraints' => new Assert\NotBlank()))
            ->add('load_on_pan', 'choice', array('choices' => ['1' => 'Oui', '0' => 'Non'], 'label' => 'Rechargement de la couche lors du zoom'))
            ->add('zoom_min', 'integer', array('label' => 'Niveau de zoom minimum à partir duquel afficher la couche (1 minimum)'))
            ->add('zoom_max', 'integer', array('label' => 'Niveau de zoom maximum à partir duquel afficher la couche (20 maximum)'))
            ->getForm();
        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();

            if ($id == 'new') {
                $data['sysma_action_type_id'] = dataIndex($app['pdo']);
                $data['created_at'] = $data['modified_at'] = date('Y-m-d H:i:s');
                $data['modified_by'] = $app['session']->get('user_id');
                $TT = new SysmaActionType(null, $app);
                $res = $TT->create($data);
                $tabres = json_decode($res);
                $newid = $tabres->id;
            } else {
                $TT = new SysmaActionType($data['sysma_action_type_id'], $app);

                $data['modified_at'] = date('Y-m-d H:i:s');
                $data['modified_by'] = $app['session']->get('user_id');
                unset($data['extra_data']);
                $TT->update($data);
                $newid = $id;
            }


            return $app->redirect($app['url_generator']->generate('thesaurus-action-type', ['id' => $newid]));
        }

        return $app['twig']->render('thesaurus/thesaurusSysmaActionTypeModificationForm.twig', ['TT' => $TT, 'form' => $form->createView(), 'to' => objectTypesList($app), 'rs' => relationTypesList($app)]);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('thesaurus-action-type-update');







$app->match('/dictionnaire/sysma-action-type/{id}/parameter/{idp}/update', function ($id, $idp, Request $request) use ($app) {

    if ($app['session']->get('is_admin')) {
        if ($idp == 'new') {
            $P['parameter_id'] = 'new';
            $P['sysma_action_type_id'] = $id;
            $P['data_source'] = ORGANISATION_ACRONYM;
        } else {
            $P = buildSimpleEntity('sysma_action_type_parameter', 'parameter_id', $idp, $app);
        }

        $form = $app['form.factory']->createBuilder('form', $P)
            ->add('parameter', 'text', array('label' => 'Paramètre', 'constraints' => new Assert\NotBlank()))
            ->add('parameter_alias', 'text', array('label' => 'Alias du paramètre (sans espace, accent ou caractères spéciaux)', 'attr' => ['maxlength' => 10]))
            ->add('description', 'textarea')
            ->add('parameter_source', 'textarea', array('label' => 'Source de la définition'))
            ->add('data_source', 'textarea', array('label' => 'Source des données', 'constraints' => new Assert\NotBlank()))
            ->add('data_type', 'choice', array('label' => 'Type de données', 'choices' => dataTypeChoices($app), 'expanded' => true))
            ->add('ordered_by', 'text', array('label' => 'Ordre d\'affichage'))
            ->add('unit', 'text', array('label' => 'Unité'))
            ->add('required', 'choice', array('label' => 'Paramètre à renseigner obligatoirement', 'choices' => array('0' => 'Non', '1' => 'Oui')))
            ->add('sysma_action_type_id', 'hidden', array('constraints' => new Assert\NotBlank()))
            ->add('parameter_id', 'hidden', array('constraints' => new Assert\NotBlank()))
            ->getForm();
        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();
            if ($idp == 'new') {
                $data['parameter_id'] = dataIndex($app['pdo']);
                $data['modified_at'] = $data['created_at'] = date('Y-m-d H:i:s');
                $data['modified_by'] = $app['session']->get('user_id');


                $P = new SysmaActionParameter(null, $app);
                $res = $P->create($data);
                $tabres = json_decode($res);
            } else {
                $data['modified_at'] = date('Y-m-d H:i:s');
                $data['modified_by'] = $app['session']->get('user_id');


                $P = new SysmaActionParameter($idp, $app);
                $P->update($data);
            }


            return $app->redirect('/dictionnaire/sysma-action-type/' . $id);
        }
        return $app['twig']->render('thesaurus/thesaurusSysmaActionTypeParameterModificationForm.twig', ['P' => $P, 'form' => $form->createView(), 'to' => objectTypesList($app), 'rs' => relationTypesList($app)]);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('thesaurus-action-type-parameter-update');






// LIEN OBJET, DEFINIR TYPE OBJET
$app->match('/dictionnaire/sysma-action-type/{id}/parameter/{idp}/objet-link/update', function ($id, $idp, Request $request) use ($app) {

    $TA = new SysmaActionParameter($idp, $app);

    if ($app['session']->get('is_admin')) {
        $form = $app['form.factory']->createBuilder('form')
            ->add('parameter_id', 'hidden', array('data' => $idp, 'constraints' => new Assert\NotBlank()))
            ->add('value_type', 'hidden', array('data' => 'sysmaObjectLinkType', 'constraints' => new Assert\NotBlank()))
            ->add('choice_id', 'hidden', array('data' => $TA->sysmaObjectLinked['choice_id']))
            ->add('display_order', 'hidden', array('data' => 0, 'constraints' => new Assert\NotBlank()))
            ->add('choice', 'choice', array('choices' => objectTypesListForSelect($app), 'data' => $TA->sysmaObjectLinked['nom'] . '|' . $TA->sysmaObjectLinked['description'], 'constraints' => new Assert\NotBlank()))
            ->getForm();
        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();

           
            $data['modified_by'] = $app['session']->get('user_id');
            $P = new SysmaActionParameter($idp, $app);
            if ($TA->sysmaObjectLinked['nom'] == null) {
                unset($data['choice_id']);
                $P->createChoice($data);

               
            } else {
                $c = explode('|', $data['choice']);
                $data['choice'] = $c[0];
                $data['description'] = $c[1];
                $P->updateChoice($data);
            }
            return $app->redirect('/dictionnaire/sysma-action-type/' . $id);
        }
        return $app['twig']->render('thesaurus/thesaurusSysmaActionTypeParameterChoiceModificationForm.twig', ['C' => null, 'idp' => $idp, 'ido' => $id, 'form' => $form->createView(), 'to' => objectTypesList($app), 'rs' => relationTypesList($app)]);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('thesaurus-action-type-parameter-object-type-link-update');



$app->match('/dictionnaire/sysma-action-type/{ido}/parameter/{id}/choice/{idc}/update', function ($ido, $id, $idc, Request $request) use ($app) {
    if ($app['session']->get('is_admin')) {

        if ($idc == 'new') {
            $C['parameter_id'] = $id;
        } else {
            $C = buildSimpleEntity('sysma_action_type_parameter_choice', 'choice_id', $idc, $app);
        }

        $form = $app['form.factory']->createBuilder('form', $C)
            ->add('parameter_id', 'hidden', array('data' => $id, 'constraints' => new Assert\NotBlank()))
            ->add('choice_id', 'hidden', array('data' => $idc, 'constraints' => new Assert\NotBlank()))
            ->add('choice', 'text', array('label' => 'Choix', 'constraints' => new Assert\NotBlank()))
            ->add('description', 'textarea')
            ->add('display_order', 'integer', array('label' => 'Ordre d\'affichage', 'constraints' => new Assert\NotBlank()))
            ->getForm();
        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();

            if ($idc == 'new') {
                unset($data['choice_id']);
                $data['modified_by'] = $app['session']->get('user_id');
                $P = new SysmaActionParameter($id, $app);
                $P->createChoice($data);
            } else {
                $data['modified_by'] = $app['session']->get('user_id');
                $P = new SysmaActionParameter($id, $app);
                $P->updateChoice($data);
            }

            return $app->redirect('/dictionnaire/sysma-action-type/' . $ido);
        }

        return $app['twig']->render('thesaurus/thesaurusSysmaActionTypeParameterChoiceModificationForm.twig', ['C' => $C, 'idp' => $id, 'ido' => $ido, 'form' => $form->createView(), 'to' => objectTypesList($app), 'rs' => relationTypesList($app)]);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('thesaurus-action-type-parameter-choice-update');


$app->match('/dictionnaire/sysma-action-type/{ido}/parameter/{id}/choice/{idc}/delete', function ($ido, $id, $idc) use ($app) {
    if ($app['session']->get('is_admin')) {
        $P = new SysmaActionParameter($id, $app);
        $P->deleteChoice($idc);
        return $app->redirect('/dictionnaire/sysma-action-type/' . $ido);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('thesaurus-action-type-parameter-choice-delete');




$app->get('/dictionnaire/type-sysma-relation/{id}', function ($id) use ($app) {

    if ($app['session']->get('is_admin')) {
        $R = new SysmaRelationType($id, $app);
        $R->loadParameters();

        return $app['twig']->render('thesaurus/thesaurusSysmaRelationType.twig', ['R' => $R, 'to' => objectTypesList($app), 'rs' => relationTypesList($app)]);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('thesaurus-sysma-relation-type')->assert('id', '\d+');



$app->match('/dictionnaire/type-sysma-relation/{id}/update', function ($id, Request $request) use ($app) {

    if ($app['session']->get('is_admin')) {

        if ($id == 'new') {
            $TyO['sysma_relation_type_id'] = 'new';
            $P['data_source'] = ORGANISATION_ACRONYM;
        } else {
            $TyO = buildSimpleEntity('sysma_relation_type', 'sysma_relation_type_id', $id, $app);
        }


        $form = $app['form.factory']->createBuilder('form', $TyO)
            ->add('sysma_relation_type', 'text', array('label' => 'Intitulé du type de relation (nom complet, ex : "Propriété")', 'constraints' => new Assert\NotBlank()))
            ->add('sysma_relation_type_alias', 'text', array('label' => 'Alias du type de relation (sans espace, accent ou caractères spéciaux)', 'constraints' => new Assert\NotBlank()))
            ->add('sysma_relation_type_from_parent', 'text', array('label' => 'Intitulé du type de relation du parent vers l\'enfant, ex : "est propriétaire de"', 'constraints' => new Assert\NotBlank()))
            ->add('sysma_relation_type_from_child', 'text', array('label' => 'Intitulé du type de relation de l\'enfant vers le parent, ex : "est détenu par"', 'constraints' => new Assert\NotBlank()))
            ->add('relation_type', 'choice', array('constraints' => new Assert\NotBlank(), 'label' => 'Type de relation', 'choices' => array('1..1' => '[ 1 <=> 1 ] 1 objet parent relié à 1 objet enfant', '1..*' => '[ 1 <=> n ] 1 objet parent relié à plusieurs objets enfants', '*..1' => '[ n <=> 1 ] Plusieurs objets parents reliés à un objet enfant', '*..*' => '[ n <=> n ] Plusieurs objets parents reliés à plusieurs objets enfants'), 'expanded' => true,))
            ->add('description', 'textarea')
            ->add('definition_source', 'textarea', array('label' => 'Source de la définition'))
            ->add('data_source', 'textarea', array('label' => 'Sources des données', 'constraints' => new Assert\NotBlank()))
            ->add('sysma_relation_type_id', 'hidden', array('constraints' => new Assert\NotBlank()))
            ->add('hidden', 'choice', array('constraints' => new Assert\NotBlank(), 'choices' => array(0 => 'Non', 1 => 'Oui'), 'label' => 'Masquer dans l\'export du dictionnaire'))
            ->getForm();
        $form->handleRequest($request);

        if ($form->isValid()) {

            $data = $form->getData();

            if ($id == 'new') {
                $data['sysma_relation_type_id'] = dataIndex($app['pdo']);
                $data['modified_by'] = $app['session']->get('user_id');
                $TyO = new SysmaRelationType(null, $app);
                $res = $TyO->create($data);
                $tabres = json_decode($res);

                $newid = $tabres->id;
            } else {
                $data['modified_at'] = date('H-m-d H:i:s');
                $data['modified_by'] = $app['session']->get('user_id');
                $TyO = new SysmaRelationType($data['sysma_relation_type_id'], $app);
                $TyO->update($data);
                $newid = $data['sysma_relation_type_id'];
            }

            return $app->redirect('/dictionnaire/type-sysma-relation/' . $newid);
        }

        return $app['twig']->render('thesaurus/thesaurusSysmaRelationTypeModificationForm.twig', ['TyO' => $TyO, 'form' => $form->createView(), 'to' => objectTypesList($app), 'rs' => relationTypesList($app)]);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('thesaurus-sysma-relation-type-update');




$app->match('/dictionnaire/type-sysma-relation/{id}/parameter/{idp}/update', function ($id, $idp, Request $request) use ($app) {

    if ($app['session']->get('is_admin')) {

        if ($idp == 'new') {
            $P['sysma_relation_type_id'] = $id;
            $P['parameter_id'] = 'new';
        } else {
            $P = buildSimpleEntity('sysma_relation_type_parameter', 'parameter_id', $idp, $app);
        }

        $form = $app['form.factory']->createBuilder('form', $P)
            ->add('parameter', 'text', array('label' => 'Paramètre', 'constraints' => new Assert\NotBlank()))
            ->add('description', 'textarea')
            ->add('parameter_source', 'textarea', array('label' => 'Source de la définition'))
            ->add('data_source', 'textarea', array('label' => 'Source des données', 'constraints' => new Assert\NotBlank()))
            ->add('data_type', 'choice', array('label' => 'Type de données', 'choices' => dataTypeChoices($app), 'expanded' => true))
            ->add('ordered_by', 'text', array('label' => 'Ordre d\'affichage'))
            ->add('unit', 'text', array('label' => 'Unité'))
            ->add('parameter_alias', 'text', array('constraints' => new Assert\NotBlank(), 'label' => 'Alias SIG - nom de la colonne dans les exports SIG', 'attr' => ['maxlength' => 10]))
            ->add('required_field_parameter', 'choice', array('choices' => array('non' => 'Non', 'oui a renseigner' => 'Oui, à renseigner', 'oui a renseigner obligatoire' => 'Oui, à renseigner obligatoirement', 'oui en lecture' => 'Oui, à afficher (non modifiable)'), 'label' => 'Champ à compléter sur le terrain'))
            ->add('required', 'choice', array('label' => 'Paramètre à renseigner obligatoirement', 'choices' => array('0' => 'Non', '1' => 'Oui')))
            ->add('time_tracking', 'choice', array('label' => 'Suivi dans le temps', 'choices' => array('0' => 'Non', '1' => 'Oui')))
            ->add('hidden_in_form', 'choice', array('label' => 'Champ caché', 'choices' => array('non' => 'Non', 'oui' => 'Oui')))
            ->add('sysma_relation_type_id', 'hidden', array('constraints' => new Assert\NotBlank()))
            ->add('parameter_id', 'hidden', array('constraints' => new Assert\NotBlank()))
            ->getForm();
        $form->handleRequest($request);

        if ($form->isValid()) {

            $data = $form->getData();


            if ($idp == 'new') {
                $data['parameter_id'] = dataIndex($app['pdo']);
                $data['modified_at'] = $data['created_at'] = date('Y-m-d H:i:s');
                $data['modified_by'] = $app['session']->get('user_id');
                $P = new SysmaRelationParameter(null, $app);;
                $res = $P->create($data);
                $tabres = json_decode($res);
            } else {
                $data['modified_at'] = date('H-m-d H:i:s');
                $data['modified_by'] = $app['session']->get('user_id');
                $P = new SysmaRelationParameter($idp, $app);
                $P->update($data);
            }

            return $app->redirect('/dictionnaire/type-sysma-relation/' . $id);
        }
        return $app['twig']->render('thesaurus/thesaurusSysmaRelationTypeParameterModificationForm.twig', ['P' => $P, 'form' => $form->createView(), 'to' => objectTypesList($app), 'rs' => relationTypesList($app)]);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('thesaurus-relation-type-parameter-update');



$app->match('/dictionnaire/type-sysma-relation/{ido}/parameter/{id}/choice/{idc}/update', function ($ido, $id, $idc, Request $request) use ($app) {

    if ($app['session']->get('is_admin')) {

        if ($idc == 'new') {
            $C = null;
        } else {
            $C = buildSimpleEntity('sysma_relation_type_parameter_choice', 'choice_id', $idc, $app);
        }

        $form = $app['form.factory']->createBuilder('form', $C)
            ->add('parameter_id', 'hidden', array('data' => $id, 'constraints' => new Assert\NotBlank()))
            ->add('choice_id', 'hidden', array('data' => $idc, 'constraints' => new Assert\NotBlank()))
            ->add('choice', 'text', array('label' => 'Choix', 'constraints' => new Assert\NotBlank()))
            ->add('description', 'textarea')
            ->add('display_order', 'integer', array('label' => 'Ordre d\'affichage', 'constraints' => new Assert\NotBlank()))
            ->getForm();
        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();

            if ($idc == 'new') {
                unset($data['choice_id']);
                $data['modified_by'] = $app['session']->get('user_id');
                $P = new SysmaRelationParameter($id, $app);
                $res = json_decode($P->createChoice($data));
            } else {
                $data['modified_by'] = $app['session']->get('user_id');
                $P = new SysmaRelationParameter($id, $app);
                $res = json_decode($P->updateChoice($data));
            }

            return $app->redirect('/dictionnaire/type-sysma-relation/' . $ido);
        }

        return $app['twig']->render('thesaurus/thesaurusSysmaRelationTypeParameterChoiceModificationForm.twig', ['C' => $C, 'idp' => $id, 'ido' => $ido, 'form' => $form->createView(), 'to' => objectTypesList($app), 'rs' => relationTypesList($app)]);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('thesaurus-relation-type-parameter-choice-update');


$app->match('/dictionnaire/type-sysma-relation/{ido}/parameter/{id}/choice/{idc}/delete', function ($ido, $id, $idc) use ($app) {
    if ($app['session']->get('is_admin')) {
        $P = new SysmaRelationParameter($id, $app);
        $P->deleteChoice($idc);
        return $app->redirect('/dictionnaire/type-sysma-relation/' . $ido);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('thesaurus-relation-type-parameter-choice-delete');


$app->match('dictionnaire/sysma-object-type/{id}/type-sysma-relation/{idr}', function (Request $request, $id, $idr) use ($app) {

    if ($app['session']->get('is_admin')) {

        if ($idr == 'new') {
            $R = null;
        } else {
            $R = buildSimpleEntity('l_sysma_object_type_sysma_relation_type', 'l_sysma_object_type_sysma_relation_type_id', $idr, $app);
        }

        $form = $app['form.factory']->createBuilder('form', $R)
            ->add('sysma_relation_type_id', 'choice', array('label' => 'Type de relation', 'choices' => relationTypesListForForm($app), 'constraints' => new Assert\NotBlank()))
            ->add('sysma_object_type_id_parent', 'choice', array('label' => 'Type d\'objet parent', 'choices' => simpleObjectTypesListForSelect($app), 'constraints' => new Assert\NotBlank()))
            ->add('sysma_object_type_id_child', 'choice', array('label' => 'Type d\'objet enfant', 'choices' => simpleObjectTypesListForSelect($app), 'constraints' => new Assert\NotBlank()))
            ->add('display_order', 'text', array('label' => 'Ordre d\'affichage'))
            ->getForm();
        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();
            $data['modified_by'] = $app['session']->get('user_id');

            $TyO = new SysmaObjectType($id, $app);

            if ($idr == 'new') {
                $TyO->createRelation($data);
                return $app->redirect('/dictionnaire/sysma-object-type/' . $id);
            } else {
                $TyO->updateRelation($idr, $data);
                return $app->redirect('/dictionnaire/sysma-object-type/' . $id);
            }
        }
        return $app['twig']->render('thesaurus/thesaurusSysmaObjectTypeSysmaRelationTypeRelationModificationForm.twig', ['form' => $form->createView(), 'idr' => $idr, 'to' => objectTypesList($app), 'rs' => relationTypesList($app)]);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('thesaurus-sysma-object-type-sysma-relation-type-update');
