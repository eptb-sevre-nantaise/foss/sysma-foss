<?php

use Symfony\Component\HttpFoundation\Request;

$app->get('/geojson/objecttype/{sysma_object_type_id}', function (Request $request, $sysma_object_type_id) use ($app) {

    $pdo = $app['pdo'];
    $r = $request->query->all();
    $r['zoom'] = intval($r['zoom']) ;

    $qry = null;

    $lop = objectTypeLayerLoadOnPan($app, $sysma_object_type_id);


    if ($lop['geometry_type'] == 'no_geom') {
        return $app->json(array());
    }

    if (isset($r['center']) and isset($r['zoom']) and $r['center'] != 'undefined' and $r['zoom'] != 'undefined' and $lop['load_on_pan'] == '1') {


        if (
            $r['zoom'] < $lop['zoom_min']
            or $r['zoom'] > $lop['zoom_max']
        ) {
            return '{"type":"FeatureCollection","features":[]}';
        } else {
            $center = explode(',', substr($r['center'], 7, -1));
            $center[0] = trim($center[0]);
            $center[1] = trim($center[1]);

            $qry = 'with
                area_download_geom as (
                select ST_buffer(
                ST_Transform(
                ST_SetSRID(ST_MakePoint(' . $center[1] . ', ' . $center[0] . '), 4326), 2154), ' . $GLOBALS['ZOOM_CONVERSION_ARRAY1'][$r['zoom']] . ') as geom
                )
                select
                \'o\'||o.sysma_object_id as id,
                o.sysma_object as name,
                \'objet\'::text as type, 
                ST_AsGeoJSON((ST_Dump(ST_Transform(ST_simplify(o.geom, ' . $GLOBALS['ZOOM_CONVERSION_ARRAY2'][$r['zoom']] . '), 4326))).geom) as geojson,
                \'layer_object_type\'||o.sysma_object_type_id as layer_id,
                t.geometry_type as type_geom,
                t.sysma_object_type as object_type,
                case when o.end_date = \'0\' then xx_99_utils.json_merge(t.style, s.style) else \'{"color":"#999999","opacity":0.6,"weight":3,"fillColor":"#999999","fillOpacity":0.7}\' end as style
                from ' . SCHEMA . '.sysma_object as o
                LEFT JOIN ' . SCHEMA . '.sysma_object_type as t ON o.sysma_object_type_id = t.sysma_object_type_id
                INNER JOIN ' . SCHEMA . '.user_privilege as d
                ON
                (o.sysma_object_type_id = ANY(d.sysma_object_type_id_list) or d.sysma_object_type_id_list = \'{0}\') and
                d.user_id = :id_util and
                (d.sysma_object_status_list = \'{tous}\' or o.status = ANY(d.sysma_object_status_list)) and
                (d.sysma_object_organisation_id_list = \'{0}\' or o.organisation_id = ANY(d.sysma_object_organisation_id_list)) and
                (\'lecteur\' = ANY(d.privilege_list) or (\'contributeur\' = ANY(d.privilege_list) and o.modified_by = d.user_id))
                LEFT JOIN ' . SCHEMA . '.l_style_user as s ON s.user_id = d.user_id and s.object_type = \'sysma_object\' and s.type_id = t.sysma_object_type_id
                LEFT JOIN area_download_geom as a on true
                where o.sysma_object_type_id = :sysma_object_type_id
                and o.geom && a.geom';
        }
    } else {

        $qry .= '
        select 
        \'o\'||o.sysma_object_id as id, 
        o.sysma_object as name,
        \'objet\'::text as type,      
        \'layer_object_type\'||o.sysma_object_type_id as layer_id,
        t.geometry_type as type_geom,
        t.sysma_object_type as object_type,
        ST_AsGeoJSON((ST_Dump(ST_Transform(ST_SnapToGrid(o.geom,1),4326))).geom) as geojson, 
        case when o.end_date = \'0\' then xx_99_utils.json_merge(t.style,s.style) else \'{"color":"#999999","opacity":0.6,"weight":2,"fillColor":"#999999","fillOpacity":0.7}\' end as style
        from ' . SCHEMA . '.sysma_object as o 
        LEFT JOIN ' . SCHEMA . '.sysma_object_type as t ON o.sysma_object_type_id = t.sysma_object_type_id 
        INNER JOIN ' . SCHEMA . '.user_privilege as d
        ON 
        (o.sysma_object_type_id = ANY(d.sysma_object_type_id_list) or d.sysma_object_type_id_list = \'{0}\') and 
        d.user_id = :id_util and
        (d.sysma_object_status_list = \'{tous}\' or o.status = ANY(d.sysma_object_status_list)) and 
        (d.sysma_object_organisation_id_list = \'{0}\' or o.organisation_id = ANY(d.sysma_object_organisation_id_list)) and 
        (\'lecteur\' = ANY(d.privilege_list) or (\'contributeur\' = ANY(d.privilege_list) and o.modified_by = d.user_id))         
        LEFT JOIN ' . SCHEMA . '.l_style_user as s ON s.user_id = d.user_id and s.object_type = \'sysma_object\' and s.type_id = t.sysma_object_type_id
        where  o.sysma_object_type_id = :sysma_object_type_id';
    }



    $qry .= buildSQLFilter('sysma_object', $sysma_object_type_id, $app);


    $qry .= ' order by st_area(o.geom) desc';


    //echo $qry ;

    $sttmt = $pdo->prepare($qry);



    $idu = $app['session']->get('user_id');
    $sttmt->bindParam(':id_util', $idu, PDO::PARAM_INT);
    $sttmt->bindParam(':sysma_object_type_id', $sysma_object_type_id, PDO::PARAM_INT);

    $sttmt->execute();

    if ($sttmt->rowCount() == 0)
        return '{"type":"FeatureCollection","features":[]}';


    // TA ?

    $qry2 = $pdo->prepare('select * from ' . SCHEMA . '.l_theme_analysis_user l '
        . 'JOIN ' . SCHEMA . '.theme_analysis at ON at.theme_analysis_id = l.theme_analysis_id and at.type_id = :sysma_object_type_id and object_type = \'sysma_object\' '
        . 'where l.user_id = :id_util');


    $qry2->bindParam(':id_util', $idu, PDO::PARAM_INT);
    $qry2->bindParam(':sysma_object_type_id', $sysma_object_type_id, PDO::PARAM_INT);
    $qry2->execute();


    if ($qry2->rowCount() > 0) {

        $rs = array();
        // TA
        $at = $qry2->fetchObject();

        $AT = new ThematicAnalysis($at->theme_analysis_id, $app);

        // AT de type classes
        $options = array();
        if ($AT->theme_analysis_type === 'gradient') {

            $minmax = findMinMax('sysma_object', $AT->parameter_id, $app);

            $options['min'] = $minmax->min;
            $options['max'] = $minmax->max;
            $options['classes'] = $AT->calculateTAClasses($options['min'], $options['max']);
        }


        while ($ob = $sttmt->fetchObject()) {

            $V = new SysmaObjectParameterValue(null, $app);
            $V->buildFromParameters(substr($ob->id, 1), $AT->parameter_id, null, $app);
            if ($V->parameter->dataType == 'multipleTextChoiceType' and $V->value != null and count($V->value > 0)) {
                $ob->value = implode(',', $V->value);
                $ob->style = $AT->apply($ob->value, $options);
            } else {
                $ob->value = $V->value;
                $ob->style = $AT->apply($V->value, $options);
            }
            if ($ob->style!==null) {                
                array_push($rs, get_object_vars($ob));
            }
        }
    } else {
        $rs = $sttmt->fetchAll(PDO::FETCH_ASSOC);
    }


    return $app->json(json_decode(toGeoJSON($rs)));
    //  return $app['twig']->render('geojson/geojson.twig', ['geojson' => toGeoJSON($rs)]);
});


/*
 * Request that filters objects depending on the user rights on object types and work types
 * WARNING : only controls contrib. rights on object modifier id (not on works)
 */

$app->get('/geojson/actiontype/{sysma_action_type_id}', function (Request $request, $sysma_action_type_id) use ($app) {

    $pdo = $app['pdo'];
    $r = $request->query->all();
    $r['zoom'] = intval($r['zoom']) ;

    $qry = null;
    $lop = workTypeLayerLoadOnPan($app, $sysma_action_type_id);

    if (isset($r['center']) and isset($r['zoom']) and $r['center'] != 'undefined' and $r['zoom'] != 'undefined' and $lop['load_on_pan'] == '1') {



        if (
            $r['zoom'] < $lop['zoom_min']
            or $r['zoom'] > $lop['zoom_max']
        ) {
            return '{"type":"FeatureCollection","features":[]}';
        } else {
            $center = explode(',', substr($r['center'], 7, -1));
            $center[0] = trim($center[0]);
            $center[1] = trim($center[1]);

            $qry = 'with
                area_download_geom as (
                select ST_buffer(
                ST_Transform(
                ST_SetSRID(ST_MakePoint(' . $center[1] . ', ' . $center[0] . '), 4326), 2154), ' . $GLOBALS['ZOOM_CONVERSION_ARRAY1'][$r['zoom']] . ') as geom
                )
                
                select 
                \'t\'||o.sysma_object_id||\'_\'||t.sysma_action_id as id, 
                o.sysma_object as name, 
                \'action\' as type, 
                tyt.sysma_action_type as object_type, 
                ST_AsGeoJSON((ST_Dump(ST_Transform(ST_simplify(o.geom, ' . $GLOBALS['ZOOM_CONVERSION_ARRAY2'][$r['zoom']] . '), 4326))).geom) as geojson,
                \'layertypesysma_action\'||t.sysma_action_type_id as layer_id  ,
                ty.geometry_type as type_geom,
                xx_99_utils.json_merge(tyt.style,s.style) as style
                
                from ' . SCHEMA . '.sysma_action as t 
                    
                JOIN ' . SCHEMA . '.l_sysma_object_sysma_action as l ON t.sysma_action_id = l.sysma_action_id 
                JOIN ' . SCHEMA . '.sysma_object as o ON l.sysma_object_id = o.sysma_object_id 
                JOIN ' . SCHEMA . '.sysma_object_type as ty ON o.sysma_object_type_id = ty.sysma_object_type_id 
                JOIN ' . SCHEMA . '.sysma_action_type as tyt ON t.sysma_action_type_id = tyt.sysma_action_type_id                 
                INNER JOIN ' . SCHEMA . '.user_privilege as d 
                    ON 
                    (o.sysma_object_type_id = ANY(d.sysma_object_type_id_list) or d.sysma_object_type_id_list = \'{0}\') and 
                    d.user_id = :id_util and 
                    (d.sysma_object_status_list = \'{tous}\' or o.status = ANY(d.sysma_object_status_list)) and 
                    (d.sysma_action_type_id_list = \'{0}\' or t.sysma_action_type_id = ANY(d.sysma_action_type_id_list)) and 
                    (d.sysma_action_status_list = \'{tous}\' or t.status = ANY(d.sysma_action_status_list)) and 
                    (d.sysma_object_organisation_id_list = \'{0}\' or o.organisation_id = ANY(d.sysma_object_organisation_id_list)) and 
                    (d.sysma_action_organisation_id_list = \'{0}\' or o.organisation_id = ANY(d.sysma_action_organisation_id_list)) and 
                    (\'lecteur\' = ANY(d.privilege_list) or (\'contributeur\' = ANY(d.privilege_list)  and o.modified_by = d.user_id)) 
                
                LEFT JOIN ' . SCHEMA . '.l_style_user as s 
                    ON s.user_id = d.user_id and s.object_type = \'sysma_action\' and s.type_id = tyt.sysma_action_type_id 
                LEFT JOIN area_download_geom as a on true
                where t.sysma_action_type_id = :sysma_action_type_id 
                and  o.geom && a.geom';
        }
    } else {

        $qry = 'select 
                \'t\'||o.sysma_object_id||\'_\'||t.sysma_action_id as id, 
                \'t\'||t.sysma_action_id as sysma_action_id, 
                o.sysma_object as name, 
                \'action\' as type, 
                tyt.sysma_action_type as object_type, 
                ST_AsGeoJSON((ST_Dump(ST_Transform(geom,4326))).geom) as geojson, 
                \'layertypesysma_action\'||t.sysma_action_type_id as layer_id  , 
                ty.geometry_type as type_geom, 
                xx_99_utils.json_merge(tyt.style,s.style) as style 
                
                from ' . SCHEMA . '.sysma_action as t 
                    
                JOIN ' . SCHEMA . '.l_sysma_object_sysma_action as l ON t.sysma_action_id = l.sysma_action_id 
                JOIN ' . SCHEMA . '.sysma_object as o ON l.sysma_object_id = o.sysma_object_id 
                JOIN ' . SCHEMA . '.sysma_object_type as ty ON o.sysma_object_type_id = ty.sysma_object_type_id 
                JOIN ' . SCHEMA . '.sysma_action_type as tyt ON t.sysma_action_type_id = tyt.sysma_action_type_id                 
                INNER JOIN ' . SCHEMA . '.user_privilege as d 
                ON 
                (o.sysma_object_type_id = ANY(d.sysma_object_type_id_list) or d.sysma_object_type_id_list = \'{0}\') and 
                d.user_id = :id_util and 
                (d.sysma_object_status_list = \'{tous}\' or o.status = ANY(d.sysma_object_status_list)) and 
                (d.sysma_action_type_id_list = \'{0}\' or t.sysma_action_type_id = ANY(d.sysma_action_type_id_list)) and 
                (d.sysma_action_status_list = \'{tous}\' or t.status = ANY(d.sysma_action_status_list)) and 
                (d.sysma_object_organisation_id_list = \'{0}\' or o.organisation_id = ANY(d.sysma_object_organisation_id_list)) and 
                (d.sysma_action_organisation_id_list = \'{0}\' or o.organisation_id = ANY(d.sysma_action_organisation_id_list)) and 
                (\'lecteur\' = ANY(d.privilege_list) or (\'contributeur\' = ANY(d.privilege_list)  and o.modified_by = d.user_id)) 
                
                LEFT JOIN ' . SCHEMA . '.l_style_user as s ON s.user_id = d.user_id and s.object_type = \'sysma_action\' and s.type_id = tyt.sysma_action_type_id 
                where t.sysma_action_type_id = :sysma_action_type_id';
    }

    $qry .= buildSQLFilter('sysma_action', $sysma_action_type_id, $app);
    $qry .= ' order by st_area(o.geom) desc';


    //  echo $qry ; 

    $sttmt = $pdo->prepare($qry);

    $idu = $app['session']->get('user_id');
    $sttmt->bindParam(':id_util', $idu, PDO::PARAM_INT);
    $sttmt->bindParam(':sysma_action_type_id', $sysma_action_type_id, PDO::PARAM_INT);
    $sttmt->execute();

    if ($sttmt->rowCount() == 0)
        return '{"type":"FeatureCollection","features":[]}';
    // var_dump($pdo->errorInfo());
   // $rs = $sttmt->fetchAll(PDO::FETCH_ASSOC);


    // TA ?

    $qry2 = $pdo->prepare('select * from ' . SCHEMA . '.l_theme_analysis_user l '
        . 'JOIN ' . SCHEMA . '.theme_analysis at ON at.theme_analysis_id = l.theme_analysis_id and at.type_id = :sysma_action_type_id and object_type = \'sysma_action\' '
        . 'where l.user_id = :id_util');


    $qry2->bindParam(':id_util', $idu, PDO::PARAM_INT);
    $qry2->bindParam(':sysma_action_type_id', $sysma_action_type_id, PDO::PARAM_INT);
    $qry2->execute();


    if ($qry2->rowCount() > 0) {


        $rs = array();
        // TA
        $at = $qry2->fetchObject();

        $AT = new ThematicAnalysis($at->theme_analysis_id, $app);

        // AT de type classes
        $options = array();
        if ($AT->theme_analysis_type === 'gradient') {

          
            $minmax = findMinMax('sysma_action', $AT->parameter_id, $app);
         

            $options['min'] = $minmax->min;
            $options['max'] = $minmax->max;
            $options['classes'] = $AT->calculateTAClasses($options['min'], $options['max']);

        }


        while ($ob = $sttmt->fetchObject()) {

            $V = new SysmaActionParameterValue(null, $app);
            $V->buildFromParameters(substr($ob->sysma_action_id, 1), $AT->parameter_id, $app);
            if ($V->parameter->dataType == 'multipleTextChoiceType' and $V->value != null and count($V->value > 0)) {
                $ob->value = implode(',', $V->value);
                $ob->style = $AT->apply($ob->value, $options);
            } else {               
                $ob->value = $V->value;
                $ob->style = $AT->apply($V->value, $options);
            }
            if ($ob->style!==null) {                
                array_push($rs, get_object_vars($ob));
            }
        }
    } else {
        $rs = $sttmt->fetchAll(PDO::FETCH_ASSOC);
    }


    return $app->json(json_decode(toGeoJSON($rs)));

    //return $app['twig']->render('geojson/geojson.twig', ['geojson' => toGeoJSON($rs)]);
});



$app->get('/geojson/objet/{sysma_object_id}', function ($sysma_object_id) use ($app) {

    $O = new SysmaObject($sysma_object_id, $app);

    if (hasAccess($app, $app['session']->get('user_id'), $O->organisation_id, $O->sysma_object_type_id, $O->status, null, null, null, 'lecteur')) {

        $pdo = $app['pdo'];
        $qry = $pdo->prepare('select \'i\'||o.sysma_object_id as id, o.sysma_object as name, \'objet\' as type,ST_AsGeoJSON((ST_Dump(ST_Transform(o.geom,4326))).geom) as geojson, t.geometry_type as type_geom from ' . SCHEMA . '.sysma_object as o JOIN ' . SCHEMA . '.sysma_object_type as t ON o.sysma_object_type_id = t.sysma_object_type_id  where sysma_object_id = :id');
        $qry->bindParam(':id', $sysma_object_id);
        $qry->execute();
        $rs = $qry->fetchAll(PDO::FETCH_ASSOC);

        return $app->json(json_decode(toGeoJSON($rs)));
        //return $app['twig']->render('geojson/geojson.twig', ['geojson' => toGeoJSON($rs)]);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à l\'objet ' . $sysma_object_id;
        return $app['twig']->render('geojson/error.twig', ['erreur' => $erreur]);
    }
});



$app->get('/geojson/piwigo_layer/1', function () use ($app) {


    if ($app['session']->get('user_id') != ANONYMID and PHOTO_PWG_MODE === 'on') {


        $qry = $app['pdo.pwg']->prepare(''
            . 'select concat(\'p\',id) as id, name, concat(\'{"type":"Point","coordinates":[\',longitude,\',\',latitude,\']}\') as geojson, path, date_creation, author, \'photo\' as type,\'photo\' as objecttype , \'{"color":"#00ffff","weight":"1","opacity":"1","fillOpacity":"0.3","radius":"3"}\' as style '
            . 'from images where latitude is not null and longitude is not null');

        $qry->execute();
        $rs = array();
        while ($res = $qry->fetchObject()) {
            $vars = get_object_vars($res);
            $path = $vars['path'];
            $date_creation = $vars['date_creation'];
            $author = $vars['author'];
            $infosp = explode(".", $path);
            $nb_car_fin = 3;
            if ($infosp[count($infosp) - 1] == 'jpg' or $infosp[count($infosp) - 1] == 'JPG') {
                $nb_car_fin = 3;
            }
            if ($infosp[count($infosp) - 1] == 'jpeg' or $infosp[count($infosp) - 1] == 'JPEG') {
                $nb_car_fin = 4;
            }

            $vars['description'] = '<div width="500px !important;">                
                <a href="http:////photos.sevre-nantaise.com/i.php?' . substr(str_replace(',', '-', $path), 1, (strlen($path) - $nb_car_fin - 2)) . '-la.' . $infosp[count($infosp) - 1] . '" target="_blank">
                <img width="300px" src="http://photos.sevre-nantaise.com/i.php?' . substr(str_replace(',', '-', $path), 1, (strlen($path) - $nb_car_fin - 2)) . '-xs.' . $infosp[count($infosp) - 1] . '" data-src="http://photos.sevre-nantaise.com/i.php?' . substr(str_replace(',', '-', $path), 1, (strlen($path) - $nb_car_fin - 2)) . '-xs.' . $infosp[count($infosp) - 1] . '" /"/>
                </a>
                 <p>' . dateDisplay($date_creation) . ' ' . utf8_decode($author) . '</p>               
                </div>';

            $vars['name'] = utf8_decode($vars['name']);
            //unset($vars['name']) ;
            unset($vars['path']);
            unset($vars['date_creation']);
            unset($vars['author']);

            array_push($rs, $vars);
        }

        return $app->json(json_decode(toGeoJSON($rs)));
        //return $app['twig']->render('geojson/geojson.twig', ['geojson' => toGeoJSON($rs)]);
    } else {
        $erreur['message'] = 'erreur';
        return $app['twig']->render('geojson/error.twig', ['erreur' => $erreur]);
    }
});





$app->get('/geojson/photo/{photo}', function ($photo) use ($app) {


    if ($app['session']->get('user_id') != ANONYMID and PHOTO_PWG_MODE === 'on') {


        $qry = $app['pdo.pwg']->prepare(''
            . 'select concat(\'p\',id) as id, name, concat(\'{"type":"Point","coordinates":[\',longitude,\',\',latitude,\']}\') as geojson, path, start_date, author, \'photo\' as type,\'photo\' as objecttype  from images where id = :photo and latitude is not null and longitude is not null');
        $qry->bindParam(':photo', $photo, PDO::PARAM_INT);
        $qry->execute();
        $rs = array();
        while ($res = $qry->fetchObject()) {
            $vars = get_object_vars($res);
            $path = $vars['path'];
            $start_date = $vars['start_date'];
            $author = $vars['author'];
            $infosp = explode(".", $path);
            $nb_car_fin = 3;
            if ($infosp[count($infosp) - 1] == 'jpg' or $infosp[count($infosp) - 1] == 'JPG') {
                $nb_car_fin = 3;
            }
            if ($infosp[count($infosp) - 1] == 'jpeg' or $infosp[count($infosp) - 1] == 'JPEG') {
                $nb_car_fin = 4;
            }

            $vars['description'] = '<div width="500px !important;">                
                <a href="http:////photos.sevre-nantaise.com/i.php?' . substr(str_replace(',', '-', $path), 1, (strlen($path) - $nb_car_fin - 2)) . '-la.' . $infosp[count($infosp) - 1] . '" target="_blank">
                <img width="300px" src="http://photos.sevre-nantaise.com/i.php?' . substr(str_replace(',', '-', $path), 1, (strlen($path) - $nb_car_fin - 2)) . '-xs.' . $infosp[count($infosp) - 1] . '" data-src="http://photos.sevre-nantaise.com/i.php?' . substr(str_replace(',', '-', $path), 1, (strlen($path) - $nb_car_fin - 2)) . '-xs.' . $infosp[count($infosp) - 1] . '" /"/>
                </a>
                 <p>' . dateDisplay($start_date) . ' ' . utf8_decode($author) . '</p>               
                </div>';

            $vars['name'] = utf8_decode($vars['name']);
            //unset($vars['name']) ;
            unset($vars['path']);
            unset($vars['start_date']);
            unset($vars['author']);

            array_push($rs, $vars);
        }

        return $app->json(json_decode(toGeoJSON($rs)));
        //return $app['twig']->render('geojson/geojson.twig', ['geojson' => toGeoJSON($rs)]);
    } else {
        $erreur['message'] = 'erreur';
        return $app['twig']->render('geojson/error.twig', ['erreur' => $erreur]);
    }
});








$app->get('/geojson/cadastre/{id_objet}/{buffer}', function ($id_objet, $buffer) use ($app) {


    if ($app['session']->get('has_cadastre_access')) {

        $O = new SysmaObject($id_objet, $app);
        $TyO = new SysmaObjectType($O->sysma_object_type_id, $app);
        $O->loadGeoInfos($TyO->geometry_type);
        $geom = $O->loadGeom();

        $parcelles = findCadastreInfosSysmaObject($app, $geom, $buffer);

        return $app['twig']->render('geojson/geojson.twig', ['geojson' => toGeoJSON($parcelles)]);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('geojson-cadastre');



$app->get('/geojson/infos-cadastre/{x}/{y}/{buffer}', function ($x, $y, $buffer) use ($app) {


    if ($app['session']->get('has_cadastre_access')) {

        $pdo = $app['pdo'];
        $i = 'select ST_Transform(ST_setSRID(ST_Point(' . $x . ',' . $y . '),4326),2154) as  point';

        $qry = $pdo->prepare($i);
        $qry->execute();
        $res = $qry->fetchObject();


        $parcelles = findCadastreInfosSysmaObject($app, $res->point, $buffer, 0);

        return $app['twig']->render('geojson/geojson.twig', ['geojson' => toGeoJSON($parcelles)]);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/erreur.twig', ['erreur' => $erreur]);
    }
})->bind('geojson-infos-cadastre');






$app->get('/geojson/infos-rpg/{x}/{y}/{buffer}/{pacage}', function ($x, $y, $buffer, $pacage) use ($app) {


    if ($app['session']->get('has_rpg_access')) {

        $pdo = $app['pdo'];
        $i = 'select ST_Transform(ST_setSRID(ST_Point(' . $x . ',' . $y . '),4326),2154) as  point';

        $qry = $pdo->prepare($i);
        $qry->execute();
        $res = $qry->fetchObject();


        $parcelles = findRpgInfosSysmaObject($app, $res->point, $buffer, $pacage);

        return $app['twig']->render('geojson/geojson.twig', ['geojson' => toGeoJSON($parcelles)]);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('error/erreur.twig', ['erreur' => $erreur]);
    }
})->bind('geojson-infos-rpg');




/**********************************************************
 * TODO  @TBC CAUTION THIS FUNCTION SHOULD BE THE LAST?
 * ???Please do so since some route templates above already use this structure: /geojson/something/something 
 * 
 */

$app->get('/geojson/{other_layer_type}/{layer_id}', function (Request $request, $other_layer_type, $layer_id) use ($app) {
    return $app->json(loadOtherLayer($app, $app['pdo.export'], $request->query->all(), $other_layer_type, $layer_id));
})->bind('geojson-otherlayer');
