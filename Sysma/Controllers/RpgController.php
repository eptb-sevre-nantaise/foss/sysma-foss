<?php

use Symfony\Component\HttpFoundation\Request;

$app->match('/infos-rpg', function (Request $request) use ($app) {

    if ($app['session']->get('has_rpg_access') and RPG_MODE == 'on') {

     
        // TODO make a function

        $pdo = $app['pdo'];
        $coords = null ;
        $geom = null ;
        if ($request->request->get('mouse') !== null) {
            $coords = substr($request->request->get('mouse'), 7, -1); 
            $tabC = explode(',', $coords);        
            
            $i = 'select ST_Transform(ST_setSRID(ST_Point(' . $tabC[1] . ',' . $tabC[0] . '),4326),2154) as  point';
            $qry = $pdo->prepare($i);
            $qry->execute();
            $res = $qry->fetchObject();
            $geom = $res->point ;
        } 
        $buffer = 1;
        

        $pacage = $request->request->get('pacage') ;
        //echo $pacage ; 
        $parcelles = findRpgObjectInfos($app, $geom, $buffer, $pacage);
        //var_dump($parcelles) ;
      
        // $rpgVersionAnnee = findRpgVersionAnnee($app);

        return $app['twig']->render('rpg/rpgInfos.twig', ['parcelles' => $parcelles, 'x' => trim($tabC[1]), 'y' => trim($tabC[0])
            , 'buffer' => $buffer, 'pacage' => $pacage
    , 'rpgVersionAnnee' => RPG_YEAR /* TODO */]);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('/error/error.twig', ['erreur' => $erreur]);
    }
})->bind('infos-rpg');




$app->match('/infos-rpg-geo/{x}/{y}/{buffer}/{pacage}', function ($x, $y, $buffer, $pacage = null) use ($app) {

    if ($app['session']->get('has_rpg_access') and RPG_MODE == 'on') {
        return '<script>
        
        if (myrpgLayer !== undefined) { map.removeLayer(myrpgLayer); }

        var myrpgLayer = new L.GeoJSON.AJAX("/geojson/infos-rpg/' . $x . '/' . $y . '/' . $buffer . '/'.$pacage.'", 
            { 
                id: "098",
                name: "098",
                style:{ "stroke" : true, "color" :"#9933ff", "opacity" :1, "weight" :"2", "fill" : true, "fillOpacity" :0.2, "fillColor" : "#9933ff"  }, 
                onEachFeature: popUp,   
                
            }
            
        );
        groupRpg.addLayer(myrpgLayer) ;
        </script>';
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cette page';
        return $app['twig']->render('erreur.twig', ['erreur' => $erreur]);
    }
})->bind('infos-rpg-geo');
