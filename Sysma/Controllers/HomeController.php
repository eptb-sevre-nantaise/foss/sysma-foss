<?php

$app->get('/', function () use ($app) {
    return $app->redirect($app['url_generator']->generate('map'));
})->bind('home');
