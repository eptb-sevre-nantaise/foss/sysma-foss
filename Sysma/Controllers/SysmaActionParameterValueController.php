<?php

use Symfony\Component\HttpFoundation\Request;

$app->match('/value-parameter-sysma-action', function (Request $request) use ($app) {

    if ($request->request->get('data_index') != null) {
        $V = new SysmaActionParameterValue($request->request->get('data_index'), $app);
    } else {
        $V = new SysmaActionParameterValue(null, $app);
        $V->buildFromParameters($request->request->get('sysma_action_id'), $request->request->get('parameter_id'), $app);
    }

    $T = new SysmaAction($V->sysma_action_id, $app);
    $TT = new SysmaActionType($T->sysma_action_type_id, $app);

    if (
        hasAccess($app, $app['session']->get('user_id'), null, $TT->sysma_object_type_id, null, $T->sysma_action_type_id, $T->status, $T->organisation_id, 'lecteur')
        or (hasAccess($app, $app['session']->get('user_id'), null, $TT->sysma_object_type_id, null, $T->sysma_action_type_id, $T->status, $T->organisation_id, 'contributeur') and $T->modified_by_sysma_action == $app['session']->get('user_id'))
    ) {

        return $app['twig']->render('sysma-action-parameter-value/sysmaActionParameterValueDisplay.twig', ['dataType' => $request->request->get('data_type'), 'id' => $request->request->get('sysma_action_id'), 'parameter_id' => $request->request->get('parameter_id'), 'V' => $V, 'TT' => $TT, 'T' => $T, 'O' => new SysmaObject($request->request->get('sysma_object_id'), $app), 'sysma_object_id' => $request->request->get('sysma_object_id')]);
        // return $app['twig']->render('sysmaActionParameterValueDisplay.twig', ['dataType' => 'multipleTextChoiceType', 'id' => 100317, 'parameter_id' =>100446, 'V' => $V, 'TT'=>$TT, 'T'=>$T]);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cet objet';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('sysma-action-parameter-value');



$app->match('/value-parameter-sysma-action/update', function (Request $request) use ($app) {

    $V = new SysmaActionParameterValue($request->request->get('data_index'), $app);

    $T = new SysmaAction($V->sysma_action_id, $app);
    $TT = new SysmaActionType($T->sysma_action_type_id, $app);
    if (
        hasAccess($app, $app['session']->get('user_id'), null, $TT->sysma_object_type_id, null, $T->sysma_action_type_id, $T->status, $T->organisation_id, 'gestionnaire')
        or (hasAccess($app, $app['session']->get('user_id'), null, $TT->sysma_object_type_id, null, $T->sysma_action_type_id, $T->status, $T->organisation_id, 'contributeur') and $T->modified_by_sysma_action == $app['session']->get('user_id'))
    ) {

        $V->loadChoices($request->request->get('data_type'),$app);

        return $app['twig']->render('sysma-action-parameter-value/sysmaActionParameterValueModificationForm.twig', ['dataType' => $request->request->get('data_type'), 'sysma_object_id' => $request->request->get('sysma_object_id'), 'id' => $request->request->get('sysma_action_id'), 'parameter_id' => $request->request->get('parameter_id'), 'V' => $V, 'T' => $T]);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cet objet';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('sysma-action-parameter-value-update');




$app->match('/value-parameter-sysma-action/update/commit', function (Request $request) use ($app) {


    $request->request->set('modified_by', $app['session']->get('user_id'));
    $request->request->set('user_id', $app['session']->get('user_id'));
    $request->request->set('modified_at', date('Y-m-d H:i:s'));
    $r = $request->request;
    $V = new SysmaActionParameterValue($r->get('data_index'), $app);

    $T = new SysmaAction($V->sysma_action_id, $app);
    $TT = new SysmaActionType($T->sysma_action_type_id, $app);
    if (
        hasAccess($app, $app['session']->get('user_id'), null, $TT->sysma_object_type_id, null, $T->sysma_action_type_id, $T->status, $T->organisation_id, 'gestionnaire')
        or (hasAccess($app, $app['session']->get('user_id'), null, $TT->sysma_object_type_id, null, $T->sysma_action_type_id, $T->status, $T->organisation_id, 'contributeur') and $T->modified_by_sysma_action == $app['session']->get('user_id'))
    ) {

        return $V->modify($r);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cet objet';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('sysma-action-parameter-value-update-commit');



$app->match('/value-parameter-multiple-sysma-action/update/commit', function (Request $request) use ($app) {

    $request->request->set('modified_by', $app['session']->get('user_id'));
    $request->request->set('user_id', $app['session']->get('user_id'));
    $request->request->set('created_at', date('Y-m-d H:i:s'));
    $request->request->set('modified_at', date('Y-m-d H:i:s'));

    $r = $request->request;
    $V = new SysmaActionParameterValue($r->get('data_index'), $app);
    $T = new SysmaAction($V->sysma_action_id, $app);
    $TT = new SysmaActionType($T->sysma_action_type_id, $app);
    if (
        hasAccess($app, $app['session']->get('user_id'), null, $TT->sysma_object_type_id, null, $T->sysma_action_type_id, $T->status, $T->organisation_id, 'gestionnaire')
        or (hasAccess($app, $app['session']->get('user_id'), null, $TT->sysma_object_type_id, null, $T->sysma_action_type_id, $T->status, $T->organisation_id, 'contributeur') and $T->modified_by_sysma_action == $app['session']->get('user_id'))
    ) {

        return $V->multipleUpdate($r);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cet objet';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('sysma-action-parameter-multiple-value-update-commit');



$app->match('/value-parameter-sysma-action/create', function (Request $request) use ($app) {



    $V = new SysmaActionParameterValue(null, $app);
    $V->buildFromParameters($request->request->get('sysma_action_id'), $request->request->get('parameter_id'), $app);

   

    $T = new SysmaAction($V->sysma_action_id, $app);
    $TT = new SysmaActionType($T->sysma_action_type_id, $app);

   

    if (
        hasAccess($app, $app['session']->get('user_id'), null, $TT->sysma_object_type_id, null, $T->sysma_action_type_id, $T->status, $T->organisation_id, 'gestionnaire')
        or (hasAccess($app, $app['session']->get('user_id'), null, $TT->sysma_object_type_id, null, $T->sysma_action_type_id, $T->status, $T->organisation_id, 'contributeur') and $T->modified_by_sysma_action == $app['session']->get('user_id'))
    ) {

        $V->loadChoices($request->request->get('data_type'),$app);       
        $V->loadChoices('multipleTextChoiceType');

          
  

        return $app['twig']->render('sysma-action-parameter-value/sysmaActionParameterValueFillForm.twig', ['dataType' => $request->request->get('data_type'), 'sysma_object_id' => $request->request->get('sysma_object_id'), 'id' => $request->request->get('sysma_action_id'), 'parameter_id' => $request->request->get('parameter_id'), 'V' => $V, 'indexDonnee' => dataIndex($app['pdo'])]);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cet objet';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('sysma-action-parameter-value-create');



$app->match('/value-parameter-sysma-action/create/commit', function (Request $request) use ($app) {

    $request->request->set('modified_by', $app['session']->get('user_id'));
    $request->request->set('created_at', date('Y-m-d H:i:s'));
    $request->request->set('user_id', $app['session']->get('user_id'));
    $r = $request->request;
    $V = new SysmaActionParameterValue(null);
    $V->buildFromParameters($request->request->get('sysma_action_id'), $request->request->get('parameter_id'), $app);
    $T = new SysmaAction($V->sysma_action_id, $app);
    $TT = new SysmaActionType($T->sysma_action_type_id, $app);
    if (
        hasAccess($app, $app['session']->get('user_id'), null, $TT->sysma_object_type_id, null, $T->sysma_action_type_id, $T->status, $T->organisation_id, 'gestionnaire')
        or (hasAccess($app, $app['session']->get('user_id'), null, $TT->sysma_object_type_id, null, $T->sysma_action_type_id, $T->status, $T->organisation_id, 'contributeur') and $T->modified_by_sysma_action == $app['session']->get('user_id'))
    ) {

        return $V->create($r);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cet objet';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('sysma-action-parameter-value-create-commit');


$app->match('/value-parameter-multiple-sysma-action/create/commit', function (Request $request) use ($app) {

    $request->request->set('modified_by', $app['session']->get('user_id'));
    $request->request->set('created_at', date('Y-m-d H:i:s'));
    $request->request->set('user_id', $app['session']->get('user_id'));
    $r = $request->request;
    $V = new SysmaActionParameterValue(null, $app);
    $V->buildFromParameters($request->request->get('sysma_action_id'), $request->request->get('parameter_id'), $app);

    $T = new SysmaAction($V->sysma_action_id, $app);
    $TT = new SysmaActionType($T->sysma_action_type_id, $app);
    if (
        hasAccess($app, $app['session']->get('user_id'), null, $TT->sysma_object_type_id, null, $T->sysma_action_type_id, $T->status, $T->organisation_id, 'gestionnaire')
        or (hasAccess($app, $app['session']->get('user_id'), null, $TT->sysma_object_type_id, null, $T->sysma_action_type_id, $T->status, $T->organisation_id, 'contributeur') and $T->modified_by_sysma_action == $app['session']->get('user_id'))
    ) {

        $V->loadChoices($V->parameter->dataType,$app);
        $res = null;

        foreach ($V->choicesArray as $choice) {

            $inputName = 'choice' . $choice['choice_id'];
            if ($r->get($inputName) == 1) {

                $r2['user_id'] = $r->get('user_id');
                $r2['parameter_id'] = $r->get('parameter_id');
                $r2['sysma_action_id'] = $r->get('sysma_action_id');
                $r2['created_at'] = $r->get('created_at');
                $r2['data_index'] = $r->get('data_index');
                $r2['value'] = $choice['choice'];
                $r2['user_id'] = $r->get('user_id');
                $r2['modified_by'] = $r->get('modified_by');

                $res = $V->create($r2);
            }
        }

        return $res; // derniere insertion
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cet objet';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('sysma-action-parameter-multiple-value-create-commit');





$app->match('/value-parameter-action/delete', function (Request $request) use ($app) {


    $r = $request->request;

    $V = new SysmaActionParameterValue($r->get('data_index'), $app);
    $T = new SysmaAction($V->sysma_action_id, $app);
    $TT = new SysmaActionType($T->sysma_action_type_id, $app);

    if (
        hasAccess($app, $app['session']->get('user_id'), null, $TT->sysma_object_type_id, null, $T->sysma_action_type_id, $T->status, $T->organisation_id, 'gestionnaire')
        or (hasAccess($app, $app['session']->get('user_id'), null, $TT->sysma_object_type_id, null, $T->sysma_action_type_id, $T->status, $T->organisation_id, 'contributeur') and $T->modified_by_sysma_action == $app['session']->get('user_id'))
    ) {

        $V->delete();

        $V2 = new SysmaActionParameterValue(null);
        $V2->buildFromParameters($V->sysma_action_id, $V->parameter_id, $app);
        return $app['twig']->render('sysma-action-parameter-value/sysmaActionParameterValueDisplay.twig', [
            'dataType' => $V2->parameter->data_type, 'id' => $V->sysma_action_id, 'parameter_id' => $V->parameter_id, 'V' => $V2, 'TT' => $TT, 'T' => $T, 'O' => new SysmaObject($r->get('sysma_object_id'), $app), 'sysma_object_id' => $r->get('sysma_object_id')
        ]);
    } else {
        $erreur['message'] = 'Vous ne pouvez pas accéder à cet objet';
        return $app['twig']->render('error/error.twig', ['erreur' => $erreur]);
    }
})->bind('sysma-action-parameter-value-delete');
