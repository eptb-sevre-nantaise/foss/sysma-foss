<?php

// Utils
require_once(dirname(__DIR__) . '/utils/debug.php');
require_once(dirname(__DIR__) . '/conf/conf.php');
require_once(dirname(__DIR__) . '/conf/conf_processes.php');
require_once(dirname(__DIR__) . '/conf/conf_addons.php');
require_once(dirname(__DIR__) . '/utils/utils.php');
require_once(dirname(__DIR__) . '/utils/TkGeo.php');

// Autoload des vendors
require_once __DIR__ . '/../vendor/autoload.php';


// CLASSES SYSMA
// Autoload
spl_autoload_register(function ($class) {
    $file = __DIR__ . '/../Sysma/Classes/' . str_replace('\\', '/', $class) . '.php';
    // var_dump($file);
    if (file_exists($file)) {
        require $file;
    }
});
