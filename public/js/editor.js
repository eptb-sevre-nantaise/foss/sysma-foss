function loadTinyMCEEditor() {

    tinyMCE.init({
       
        selector: "textarea",
        menubar: false,
        toolbar: "styleselect | bold,italic,underline,|,bullist,numlist,outdent,indent,| table, |,undo,redo,|,link,code,fullscreen",
        plugins: "code,table,link",        
        language_url : '/js/langs/fr_FR.js',
        language: 'fr_FR'
    });
}
