function sendForm(form, page, emplacement, event, script) {
    script = typeof script !== 'undefined' ? script : false;
    event = typeof event !== 'undefined' ? event : null;

    if (event !== null && event !== 'undefined') {
        event.preventDefault();
    }// using this page stop being refreshing 

    //console.log($('#' + form).serialize());

    var loader = '<div style="padding:10px;text-align:center;"><img src="/img/loader.gif"></div>';
    $("#" + emplacement).html(loader);

    $.ajax({
        type: "POST",
        url: page,
        data: $('#' + form).serialize(),
        //async: false,
        success: function (data) {
            $("#" + emplacement).html(data);
            if (script) {
                console.log("sendForm | attempt to force to eval script. CAUTION! Script are already eval by ajax calls by default. So script will be eval/run twice. Do we really want this here?")
                $("#" + emplacement).find("script").each(function (i) {
                    eval($(this).text());
                });
            }
        },
        error: function (data, event) {
            var json = $.parseJSON(data);
            sendData('message=' + json.message + '&data=' + json.data, '/error', 'errorDiv', null);
        }
    });
}



/*
 * 
 * récupère un id de formulaire, serialize et envoi à une page de traitement puis 
 * redirige sur une page de succes avec les params donnés et à afficher dans l'emplacement donné
 * 
 * @param {string} formulaire
 * @param {string} pageTraitement
 * @param {string} dataSucess
 * @param {string} pageSuccess
 * @param {string} emplacementSuccess
 * @param {avant} event
 * @returns {undefined}
 */
function sendFormAndRedirect(form, pageProcess, dataSuccess, pageSuccess, divSuccess, event) {
    event.preventDefault();// using this page stop being refreshing 

    s = $('#' + form).serialize(); // recupere toutes les données du formulaire
    ////console.log(s);
    $.ajax({
        type: "POST",
        url: pageProcess,
        data: s,
        async: false,
        success: function (data, event) {
            //console.log(data);
            var json = $.parseJSON(data);

            if (json.status === 'error') {
                sendData('message=' + json.message + '&data=' + json.data, '/error', 'errorDiv', null);
            } else {
                sendData(dataSuccess, pageSuccess, divSuccess, null);
            }
        },
        error: function (data, event) {
            var json = $.parseJSON(data);
            sendData('message=' + json.message + '&data=' + json.data, '/error', 'errorDiv', null);
        }
    });
}



/*
 * 
 * idem mais utilisé pour la création d'un nouvel objet, avec suppression de la layer dessin et retrçage de la layer avec le nouvel objet
 */

function createObjectAndRedirect(form, pageProcess, dataSuccess, pageSuccess, divSuccess, event) {
    // console.log("createObjectAndRedirect")

    event.preventDefault();// using this page stop being refreshing 

    s = $('#' + form).serialize(); // recupere toutes les données du formulaire
    var sysma_object_type_id = $('#sysma_object_type_id').val();
    var idsysma_object = $('#sysma_object_id').val();
    //console.log(idsysma_object);
    $.ajax({
        type: "POST",
        url: pageProcess,
        data: s,
        async: false,
        success: function (data, event) {
            //console.log(data);
            var json = $.parseJSON(data);
            //console.log(json.status);

            if (json.status === 'error') {
                sendData('message=' + json.message + '&data=' + json.data, '/error', divSuccess, null);
            } else {
                refreshLayer('objecttype', sysma_object_type_id, null);
                drawnItems.clearLayers();
                cancelGeoCreateFeature(sysma_object_type_id);
                $('.o' + sysma_object_type_id + 'GeoCancelButton').css('display', 'none');
                $('.o' + sysma_object_type_id + 'GeoNewButton').css('display', 'inline-block');
                sendData(dataSuccess, pageSuccess, divSuccess, null);
            }

        }
    });
}





function sendFormAndRedirectAndMove(form, pageProcess, dataSuccess, pageSuccess, divSuccess, event, target) {
    event.preventDefault();// using this page stop being refreshing 

    // s = $('#' + form).serialize(); // recupere toutes les données du formulaire
    var myForm = document.getElementById(form);
    var s = new FormData(myForm);

    ////console.log(s) ;

    $.ajax({
        type: "POST",
        url: pageProcess,
        data: s,
        async: false,
        processData: false,
        contentType: false,
        success: function (data, event) {
            //console.log(data);
            var json = $.parseJSON(data);
            if (json.status === 'error') {
                sendData('message=' + json.message + '&data=' + json.data, '/error', 'errorDiv', null);
            } else {
                sendDataAndMoveToElement(dataSuccess, pageSuccess, divSuccess, null, null, target);
            }
        },
        error: function (data, event) {
            var json = $.parseJSON(data);
            sendData('message=' + json.message + '&data=' + json.data, '/error', 'errorDiv', null);
        }
    });
}


/*****************************************************
 * 
 * 
 * sendData : 
 * param : param1=123&param2=456
 * page : url appelée
 * emplacmement : id de la  div cible
 * event : evenement du clic
 * script : doit on jouer le JS dans la page cible (true , false)
 */
function sendData_orig(param, page, emplacement, event, script) {
    script = typeof script !== 'undefined' ? script : false;
    event = typeof event !== 'undefined' ? event : null;
    // if (event !== null && event !== 'undefined') {
    //     //event.preventDefault();
    // }// using this page stop being refreshing 
    //console.log(param);
    var loader = '<div style="padding:10px;text-align:center;"><img src="/img/loader.gif"></div>';
    $("#" + emplacement).html(loader);
    $.ajax({
        type: "POST",
        url: page,
        data: param,
        // async: false,
        success: function (data) {
            // console.log("sendData success: AVANT" + data);
            $("#" + emplacement).html(data);
            if (script) {
                console.log("sendData_orig | attempt to force to eval script. CAUTION! Script are already eval by ajax calls by default. So script will be eval/run twice. Do we really want this here?")
                $("#" + emplacement).find("script").each(function (i) {
                    eval($(this).text());
                });
            }
            // console.log("sendData success: APRES" + data);
        },
        error: function (data) {
            // console.log("sendData error: AVANT" + data);
            $("#" + emplacement).html(data.responseText);
            // console.log("sendData error: APRES" + data);
        }
    });
}

////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////


// The following function creates the DB which will contain some values concerning the state of the application
// (here especially for the mode : either online or offline), it does not set the value in IndexedDB because it is
// already done by the onlie/offline button Vue component which takes care to have a coherent state between the button
// and the value in indexedDB. By default, if the DB does not exists (which means the user is begining a new session and
// hasn't downloaded any FeatureCollection neither) the value returned is 'off' so that the map is able to draw all
// objects : this provides a totally transparent experience for the user. If it already exists it does not modify it.
// ^^^^^^^ not valid anymore <- TODO REDO

async function sendData_offline(param, page, emplacement, event) {
    // console.log("sendData offline : " + param);

    if (!(param === null)) {
        if (param.substring(0, 4) === 'geom') {
            $('#objet').hide(0);
            $('#offline-right-vue-content').show(150);
            const sysmaAppStateDB = await idb.openDB('sysmaAppStateDB');
            await sysmaAppStateDB.put('state', { parameterName: 'objectGeomBeingCreated', value: param });
            console.log(param.substring(5));
        } else if (param.substring(0, 20) === 'sysma_object_type_id') {
            const sysmaAppStateDB = await idb.openDB('sysmaAppStateDB');
            await sysmaAppStateDB.put('state', { parameterName: 'objectTypeBeingCreated', value: parseInt(param.substring(21)) });
        } else if (param.substring(0, 3) === 'id=') {
            $('#objet').hide(0);
            $('#offline-right-vue-content').show(150);
        } else if (param.substring(0, 5) === 'login') {
            // console.log('Sending login infos');
            sendData_orig(param, page, emplacement, event);
        }
    } else if (emplacement === 'infos') {
        // Ici il faut aller récupérer le html qui s'affiche à droite présentant toutes les couches
        // on peut faire en sorte de stocker ce HTML à chaque fois qu'il se charge avec un sendData
        // en onLine de sorte que l'utilisateur puisse toujours y avoir accès
        const sysmaAppStateDB = await idb.openDB('sysmaAppStateDB');
        let layerManagerDiv = (await sysmaAppStateDB.get('state', 'layerManagerDiv')).value;
        $("#" + emplacement).html(layerManagerDiv);
    } else if ((param === null) && (page.substring(page.length - 5) === 'login') && (emplacement === 'modal-content')) {
        // console.log('Display login modal');
        sendData_orig(param, page, emplacement, event, null);
    }
}

// Not working ATM
// function sendData_WIP_AS(param, page, emplacement, event, script) {
//     idb.openDB('sysmaAppStateDB', 1, {
//         upgrade(db) {
//             const store = db.createObjectStore('state', {
//                 keyPath: 'parameterName',
//                 autoIncrement: true,
//             });
//             store.createIndex('parameterName', 'parameterName');
//         },
//     }).then(sysmaAppStateDB=>{
//         sysmaAppStateDB.get('state', 'isOffline')
//     }).then(isOfflineStatus=>{
//         if ((typeof isOfflineStatus != "undefined") && (isOfflineStatus.value) ) {
//             // console.log("isOfflineModeActive2 -> true")
//             sendData_offline(param, emplacement);
//         } else {
//             // console.log("isOfflineModeActive2 -> false")
//             sendData_orig(param, page, emplacement, event, script);
//         }
//     });
// }


// async function sendData(param, page, emplacement, event, script){
function sendData(param, page, emplacement, event, script) {
    const isOfflineModeActive = new Promise(async (resolve, reject) => {
        try {
            const sysmaAppStateDB = await idb.openDB('sysmaAppStateDB', 1, {
                upgrade(db) {
                    const store = db.createObjectStore('state', {
                        keyPath: 'parameterName',
                        autoIncrement: true,
                    });
                    store.createIndex('parameterName', 'parameterName');
                },
            });
            const isOfflineEnabled = await sysmaAppStateDB.get('state', 'isOffline');
            let isActive = false;
            if ((typeof isOfflineEnabled != "undefined") && (isOfflineEnabled.value)) {
                // console.log("isOfflineModeActive -> true")
                isActive = true;
                // } else {
                //     console.log("isOfflineModeActive -> false")
            }
            return resolve(isActive);
        } catch (error) {
            // console.log('error', error)
            // throw '';
            // die;
            return reject(error);
        }
    });

    isOfflineModeActive
        .then(isActive => {
            // console.log("sendData isOfflineModeActive: " + isActive);
            if (!isActive) {
                sendData_orig(param, page, emplacement, event, script);
            } else {
                sendData_offline(param, page, emplacement, event);
            }
        })
        .catch(err => {
            console.log(err)
            // throw '';
        });
}


/* version sans le loader, plus light */
function sendData2_orig(param, page, emplacement, event, script) {
    script = typeof script !== 'undefined' ? script : false;
    event = typeof event !== 'undefined' ? event : null;
    // if (event !== null && event !== 'undefined') {
    //     // event.preventDefault();
    // }// using this page stop being refreshing 

    $.ajax({
        type: "POST",
        url: page,
        data: param,
        //async: false,
        success: function (data) {
            $("#" + emplacement).html(data);
            if (script) {
                console.log("sendData2_orig | attempt to force to eval script. CAUTION! Script are already eval by ajax calls by default. So script will be eval/run twice. Do we really want this here?")
                $("#" + emplacement).find("script").each(function (i) {
                    eval($(this).text());
                });
            }
        }
    });
}


async function sendData2(param, page, emplacement, event, script) {
    const isOfflineModeActive = new Promise(async (resolve, reject) => {
        try {
            const sysmaAppStateDB = await idb.openDB('sysmaAppStateDB', 1, {
                upgrade(db) {
                    const store = db.createObjectStore('state', {
                        keyPath: 'parameterName',
                        autoIncrement: true,
                    });
                    store.createIndex('parameterName', 'parameterName');
                },
            });
            const isOfflineEnabled = await sysmaAppStateDB.get('state', 'isOffline');
            let isActive = false;
            if ((typeof isOfflineEnabled != "undefined") && (isOfflineEnabled.value)) {
                // console.log("sendData2: isOfflineModeActive -> true")
                isActive = true;
            } else {
                // console.log("sendData2: isOfflineModeActive -> false")
            }
            return resolve(isActive);
        } catch (error) {
            return reject(error);
        }
    });

    isOfflineModeActive
        .then(isActive => {
            // console.log("sendData2: isOfflineModeActive: " + isActive);
            if (!isActive) {
                sendData2_orig(param, page, emplacement, event, script);
                // } else {
                //     ; // CAUTIOn hacky way to quit execution here 
            }
        })
        .catch(err => {
            console.log(err)
        });
}



function sendDataAndRedirect(param, page, emplacement, event, script, dataSuccess, pageSuccess, divSuccess) {

    script = typeof script !== 'undefined' ? script : false;
    event = typeof event !== 'undefined' ? event : null;

    //event.preventDefault();// using this page stop being refreshing 

    var loader = '<div style="padding:10px;text-align:center;"><img src="/img/loader.gif"></div>';
    $("#" + emplacement).html(loader);
    //console.log(param);
    $.ajax({
        type: "POST",
        url: page,
        data: param,
        //async: false,
        success: function (data, event) {
            //console.log(dataSuccess);
            sendData(dataSuccess, pageSuccess, divSuccess, event, script);
        }
        //,
        // error: function (data) {
        //$("#" + emplacement).html(data);
        //  }
    });
}




/*
 * 
 * scroll la colonne droite sur l'élément considéré
 * @param {type} param
 * @param {type} page
 * @param {type} emplacement
 * @param {type} event
 * @param {type} script
 * @param {type} target
 * @returns {undefined}
 */

function sendDataAndMoveToElement(param, page, emplacement, event, script, target) {
    script = typeof script !== 'undefined' ? script : false;
    event = typeof event !== 'undefined' ? event : null;

    //event.preventDefault();// using this page stop being refreshing 

    var loader = '<div style="padding:10px;text-align:center;"><img src="/img/loader.gif"></div>';
    $("#" + emplacement).html(loader);

    $.ajax({
        type: "POST",
        url: page,
        data: param,
        //async: false,
        success: function (data) {
            $("#" + emplacement).html(data);
            if (script) {
                // console.log("sendDataAndMoveToElement | attempt to force to eval script. CAUTION! Script are already eval by ajax calls by default. So script will be eval/run twice. Do we really want this here?")
                $("#" + emplacement).find("script").each(function (i) {
                    eval($(this).text());
                });
            }
            goToByScroll(target);
        }
        //,
        // error: function (data) {
        //$("#" + emplacement).html(data);
        //  }
    });
}


function goToByScroll(id) {
    $('#rightColumn').animate({ scrollTop: $("#" + id).position().top }, 'slow');
    blink('#' + id, 3, 250);
}


function blink(elem, times, speed) {
    if (times > 0 || times < 0) {
        if ($(elem).hasClass("blink"))
            $(elem).removeClass("blink");
        else
            $(elem).addClass("blink");
    }

    clearTimeout(function () {
        blink(elem, times, speed);
    });

    if (times > 0 || times < 0) {
        setTimeout(function () {
            blink(elem, times, speed);
        }, speed);
        times -= .5;
    }
}


/*
 // ancienne fonction
 
 function sendData2(param, page, emplacement, event) {
 //alert('send data / Param : '+param+' / page : '+page+' / emplecmt : '+emplacement) ;
 
 
 if (event !== null) {
 event.preventDefault();
 }
 // using this page stop being refreshing 
 
 // definition de l'endroit d'affichage:
 var content = document.getElementById(emplacement);
 
 content.innerHTML = '<div style="padding:10px;text-align:center;"><img src="/img/loader.gif"></div>';
 
 if (document.all) {
 // Internet Explorer
 var XhrObj = new ActiveXObject("Microsoft.XMLHTTP");
 }// fin if
 else {
 // Mozilla
 var XhrObj = new XMLHttpRequest();
 }// fin else
 
 XhrObj.open("POST", page);
 
 if (XhrObj != undefined) {
 // Ok pour la page cible
 XhrObj.onreadystatechange = function () {
 if (XhrObj.readyState == 4 && XhrObj.status == 200) {
 content.innerHTML = XhrObj.responseText;
 
 
 } else {  //content.innerHTML=" Error:\n"+ XhrObj.status + "\n"
 //  +XhrObj.statusText;
 }
 }
 
 XhrObj.setRequestHeader('Content-Type',
 'application/x-www-form-urlencoded');
 if (XhrObj.overrideMimeType) {
 XhrObj.overrideMimeType('text/html; charset=utf-8');
 }
 XhrObj.send(param);
 
 
 }
 
 ////console.log();
 
 }// fin fonction SendData
 */


function afficheBloc(bloc, lien) {
    if (document.getElementById(lien).innerHTML == "-") {
        $("#" + bloc).hide("fast");
        document.getElementById(lien).innerHTML = "+";
    } else {
        if (document.getElementById(lien).innerHTML == "+") {
            $("#" + bloc).show("fast");
            document.getElementById(lien).innerHTML = "-";
        }
    }
}


function openClose(bloc, width, bloc2, width2) {
    $("#" + bloc2).animate({
        width: width2
    }, 200, function () {
        map.invalidateSize(true);
    }
    );

    $("#" + bloc).animate({
        width: width
    }, 200);
}


function disableLoadOnPan() {
    console.log('LOP Off');
    sendData2(null, '/disable-loadonpan', 'layerstate', null, false);
}

function enableLoadOnPan() {
    console.log('LOP On');
    sendData2(null, '/enable-loadonpan', 'layerstate', null, false);
}

/*
 $(function() {
 $( "#infos" ).resizable();
 });
 */
/*
 $(function() {
 $( "#infos" ).draggable();
 });*/



function fermeForm(formulaire) {
    // permet de fermer le formulaire et de l'effacer
    //$('#renseigne'+formulaire).hide("slow");
    // efface le formulaire
    $('#renseigne' + formulaire).text('');
}

function changeStreet() {
    if ($('#street').val() === 'inactif') {
        $('#street').val('actif');
        if ($('#infoscad').val() === 'actif') {
            changeInfosCad();
        }
        $('#changeStreet').css('width', '50%');
        $('#changeStreet').html('<span class="glyphicon glyphicon-picture"  title="Afficher Street view"></span> Cliquez sur la carte pour afficher les images Street View');
        $('#changeStreet').css('color', '#fff');
        $('#map').css('cursor', 'crosshair');

    } else if ($('#street').val() === 'actif') {
        $('#street').val('inactif');
        $('#changeStreet').css('width', '15px');
        $('#changeStreet').html('<span class="glyphicon glyphicon-picture"  title="Afficher Street view"></span>');
        $('#changeStreet').css('color', '#c3c3c3');
        $('#map').css('cursor', '-webkit-grab');
        $('#map').css('cursor', 'grab');
    }
}


/*
 function changeEPSG() {
 
 $('#infoscad').val('inactif');
 ////console.log($('#epsg').val());
 if ($('#epsg').val() === '4326') {
 $('#epsg').val('2154');
 } else if ($('#epsg').val() === '2154') {
 //$('#coords').html('<span class="glyphicon glyphicon-map-marker"  title="Afficher les coordonnées de la souris"></span>'); 
 $('#epsg').val('27572');
 } else if ($('#epsg').val() === '27572') {
 $('#coords').html('<span class="glyphicon glyphicon-map-marker text-inactive"  title="Afficher les coordonnées de la souris"></span>');        
 $('#epsg').val('');
 
 } else if ($('#epsg').val() === '') {
 $('#coords').html('<span class="glyphicon glyphicon-map-marker"  title="Afficher les coordonnées de la souris"></span> Cliquez sur la carte pour afficher les coordonnées');
 $('#epsg').val('4326');
 }
 ////console.log($('#epsg').val());
 }
 
 */
function changeInfosCad() {
    //console.log($('#epsg').val());
    if ($('#infoscad').val() === 'inactif') {
        $('#infoscad').val('actif');
        $('#cad').html('<span class="glyphicon glyphicon-list-alt"  title="Afficher les informations du cadastre"></span> Cliquez sur la carte pour afficher les informations du cadastre');
        $('#cad').css('width', 'auto');
        $('#cad').css('color', '#fff');
    } else if ($('#infoscad').val() === 'actif') {
        $('#infoscad').val('inactif');
        $('#cad').html('<span class="glyphicon glyphicon-list-alt text-inactive"  title="Afficher les informations du cadastre"></span>');
        $('#cad').css('width', '15px');
        $('#cad').css('color', '#c3c3c3');
    }
    ////console.log($('#epsg').val());
}


function dateFormatIsOK(date) {

    if (date !== '0') {
        if (checkDateType(date) !== false) {

            //console.log('est une date');

            if (checkDateType(date) === 'shortDate') {

                //console.log('date courte');

                if (isNaN(date.substring(0, 4))
                    ||
                    date.substring(4, 5) != '-'
                    ||
                    isNaN(date.substring(5, 7))
                    ||
                    parseInt(date.substring(5, 7)) < 1
                    ||
                    parseInt(date.substring(5, 7)) > 12) {
                    return false;
                } else {
                    return true;
                }
            } else if (checkDateType(date) === 'completeDate') {

                //console.log('date complete');

                if (isNaN(date.substring(0, 4))
                    ||
                    date.substring(4, 5) != '-'
                    ||
                    isNaN(date.substring(5, 7))
                    ||
                    parseInt(date.substring(5, 7)) < 1
                    ||
                    parseInt(date.substring(5, 7)) > 12
                    ||
                    date.substring(7, 8) != '-'
                    ||
                    isNaN(date.substring(8, 10))
                    ||
                    parseInt(date.substring(8, 10)) < 1
                    ||
                    parseInt(date.substring(8, 10)) > 31) {
                    return false;
                } else {
                    return true;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    } else {
        return true;
    }
}



function checkEndDateIsNotZero(input, comment) {
    if ($('#' + input).val() !== '0' && $('#' + input).val() !== '') {
        displayEndDateAlert(comment);
    } else {
        $('#' + comment).html(null);
    }
}



// vérifie le format d'une date
// doit être correct ainsi que celle du second input

function checkDateEndOK(input, comment, bouton, input2) {


    //console.log(dateFormatIsOK($('#' + input).val()));
    //console.log(dateFormatIsOK($('#' + input2).val()));


    if (dateFormatIsOK($('#' + input).val()) && dateFormatIsOK($('#' + input2).val())) {
        displayRightDate(input, comment, bouton, true);

    } else if (dateFormatIsOK($('#' + input).val()) && dateFormatIsOK($('#' + input2).val()) === false) {
        displayRightDate(input, comment, bouton, false);

    } else if (dateFormatIsOK($('#' + input).val()) === false) {
        displayWrongDate(input, comment, bouton, true);
    }


}

// vérifie le format d'une date
// doit être correct ainsi que celle du second input

function checkDate(input, comment, bouton, input2) {


    //console.log(dateFormatIsOK($('#' + input).val()));
    //console.log(dateFormatIsOK($('#' + input2).val()));


    if (dateFormatIsOK($('#' + input).val()) && dateFormatIsOK($('#' + input2).val())) {
        displayRightDate(input, comment, bouton, true);
        if (input === 'end_date') {
            checkEndDateIsNotZero(input, comment);
        }
    } else if (dateFormatIsOK($('#' + input).val()) && dateFormatIsOK($('#' + input2).val()) === false) {
        displayRightDate(input, comment, bouton, false);
        if (input === 'end_date') {
            checkEndDateIsNotZero(input, comment);
        }
    } else if (dateFormatIsOK($('#' + input).val()) === false) {
        displayWrongDate(input, comment, bouton, true);
    }


}

/* look for https:// http:// *:/ \\ */

function checkHyperLink(value, inputId) {

    let linkIsOK = false;
    if (value.substring(0, 8) === 'https://') linkIsOK = true;
    if (value.substring(0, 7) === 'http://') linkIsOK = true;
    if (value.substring(1, 3) === ':\\') linkIsOK = true;
    if (value.substring(0, 2) === '\\\\') linkIsOK = true;
    if (value.substring(0, 1) === '/') linkIsOK = true;

    if (!linkIsOK) {

        $('#input' + inputId).css('color', 'red');
        $('#comment' + inputId).css('color', 'red');
        $('#comment' + inputId).html('Lien non valide (lien web (https://, http://) ou liens locaux (*:\\ ou \\\\ ou /) uniquement');
        $('#button' + inputId).prop('disabled', true);

    } else {
        $('#input' + inputId).css('color', 'green');
        $('#comment' + inputId).css('color', 'green');
        $('#comment' + inputId).html('Lien valide');
        $('#button' + inputId).prop('disabled', false);
    }


}


function isFloat(value) {

    console.log(value) ;

    if (
        typeof value === 'number' &&
        !Number.isNaN(value) &&
        !Number.isInteger(value)
    ) {
        return true;
    }

    return false;
}




function isInt(value) {

    
    return Number.isInteger(value);

}

function checkInteger(e, inputId) {

    
    let n = (document.getElementById('input'+inputId).value) ;
    //console.log(n) ;

    if (n=='') {
        $('#input' + inputId).css('color', 'red');
        $('#comment' + inputId).css('color', 'red');
        $('#comment' + inputId).html('Nombre entier uniquement');
        $('#button' + inputId).prop('disabled', true);

    } else if (isInt(Number(n))) {
        $('#input' + inputId).css('color', 'green');
        $('#comment' + inputId).css('color', 'green');
        $('#comment' + inputId).html('');
        $('#button' + inputId).prop('disabled', false);
    } else {
        $('#input' + inputId).css('color', 'red');
        $('#comment' + inputId).css('color', 'red');
        $('#comment' + inputId).html('Nombre entier uniquement');
        $('#button' + inputId).prop('disabled', true);
    }

}



function checkFloat(e, inputId) {

    let n = (document.getElementById('input'+inputId).value) ;

    if (n=='') {
        $('#input' + inputId).css('color', 'red');
        $('#comment' + inputId).css('color', 'red');
        $('#comment' + inputId).html('Numérique uniquement');
        $('#button' + inputId).prop('disabled', true);

    } else if (isFloat(Number(n)) || isInt(Number(n))) {
        $('#input' + inputId).css('color', 'green');
        $('#comment' + inputId).css('color', 'green');
        $('#comment' + inputId).html('');
        $('#button' + inputId).prop('disabled', false);
    }
    else {
        $('#input' + inputId).css('color', 'red');
        $('#comment' + inputId).css('color', 'red');
        $('#comment' + inputId).html('Numérique uniquement');
        $('#button' + inputId).prop('disabled', true);

    }

}


function displayEndDateAlert(comment) {


    $('#' + comment).css('color', 'red');
    $('#' + comment).html('<big>Attention, vous avez saisi une date de fin, cela signifie que votre objet n\'existe plus à partir de cette date !</big><p> Si votre objet existe encore, indiquez <b>0</b> comme date de fin.<br> Si votre objet a bien été supprimé (alignement de peuliers, abreuvoir sauvage etc.) vous pouvez conserver votre date de fin.</p>');

}

function displayWrongDate(input, comment, bouton, disable) {


    $('#' + input).css('color', 'red');
    $('#' + comment).css('color', 'red');
    $('#' + comment).html('Date non valide (0, aaaa-mm ou aaaa-mm-jj autorisés)');
    if (disable) {
        $('#' + bouton).prop('disabled', true);
    }
}

function displayRightDate(input, comment, bouton, disable) {

    $('#' + input).css('color', 'green');
    $('#' + comment).css('color', 'green');
    $('#' + comment).html('Date valide');
    if (disable) {
        $('#' + bouton).prop('disabled', false);
    }

}

// donne le format présumé d'une date
// shortData du type aaaa-mm
// completeDate du type aaaa-mm-jj

function checkDateType(date) {

    if (date.length < 7) {
        return false;
    }
    if (date.length === 7) {
        return 'shortDate';
    }
    if (date.length === 10) {
        return 'completeDate';
    }


}



/*
 * hide and show
 */
function hs(element) {

    var vis = $('#' + element).css('display');
    if (vis === 'none') {
        $('#' + element).show(150);
    }
    if (vis === 'block') {

        $('#' + element).hide(150);
    }

}

/*
 * hide
 */

function h(element) {
    $('#' + element).hide(150);
}

/*
 * show
 */

function s(element) {
    $('#' + element).show(150);
}



/*
 * hide only display
 */

function hsimple(element) {
    $('#' + element).hide(0);
}

/*
 * show
 */

function ssimple(element) {
    $('#' + element).show(0);
}


/* convert hex color to RGB */
function hexToRgb(hex) {
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null;
}


function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}






//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// "mapFunctions.js" functions
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////







// set the layer style with optionnal feature style
function setLayerStyle(theLayer, layerstyle) {
    theLayer.setStyle(
        function (feature) {
            return layerStyleFn(feature.style, layerstyle)
        });
}



// load the layer style with optionnal feature style
function layerStyleFn(featureStyle, defaultStyle) {

    if (featureStyle === undefined) {
        return defaultStyle;
    }
    else {


        return {
            color: featureStyle['color'] || "blue",
            fillColor: featureStyle['fillColor'] || featureStyle['color'] || "blue",
            opacity: featureStyle['opacity'] || "1",
            fillOpacity: featureStyle['fillOpacity'] || "0.8",
            weight: featureStyle['weight'] || "3",
            radius: featureStyle['radius'] || "7",
            stroke: featureStyle['stroke'] || true,
            lineCap: featureStyle['lineCap'] || 'round',
            lineJoin: featureStyle['lineJoin'] || 'round',
            dashArray: featureStyle['dashArray'] || null,
            dashOffset: featureStyle['dashOffset'] || null,
            // fill : featureStyle['fill'],
            // fillRule : featureStyle['fillRule'] || 'evenodd',
            fillRadius: featureStyle['fillRadius'] || "5",
            iconUrl: featureStyle['iconUrl'] || null
        }
    }
}


// specific function for the cadastre layer
function addLayerCad(id, buffer, layerstyle) {

    var myLayerName = 'layerCadastre' + id;
    var myLayer = new L.GeoJSON.AJAX("/geojson/cadastre/" + id + "/" + buffer,
        {
            id: myLayerName,
            name: myLayerName,
            onEachFeature: popUp,
            style: function (feature) {
                return layerStyleFn(feature.style, layerstyle)
            },
        });

    suff = '098'; // suffixe cadastre
    myLayer._leaflet_id = Number(id + suff);
    map.addLayer(myLayer);
}


function refreshLayers(layers) {
    console.log("refreshLayers: TODO refactor to gold standard? cf. addLayer and refreshLayer");
    //console.log(layers) ;


    var myLayer = new L.GeoJSON.AJAX(layers,

        {
            middleware: function (data) {
                //console.log(data) ;
                data._leaflet_id = 10001;
            },
            // id: myLayerName,
            // name: myLayerName,               
            onEachFeature: popUp,
            style: function (feature) {
                return feature.style
            },
            pointToLayer: function (feature, latlng) {
                // default point marker = circle
                if (feature.style.radius !== undefined) { radius = feature.style.radius } else { radius = 7 }
                return new L.CircleMarker(latlng).setRadius(radius);
            }
        });

    //console.log(myLayer) ;


    // myLayer._leaflet_id = new_leaflet_id ;

    myLayer.on('data:loaded', function (data) {
        ////console.log(data) ;
        // //console.log('loaded') ;
        // map.removeLayer(map._layers) ; 
        ////console.log('removed') ;
        map.addLayer(myLayer);
        // //console.log(myLayer);                      

    });



}




function createLabel(feature) {

    //console.log('FUNCTION CREATE LABEL')

    var iconSizeCalc = Number(feature.style.labelSize * 2)
    var posCalc = Number(iconSizeCalc) / 2
    var labelFillRadius = iconSizeCalc / 2 * feature.style.labelFillRadius / 10


    if (feature.style.iconUrl !== undefined && feature.style.iconUrl !== '') {
        var leftMargin = 0;
        if (Number(feature.style.labelSize) / 2 > Number(feature.style.radius)) {
            var leftMargin = Number(feature.style.radius) / 2
        }
        var labelLeftPadding = Math.max(Number(feature.style.radius) * 6, Number(feature.style.labelSize) * 2) + leftMargin
        var labelLeftMargin = 0


    } else {


        var labelLeftPadding = Number(6)
        var labelLeftMargin = Number(feature.style.labelSize) + 12
    }

    var label = feature.properties.name



    var labelCoords = [0, 0]
    if (feature.properties.type_geom == 'ST_Point') {
        var labelCoords = feature.geometry.coordinates
    } else if (feature.properties.type_geom == 'ST_Linestring') {
        var labelCoords = feature.geometry.coordinates[Math.round(feature.geometry.coordinates.length / 2)]
    } else if (feature.properties.type_geom == 'ST_Polygon') {
        var labelCoords = feature.geometry.coordinates[0][Math.round(feature.geometry.coordinates.length / 2)]
    }

    var fontSize = Number(feature.style.labelSize)
    var labelSize = 'fit-content'
    var html = null;
    var labelFillColor = hexToRgb(feature.style.labelFillColor)

    var topMargin = iconSizeCalc / 2
    if (feature.style.iconUrl !== undefined && feature.style.iconUrl !== '') {
        var topMargin = -iconSizeCalc / 2 + Number(feature.style.radius) * 5 / 2
    }


    html = '<div style="--LABEL;position:relative;top:' + topMargin + 'px;white-space: nowrap;text-align:center;width:' + labelSize + ';color:' + feature.style.labelColor + ';background-color: rgb(' + labelFillColor.r + ',' + labelFillColor.g + ',' + labelFillColor.b + ',' + feature.style.labelFillOpacity + ');line-height:' + iconSizeCalc + 'px;height:' + iconSizeCalc + 'px;border-radius:' + labelFillRadius + 'px;padding-left:' + labelLeftPadding + 'px;padding-right:5px;margin-left:' + (labelLeftMargin) + 'px;font-size:' + fontSize + 'px" title="' + feature.properties.name + '"><span style="opacity:' + feature.style.labelOpacity + '">' + label + '</span></div>'

    return { 'html': html, 'coordinates': labelCoords, 'iconAnchor': [posCalc, posCalc], 'iconSize': [iconSizeCalc, iconSizeCalc] }



}

/*****************************************************************************************************
 * 
 */

function getGeojsonOption(myLayerName, type, id, layerstyle, myDecorationLayer = null) {

   
    geojsonOption = {
        decorationLayer: new L.LayerGroup(),
        id: myLayerName,
        name: myLayerName,
        sysmaOptions: { 'object_type': type, 'object_id': id },
        style: function (feature) {
            return layerStyleFn(feature.style, layerstyle)
        },
        /*****************************************************************************************/
        onEachFeature: function (feature, layer) {
           
            popUp(feature, layer, id);            
            // console.log("onEachFeature | feature:", feature)
            if (feature.style !== undefined) {
                if (feature.style.label === '1' && feature.properties.name !== '' && feature.properties.type_geom !== 'ST_Point') {
                    //console.log('LABEL') ;
                    var myLabelInfos = createLabel(feature)
                    //console.log(myLabelInfos)
                    var divIcon = L.divIcon({
                        className: "leaflet-data-marker",
                        html: myLabelInfos.html,
                        iconAnchor: myLabelInfos.iconAnchor,
                        iconSize: myLabelInfos.iconSize,
                        popupAnchor: [0, 0]
                    })
                    var labelMarker = new L.marker([myLabelInfos.coordinates[1], myLabelInfos.coordinates[0]], { onEachFeature: function (feature, layer) { popUp(feature, layer, id) } }).setIcon(divIcon);
                    labelMarker.bindPopup(popUpContent(feature).join('<br>'))
                    // myLayer.addLayer(labelMarker);
                    // layer.addLayer(labelMarker);
                    layer.options.decorationLayer.addLayer(labelMarker)
                }

                if (feature.properties.type_geom === 'ST_Linestring') {
                    // console.log("feature.style: ", feature.style)
                    // console.log("feature.geometry: ", feature.geometry)
                    if (feature.style.secondLine === '1') {
                        var newFeature = new L.polyline(L.GeoJSON.AJAX.coordsToLatLngs(feature.geometry.coordinates), { color: feature.style.secondLineColor, weight: feature.style.secondLineWeight, opacity: feature.style.secondLineOpacity });
                        // console.log("newFeature ", newFeature)
                        newFeature.bindPopup(popUpContent(feature).join('<br>'))
                        // layer.addLayer(newFeature)
                        layer.options.decorationLayer.addLayer(newFeature)
                    }

                    if (feature.style.lineExtremities === '1') {
                        var firstPointHtml = '<div style="background-color:' + feature.style.color + ';background-opacity:' + feature.style.opacity + ';width:10px;height:10px;border-radius:10px"></div>';
                        var firstPointDivIcon = L.divIcon({
                            className: "leaflet-data-marker",
                            html: firstPointHtml,
                            iconAnchor: [5, 5],
                            iconSize: [10, 10],
                            popupAnchor: [5, 5]
                        })
                        var firstPoint = new L.marker([feature.geometry.coordinates[0][1], feature.geometry.coordinates[0][0]]).setIcon(firstPointDivIcon);
                        // myLayer.addLayer(firstPoint);
                        // layer.addLayer(firstPoint);
                        layer.options.decorationLayer.addLayer(firstPoint)
                        var geomLength = feature.geometry.coordinates.length - 1
                        var lastPoint = new L.marker([feature.geometry.coordinates[geomLength][1], feature.geometry.coordinates[geomLength][0]]).setIcon(firstPointDivIcon);
                        // myLayer.addLayer(firstPoint);
                        // layer.addLayer(firstPoint);
                        layer.options.decorationLayer.addLayer(lastPoint)

                    }
                }
            }
        },
        /*****************************************************************************************/
        pointToLayer: function (feature, latlng) {
            // no specific style have been set
            if (feature.style == undefined) {
                return L.circleMarker(latlng);
            }

            let labelSize = 0
            if (feature.style.radius == undefined) { feature.style.radius = feature.style.weight * 3 }
            if (feature.style.fillColor == undefined) { feature.style.fillColor = feature.style.color }

            var size = feature.style.radius * 5 || 50
            var fillRadius = size / 2 * feature.style.fillRadius / 10
            var backColor = hexToRgb(feature.style.fillColor)
            var html = null


            // console.log("pointToLayer | feature:", feature)
            //specific icon is set
            if ((typeof feature.style.icon != 'undefined') && !isNumeric(feature.style.icon) && (feature.style.icon !== '')) {
                const svgIcon = L.divIcon({
                    html: feature.style.icon,
                    className: "svg-icon",
                    iconSize: [22, 22],
                    iconAnchor: [11, 22],
                });
                return L.marker(latlng, { icon: svgIcon })
            } else if (feature.style.iconUrl !== undefined && feature.style.iconUrl !== '') {
                html = '';
                var leftMargin = 0
                var topMargin = 0

                if (feature.style.label === '1' && feature.properties.name !== '') {
                    var myLabelInfos = createLabel(feature)
                    html = html + myLabelInfos.html
                    if (Number(feature.style.labelSize) / 2 > Number(feature.style.radius)) {
                        var leftMargin = Number(feature.style.radius) / 2
                    }
                    var topMargin = -myLabelInfos.iconSize[0]
                    //var topMargin = 0
                }

                html = html + '<div style="--POINT/ICON;position:relative;top:' + topMargin + 'px;background-color:rgba(' + backColor.r + ',' + backColor.g + ',' + backColor.b + ',' + feature.style.fillOpacity + ');width:' + (size) + 'px;height:' + size + 'px;border-radius:' + fillRadius + 'px;margin-left:' + Number(leftMargin) + 'px;"><div style="position:relative;width:' + size + 'px;height:' + size + 'px;background:' + feature.style.color + ';-webkit-mask-image:url(' + feature.style.iconUrl + ');-webkit-mask-repeat: no-repeat;-webkit-mask-size:' + size * 0.7 + 'px;-webkit-mask-position: center center;mask-image:url(' + feature.style.iconUrl + ');mask-repeat: no-repeat;mask-size:' + size * 0.7 + 'px;mask-position: center center;opacity:' + feature.style.opacity + ';"></div>';

                divIcon = L.divIcon({
                    className: "leaflet-data-marker",
                    html: html,
                    iconAnchor: [feature.style.radius * 5 / 2, feature.style.radius * 5 / 2],
                    iconSize: [feature.style.radius * 5, feature.style.radius * 5],
                    popupAnchor: [1, -feature.style.radius + 2]
                })
            } else {
                var iconSizeCalc = Number(feature.style.radius) * 2 + 10
                var posCalc = Number(iconSizeCalc) / 2
                var radiusCalc = Number(feature.style.radius)
                var html = '';
                var leftMargin = 0
                var topMargin = 0
                if (feature.style.label === '1' && feature.properties.name !== '') {
                    var myLabelInfos = createLabel(feature)
                    html = myLabelInfos.html
                    var topMargin = -myLabelInfos.iconSize[0] - iconSizeCalc / 2 - 5
                    var leftMargin = Number(feature.style.radius)
                }

                html = '<div style="--POINT/NO-ICON;width:' + Number(iconSizeCalc) + 'px;height:' + Number(iconSizeCalc) + 'px"><div style=""><svg height="' + iconSizeCalc + '" width="' + iconSizeCalc + '"><circle cx="' + posCalc + '" cy="' + posCalc + '" r="' + radiusCalc + '" stroke="' + feature.style.color + '" stroke-opacity="' + feature.style.opacity + '" stroke-width="' + feature.style.weight + '" fill="' + feature.style.fillColor + '" fill-opacity="' + feature.style.fillOpacity + '"/></svg></div><div style="width:' + Number(iconSizeCalc) + 'px;height:' + Number(iconSizeCalc) + 'px;position:relative;top:' + topMargin + 'px;margin-left:' + leftMargin + 'px">' + html + '</div>';

                var divIcon = L.divIcon({
                    className: "leaflet-data-marker",
                    html: html,
                    iconAnchor: [iconSizeCalc / 2, iconSizeCalc / 2],
                    iconSize: [iconSizeCalc, iconSizeCalc],
                    popupAnchor: [0, -iconSizeCalc / 2]
                })
            }

            markerOptions = { 'object_type': type, 'object_id': id }

            return new L.marker(latlng, { 'sysmaOptions': markerOptions }).setIcon(divIcon);
        }
    };
   
    return geojsonOption;
}



/*****************************************************************************************************
 * 
 */
async function setupLayer(type, id, layerstyle, parentid, options) {   
    parentid = parentid || null;
    layerstyle = layerstyle || null;
    //console.log(options) ;
    if (options != undefined) {
        center = options.center;
        zoom = options.zoom;
    } else {
        center = null;
        zoom = null;
    }

    //console.log('Add layer '+type+' '+id+', center : '+center+', zoom : '+zoom) ;

    if (type === 'objecttype') {
        suff = '001';
    } else if (type === 'actiontype') {
        suff = '002';
    } else {
        suff = '099';
    }

    new_leaflet_id = Number(id + suff);
    //console.log('new_leaflet_id : ', new_leaflet_id);    

    removeLayerAndKeepItSelected(type, id, null, null, layerstyle, parentid);

    var myLayerName = 'layer' + type + id;
    console.log("setupLayer | /geojson/" + type + "/" + id);

    var myLayer = new L.Layer();
    var decorationLayer = new L.LayerGroup()



    // myLayer = new L.GeoJSON.AJAX("/geojson/" + type + "/" + id + "?center=" + center + "&zoom=" + zoom, getGeojsonOption(myLayerName, type, id, layerstyle));

    /****************************** */
    const sysmaAppStateDB = await idb.openDB('sysmaAppStateDB', 1, {
        upgrade(db) {
            const store = db.createObjectStore('state', {
                keyPath: 'parameterName',
                autoIncrement: true,
            });
            store.createIndex('parameterName', 'parameterName');
        },
    });
    const isOfflineEnabled = await sysmaAppStateDB.get('state', 'isOffline');

    if ((typeof isOfflineEnabled != "undefined") && (isOfflineEnabled.value)) {
        ///////////////////////// offline specific
        console.log("setupLayer: offline mode");

        let ident = parseInt(id);
        const sysmaLayersDB = await idb.openDB('sysmaLayersDB');
        // console.log(sysmaLayersDB);
        var geojson = await sysmaLayersDB.get('layers', ident);
        // console.log(geojson);

        // pass geojson to leaflet
        myLayer = new L.GeoJSON(geojson.data, getGeojsonOption(myLayerName, type, id));

        // myLayer.addTo(map);
    } else {
        ///////////////////////// online specific
        console.log("setupLayer | online mode");
        // load geojson frome API into leaflet
        geojsonUrl = "/geojson/" + type + "/" + id + "?center=" + center + "&zoom=" + zoom;
        // console.log("addLayer: geojsonUrl: " + geojsonUrl)
        // console.log("addLayer: geojsonOption: ", getGeojsonOption(myLayerName, type, id))
        
        myLayer = new L.GeoJSON.AJAX(geojsonUrl, getGeojsonOption(myLayerName, type, id, layerstyle));
       
    }

    /*************************************************** */

    myLayer.on('add', function (data) {
        console.log("setupLayer | layer | on 'add'")
        sendData2('type=' + type + '&id=' + id + '&action=add&layer=' + myLayerName + '&leaflet_id=' + new_leaflet_id + '&layerstyle=' + JSON.stringify(layerstyle) + '&parentid=' + parentid + '&options=' + JSON.stringify(options), '/updatelayerstate', 'layerstate', null);


        //console.log('ADD LAYER / myLayer.on(add)') ;
        //console.log('DATA TARGET') ;
        //console.log(data.target) ;       

        if (Object.keys(data.target._layers).length == 0) {

            //console.log('Layer '+type  + id+' is empty') ;   
            $('#layer' + type + id + 'LayerControllerSpan').css('display', 'none');
            $('#layer' + type + id + 'LayerController').css('display', 'block');
            $('#layer' + type + id + 'LayerName').css('color', '#ccc');

        } else {

            //console.log('#layer'+type+id+'LayerName')

            $('#layer' + type + id + 'LayerName').css('color', 'rgb(51,51,51)');
            $('#layer' + type + id + 'LayerControllerSpan').css('display', 'none');
            $('#layer' + type + id + 'LayerController').css('display', 'block');


            //console.log('myLayer on load') ;
            //console.log(data.target) ;
            //console.log('compte ' + Object.keys(data.target._layers).length) ;




            // create a selectedMarker and add it to selectedLayer


            myLayer.on('click', function (e) {

                if (e.sourceTarget !== undefined && e.sourceTarget.feature !== undefined) {

                    if (e.sourceTarget.feature.properties.type_geom === 'ST_Point') {
                        var selectedMarker = new L.CircleMarker(L.GeoJSON.AJAX.coordsToLatLng(e.sourceTarget.feature.geometry.coordinates), { 'radius': Number(e.sourceTarget.feature.style.radius ?? 7) * 2 + 5, 'color': 'yellow', 'opacity': 0.5 })
                    } else if (e.sourceTarget.feature.properties.type_geom === 'ST_Linestring') {
                        var selectedMarker = new L.Polyline(L.GeoJSON.AJAX.coordsToLatLngs(e.sourceTarget.feature.geometry.coordinates), { 'weight': Number(e.sourceTarget.feature.style.radius ?? 7) * 2 + 5, 'color': 'yellow', 'opacity': 0.5 })
                    } else if (e.sourceTarget.feature.properties.type_geom === 'ST_Polygon') {
                        var selectedMarker = new L.Polygon(L.GeoJSON.AJAX.coordsToLatLngs(e.sourceTarget.feature.geometry.coordinates[0]), { 'weight': 5, 'color': 'yellow', 'opacity': 1, 'fillColor': 'yellow', 'fillOpacity': 0.5 })
                    }

                    selectedLayer.addLayer(selectedMarker);
                    selectedLayer.addTo(map);

                    selectedItems.push({ 'id': e.sourceTarget._leaflet_id, 'style': layerStyleFn(map._layers[e.sourceTarget._leaflet_id].feature.style, layerstyle) });

                }
            });
        }

    });


    // $('#' + myLayerName + 'LayerController').prop('checked', true);
    // $('#' + myLayerName + 'LayerController').attr('onclick', 'removeLayer("' + type + '","' + id + '", "' + myLayerName + '",' + myLayer._leaflet_id + ',' + JSON.stringify(layerstyle) + ',' + Number(parentid) + ')');
    // $('#' + myLayerName + 'LayerName').wrap("<strong></strong>");
    // $('#layer' + type + 'Row' + id).wrap('<div style="\" background-color:#ececec\""></div>');
    /*
        if (parentid != null) {
            $('#layerobjecttypeRow' + parentid).wrap('<div style="\" background-color:#ececec\""></div>');
        }
    */


    myLayer.on('data:loading', function (data) {        
        console.log("setupLayer | layer | on 'data:loading'")
        $('#layer' + type + id + 'LayerController').css('display', 'none');
        $('#layer' + type + id + 'LayerControllerSpan').css('display', 'block');
        $('#layer' + type + id + 'LayerName').css('color', '#ccc');
    });


    myLayer.on('data:loaded', function (data) {
        console.log("setupLayer | layer | on 'data:loaded'")
        //console.log('ADDLAYER / myLayer.on(data:loaded)') ;
        // myLayer.addLayer(myLayer.decorationLayer);
        // map.addLayer(myLayer.decorationLayer);
        map.addLayer(myLayer.options.decorationLayer);
        map.addLayer(myLayer);
        // autoZIndex
        myLayer.options.decorationLayer.setZIndex(6);
        myLayer.setZIndex(5)

    });

    ///////////////////////// offline specific
    if ((typeof isOfflineEnabled != "undefined") && (isOfflineEnabled.value)) {
        map.addLayer(myLayer.options.decorationLayer);
        map.addLayer(myLayer);
        // autoZIndex
        myLayer.options.decorationLayer.setZIndex(6);
        myLayer.setZIndex(5)
    }

    return myLayer;
}


/*****************************************************************************************************
 * 
 */
function addLayer(type, id, layerstyle, parentid, options) {
    console.log('addLayer');
    var myLayer = setupLayer(type, id, layerstyle, parentid, options);
    var myLayerName = 'layer' + type + id;
    $('#' + myLayerName + 'LayerController').prop('checked', true);
    $('#' + myLayerName + 'LayerController').attr('onclick', 'removeLayer("' + type + '","' + id + '", "' + myLayerName + '",' + myLayer._leaflet_id + ',' + JSON.stringify(layerstyle) + ',' + Number(parentid) + ')');
    $('#' + myLayerName + 'LayerName').wrap("<strong></strong>");
    $('#layer' + type + 'Row' + id).wrap("<div style='background-color:#ececec'></div>");
    if (parentid != null) {
        $('#layerobjecttypeRow' + parentid).wrap("<div style='background-color:#ececec'></div>");
    }
}



/*****************************************************************************************************
 * 
 */
async function refreshLayer(type, id, layerstyle, parentid, options) {
    console.log('refreshLayer | type:', type);
    setupLayer(type, id, layerstyle, parentid, options);
    // setupLayer(type, id, layerstyle, parentid, options, );
}

/*****************************************************************************************************
 * 
 */
function addLayerToSimpleMap(type, id, layerstyle, parentid, options) {
    console.log("addLayerToSimpleMap: TODO refactor to gold standard? cf. addLayer")
    parentid = parentid || null;
    layerstyle = layerstyle || null;
    //console.log(options) ;
    if (options != undefined) {
        center = options.center;
        zoom = options.zoom;
    } else {
        center = null;
        zoom = null;
    }

    if (type === 'objecttype') {
        suff = '001';
    } else if (type === 'actiontype') {
        suff = '002';
    } else {
        suff = '099';
    }

    new_leaflet_id = Number(id + suff);


    var myLayerName = 'layer' + type + id;
    var myLayer = new L.GeoJSON.AJAX("/geojson/" + type + "/" + id + "?center=" + center + "&zoom=" + zoom,
        {
            id: myLayerName,
            name: myLayerName,
            onEachFeature: popUp,
            style: function (feature) {
                return layerStyleFn(feature.style, layerstyle)
            },
            pointToLayer: function (feature, latlng) {

                if (layerstyle != undefined && layerstyle.radius != undefined) {
                    return L.circleMarker(latlng).setRadius(layerstyle.radius);
                } else if (feature.style != undefined && feature.style.radius != undefined) {
                    return L.circleMarker(latlng).setRadius(feature.style.radius);
                } else {
                    return L.circleMarker(latlng).setRadius(5);
                }

            }
        });


    myLayer.id = 'layer' + type + id;
    myLayer._leaflet_id = new_leaflet_id;

    map.addLayer(myLayer);

}


function removeLayerAndKeepItSelected(type, id, layer, leaflet_id, layerstyle, parentid) {

    //console.log('removeLayerAndKeepItSelected '+type+id) ;
    parentid = parentid || null;
    layerstyle = layerstyle || null;

    //map.removeLayer(map._layers[leaflet_id]);

    $('#layer' + type + id + 'LayerName').css('color', '#ccc');
    removeLayerByName(map, type, id);
    /*
    $('#layer' + type + id + 'LayerName').unwrap();
    $('#layer' + type + 'Row' + id).unwrap();
    if (parentid != null)
    {
        $('#layerobjecttypeRow' + parentid).unwrap();
    }
    */
    return true;

}



function removeLayer(type, id, layer, leaflet_id, layerstyle, parentid) {

    parentid = parentid || null;
    layerstyle = layerstyle || null;
    //map.removeLayer(map._layers[leaflet_id]);

    removeLayerByName(map, type, id);
    $('#layer' + type + id + 'LayerName').unwrap();
    $('#layer' + type + 'Row' + id).unwrap();
    if (parentid != null && parentid != 0 && parentid != '0') {
        $('#layerobjecttypeRow' + parentid).unwrap();
    }
    $('#layer' + type + id + 'LayerName').css('color', 'rgb(51,51,51)');
    $('#' + layer + 'LayerController').attr('onclick', 'addLayer("' + type + '","' + id + '",' + JSON.stringify(layerstyle) + ',' + Number(parentid) + ', { "center" : $(\'#center\').val(), "zoom" : $(\'#zoom\').val() })');
    sendData2('type=' + type + '&id=' + id + '&action=delete&layer=' + layer + '&leaflet_id=' + new_leaflet_id + '&layerstyle=' + JSON.stringify(layerstyle) + '&parentid=' + parentid + '&options=', '/updatelayerstate', 'layerstate', null);
    $('#' + layer + 'LayerController').attr('checked', false);

    return true;

}


function removeLayerByName(map, type, id) {
    // console.log("removeLayerByName")

    $.each(map._layers, function (index, layer) {
        

        if (layer.options.id != undefined && layer.options.id == 'layer' + type + id) {

           // console.log('remove layer by name '+type+id+' : layer option id : '+layer.options.id) ;

          //  console.log(layer) ;

            if (layer.options.hasOwnProperty('decorationLayer')) {
                map.removeLayer(layer.options.decorationLayer);
            }
            map.removeLayer(layer);
            layer = null;

        }
    });

}


function zoomToLayerBoundsByLayerName(map, type, id) {

    boundsLayer = new L.FeatureGroup();

    $.each(map._layers, function (index, layer) {
        if (layer.options.id != undefined && layer.options.id == 'layer' + type + id) {
            layer.addTo(boundsLayer);
        }
    });
    if (boundsLayer.getBounds()._northEast != undefined) {
        map.fitBounds(boundsLayer.getBounds());
    }

}

function selectFeature(feature, layer, layerstyle) {

    feature.sysmaStyle = layerStyleFn(feature.style, layerstyle);
    /*
        layer.on('click', function(e) {        
            
            //console.log(e.sourceTarget._leaflet_id) ;
            selectedItems.push({'id' : e.sourceTarget._leaflet_id, 'style' : layerStyleFn(feature.style,layerstyle)}) ;
            map._layers[e.sourceTarget._leaflet_id].setStyle({'color': '#ffff33', 'weight': '7'});
        }) ;*/
}



/*
    * POPUP
    */
function popUp(feature, layer, id = null) {

    /* making a unique leafelt_id */
   
    // sysma internal layers (objets, action, cadastre, simple_pg_layers)
    if (feature.properties.type) {
        t = feature.properties.type.substring(0, 1);
    }
    // other layers declared in other layers (external layers)
    else if (id) {        
        t = 'other'+id
       
    }
    // other other layers (just in case)
    else {
        t = 'z'
    }
    var i = feature.id
    if (feature.properties.id) {
        i = feature.properties.id
    }

    layer._leaflet_id = t + i.substring(1); 
    //   console.log(layer._leaflet_id) ;

    if (feature.properties) {
        layer.bindPopup(popUpContent(feature).join("<br />"));
    }  
}


function showPolygonArea(e) {
    drawnItems.clearLayers();
    drawnItems.addLayer(e.layer);
    return ((LGeo.area(e.layer)).toFixed(2) + ' m<sup>2</sup>');
}



function loadFeature(object_id, type_geom) {

    selectedFeature = map._layers[object_id];
    selectedFeature.closePopup();
    console.log(selectedFeature);
    selectedLayer.remove();
    selectedLayer.clearLayers();
    selectedFeature.enableEdit();

}


function cancelGeoEditFeature(object_id, id_type) {
    // console.log('cancelGeoEditFeature') ;      

    var snap = null;
    selectedFeature = map._layers[object_id];
    selectedFeature.disableEdit();
    refreshLayer('objecttype', id_type);

}



function cancelGeoCreateFeature(id_type) {

    //console.log('Cancel geo new') ;   
    map.editTools.stopDrawing();
    drawnItems.clearLayers();

}



function showObjectInMap(id) {

    if (map._layers[id] !== undefined) {

        
        //console.log(map._layers[ + id]);

        if (map._layers[id]._path !== undefined) {

           
            
           

            map._layers[id].options.init = {                
                "stroke":  map._layers[id].options.stroke,
                "color":  map._layers[id].options.color,
                "weight":map._layers[id].options.weight,
                "opacity":  map._layers[id].options.opacity,
                "fill" : map._layers[id].options.fill,
                "fillColor" : map._layers[id].options.fillColor,
                "fillOpacity" :map._layers[id].options.fillOpacity
            };

            map._layers[id].setStyle( {'stroke' : true, 'color' : '#e6f557', 'weight' : 4, 'fill' : true , 'fillColor' : '#e6f557', 'fillOpacity' : 0.4  })         
         
            map.panTo(map._layers[id].getCenter());

            map._layers[id].openPopup();

        }

    } 
  

}

function hideObjectInMap(id) {

    if (map._layers[id] !== undefined) {

        map._layers[id].closePopup();

        if (map._layers[id]._path !== undefined) {  
            
            let init = map._layers[id].options.init ;         

            map._layers[id].setStyle( { 'stroke' : init.stroke, 'color' : init.color, 'weight' : init.weight, 'fill' : init.fill,  'fillColor' : init.fillColor, 'fillOpacity' : init.fillOpacity }  ) ;

         
        }

    }
    

}

function slugify(str) {
    str = str.trim() ;
   // str = str.replace(/^\s+|\s+$/g, ''); // trim leading/trailing white space
    str = str.toLowerCase(); // convert string to lowercase
    str = str.trim()
    str = str.replace(/[^a-z0-9]/g, '_') ; // remove any non-alphanumeric characters
    str = str.replace(/\s+/g, '_') ;  // replace spaces with hyphens
    str = str.replace(/-+/g, '_') ; // remove consecutive hyphens
    return str;
  }


  function forceAlias(source,target,length) {

    
    let p = document.getElementById(source) ;
    let a = document.getElementById(target) ;
    p.addEventListener('keyup', function() {
        a.value = slugify(p.value).substring(0,length-1) ;
    }) ;
    a.addEventListener('keyup', function() {
        a.value = slugify(a.value);
    }) ;

}

