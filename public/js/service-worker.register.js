
/**********************************************************
 *                                                        *
 * Registration of the service worker and declaration     *
 * of the file containing the core of the service worker  *
 *                                                        *
 **********************************************************/


const registerServiceWorker = async () => {
    if ('serviceWorker' in navigator) {
        try {
            const registration = await navigator.serviceWorker.register(
                '/service-worker.js',
                {
                    scope: '/',
                }
            );
            if (registration.installing) {
                console.log('Service worker is installing');
            } else if (registration.waiting) {
                console.log('Service worker is installed');
            } else if (registration.active) {
                console.log('Service worker is active');
            }
        } catch (error) {
            console.error(`Registration failed with ${error}`);
        }
    }
};
registerServiceWorker();

// if ("serviceWorker" in navigator) {
//     navigator.serviceWorker
//         .register("/service-worker.js")
//         .then(serviceWorker => {
//             console.log("Service Worker registered: ", serviceWorker);
//         })
//         .catch(error => {
//             console.error("Error registering the Service Worker: ", error);
//         });
// }

// if ('serviceWorker' in navigator) {
//     window.addEventListener('load', function () {
//         navigator.serviceWorker
//             .register('/service-worker.js')
//             .then(serviceWorker => {
//                 console.log("Service Worker registered: ", serviceWorker);
//             })
//             .catch(error => {
//                 console.error("Error registering the Service Worker: ", error);
//             });
//     });
// }