
/***************************************************************
 *                                                             *
 * The core of the service worker, it describes the behaviour  *
 * of the service worker                                       *
 *                                                             *
 ***************************************************************/


// self.addEventListener("install", event => {
//     console.log("Service Worker installing.");
// });

// self.addEventListener("activate", event => {
//     console.log("Service Worker activating.");
// });

// First attempt, we just set the version to V1 and we describe the files that must be put in the cache
const CACHE_NAME = "V1";
const STATIC_CACHE_URLS = ["/", "/carte", "/js/editor.js", "/js/leaflet.ajax.js", "/js/leaflet.geometryutils.js", "/js/leaflet.measure.min.js", "/js/leaflet.search.js",
    "/js/bootstrap-toggle.min.js", "/js/jquery-ui.js", "/js/leaflet.draw.js", "/js/leaflet.js", "/js/leaflet.measurecontrol.min.js", "/js/leaflet.snap.js",
    "/js/utils.js", "/js/bootstrap.min.js", "/js/jquery.min.js", "/js/leaflet.editable.js", "/js/leaflet.lineextremities.js", "/js/leaflet.mouseposition.js",
    "/js/leaflet.textpath.js", "/js/leaflet.geodesy.js", "/js/leaflet.path.drag.js",
    "/service-worker.js", "/js/sysma-offline.js",
    "/css/L.Control.Locate.css", "/css/bootstrap-toggle.min.css", "/css/bootstrap.min.css", "/css/custom-nomap.css", "/css/custom-print-simple.css", "/css/custom-print.css",
    "/css/custom.css", "/css/jquery-ui.css", "/css/lato.css", "/css/leaflet.css", "/css/leaflet.draw.css", "/css/leaflet.mapslider.css", "/css/leaflet.measure.css",
    "/css/leaflet.mouseposition.css", "/css/leaflet.search.css", "/css/roboto.css"];


// self.addEventListener("install", event => {
//     console.log("Service Worker installing.");
//     event.waitUntil(
//         caches.open(CACHE_NAME).then(cache => cache.addAll(STATIC_CACHE_URLS))
//     );
// });

// self.addEventListener("activate", event => {
//     console.log("Service Worker activating.");
// });

// self.addEventListener("fetch", event => {
//     // Cache-First Strategy
//     event.respondWith(
//         caches
//             .match(event.request) // check if the request has already been cached
//             .then(cached => cached || fetch(event.request)) // otherwise request network
//     );
// });



const addResourcesToCache = async (resources) => {
    const cache = await caches.open(CACHE_NAME);
    await cache.addAll(resources);
};

const putInCache = async (request, response) => {
    const cache = await caches.open(CACHE_NAME);
    await cache.put(request, response);
};

const cacheFirst = async ({ request, preloadResponsePromise, fallbackUrl }) => {
    // First try to get the resource from the cache
    console.log('1');
    const responseFromCache = await caches.match(request);
    if (responseFromCache) {
        return responseFromCache;
    }

    // Next try to use the preloaded response, if it's there
    const preloadResponse = await preloadResponsePromise;
    if (preloadResponse) {
        console.info('using preload response', preloadResponse);
        putInCache(request, preloadResponse.clone());
        return preloadResponse;
    }
    console.log('on est la')
    // Next try to get the resource from the network
    try {
        const responseFromNetwork = await fetch(request);
        // response may be used only once
        // we need to save clone to put one copy in cache
        // and serve second one
        putInCache(request, responseFromNetwork.clone());
        return responseFromNetwork;
    } catch (error) {
        const fallbackResponse = await caches.match(fallbackUrl);
        if (fallbackResponse) {
            return fallbackResponse;
        }
        // when even the fallback response is not available,
        // there is nothing we can do, but we must always
        // return a Response object
        return new Response('Network error happened', {
            status: 408,
            headers: { 'Content-Type': 'text/plain' },
        });
    }
};

const enableNavigationPreload = async () => {
    if (self.registration.navigationPreload) {
        // Enable navigation preloads!
        await self.registration.navigationPreload.enable();
    }
};

self.addEventListener('activate', (event) => {
    event.waitUntil(enableNavigationPreload());
});

self.addEventListener('install', (event) => {
    event.waitUntil(
        addResourcesToCache(STATIC_CACHE_URLS)
    );
    console.log('static cache is cached');
});

self.addEventListener('fetch', (event) => {
    event.respondWith(
        cacheFirst({
            request: event.request,
            preloadResponsePromise: event.preloadResponse,
            fallbackUrl: '/carte',
        })
    );
});
